﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MVC___Internal_System.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    class JsonFormDataFilter : ActionFilterAttribute
    {
        public string Parameter { get; set; }
        public Type JsonDataType { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (filterContext.HttpContext.Request.ContentType.Contains("multipart/form-data"))
                {
                        string inputContent = filterContext.HttpContext.Request.Form[Parameter];

                        string format = "dd/MM/yyyy"; // your datetime format

                        IsoDateTimeConverter dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };

                        object result = JsonConvert.DeserializeObject(inputContent, JsonDataType, dateTimeConverter);

                        filterContext.ActionParameters[Parameter] = result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.WriteLine(e.Message);
            }            
        }
    }
}
