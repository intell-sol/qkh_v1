﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace MVC___Internal_System.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class ReadOnlyAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //string s = "TODO";
        }
        //public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        //{
        //    throw new NotImplementedException();
        //}
    }
}