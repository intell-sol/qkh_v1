﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC___Internal_System.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class PermissionLevel : ActionFilterAttribute
    {
        public PermissionsHardCodedIds PermissionHCID { get; set; }
        
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            BEUser user = (BEUser)filterContext.HttpContext.Session["User"];
            if(user == null)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    //HttpUnauthorizedResult result = new HttpUnauthorizedResult();
                    ViewResult result = new ViewResult();
                    result.ViewName = "~/Views/Error/401.cshtml";
                    filterContext.HttpContext.Response.StatusCode = 401;
                    filterContext.Result = result;
                }
                else
                    filterContext.Result = new RedirectResult("/", true);
                return;
            }
            else
            {
                PermEntity permission = user.Permissions.Find(p => p.ID == (int)PermissionHCID);
                if (permission == null)
                {
                    //HttpStatusCodeResult result = new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden);
                    ViewResult result = new ViewResult();                    
                    result.ViewName = "~/Views/Error/403.cshtml";
                    filterContext.HttpContext.Response.StatusCode = 403;
                    filterContext.Result = result;
                    return;
                }

                filterContext.Controller.ViewBag.PPType = permission.PPType;
            }
        }
    }
}