﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using CommonLayer.BusinessEntities;

namespace MVC___Internal_System.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class ReadWriteAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            int PPType = (int)filterContext.Controller.ViewBag.PPType;

            if (PPType < 1)
            {
                filterContext.Result = new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden);
                return;
            }
        }
        
        //public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        //{
        //    BEUser user = (BEUser)controllerContext.HttpContext.Session["User"];

        //    if(user != null)
        //    {
        //        PermissionsHardCodedIds PermissionHCID = PermissionsHardCodedIds.Convicts;//((MVC___Internal_System.Controllers.BaseController)controllerContext.Controller).PermissionHCID;
        //        PermEntity permissions = user.Permissions.Find(permission => permission.ID == (int)PermissionHCID);
        //        if (permissions != null && permissions.PPType >= 1)
        //        {
        //            return true;
        //        }
        //    }

        //    controllerContext.HttpContext.Response.Status = "403 Forbidden";
        //    controllerContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
        //    controllerContext.HttpContext.Response.End();
        //    return false;
        //}
    }
}