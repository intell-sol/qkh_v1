﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace MVC___Internal_System
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.Reporting.WebForms;
    using System.Globalization;

    public class CReportViewerCustomMessages :
            IReportViewerMessages, IReportViewerMessages2, IReportViewerMessages3

    {
        #region IReportViewerMessages Members

        public string BackButtonToolTip
            {
                get { return ("Վերադարձ դեպի նախորդ հաշվետվություն"); }
            }

            public string ChangeCredentialsText
            {
                get { return ("Փոփոխության կառավարում"); }
            }

            public string ChangeCredentialsToolTip
            {
                get { return ("Փոփոխության կառավարում"); }
            }

            public string CurrentPageTextBoxToolTip
            {
                get { return ("Ընթացիկ էջ"); }
            }

            public string DocumentMap
            {
                get { return ("փաստաթղթում Քարտեզ"); }
            }

            public string DocumentMapButtonToolTip
            {
                get { return ("փաստաթղթում Քարտեզ"); }
            }

            public string ExportButtonText
            {
                get { return ("Արտահանել"); }
            }

            public string ExportButtonToolTip
            {
                get { return ("Արտահանել"); }
            }

            public string ExportFormatsToolTip
            {
                get { return ("արտահանման ձեւաչափերով"); }
            }

            public string FalseValueText
            {
                get { return ("Սխալ է"); }
            }

            public string FindButtonText
            {
                get { return ("Գտնել"); }
            }

            public string FindButtonToolTip
            {
                get { return ("Գտնել"); }
            }

            public string FindNextButtonText
            {
                get { return ("Հաջորդ"); }
            }

            public string FindNextButtonToolTip
            {
                get { return ("Հաջորդ"); }
            }

            public string FirstPageButtonToolTip
            {
                get { return ("Առաջին Էջ"); }
            }

            public string InvalidPageNumber
            {
                get { return ("Մուտքագրեք վավերական էջի համարը"); }
            }

            public string LastPageButtonToolTip
            {
                get { return ("Վերջին էջ"); }
            }

            public string NextPageButtonToolTip
            {
                get { return ("Հաջորդ էջ"); }
            }

            public string NoMoreMatches
            {
                get { return ("Այլ համընկնումներ չկան"); }
            }

            public string NullCheckBoxText
            {
                get { return ("ոչ մի արժեք"); }
            }

            public string NullValueText
            {
                get { return ("ոչ մի արժեք"); }
            }

            public string PageOf
            {
                get { return ("ից"); }
            }

            public string ParameterAreaButtonToolTip
            {
                get { return ("Ցույց տալ / թաքցնել Պարամետրեր"); }
            }

            public string PasswordPrompt
            {
                get { return ("Գաղտնաբառ"); }
            }

            public string PreviousPageButtonToolTip
            {
                get { return ("Նախորդ էջ"); }
            }

            public string PrintButtonToolTip
            {
                get { return ("Տպել"); }
            }

            public string ProgressText
            {
                get { return ("Բեռնում"); }
            }

            public string RefreshButtonToolTip
            {
                get { return ("Թարմացնել"); }
            }

            public string SearchTextBoxToolTip
            {
                get { return ("Գտնել տեքստ հաշվետվությունում"); }
            }

            public string SelectAValue
            {
                get { return ("<Ընտրեք արժեքը>"); }
            }

            public string SelectAll
            {
                get { return ("(Ընտրել բոլորը)"); }
            }

            public string SelectFormat
            {
                get { return ("Ընտրեք ձեւաչափը"); }
            }

            public string TextNotFound
            {
                get { return ("Խուզարկությունը տեքստը չի գտնվել:"); }
            }

            public string TodayIs
            {
                get { return ("Այսօր {0}"); }
            }

            public string TrueValueText
            {
                get { return ("Ճիշտ"); }
            }

            public string UserNamePrompt
            {
                get { return ("Մուտք Անունը:"); }
            }

            public string ViewReportButtonText
            {
                get { return ("Դիտել հաշվետվություն"); }
            }

            public string ZoomControlToolTip
            {
                get { return ("Մեծացնել"); }
            }

            public string ZoomToPageWidth
            {
                get { return ("Լայնությամբ"); }
            }

            public string ZoomToWholePage
            {
                get { return ("ամբողջ էջ"); }
            }

        #endregion
        #region IReportViewerMessages2 Members

        // English value: Your browser does not support scripts or has been configured not to allow scripts.
        public string ClientNoScript
        {
            get { return "Ձեր զննարկիչը չի աջակցում սցենարներ, կամ արդեն կազմաձեւված չէ թույլ սցենար."; }
        }

        // English value: Unable to load client print control.
        public string ClientPrintControlLoadFailed
        {
            get { return "Անհնար է բեռնել հաճախորդների տպել վերահսկողությունը."; }
        }

        // English value: One or more data sources is missing a user name.
        public string CredentialMissingUserNameError(string dataSourcePrompt)
        {
            return "Մեկ կամ ավելի տվյալների աղբյուրները բացակայում է օգտագործողի անունը:";
        }

        // English value is different for each Rendering Extension. See comment behind each type.
        public string GetLocalizedNameForRenderingExtension(string format)
        {
            switch (format)
            {
                case "XML": return "XML file with report data (.xml)";  // XML file with report data
                case "CSV": return "CSV (comma delimited) (.csv)";  // CSV (comma delimited)
                case "PDF": return "PDF document (.pdf)";     // PDF
                case "MHTML": return "Web archive (.mhtml)";     // MHTML (web archive)
                case "EXCEL": return "Excel (.xls)";  // Excel
                case "IMAGE": return "TIFF file (.tif)";       // TIFF file
                case "WORD": return "Word document (.doc)";    // Word
                default: return null;
            }
        }

        // English value: Select a value
        public string ParameterDropDownToolTip
        {
            get { return "Ընտրեք արժեք"; }
        }

        // English value: Please select a value for the parameter '{0}'.
        public string ParameterMissingSelectionError(string parameterPrompt)
        {
            return String.Format(CultureInfo.CurrentCulture, "Խնդրում ենք ընտրել պարամետրի արժեք '{0}'.", parameterPrompt);
        }

        // English value: Please enter a value for the parameter '{0}'. The parameter cannot be blank.
        public string ParameterMissingValueError(string parameterPrompt)
        {
            return String.Format(CultureInfo.CurrentCulture, "Խնդրում ենք մուտքագրել արժեք է պարամետր '{0}'. պարամետր չի կարող լինել դատարկ.", parameterPrompt);
        }

        #endregion

        #region IReportViewerMessages3 Members

        // English value: Loading...
        public string CalendarLoading
        {
            get { return "Բեռնում..."; }
        }

        // English value: Cancel
        public string CancelLinkText
        {
            get { return "Cancel"; }
        }

        // English value: pageCount if PageCountMode.Actual, else pageCount suffixed with a ?
        public string TotalPages(int pageCount, PageCountMode pageCountMode)
        {
            return string.Format(CultureInfo.CurrentCulture, "{0}{1}", pageCount, pageCountMode == PageCountMode.Estimate ? "~" : String.Empty);
        }

        #endregion
    }

}