﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using DataAccessLayer.DataCommunication;
using System.IO;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.MainData)]
    public class _Data_Primary_Controller : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public _Data_Primary_Controller()
        {
            PermissionHCID = PermissionsHardCodedIds.MainData;
        }
        /// <summary>
        /// Primary data add partial view
        /// </summary>
        public ActionResult Add()
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            PrisonerEntity Prisoner = new PrisonerEntity();

            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            Model.Prisoner = Prisoner;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել հիմնական տվյալներ"));

            return PartialView("Index", Model);
        }

        [HttpPost]
        /// <summary>
        /// Primary data edit partial view
        /// </summary>
        public ActionResult Edit(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            PrisonerEntity Prisoner = new PrisonerEntity();

            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            Prisoner.ID = PrisonerID.Value;

            if (PrisonerID != null)
            {
                Prisoner.MainData = BusinessLayer_PrisonerService.GetMainData(PrisonerID.Value);
            }

            Model.Prisoner = Prisoner;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել հիմնական տվյալները", RequestData: PrisonerID.ToString()));

            return PartialView("Index", Model);
        }

        [ReadWrite]
        public ActionResult AddPrevConviction(PreviousConvictionsEntity Data)
        {
            bool status = false;
            int? id = null;
            bool isOpening = false;
            bool needApprove = false;

            if (Data.PrisonerID != null)
            {
                    BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
                    id = BusinessLayer_PrisonerService.AddPreviousConviction(Data);

                    if (id != null && Data.SentencingDataArticles != null)
                    {
                        foreach (SentencingDataArticleLibsEntity curData in Data.SentencingDataArticles)
                        {
                            curData.PrevConvictionDataID = id;
                            int? curID = BusinessLayer_PrisonerService.AddSentencingDataArticle(curData);
                            if (curID == null) status = false;
                        }
                    }
                    if (id == null) status = false;
            }


            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status, isopen = isOpening, needApprove = needApprove });
        }
        [ReadWrite]
        public ActionResult EditPrevConviction(PreviousConvictionsEntity Data)
        {
            bool? status = false;
            bool needApprove = false;
            int PrisonerID = BusinessLayer_PrisonerService.GetPreviousConviction(new PreviousConvictionsEntity(ID: Data.ID)).First().PrisonerID.Value;
            PrisonerEntity currentPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID, false);
            if (Data.ID != null)
            {
                if (ViewBag.PPType == 2 && currentPrisoner.State.Value)
                {
                    MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID,
                        EmployeeID: ViewBag.UserID,
                        Json: JsonConvert.SerializeObject(Data),
                        EntityID: Data.ID,
                        Type: 3,
                        EID: (int)MergeEntity.TableIDS.PRIMARY_PREVENTION);

                    int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                    status = tempID != null;
                    needApprove = true;
                }
                else
                {
                    status = BusinessLayer_PrisonerService.UpdatePreviousConviction(Data);
                }
            }

            if (status == null) status = false;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status, needApprove = needApprove });
        }
        [ReadWrite]
        public ActionResult RemPrevConviction(int ID)
        {
            bool? status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetPreviousConviction(new PreviousConvictionsEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(new PreviousConvictionsEntity(ID: ID, Status: false)),
                    EntityID: ID,
                    Type: 2,
                    EID: (int)MergeEntity.TableIDS.PRIMARY_PREVENTION);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                status = tempID != null;
                needApprove = true;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdatePreviousConviction(new PreviousConvictionsEntity(ID: ID, Status: false));
            }

            if (status == null) status = false;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }

    }
}