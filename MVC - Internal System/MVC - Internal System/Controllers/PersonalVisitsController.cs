﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;
using Microsoft.Reporting.WebForms;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.PersonalVisits)]
    [HandleError()]
    public class PersonalVisitsController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public PersonalVisitsController()
        {
            PermissionHCID = PermissionsHardCodedIds.PersonalVisits;
        }

        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Տեսակցություններ";

                PersonalVisitsViewModel Model = new PersonalVisitsViewModel();
                
                Model.FilterPersonalVisits = new FilterPersonalVisitsEntity();
                if ((System.Web.HttpContext.Current.Session["FilterPersonalVisits"] as FilterPersonalVisitsEntity) != null)
                    Model.FilterPersonalVisits = (FilterPersonalVisitsEntity)System.Web.HttpContext.Current.Session["FilterPersonalVisits"];

                Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PERSONAL_VISITS_TYPE);
                Model.PurposeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PERSONAL_VISITS_PURPOSE);
               
                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեսակցություններ"));

                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեսակցության ավելացում"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }

        [HttpPost]
        public ActionResult Edit(int? PrisonerID)
        {
            PersonalVisitsViewModel Model = new PersonalVisitsViewModel();

            Model.PersonalVisit = BusinessLayer_PrisonerService.GetPersonalVisit(PrisonerID: PrisonerID.Value);
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեսակցության խմբագրում"));

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;
            return PartialView("Add", Model);
        }
        public ActionResult Draw_ResetPersonalVisitTableRow()
        {
            Session["FilterPersonalVisits"] = null;
            return Json(new { status = true });
        }
        public ActionResult Draw_PersonalVisitTableRow(int? id = null)
        {
            // UNDONE: watch this section
            FilterPersonalVisitsEntity FilterEntity = new FilterPersonalVisitsEntity();
            if ((System.Web.HttpContext.Current.Session["FilterPersonalVisits"] as FilterPersonalVisitsEntity) != null)
                FilterEntity = (FilterPersonalVisitsEntity)System.Web.HttpContext.Current.Session["FilterPersonalVisits"];

            int page = id ?? 1;

            FilterEntity.paging = new FilterPersonalVisitsEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PersonalVisitsDashboardEntity> ListDashboardPersonalVisits = BusinessLayer_PrisonerService.GetPersonalVisitsForDashboard(FilterEntity);

            PersonalVisitsViewModel Model = new PersonalVisitsViewModel();

            Model.ListDashboardPersonalVisits = ListDashboardPersonalVisits;
            Model.PersonalVisitsEntityPaging = FilterEntity.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեսակցությունների Դիտում"));
            
            return PartialView("PersonalVisits_Table_Row", Model);
        }

        public ActionResult Draw_PersonalVisitTableRowByFilter(FilterPersonalVisitsEntity FilterData)
        {
            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterPersonalVisits"] = FilterData;

            FilterData.paging = new FilterPersonalVisitsEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PersonalVisitsDashboardEntity> EntityList = BusinessLayer_PrisonerService.GetPersonalVisitsForDashboard(FilterData);

            PersonalVisitsViewModel Model = new PersonalVisitsViewModel();

            Model.ListDashboardPersonalVisits = EntityList;
            Model.PersonalVisitsEntityPaging = FilterData.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեսակցությունների Որոնում", RequestData: JsonConvert.SerializeObject(FilterData)));

            return PartialView("PersonalVisits_Table_Row", Model);
        }

        public ActionResult AddData(PersonalVisitsEntity Item)
        {
            bool status = false;
            int? id = null;

            Item.VisitStatus = null;
            Item.ApproverEmployeeID = null;

            if (Item.PrisonerID != null)
            {
                //BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];

                if (Item.VisitorList != null && Item.VisitorList.Any())
                {
                    status = true;

                    for (int i = 0; i < Item.VisitorList.Count; i++)
                    {
                        if (Item.VisitorList[i].Person != null)
                        {
                            Item.VisitorList[i].Person = BusinessLayer_PrisonerService.AddPersonAndGet(Person: Item.VisitorList[i].Person);
                        }
                     }
                }

                if (status)
                {
                    foreach (VisitorEntity curEntity in Item.VisitorList)
                    {
                        PersonalVisitsEntity tempEnt = Item;
                        tempEnt.PersonID = curEntity.Person.ID;

                        PrisonerEntity Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Item.PrisonerID.Value);

                        if (Prisoner.ArchiveData != null && Prisoner.ArchiveData.Value)
                        {
                            Item.VisitStatus = true;
                            Item.ApproverEmployeeID = ViewBag.UserID;
                        }

                        id = BusinessLayer_PrisonerService.AddPersonalVisit(tempEnt);
                        if (id == null) status = false;
                    }
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեսակցության ավելացում", RequestData: JsonConvert.SerializeObject(Item)));


            return Json(new { status = status });
        }

        public ActionResult RemData(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetPersonalVisits(new PersonalVisitsEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                PersonalVisitsEntity oldEntity = BusinessLayer_PrisonerService.GetPersonalVisits(new PersonalVisitsEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.PERSONAL_VISITS_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdatePersonalVisit(new PersonalVisitsEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեսակցության հեռացում", RequestData: ID.ToString()));


            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult EditData(PersonalVisitsEntity Item)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetPersonalVisits(new PersonalVisitsEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && PrisonerID.HasValue && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.PERSONAL_VISITS_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdatePersonalVisit(Item);
            }
            string s = Item.ToString();

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեսակցության խմբագրում", RequestData: JsonConvert.SerializeObject(Item)));


            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult CompleteData(PersonalVisitsEntity entity)
        {
            bool status = false;

            if (entity.ID != null && entity.EndDate != null)
            {
                PersonalVisitsEntity curEntity = new PersonalVisitsEntity(ID: entity.ID.Value, EndDate: entity.EndDate);

                status = BusinessLayer_PrisonerService.UpdatePersonalVisit(curEntity);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեսակցության հաստատում", RequestData: entity.ID.ToString()));


            return Json(new { status = status });
        }

        public ActionResult DeclineData(int? id)
        {
            bool status = false;

            if (id != null)
            {
                PersonalVisitsEntity curEntity = new PersonalVisitsEntity(ID: id.Value, VisitStatus: false, ApproverEmployeeID: ViewBag.UserID);

                status = BusinessLayer_PrisonerService.UpdatePersonalVisit(curEntity);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեսակցության մերժում", RequestData: id.ToString()));


            return Json(new { status = status });
        }
        public ActionResult ApproveData(int? id)
        {
            bool status = false;

            if (id != null)
            {
                PersonalVisitsEntity curEntity = new PersonalVisitsEntity(ID: id.Value, VisitStatus: true, ApproverEmployeeID: ViewBag.UserID);

                status = BusinessLayer_PrisonerService.UpdatePersonalVisit(curEntity);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեսակցության հաստատում", RequestData: id.ToString()));

            return Json(new { status = status });
        }
        #region Report
        [HttpPost]
        public ActionResult PersonalVisitsReportListData(PersonalVisitsReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեսակցություն report data ", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
        [HttpGet]
        public ActionResult PersonalVisitsReportList(string key)
        {
            PersonalVisitsReportEntity Item = (PersonalVisitsReportEntity)Session[key];
            SetPersonalVisitsReportList(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեսակցություն(get)", RequestData: JsonConvert.SerializeObject(Item)));
            return View("PersonalVisitsReportList");
        }


        private void SetPersonalVisitsReportList(PersonalVisitsReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;

            List<PersonalVisitsReportEntity> ent = BL_ReportLayer.FillPersonalVisitReport_List(entity);
            reportViewer.LocalReport.DisplayName = "Տեսակցություն";
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PersonalVisitReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("PersonalVisitDS", ent));
            ViewBag.PersonalVisitReportViewer = reportViewer;
        }
        #endregion
    }
}