﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.EducationalCourses)]
    public class EducationalCoursesController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public EducationalCoursesController()
        {
            PermissionHCID = PermissionsHardCodedIds.EducationalCourses;
        }
        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Դասընթացներ";

                EducationalCoursesViewModel Model = new EducationalCoursesViewModel();
                
                Model.FilterEducationalCourses = new FilterEducationalCoursesEntity();
                if ((System.Web.HttpContext.Current.Session["FilterEducationalCourses"] as FilterEducationalCoursesEntity) != null)
                    Model.FilterEducationalCourses = (FilterEducationalCoursesEntity)System.Web.HttpContext.Current.Session["FilterEducationalCourses"];

                Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.EDUCATIONAL_COURSES_TYPE);
                Model.EngagementBasisLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.EDUCATIONAL_COURSES_ENG);
                Model.ReleaseBasisLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.EDUCATIONAL_COURSES_REL);

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դասընթացներ"));

                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել դասընթացներ"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }
        [HttpPost]
        public ActionResult Edit(int? PrisonerID)
        {
            EducationalCoursesViewModel Model = new EducationalCoursesViewModel();

            Model.EducationalCourse = BusinessLayer_PrisonerService.GetEducationalCourse(PrisonerID: PrisonerID.Value);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դասընթացների խմբագրում (preview)", RequestData: PrisonerID.ToString()));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;
            return PartialView("Add", Model);
        }
        public ActionResult Draw_EducationalCourseTableRow(int? id = null)
        {
            // UNDONE: watch this section
            FilterEducationalCoursesEntity FilterEducationalCourses = new FilterEducationalCoursesEntity();
            if ((System.Web.HttpContext.Current.Session["FilterEducationalCourses"] as FilterEducationalCoursesEntity) != null)
                FilterEducationalCourses = (FilterEducationalCoursesEntity)System.Web.HttpContext.Current.Session["FilterEducationalCourses"];

            int page = id ?? 1;


            FilterEducationalCourses.paging = new FilterEducationalCoursesEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<EducationalCoursesDashboardEntity> ListDashboardEducationalCourses = BusinessLayer_PrisonerService.GetEducationalCoursesForDashboard(FilterEducationalCourses);

            EducationalCoursesViewModel Model = new EducationalCoursesViewModel();

            Model.ListDashboardEducationalCourses = ListDashboardEducationalCourses;
            Model.EducationalCoursesEntityPaging = FilterEducationalCourses.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դասընթացների դիտում"));

            return PartialView("EducationalCourses_Table_Row", Model);
        }
        public ActionResult Draw_EducationalCourseTableRowByFilter(FilterEducationalCoursesEntity FilterData)
        {
            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterEducationalCourses"] = FilterData;

            FilterData.paging = new FilterEducationalCoursesEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<EducationalCoursesDashboardEntity> PrisonersList = BusinessLayer_PrisonerService.GetEducationalCoursesForDashboard(FilterData);

            EducationalCoursesViewModel Model = new EducationalCoursesViewModel();

            Model.ListDashboardEducationalCourses = PrisonersList;
            Model.EducationalCoursesEntityPaging= FilterData.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դասընթացների Որոնում", RequestData: JsonConvert.SerializeObject(FilterData)));

            return PartialView("EducationalCourses_Table_Row", Model);
        }
        public ActionResult Draw_ResetEducationalCourseTableRow()
        {
            Session["FilterEducationalCourses"] = null;
            return Json(new { status = true });
        }
        [HttpPost]
        public ActionResult AddData(EducationalCoursesEntity Item)
        {
            bool status = false;
            int? id = null;
            if (Item.PrisonerID != null)
            {
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];

                id = BusinessLayer_PrisonerService.AddEducationalCourse(Item);
            }

            if (id != null) status = true;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել դասընթաց", RequestData: JsonConvert.SerializeObject(Item)));
            
            return Json(new { status = status });
        }

        public ActionResult RemData(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetEducationalCourses(new EducationalCoursesEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value )
            {
                EducationalCoursesEntity oldEntity = BusinessLayer_PrisonerService.GetEducationalCourses(new EducationalCoursesEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.EDUCATIONAL_COURSES_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateEducationalCourse(new EducationalCoursesEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել դասընթաց", RequestData: JsonConvert.SerializeObject(ID)));

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult EditData(EducationalCoursesEntity Item)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetEducationalCourses(new EducationalCoursesEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value && PrisonerID.HasValue)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.EDUCATIONAL_COURSES_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateEducationalCourse(Item);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել դասընթացը", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status, needApprove = needApprove });
        }

    }
}