﻿using BusinessLayer.BusinessServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using Newtonsoft.Json;
using System.Web.Routing;

namespace MVC___Internal_System.Controllers
{
    /// <summary>
    /// Base controller for another controllers
    /// </summary>
    public class BaseController : AsyncController
    {
        public BL_UsersAuthentication BusinesLayer_UsersAuthentication = new BL_UsersAuthentication();
        public BL_LibsLayer BusinessLayer_LibsLayer = new BL_LibsLayer();
        public BL_PoliceService BusinessLayer_PoliceService = new BL_PoliceService();
        public BL_PrisonersLayer BusinessLayer_PrisonerService = new BL_PrisonersLayer(); 
        public BL_PropsLayer BusinessLayer_PropsLayer = new BL_PropsLayer();
        public BL_PermLayer BusinessLayer_PermLayer = new BL_PermLayer();
        public BL_OrganizatonLayer BusinessLayer_OrganizatonLayer = new BL_OrganizatonLayer();
        public BL_DashboardLayer BusinessLayer_DashboardLayer = new BL_DashboardLayer();
        public BL_MedicalLayer BL_MedicalLayer = new BL_MedicalLayer();
        public BL_AmnestyLayer BusinessLayer_AmnestyLayer = new BL_AmnestyLayer();
        public BL_MergeLayer BusinessLayer_MergeLayer = new BL_MergeLayer();
        public BL_Log BL_LogLayer = new BL_Log();
        public BL_ReportLayer BL_ReportLayer = new BL_ReportLayer();
        //public static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        protected virtual PermissionsHardCodedIds PermissionHCID { get; set; }

        public BaseController()
        {
            PermissionHCID = 0;

            ViewBag.LoggedIn = false;
            ViewBag.FirstName = "Unauthorized User";
            ViewBag.FileNames = new List<string>();

            // setting system user parameters
            if ((System.Web.HttpContext.Current.Session["User"] as BEUser) != null)
            {
                BEUser User = (BEUser) System.Web.HttpContext.Current.Session["User"];
                ViewBag.UserID = User.ID;
                ViewBag.LoggedIn = true;
                ViewBag.FirstName = User.FirstName;
                ViewBag.LastName = User.LastName;
                ViewBag.LastLoginDate = User.LastLoginDate;
                ViewBag.VisibleOrgUnits = User.VisibleOrgUnits;
                ViewBag.SelectedOrgUnitID = User.SelectedOrgUnitID;
                ViewBag.Permissions = User.Permissions;                
                ViewBag.PermissionGuards = User.PermissionGuards;
                ViewBag.Position = User.Position;
                ViewBag.Avatar = User.UserIcon;
            }
        }

        [HttpPost]
        public ActionResult SelectOrgUnit(int OrgUnitID)
        {
            try
            {
                var routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(System.Web.HttpContext.Current));
                
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
                if(User.VisibleOrgUnits.Keys.Contains(OrgUnitID))
                {
                    User.SelectedOrgUnitID = OrgUnitID;
                    User.PermissionGuards = BusinessLayer_PrisonerService.GetPermissionGuard(PositionID: User.PositionOrgUnitID);

                    System.Web.HttpContext.Current.Session["User"] = User;
                    return Json(new { status = true , url = "/" + Request.UrlReferrer.Segments[1]+"Index" });
                }
                else
                {
                    return Json(new { status = false });
                }
            }
            catch(Exception e)
            {
                Console.Write(e);
                return Json(new { status = false });
            }
        }
        
        protected void GetReffererURL(string url)
        {
            //Request.ServerVariables["http_referer"];
            
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //string controller = (string)filterContext.RequestContext.RouteData.Values["controller"];
            //string action = (string)filterContext.RequestContext.RouteData.Values["action"];

            BEUser user = (BEUser)Session["User"];

            // User Authorization Check
            if (user == null)
            {
                if (Request.IsAjaxRequest())
                    filterContext.Result = new HttpUnauthorizedResult();
                else
                    filterContext.Result = RedirectToAction("", "");
                return;
            }

            //// User Permission Check
            //PermEntity permissions = user.Permissions.Find(permission => permission.ID == (int)PermissionHCID);

            //if (permissions == null && PermissionHCID != 0)
            //{
            //    filterContext.Result = new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden);
            //    return;
            //}
            //else if(permissions != null)
            //{

            //    if( (action.Contains("Add") || action.Contains("Edit")) && permissions.PPType >= 1)
            //    {
            //        filterContext.Result = new HttpStatusCodeResult(System.Net.HttpStatusCode.Forbidden);
            //        return;
            //    }
            //}
        }
        
        public ActionResult Error()
        {
            return View();
        }
        
        new public ActionResult Json(object Object)
        {
            string json = "";

            json = JsonConvert.SerializeObject(Object, Formatting.None, new Newtonsoft.Json.Converters.IsoDateTimeConverter() { DateTimeFormat = System.Globalization.CultureInfo.DefaultThreadCurrentCulture.DateTimeFormat.ShortDatePattern });

            return Content(json, "application/json");
        }
    }
}