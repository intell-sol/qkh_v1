﻿using System;
using System.Web.Mvc;
using BusinessLayer.BusinessServices;
using CommonLayer.BusinessEntities;
using System.Drawing;
using System.IO;

namespace MVC___Internal_System.Controllers
{
    //[PermissionLevel(PermissionHCID = PermissionsHardCodedIds.Medias)]
    public class MediaController : BaseController
    {
        public MediaController()
        {
            //PermissionHCID = PermissionsHardCodedIds.Medias;
        }
        public ActionResult Get(int? id)
        {
            Tuple<Byte[], string> tuple = BL_Files.getInstance().GetEmployeeFile(id);
            try {
                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ֆայլի դիտում", RequestData: id.ToString()));
                return File(tuple.Item1, tuple.Item2);
            } catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }

        }
        public ActionResult GetImage(int? id, int? width, int? height)
        {
            Tuple<Byte[], string, string> tuple = BL_Files.getInstance().GetPrisonerFile(id);
            
            try
            {
                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ֆայլի դիտում", RequestData: id.ToString()));
                try
                {
                    return File(converterImageToBytes(ScaleImage(byteArrayToImage(tuple.Item1), width, height)), tuple.Item2);
                } catch
                {

                }
                return File(tuple.Item1, tuple.Item2/*, tuple.Item3*/);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }
        public Image ScaleImage(Image image, int? maxWidth, int? maxHeight)
        {
            maxWidth = maxWidth ?? image.Width;
            maxHeight = maxHeight ?? image.Height;
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);

            using (var graphics = Graphics.FromImage(newImage))
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);

            return newImage;
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length))
                {
                    ms.Write(byteArrayIn, 0, byteArrayIn.Length);
                    return Image.FromStream(ms, true);//Exception occurs here
                }
            }
            catch { }
            return null;
        }
        public byte[] converterImageToBytes(Image x)
        {
            ImageConverter _imageConverter = new ImageConverter();
            return (byte[])_imageConverter.ConvertTo(x, typeof(byte[]));
            
        }
    }
}