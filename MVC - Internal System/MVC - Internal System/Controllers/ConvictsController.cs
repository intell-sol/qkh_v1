﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.Convicts)]
    public class ConvictsController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public ConvictsController()
        {
            PermissionHCID = PermissionsHardCodedIds.Convicts;
        }

        //public BL_PrisonersLayer BusinessLayer_PrisonerService = new BL_PrisonersLayer();

        /// <summary>
        /// Main View for convicts
        /// </summary>
        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Դատապարտյալներ";

                PrisonersViewModel Model = new PrisonersViewModel();

                Model.Gender = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);

                Model.Citizenships = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);

                Model.Nationality = BusinessLayer_LibsLayer.GetNationality();
               

                Model.FilterPrisoner = new FilterPrisonerEntity();
                if ((System.Web.HttpContext.Current.Session["FilterConvicts"] as FilterPrisonerEntity) != null)
                    Model.FilterPrisoner = (FilterPrisonerEntity)System.Web.HttpContext.Current.Session["FilterConvicts"];

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալներ"));

                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        [ReadWrite]
        /// <summary>
        /// Add View for convicts
        /// </summary>
        public ActionResult Add()
        {
            try
            {
                // page title
                ViewBag.Title = "Ավելացնել Դատապարտյալ";

                PrisonersViewModel Model = new PrisonersViewModel();

                Model.Prisoner = new PrisonerEntity();

                System.Web.HttpContext.Current.Session["ViewMode"] = 1;

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել Դատապարտյալ"));

                return View("Add", Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        [ReadWrite]
        /// <summary>
        /// EditView for convicts
        /// </summary>
        public ActionResult Edit()
        {
            try
            {
                int id = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 0;
                bool error = false;
                if (id < 1)
                    error = true;
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];

                PrisonerEntity prisoner = BusinessLayer_PrisonerService.GetPrisoner(id,true);

                if (prisoner.OrgUnitID.Value != User.SelectedOrgUnitID)
                {
                    if (User.VisibleOrgUnits.Keys.Contains(prisoner.OrgUnitID.Value))
                    {
                        User.SelectedOrgUnitID = prisoner.OrgUnitID.Value;
                    }
                }
                if (error)
                    return RedirectToAction("index");

                // check view
                if (!prisoner.ArchiveStatus.Value && MVC___Internal_System.Models.PrisonersViewModel.exists(ViewBag.Permissions, PermissionsHardCodedIds.ArchivePrisoner) && !prisoner.State.Value)
                {
                    return RedirectToAction("Preview", "Convicts", new { id = id });
                }
                // page title
                ViewBag.Title = "Խմբագրել Դատապարտյալ";
                System.Web.HttpContext.Current.Session["ViewMode"] = 1;

                PrisonersViewModel Model = new PrisonersViewModel();
               
                Model.Prisoner = prisoner;
                MergeObjectEntity mergeObject = BusinessLayer_MergeLayer.GetPrisonerActiveModifications(PrisonerID: id);
                Model.MergeStatus = (mergeObject.Merge != null && mergeObject.Merge.Any()) ? true : false;
               
                Model.ConflictPersons = BusinessLayer_PrisonerService.CheckConflictPersons(prisoner.ID.Value);
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել Դատապարտյալ", RequestData: id.ToString()));

                return View("Add", Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        public ActionResult Preview()
        {
            try
            {
                int id = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 0;
                bool error = false;
                if (id < 1)
                    error = true;
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
                PrisonerEntity prisoner = BusinessLayer_PrisonerService.GetPrisoner(id, true);
                if (prisoner.OrgUnitID.Value != User.SelectedOrgUnitID)
                    error = true;
                if (error)
                    return RedirectToAction("index");

                // page title
                ViewBag.Title = "Դիտել Դատապարտյալ";
                System.Web.HttpContext.Current.Session["ViewMode"] = 0;

                PrisonersViewModel Model = new PrisonersViewModel();

                Model.Prisoner = prisoner;
                //Model.Prisoner.Person = BusinessLayer_PrisonerService.GetPerson(Model.Prisoner.PersonID);


                if (!prisoner.ArchiveStatus.Value)
                {
                    CaseCloseEntity tempEntity = BusinessLayer_PrisonerService.GetCaseClose(new CaseCloseEntity(PrisonerID: Model.Prisoner.ID.Value)).First();

                    LibsEntity tempCurEntity = BusinessLayer_LibsLayer.GetLibByID(LibID: tempEntity.CloseTypeLibItemID.Value);

                    List<PropsEntity> curEntity = BusinessLayer_PropsLayer.GetPropsList(LibID: tempEntity.CloseTypeLibItemID.Value);


                    if (curEntity != null && curEntity.FindAll(r => r.ID == 5).Any() && curEntity.FindAll(r => r.ID == 5).Last().Name == "Ոչ")
                        Model.Prisoner.ArchiveStatus = true;

                }

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել Դատապարտյալ", RequestData: id.ToString()));

                return View("Add", Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        public ActionResult Draw_ConvictsTableRows(int? id = null)
        {

            FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();
            if ((System.Web.HttpContext.Current.Session["FilterConvicts"] as FilterPrisonerEntity) != null)
                FilterPrisoner = (FilterPrisonerEntity)System.Web.HttpContext.Current.Session["FilterConvicts"];
            
            int page = id ?? 1;
           
            FilterPrisoner.Type = 2;
            FilterPrisoner.paging = new FilterPrisonerEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PrisonerEntity> PrisonersList = BusinessLayer_PrisonerService.GetPrisonersForDashboard(FilterPrisoner);

            PrisonersViewModel Model = new PrisonersViewModel();

            Model.ListPrisoners = PrisonersList;
            Model.PrisonerEntityPaging = FilterPrisoner.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալների Դիտում", RequestData: JsonConvert.SerializeObject(FilterPrisoner)));

            return PartialView("Convict_Table_Row", Model);
        }

        [HttpPost]
        public ActionResult Draw_ConvictsTableRowsByFilter(FilterPrisonerEntity FilterPrisoner)
        {

            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterConvicts"] = FilterPrisoner;

            FilterPrisoner.paging = new FilterPrisonerEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PrisonerEntity> PrisonersList = BusinessLayer_PrisonerService.GetPrisonersForDashboard(FilterPrisoner);

            PrisonersViewModel Model = new PrisonersViewModel();

            Model.ListPrisoners = PrisonersList;
            Model.PrisonerEntityPaging = FilterPrisoner.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալներ Որոնում", RequestData: JsonConvert.SerializeObject(FilterPrisoner)));

            return PartialView("Convict_Table_Row", Model);
        }

        public ActionResult Draw_ResetConvictsTableRows()
        {
            Session["FilterConvicts"] = null;
            return Json(new { status = true });
        }
        public ActionResult GetPreview(int PrisonerID)
        {
            PreviewPrisonerViewModel Model = new PreviewPrisonerViewModel();

            Model.PrisonerID = PrisonerID;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալ Դիտում (Preview)", RequestData: PrisonerID.ToString()));

            Model.canApprove = BusinessLayer_PrisonerService.GetActiveCase(new CaseCloseEntity(PrisonerID: PrisonerID)).Any();

            return PartialView("PreviewPrisoner", Model);
        }
        [ReadWrite]
        public ActionResult ApprovePrisoner(int PrisonerID)
        {
            bool status = false;

            if (BusinessLayer_PrisonerService.GetActiveCase(new CaseCloseEntity(PrisonerID: PrisonerID)).Any())
            {
                status = BusinessLayer_PrisonerService.UpdatePrisoner(new PrisonerEntity(ID: PrisonerID, State: true));
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալ Հաստատում", RequestData: PrisonerID.ToString()));

            return Json(new { status = status });
        }

        // Search for Pirsoner
        public ActionResult SearchPrisoner(string query)
        {
            bool Status = false;

            List<PrisonerEntity> Prisoners = BusinessLayer_PrisonerService.GetPrisonersBySearch(query);

            if (Prisoners != null) Status = true;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալներ Որոնում", RequestData: query));

            return Json(new { status = Status, prisoners = Prisoners });
        }

    }
}