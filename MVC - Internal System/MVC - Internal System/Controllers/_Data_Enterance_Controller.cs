﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.AdoptionInformation)]
    public class _Data_Enterance_Controller : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public _Data_Enterance_Controller()
        {
            PermissionHCID = PermissionsHardCodedIds.AdoptionInformation;
        }
        /// <summary>
        /// Primary data add partial view
        /// </summary>
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել մուտք"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }

        [HttpPost]
        public ActionResult Edit(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;

            Model.Prisoner.AdoptionInformation = new AdoptionInformationEntity();

            Model.Prisoner.AdoptionInformation.ArrestData =  BusinessLayer_PrisonerService.GetArrestData(new ArrestDataEntity(PrisonerID: PrisonerID.Value));

            Model.Prisoner.AdoptionInformation.PrisonerTypeList =  BusinessLayer_PrisonerService.GetPrisonerTypeStatus(new PrisonerTypeStatusEntity(PrisonerID: PrisonerID.Value));

            Model.Prisoner.AdoptionInformation.PrisonAccessProtocol =  BusinessLayer_PrisonerService.GetPrisonAccessProtocol(new PrisonAccessProtocolEntity(PrisonerID: PrisonerID.Value));

            Model.Prisoner.AdoptionInformation.PrisonerPunishmentTypeList = BusinessLayer_PrisonerService.GetPrisonerPunishmentType(new PrisonerPunishmentTypeEntity(PrisonerID: PrisonerID.Value));

            Model.Prisoner.AdoptionInformation.SentencingData =  BusinessLayer_PrisonerService.GetSentencingData(new SentencingDataEntity(PrisonerID: PrisonerID.Value));

            for (int i = 0; i < Model.Prisoner.AdoptionInformation.SentencingData.Count; i++)
            {
                Model.Prisoner.AdoptionInformation.SentencingData[i].SentencingDataArticles =  BusinessLayer_PrisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(SentencingDataID: Model.Prisoner.AdoptionInformation.SentencingData[i].ID));
            }

            if (PrisonerID != null)
            {
                PrisonerEntity currentPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: PrisonerID.Value);

                Model.Prisoner.AdoptionInformation.ArrestFiles = BusinessLayer_PrisonerService.GetFiles(PersonID: currentPrisoner.PersonID.Value, TypeID: FileType.ADOPTION_INFORMATION_ARREST_RECORD);

                Model.Prisoner.AdoptionInformation.Files = BusinessLayer_PrisonerService.GetFiles(PrisonerID: PrisonerID, TypeID: FileType.ADOPTION_INFORMATION_ATTACHED_FILES);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Մուտքի խմբագրում", RequestData: PrisonerID.ToString()));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index", Model);
        }

        public ActionResult AddSentence(SentencingDataEntity Data)
        {
            bool status = false;
            int? id = null;
            bool isOpening = false;

            if (Data.PrisonerID != null)
            {
                status = true;
                
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
                Data.OrgUnitID = User.SelectedOrgUnitID;
                Data.PenaltyLibItemID = 3312; // Ազատազրկում
                id = BusinessLayer_PrisonerService.AddSentencingData(Data);
                
                if (id != null && Data.SentencingDataArticles != null)
                {
                    // open case part
                    PrisonerEntity prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Data.PrisonerID.Value);

                    if (!prisoner.ArchiveStatus.Value)
                    {
                        CaseOpenEntity openCaseEntity = new CaseOpenEntity();

                        if ((System.Web.HttpContext.Current.Session["CaseOpenEntity"] as CaseOpenEntity) != null)
                        {
                            openCaseEntity = (CaseOpenEntity)System.Web.HttpContext.Current.Session["CaseOpenEntity"];

                            openCaseEntity.StartDate = Data.SentencingStartDate;
                            openCaseEntity.EndDate = Data.SentencingEndDate;
                            openCaseEntity.PCN = prisoner.PersonalCaseNumber;
                            openCaseEntity.OrgUnitID = prisoner.OrgUnitID;

                            int? curId = BusinessLayer_PrisonerService.AddCaseOpen(openCaseEntity);

                            if (curId != null)
                            {
                                isOpening = true;
                                // clean session
                                System.Web.HttpContext.Current.Session["CaseOpenEntity"] = null;
                            }
                        }
                    }

                    //###############
                    foreach (SentencingDataArticleLibsEntity curData in Data.SentencingDataArticles)
                    {
                        curData.SentencingDataID = id;
                        int? curID = BusinessLayer_PrisonerService.AddSentencingDataArticle(curData);
                        if (curID == null) status = false;
                    }
                }
            }

            if (id == null) status = false;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել դատավճիռ", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status, isopen = isOpening });
        }
        public ActionResult EditSentence(SentencingDataEntity Data)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetSentencingData(new SentencingDataEntity(ID: Data.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Data),
                    Type: 3,
                    EntityID: Data.ID,
                    EID: (int)MergeEntity.TableIDS.ENTERANCE_SENTENCE);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                if (Data.ID != null)
                {
                    status = BusinessLayer_PrisonerService.UpdateSentencingData(Data);
                }
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատավճռի խմբագրում", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult RemSentence(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetSentencingData(new SentencingDataEntity(ID:ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                SentencingDataEntity oldEntity = BusinessLayer_PrisonerService.GetSentencingData(new SentencingDataEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.ENTERANCE_SENTENCE);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateSentencingData(new SentencingDataEntity(ID: ID, Status: false));

            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել դատավճիռը", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult AddProtocol(PrisonAccessProtocolEntity Data)
        {
            bool status = false;
            int? id = null;

            if (Data.PrisonerID != null)
            {
                status = true;

                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
                Data.OrgUnitID = User.SelectedOrgUnitID;
                id = BusinessLayer_PrisonerService.AddPrisonAccessProtocol(Data);
            }

            if (id == null) status = false;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել արձանագրություն", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status });
        }
        public ActionResult EditProtocol(PrisonAccessProtocolEntity Data)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetPrisonAccessProtocol(new PrisonAccessProtocolEntity(ID: Data.ID)).First().PrisonerID;
            if (Data.SPC == null)
            {
                Data.SPC = " ";
            }
            
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Data),
                    Type: 3,
                    EntityID: Data.ID,
                    EID: (int)MergeEntity.TableIDS.ENTERANCE_PROTOCOL);
               
                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                if (Data.ID != null)
                {
                    status = BusinessLayer_PrisonerService.UpdatePrisonAccessProtocol(Data);
                }
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել արձանագրությունը", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult RemProtocol(int ID)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetPrisonAccessProtocol(new PrisonAccessProtocolEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                PrisonAccessProtocolEntity oldEntity = BusinessLayer_PrisonerService.GetPrisonAccessProtocol(new PrisonAccessProtocolEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.ENTERANCE_PROTOCOL);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdatePrisonAccessProtocol(new PrisonAccessProtocolEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել արձանագրությունը", RequestData: ID.ToString()));

            return Json(new { status = status,needApprove = needApprove });
        }

        public ActionResult AddArrestData(ArrestDataEntity Data)
        {
            bool status = false;
            int? id = null;
            bool isOpening = false;

            if (Data.PrisonerID != null)
            {
                status = true;
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
                Data.OrgUnitID = User.SelectedOrgUnitID;
                id = BusinessLayer_PrisonerService.AddArrestData(Data);
                if (id != null && Data.SentencingDataArticles != null)
                {
                    PrisonerEntity prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Data.PrisonerID.Value);

                    if (!prisoner.ArchiveStatus.Value)
                    {
                        CaseOpenEntity openCaseEntity = new CaseOpenEntity();

                        if ((System.Web.HttpContext.Current.Session["CaseOpenEntity"] as CaseOpenEntity) != null)
                        {
                            openCaseEntity = (CaseOpenEntity)System.Web.HttpContext.Current.Session["CaseOpenEntity"];

                            openCaseEntity.StartDate = Data.DetentionStart;
                            openCaseEntity.EndDate = Data.DetentionEnd;
                            openCaseEntity.PCN = prisoner.PersonalCaseNumber;
                            openCaseEntity.OrgUnitID = prisoner.OrgUnitID;

                            int? curId = BusinessLayer_PrisonerService.AddCaseOpen(openCaseEntity);

                            if (curId != null)
                            {
                                isOpening = true;
                                // clean session
                                System.Web.HttpContext.Current.Session["CaseOpenEntity"] = null;
                            }
                        }
                    }

                    //###############
                    foreach (SentencingDataArticleLibsEntity curData in Data.SentencingDataArticles)
                    {
                        curData.ArrestDataID = id;
                        int? curID = BusinessLayer_PrisonerService.AddSentencingDataArticle(curData);
                        if (curID == null) status = false;
                    }
                }
            }

            if (id == null) status = false;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել Ձերբակալություն", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status, isopen = isOpening });
        }
        public ActionResult EditArrestData(ArrestDataEntity Data)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetArrestData(new ArrestDataEntity(ID: Data.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Data),
                    Type: 3,
                    EntityID: Data.ID,
                    EID: (int)MergeEntity.TableIDS.ENTERANCE_ARREST_DATA);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                if (Data.ID != null)
                {
                    status = BusinessLayer_PrisonerService.UpdateArrestData(Data);
                }
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Բմբագրել ձերբակալությունը", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult RemArrestData(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetArrestData(new ArrestDataEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                ArrestDataEntity oldEntity = BusinessLayer_PrisonerService.GetArrestData(new ArrestDataEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.ENTERANCE_ARREST_DATA);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateArrestData(new ArrestDataEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել ձերբակալությունը", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult AddPrisonerTypeStatus(PrisonerTypeStatusEntity entity)
        {
            bool status = true;
            int? id = null;

            entity.OrgUnitID = ViewBag.SelectedOrgUnitID;
            if (entity.PrisonerID != null && entity.PrisonerType != null)
            {
                BusinessLayer_PrisonerService.UpdatePrisoner(new PrisonerEntity(ID: entity.PrisonerID, Type: entity.PrisonerType));
            }
            id = BusinessLayer_PrisonerService.AddPrisonerTypeStatus(entity);
            if (id == null) status = false;

            return Json(new { status = status });
        }

        public ActionResult EditPrisonerTypeStatus(PrisonerTypeStatusEntity entity)
        {
            bool status = true;
            bool? tempStatus = null;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetPrisonerTypeStatus(new PrisonerTypeStatusEntity(ID: entity.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(entity),
                    Type: 3,
                    EntityID: entity.ID,
                    EID: (int)MergeEntity.TableIDS.ENTERANCE_PrisonerTypeStatus);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                tempStatus = BusinessLayer_PrisonerService.UpdatePrisonerTypeStatus(entity);
                if (tempStatus == null) status = false;
            }
            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult RemPrisonerTypeStatus(PrisonerTypeStatusEntity entity)
        {
            bool status = true;
            bool? tempStatus = null;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetPrisonerTypeStatus(new PrisonerTypeStatusEntity(ID: entity.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                PrisonerTypeStatusEntity oldEntity = BusinessLayer_PrisonerService.GetPrisonerTypeStatus(new PrisonerTypeStatusEntity(ID: entity.ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: entity.ID,
                    EID: (int)MergeEntity.TableIDS.ENTERANCE_PrisonerTypeStatus);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                entity.Status = false;
                tempStatus = BusinessLayer_PrisonerService.UpdatePrisonerTypeStatus(entity);
                if (tempStatus == null) status = false;
            }
            return Json(new { status = status, needApprove = needApprove });
        }


        public ActionResult AddPrisonerPunishmentType(PrisonerPunishmentTypeEntity entity)
        {
            bool status = true;
            int? id = null;

            entity.OrgUnitID = ViewBag.SelectedOrgUnitID;
            if (entity.PrisonerID != null)
            {
                entity.PrisonerType = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: entity.PrisonerID.Value).Type;
                id = BusinessLayer_PrisonerService.AddPrisonerPunishmentType(entity);
            }
            if (id == null) status = false;

            return Json(new { status = status });
        }

        public ActionResult EditPrisonerPunishmentType(PrisonerPunishmentTypeEntity entity)
        {
            bool status = true;
            bool needApprove = false;
            bool? tempStatus = null;
            int? PrisonerID = BusinessLayer_PrisonerService.GetPrisonerPunishmentType(new PrisonerPunishmentTypeEntity(ID: entity.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(entity),
                    Type: 3,
                    EntityID: entity.ID,
                    EID: (int)MergeEntity.TableIDS.ENTERANCE_PRISONERPUNISHMENTTYPE);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                tempStatus = BusinessLayer_PrisonerService.UpdatePrisonerPunishmentType(entity);
                if (tempStatus == null) status = false;
            }
            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult RemPrisonerPunishmentType(PrisonerPunishmentTypeEntity entity)
        {
            bool status = true;
            bool? tempStatus = null;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetPrisonerPunishmentType(new PrisonerPunishmentTypeEntity(ID: entity.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                PrisonerPunishmentTypeEntity oldEntity = BusinessLayer_PrisonerService.GetPrisonerPunishmentType(new PrisonerPunishmentTypeEntity(ID: entity.ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: entity.ID,
                    EID: (int)MergeEntity.TableIDS.ENTERANCE_PRISONERPUNISHMENTTYPE);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                entity.Status = false;
                tempStatus = BusinessLayer_PrisonerService.UpdatePrisonerPunishmentType(entity);
                if (tempStatus == null) status = false;
            }
            return Json(new { status = status, needApprove = needApprove });
        }
    }
}