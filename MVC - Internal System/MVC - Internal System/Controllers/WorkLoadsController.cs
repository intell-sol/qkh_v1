﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.WorkLoads)]
    public class WorkLoadsController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public WorkLoadsController()
        {
            PermissionHCID = PermissionsHardCodedIds.WorkLoads;
        }

        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Աշխատանք";

                WorkLoadsViewModel Model = new WorkLoadsViewModel();

                Model.FilterWorkLoads = new FilterWorkLoadsEntity();
                if ((System.Web.HttpContext.Current.Session["FilterWorkLoads"] as FilterWorkLoadsEntity) != null)
                    Model.FilterWorkLoads = (FilterWorkLoadsEntity)System.Web.HttpContext.Current.Session["FilterWorkLoads"];

                Model.WorkTitleLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.WORKLOADS_TITLE);
                Model.JobTypeLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.WORKLOADS_TYPE);
                Model.EmployerLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.WORKLOADS_EMP);
                Model.ReleaseBasisLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.WORKLOADS_REL);
                Model.EngagementBasisLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.WORKLOADS_ENG);

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Աշխատանք"));

                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել աշխատանք"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }

        [HttpPost]
        public ActionResult Edit(int? PrisonerID)
        {
            WorkLoadsViewModel Model = new WorkLoadsViewModel();
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            Model.WorkLoad = BusinessLayer_PrisonerService.GetWorkLoad(PrisonerID: PrisonerID.Value);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել աշխատանք", RequestData: PrisonerID.ToString()));

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;
            return PartialView("Add", Model);
        }

        public ActionResult Draw_WorkLoadTableRow(int? id = null)
        {
            // UNDONE: watch this section
            FilterWorkLoadsEntity FilterWorkLoads = new FilterWorkLoadsEntity();
            if ((System.Web.HttpContext.Current.Session["FilterWorkLoads"] as FilterWorkLoadsEntity) != null)
                FilterWorkLoads = (FilterWorkLoadsEntity)System.Web.HttpContext.Current.Session["FilterWorkLoads"];

            int page = id ?? 1;

            FilterWorkLoads.paging = new FilterWorkLoadsEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<WorkLoadsDashboardEntity> ListDashboardWorkLoads = BusinessLayer_PrisonerService.GetWorkLoadsForDashboard(FilterWorkLoads);

            WorkLoadsViewModel Model = new WorkLoadsViewModel();

            Model.ListDashboardWorkLoads = ListDashboardWorkLoads;
            Model.WorkLoadsEntityPaging = FilterWorkLoads.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Աշխատանքների դիտում"));

            return PartialView("WorkLoads_Table_Row", Model);
        }

        public ActionResult Draw_WorkLoadTableRowByFilter(FilterWorkLoadsEntity FilterData)
        {
            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterWorkLoads"] = FilterData;

            FilterData.paging = new FilterWorkLoadsEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<WorkLoadsDashboardEntity> PrisonersList = BusinessLayer_PrisonerService.GetWorkLoadsForDashboard(FilterData);

            WorkLoadsViewModel Model = new WorkLoadsViewModel();

            Model.ListDashboardWorkLoads = PrisonersList;
            Model.WorkLoadsEntityPaging = FilterData.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Աշխատանքի Որոնում", RequestData: JsonConvert.SerializeObject(FilterData)));
            return PartialView("WorkLoads_Table_Row", Model);
        }

        [HttpPost]
        public ActionResult AddData(WorkLoadsEntity Item)
        {
            bool status = false;
            int? id = null;
            if (Item.PrisonerID != null)
            {
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];

                id = BusinessLayer_PrisonerService.AddWorkLoad(Item);
            }

            if (id != null) status = true;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Աշխատանքի ավելացում", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = status });
        }

        public ActionResult Draw_ResetWorkLoadTableRow()
        {
            Session["FilterWorkLoads"] = null;
            return Json(new { status = true });
        }
        public ActionResult RemData(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetWorkLoads(new WorkLoadsEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                WorkLoadsEntity oldEntity = BusinessLayer_PrisonerService.GetWorkLoads(new WorkLoadsEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.WORKLOADS_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateWorkLoad(new WorkLoadsEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Աշխատանքի հեռացում", RequestData: ID.ToString()));
            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult EditData(WorkLoadsEntity Item)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetWorkLoads(new WorkLoadsEntity(ID: Item.ID)).First().PrisonerID;
            WorkLoadsEntity old = BusinessLayer_PrisonerService.GetWorkLoads(new WorkLoadsEntity(ID: Item.ID)).First();
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && PrisonerID.HasValue && !Item.Equals(old) && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.WORKLOADS_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateWorkLoad(Item);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Աշխատանքի խմբագրում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status, needApprove = needApprove });
        }
    }
}