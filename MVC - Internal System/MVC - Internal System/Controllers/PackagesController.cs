﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;
using Microsoft.Reporting.WebForms;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.Packages)]
    public class PackagesController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public PackagesController()
        {
            PermissionHCID = PermissionsHardCodedIds.Packages;
        }
        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Հանձնուքներ";

                PackagesViewModel Model = new PackagesViewModel();

                Model.FilterPackages = new FilterPackagesEntity();
                if ((System.Web.HttpContext.Current.Session["FilterPackages"] as FilterPackagesEntity) != null)
                    Model.FilterPackages = (FilterPackagesEntity)System.Web.HttpContext.Current.Session["FilterPackages"];

                Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PACHACKE_TYPE);
                Model.TypeContentLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PACKAGE_CONTENT_TYPE);
                Model.GoodsLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PACKAGE_CONTENT_GOODS);
                Model.MeasureLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PACKAGE_CONTENT_MEASURES);

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հանձնուքներ"));

                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հանձնուքի ավելացում"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }

        [HttpPost]
        public ActionResult Edit(int? PrisonerID)
        {
            PackagesViewModel Model = new PackagesViewModel();

            Model.Package = BusinessLayer_PrisonerService.GetPackage(PrisonerID: PrisonerID.Value);
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հանձնուքի խմբագրում", RequestData: PrisonerID.ToString()));

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;
            return PartialView("Add", Model);
        }

        public ActionResult Draw_ResetPackageTableRow()
        {
            Session["FilterPackages"] = null;
            return Json(new { status = true });
        }
        public ActionResult Draw_PackageTableRow(int? id = null)
        {
            // UNDONE: watch this section
            FilterPackagesEntity FilterEntity = new FilterPackagesEntity();
            FilterEntity.ApproveStatusAll = true;
            if ((System.Web.HttpContext.Current.Session["FilterPackages"] as FilterPackagesEntity) != null)
                FilterEntity = (FilterPackagesEntity)System.Web.HttpContext.Current.Session["FilterPackages"];

            int page = id ?? 1;


            FilterEntity.paging = new FilterPackagesEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PackagesDashboardEntity> ListDashboardPackages = BusinessLayer_PrisonerService.GetPackagesForDashboard(FilterEntity);

            PackagesViewModel Model = new PackagesViewModel();

            Model.ListDashboardPackages = ListDashboardPackages;
            Model.PackagesEntityPaging = FilterEntity.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հանձնուքների դիտում"));

            return PartialView("Packages_Table_Row", Model);
        }

        public ActionResult Draw_PackageTableRowByFilter(FilterPackagesEntity FilterData)
        {
            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterPackages"] = FilterData;

            FilterData.paging = new FilterPackagesEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PackagesDashboardEntity> EntityList = BusinessLayer_PrisonerService.GetPackagesForDashboard(FilterData);

            PackagesViewModel Model = new PackagesViewModel();

            Model.ListDashboardPackages = EntityList;
            Model.PackagesEntityPaging = FilterData.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հանձնուքների Որոնում", RequestData: JsonConvert.SerializeObject(FilterData)));
            return PartialView("Packages_Table_Row", Model);
        }

        public ActionResult AddData(PackagesEntity Item)
        {
            bool status = false;
            int? id = null;

            if (Item.Person != null)
            {
                Item.Person = BusinessLayer_PrisonerService.AddPersonAndGet(Person: Item.Person);

                Item.PersonID = Item.Person.ID;

                Item.ApproveStatus = null;
                Item.ApproveEmployeeID = null;

                if (Item.PersonID != null)
                {
                    PrisonerEntity Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Item.PrisonerID.Value);

                    if (Prisoner.ArchiveData != null && Prisoner.ArchiveData.Value)
                    {
                        Item.ApproveStatus = true;
                        Item.ApproveEmployeeID = ViewBag.UserID;
                    }

                    id = BusinessLayer_PrisonerService.AddPackage(Item);
                    if (id != null) status = true;
                }

                if (status && Item.ContentList != null && Item.ContentList.Any())
                {
                    foreach (var item in Item.ContentList)
                    {
                        item.PackageID = id;
                        int? curID = BusinessLayer_PrisonerService.AddPackageContent(item);
                        if (curID == null) status = false;
                    }
                }

            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հանձնուքների Ավելացում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }

        public ActionResult RemData(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetPackages(new PackagesEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                PackagesEntity oldEntity = BusinessLayer_PrisonerService.GetPackages(new PackagesEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.PACKAGES_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdatePackage(new PackagesEntity(ID: ID, Status: false));
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հանձնուքների Հեռացում", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult EditData(PackagesEntity Item)
        {
            bool status = false;
            bool needApprove = false;

            BEUser user = (BEUser)Session["User"];
            int? PrisonerID = BusinessLayer_PrisonerService.GetPackages(new PackagesEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                PackagesEntity oldEntity = BusinessLayer_PrisonerService.GetPackages(new PackagesEntity(ID: Item.ID)).First();

                Item.PrisonerID = oldEntity.PrisonerID;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.PACKAGES_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdatePackage(Item);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հանձնուքների Խմբագրում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult CompleteData(PackagesEntity entity)
        {
            bool status = false;

            if (entity.ID != null && entity.AcceptDate != null)
            {
                PackagesEntity curEntity = new PackagesEntity(ID: entity.ID.Value, AcceptDate: entity.AcceptDate, ApproveEmployeeID: ViewBag.UserID);

                status = BusinessLayer_PrisonerService.UpdatePackage(curEntity);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հանձնուքների հաստատում", RequestData: entity.ID.ToString()));

            return Json(new { status = status });
        }
        public ActionResult DeclineData(int? id)
        {
            bool status = false;

            if (id != null)
            {
                PackagesEntity curEntity = new PackagesEntity(ID: id.Value, AcceptDate: DateTime.Now, ApproveEmployeeID: ViewBag.UserID, ApproveStatus: false);

                status = BusinessLayer_PrisonerService.UpdatePackage(curEntity);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղափողման մերժում", RequestData: id.ToString()));

            return Json(new { status = status });
        }
        public ActionResult ApproveData(int? id)
        {
            bool status = false;

            if (id != null)
            {
                PackagesEntity curEntity = new PackagesEntity(ID: id.Value, AcceptDate: DateTime.Now, ApproveEmployeeID: ViewBag.UserID, ApproveStatus: true);

                status = BusinessLayer_PrisonerService.UpdatePackage(curEntity);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղափողման հաստատում", RequestData: id.ToString()));

            return Json(new { status = status });
        }
        #region PackageReport
        [HttpPost]
        public ActionResult PackageReportListData(PackageReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հանձնուք ", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
        [HttpGet]
        public ActionResult PackageReportList(string key)
        {
            PackageReportEntity Item = (PackageReportEntity)Session[key];
            SetPackageReportList(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հանձնուք(get)", RequestData: JsonConvert.SerializeObject(Item)));
            return View("PackageReportList");
        }


        private void SetPackageReportList(PackageReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;

            List<PackageReportEntity> ent = BL_ReportLayer.FillPackageReport_List(entity);
            List<PackagesContentReportEntity> packagesContent = BL_ReportLayer.FillPackagesContenReport_List(new PackagesContentReportEntity(ID: entity.ID));
            reportViewer.LocalReport.DisplayName = "Հանձնուք";
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PackageReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("PackageDS", ent));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("PackageContentDS", packagesContent));
            ViewBag.PackageReportViewer = reportViewer;
        }
        #endregion

    }
}