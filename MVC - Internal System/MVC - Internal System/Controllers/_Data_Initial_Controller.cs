﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using MVC___Internal_System.Attributes;
using BusinessLayer.BusinessServices;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.MainData)]
    public class _Data_Initial_Controller : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public _Data_Initial_Controller()
        {
            PermissionHCID = PermissionsHardCodedIds.MainData;
        }
        /// <summary>
        /// Initial Data add partial view
        /// </summary>
        public ActionResult Add()
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            PrisonerEntity Prisoner = new PrisonerEntity();
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;
            // getting gender libs
            List<LibsEntity> libs = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);
            foreach (LibsEntity lib in libs)
            {
                // calling getting lib path method from business layer
                lib.propList = BusinessLayer_PropsLayer.GetPropsList(lib.ID);
            }

            // getting nationality
            List<NationalityEntity> nationalitys = BusinessLayer_LibsLayer.GetNationality(null, null);

            Model.Gender = libs;

            Model.Prisoner = Prisoner;

            Model.Nationality = nationalitys;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել դատապարտյալ(սկզբնական)"));

            return PartialView("Index", Model);
        }
        /// <summary>
        /// Initial Data add partial view
        /// </summary>
        public ActionResult Edit(int PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            // getting gender libs
            List<LibsEntity> libs = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);
            foreach (LibsEntity lib in libs)
            {
                // calling getting lib path method from business layer
                lib.propList = BusinessLayer_PropsLayer.GetPropsList(lib.ID);
            }

            // getting nationality
            List<NationalityEntity> nationalitys = BusinessLayer_LibsLayer.GetNationality(null, null);

            // getting prisoner
            PrisonerEntity Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID);

            Prisoner.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: Prisoner.PersonID.Value);

            List<AdditionalFileEntity> files = BusinessLayer_PrisonerService.GetFiles(PersonID: Prisoner.PersonID.Value, TypeID: FileType.MAIN_DATA_AVATAR_IMAGES, PrisonerID: PrisonerID);

            files.AddRange(BusinessLayer_PrisonerService.GetFiles(TypeID: FileType.MAIN_DATA_ATTACHED_IMAGES, PrisonerID: PrisonerID));

            if (files.Any())
            {

                Prisoner.Person.PhotoLink = BL_Files.getInstance().GetLink(files.First().ID, files.First().FileName);

                foreach (var item in files)
                {
                    Model.Images.Add(BL_Files.getInstance().GetLink(item.ID, item.FileName));
                }
            }

            if (!Model.Images.Any())
            {
                Model.Images.Add(BL_Files.getInstance().getDefaultPhotoLink());
            }

            IdentificationDocument curEntity = new IdentificationDocument();
            curEntity.PersonID = Prisoner.PersonID;

            List<IdentificationDocument> docIdentities = BusinessLayer_PrisonerService.GetIdentificationDocuments(curEntity);

            Model.Prisoner = Prisoner;

            Model.Prisoner.Person.IdentificationDocuments = docIdentities;

            Model.Gender = libs;

            Model.Nationality = nationalitys;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել դատապարտյալ(սկզբնական)", RequestData: PrisonerID.ToString()));

            return PartialView("Index", Model);
        }

        [HttpPost]
        //[JsonFormDataFilter(Parameter = "DocIdentities", JsonDataType = typeof(List<IdentificationDocument>))]
        [JsonFormDataFilter(Parameter = "Person", JsonDataType = typeof(PersonEntity))]
        public ActionResult AddData(int PrisonerType, PersonEntity Person, bool isCustomPhoto = false, bool ArchiveData = true)
        {
            //PersonEntity MenualDataEntity = null;
            //if (MenualData != null)
            //{
            //    MenualDataEntity = JsonConvert.DeserializeObject<PersonEntity>(MenualData);
            //}

            bool status = false;
            int? ConvictID = null;

            //AVVPerson person = null;
            int? fileID = null;

            //if (MenualDataEntity == null && DocNumber != null)
            //{
            //    if (Type)
            //        person = BusinessLayer_PoliceService.getBYDocumentNumber(DocNumber);
            //    else
            //        person = BusinessLayer_PoliceService.getBYPersonalNumber(DocNumber);
            //}
            //else
            //{
            //    PersonEntity tempPerson = BusinessLayer_PrisonerService.GetPerson(PersonalID: MenualDataEntity.Personal_ID);

            //    if (tempPerson != null && tempPerson.ID != null)
            //    {
            //        checkPersonalID = false;
            //    }
            //}

            //if ( != null && checkPersonalID)
            //{
                Person.Personal_ID = (Person.Personal_ID != null) ? Person.Personal_ID : "8" + Person.FirstName.GetHashCode().ToString();

                // adding employee
                PrisonerEntity curPrisoner = AddPrisoner(PrisonerType: PrisonerType, CurPerson: Person, ArchiveData: ArchiveData);

                ConvictID = curPrisoner.ID;

                if (ConvictID != null) status = true;

                // upload photo
                if (ConvictID != null)
                {
                    // adding file
                    if (Request.Files.Count > 0 && curPrisoner.PersonID != null)
                    {
                        // here
                        //BusinessLayer_PrisonerService.ChangeFileType

                        HttpPostedFileBase file = Request.Files[0];
                        AdditionalFileEntity fileEntry = new AdditionalFileEntity(file.ContentLength);

                        fileEntry.FileName = Path.GetFileName(file.FileName);

                        fileEntry.FileExtension = Path.GetExtension(file.FileName);
                        fileEntry.ContentType = file.ContentType;
                        fileEntry.PersonID = (int)curPrisoner.PersonID;
                        fileEntry.isPrisoner = true;
                        fileEntry.PrisonerID = ConvictID;
                        fileEntry.PNum = Person.Personal_ID;
                        fileEntry.TypeID = FileType.MAIN_DATA_AVATAR_IMAGES;
                        file.InputStream.Read(fileEntry.FileContent, 0, fileEntry.FileContent.Length);

                        fileID = BL_Files.getInstance().AddFile(fileEntry);
                    }
                }
            //}

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել դատապարտյալ(սկզբնական)", RequestData: JsonConvert.SerializeObject(new { PrisonerType = PrisonerType, isCustomPhoto = isCustomPhoto, Person = Person })));

            return Json(new { status = status ? true : false, PrisonerID = ConvictID, ArchiveData = ArchiveData });
        }

        [HttpPost]
        [ReadWrite]
        public ActionResult remDocIdentity(IdentificationDocument entity)
        {
            bool status = false;
            bool needApprove = false;

            int? DocID = entity.ID;

            if (DocID != null)
            {
                if (ViewBag.PPType == 2)
                {
                    MergeEntity curModEntity = new MergeEntity(PrisonerID: BusinessLayer_PrisonerService.GetIdentificationDocuments(new IdentificationDocument(ID: DocID)).First().PrisonerID.Value,
                        EmployeeID: ViewBag.UserID,
                        Json: JsonConvert.SerializeObject(new PreviousConvictionsEntity(ID: DocID, Status: false)),
                        EntityID: DocID,
                        Type: 2,
                        EID: (int)MergeEntity.TableIDS.INITIAL_DOC_IDENTITY);

                    int? tempID = BusinessLayer_MergeLayer.AddModification(curModEntity);
                    status = tempID != null;
                    needApprove = true;
                }
                else
                {
                    // remove document identity
                    IdentificationDocument curEntity = new IdentificationDocument();
                    curEntity.ID = DocID;
                    curEntity.Status = false;

                    status = BusinessLayer_PrisonerService.UpdateIdentificationDocuments(curEntity);
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել անձը հաստատող փաստաթուղթը", RequestData: DocID.ToString()));

            return Json(new {
                status = status,
                needApprove = needApprove
            });
        }

        [HttpPost]
        [ReadWrite]
        public ActionResult addDocIdentity(IdentificationDocument docEntity)
        {
            bool status = false;
            int? DocID = null;
            bool needApprove = false;

            if (docEntity != null)
            {
                //if (ViewBag.PPType == 2 && docEntity.PrisonerID != null)
                //{
                //    MergeEntity curEntity = new MergeEntity(PrisonerID: docEntity.PrisonerID.Value,
                //        EmployeeID: ViewBag.UserID,
                //        Json: JsonConvert.SerializeObject(docEntity),
                //        Type: 1,
                //        EID: (int)MergeEntity.TableIDS.INITIAL_DOC_IDENTITY);

                //    int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                //    status = tempID != null;
                //    needApprove = true;
                //}
                //else
                //{
                    // adding doc identity
                    DocID = BusinessLayer_PrisonerService.AddIdentificationDocuments(docEntity);
                    if (DocID != null) status = true;
                //}
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել անձը հաստատող փաստաթուղթը", RequestData: JsonConvert.SerializeObject(docEntity)));

            return Json(new
            {
                status = status,
                id = DocID,
                needApprove = needApprove
            });
        }

        /*[HttpPost]
        public ActionResult GetFullData(int PrisonerID)
        {
#pragma warning disable CS0219 // The variable 'status' is assigned but its value is never used
            var status = true;
#pragma warning restore CS0219 // The variable 'status' is assigned but its value is never used

            PrisonerEntity prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID);

            IdentificationDocument curEntity = new IdentificationDocument();
            curEntity.PrisonerID = PrisonerID;

            List<IdentificationDocument> docIdentities = BusinessLayer_PrisonerService.GetIdentificationDocuments(curEntity);


            // here file part

            //string PhotoLink = BusinessLayer_Files.GetLink(entity.Photo);

            return Json(new
            {
                status = "success",

                FirstName = prisoner.FirstName,
                MiddleName = prisoner.MiddleName,
                LastName = prisoner.LastName,
                BirthDate = prisoner.Birthday,

                SocialCardNumber = prisoner.Personal_ID,
                PassportNumber = "",
                Nationality = prisoner.NationalityID,
                NationalityLabel = prisoner.NationalityID,
                DocIdentities = docIdentities,


                Gender = prisoner.SexLibItem_ID,
                LivingAddress = prisoner.Living_place,
                RegistrationAddress = prisoner.Registration_address,
                PhotoLink = "Somelink"
            });
        } */

        private PrisonerEntity AddPrisoner(int PrisonerType, PersonEntity CurPerson, bool ArchiveData = true)
        {
            // prisoner entity
            PrisonerEntity prisoner = new PrisonerEntity();

            Random r = new Random();
            int personalNum = r.Next(0, 4000000); //for ints

            prisoner.PersonalCaseNumber = personalNum;
            prisoner.ArchiveData = ArchiveData;
            prisoner.Type = PrisonerType;
            prisoner.State = false;
            PersonEntity Person = BusinessLayer_PrisonerService.AddPersonAndGet(Person: CurPerson);
            Person.Photo_ID = CurPerson.Photo_ID;
            Person.PhotoLink = CurPerson.PhotoLink;
            prisoner.Person = Person;
            prisoner.PersonID = Person.ID;

            //if (person != null && person.PNum != null)
            //{
            //    Document idCard = person.AVVDocuments.Document.Find(d => d.Document_Type == Document_Type.ID_CARD);
            //    Document biometricPassport = person.AVVDocuments.Document.Find(d => d.Document_Type == Document_Type.BIOMETRIC_PASSPORT && d.Document_Status == Document_Status.ACTIVE);
            //    Document nonBiometricPassport = person.AVVDocuments.Document.Find(d => d.Document_Type == Document_Type.NON_BIOMETRIC_PASSPORT && d.Document_Status == Document_Status.ACTIVE);

            //    Document document = idCard == null ? nonBiometricPassport : idCard;
            //    document = document ?? person.AVVDocuments.Document.First();

            //    String firstName = document.Person.First_Name;
            //    String middleName = document.Person.Patronymic_Name;
            //    String lastName = document.Person.Last_Name;

            //    AVVAddress PermanentAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Code == "Ն");
            //    PermanentAddress = PermanentAddress ?? person.AVVAddresses.AVVAddress.First();
            //    //AVVAddress PermanentAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.P);
            //    AVVAddress ActiveAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.A);
            //    AVVAddress TemporaryAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.T);

            //    String Nationality = document.Person.NationalityLabel;
            //    String PassportDepartment = document.Document_Department;
            //    String PassportIssuanceDate = document.PassportData.Passport_Issuance_Date;

            //    RegistrationAddress liv = ActiveAddress != null ? ActiveAddress.RegistrationAddress : PermanentAddress.RegistrationAddress;
            //    RegistrationAddress reg = PermanentAddress.RegistrationAddress;

            //    //String LivingAddress = liv.Registration_Region + ", " + liv.Registration_Community + " " + (liv.Registration_Apartment != null ? liv.Registration_Apartment : "") + ", " + liv.Registration_Street + " " + liv.Registration_Building_Type + " " + liv.Registration_Building;

            //    //String RegistrationAddress = reg.Registration_Region + ", " + reg.Registration_Community + " " + (reg.Registration_Apartment != null ? reg.Registration_Apartment : "") + ", " + reg.Registration_Street + " " + reg.Registration_Building_Type + " " + reg.Registration_Building;


            //    // gender part
            //    // getting gender libs

            //    List<LibsEntity> genderLibs = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);
            //    foreach (LibsEntity lib in genderLibs)
            //    {
            //        // calling getting lib path method from business layer
            //        lib.propList = BusinessLayer_PropsLayer.GetPropsList(lib.ID);
            //    }

            //    int? GenderLibID = null;

            //    foreach (LibsEntity curGender in genderLibs)
            //    {
            //        PropsEntity idCodeProp = curGender.propList.Find(w => w.ID == (int)PropsEntity.PropToLibID.IDENTIFICATION_CODE);
            //        string value = idCodeProp.Values[0].Value;
            //        if (value == document.Person.Genus)
            //        {
            //            GenderLibID = curGender.ID;
            //            break;
            //        }
            //    }

            //    // nationality part
            //    List<NationalityEntity> nationality = BusinessLayer_LibsLayer.GetNationality(Code: int.Parse(document.Person.Nationality));

            //    //prisoner.Personal_ID = person.PNum;
            //    //prisoner.FirstName = firstName;
            //    //prisoner.MiddleName = middleName;
            //    //prisoner.LastName = lastName;
            //    //prisoner.Birthday = DateTime.ParseExact(document.Person.Birth_Date, "dd/mm/yyyy", null);
            //    //prisoner.SexLibItem_ID = Convert.ToInt32(GenderLibID.Value);
            //    //prisoner.NationalityID = Convert.ToInt32(nationality[0].ID.Value);
            //    //prisoner.Registration_address = RegistrationAddress;
            //    //prisoner.Living_place = LivingAddress;
            //    //prisoner.IdentificationDocuments = idDocuments;

            //    prisoner.Person = new PersonEntity(Personal_ID: person.PNum, FirstName: firstName, MiddleName: middleName, LastName: lastName, Birthday: DateTime.ParseExact(document.Person.Birth_Date, "dd/MM/yyyy", null),
            //        SexLibItem_ID: Convert.ToInt32(GenderLibID.Value), NationalityID: Convert.ToInt32(nationality[0].ID.Value));
            //    prisoner.Person.Registration_Entity = reg;
            //    prisoner.Person.Living_Entity = liv;
            //}
            //else
            //{
            //    prisoner.Person = MenualData;
            //}

            prisoner.Person.IdentificationDocuments = Person.IdentificationDocuments;

            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.AddPrisoner(prisoner);

            if ((Person != null && Person.Photo_ID != null) && curPrisoner.ID != null)
            {
                String PhotoLink = Person.Photo_ID;

                Byte[] bytes = Convert.FromBase64String(PhotoLink);

                AdditionalFileEntity fileEntry = new AdditionalFileEntity(bytes.Length);

                fileEntry.FileName = Person.Personal_ID.ToString() + ".jpg";
                fileEntry.FileExtension = ".jpg";
                fileEntry.ContentType = "image/jpeg";
                fileEntry.isPrisoner = true;
                fileEntry.PersonID = curPrisoner.PersonID;
                fileEntry.PrisonerID = curPrisoner.ID;
                fileEntry.PNum = Person.Personal_ID;
                fileEntry.TypeID = FileType.MAIN_DATA_AVATAR_IMAGES;
                fileEntry.FileContent = bytes;

                int? fileID = BL_Files.getInstance().AddFile(fileEntry);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել դատապարտյալ (սկզբնական)", RequestData: JsonConvert.SerializeObject( new { person = Person, PrisonerType = PrisonerType})));

            return curPrisoner;
        }

        public ActionResult removePerson(int ID)
        {
            bool status = false;

            status = BusinessLayer_PrisonerService.UpdatePrisoner(new PrisonerEntity(ID: ID, State: false, Status: false));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել դատապարտյալ (սկզբնական)", RequestData: ID.ToString()));

            return Json(new { status = status });
        }
    }
}