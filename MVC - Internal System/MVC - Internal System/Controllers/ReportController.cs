﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using CommonLayer.BusinessEntities;
using MVC___Internal_System.Models;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.Reports)]
    public class ReportController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public ReportController()
        {
            PermissionHCID = PermissionsHardCodedIds.Reports;
        }
        // GET: Report
        public ActionResult Index()
        {
            ViewBag.Title = "Հաշվետվություններ";
            return View();
        }

        #region EarlyCloseReport
        [HttpPost]
        public ActionResult EarlyReportListData(EarlyReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Վաղաժամկետ ազատված դատապարտյալներ", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
        [HttpGet]
        public ActionResult EarlyReportList(string key)
        {
            EarlyReportEntity Item = (EarlyReportEntity)Session[key];
            SetEarlyReportList(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Վաղաժամկետ ազատված դատապարտյալներ(get)", RequestData: JsonConvert.SerializeObject(Item)));
            return View("EarlyReportList");
        }
        [HttpPost]
        public ActionResult EarlyReportFilter()
        {
            ViewBag.Title = "Վաղաժամկետ ազատված դատապարտյալներ";

            EarlyReportViewModel Model = new EarlyReportViewModel();
            Model.Filter = new EarlyReportEntity();
            if ((System.Web.HttpContext.Current.Session["FilterEarlyReports"] as EarlyReportEntity) != null)
                Model.Filter = (EarlyReportEntity)System.Web.HttpContext.Current.Session["FilterEarlyReports"];

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Վաղաժամկետ ազատված դատապարտյալներ(filter)"));
            return PartialView("EarlyReportFilter", Model);
        }
        private void SetEarlyReportList(EarlyReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;

            List<EarlyReportEntity> ent = BL_ReportLayer.EarlyReport_List(entity);

            reportViewer.LocalReport.DisplayName = "Վաղաժամկետ ազատված դատապարտյալներ ";
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\EarlyCloseReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("EarlyCloseDS", ent));
            ViewBag.EarlyReportViewer = reportViewer;
        }
        #endregion

        #region OpenTypeReport
        [HttpPost]
        public ActionResult OpenTypeReportListData(OpenTypeReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալներ ըստ տիպի ", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
        [HttpGet]
        public ActionResult OpenTypeReportList(string key)
        {
            OpenTypeReportEntity Item = (OpenTypeReportEntity)Session[key];
            SetOpenTypeReportList(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալներ ըստ տիպի(get)", RequestData: JsonConvert.SerializeObject(Item)));
            return View("OpenTypeReportList");
        }
        [HttpPost]
        public ActionResult OpenTypeReportFilter()
        {
            ViewBag.Title = "Դատապարտյալներ ըստ տիպի";

            OpenTypeReportViewModel Model = new OpenTypeReportViewModel();
            Model.Filter = new OpenTypeReportEntity();
            if ((System.Web.HttpContext.Current.Session["FilterConvicts"] as OpenTypeReportEntity) != null)
                Model.Filter = (OpenTypeReportEntity)System.Web.HttpContext.Current.Session["FilterOpenTypeReports"];
            Model.QKHType = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.QKH_TYPE);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալներ ըստ տիպի(filter)"));
            return PartialView("OpenTypeReportFilter", Model);
        }
        private void SetOpenTypeReportList(OpenTypeReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;

            List<OpenTypeReportEntity> ent = BL_ReportLayer.FillOpenTypeReport_List(entity);

            reportViewer.LocalReport.DisplayName = "Դատապարտյալներ ըստ տիպի ";
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\OpenTypeReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("OpenTypeDS", ent));
            ViewBag.OpenTypeReportViewer = reportViewer;
        }
        #endregion

        #region PersonalDescriptionReport
        public ActionResult Draw_ResetPersonalDescriptionReportTableRows()
        {
            Session["FilterPsDReport"] = null;
            return Json(new { status = true });
        }
        [HttpPost]
        public ActionResult PersonalDescriptionReportListData(PersonalDescriptionReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալի բնութագիր(report PersonalDescription)", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
        [HttpGet]
        public ActionResult PersonalDescriptionReportList(string key)
        {
            PersonalDescriptionReportEntity Item = (PersonalDescriptionReportEntity)Session[key];
            SetPersonalDescription(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալի բնութագիր(get)", RequestData: JsonConvert.SerializeObject(Item)));
            return View("PersonalDescriptionReportList");
        }
        [HttpPost]
        public ActionResult PersonalDescriptionReportFilter()
        {
            //PersonalDescriptionReportViewModel Model = new PersonalDescriptionReportViewModel();
            //Model.Filter = new PersonalDescriptionReportEntity();
            //if ((System.Web.HttpContext.Current.Session["FilterConvicts"] as PersonalDescriptionReportEntity) != null)
            //    Model.Filter = (PersonalDescriptionReportEntity)System.Web.HttpContext.Current.Session["FilterPersonalDescriptionReports"];
            //BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալի բնութագիր"));
            try
            {
                // page title

                ViewBag.Title = "Դատապարտյալի բնութագիր";
                PrisonersViewModel Model = new PrisonersViewModel();

                Model.Gender = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);

                Model.Citizenships = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);

                Model.Nationality = BusinessLayer_LibsLayer.GetNationality();
                // Model.FilterPrisoner.Type = 2;
                Model.FilterPrisoner = new FilterPrisonerEntity();
                if ((System.Web.HttpContext.Current.Session["FilterPsDReport"] as FilterPrisonerEntity) != null)
                    Model.FilterPrisoner = (FilterPrisonerEntity)System.Web.HttpContext.Current.Session["FilterPsDReport"];

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալի բնութագիր(filter)"));

                return PartialView("PersonalDescriptionReportFilter", Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }
        private void SetPersonalDescription(PersonalDescriptionReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;

            List<PersonalDescriptionReportEntity> item = BL_ReportLayer.FillPersonalDescriptionReport_List(entity);

            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PersonalDescriptionReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("PersonalDescriptionDS", item));
            ViewBag.PersonalDescriptionViewer = reportViewer;
        }
        public ActionResult Draw_PersonalDescriptionReportTableRows()
        {

            FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            if ((System.Web.HttpContext.Current.Session["FilterPsDReport"] as FilterPrisonerEntity) != null)
                FilterPrisoner = (FilterPrisonerEntity)System.Web.HttpContext.Current.Session["FilterPsDReport"];

            if (FilterPrisoner.Type == null) FilterPrisoner.Type = 2;
            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;
            FilterPrisoner.paging = new FilterPrisonerEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PrisonerEntity> PrisonersList = BusinessLayer_PrisonerService.GetPrisonersForDashboard(FilterPrisoner);

            PrisonersViewModel Model = new PrisonersViewModel();

            Model.ListPrisoners = PrisonersList;
            Model.PrisonerEntityPaging = FilterPrisoner.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալի բնութագիր(table row)", RequestData: JsonConvert.SerializeObject(FilterPrisoner)));

            return PartialView("PersonalDescriptionReport_Table_Row", Model);

        }
        [HttpPost]
        public ActionResult Draw_PersonalDescriptionReportTableRowsByFilter(FilterPrisonerEntity FilterPrisoner)
        {

            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterPsDReport"] = FilterPrisoner;

            FilterPrisoner.paging = new FilterPrisonerEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PrisonerEntity> PrisonersList = BusinessLayer_PrisonerService.GetPrisonersForDashboard(FilterPrisoner);

            PrisonersViewModel Model = new PrisonersViewModel();

            Model.ListPrisoners = PrisonersList;
            Model.PrisonerEntityPaging = FilterPrisoner.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալի բնութագիր(row by filter)", RequestData: JsonConvert.SerializeObject(FilterPrisoner)));

            return PartialView("PersonalDescriptionReport_Table_Row", Model);
        }
        #endregion

        #region CustomReport
        public ActionResult Draw_CustomReportTableRows()
        {

            FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            if ((System.Web.HttpContext.Current.Session["FilterCustomReport"] as FilterPrisonerEntity) != null)
                FilterPrisoner = (FilterPrisonerEntity)System.Web.HttpContext.Current.Session["FilterCustomReport"];

            if (FilterPrisoner.Type == null) FilterPrisoner.Type = 2;
            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;
            FilterPrisoner.paging = new FilterPrisonerEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PrisonerEntity> PrisonersList = BusinessLayer_PrisonerService.GetPrisonersForDashboard(FilterPrisoner);

            CustomReportViewModel Model = new CustomReportViewModel();

            Model.Prisoner.ListPrisoners = PrisonersList;
            Model.Prisoner.PrisonerEntityPaging = FilterPrisoner.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ընդհանուր Հաշվետվություն(table row)", RequestData: JsonConvert.SerializeObject(FilterPrisoner)));

            return PartialView("CustomReport_Table_Row", Model);

        }
        [HttpPost]
        public ActionResult Draw_ResetCustomReportTableRows()
        {
            Session["FilterCustomReport"] = null;
            return Json(new { status = true });
        }
        [HttpPost]
        public ActionResult CustomReportListData(CustomReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ընդհանուր Հաշվետվություն(report Custom)", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
       
        [HttpGet]
        public ActionResult CustomReportList(string key)
        {
            CustomReportEntity Item = (CustomReportEntity)Session[key];
            SetCustomReport(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ընդհանուր Հաշվետվություն(get)", RequestData: JsonConvert.SerializeObject(Item)));
            return View("CustomReportList");
        }
        [HttpPost]
        public ActionResult CustomReportFilter()
        {
            //PersonalDescriptionReportViewModel Model = new PersonalDescriptionReportViewModel();
            //Model.Filter = new PersonalDescriptionReportEntity();
            //if ((System.Web.HttpContext.Current.Session["FilterConvicts"] as PersonalDescriptionReportEntity) != null)
            //    Model.Filter = (PersonalDescriptionReportEntity)System.Web.HttpContext.Current.Session["FilterPersonalDescriptionReports"];
            //BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալի բնութագիր"));
            try
            {
                // page title

                ViewBag.Title = "Ընդհանուր Հաշվետվություն";
                //PrisonersViewModel Model = new PrisonersViewModel();
                CustomReportViewModel Model = new CustomReportViewModel();
                Model.Prisoner.Gender = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);

                Model.Prisoner.Citizenships = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);

                Model.Prisoner.Nationality = BusinessLayer_LibsLayer.GetNationality();
                // Model.FilterPrisoner.Type = 2;
                Model.Prisoner.FilterPrisoner = new FilterPrisonerEntity();
                if ((System.Web.HttpContext.Current.Session["FilterCustomDReport"] as FilterPrisonerEntity) != null)
                    Model.Prisoner.FilterPrisoner = (FilterPrisonerEntity)System.Web.HttpContext.Current.Session["FilterCustomDReport"];

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ընդհանուր Հաշվետվություն(filter)"));

                return PartialView("CustomReportFilter", Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        private void SetCustomReport(CustomReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;

            List<CustomReportEntity> item = BL_ReportLayer.FillCustomReport_List(entity);

            ReportParameter[] customParameters = new ReportParameter[43];
            customParameters[0] = new ReportParameter("chPrisonerName", entity.chPrisonerName.ToString());
            customParameters[1] = new ReportParameter("chPersonalNumber", entity.chPersonalNumber.ToString());
            customParameters[2] = new ReportParameter("chPersonalCaseNumber", entity.chPersonalCaseNumber.ToString());
            customParameters[3] = new ReportParameter("chBirthDay", entity.chBirthDay.ToString());
            customParameters[4] = new ReportParameter("chSexType", entity.chSexType.ToString());
            customParameters[5] = new ReportParameter("chNationality", entity.chNationality.ToString());
            customParameters[6] = new ReportParameter("chCitizenship", entity.chCitizenship.ToString());
            customParameters[7] = new ReportParameter("chBloodType", entity.chBloodType.ToString());
            customParameters[8] = new ReportParameter("chInvalidClass", entity.chInvalidClass.ToString());
            customParameters[9] = new ReportParameter("chCameraCardNumber", entity.chCameraCardNumber.ToString());
            customParameters[10] = new ReportParameter("chArrestStartDate", entity.chArrestStartDate.ToString());
            customParameters[11] = new ReportParameter("chSentencingStartDate", entity.chSentencingStartDate.ToString());
            customParameters[12] = new ReportParameter("chPrisonAccessDate", entity.chPrisonAccessDate.ToString());
            customParameters[13] = new ReportParameter("chTransfersCount", entity.chTransfersCount.ToString());
            customParameters[14] = new ReportParameter("chOfficialVisitsCount", entity.chOfficialVisitsCount.ToString());
            customParameters[15] = new ReportParameter("chPersonalVisitsCount", entity.chPersonalVisitsCount.ToString());
            customParameters[16] = new ReportParameter("chPackagesCount", entity.chPackagesCount.ToString());
            customParameters[17] = new ReportParameter("chParents", entity.chParents.ToString());
            customParameters[18] = new ReportParameter("chSpouses", entity.chSpouses.ToString());
            customParameters[19] = new ReportParameter("chChildren", entity.chChildren.ToString());
            customParameters[20] = new ReportParameter("chSisterBrother", entity.chSisterBrother.ToString());
            customParameters[21] = new ReportParameter("chEducationName", entity.chEducationName.ToString());
            customParameters[22] = new ReportParameter("chProfessionName", entity.chProfessionName.ToString());
            customParameters[23] = new ReportParameter("chWorkName", entity.chWorkName.ToString());
            customParameters[24] = new ReportParameter("chLeaningNames", entity.chLeaningNames.ToString());
            customParameters[25] = new ReportParameter("chBloodType", entity.chBloodType.ToString());
            customParameters[26] = new ReportParameter("chInvalidClass", entity.chInvalidClass.ToString());
            customParameters[27] = new ReportParameter("chCameraCardNumber", entity.chCameraCardNumber.ToString());
            customParameters[28] = new ReportParameter("chArrestStartDate", entity.chArrestStartDate.ToString());
            customParameters[29] = new ReportParameter("chSentencingStartDate", entity.chSentencingStartDate.ToString());
            customParameters[30] = new ReportParameter("chPrisonAccessDate", entity.chPrisonAccessDate.ToString());
            customParameters[31] = new ReportParameter("chTransfersCount", entity.chTransfersCount.ToString());
            customParameters[32] = new ReportParameter("chOfficialVisitsCount", entity.chOfficialVisitsCount.ToString());
            customParameters[33] = new ReportParameter("chPersonalVisitsCount", entity.chPersonalVisitsCount.ToString());
            customParameters[34] = new ReportParameter("chPackagesCount", entity.chPackagesCount.ToString());
            customParameters[35] = new ReportParameter("chPenaltyName", entity.chPenaltyName.ToString());
            customParameters[36] = new ReportParameter("chEncouragementsName", entity.chEncouragementsName.ToString());
            customParameters[37] = new ReportParameter("chConflictsPersons", entity.chConflictsPersons.ToString());
            customParameters[38] = new ReportParameter("chInjunctionsName", entity.chInjunctionsName.ToString());
            customParameters[39] = new ReportParameter("chPresentArticle", entity.chPresentArticle.ToString());
            customParameters[40] = new ReportParameter("chPreviousConviction", entity.chPreviousConviction.ToString());
            customParameters[41] = new ReportParameter("chSentenceArticleType", entity.chSentenceArticleType.ToString());
            customParameters[42] = new ReportParameter("chPunishmentType", entity.chPunishmentType.ToString());


            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\CustomReport.rdlc";
            reportViewer.LocalReport.SetParameters(customParameters );
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("CustomDS", item));
            ViewBag.CustomViewer = reportViewer;
        }
        [HttpPost]
        public ActionResult Draw_CustomReportTableRowsByFilter(FilterPrisonerEntity FilterPrisoner)
        {

            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterCustomReport"] = FilterPrisoner;

            FilterPrisoner.paging = new FilterPrisonerEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PrisonerEntity> PrisonersList = BusinessLayer_PrisonerService.GetPrisonersForDashboard(FilterPrisoner);

            PrisonersViewModel Model = new PrisonersViewModel();

            Model.ListPrisoners = PrisonersList;
            Model.PrisonerEntityPaging = FilterPrisoner.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ընդհանուր Հաշվետվություն(row by filter)", RequestData: JsonConvert.SerializeObject(FilterPrisoner)));

            return PartialView("CustomReport_Table_Row", Model);
        }

        #endregion

        #region CaseClosed
        [HttpPost]
        public ActionResult CaseClosedReportListData(CaseClosedReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Կալանավորումը փոփոխվել կամ վերացվել է(post)", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
        [HttpGet]
        public ActionResult CaseClosedReportList(string key)
        {
            CaseClosedReportEntity Item = (CaseClosedReportEntity)Session[key];
            SetCaseClosed(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Կալանավորումը փոփոխվել կամ վերացվել է(get)", RequestData: key));
            return View("CaseClosedReportList");
        }
        [HttpPost]
        public ActionResult CaseClosedReportFilter()
        {
            ViewBag.Title = "Կալանավորումը փոփոխվել կամ վերացվել է";

            CaseClosedReportViewModel Model = new CaseClosedReportViewModel();
            Model.Filter = new CaseClosedReportEntity();
            if ((System.Web.HttpContext.Current.Session["FilterCaseClosedReports"] as CaseClosedReportEntity) != null)
                Model.Filter = (CaseClosedReportEntity)System.Web.HttpContext.Current.Session["FilterCaseClosedReports"];

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Կալանավորումը փոփոխվել կամ վերացվել է(filter)"));
            return PartialView("CaseClosedReportFilter", Model);
        }
        private void SetCaseClosed(CaseClosedReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;

            List<CaseClosedReportEntity> item = BL_ReportLayer.FillCaseClosedReport_List(entity);
            reportViewer.LocalReport.DisplayName = "Կալանավորումը փոփոխվել կամ վերացվել է ";
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\CaseClosedReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("CaseClosedDS", item));
            ViewBag.CaseClosedReportViewer = reportViewer;
        }
        #endregion

        #region PrisonerReference
        [HttpPost]
        public ActionResult PrisonerReferenceReportListData(PrisonerReferenceReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղեկանք դատապարտյալի անձնական գործից(report PrisonerReference post)", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
        [HttpGet]
        public ActionResult PrisonerReferenceReportList(string key)
        {
            PrisonerReferenceReportEntity Item = (PrisonerReferenceReportEntity)Session[key];
            SetPrisonerReference(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղեկանք դատապարտյալի անձնական գործից(get)", RequestData: JsonConvert.SerializeObject(Item)));
            return View("PrisonerReferenceReportList");
        }
        public ActionResult PrisonerReferenceReportFilter()
        {

            try
            {
                // page title

                ViewBag.Title = "Տեղեկանք դատապարտյալի անձնական գործից";
                PrisonersViewModel Model = new PrisonersViewModel();

                Model.Gender = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);

                Model.Citizenships = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);

                Model.Nationality = BusinessLayer_LibsLayer.GetNationality();
                // Model.FilterPrisoner.Type = 2;
                Model.FilterPrisoner = new FilterPrisonerEntity();
                if ((System.Web.HttpContext.Current.Session["FilterPsRefReport"] as FilterPrisonerEntity) != null)
                    Model.FilterPrisoner = (FilterPrisonerEntity)System.Web.HttpContext.Current.Session["FilterPsRefReport"];

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղեկանք դատապարտյալի անձնական գործից(filter)"));

                return PartialView("PrisonerReferenceReportFilter", Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }
        private void SetPrisonerReference(PrisonerReferenceReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;

            List<PrisonerReferenceReportEntity> item = BL_ReportLayer.FillPrisonerReferenceReport_List(entity);

            reportViewer.LocalReport.DisplayName = "Տեղեկանք դատապարտյալի անձնական գործից";
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PrisonerReferenceReport.rdlc";
            reportViewer.LocalReport.EnableExternalImages = true;
            reportViewer.LocalReport.EnableHyperlinks = true;
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("PrisonerReferenceDS", item));
            reportViewer.AsyncRendering = false;
            reportViewer.ShowWaitControlCancelLink = false;
            reportViewer.LocalReport.Refresh();
            ViewBag.PrisonerReferenceReportViewer = reportViewer;
        }
        public ActionResult Draw_PrisonerReferenceReportTableRows()
        {

            FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();
            if ((System.Web.HttpContext.Current.Session["FilterPsRefReport"] as FilterPrisonerEntity) != null)
                FilterPrisoner = (FilterPrisonerEntity)System.Web.HttpContext.Current.Session["FilterPsRefReport"];
            FilterPrisoner.Type = 2;
            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;
            FilterPrisoner.paging = new FilterPrisonerEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PrisonerEntity> PrisonersList = BusinessLayer_PrisonerService.GetPrisonersForDashboard(FilterPrisoner);

            PrisonersViewModel Model = new PrisonersViewModel();

            Model.ListPrisoners = PrisonersList;
            Model.PrisonerEntityPaging = FilterPrisoner.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղեկանք դատապարտյալի անձնական գործից(table row)", RequestData: JsonConvert.SerializeObject(FilterPrisoner)));

            return PartialView("PrisonerReferenceReport_Table_Row", Model);

        }
        [HttpPost]
        public ActionResult Draw_PrisonerReferenceReportTableRowsByFilter(FilterPrisonerEntity FilterPrisoner)
        {

            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterPsRefReport"] = FilterPrisoner;

            FilterPrisoner.paging = new FilterPrisonerEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PrisonerEntity> PrisonersList = BusinessLayer_PrisonerService.GetPrisonersForDashboard(FilterPrisoner);

            PrisonersViewModel Model = new PrisonersViewModel();

            Model.ListPrisoners = PrisonersList;
            Model.PrisonerEntityPaging = FilterPrisoner.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղեկանք դատապարտյալի անձնական գործից(row by filter)", RequestData: JsonConvert.SerializeObject(FilterPrisoner)));

            return PartialView("PrisonerReferenceReport_Table_Row", Model);
        }
        #endregion

        #region MedicalCount
        [HttpPost]
        public ActionResult MedicalCountReportListData(MedicalCountReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալների Բժշկական քանակ(post)", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
        [HttpGet]
        public ActionResult MedicalCountReportList(string key)
        {
            MedicalCountReportEntity Item = (MedicalCountReportEntity)Session[key];
            SetMedicalCount(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալների Բժշկական քանակ(get)", RequestData: JsonConvert.SerializeObject(Item)));
            return View("MedicalCountReportList");
        }
        [HttpPost]
        public ActionResult MedicalCountReportFilter()
        {
            ViewBag.Title = "Դատապարտյալների Բժշկական քանակ";

            MedicalCountViewModel Model = new MedicalCountViewModel();
            Model.Filter = new MedicalCountReportEntity();
            if ((System.Web.HttpContext.Current.Session["FilterMedicalCountReports"] as EarlyReportEntity) != null)
                Model.Filter = (MedicalCountReportEntity)System.Web.HttpContext.Current.Session["FilterMedicalCountReports"];

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալների Բժշկական քանակ(filter)"));
            return PartialView("MedicalCountReportFilter", Model);
        }
        private void SetMedicalCount(MedicalCountReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;

            List<MedicalCountReportEntity> item = BL_ReportLayer.FillMedicalCountReport_List(entity);

            reportViewer.LocalReport.DisplayName = "Դատապարտյալների Բժշկական քանակ";
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\MedicalCountReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("MedicalCountDS", item));
            ViewBag.MedicalCountReportViewer = reportViewer;
        }
        #endregion

        #region PrisonersCountByOrgUnit

        public ActionResult PrisonersCountByOrgUnitReportListData(CountPrisonersByOrgReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Քկ հիմնարկներում պահվող կալանավորված անձանց և դատապարտյալների թվաքանակը(post)", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
        [HttpGet]
        public ActionResult PrisonersCountByOrgUnitReportList(string key)
        {
            CountPrisonersByOrgReportEntity Item = (CountPrisonersByOrgReportEntity)Session[key];
            SetPrisonersCountByOrgUnit(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Քկ հիմնարկներում պահվող կալանավորված անձանց և դատապարտյալների թվաքանակը(get)", RequestData: JsonConvert.SerializeObject(Item)));
            return View("PrisonersCountByOrgUnitReportList");
        }

        [HttpPost]
        public ActionResult PrisonersCountByOrgUnitReportFilter()
        {
            ViewBag.Title = "Քկ հիմնարկներում պահվող կալանավորված անձանց և դատապարտյալների թվաքանակը";

            PrisonersCountByOrgUnitViewModel Model = new PrisonersCountByOrgUnitViewModel();
            Model.Filter = new CountPrisonersByOrgReportEntity();
            if ((System.Web.HttpContext.Current.Session["FilterPrisonersCountByOrgUnitReports"] as CountPrisonersByOrgReportEntity) != null)
                Model.Filter = (CountPrisonersByOrgReportEntity)System.Web.HttpContext.Current.Session["FilterPrisonersCountByOrgUnitReports"];

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Քկ հիմնարկներում պահվող կալանավորված անձանց և դատապարտյալների թվաքանակը(filter)"));
            return PartialView("PrisonersCountByOrgUnitReportFilter", Model);
        }
        private void SetPrisonersCountByOrgUnit(CountPrisonersByOrgReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            CountPrisonersByOrgTypeEntity entity2 = new CountPrisonersByOrgTypeEntity();
            entity2.CurrentDate = entity.CurrentDate;
            entity2.FirstDate = entity.FirstDate;
            CountPrisonersByCaseCloseEntity entity3 = new CountPrisonersByCaseCloseEntity();
            entity3.CurrentDate = entity.CurrentDate;
            entity3.FirstDate = entity.FirstDate;

            List<CountPrisonersByOrgReportEntity> item = BL_ReportLayer.FillPrisonersCountByOrgUnitReport_List(entity);
            List<CountPrisonersByOrgTypeEntity> item2 = BL_ReportLayer.FillPrisonersCountByOrgType_List(entity2);
            List<CountPrisonersByCaseCloseEntity> item3 = BL_ReportLayer.FillPrisonersCountClosedReport_List(entity3);

            reportViewer.LocalReport.DisplayName = "Քկ հիմնարկներում պահվող կալանավորված անձանց և դատապարտյալների թվաքանակը";
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PrisonersCountByOrgUnitReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("PrisonersCountByOrgUnitDS", item));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("PrisonersCountByOrgTypeDS", item2));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("PrisonersCaseCloseCount", item3));
            ViewBag.PrisonersCountByOrgUnitReportViewer = reportViewer;
        }
        #endregion

        #region PrisonerConvictSemester

        public ActionResult PrisonerConvictSemesterReportListData(PrisonerConvictSemesterReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հատուկ քանակակազմի թվաքանակի և շարժի վերաբերյալ(post)", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
        [HttpGet]
        public ActionResult PrisonerConvictSemesterReportList(string key)
        {
            PrisonerConvictSemesterReportEntity Item = (PrisonerConvictSemesterReportEntity)Session[key];
            SetSemesterCount(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հատուկ քանակակազմի թվաքանակի և շարժի վերաբերյալ", RequestData: JsonConvert.SerializeObject(Item)));
            return View("PrisonerConvictSemesterReportList");
        }
        [HttpPost]
        public ActionResult PrisonerConvictSemesterReportFilter()
        {
            ViewBag.Title = "Հատուկ քանակակազմի թվաքանակի և շարժի վերաբերյալ";

            PrisonerConvictSemesterViewModel Model = new PrisonerConvictSemesterViewModel();
            Model.Filter = new PrisonerConvictSemesterReportEntity();
            if ((System.Web.HttpContext.Current.Session["FilterPrisonersCountByOrgUnitReports"] as PrisonerConvictSemesterReportEntity) != null)
                Model.Filter = (PrisonerConvictSemesterReportEntity)System.Web.HttpContext.Current.Session["PrisonerConvictSemesterReportEntity"];

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հատուկ քանակակազմի թվաքանակի և շարժի վերաբերյալ(filter)"));
            return PartialView("PrisonerConvictSemesterReportFilter", Model);
        }
        private void SetSemesterCount(PrisonerConvictSemesterReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;

            List<PrisonerConvictSemesterReportEntity> item = BL_ReportLayer.FillPrisonersConvictCountSemesterReport_List(entity);

            reportViewer.LocalReport.DisplayName = "Հատուկ քանակակազմի թվաքանակի և շարժի վերաբերյալ";
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PrisonerConvictSemesterReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("PrisonerConvictSemesterDS", item));
            ViewBag.PrisonerConvictSemesterReportViewer = reportViewer;
        }
        #endregion

        #region ArticelePercent

        public ActionResult ArticlePercentReportListData(ArticlePercentReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հոդվածների տոկոսային հարաբերությունը(post)", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
        [HttpGet]
        public ActionResult ArticlePercentReportList(string key)
        {
            ArticlePercentReportEntity Item = (ArticlePercentReportEntity)Session[key];
            SetArticlePercent(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հոդվածների տոկոսային հարաբերությունը", RequestData: JsonConvert.SerializeObject(Item)));
            return View("ArticlePercentReportList");
        }
        [HttpPost]
        public ActionResult ArticlePercentReportFilter()
        {
            ViewBag.Title = "Հոդվածների տոկոսային հարաբերությունը";

            ArticlePercentViewModel Model = new ArticlePercentViewModel();
            Model.Filter = new ArticlePercentReportEntity();
            if ((System.Web.HttpContext.Current.Session["FilterArticlePercentReports"] as ArticlePercentReportEntity) != null)
                Model.Filter = (ArticlePercentReportEntity)System.Web.HttpContext.Current.Session["ArticlePercentReportEntity"];

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հոդվածների տոկոսային հարաբերությունը(filter)"));
            return PartialView("ArticlePercentReportFilter", Model);
        }
        private void SetArticlePercent(ArticlePercentReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;

            List<ArticlePercentReportEntity> item = BL_ReportLayer.FillArticlePercentReport_List(entity);

            reportViewer.LocalReport.DisplayName = "Հոդվածների տոկոսային հարաբերությունը";
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\ArticlePercentReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("ArticlePercentDS", item));
            ViewBag.ArticelPercentReportViewer = reportViewer;
        }
        #endregion

        #region PrisonersReferendum

        public ActionResult PrisonersReferendumReportListData(PrisonersReferendumReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Կալանավորված անձանց պահելու վայրում կազմված քաղաքացիների ցուցակ(post)", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
        [HttpGet]
        public ActionResult PrisonersReferendumReportList(string key)
        {
            PrisonersReferendumReportEntity Item = (PrisonersReferendumReportEntity)Session[key];
            SetPrisonersReferendum(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Կալանավորված անձանց պահելու վայրում կազմված քաղաքացիների ցուցակ", RequestData: JsonConvert.SerializeObject(Item)));
            return View("PrisonersReferendumReportList");
        }
        [HttpPost]
        public ActionResult PrisonersReferendumReportFilter()
        {
            ViewBag.Title = "Կալանավորված անձանց պահելու վայրում կազմված քաղաքացիների ցուցակ";

            PrisonerReferendumReportViewModel Model = new PrisonerReferendumReportViewModel();
            Model.Filter = new PrisonersReferendumReportEntity();
            if ((System.Web.HttpContext.Current.Session["FilterPrisonersReferendumReports"] as ArticlePercentReportEntity) != null)
                Model.Filter = (PrisonersReferendumReportEntity)System.Web.HttpContext.Current.Session["PrisonersReferendumReportEntity"];

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Կալանավորված անձանց պահելու վայրում կազմված քաղաքացիների ցուցակ(filter)"));
            return PartialView("PrisonersReferendumReportFilter", Model);
        }
        private void SetPrisonersReferendum(PrisonersReferendumReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;

            List<PrisonersReferendumReportEntity> item = BL_ReportLayer.FillPrisonersReferendumReport_List(entity);

            reportViewer.LocalReport.DisplayName = "Կալանավորված անձանց պահելու վայրում կազմված քաղաքացիների ցուցակ";
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\PrisonersReferendumReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("PrisonerReferendumDS", item));
            ViewBag.PrisonersReferendumReportViewer = reportViewer;
        }
        #endregion

        #region OrgTypeChanged
        public ActionResult OrgTypeChangedReportListData(OrgTypeChangedReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ուղղիչ հիմնարկի տեսկը փոխվել է(post)", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
        [HttpGet]
        public ActionResult OrgTypeChangedReportList(string key)
        {
            OrgTypeChangedReportEntity Item = (OrgTypeChangedReportEntity)Session[key];
            SetOrgTypeChanged(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ուղղիչ հիմնարկի տեսկը փոխվել է", RequestData: JsonConvert.SerializeObject(Item)));
            return View("OrgTypeChangedReportList");
        }
        [HttpPost]
        public ActionResult OrgTypeChangedReportFilter()
        {
            ViewBag.Title = "Ուղղիչ հիմնարկի տեսկը փոխվել է";

            OrgTypeChangedReportViewModel Model = new OrgTypeChangedReportViewModel();
            Model.Filter = new OrgTypeChangedReportEntity();
            if ((System.Web.HttpContext.Current.Session["FilterOrgTypeChangedReports"] as OrgTypeChangedReportEntity) != null)
                Model.Filter = (OrgTypeChangedReportEntity)System.Web.HttpContext.Current.Session["OrgTypeChangedReportEntity"];

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ուղղիչ հիմնարկի տեսկը փոխվել է(filter)"));
            return PartialView("OrgTypeChangedReportFilter", Model);
        }
        private void SetOrgTypeChanged(OrgTypeChangedReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;

            List<OrgTypeChangedReportEntity> item = BL_ReportLayer.FillOrgTypeChangedReport_List(entity);

            reportViewer.LocalReport.DisplayName = "Ուղղիչ հիմնարկի տեսկը փոխվել է";
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\OrgTypeChangedReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("OrgTypeChangedDS", item));
            ViewBag.OrgTypeChangedReportViewer = reportViewer;
        }
        
        #endregion
    }
}