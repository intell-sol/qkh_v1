﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.Encouragements)]
    public class EncouragementsController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public EncouragementsController()
        {
            PermissionHCID = PermissionsHardCodedIds.Encouragements;
        }

        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Խրախուսանքներ";

                EncouragementsViewModel Model = new EncouragementsViewModel();
                
                Model.FilterEncouragements = new FilterEncouragementsEntity();
                if ((System.Web.HttpContext.Current.Session["FilterEncouragements"] as FilterEncouragementsEntity) != null)
                    Model.FilterEncouragements = (FilterEncouragementsEntity)System.Web.HttpContext.Current.Session["FilterEncouragements"];

                Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ENCOURAGEMENT_TYPE_CONVICT);
                Model.TypeLibListPrisoners = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ENCOURAGEMENT_TYPE_PRISONER);
                Model.BaseLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ENCOURAGEMENT_BASE);
                Model.EmployeeList = BusinessLayer_OrganizatonLayer.GetOrganizationEmployees(ViewBag.SelectedOrgUnitID);

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խրախուսանքներ"));

                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել խրախուսանքներ"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }

        [HttpPost]
        public ActionResult Edit(int? PrisonerID)
        {
            EncouragementsViewModel Model = new EncouragementsViewModel();

            Model.Encouragement = BusinessLayer_PrisonerService.GetEncouragement(PrisonerID: PrisonerID.Value);
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել խրախուսանքներ", RequestData: PrisonerID.ToString()));

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;
            return PartialView("Add", Model);
        }

        public ActionResult Draw_EncouragementTableRow(int? id = null)
        {
            // UNDONE: watch this section
            FilterEncouragementsEntity FilterEncouragements = new FilterEncouragementsEntity();
            if ((System.Web.HttpContext.Current.Session["FilterEncouragements"] as FilterEncouragementsEntity) != null)
                FilterEncouragements = (FilterEncouragementsEntity)System.Web.HttpContext.Current.Session["FilterEncouragements"];

            int page = id ?? 1;


            FilterEncouragements.paging = new FilterEncouragementsEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<EncouragementsDashboardEntity> ListDashboardEncouragements = BusinessLayer_PrisonerService.GetEncouragementsForDashboard(FilterEncouragements);

            EncouragementsViewModel Model = new EncouragementsViewModel();

            Model.ListDashboardEncouragements = ListDashboardEncouragements;
            Model.EncouragementsEntityPaging = FilterEncouragements.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խրախուսանքներր դիտում"));

            return PartialView("Encouragements_Table_Row", Model);
        }

        public ActionResult Draw_EncouragementTableRowByFilter(FilterEncouragementsEntity FilterEncouragements)
        {
            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterEncouragements"] = FilterEncouragements;

            FilterEncouragements.paging = new FilterEncouragementsEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<EncouragementsDashboardEntity> PrisonersList = BusinessLayer_PrisonerService.GetEncouragementsForDashboard(FilterEncouragements);

            EncouragementsViewModel Model = new EncouragementsViewModel();

            Model.ListDashboardEncouragements = PrisonersList;
            Model.EncouragementsEntityPaging= FilterEncouragements.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խրախուսանքների Որոնում", RequestData: JsonConvert.SerializeObject(FilterEncouragements)));
            return PartialView("Encouragements_Table_Row", Model);
        }

        [HttpPost]
        public ActionResult AddEnc(EncouragementsEntity Item)
        {
            bool status = false;
            int? id = null;
            if (Item.PrisonerID != null)
            {
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];

                id = BusinessLayer_PrisonerService.AddEncouragement(Item);
            }

            if (id != null) status = true;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խրախուսանքի Ավելացում", RequestData: JsonConvert.SerializeObject(Item)));


            return Json(new { status = status });
        }

        public ActionResult RemEnc(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetEncouragements(new EncouragementsEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                EncouragementsEntity oldEntity = BusinessLayer_PrisonerService.GetEncouragements(new EncouragementsEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.ENCOURAGEMENTS_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateEncouragements(new EncouragementsEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խրախուսանքի Հեռացում", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult Draw_ResetEncouragementTableRow()
        {
            Session["FilterEncouragements"] = null;
            return Json(new { status = true });
        }
        public ActionResult EditEnc(EncouragementsEntity Item)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetEncouragements(new EncouragementsEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.ENCOURAGEMENTS_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateEncouragements(Item);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խրախուսանքի Խմբագրում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status, needApprove = needApprove });
        }

    }
}