﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.ConflictPersons)]
    public class ConflictsController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public ConflictsController()
        {
            PermissionHCID = PermissionsHardCodedIds.ConflictPersons;
        }
        
        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Աշխատանք";

                //ConflictsViewModel Model = new ConflictsViewModel();
                
                //Model.FilterConflicts = new FilterConflictsEntity();
                //if ((System.Web.HttpContext.Current.Session["FilterConflicts"] as FilterConflictsEntity) != null)
                //    Model.FilterConflicts = (FilterConflictsEntity)System.Web.HttpContext.Current.Session["FilterConflicts"];

                //Model.WorkTitleLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ConflictS_TITLE);
                //Model.JobTypeLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ConflictS_TYPE);
                //Model.EmployerLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ConflictS_EMP);
                //Model.ReleaseBasisLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ConflictS_REL);
                //Model.EngagementBasisLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ConflictS_ENG);

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown"));

                return View();
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }
        [HttpPost]
        public ActionResult Edit(int? PrisonerID)
        {
            ConflictsViewModel Model = new ConflictsViewModel();
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            Model.Conflict = BusinessLayer_PrisonerService.GetConflict(PrisonerID: PrisonerID.Value);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: PrisonerID.ToString()));

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;

            return PartialView("Index", Model);
        }

        [HttpPost]
        public ActionResult AddData(ConflictsEntity Item)
        {
            bool status = false;
            int? id = null;
            if (Item.PrisonerID != null)
            {
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];

                if (Item.Person != null)
                {

                    Item.Person = BusinessLayer_PrisonerService.AddPersonAndGet(Person: Item.Person);

                    Item.PersonID = Item.Person.ID;
                  
                    if (Item.PersonID != null)
                    {
                        id = BusinessLayer_PrisonerService.AddConflict(Item);
                        if (id != null) status = true;
                    }
                }
            }
           
            if (id != null) status = true;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = status });
        }

        public ActionResult RemData(int ID)
        {
            bool? status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetConflicts(new ConflictsEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                ConflictsEntity oldEntity = BusinessLayer_PrisonerService.GetConflicts(new ConflictsEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.CONFLICTS_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateConflict(new ConflictsEntity(ID: ID, Status: false));
            }
            if (status == null) status = false;
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: ID.ToString()));
            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult EditData(ConflictsEntity Item)
        {
            bool? status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetConflicts(new ConflictsEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && PrisonerID.HasValue && curPrisoner.State.Value)
                {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.CONFLICTS_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateConflict(Item);
                if (status == null) status = false;
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status, needApprove = needApprove });
        }

    }
}