﻿using BusinessLayer.BusinessServices;
using CommonLayer.BusinessEntities;
using MVC___Internal_System.Attributes;
using MVC___Internal_System.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.Amnesties)]
    public class AmnestyController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public AmnestyController()
        {
            PermissionHCID = PermissionsHardCodedIds.Amnesties;
        }
        // GET: Amnesty
        public ActionResult Index()
        {
            try
            {
                AmnestyViewModel Model = new AmnestyViewModel();
                ViewBag.Title = "Համաներում";
                Model.FilterAmnesty = new FilterAmnestyEntity();
                if ((System.Web.HttpContext.Current.Session["FilterAmnesty"] as FilterDepartureEntity) != null)
                    Model.FilterAmnesty = (FilterAmnestyEntity)System.Web.HttpContext.Current.Session["FilterAmnesty"];
                
                //Model.Amnesties = BusinessLayer_AmnestyLayer.GetList_Amensty(new AmnestyEntity());
        
                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Համաներումներ"));

                return View("Index", Model);
            }
            catch (Exception e)
            {
                return View("Error", e);
            }
        }
        public ActionResult Get_AmnestyListTableRow(string id, int page = 1)
        {
            // UNDONE: watch this section
            FilterAmnestyEntity FilterEntity = new FilterAmnestyEntity();
            if ((System.Web.HttpContext.Current.Session["FilterAmnesty"] as FilterAmnestyEntity) != null)
                FilterEntity = (FilterAmnestyEntity)System.Web.HttpContext.Current.Session["FilterAmnesty"];
           
            FilterEntity.paging = new FilterAmnestyEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());
            AmnestyViewModel Model = new AmnestyViewModel();
            //Model.AmnestiesPersons == BusinessLayer_AmnestyLayer.GetList_AmenstyByNumber();
            Model.AmnestyEntityPaging = FilterEntity.paging;
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել (hamanerum)"));

            return View("AmnestyList_Table_Row", Model);
        }
        public ActionResult Get_AmnestyTableRow(int? id = null)
        {
            // UNDONE: watch this section
            FilterAmnestyEntity FilterEntity = new FilterAmnestyEntity();
            if ((System.Web.HttpContext.Current.Session["FilterAmnesty"] as FilterAmnestyEntity) != null)
                FilterEntity = (FilterAmnestyEntity)System.Web.HttpContext.Current.Session["FilterAmnesty"];

            int page = id ?? 1;


            FilterEntity.paging = new FilterAmnestyEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());
            List<AmnestyDashboardEntity> ListDashboardAmnesties= BusinessLayer_AmnestyLayer.GetList_AmenstyForDashboard(FilterEntity);

            AmnestyViewModel Model = new AmnestyViewModel();

           
            Model.ListDashboardAmnesty = ListDashboardAmnesties;
            Model.AmnestyEntityPaging = FilterEntity.paging;
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել (hamanerum)"));

            return PartialView("Amnesty_Table_Row", Model);
        }
        [HttpGet]
        public ActionResult Add()
        {
            try
            {
                ViewBag.Title = "Ավելացնել Դատապարտյալ";
                AmnestyViewModel Model = new AmnestyViewModel();
                Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.AMNESTY_TYPE);
                Model.ReciepeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.AMNESTY_RECIPIENT);
                Model.SignPersonLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.AMNESTY_SIGNATURE);
                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել Դատապարտյալ"));

                return View("Add",Model);
            }
            catch (Exception e)
            {
                return View("Error", e);
            }
        }
        public ActionResult GetAmnesties(string number)
        {
            try
            {
                ViewBag.Title = "Համաներումների ցուցակ";
                AmnestyViewModel Model = new AmnestyViewModel();
                
                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Համաներումների ցուցակ"));
                Model.Amnesties = BusinessLayer_AmnestyLayer.GetList_Amensty(new AmnestyEntity(Number: number));
                
                return View("AmnestyList", Model);
            }
            catch (Exception e)
            {
                return View("Error", e);
            }
        }
        public ActionResult Edit(int id)
        {
            try
            {
                ViewBag.Title = "Ավելացնել Դատապարտյալ";
                AmnestyViewModel Model = new AmnestyViewModel();
                Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.AMNESTY_TYPE);
                Model.ReciepeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.AMNESTY_RECIPIENT);
                Model.SignPersonLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.AMNESTY_SIGNATURE);
                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել Դատապարտյալ"));
                Model.currentAmnesty = BusinessLayer_AmnestyLayer.GetList_Amensty(new AmnestyEntity(ID: id)).First();
                if (Model.currentAmnesty != null)
                    Model.currentAmnesty.AmnestyPoints = BusinessLayer_AmnestyLayer.GetList_AmenstyPointsClassified(new AmnestyPointsEntity(AmnestyID: id));
                return View("Add", Model);
            }
            catch (Exception e)
            {
                return View("Error", e);
            }
        }
        [HttpPost]
        public ActionResult EditAmnesty(AmnestyEntity Item)
        {
            bool? status = true;
            Item.State = true;
            status = BusinessLayer_AmnestyLayer.UpdateAmnesty(Item);
            if (status == null) status = false;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելավնել համաներում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }

        [HttpPost]
        public ActionResult AddAmnesty(AmnestyEntity Item)
        {

            bool status = false;
            int? id = null;
            Item.State = true;
            id = BusinessLayer_AmnestyLayer.AddAmnesty(Item);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելավնել համաներում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }

    }
}