﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System.Linq;
using System.Web.Mvc;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.MilitaryService)]
    public class _Data_Army_Controller : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public _Data_Army_Controller()
        {
            PermissionHCID = PermissionsHardCodedIds.MilitaryService;
        }
        /// <summary>
        /// Primary data add partial view
        /// </summary>
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ծառայության ավելացում"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }

        [HttpPost]
        public ActionResult Edit(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            PrisonerEntity Prisoner = new PrisonerEntity();
            Prisoner.ID = PrisonerID;
            if (PrisonerID != null)
            {
                Prisoner.MilitaryService = BusinessLayer_PrisonerService.GetMilitaryServiceData(PrisonerID.Value);

                Prisoner.MilitaryService.Army = BusinessLayer_PrisonerService.GetArmy(new ArmyEntity(PrisonerID: PrisonerID.Value));

                Prisoner.MilitaryService.ArmyAwards = BusinessLayer_PrisonerService.GetArmyAwards(PrisonerID.Value);

                PrisonerEntity currentPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: PrisonerID.Value);
             
                Prisoner.MilitaryService.Files = BusinessLayer_PrisonerService.GetFiles(PrisonerID: PrisonerID, TypeID: FileType.ARMY_ATTACHED_FILES);

                Prisoner.MilitaryService.NotServed = BusinessLayer_PrisonerService.GetNotServed(new NotServedEntity(PrisonerID: PrisonerID.Value));

                Prisoner.MilitaryService.WarInvolved = BusinessLayer_PrisonerService.GetWarInvolved(new WarInvolvedEntity(PrisonerID: PrisonerID.Value));
            }

            Model.Prisoner = Prisoner;

            Model.NoMilitaryServe = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.NO_SERVE);
            Model.MilitarWars = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.MILITARY_WARS);
            Model.MilitaryAwards = BusinessLayer_LibsLayer.GetLibsLocalTreeByLibPathID((int)LibsEntity.LIBS.MILITARY_AWARDS);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ծառայության խմբագրում (preview)", RequestData: PrisonerID.ToString()));

            return PartialView("Index", Model);
        }

        public ActionResult AddArmy(ArmyEntity Army)
        {
            bool status = false;
            int? id = null;
            if (Army.PrisonerID != null)
            {
                id = BusinessLayer_PrisonerService.AddArmy(Army);
            }

            if (id != null) status = true;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ծառայության ավելացում", RequestData: JsonConvert.SerializeObject(Army)));

            return Json(new { status = status });
        }

        public ActionResult EditArmy(ArmyEntity Data)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetArmy(new ArmyEntity(ID: Data.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Data),
                    Type: 3,
                    EntityID: Data.ID,
                    EID: (int)MergeEntity.TableIDS.ARMY_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                if (Data.ID != null)
                {
                    status = BusinessLayer_PrisonerService.UpdateArmy(Data);
                }
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ծառայության խմբագրում", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult RemArmy(int ID)
        {
            bool status = false;
            bool needApprove = false;
            
            ArmyEntity Data = BusinessLayer_PrisonerService.GetArmy(new ArmyEntity(ID: ID)).First();
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(Data.PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && Data.PrisonerID.HasValue && curPrisoner.State.Value)
            {
                Data.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: Data.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Data),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.ARMY_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateArmy(new ArmyEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ծառայության հեռացում", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult SaveContent(MilitaryServiceEntity Entity)
        {
            bool status = false;
            bool needApprove = false;

            if (Entity != null && Entity.PrisonerID != null)
            {
                MilitaryServiceEntity curPrisonerMilitary = new MilitaryServiceEntity();

                curPrisonerMilitary.Army = BusinessLayer_PrisonerService.GetArmy(new ArmyEntity(PrisonerID: Entity.PrisonerID.Value));

                status = true;;


                // check awards
                curPrisonerMilitary.ArmyAwards = BusinessLayer_PrisonerService.GetArmyAwards(Entity.PrisonerID.Value);
                curPrisonerMilitary.WarInvolved = BusinessLayer_PrisonerService.GetWarInvolved(new WarInvolvedEntity(PrisonerID: Entity.PrisonerID.Value));
                NotServedEntity curEntity = BusinessLayer_PrisonerService.GetNotServed(new NotServedEntity(PrisonerID: Entity.PrisonerID));

                if (ViewBag.PPType == 2 && (curPrisonerMilitary.ArmyAwards.Any() || curPrisonerMilitary.WarInvolved.Any() || curEntity != null || curEntity.ID != null))
                {
                    MergeEntity curMergeEntity = new MergeEntity(PrisonerID: Entity.PrisonerID.Value,
                        EmployeeID: ViewBag.UserID,
                        Json: JsonConvert.SerializeObject(Entity),
                        Type: 3,
                        EntityID: null,
                        EID: (int)MergeEntity.TableIDS.ARMY_CONTENT);

                    int? tempID = BusinessLayer_MergeLayer.AddModification(curMergeEntity);

                    needApprove = true;

                    status = tempID != null;
                }

                if (curPrisonerMilitary.ArmyAwards != null && Entity.ArmyAwards != null && !needApprove)
                {
                    // update ArmyAwards
                    foreach (ArmyAwardEntity currentEntity in Entity.ArmyAwards)
                    {
                        if (currentEntity.LibItemID != null && currentEntity.PrisonerID != null && !curPrisonerMilitary.ArmyAwards.FindAll(r => r.LibItemID == currentEntity.LibItemID).Any())
                        {
                            int? curID = BusinessLayer_PrisonerService.AddArmyAwards(currentEntity);
                            if (curID == null) status = false;
                        }
                    }
                    foreach (ArmyAwardEntity currentEntity in curPrisonerMilitary.ArmyAwards)
                    {
                        if (!Entity.ArmyAwards.FindAll(r => (r.LibItemID != null && r.PrisonerID != null && r.LibItemID == currentEntity.LibItemID)).Any())
                        {
                            currentEntity.Status = false;
                            bool tempStatus = BusinessLayer_PrisonerService.UpdateArmyAwards(currentEntity);
                            if (!tempStatus) status = false;
                        }
                    }
                }

                // check war involved

              if (curPrisonerMilitary.WarInvolved != null && Entity.WarInvolved != null && !needApprove)
                {
                    // update War involved
                    foreach (WarInvolvedEntity currentEntity in Entity.WarInvolved)
                    {
                        if (currentEntity.LibItemID != null && currentEntity.PrisonerID != null && !curPrisonerMilitary.WarInvolved.FindAll(r => r.LibItemID == currentEntity.LibItemID).Any())
                        {
                            int? curID = BusinessLayer_PrisonerService.AddWarInvolved(currentEntity);
                            if (curID == null) status = false;
                        }
                    }
                    foreach (WarInvolvedEntity currentEntity in curPrisonerMilitary.WarInvolved)
                    {
                        if (!Entity.WarInvolved.FindAll(r => (r.LibItemID != null && r.PrisonerID != null && r.LibItemID == currentEntity.LibItemID)).Any())
                        {
                            currentEntity.Status = false;
                            bool tempStatus = BusinessLayer_PrisonerService.UpdateWarInvolved(currentEntity);
                            if (!tempStatus) status = false;
                        }
                    }
                }

                if (Entity.NotServed != null && Entity.NotServed.ReasonLibItemID != null && Entity.NotServed.PrisonerID != null && !needApprove)
                {
                    status = true;

                    if (curPrisonerMilitary.Army != null && curPrisonerMilitary.Army.Any() && curEntity != null && curEntity.ID != null)
                    {
                        status = BusinessLayer_PrisonerService.UpdateNotServed(new NotServedEntity(ID: curEntity.ID, Status: false));
                    }
                    else if (curPrisonerMilitary.Army == null || !curPrisonerMilitary.Army.Any())
                    {
                        if (curEntity != null && curEntity.PrisonerID != null && curEntity.ReasonLibItemID != null)
                        {
                            if ((curEntity.ReasonLibItemID != Entity.NotServed.ReasonLibItemID || curEntity.Description != Entity.NotServed.Description) && curEntity.PrisonerID == Entity.NotServed.PrisonerID)
                            {
                                curEntity.ReasonLibItemID = Entity.NotServed.ReasonLibItemID;
                                curEntity.Description = Entity.NotServed.Description;
                                status = BusinessLayer_PrisonerService.UpdateNotServed(curEntity);
                            }
                        }
                        else
                        {
                            int? id = BusinessLayer_PrisonerService.AddNotServed(Entity.NotServed);
                            if (id == null) status = false;
                        }
                    }
                }

            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ծառայության  պահպանում", RequestData: JsonConvert.SerializeObject(Entity)));

            return Json(new { status = status, needApprove = needApprove });
        }
    }
}