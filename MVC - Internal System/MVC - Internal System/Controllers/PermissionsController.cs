﻿using BusinessLayer.BusinessServices;
using CommonLayer.BusinessEntities;
using MVC___Internal_System.Attributes;
using MVC___Internal_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.Permissions)]
    public class PermissionsController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public PermissionsController()
        {
            PermissionHCID = PermissionsHardCodedIds.Permissions;
        }

        // GET: Permission
        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Թույլատվությունների դիտում";

                PermissionsViewModel Model = new PermissionsViewModel();

                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
                
                //List<PermissionsGurdEntity> positions = BusinessLayer_PrisonerService.GetPermissionGuard(User.SelectedOrgUnitID);
                List<OrgUnitEntity> positions = BusinessLayer_OrganizatonLayer.GetPositionsByOrgUnitID(User.SelectedOrgUnitID);
                Model.OrgUnitPositions = positions;
                Model.Employees = BusinessLayer_OrganizatonLayer.GetOrganizationEmployees(User.SelectedOrgUnitID);
                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        public ActionResult Add(int PositionID, List<int> PermissionIDs)
        {
            try
            {
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
                int? OrgUnitID = User.SelectedOrgUnitID;

                foreach(int PermissionID in PermissionIDs)
                {
                    int? id = BusinessLayer_PrisonerService.AddPermissionGuard(PositionID, PermissionID, OrgUnitID);
                }
                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Թույլատվության ավելացում", RequestData: JsonConvert.SerializeObject(new { PositionID = PositionID, PermissionIDs = PermissionIDs.ToString() })));

                return Json(new { status = true });
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return Json(new { status = false });
            }
        }

        public ActionResult Remove(int ID)
        {
            try
            {
                BusinessLayer_PrisonerService.UpdatePermissionGuard(ID,null,null,false);

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Թույլատվության հեռացում", RequestData: JsonConvert.SerializeObject(new { ID = ID })));
                return Json(new { status = true });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return Json(new { status = false });
            }
        }

        public ActionResult Draw_PermissionsTableRows()
        {
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];

            PermissionsViewModel Model = new PermissionsViewModel();

            //Model.OfficialVisitsPositions = BusinessLayer_PermLayer.GetPermissionedPositionsForDashboard((int)PermissionsHardCodedIds.OfficialVisits, User.SelectedOrgUnitID);
            Model.PersonalVisitsPositions = BusinessLayer_PrisonerService.GetPermissionGuard(OrgUnitID: User.SelectedOrgUnitID, PermissionID: (int)PermissionsHardCodedIds.PersonalVisits);
            Model.PackagesPositions = BusinessLayer_PrisonerService.GetPermissionGuard(OrgUnitID: User.SelectedOrgUnitID, PermissionID: (int)PermissionsHardCodedIds.Packages);
            Model.DeparturesPositions = BusinessLayer_PrisonerService.GetPermissionGuard(OrgUnitID: User.SelectedOrgUnitID, PermissionID: (int)PermissionsHardCodedIds.Departures);
            Model.TransferPositions = BusinessLayer_PrisonerService.GetPermissionGuard(OrgUnitID: User.SelectedOrgUnitID, PermissionID: (int)PermissionsHardCodedIds.Transfers);
            //Model.PersonalVisitsPositions = BusinessLayer_PermLayer.GetPermissionedPositionsForDashboard((int)PermissionsHardCodedIds.PersonalVisits, User.SelectedOrgUnitID);
            //Model.PackagesPositions = BusinessLayer_PermLayer.GetPermissionedPositionsForDashboard((int)PermissionsHardCodedIds.Packages, User.SelectedOrgUnitID);
            //Model.DeparturesPositions = BusinessLayer_PermLayer.GetPermissionedPositionsForDashboard((int)PermissionsHardCodedIds.Departures, User.SelectedOrgUnitID);
            //Model.TransferPositions = BusinessLayer_PermLayer.GetPermissionedPositionsForDashboard((int)PermissionsHardCodedIds.Transfers, User.SelectedOrgUnitID);

            return PartialView("Permissions_Table_Row", Model);
        }

    }
}