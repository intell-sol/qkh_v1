﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    //[PermissionLevel(PermissionHCID = PermissionsHardCodedIds.Dashboard)]
    public class DashboardController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public DashboardController()
        {
            //PermissionHCID = PermissionsHardCodedIds.Dashboard;
        }
        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Ցուցատախտակ";

                DashboardViewModel Model = new DashboardViewModel();             

               

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հանձնուքներ"));

                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }
        public ActionResult Get_PackageTableRow()
        {
            // UNDONE: watch this section

            PackagesViewModel Model = new PackagesViewModel();
            List<int> Ids = new List<int>();
            //List<OrgUnitEntity> Units = BusinessLayer_OrganizatonLayer.GetOrgUnitByTypeID();

            //foreach (var item in Units)
            //{
            //    Ids.Add(item.ID);
            //}
            Ids.Add(ViewBag.SelectedOrgUnitID);
            FilterPackagesEntity FilterEntity = new FilterPackagesEntity(OrgUnitIDList: Ids,ApproveStatusAll:false);
            FilterEntity.paging = new FilterPackagesEntity.Paging(CurrentPage: 1, ViewCount: 100);
            Model.ListDashboardPackages = BusinessLayer_PrisonerService.GetPackagesForDashboard(FilterEntity);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Փաթեթների դիտում"));

            return PartialView("Packages_Table_Row", Model);
        }

        public ActionResult Get_TransferTableRow()
        {
            // UNDONE: watch this section

            TransferViewModel Model = new TransferViewModel();
            List<int> Ids = new List<int>();
            //List<OrgUnitEntity> Units = BusinessLayer_OrganizatonLayer.GetOrgUnitByTypeID();

            //foreach (var item in Units)
            //{
            //    Ids.Add(item.ID);
            //}
            Ids.Add(ViewBag.SelectedOrgUnitID);
            FilterTransferEntity FilterEntity = new FilterTransferEntity(OrgUnitIDList: Ids);
            FilterEntity.paging = new FilterTransferEntity.Paging(CurrentPage: 1, ViewCount: 100);

            Model.ListDashboardTransfer = BusinessLayer_PrisonerService.GetTransfersForDashboard(FilterEntity);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "տեղափոխումների դիտում"));

            return PartialView("Transfers_Table_Row", Model);
        }
        public ActionResult Get_DeparturesTableRow()
        {
            // UNDONE: watch this section

            DepartureViewModel Model = new DepartureViewModel();
            List<int> Ids = new List<int>();
            List<OrgUnitEntity> Units = BusinessLayer_OrganizatonLayer.GetOrgUnitByTypeID();

            foreach (var item in Units)
            {
                Ids.Add(item.ID);
            }
            FilterDepartureEntity FilterEntity = new FilterDepartureEntity(OrgUnitIDList: Ids, State: true);
            FilterEntity.paging = new FilterDepartureEntity.Paging(CurrentPage: 1, ViewCount: 100);

            Model.ListDashboardDeparture = BusinessLayer_PrisonerService.GetDeparturesForDashboard(FilterEntity);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Մեկնումների դիտում"));

            return PartialView("Departures_Table_Row", Model);
        }

        public ActionResult Get_List_Overdue()
        {
            SentenceViewModel Model = new SentenceViewModel();

            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            Model.SentencingOverdueDashboardList = BusinessLayer_DashboardLayer.GetListOverdueForDashboard(User.SelectedOrgUnitID);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Օրվա ժամկետանցների դիտում"));
            
            return PartialView("List_Overdue_Table_Row", Model);
        }

        public ActionResult Get_List_SevenDay()
        {
            SentenceViewModel Model = new SentenceViewModel();

            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            Model.SentencingOverdueDashboardList = BusinessLayer_DashboardLayer.GetListSevenDayForDashboard(User.SelectedOrgUnitID);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Կալանքի ավարտից 7 աշխատանքային օրերի դիտում"));

            return PartialView("List_SevenDay_Table_Row", Model);
        }

        public ActionResult Get_List_EarlyDate()
        {
            SentenceViewModel Model = new SentenceViewModel();

            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            Model.SentencingOverdueDashboardList = BusinessLayer_DashboardLayer.GetListEarlyDateForDashboard(User.SelectedOrgUnitID);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Պայմանական Վաղաժամկետների դիտում"));

            return PartialView("List_EarlyDate_Table_Row", Model);
        }

    }
}