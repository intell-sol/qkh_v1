﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.DataTerminate)]
    public class _Data_Terminate_Controller : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public _Data_Terminate_Controller()
        {
            PermissionHCID = PermissionsHardCodedIds.DataTerminate;
        }
        /// <summary>
        /// Primary data add partial view
        /// </summary>
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել գործի փակում"));

            return PartialView("Index");
        }

        [HttpPost]
        /// <summary>
        /// CaseClose data edit partial view
        /// </summary>
        public ActionResult Edit(int? PrisonerID)
        {
            TerminationViewModel Model = new TerminationViewModel();
            
            Model.TerminateEntity = BusinessLayer_PrisonerService.GetCaseClose(new CaseCloseEntity(PrisonerID: PrisonerID.Value));

            if(BusinessLayer_PrisonerService.GetActiveCase(new CaseCloseEntity(PrisonerID: PrisonerID.Value)).Any())
                Model.TerminateEntity.Add(BusinessLayer_PrisonerService.GetActiveCase(new CaseCloseEntity(PrisonerID: PrisonerID.Value)).First());

            if (PrisonerID != null)
            {
                PrisonerEntity currentPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: PrisonerID.Value);

                Model.Files = BusinessLayer_PrisonerService.GetFiles(PrisonerID: PrisonerID, TypeID: FileType.TERMINATE);
            }

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել  գործի փակումը", RequestData: PrisonerID.ToString()));

            return PartialView("Index", Model);
        }

        public ActionResult SaveTermination(CaseCloseEntity Data)
        {
            bool status = false;
            int? id = null;

            if (Data.PrisonerID != null)
            {
                List<CaseCloseEntity> activeCaseList = BusinessLayer_PrisonerService.GetActiveCase(new CaseCloseEntity(PrisonerID: Data.PrisonerID.Value));

                if (activeCaseList.Any())
                {
                    CaseCloseEntity activeCase = activeCaseList.First();

                    if (activeCase.PCN == Data.PCN)
                    {
                        activeCase.CloseTypeLibItemID = Data.CloseTypeLibItemID;
                        activeCase.CloseDate = Data.CloseDate;
                        activeCase.CommandNumber = Data.CommandNumber;
                        activeCase.CloseFromWhomID = Data.CloseFromWhomID;
                        activeCase.CourtDesicion = Data.CourtDesicion;
                        activeCase.Notes = Data.Notes;
                        activeCase.Suicide = Data.Suicide;

                        id = BusinessLayer_PrisonerService.AddCaseClose(activeCase);

                        if (id != null && Data.DataType != null)
                        {
                            bool? tempStatus = true;
                            if (Data.DataType.Value && Data.SentenceID != null)
                            {
                                tempStatus = BusinessLayer_PrisonerService.UpdateSentencingData(new SentencingDataEntity(ID: Data.SentenceID.Value, CloseCaseID: id.Value));
                            }
                            else if (Data.ArrestID != null)
                            {
                                tempStatus = BusinessLayer_PrisonerService.UpdateArrestData(new ArrestDataEntity(ID: Data.ArrestID.Value, CloseCaseID: id.Value));
                            }
                        }
                    }
                }

            }

            if (id != null) status = true;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Պահպանել  գործի փակումը", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status });
        }

        public ActionResult OpenTermination(CaseOpenEntity Data)
        {
            bool status = true;

            System.Web.HttpContext.Current.Session["CaseOpenEntity"] = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Բացել գործի փակում", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status });
        }

    }
}