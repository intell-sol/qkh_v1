﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.Transfers)]
    public class TransfersController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public TransfersController()
        {
            PermissionHCID = PermissionsHardCodedIds.Transfers;
        }
        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Տեղափոխումներ";

                TransferViewModel Model = new TransferViewModel();

                Model.FilterTransfer = new FilterTransferEntity();
                if ((System.Web.HttpContext.Current.Session["FilterTransfers"] as FilterTransferEntity) != null)
                    Model.FilterTransfer = (FilterTransferEntity)System.Web.HttpContext.Current.Session["FilterTransfers"];

                Model.PurposeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_PURPOSE);
                Model.TransferTypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_TYPE);
                Model.TransferLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER);
                Model.HospitalLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_HOSPITAL);
                Model.CourtLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_COURT);
                Model.StateLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_STATE);
                Model.EndLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_END);
                Model.EmergencyLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_EMERGENCY);
                Model.OrgUnitList = BusinessLayer_OrganizatonLayer.GetOrgUnitByTypeID(TypeID: 3);

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղափոխումներ"));

                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }
        public ActionResult Add()
        {
            return PartialView("Index");
        }
        [HttpPost]
        public ActionResult Edit(int? PrisonerID)
        {
            TransferViewModel Model = new TransferViewModel();
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            Model.Transfer = BusinessLayer_PrisonerService.GetTransfer(PrisonerID: PrisonerID.Value);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղափոխումներ խմբագրում (preview)", RequestData: PrisonerID.ToString()));

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;
            return PartialView("Add", Model);
        }

        public ActionResult Draw_ResetTransferTableRow()
        {
            Session["FilterTransfers"] = null;
            return Json(new { status = true });
        }
        public ActionResult Get_TransferTableRow(int? id = null)
        {
            // UNDONE: watch this section
            FilterTransferEntity FilterEntity = new FilterTransferEntity();
            if ((System.Web.HttpContext.Current.Session["FilterTransfers"] as FilterTransferEntity) != null)
                FilterEntity = (FilterTransferEntity)System.Web.HttpContext.Current.Session["FilterTransfers"];

            int page = id ?? 1;

            FilterEntity.paging = new FilterTransferEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<TransferDashboardEntity> ListDashboardTransfers = BusinessLayer_PrisonerService.GetTransfersForDashboard(FilterEntity);

            TransferViewModel Model = new TransferViewModel();

            Model.ListDashboardTransfer = ListDashboardTransfers;
            Model.TransferEntityPaging = FilterEntity.paging;
            Model.CurrentOrgUnitID = ViewBag.SelectedOrgUnitID;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղափոխումներ Դիտում"));

            return PartialView("Transfers_Table_Row", Model);
        }

        public ActionResult Get_TransferTableRowByFilter(FilterTransferEntity FilterData)
        {
            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterTransfers"] = FilterData;

            FilterData.paging = new FilterTransferEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<TransferDashboardEntity> EntityList = BusinessLayer_PrisonerService.GetTransfersForDashboard(FilterData);

            TransferViewModel Model = new TransferViewModel();

            Model.ListDashboardTransfer = EntityList;
            Model.TransferEntityPaging = FilterData.paging;
            Model.CurrentOrgUnitID = ViewBag.SelectedOrgUnitID;
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղափողման Որոնում", RequestData: JsonConvert.SerializeObject(FilterData)));
            return PartialView("Transfers_Table_Row", Model);
        }

        public ActionResult AddData(TransferEntity Item)
        {
            bool status = false;
            int? id = null;

            Item.ApproveStatus = null;
            Item.ApproverEmployeeID = null;

            if (Item != null)
            {
                Item.TransferTypeLibItemID = 1569;

                if (Item.Person != null)
                {

                    Item.Person = BusinessLayer_PrisonerService.AddPersonAndGet(Person: Item.Person);

                    Item.PersonID = Item.Person.ID;

                    if (Item.CurrentOrgUnit == null)
                    {
                        Item.CurrentOrgUnit = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Item.PrisonerID.Value).OrgUnitID;
                    }

                    if (Item.PrisonerID != null)
                    {
                        PrisonerEntity Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Item.PrisonerID.Value);

                        if (Prisoner.ArchiveData != null && Prisoner.ArchiveData.Value)
                        {
                            Item.StateLibItemID = 1552;
                            Item.ApproveStatus = true;
                            Item.ApproveDate = DateTime.Now;
                            Item.ApproverEmployeeID = ViewBag.UserID;
                        }
                    }

                    if (Item.PersonID != null)
                        id = BusinessLayer_PrisonerService.AddTransfer(Item);
                }
            }
            if (id != null)
            {
                status = true;
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղափողման ավելացում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }

        public ActionResult RemData(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetTransfer(new TransferEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                TransferEntity oldEntity = BusinessLayer_PrisonerService.GetTransfer(new TransferEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.TRANSFERS_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateTransfer(new TransferEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղափողման հեռացում", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult EditData(TransferEntity Item)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetTransfer(new TransferEntity(ID: Item.ID)).First().PrisonerID;
            if (Item.StateLibItemID != null && Item.StateLibItemID == 1552)
            {
                Item.ApproverEmployeeID = ViewBag.UserID;
            }
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && PrisonerID.HasValue && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.TRANSFERS_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateTransfer(Item);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղափողման խմբագրում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult CompleteData(TransferEntity Item)
        {
            bool status = false;

            Item.StateLibItemID = 1552;
            if (Item.EndDate == null)
            {
                Item.EndDate = DateTime.Now;
            }

            status = BusinessLayer_PrisonerService.UpdateTransfer(Item);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղափողման հաստատում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }
        public ActionResult DeclineData(int? id)
        {
            bool status = false;

            if (id != null)
            {
                TransferEntity curEntity = new TransferEntity(ID: id.Value, ApproverEmployeeID: ViewBag.UserID);
                curEntity.ApproveStatus = false;
                curEntity.ApproveDate = DateTime.Now;

                status = BusinessLayer_PrisonerService.UpdateTransfer(curEntity);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղափողման մերժում", RequestData: id.ToString()));

            return Json(new { status = status });
        }
        public ActionResult ApproveData(int? id)
        {
            bool status = false;

            if (id != null)
            {
                TransferEntity curEntity = new TransferEntity(ID: id.Value, Status: true, ApproverEmployeeID: ViewBag.UserID);
                curEntity.ApproveStatus = true;
                curEntity.ApproveDate = DateTime.Now;

                status = BusinessLayer_PrisonerService.UpdateTransfer(curEntity);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տեղափողման հաստատում", RequestData: id.ToString()));

            return Json(new { status = status });
        }
    }
}