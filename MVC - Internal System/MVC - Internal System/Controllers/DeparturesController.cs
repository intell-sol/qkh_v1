﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.Departures)]
    public class DeparturesController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public DeparturesController()
        {
            PermissionHCID = PermissionsHardCodedIds.Departures;
        }
        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Մեկնումներ";

                DepartureViewModel Model = new DepartureViewModel();

                Model.FilterDeparture = new FilterDepartureEntity();
                if ((System.Web.HttpContext.Current.Session["FilterDepartures"] as FilterDepartureEntity) != null)
                    Model.FilterDeparture = (FilterDepartureEntity)System.Web.HttpContext.Current.Session["FilterDepartures"];

                Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_TYPE);
                Model.PurposeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_PURPOSE);
                Model.MedLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_MED);
                Model.CaseLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_CASE);
                Model.MoveLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_MOVE);
                Model.InvestigateLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_INVESTIGATE);
                Model.CourtLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_COURT);
                Model.OrgUnitList = BusinessLayer_OrganizatonLayer.GetOrgUnitByTypeID(TypeID: 3);
                Model.HospitalLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_HOSPITAL);

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Մեկնումներ"));

                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել մեկնումներ"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }
        [HttpPost]
        public ActionResult Edit(int? PrisonerID)
        {
            DepartureViewModel Model = new DepartureViewModel();
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            Model.Departure = BusinessLayer_PrisonerService.GetDeparture(PrisonerID: PrisonerID.Value);


            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել մեկնումներ (preview)", RequestData: PrisonerID.ToString()));

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;
            return PartialView("Add", Model);
        }
        public ActionResult Draw_ResetDepartureTableRow()
        {
            Session["FilterDepartures"] = null;
            return Json(new { status = true });
        }
        public ActionResult Get_DepartureTableRow(int? id = null)
        {
            // UNDONE: watch this section
            FilterDepartureEntity FilterEntity = new FilterDepartureEntity();
            if ((System.Web.HttpContext.Current.Session["FilterDepartures"] as FilterDepartureEntity) != null)
                FilterEntity = (FilterDepartureEntity)System.Web.HttpContext.Current.Session["FilterDepartures"];

            int page = id ?? 1;


            FilterEntity.paging = new FilterDepartureEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<DepartureDashboardEntity> ListDashboardDepartures = BusinessLayer_PrisonerService.GetDeparturesForDashboard(FilterEntity);

            DepartureViewModel Model = new DepartureViewModel();

            Model.ListDashboardDeparture = ListDashboardDepartures;
            Model.DepartureEntityPaging = FilterEntity.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել մեկնումները"));

            return PartialView("Departures_Table_Row", Model);
        }

        public ActionResult Get_DepartureTableRowByFilter(FilterDepartureEntity FilterData)
        {
            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterDepartures"] = FilterData;

            FilterData.paging = new FilterDepartureEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<DepartureDashboardEntity> EntityList = BusinessLayer_PrisonerService.GetDeparturesForDashboard(FilterData);

            DepartureViewModel Model = new DepartureViewModel();

            Model.ListDashboardDeparture = EntityList;
            Model.DepartureEntityPaging = FilterData.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Մեկնումի Որոնում", RequestData: JsonConvert.SerializeObject(FilterData)));
            return PartialView("Departures_Table_Row", Model);
        }

        public ActionResult AddData(DepartureEntity Item)
        {
            bool status = false;
            int? id = null;

            Item.State = null;
            Item.ApproverEmployeeID = null;

            if (Item != null)
            {
                PrisonerEntity Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Item.PrisonerID.Value);

                if (Prisoner.ArchiveData != null && Prisoner.ArchiveData.Value)
                {
                    Item.State = true;
                    Item.ApproverEmployeeID = ViewBag.UserID;
                }

                id = BusinessLayer_PrisonerService.AddDeparture(Item);
            }
            if (id != null)
            {
                status = true;
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել մեկնում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }

        public ActionResult RemData(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetDeparture(new DepartureEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                DepartureEntity oldEntity = BusinessLayer_PrisonerService.GetDeparture(new DepartureEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.DEPARTURES_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateDeparture(new DepartureEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել մեկնում", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult EditData(DepartureEntity Item)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetDeparture(new DepartureEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && PrisonerID.HasValue && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.DEPARTURES_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateDeparture(Item);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել մեկնում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult CompleteData(DepartureEntity Item)
        {
            bool status = false;

            if (Item.ID != null && Item.EndDate != null)
            {
                Item.ApproverEmployeeID = ViewBag.UserID;
                status = BusinessLayer_PrisonerService.UpdateDeparture(Item);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հաստատել մեկնում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }
        public ActionResult DeclineData(int? id)
        {
            bool status = false;

            if (id != null)
            {
                DepartureEntity curEntity = new DepartureEntity(ID: id.Value, State: false, ApproverEmployeeID: ViewBag.UserID);

                status = BusinessLayer_PrisonerService.UpdateDeparture(curEntity);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Մերժել մեկնում", RequestData: id.ToString()));

            return Json(new { status = status });
        }
        public ActionResult ApproveData(int? id)
        {
            bool status = false;

            if (id != null)
            {
                DepartureEntity curEntity = new DepartureEntity(ID: id.Value, State: true, Status: true, ApproverEmployeeID: ViewBag.UserID);

                status = BusinessLayer_PrisonerService.UpdateDeparture(curEntity);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հաստատել մեկնում", RequestData: id.ToString()));

            return Json(new { status = status });
        }
    }
}