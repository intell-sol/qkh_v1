﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{

    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.Prisoners)]
    public class PrisonersController : BaseController
    {
        //public BL_PrisonersLayer BusinessLayer_PrisonerService = new BL_PrisonersLayer();

        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public PrisonersController()
        {
            PermissionHCID = PermissionsHardCodedIds.Prisoners;
        }

        /// <summary>
        /// Main View for convicts
        /// </summary>
        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Կալանավորներ";

                PrisonersViewModel Model = new PrisonersViewModel();

                Model.Gender = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);

                Model.Citizenships = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);

                Model.Nationality = BusinessLayer_LibsLayer.GetNationality();
                Model.FilterPrisoner = new FilterPrisonerEntity();
                if ((System.Web.HttpContext.Current.Session["FilterConvicts"] as FilterPrisonerEntity) != null)
                    Model.FilterPrisoner = (FilterPrisonerEntity)System.Web.HttpContext.Current.Session["FilterConvicts"];

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Կալանավորներ"));

                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        /// <summary>
        /// Add View for convicts
        /// </summary>
        public ActionResult Add()
        {
            try
            {
                // page title
                ViewBag.Title = "Ավելացնել Կալանավոր";

                PrisonersViewModel Model = new PrisonersViewModel();
                System.Web.HttpContext.Current.Session["ViewMode"] = 1;

                Model.Prisoner = new PrisonerEntity();
                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել Կալանավոր"));

                return View("Add", Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        /// <summary>
        /// EditView for convicts
        /// </summary>

        public ActionResult Edit()
        {
            try
            {
                int id = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 0;
                bool error = false;
                if (id < 1)
                    error = true;
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
                PrisonerEntity prisoner = BusinessLayer_PrisonerService.GetPrisoner(id, true);
                if (prisoner.OrgUnitID.Value != User.SelectedOrgUnitID)
                    error = true;
                if (error)
                    return RedirectToAction("index");
                // check view
                if (!prisoner.ArchiveStatus.Value)
                {
                    return RedirectToAction("Preview", "Convicts", new { id = id });
                }
                // page title
                ViewBag.Title = "Խմբագրել Կալանավոր";
                System.Web.HttpContext.Current.Session["ViewMode"] = 1;

                PrisonersViewModel Model = new PrisonersViewModel();

                Model.Prisoner = prisoner;
                MergeObjectEntity mergeObject = BusinessLayer_MergeLayer.GetPrisonerActiveModifications(PrisonerID: id);
                Model.MergeStatus = (mergeObject.Merge != null && mergeObject.Merge.Any()) ? true : false;



                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել Կալանավոր", RequestData: id.ToString()));

                return View("Add", Model);


            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        public ActionResult Preview()
        {
            try
            {
                int id = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 0;
                bool error = false;
                if (id < 1)
                    error = true;
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
                PrisonerEntity prisoner = BusinessLayer_PrisonerService.GetPrisoner(id, true);
                if (prisoner.OrgUnitID.Value != User.SelectedOrgUnitID)
                    error = true;
                if (error)
                    return RedirectToAction("index");

                // page title
                ViewBag.Title = "Դիտել Կալանավոր";
                System.Web.HttpContext.Current.Session["ViewMode"] = 0;

                PrisonersViewModel Model = new PrisonersViewModel();

                Model.Prisoner = prisoner;
                //Model.Prisoner.Person = BusinessLayer_PrisonerService.GetPerson(Model.Prisoner.PersonID);

                if (!prisoner.ArchiveStatus.Value)
                {
                    CaseCloseEntity tempEntity = BusinessLayer_PrisonerService.GetCaseClose(new CaseCloseEntity(PrisonerID: Model.Prisoner.ID.Value)).First();

                    LibsEntity tempCurEntity = BusinessLayer_LibsLayer.GetLibByID(LibID: tempEntity.CloseTypeLibItemID.Value);

                    List<PropsEntity> curEntity = BusinessLayer_PropsLayer.GetPropsList(LibID: tempEntity.CloseTypeLibItemID.Value);

                    if (curEntity != null && curEntity.FindAll(r => r.ID == 5).Any() && curEntity.FindAll(r => r.ID == 5).Last().Name == "Ոչ")
                        Model.Prisoner.ArchiveStatus = true;

                }

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել Կալանավոր", RequestData: id.ToString()));

                return View("Add", Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        public ActionResult Draw_ResetConvictsTableRows()
        {
            Session["FilterPrisoner"] = null;
            return Json(new { status = true });
        }

        public ActionResult Draw_ConvictsTableRows()
        {

            FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            if ((System.Web.HttpContext.Current.Session["FilterPrisoner"] as FilterPrisonerEntity) != null)
                FilterPrisoner = (FilterPrisonerEntity)System.Web.HttpContext.Current.Session["FilterPrisoner"];

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;


            FilterPrisoner.Type = 3;
            FilterPrisoner.paging = new FilterPrisonerEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PrisonerEntity> PrisonersList = BusinessLayer_PrisonerService.GetPrisonersForDashboard(FilterPrisoner);

            PrisonersViewModel Model = new PrisonersViewModel();

            Model.ListPrisoners = PrisonersList;
            Model.PrisonerEntityPaging = FilterPrisoner.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Կալանավորների Դիտում"));

            return PartialView("Convict_Table_Row", Model);
        }

        [HttpPost]
        public ActionResult Draw_ConvictsTableRowsByFilter(FilterPrisonerEntity FilterPrisoner)
        {

            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterPrisoner"] = FilterPrisoner;

            FilterPrisoner.paging = new FilterPrisonerEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PrisonerEntity> PrisonersList = BusinessLayer_PrisonerService.GetPrisonersForDashboard(FilterPrisoner);

            PrisonersViewModel Model = new PrisonersViewModel();

            Model.ListPrisoners = PrisonersList;
            Model.PrisonerEntityPaging = FilterPrisoner.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Կալանավորներ Որոնում", RequestData: JsonConvert.SerializeObject(FilterPrisoner)));
            return PartialView("Convict_Table_Row", Model);
        }
        
        public ActionResult GetPreview(int PrisonerID)
        {
            PreviewPrisonerViewModel Model = new PreviewPrisonerViewModel();

            Model.PrisonerID = PrisonerID;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Կալանավոր Դիտում (Preview)", RequestData: PrisonerID.ToString()));

            Model.canApprove = BusinessLayer_PrisonerService.GetActiveCase(new CaseCloseEntity(PrisonerID: PrisonerID)).Any();

            return PartialView("PreviewPrisoner", Model);
        }

        public ActionResult ApprovePrisoner(int PrisonerID)
        {
            bool status = false;

            if (BusinessLayer_PrisonerService.GetActiveCase(new CaseCloseEntity(PrisonerID: PrisonerID)).Any())
            {
                status = BusinessLayer_PrisonerService.UpdatePrisoner(new PrisonerEntity(ID: PrisonerID, State: true));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Կալանավոր Հաստատում", RequestData: PrisonerID.ToString()));

            return Json(new { status = status });
        }

    }
}