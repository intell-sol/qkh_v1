﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.CameraCard)]
    public class _Data_Cell_Controller : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public _Data_Cell_Controller()
        {
            PermissionHCID = PermissionsHardCodedIds.CameraCard;
        }

        public ActionResult GetConflictPersons(int PrisonerID, int? Number)
        {
            bool? status = false;
            status = BusinessLayer_PrisonerService.CheckConflictPersons(PrisonerID, Number);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խցիկի հեռացում", RequestData: JsonConvert.SerializeObject(new { PrisonerID = PrisonerID, Number = Number })));

            return Json(new { status = status });
        }
        
        /// <summary>
        /// Primary data add partial view
        /// </summary>
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել խցիկ (preview)"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }
        
        /// <summary>
        /// Primary data edit partial view
        /// </summary>
        [HttpPost]
        public ActionResult Edit(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;

            Model.Prisoner.CameraCard = BusinessLayer_PrisonerService.GetCamerCards(new CameraCardEntity(PrisonerID: PrisonerID.Value));
            if (Model.Prisoner.CameraCard != null && Model.Prisoner.CameraCard.CameraCardItem != null && Model.Prisoner.CameraCard.CameraCardItem.Any())
                Model.Prisoner.CameraCard.CameraCardItem.Last().Conflicted = BusinessLayer_PrisonerService.CheckConflictPersons(Model.Prisoner.ID.Value, Model.Prisoner.CameraCard.CameraCardItem.Last().Number);
            //Model.ConflictPersons=BusinessLayer_PrisonerService.CheckConflictPersons(Model.Prisoner.ID.Value, ((Model.Prisoner.CameraCard != null && Model.Prisoner.CameraCard.CameraCardItem !=null && Model.Prisoner.CameraCard.CameraCardItem.Any())?Model.Prisoner.CameraCard.CameraCardItem.Last().Number:null));
            Model.EmployeerList = BusinessLayer_OrganizatonLayer.GetEmployees(ViewBag.SelectedOrgUnitID);
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել խցիկը",RequestData: PrisonerID.ToString()));

            return PartialView("Index", Model);
        }

        [ReadWrite]
        public ActionResult AddCell(CameraCardItemEntity Item)
        {
            bool status = false;
            int? id = null;

            if (Item.PrisonerID != null)
            {
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
                Item.OrgUnitID = User.SelectedOrgUnitID;
                //if (ViewBag.PPType == 2)
                //{
                //    MergeEntity curEntity = new MergeEntity(PrisonerID: Item.PrisonerID.Value,
                //        EmployeeID: ViewBag.UserID,
                //        Json: JsonConvert.SerializeObject(Item),
                //        Type: 1,
                //        EID: (int)MergeEntity.TableIDS.CELL_MAIN);

                //    BusinessLayer_MergeLayer.AddModification(curEntity);
                //}
                //else
                //{
                    id = BusinessLayer_PrisonerService.AddCameraCardItem(Item);
                //}
            }

            if (id != null) status = true;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել խցիկ", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }

        [ReadWrite]
        public ActionResult RemCell(int ID)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetCameraCardItems(new CameraCardItemEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(new CameraCardItemEntity(ID: ID, Status: false)),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.CELL_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateCameraCard(new CameraCardItemEntity(ID: ID, Status: false));
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խցիկի հեռացում", RequestData: ID.ToString() ));

            return Json(new { status = status, needApprove = needApprove });
        }
        
        [ReadWrite]
        public ActionResult EditCell(CameraCardItemEntity Item)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetCameraCardItems(new CameraCardItemEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.CELL_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateCameraCard(Item);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խցիկի խմբագրում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status, needApprove = needApprove });
        }
    }
}