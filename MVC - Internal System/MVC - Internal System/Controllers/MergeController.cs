﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.Merge)]
    public class MergeController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public MergeController()
        {
            PermissionHCID = PermissionsHardCodedIds.Merge;
        }

        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Հաստատումներ";

                MergeViewModel Model = new MergeViewModel();
             
                Model.FilterMerge = new FilterMergeEntity();
                if ((System.Web.HttpContext.Current.Session["FilterMerge"] as FilterMergeEntity) != null)
                    Model.FilterMerge = (FilterMergeEntity)System.Web.HttpContext.Current.Session["FilterMerge"];

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հաստատումներ(Ընդհանուր էջ)"));

                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        [HttpGet]
        public ActionResult Edit(int? ID)
        {
            MergeViewModel Model = new MergeViewModel();

            Model.Merge = BusinessLayer_MergeLayer.GetPrisonerActiveModifications(ID.Value);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հաստատման խմբագրում", RequestData: ID.ToString()));

            return PartialView("Add", Model);
        }

        public ActionResult Draw_MergeTableRow()
        {

            //List<MergeObjectEntity> list = BusinessLayer_MergeLayer.GetModificationList();

            // UNDONE: watch this section
            FilterMergeEntity FilterMerge = new FilterMergeEntity();
            if ((System.Web.HttpContext.Current.Session["FilterMerge"] as FilterMergeEntity) != null)
                FilterMerge = (FilterMergeEntity)System.Web.HttpContext.Current.Session["FilterMerge"];

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            FilterMerge.paging = new FilterMergeEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<MergeDashboardEntity> ListDashboardMerge = BusinessLayer_MergeLayer.GetGroupedMergeForDashboard(FilterMerge);

            MergeViewModel Model = new MergeViewModel();

            Model.ListDashboardMerge = ListDashboardMerge;
            Model.MergeEntityPaging = FilterMerge.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հաստատումների դիտում"));

            return PartialView("Merge_Table_Row", Model);
        }

        public ActionResult Draw_MergeTableRowByFilter(FilterMergeEntity FilterData)
        {
            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterMerge"] = FilterData;

            FilterData.paging = new FilterMergeEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<MergeDashboardEntity> ModificationList = BusinessLayer_MergeLayer.GetGroupedMergeForDashboard(FilterData);

            MergeViewModel Model = new MergeViewModel();

            Model.ListDashboardMerge = ModificationList;
            Model.MergeEntityPaging= FilterData.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հաստատման Որոնում", RequestData: JsonConvert.SerializeObject(FilterData)));
            return PartialView("Merge_Table_Row", Model);
        }

        [HttpGet]
        public ActionResult Prisoner(int ID)
        {
            try
            {
                // page title
                ViewBag.Title = "Հաստատումներ";

                MergeViewModel Model = new MergeViewModel();

                Model.FilterMerge = new FilterMergeEntity();
                if ((System.Web.HttpContext.Current.Session["FilterMerge"] as FilterMergeEntity) != null)
                    Model.FilterMerge = (FilterMergeEntity)System.Web.HttpContext.Current.Session["FilterMerge"];
                Model.FilterMerge.PrisonerID = ID;
                Model.Person = BusinessLayer_PrisonerService.GetPersonByPrisonerID(PrisonerID: ID);
                if (Model.Person != null && Model.Person.ID != null)
                {
                    List<AdditionalFileEntity> files = BusinessLayer_PrisonerService.GetFiles(PersonID: Model.Person.ID, TypeID: FileType.MAIN_DATA_AVATAR_IMAGES, PrisonerID: ID);
                    if (files.Any())
                    {
                        Model.Person.PhotoLink = BL_Files.getInstance().GetLink(files.First().ID, files.First().FileName);
                    }
                    else
                    {
                        Model.Person.PhotoLink = BL_Files.getInstance().getDefaultPhotoLink();
                    }
                }

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հաստատումներ(Դատապարտյալի էջ)"));

                return View(Model);
            }
            catch (Exception e)
            {
                return View("Error", e);
            }
        }
        
        public ActionResult Draw_MergePrisonerTableRow(FilterMergeEntity FilterData)
        {
            // UNDONE: watch this section
            FilterMergeEntity FilterMerge = new FilterMergeEntity();
            if ((System.Web.HttpContext.Current.Session["FilterMerge"] as FilterMergeEntity) != null)
                FilterMerge = (FilterMergeEntity)System.Web.HttpContext.Current.Session["FilterMerge"];

            int page = RouteData.Values.ContainsKey("page") ? int.Parse(RouteData.Values["page"].ToString()) : 1;
            // TODO: 
            if (FilterData.PrisonerID == null) FilterData.PrisonerID = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            FilterMerge.paging = new FilterMergeEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());
            FilterMerge.PrisonerID = FilterData.PrisonerID;

            List<MergeDashboardEntity> ListDashboardMerge = BusinessLayer_MergeLayer.GetMergeForDashboard(FilterMerge);

            MergeViewModel Model = new MergeViewModel();

            Model.PrisonerID = FilterMerge.PrisonerID.Value;
            Model.ListDashboardMerge = ListDashboardMerge;
            Model.MergeEntityPaging = FilterMerge.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հաստատումների դիտում"));

            return PartialView("Merge_Table_Row_Prisoner", Model);
        }
        
        [HttpPost]
        [ReadWrite]
        [ActionName("ApproveData")]
        public ActionResult ApproveData(int ID, bool State)
        {
            try
            {
                bool status = false;

                List<MergeEntity> MEList = BusinessLayer_MergeLayer.GetModificationInfo(new MergeEntity(ID: ID, Status: true));
                if(MEList.Any())
                {
                    MergeEntity entity = MEList.First();
                    entity.ApproverID = (int)ViewBag.UserID;
                    if (State)
                    {
                        status = BusinessLayer_MergeLayer.ApproveModification(entity);
                    }
                    else
                    {
                        status = BusinessLayer_MergeLayer.CancelModification(entity);
                    }
                }
                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հաստատման հաստատում", RequestData: JsonConvert.SerializeObject(new { })));

                return Json(new { status = status });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Json(new { status = false });
            }
        }

    }
}