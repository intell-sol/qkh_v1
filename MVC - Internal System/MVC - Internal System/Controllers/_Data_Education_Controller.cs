﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using MVC___Internal_System.Attributes;
using Newtonsoft.Json;
using BusinessLayer.BusinessServices;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.EducationsProfessions)]
    public class _Data_Education_Controller : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public _Data_Education_Controller()
        {
            PermissionHCID = PermissionsHardCodedIds.EducationsProfessions;
        }
        /// <summary>
        /// _Data_Education_ partial view
        /// </summary>
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել ուսում"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }

        public ActionResult Edit(int PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID;

            EducationsProfessionsEntity curEntity = new EducationsProfessionsEntity();

            curEntity.PrisonerID = PrisonerID;

            Model.Prisoner.EducationsProfessions = BusinessLayer_PrisonerService.GetEducationsProfessions(curEntity, true);

            Model.Education = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.EDUCATION);
            Model.Preferance = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PREFERANCE);
            Model.Languages = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.LAGNUAGES);
            Model.Profession = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PROFESSION);
            Model.Abilities = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ABILITIES);
            Model.AcademicDegree = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ACADEMIC_DEGREE);
            Model.ScienceDegree = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.SCIENCE_DEGREE);
            Model.OfficialGifts = BusinessLayer_LibsLayer.GetLibsLocalTreeByLibPathID((int)LibsEntity.LIBS.OFFICIAL_GIFTS);
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել ուսում", RequestData: PrisonerID.ToString()));

            return PartialView("Index", Model);
        }

        [HttpPost]
        public ActionResult SaveContent(EducationsProfessionsEntity Entity)
        {
            bool status = false;
            bool needApprove = false;

            if (Entity.PrisonerID != null)
            {
                EducationsProfessionsEntity oldEntity = BusinessLayer_PrisonerService.GetEducationsProfessions(Entity, true);
                PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(oldEntity.PrisonerID.Value, false);
                if (oldEntity.PrisonerID != null)
                {
                    if (ViewBag.PPType == 2 && curPrisoner.State.Value)
                    {
                        Entity.PrisonerID = oldEntity.PrisonerID;
                        Entity.ID = oldEntity.ID;
                        MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                            EmployeeID: ViewBag.UserID,
                            Json: JsonConvert.SerializeObject(Entity),
                            Type: 3,
                            EntityID: Entity.ID,
                            EID: (int)MergeEntity.TableIDS.EDUCATION);

                        int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                        needApprove = true;

                        status = tempID != null;
                    }
                    else
                    {
                        bool tempStatusProf = BusinessLayer_PrisonerService.UpdateEducationProfessions(Entity);

                        status = tempStatusProf;
                    }
                }
                else
                {
                    int? id = BusinessLayer_PrisonerService.AddEducationsProfessions(Entity);

                    if (id != null)
                    {
                        status = true;

                        // add profession
                        foreach (ProfessionEntity curEntity in Entity.Professions)
                        {
                            curEntity.EducationProfessionsID = id;
                            int? curID = BusinessLayer_PrisonerService.AddProfession(curEntity);
                            if (curID == null) status = false;
                        }


                        // add interest
                        foreach (InterestEntity curEntity in Entity.Interests)
                        {
                            curEntity.EducationProfessionsID = id;
                            int? curID = BusinessLayer_PrisonerService.AddInterest(curEntity);
                            if (curID == null) status = false;
                        }

                        // add languages
                        foreach (LanguageEntity curEntity in Entity.Languages)
                        {
                            curEntity.EducationProfessionsID = id;
                            int? curID = BusinessLayer_PrisonerService.AddLanguage(curEntity);
                            if (curID == null) status = false;
                        }

                        // add AbilitiesSkills
                        foreach (AbilitiesSkillsEntity curEntity in Entity.AbilitiesSkills)
                        {
                            curEntity.EducationProfessionsID = id;
                            int? curID = BusinessLayer_PrisonerService.AddAbilitiesSkills(curEntity);
                            if (curID == null) status = false;
                        }

                        // add UniversityDegree
                        if (Entity.LibItemID == 202)
                        {
                            Entity.UniversityDegree.EducationProfessionsID = id;
                            int? degreeID = BusinessLayer_PrisonerService.AddUniversityDegree(Entity.UniversityDegree);
                            if (degreeID == null) status = false;
                        }
                        if (Entity.UniversityDegree != null && Entity.UniversityDegree.Awardes != null)
                        {
                            bool statusAwards = BusinessLayer_PrisonerService.AddEducationAwards(Entity.UniversityDegree.Awardes);
                            if (statusAwards == false) status = false;
                        }
                    }
                }

            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Պահպանել ուսումը", RequestData: JsonConvert.SerializeObject(Entity)));

            return Json(new { status = status, needApprove = needApprove });
        }
    }
}