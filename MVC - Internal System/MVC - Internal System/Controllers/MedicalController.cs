﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.Medicals)]
    public class MedicalController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public MedicalController()
        {
            PermissionHCID = PermissionsHardCodedIds.Medicals;
        }
        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Բժշկական";

                MedicalViewModel Model = new MedicalViewModel();

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Բժշկական"));

                return View("Index", Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել Բժշկական"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }
        [HttpPost]
        public ActionResult Edit(int? PrisonerID, int? Type = 1)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.Medical = new MedicalObjectEntity();

            if (PrisonerID != null)
            {
                PrisonerEntity currentPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: PrisonerID.Value);

                Model.Medical.MedicalComplaints = BL_MedicalLayer.GetList_MedicalComplaints(new MedicalComplaintsEntity(PrisonerID: PrisonerID.Value));
                Model.Medical.MedicalExternalExaminations = BL_MedicalLayer.GetList_MedicalExternalExamination(new MedicalExternalExaminationEntity(PrisonerID: PrisonerID.Value));
                Model.Medical.MedicalDiagnosis = BL_MedicalLayer.GetList_MedicalDiagnosis(new MedicalDiagnosisEntity(PrisonerID: PrisonerID.Value));
                Model.Medical.Files = BusinessLayer_PrisonerService.GetFiles(PrisonerID: PrisonerID, TypeID: FileType.MEDICAL_PRELIMINARY);
            }

            if (Type == 1) Model.ActionName = "GetPreliminary";
            else if (Type == 2) Model.ActionName = "GetAmbulator";
            else if (Type == 3) Model.ActionName = "GetStationery";
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            Model.PrisonerID = PrisonerID;

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել Բժշկական", RequestData: PrisonerID.ToString()));

            return PartialView("Index", Model);
        }
        [HttpPost]
        public ActionResult GetPreliminary(int? PrisonerID)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.HealthLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.HEALTH_LIB);

            Model.Medical = new MedicalObjectEntity();

            if (PrisonerID != null)
            {
                List<MedicalPrimaryEntity> newList = BL_MedicalLayer.GetList_MedicalPrimary(new MedicalPrimaryEntity(PrisonerID: PrisonerID.Value));
                if (newList != null && newList.Any())
                {
                    Model.Medical.primaryEntity = newList.Last();
                }

                // medical data
                Model.Medical.MedicalComplaints = BL_MedicalLayer.GetList_MedicalComplaints(new MedicalComplaintsEntity(PrisonerID: PrisonerID.Value, MedicalHistoryID: 1));
                Model.Medical.MedicalDiagnosis = BL_MedicalLayer.GetList_MedicalDiagnosis(new MedicalDiagnosisEntity(PrisonerID: PrisonerID.Value, MedicalHistoryID: 1));
                Model.Medical.MedicalExternalExaminations = BL_MedicalLayer.GetList_MedicalExternalExamination(new MedicalExternalExaminationEntity(PrisonerID: PrisonerID.Value));
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել Բժշկական Հիմնական", RequestData: PrisonerID.ToString()));

            return PartialView("Preliminary", Model);
        }
        [HttpPost]
        public ActionResult GetAmbulator(int? PrisonerID)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.Medical = new MedicalObjectEntity();

            if (PrisonerID != null)
            {
                Model.Medical.MedicalHistories = BL_MedicalLayer.GetList_MedicalHistory(new MedicalHistoryEntity(PrisonerID: PrisonerID.Value, Type: 2));
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել Բժշկական", RequestData: PrisonerID.ToString()));

            return PartialView("Ambulator", Model);
        }
        [HttpPost]
        public ActionResult GetStationery(int? PrisonerID)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.Medical = new MedicalObjectEntity();

            if (PrisonerID != null)
            {
                Model.Medical.MedicalHistories = BL_MedicalLayer.GetList_MedicalHistory(new MedicalHistoryEntity(PrisonerID: PrisonerID.Value, Type: 3));
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել Բժշկական", RequestData: PrisonerID.ToString()));

            return PartialView("Stationery", Model);
        }

        public ActionResult GetHistoryAmbulator(MedicalHistoryEntity entity)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.MedicalHistory = new MedicalHistoryObjectEntity();

            if (entity.ID != null)
            {
                Model.MedicalHistory.currentMedicalHistory = BL_MedicalLayer.GetList_MedicalHistory(new MedicalHistoryEntity(ID: entity.ID)).First();
                Model.MedicalHistory.MedicalComplaints = BL_MedicalLayer.GetList_MedicalComplaints(new MedicalComplaintsEntity(MedicalHistoryID: entity.ID));
                Model.MedicalHistory.MedicalResearch = BL_MedicalLayer.GetList_MedicalResearch(new MedicalResearchEntity(MedicalHistoryID: entity.ID));
                Model.MedicalHistory.MedicalDiagnosis = BL_MedicalLayer.GetList_MedicalDiagnosis(new MedicalDiagnosisEntity(MedicalHistoryID: entity.ID));
                Model.MedicalHistory.MedicalHealing = BL_MedicalLayer.GetList_MedicalMedicalHealing(new MedicalHealingEntity(MedicalHistoryID: entity.ID));
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել Բժշկական", RequestData: entity.ID.ToString()));

            return PartialView("Ambulator_Page", Model);
        }
        public ActionResult GetHistoryStatonary(MedicalHistoryEntity entity)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.MedicalHistory = new MedicalHistoryObjectEntity();

            if (entity.ID != null)
            {
                Model.MedicalHistory.currentMedicalHistory = BL_MedicalLayer.GetList_MedicalHistory(new MedicalHistoryEntity(ID: entity.ID)).First();
                Model.MedicalHistory.MedicalComplaints = BL_MedicalLayer.GetList_MedicalComplaints(new MedicalComplaintsEntity(MedicalHistoryID: entity.ID));
                Model.MedicalHistory.MedicalResearch = BL_MedicalLayer.GetList_MedicalResearch(new MedicalResearchEntity(MedicalHistoryID: entity.ID));
                Model.MedicalHistory.MedicalDiagnosis = BL_MedicalLayer.GetList_MedicalDiagnosis(new MedicalDiagnosisEntity(MedicalHistoryID: entity.ID));
                Model.MedicalHistory.MedicalHealing = BL_MedicalLayer.GetList_MedicalMedicalHealing(new MedicalHealingEntity(MedicalHistoryID: entity.ID));
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել Բժշկական", RequestData: entity.ID.ToString()));

            return PartialView("Stationery_Page", Model);
        }
        public ActionResult AddComplaintsData(MedicalComplaintsEntity Item)
        {
            bool status = true;

            int? id = BL_MedicalLayer.AddComplaints(Item);

            if (id == null) status = false;

            return Json(new { status = status });
        }
        public ActionResult EditComplaintsData(MedicalComplaintsEntity Item)
        {
            bool? status = true;
            bool needApprove = false;
            int? PrisonerID = BL_MedicalLayer.GetList_MedicalComplaints(new MedicalComplaintsEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MedicalComplaintsEntity oldEntity = BL_MedicalLayer.GetList_MedicalComplaints(new MedicalComplaintsEntity(ID: Item.ID)).First();

                Item.PrisonerID = oldEntity.PrisonerID;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.MEDICAL_PRELIMINARY_COMPLATIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BL_MedicalLayer.UpdateMedicalComplaints(Item);
            }

            if (status == null) status = false;

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult RemComplaintsData(int id)
        {
            bool? status = true;
            bool needApprove = false;

            int? PrisonerID = BL_MedicalLayer.GetList_MedicalComplaints(new MedicalComplaintsEntity(ID: id)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MedicalComplaintsEntity oldEntity = BL_MedicalLayer.GetList_MedicalComplaints(new MedicalComplaintsEntity(ID: id)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: id,
                    EID: (int)MergeEntity.TableIDS.MEDICAL_PRELIMINARY_COMPLATIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                MedicalComplaintsEntity Item = new MedicalComplaintsEntity(ID: id, Status: false);

                status = BL_MedicalLayer.UpdateMedicalComplaints(Item);
            }
            if (status == null) status = false;

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult AddExternalExaminationsData(MedicalExternalExaminationEntity Item)
        {
            bool status = true;

            int? id = BL_MedicalLayer.AddExternalExamination(Item);

            if (id == null) status = false;

            return Json(new { status = status });
        }
        public ActionResult EditExternalExaminationsData(MedicalExternalExaminationEntity Item)
        {
            bool? status = true;
            bool needApprove = false;
            int? PrisonerID = BL_MedicalLayer.GetList_MedicalExternalExamination(new MedicalExternalExaminationEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MedicalExternalExaminationEntity oldEntity = BL_MedicalLayer.GetList_MedicalExternalExamination(new MedicalExternalExaminationEntity(ID: Item.ID)).First();

                Item.PrisonerID = oldEntity.PrisonerID;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.MEDICAL_PRELIMINARY_EXTERNAL_EXAMINATION);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BL_MedicalLayer.UpdateMedicalExternalExamination(Item);
            }
            if (status == null) status = false;

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult RemExternalExaminationsData(int id)
        {
            bool? status = true;
            bool needApprove = false;

            int? PrisonerID = BL_MedicalLayer.GetList_MedicalExternalExamination(new MedicalExternalExaminationEntity(ID: id)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MedicalExternalExaminationEntity oldEntity = BL_MedicalLayer.GetList_MedicalExternalExamination(new MedicalExternalExaminationEntity(ID: id)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: id,
                    EID: (int)MergeEntity.TableIDS.MEDICAL_PRELIMINARY_EXTERNAL_EXAMINATION);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                MedicalExternalExaminationEntity Item = new MedicalExternalExaminationEntity(ID: id, Status: false);

                status = BL_MedicalLayer.UpdateMedicalExternalExamination(Item);
            }
            if (status == null) status = false;

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult AddDiagnosisData(MedicalDiagnosisEntity Item)
        {
            bool status = true;

            int? id = BL_MedicalLayer.AddDiagnosis(Item);

            if (id == null) status = false;

            return Json(new { status = status });
        }
        public ActionResult EditDiagnosisData(MedicalDiagnosisEntity Item)
        {
            bool? status = true;
            bool needApprove = false;
            int? PrisonerID = BL_MedicalLayer.GetList_MedicalDiagnosis(new MedicalDiagnosisEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MedicalDiagnosisEntity oldEntity = BL_MedicalLayer.GetList_MedicalDiagnosis(new MedicalDiagnosisEntity(ID: Item.ID)).First();

                Item.PrisonerID = oldEntity.PrisonerID;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.MEDICAL_PRELIMINARY_DIAGNOISIS);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BL_MedicalLayer.UpdateMedicalDiagnosis(Item);
            }
            if (status == null) status = false;

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult RemDiagnosisData(int id)
        {
            bool? status = true;
            bool needApprove = false;

            int? PrisonerID = BL_MedicalLayer.GetList_MedicalDiagnosis(new MedicalDiagnosisEntity(ID: id)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MedicalDiagnosisEntity oldEntity = BL_MedicalLayer.GetList_MedicalDiagnosis(new MedicalDiagnosisEntity(ID: id)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: id,
                    EID: (int)MergeEntity.TableIDS.MEDICAL_PRELIMINARY_DIAGNOISIS);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                MedicalDiagnosisEntity Item = new MedicalDiagnosisEntity(ID: id, Status: false);

                status = BL_MedicalLayer.UpdateMedicalDiagnosis(Item);
            }
            if (status == null) status = false;

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult AddHistoryData(MedicalHistoryEntity Item)
        {
            bool status = true;

            int? id = BL_MedicalLayer.AddHistory(Item);

            if (id == null) status = false;

            return Json(new { status = status });
        }

        public ActionResult EditHistoryData(MedicalHistoryEntity Item)
        {
            bool? status = true;
            bool needApprove = false;
            int? PrisonerID = BL_MedicalLayer.GetList_MedicalHistory(new MedicalHistoryEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MedicalHistoryEntity oldEntity = BL_MedicalLayer.GetList_MedicalHistory(new MedicalHistoryEntity(ID: Item.ID)).First();

                Item.PrisonerID = oldEntity.PrisonerID;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.MEDICAL_AMBULATOR_HISTORY);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BL_MedicalLayer.UpdateMedicalHistory(Item);
            }

            if (status == null) status = false;

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult RemHistoryData(int id)
        {
            bool? status = true;
            bool needApprove = false;

            int? PrisonerID = BL_MedicalLayer.GetList_MedicalHistory(new MedicalHistoryEntity(ID: id)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MedicalHistoryEntity oldEntity = BL_MedicalLayer.GetList_MedicalHistory(new MedicalHistoryEntity(ID: id)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: id,
                    EID: (int)MergeEntity.TableIDS.MEDICAL_AMBULATOR_HISTORY);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                MedicalHistoryEntity Item = new MedicalHistoryEntity(ID: id, Status: false);

                status = BL_MedicalLayer.UpdateMedicalHistory(Item);
            }
            if (status == null) status = false;

            return Json(new { status = status, needApprove = needApprove });
        }


        public ActionResult AddResearchData(MedicalResearchEntity Item)
        {
            bool status = true;

            int? id = BL_MedicalLayer.AddResearch(Item);

            if (id == null) status = false;

            return Json(new { status = status });
        }
        public ActionResult EditResearchData(MedicalResearchEntity Item)
        {
            bool? status = true;
            bool needApprove = false;
            int? PrisonerID = BL_MedicalLayer.GetList_MedicalResearch(new MedicalResearchEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MedicalResearchEntity oldEntity = BL_MedicalLayer.GetList_MedicalResearch(new MedicalResearchEntity(ID: Item.ID)).First();

                Item.PrisonerID = oldEntity.PrisonerID;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.MEDICAL_AMBULATOR_HISTORY_RESEARCH);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BL_MedicalLayer.UpdateMedicalResearch(Item);
            }
            if (status == null) status = false;

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult RemResearchData(int id)
        {
            bool? status = true;
            bool needApprove = false;

            int? PrisonerID = BL_MedicalLayer.GetList_MedicalResearch(new MedicalResearchEntity(ID: id)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MedicalResearchEntity oldEntity = BL_MedicalLayer.GetList_MedicalResearch(new MedicalResearchEntity(ID: id)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: id,
                    EID: (int)MergeEntity.TableIDS.MEDICAL_AMBULATOR_HISTORY_RESEARCH);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                MedicalResearchEntity Item = new MedicalResearchEntity(ID: id, Status: false);

                status = BL_MedicalLayer.UpdateMedicalResearch(Item);
            }
            if (status == null) status = false;

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult AddHealingData(MedicalHealingEntity Item)
        {
            bool status = true;

            int? id = BL_MedicalLayer.AddHealing(Item);

            if (id == null) status = false;

            return Json(new { status = status });
        }
        public ActionResult EditHealingData(MedicalHealingEntity Item)
        {
            bool? status = true;

            status = BL_MedicalLayer.UpdateMedicalHealing(Item);

            if (status == null) status = false;

            return Json(new { status = status});
        }
        public ActionResult RemHealingData(int id)
        {
            bool? status = true;

            MedicalHealingEntity Item = new MedicalHealingEntity(ID: id, Status: false);

            status = BL_MedicalLayer.UpdateMedicalHealing(Item);

            if (status == null) status = false;

            return Json(new { status = status });
        }

        public ActionResult EditHealthData(MedicalPrimaryEntity Item)
        {
            bool? status = null;

            bool needApprove = false;

            List<MedicalPrimaryEntity> newList = BL_MedicalLayer.GetList_MedicalPrimary(new MedicalPrimaryEntity(ID: Item.ID));
            int? PrisonerID = BL_MedicalLayer.GetList_MedicalPrimary(new MedicalPrimaryEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MedicalPrimaryEntity oldEntity = newList.Last();

                Item.PrisonerID = oldEntity.PrisonerID;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: oldEntity.ID,
                    EID: (int)MergeEntity.TableIDS.MEDICAL_PRELIMINARY_CONTENT);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BL_MedicalLayer.UpdatePrimary(Item);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Բժշկականի խմբագրում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status, needApprove = needApprove });
        }

    }
}