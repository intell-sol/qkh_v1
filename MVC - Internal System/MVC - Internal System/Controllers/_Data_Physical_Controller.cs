﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using MVC___Internal_System.Attributes;
using System.IO;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.PhysicalData)]
    public class _Data_Physical_Controller : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public _Data_Physical_Controller()
        {
            PermissionHCID = PermissionsHardCodedIds.PhysicalData;
        }
        /// <summary>
        /// Primary data add partial view
        /// </summary>
        public ActionResult Add()
        {

            PrisonersViewModel Model = new PrisonersViewModel();

            PrisonerEntity Prisoner = new PrisonerEntity();

            Model.Prisoner = Prisoner;

            Prisoner.PhysicalData = new PhysicalDataEntity();

            Prisoner.PhysicalData.Leanings = new List<LeaningEntity>();

            Prisoner.PhysicalData.Files = new List<AdditionalFileEntity>();

            Prisoner.PhysicalData.Invalides = new List<InvalidEntity>();
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել Ֆիզիկական տվյալներ"));

            return PartialView("Index", Model);
        }

        [HttpPost]
        /// <summary>
        /// Primary data edit partial view
        /// </summary>
        public ActionResult Edit(int? PrisonerID)
        {

            PrisonersViewModel Model = new PrisonersViewModel();

            if (PrisonerID != null)
            {
                PrisonerEntity currentPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: PrisonerID.Value);

                Model.Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value);

                Model.Prisoner.PhysicalData = BusinessLayer_PrisonerService.GetPhysicalData(PrisonerID.Value);

                LeaningEntity Leaning = new LeaningEntity();

                Leaning.PrisonerID = PrisonerID.Value;

                Model.Prisoner.PhysicalData.Leanings = BusinessLayer_PrisonerService.GetLeaning(Leaning);

                HeightWeightEntity curHeightWeight = new HeightWeightEntity();

                curHeightWeight.PrisonerID = PrisonerID;

                curHeightWeight.PhysicalDataID = Model.Prisoner.PhysicalData.ID;

                Model.Prisoner.PhysicalData.HeightWeight = BusinessLayer_PrisonerService.GetHeightWeight(curHeightWeight);

                Model.Prisoner.PhysicalData.Files = BusinessLayer_PrisonerService.GetFiles(PersonID: currentPrisoner.PersonID.Value, TypeID: FileType.PHYSICAL_DATA_ATTACHED_FILES);

                InvalidEntity Invalid = new InvalidEntity();

                Invalid.PrisonerID = PrisonerID;

                Model.Prisoner.PhysicalData.Invalides = BusinessLayer_PrisonerService.GetInvalides(Invalid);
            }

            Model.SkinColor = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.SKIN_COLOR);

            Model.EyesColor = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.EYES_COLOR);

            Model.HairColor = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.HAIR_COLOR);

            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;
            Model.BloodType = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.Blood_Type);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել Ֆիզիկական տվյալները", RequestData: PrisonerID.ToString()));

            return PartialView("Index", Model);
        }

        public ActionResult SaveContent(PhysicalDataEntity PhysicalData)
        {
            bool status = false;
            int? id = null;
            bool needApprove = false;
            bool exist = BusinessLayer_PrisonerService.CheckPhysicalData(PhysicalData.PrisonerID.Value);

            if (ViewBag.PPType == 2 && exist)
            {
                int Type = 3;
                int? EntityID = BusinessLayer_PrisonerService.GetPhysicalData(PhysicalData.PrisonerID.Value).ID;
                MergeEntity curEntity = new MergeEntity(PrisonerID: PhysicalData.PrisonerID,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(PhysicalData),
                    EntityID: EntityID,
                    Type: Type,
                    EID: (int)MergeEntity.TableIDS.PHYSICAL_CONTENT);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                status = tempID != null;
                needApprove = true;
            }
            else
            {
                if (exist)
                {
                    status = BusinessLayer_PrisonerService.UpdatePhysicalData(PhysicalData);
                }
                else
                {
                    id = BusinessLayer_PrisonerService.AddPhysicalData(PhysicalData);
                    if (id != null)
                    {
                        status = true;
                    }
                }

            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Պահպանել ֆիզիկական տվյալները", RequestData: JsonConvert.SerializeObject(PhysicalData)));

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult AddInvalid(InvalidEntity Invalids)
        {

            bool status = false;
            int? fileID = null;
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            if (Invalids.PrisonerID != null)
            {
                PrisonerEntity currentPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Invalids.PrisonerID.Value);
                // adding file
                if (Request.Files.Count > 0)
                {
                    // here
                    //BusinessLayer_PrisonerService.ChangeFileType

                    PrisonerEntity Prisoner = BusinessLayer_PrisonerService.GetPrisoner(Invalids.PrisonerID.Value, true);

                    HttpPostedFileBase file = Request.Files[0];
                    AdditionalFileEntity fileEntry = new AdditionalFileEntity(file.ContentLength);

                    fileEntry.FileName = Path.GetFileName(file.FileName);

                    fileEntry.FileExtension = Path.GetExtension(file.FileName);
                    fileEntry.ContentType = file.ContentType;
                    fileEntry.PersonID = (int)currentPrisoner.PersonID;
                    fileEntry.isPrisoner = true;
                    fileEntry.PNum = Prisoner.Person.Personal_ID;
                    fileEntry.TypeID = FileType.PHYSICAL_DATA_INVALID;
                    file.InputStream.Read(fileEntry.FileContent, 0, fileEntry.FileContent.Length);

                    fileID = BL_Files.getInstance().AddFile(fileEntry);
                }
                Invalids.CertificateFileID = fileID;

                int? id = BusinessLayer_PrisonerService.AddInvalides(Invalids);

                if (id != null)
                {
                    status = true;
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել հաշմանդամ", RequestData: JsonConvert.SerializeObject(Invalids)));

            return Json(new { status = status });
        }

        public ActionResult EditInvalids(InvalidEntity Invalids)
        {
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            bool status = false;
            bool needApprove = false;
            int? fileID = null;
            if (Invalids.PrisonerID != null)
            {
                PrisonerEntity currentPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Invalids.PrisonerID.Value);
                if (Invalids.ID != null)
                {
                    // adding file
                    if (Request.Files.Count > 0)
                    {
                        // here
                        //BusinessLayer_PrisonerService.ChangeFileType

                        PrisonerEntity Prisoner = BusinessLayer_PrisonerService.GetPrisoner(Invalids.PrisonerID.Value, true);

                        HttpPostedFileBase file = Request.Files[0];
                        AdditionalFileEntity fileEntry = new AdditionalFileEntity(file.ContentLength);

                        fileEntry.FileName = Path.GetFileName(file.FileName);

                        fileEntry.FileExtension = Path.GetExtension(file.FileName);
                        fileEntry.ContentType = file.ContentType;
                        fileEntry.PersonID = (int)currentPrisoner.PersonID;
                        fileEntry.isPrisoner = true;
                        fileEntry.PNum = Prisoner.Person.Personal_ID;
                        fileEntry.TypeID = FileType.PHYSICAL_DATA_INVALID;
                        file.InputStream.Read(fileEntry.FileContent, 0, fileEntry.FileContent.Length);

                        fileID = BL_Files.getInstance().AddFile(fileEntry);

                        Invalids.CertificateFileID = fileID;
                    }

                    int? PrisonerID = BusinessLayer_PrisonerService.GetInvalides(new InvalidEntity(ID: Invalids.ID)).First().PrisonerID;
                    PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
                    if (ViewBag.PPType == 2 && PrisonerID.HasValue && curPrisoner.State.Value)
                    {
                        MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                            EmployeeID: ViewBag.UserID,
                            Json: JsonConvert.SerializeObject(Invalids),
                            Type: 3,
                            EntityID: Invalids.ID,
                            EID: (int)MergeEntity.TableIDS.PHYSICAL_INVALIDS);
                        if (Request.Files.Count > 0)
                    {
                        // here
                        //BusinessLayer_PrisonerService.ChangeFileType

                        PrisonerEntity Prisoner = BusinessLayer_PrisonerService.GetPrisoner(Invalids.PrisonerID.Value, true);

                        HttpPostedFileBase file = Request.Files[0];
                        AdditionalFileEntity fileEntry = new AdditionalFileEntity(file.ContentLength);

                        fileEntry.FileName = Path.GetFileName(file.FileName);

                        fileEntry.FileExtension = Path.GetExtension(file.FileName);
                        fileEntry.ContentType = file.ContentType;
                        fileEntry.PersonID = (int)currentPrisoner.PersonID;
                        fileEntry.isPrisoner = true;
                        fileEntry.PNum = Prisoner.Person.Personal_ID;
                        fileEntry.TypeID = FileType.PHYSICAL_DATA_INVALID;
                        file.InputStream.Read(fileEntry.FileContent, 0, fileEntry.FileContent.Length);

                        fileID = BL_Files.getInstance().AddFile(fileEntry);

                        Invalids.CertificateFileID = fileID;
                    }
                        int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                        needApprove = true;
                        status = tempID != null;
                    }
                    
                    else
                    {
                        status = BusinessLayer_PrisonerService.UpdateInvalides(Invalids);
                    }
                }
            }
            
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել հաշմանդամ", RequestData: JsonConvert.SerializeObject(Invalids)));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult RemInvalid(int ID)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetInvalides(new InvalidEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                InvalidEntity invalidEntity = BusinessLayer_PrisonerService.GetInvalides(new InvalidEntity(ID: ID)).First();

                invalidEntity.Status = false;

                MergeEntity curEntity = new MergeEntity(PrisonerID: invalidEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(new InvalidEntity(ID: ID, Status: false)),
                    Type: 2,
                    EntityID: invalidEntity.ID,
                    EID: (int)MergeEntity.TableIDS.PHYSICAL_INVALIDS);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateInvalides(new InvalidEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել հաշմանդամ", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }
       
        public ActionResult RemLeaning(int ID)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetLeaning(new LeaningEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                LeaningEntity leaningEntity = BusinessLayer_PrisonerService.GetLeaning(new LeaningEntity(ID: ID)).First();

                leaningEntity.Status = false;

                MergeEntity curEntity = new MergeEntity(PrisonerID: leaningEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(new LeaningEntity(ID: ID, Status: false)),
                    Type: 2,
                    EntityID: leaningEntity.ID,
                    EID: (int)MergeEntity.TableIDS.PHYSICAL_LEANINGS);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateLeaning(new LeaningEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել հակվածություն", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult EditLeanings(LeaningEntity entity)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetLeaning(new LeaningEntity(ID: entity.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                LeaningEntity leaningEntity = BusinessLayer_PrisonerService.GetLeaning(new LeaningEntity(ID: entity.ID)).First();

                leaningEntity.Status = false;
                entity.PrisonerID = leaningEntity.PrisonerID;
                MergeEntity curEntity = new MergeEntity(PrisonerID: entity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(entity),
                    Type: 3,
                    EntityID: entity.ID,
                    EID: (int)MergeEntity.TableIDS.PHYSICAL_LEANINGS);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateLeaning(entity);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել հակվածություն", RequestData: entity.ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult AddLeaning(LeaningEntity Leanings)
        {
            bool status = false;
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            if (Leanings.PrisonerID != null)
            {
                int? id = BusinessLayer_PrisonerService.AddLeaning(Leanings);

                if (id != null)
                {
                    status = true;
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել հակվածություն", RequestData: JsonConvert.SerializeObject(Leanings)));

            return Json(new { status = status });
        }

        public ActionResult SaveHeightWeight(HeightWeightViewModel Entity)
        {
            bool status = true;
            bool needApprove = false;

            if (Entity.PrisonerID != null)
            {
                List<HeightWeightEntity> oldList = BusinessLayer_PrisonerService.GetHeightWeight(new HeightWeightEntity(PrisonerID: Entity.PrisonerID));

                // update Height Weight
                foreach (var curEntity in Entity.List)
                {
                    if (!oldList.FindAll(r => r.ID == curEntity.ID).Any())
                    {
                        int? curID = BusinessLayer_PrisonerService.AddHeightWeight(curEntity);
                        if (curID == null) status = false;
                    }
                }
                foreach (var curEntity in oldList)
                {
                    if (!Entity.List.FindAll(r => r.ID == curEntity.ID).Any())
                    {
                        
                        PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(Entity.PrisonerID.Value, false);
                        if (ViewBag.PPType == 2 && curPrisoner.State.Value)
                        {
                            curEntity.Status = false;

                            MergeEntity curMergeEntity = new MergeEntity(PrisonerID: Entity.PrisonerID.Value,
                                EmployeeID: ViewBag.UserID,
                                Json: JsonConvert.SerializeObject(new LeaningEntity(ID: curEntity.ID, Status: false)),
                                Type: 2,
                                EntityID: curEntity.ID,
                                EID: (int)MergeEntity.TableIDS.PHYSICAL_HEIGHT_WEIGHT);

                            int? tempID = BusinessLayer_MergeLayer.AddModification(curMergeEntity);

                            needApprove = true;

                            status = tempID != null;
                        }
                        else
                        {
                            curEntity.Status = false;
                            bool tempStatus = BusinessLayer_PrisonerService.RemoveHeightWeight(curEntity);
                            if (!tempStatus) status = false;
                        }
                    }
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել բոյ/քաշ", RequestData: JsonConvert.SerializeObject(Entity)));

            return Json(new { status = status, needApprove = needApprove });
        }


    }
}