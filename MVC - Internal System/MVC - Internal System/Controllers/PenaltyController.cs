﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.Penalties)]
    public class PenaltyController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public PenaltyController()
        {
            PermissionHCID = PermissionsHardCodedIds.Penalties;
        }
        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Տույժեր";

                PenaltiesViewModel Model = new PenaltiesViewModel();
                
                Model.FilterPenalties = new FilterPenaltiesEntity();
                if ((System.Web.HttpContext.Current.Session["FilterPenalties"] as FilterPenaltiesEntity) != null)
                    Model.FilterPenalties = (FilterPenaltiesEntity)System.Web.HttpContext.Current.Session["FilterPenalties"];

                Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PENALTY_TYPE_CONVICT);
                Model.ViolationLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PENALTY_VIOLATION);
                Model.StateLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PENALTY_STATE);
                Model.StateBaseLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PENALTY_STATE_BASE);
                Model.EmployeeList = BusinessLayer_OrganizatonLayer.GetOrganizationEmployees(ViewBag.SelectedOrgUnitID);

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տույժեր"));

                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տույժի ավելացում"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;
            return PartialView("Index");
        }
        [HttpPost]
        public ActionResult Edit(int? PrisonerID)
        {
            PenaltiesViewModel Model = new PenaltiesViewModel();         

            Model.Penalty = BusinessLayer_PrisonerService.GetPenalty(PrisonerID: PrisonerID.Value);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տույժի խմբագրում", RequestData: PrisonerID.ToString()));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;
            return PartialView("Add", Model);
        }
        public ActionResult Draw_PenaltyTableRow(int? id = null)
        {
            // UNDONE: watch this section
            FilterPenaltiesEntity FilterPenalties = new FilterPenaltiesEntity();
            if ((System.Web.HttpContext.Current.Session["FilterPenalties"] as FilterPenaltiesEntity) != null)
                FilterPenalties = (FilterPenaltiesEntity)System.Web.HttpContext.Current.Session["FilterPenalties"];

            int page = id ?? 1;

            FilterPenalties.paging = new FilterPenaltiesEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PenaltiesDashboardEntity> ListDashboardPenalties = BusinessLayer_PrisonerService.GetPenaltiesForDashboard(FilterPenalties);

            PenaltiesViewModel Model = new PenaltiesViewModel();

            Model.ListDashboardPenalties = ListDashboardPenalties;
            Model.PenaltiesEntityPaging = FilterPenalties.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տույժերի դիտում"));

            return PartialView("Penalty_Table_Row", Model);
        }

        public ActionResult Draw_PenaltyTableRowByFilter(FilterPenaltiesEntity FilterPenalties)
        {
            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterPenalties"] = FilterPenalties;

            FilterPenalties.paging = new FilterPenaltiesEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<PenaltiesDashboardEntity> PenaltiesList = BusinessLayer_PrisonerService.GetPenaltiesForDashboard(FilterPenalties);

            PenaltiesViewModel Model = new PenaltiesViewModel();

            Model.ListDashboardPenalties = PenaltiesList;
            Model.PenaltiesEntityPaging= FilterPenalties.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տույժերի Որոնում", RequestData: JsonConvert.SerializeObject(FilterPenalties)));

            return PartialView("Penalty_Table_Row", Model);
        }

        [HttpPost]
        public ActionResult AddPen(PenaltiesEntity Item)
        {
            bool status = false;
            int? id = null;
            if (Item.PrisonerID != null)
            {
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];

                id = BusinessLayer_PrisonerService.AddPenalty(Item);

                if (id != null && Item.ViolationList != null)
                {
                    status = true;

                    foreach (PenaltiesViolationEntity curEntity in Item.ViolationList)
                    {
                        curEntity.PenaltyID = id;
                        curEntity.PrisonerID = Item.PrisonerID;
                        int? tempID = BusinessLayer_PrisonerService.AddViolationLibItemToPenalty(curEntity);
                        if (tempID == null) status = false;
                    }
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տույժի ավելացում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }
        public ActionResult Draw_ResetPenaltiesTableRow()
        {
            Session["FilterPenalties"] = null;
            return Json(new { status = true });
        }
        public ActionResult RemPen(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetPenalties(new PenaltiesEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                PenaltiesEntity oldEntity = BusinessLayer_PrisonerService.GetPenalties(new PenaltiesEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.PENALTY_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdatePenalties(new PenaltiesEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տույժի հեռացում", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult EditPen(PenaltiesEntity Item)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetPenalties(new PenaltiesEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.PENALTY_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                if (Item.ID != null && Item.PrisonerID != null)
                {
                    status = BusinessLayer_PrisonerService.UpdatePenalties(Item);

                    List<PenaltiesViolationEntity> oldList = BusinessLayer_PrisonerService.GetViolationLibItemToPenalty(new PenaltiesViolationEntity(PenaltyID: Item.ID));

                    List<PenaltiesViolationEntity> newList = Item.ViolationList;

                    foreach (PenaltiesViolationEntity item in oldList)
                    {
                        if (!newList.FindAll(r => r.LibItemID == item.LibItemID).Any())
                        {
                            bool tempStatus = BusinessLayer_PrisonerService.UpdateViolationLibItemToPenalty(new PenaltiesViolationEntity(ID: item.ID, Status: false));
                            if (!tempStatus) status = false;
                        }
                    }

                    foreach (PenaltiesViolationEntity item in newList)
                    {
                        if (!oldList.FindAll(r => r.LibItemID == item.LibItemID).Any())
                        {
                            item.PenaltyID = Item.ID;
                            item.PrisonerID = Item.PrisonerID;
                            int? tempID = BusinessLayer_PrisonerService.AddViolationLibItemToPenalty(item);
                            if (tempID == null) status = false;
                        }
                    }
                }
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Տույժի խմբագրում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status, needApprove = needApprove });
        }

        [HttpPost]
        public ActionResult AddPenObjection(PenaltiesProtestEntity Item)
        {
            bool status = false;
            int? id = null;
            if (Item.PenaltyID != null)
            {
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];

                id = BusinessLayer_PrisonerService.AddPenaltyProtest(Item);

                if (id != null) status = true;
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելավնել տույժի առարկություն", RequestData: JsonConvert.SerializeObject(Item)));
            
            return Json(new { status = status });
        }

        [HttpPost]
        public ActionResult EditPenObjection(PenaltiesProtestEntity Item)
        {
            bool status = false;
            int? id = null;
            if (Item.PenaltyID != null || Item.AppealResultLibItemID != null)
            {
                List<PenaltiesProtestEntity> list = BusinessLayer_PrisonerService.GetPenaltyProtest(new PenaltiesProtestEntity(ID: Item.ID == null ? Item.ID : null , PenaltyID: Item.PenaltyID.Value));

                if (list != null)
                {
                    if (list.Any())
                    {
                        BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
                        
                        status = BusinessLayer_PrisonerService.UpdatePenaltyProtest(Item);
                        if(Item.AppealResultLibItemID == 583)
                        {
                            PenaltiesEntity penalty = new PenaltiesEntity();
                            penalty.ID = Item.PenaltyID;
                            BusinessLayer_PrisonerService.UpdatePenalties(penalty);
                        }
                    }
                    else
                    {
                        BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];

                        id = BusinessLayer_PrisonerService.AddPenaltyProtest(Item);
                        if (id == null) status = false;
                    }
                }
                
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել տույժի առարկություն", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }
        [ReadWrite]
        [HttpPost]
        public ActionResult CompleteData(PenaltiesEntity Item)
        {
            bool status = false;

            if (Item.ID != null)
            {
                Item.StateLibItemID = 564; // LibItemID
                if (Item.StatusChangeBasisLibItemID != null && Item.StatusChangeBasisLibItemID == 570)
                {
                    Item.PenaltiesMaturity = DateTime.Now;
                }
                status = BusinessLayer_PrisonerService.UpdatePenalties(Item);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավարտել տույժը", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }
    }
}