﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.FamilyRelative)]
    public class _Data_Family_Controller : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public _Data_Family_Controller()
        {
            PermissionHCID = PermissionsHardCodedIds.FamilyRelative;
        }
        /// <summary>
        /// Primary data add partial view
        /// </summary>
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել ընտանիք"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }

        [HttpPost]
        public ActionResult Edit(int PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID;

            PrisonerEntity currentPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: PrisonerID);

            FamilyRelativeEntity curEntity = new FamilyRelativeEntity();

            curEntity.PrisonerID = PrisonerID;

            Model.Prisoner.FamiltyRelative = BusinessLayer_PrisonerService.GetFamilyRelations(new FamilyRelativeEntity(PrisonerID: PrisonerID));

            Model.Prisoner.FamiltyRelative.Files = BusinessLayer_PrisonerService.GetFiles(PrisonerID: PrisonerID, TypeID: FileType.FAMILY_ATTACHED_FILES);

            Model.Prisoner.FamiltyRelative.Spouses = BusinessLayer_PrisonerService.GetSpouses(new SpousesEntity(PrisonerID: PrisonerID));

            Model.Prisoner.FamiltyRelative.Childrens = BusinessLayer_PrisonerService.GetChildren(new ChildrenEntity(PrisonerID: PrisonerID));

            Model.Prisoner.FamiltyRelative.Parents = BusinessLayer_PrisonerService.GetParents(new ParentsEntity(PrisonerID: PrisonerID));

            Model.Prisoner.FamiltyRelative.SisterBrother = BusinessLayer_PrisonerService.GetSisterBrother(new SisterBrotherEntity(PrisonerID: PrisonerID));

            Model.Prisoner.FamiltyRelative.OtherRelatives = BusinessLayer_PrisonerService.GetOtherRelatives(new OtherRelativesEntity(PrisonerID: PrisonerID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել ընտանիքը", RequestData: PrisonerID.ToString()));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index", Model);
        }

        public ActionResult SaveFamiltyStatus(FamilyRelativeEntity entity)
        {
            bool? status = true;
            bool needApprove = false;

            if (entity.PrisonerID != null && entity.FamilyStatusID != null)
            {
                FamilyRelativeEntity oldEntity = BusinessLayer_PrisonerService.GetFamilyRelations(new FamilyRelativeEntity(PrisonerID: entity.PrisonerID));

                if (oldEntity.ID != null)
                {

                    if (ViewBag.PPType == 2)
                    {
                        oldEntity.FamilyStatusID = entity.FamilyStatusID;
                        MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                            EmployeeID: ViewBag.UserID,
                            Json: JsonConvert.SerializeObject(oldEntity),
                            Type: 3,
                            EntityID: oldEntity.ID,
                            EID: (int)MergeEntity.TableIDS.FAMILY_CONTENT);

                        int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                        needApprove = true;

                        status = tempID != null;
                    }
                    else
                    {
                        oldEntity.FamilyStatusID = entity.FamilyStatusID;
                        status = BusinessLayer_PrisonerService.UpdateFamilyRelations(oldEntity);
                    }
                }
                else
                {
                    int? id = BusinessLayer_PrisonerService.AddFamilyRelations(entity);
                    if (id == null) status = false;
                }
            }

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult AddSpouse(SpousesEntity Data)
        {
            bool status = false;

            if (Data.Person != null)
            {
                Data.Person = BusinessLayer_PrisonerService.AddPersonAndGet(Person: Data.Person);
            }

            if (Data.Person != null && Data.PrisonerID != null)
            {
                int? id = BusinessLayer_PrisonerService.AddSpouses(Data);
                //int? id = ((Spouses)Data).Add();

                if (id != null) status = true;
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել ամուսին", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status });
        }
        public ActionResult EditSpouse(SpousesEntity Data)
        {
            bool status = false;
            bool needApprove = false;

            if (Data.ID != null)
            {

                int? PrisonerID = BusinessLayer_PrisonerService.GetSpouses(new SpousesEntity(ID: Data.ID)).First().PrisonerID;
                PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
                if (ViewBag.PPType == 2 && curPrisoner.State.Value)
                {
                    SpousesEntity oldEntity = BusinessLayer_PrisonerService.GetSpouses(new SpousesEntity(ID: Data.ID)).First();
                    if (Data.Person == null) Data.Person = oldEntity.Person;

                    Data.PrisonerID = oldEntity.PrisonerID;
                    MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                        EmployeeID: ViewBag.UserID,
                        Json: JsonConvert.SerializeObject(Data),
                        Type: 3,
                        EntityID: Data.ID,
                        EID: (int)MergeEntity.TableIDS.FAMILY_SPOUSES);

                    int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                    needApprove = true;

                    status = tempID != null;
                }
                else
                {
                    status = BusinessLayer_PrisonerService.UpdateSpouses(Data);
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել ամուսնուն", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult RemSpouse(int ID)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetSpouses(new SpousesEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                SpousesEntity oldEntity = BusinessLayer_PrisonerService.GetSpouses(new SpousesEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.FAMILY_SPOUSES);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateSpouses(new SpousesEntity(ID: ID, Status: false));
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել ամուսնուն", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult AddChildren(ChildrenEntity Data)
        {
            bool status = false;

            if (Data.Person != null)
            {
                Data.Person = BusinessLayer_PrisonerService.AddPersonAndGet(Person: Data.Person);
            }

            if (Data.Person != null && Data.PrisonerID != null)
            {
                int? id = BusinessLayer_PrisonerService.AddChildren(Data);
                if (id != null) status = true;
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել երեխա", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status });
        }
        public ActionResult EditChildren(ChildrenEntity Data)
        {
            bool status = false;
            bool needApprove = false;

            if (Data.ID != null)
            {

                int? PrisonerID = BusinessLayer_PrisonerService.GetChildren(new ChildrenEntity(ID: Data.ID)).First().PrisonerID;
                PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
                if (ViewBag.PPType == 2 && curPrisoner.State.Value)
                {
                    ChildrenEntity oldEntity = BusinessLayer_PrisonerService.GetChildren(new ChildrenEntity(ID: Data.ID)).First();
                    if (Data.Person == null) Data.Person = oldEntity.Person;

                    Data.PrisonerID = oldEntity.PrisonerID;
                    MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                        EmployeeID: ViewBag.UserID,
                        Json: JsonConvert.SerializeObject(Data),
                        Type: 3,
                        EntityID: Data.ID,
                        EID: (int)MergeEntity.TableIDS.FAMILY_CHILDRENS);

                    int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                    needApprove = true;

                    status = tempID != null;
                }
                else
                {
                    status = BusinessLayer_PrisonerService.UpdateChildren(Data);
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել երեխաներին", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult RemChildren(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetChildren(new ChildrenEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                ChildrenEntity oldEntity = BusinessLayer_PrisonerService.GetChildren(new ChildrenEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.FAMILY_CHILDRENS);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateChildren(new ChildrenEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել երեխաներին", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult AddParent(ParentsEntity Data)
        {
            bool status = false;

            if (Data.Person != null)
            {
                Data.Person = BusinessLayer_PrisonerService.AddPersonAndGet(Person: Data.Person);
            }

            if (Data.Person != null && Data.PrisonerID != null)
            {
                int? id = BusinessLayer_PrisonerService.AddParents(Data);
                if (id != null) status = true;
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել ծնող", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status });
        }
        public ActionResult EditParent(ParentsEntity Data)
        {
            bool status = false;
            bool needApprove = false;

            if (Data.ID != null)
            {

                int? PrisonerID = BusinessLayer_PrisonerService.GetParents(new ParentsEntity(ID: Data.ID)).First().PrisonerID;
                PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
                if (ViewBag.PPType == 2 && curPrisoner.State.Value)
                {
                    ParentsEntity oldEntity = BusinessLayer_PrisonerService.GetParents(new ParentsEntity(ID: Data.ID)).First();
                    if (Data.Person == null) Data.Person = oldEntity.Person;

                    Data.PrisonerID = oldEntity.PrisonerID;
                    MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                        EmployeeID: ViewBag.UserID,
                        Json: JsonConvert.SerializeObject(Data),
                        Type: 3,
                        EntityID: Data.ID,
                        EID: (int)MergeEntity.TableIDS.FAMILY_PARENTS);

                    int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                    needApprove = true;

                    status = tempID != null;
                }
                else
                {
                    status = BusinessLayer_PrisonerService.UpdateParents(Data);
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել ծնողին", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status, needApprove  = needApprove });
        }
        public ActionResult RemParent(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetParents(new ParentsEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                ParentsEntity oldEntity = BusinessLayer_PrisonerService.GetParents(new ParentsEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.FAMILY_PARENTS);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateParents(new ParentsEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել ծնողին", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult AddSysterBrother(SisterBrotherEntity Data)
        {
            bool status = false;

            if (Data.Person != null)
            {
                Data.Person = BusinessLayer_PrisonerService.AddPersonAndGet(Person: Data.Person);
            }

            if (Data.Person != null && Data.PrisonerID != null)
            {
                int? id = BusinessLayer_PrisonerService.AddSisterBrother(Data);
                if (id != null) status = true;
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել քույր/եղբայր", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status });
        }
        public ActionResult EditSysterBrother(SisterBrotherEntity Data)
        {
            bool status = false;
            bool needApprove = false;

            if (Data.ID != null)
            {

                int? PrisonerID = BusinessLayer_PrisonerService.GetSisterBrother(new SisterBrotherEntity(ID: Data.ID)).First().PrisonerID;
                PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
                if (ViewBag.PPType == 2 && curPrisoner.State.Value)
                {
                    SisterBrotherEntity oldEntity = BusinessLayer_PrisonerService.GetSisterBrother(new SisterBrotherEntity(ID: Data.ID)).First();
                    if (Data.Person == null) Data.Person = oldEntity.Person;

                    Data.PrisonerID = oldEntity.PrisonerID;
                    MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                        EmployeeID: ViewBag.UserID,
                        Json: JsonConvert.SerializeObject(Data),
                        Type: 3,
                        EntityID: Data.ID,
                        EID: (int)MergeEntity.TableIDS.FAMILY_SYSTER_BROTHER);

                    int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                    needApprove = true;

                    status = tempID != null;
                }
                else
                {
                    status = BusinessLayer_PrisonerService.UpdateSisterBrother(Data);
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել քրոջը/եղբորը", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult RemSysterBrother(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetSisterBrother(new SisterBrotherEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                SisterBrotherEntity oldEntity = BusinessLayer_PrisonerService.GetSisterBrother(new SisterBrotherEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.FAMILY_SYSTER_BROTHER);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateSisterBrother(new SisterBrotherEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել քրոջը/եղբորը", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult AddOtherRelative(OtherRelativesEntity Data)
        {
            bool status = false;

            if (Data.Person != null)
            {
                Data.Person = BusinessLayer_PrisonerService.AddPersonAndGet(Person: Data.Person);
            }

            if (Data.Person != null && Data.PrisonerID != null)
            {
                int? id = BusinessLayer_PrisonerService.AddOtherRelatives(Data);
                if (id != null) status = true;
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել այլ հարազատ", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status });
        }
        public ActionResult EditOtherRelative(OtherRelativesEntity Data)
        {
            bool status = false;
            bool needApprove = false;

            if (Data.ID != null)
            {

                int? PrisonerID = BusinessLayer_PrisonerService.GetOtherRelatives(new OtherRelativesEntity(ID: Data.ID)).First().PrisonerID;
                PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
                if (ViewBag.PPType == 2 && curPrisoner.State.Value)
                {
                    OtherRelativesEntity oldEntity = BusinessLayer_PrisonerService.GetOtherRelatives(new OtherRelativesEntity(ID: Data.ID)).First();
                    if (Data.Person == null) Data.Person = oldEntity.Person;

                    Data.PrisonerID = oldEntity.PrisonerID;
                    MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                        EmployeeID: ViewBag.UserID,
                        Json: JsonConvert.SerializeObject(Data),
                        Type: 3,
                        EntityID: Data.ID,
                        EID: (int)MergeEntity.TableIDS.FAMILY_OTHERS);

                    int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                    needApprove = true;

                    status = tempID != null;
                }
                else
                {
                    status = BusinessLayer_PrisonerService.UpdateOtherRelatives(Data);
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Այլ հարազատի խմբագրում", RequestData: JsonConvert.SerializeObject(Data)));

            return Json(new { status = status, needApprove = needApprove });
        }
        public ActionResult RemOtherRelative(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetOtherRelatives(new OtherRelativesEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                OtherRelativesEntity oldEntity = BusinessLayer_PrisonerService.GetOtherRelatives(new OtherRelativesEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.FAMILY_OTHERS);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateOtherRelatives(new OtherRelativesEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել այլ հարազատի", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }


        private AVVPerson Getperson(String DocNumber, bool Type)
        {
            //int? PersonID = null;

            AVVPerson person = null;

            if (Type)
                person = BusinessLayer_PoliceService.getBYDocumentNumber(DocNumber);
            else
                person = BusinessLayer_PoliceService.getBYPersonalNumber(DocNumber);

            return person;
        }

    }
}