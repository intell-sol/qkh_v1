﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.ItemsObjects)]
    public class _Data_Items_Controller : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public _Data_Items_Controller()
        {
            PermissionHCID = PermissionsHardCodedIds.ItemsObjects;
        }
        /// <summary>
        /// Primary data add partial view
        /// </summary>
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել իրեր/առարկաներ"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }

        [HttpPost]
        /// <summary>
        /// Primary data edit partial view
        /// </summary>
        public ActionResult Edit(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;

            Model.Prisoner.ItemsObjects = BusinessLayer_PrisonerService.GetItemObject(PrisonerID.Value);

            Model.ItemsObjects = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ITEMS_OBJECTS);
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել իրեր/առարկաներ", RequestData: PrisonerID.ToString()));

            return PartialView("Index", Model);
        }

        public ActionResult AddItem(ItemEntity Item)
        {
            bool status = false;
            int? id = null;
            if (Item.PrisonerID != null && Item.Count != null && Item.LibItemID != null)
            {
                id = BusinessLayer_PrisonerService.AddItemObject(Item);
            }

            if (id != null) status = true;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել իրեր/առարկաներ", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }

        public ActionResult EditItem(ItemEntity Item)
        {
            bool status = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetItems(new ItemEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.ITEMS_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateItemObject(Item);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել իրեր/առարկաներ", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }

        public ActionResult RemItem(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetItems(new ItemEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                ItemEntity oldEntity = BusinessLayer_PrisonerService.GetItems(new ItemEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.ITEMS_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateItemObject(new ItemEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել իրեր/առարկաներ", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }
    }
}