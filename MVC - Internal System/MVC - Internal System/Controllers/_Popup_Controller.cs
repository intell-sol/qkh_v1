﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using DataAccessLayer.DataCommunication;
using Newtonsoft.Json;
using BusinessLayer.BusinessServices;
using System.Threading.Tasks;
using System.Web.UI;

namespace MVC___Internal_System.Controllers
{
    public class _Popup_Controller : BaseController
    {
        public ActionResult Get_YesOrNO(int type)
        {
            YesOrNoViewModel Model = new YesOrNoViewModel(type);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ բացվող պատուհան այո/ոչ"));

            return PartialView("YesOrNo", Model);
        }
        public ActionResult Get_DocIdentity()
        {

            _Popup_DocIdentity_ViewModel Model = new _Popup_DocIdentity_ViewModel();

            // getting citizenship libs
            List<LibsEntity> citizenship = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);

            // getting docidentity libs
            List<LibsEntity> docidentity = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DOCIDENTITY);

            Model.Citizenship = citizenship;
            Model.DocIdentity = docidentity;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ փաստաթղթերի նույնությունը"));

            return PartialView("DocIdentity", Model);
        }

        [HttpPost]
        public ActionResult Draw_DocIdentity_Table_Row(List<IdentificationDocument> idDocList)
        {
            _Popup_DocIdentity_ViewModel Model = new _Popup_DocIdentity_ViewModel();

            Model.idDocuments = idDocList;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել փաստաթղթերի նույնությունը", RequestData: JsonConvert.SerializeObject(idDocList)));

            return PartialView("DocIdentity_Table_Row", Model);
        }
        [HttpPost]
        public ActionResult Get_DocIdentity_Table_Row(int? PrisonerID)
        {
            _Popup_DocIdentity_ViewModel Model = new _Popup_DocIdentity_ViewModel();

            PrisonerEntity Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value);

            IdentificationDocument curEntity = new IdentificationDocument();
            curEntity.PersonID = Prisoner.PersonID;

            List<IdentificationDocument> idDocList = new List<IdentificationDocument>();

            if (PrisonerID != null) idDocList = BusinessLayer_PrisonerService.GetIdentificationDocuments(curEntity);

            Model.idDocuments = idDocList;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ փաստաթղթերի նույնություննները", RequestData: PrisonerID.ToString()));

            return PartialView("DocIdentity_Table_Row", Model);
        }
        public ActionResult Get_FindPerson()
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Gender = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);
            for (int i = 0; i < Model.Gender.Count; i++)
            {
                // calling getting lib path method from business layer
                Model.Gender[i].propList = BusinessLayer_PropsLayer.GetPropsList(Model.Gender[i].ID);
            }
            Model.Nationality = BusinessLayer_LibsLayer.GetNationality();
            Model.Citizenships = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);
            Model.DocIdentity  = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DOCIDENTITY);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ փնտրվող անձը"));

            return PartialView("FindPerson", Model);
        }
        public ActionResult Get_FindPersonTableRow(PersonEntity person)
        {
            FamilyRelativesViewModel Model = new FamilyRelativesViewModel();

            Model.Person = person;

            if (person.ID != null)
            {
                List<AdditionalFileEntity> files = BusinessLayer_PrisonerService.GetFiles(PersonID: person.ID.Value);

                if (files != null && files.Any())
                {
                    Model.Person.PhotoLink = BL_Files.getInstance().GetLink(files.Last().ID.Value, files.First().FileName);
                }
                else
                {
                    Model.Person.PhotoLink = BL_Files.getInstance().getDefaultPhotoLink();
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել փնտրվող անձը", RequestData: JsonConvert.SerializeObject(person)));

            return PartialView("FindPersonTableRow", Model);
        }
        public ActionResult Draw_PersonFullData(PersonEntity person)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Person = person;

            if (Model.Person.PhotoLink == null)
            {
                Model.Person.PhotoLink = BL_Files.getInstance().getDefaultPhotoLink();
            }

            Model.Gender = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);
            for (int i = 0; i < Model.Gender.Count; i++)
            {
                // calling getting lib path method from business layer
                Model.Gender[i].propList = BusinessLayer_PropsLayer.GetPropsList(Model.Gender[i].ID);
            }

            Model.Nationality = BusinessLayer_LibsLayer.GetNationality();

            Model.Citizenships = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);

            Model.DocIdentity = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DOCIDENTITY);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել փնտրվող անձը", RequestData: JsonConvert.SerializeObject(person)));

            return PartialView("PersonFullData", Model);
        }
        public ActionResult Get_AddFile(int? id)
        {
            _Popup_Files_ViewModel Model = new _Popup_Files_ViewModel();

            if (id != null)
            {
                Model.currentFile = BL_Files.getInstance().GetFile(id: id.Value);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ ավելացվող ֆայլը", RequestData: id.ToString()));

            return PartialView("AddFile", Model);
        }

        public ActionResult Get_AddFileTableRow(int? PrisonerID, int TypeID)
        {
            _Popup_Files_ViewModel Model = new _Popup_Files_ViewModel();

            if (PrisonerID != null)
            {
                PrisonerEntity currentPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: PrisonerID.Value);

                FileType fileType = new FileType();

                fileType = (FileType)TypeID;
                //if (Type == "image") fileType = FileType.MAIN_DATA_ATTACHED_IMAGES;
                //else if (Type == "main") fileType = FileType.MAIN_DATA_ATTACHED_FILES;
                //else if (Type == "physical") fileType = FileType.PHYSICAL_DATA_ATTACHED_FILES;
                //else if (Type == "army") fileType = FileType.ARMY_ATTACHED_FILES;
                //else if (Type == "items") fileType = FileType.ITEMS_ATTACHED_FILES;
                //else if (Type == "cell") fileType = FileType.CAMERA_CARD_ATTACHED_FILES;
                //else if (Type == "bans") fileType = FileType.INJUNCTIONS_ATTACHED_FILES;
                //else if (Type == "adfile") fileType = FileType.ADOPTION_INFORMATION_ATTACHED_FILES;
                //else if (Type == "adsfile") fileType = FileType.ADOPTION_INFORMATION_ARREST_RECORD;
                //else if (Type == "family") fileType = FileType.FAMILY_ATTACHED_FILES;

                Model.Files = BusinessLayer_PrisonerService.GetFiles(PrisonerID: PrisonerID, TypeID: fileType);
            }

            Model.PrisonerID = PrisonerID;
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել ավելացվող ֆայլերը (get)", RequestData: JsonConvert.SerializeObject(new { PrisonerID = PrisonerID, TypeID = TypeID })));

            return PartialView("AddFile_Table_Row", Model);
        }
        public ActionResult Draw_AddFileTableRow(List<AdditionalFileEntity> Files, int? PrisonerID = null)
        {
            _Popup_Files_ViewModel Model = new _Popup_Files_ViewModel();

            Model.Files = Files;
            Model.PrisonerID = PrisonerID;

            //BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել ավելացվող ֆայլերը", RequestData: JsonConvert.SerializeObject(Files)));

            return PartialView("AddFile_Table_Row", Model);
        }
        

        public ActionResult Get_Invalids(int? id = null)
        {
            InvalidsViewModel Model = new InvalidsViewModel();

            List<LibsEntity> InvalidsDegree = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.INVALID_DEGREE);

            Model.InvalidsDegree = InvalidsDegree;

            if (id != null)
            {
                Model.currentInvalid = BusinessLayer_PrisonerService.GetInvalides(new InvalidEntity(ID: id.Value)).First();

                if (Model.currentInvalid.CertificateFileID != null)
                {
                    Model.FileSrc = BL_Files.getInstance().GetLink(Model.currentInvalid.CertificateFileID);
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հաշմանդամություն (get)"));

            return PartialView("Invalids", Model);
        }
        public ActionResult Get_InvalidTableRows(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            if (PrisonerID != null)
            {
                Model.Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value);

                Model.Prisoner.PhysicalData = BusinessLayer_PrisonerService.GetPhysicalData(PrisonerID.Value);

                InvalidEntity Invalids = new InvalidEntity();

                Invalids.PrisonerID = PrisonerID.Value;

                Model.Prisoner.PhysicalData.Invalides = BusinessLayer_PrisonerService.GetInvalides(Invalids);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել հաշմանդամությունը (get)", RequestData: PrisonerID.ToString()));

            return PartialView("Invalid_Table_Row", Model);
        }

        public ActionResult Draw_InvalidTableRows(List<InvalidEntity> Invalids)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.PhysicalData = new PhysicalDataEntity();

            Model.Prisoner.PhysicalData.Invalides = Invalids;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել հաշմանդամությունը", RequestData: JsonConvert.SerializeObject(Invalids)));

            return PartialView("Invalid_Table_Row", Model);
        }

        public ActionResult Get_Leanings(int? id = null)
        {
            LeaningsViewModel Model = new LeaningsViewModel();

            List<LibsEntity> LeaningsType = BusinessLayer_LibsLayer.GetLibsLocalTreeByLibPathID((int)LibsEntity.LIBS.LEANING_TYPE);

            Model.LeaningsType = LeaningsType;


            if (id != null)
            {
                Model.currentLeaning = BusinessLayer_PrisonerService.GetLeaning(new LeaningEntity(ID: id.Value)).First();
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ հակվածությունները"));

            return PartialView("Leanings", Model);
        }

        public ActionResult Get_LeaningTableRows(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            if (PrisonerID != null)
            {
                Model.Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value);

                Model.Prisoner.PhysicalData = BusinessLayer_PrisonerService.GetPhysicalData(PrisonerID.Value);

                LeaningEntity Leaning = new LeaningEntity();

                Leaning.PrisonerID = PrisonerID.Value;

                Model.Prisoner.PhysicalData.Leanings = BusinessLayer_PrisonerService.GetLeaning(Leaning);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել հակվածությունները (get)",RequestData: PrisonerID.ToString()));

            return PartialView("Leaning_Table_Row", Model);
        }

        public ActionResult Draw_LeaningTableRows(List<LeaningEntity> Leanings)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.PhysicalData = new PhysicalDataEntity();

            Model.Prisoner.PhysicalData.Leanings = Leanings;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել հակվածությունները", RequestData: JsonConvert.SerializeObject(Leanings)));

            return PartialView("Leaning_Table_Row", Model);
        }

        public ActionResult Get_HeightWeight(int PrisonerID)
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ հասակ/կշիռ"));

            HeightWeightViewModel Model = new HeightWeightViewModel();

            Model.List = BusinessLayer_PrisonerService.GetHeightWeight(new HeightWeightEntity(PrisonerID: PrisonerID));

            return PartialView("HeightWeight", Model);
        }
        public ActionResult Get_HeightWeightTableRows(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            if (PrisonerID != null)
            {
                Model.Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value);

                Model.Prisoner.PhysicalData = BusinessLayer_PrisonerService.GetPhysicalData(PrisonerID.Value);

                HeightWeightEntity Entity = new HeightWeightEntity();

                Entity.PrisonerID = PrisonerID;

                Model.Prisoner.PhysicalData.HeightWeight = BusinessLayer_PrisonerService.GetHeightWeight(Entity);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել բոյ/քաշ", RequestData: PrisonerID.ToString()));

            return PartialView("HeightWeight_Table_Row", Model);
        }

        public ActionResult Get_AddFingerTattoScar(FingerprintsTattoosScarsItemEntity Item)
        {
            _Popup_Files_ViewModel Model = new _Popup_Files_ViewModel();

            if (Item.ID != null)
            {
                Model.curFingerTattoScarItem = BusinessLayer_PrisonerService.GetFingerprintsTattoosScarsItem(ID: Item.ID.Value).First();
                if(Model.curFingerTattoScarItem.FileID != null)
                    Model.currentFile = BL_Files.getInstance().GetFile(id: Model.curFingerTattoScarItem.FileID.Value);
                else
                {
                    Model.currentFile = new AdditionalFileEntity();
                }
            }
            if (Item.PrisonerID != null)
            {
                PrisonerEntity currentPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Item.PrisonerID.Value);

                FileType fileType = new FileType();

                fileType = (FileType)Item.TypeID;
                

                Model.Files = BusinessLayer_PrisonerService.GetFiles(PrisonerID: Item.PrisonerID, TypeID: fileType);
            }

            Model.PrisonerID = Item.PrisonerID;
            string name = "";

            switch (Item.TypeID)
            {
                case 18:
                    name = "մատնահետք";
                    break;
                case 20:
                    name = "դաջվածք";
                    break;
                case 22:
                    name = "սպի";
                    break;
            }

            Model.Name = name;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել մատնահետք/դաջվածք/սպի (get)", RequestData: Item.ID.ToString()));

            return PartialView("FingerTatooScar", Model);
        }

        public ActionResult Get_FingerRows(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();
           
            if (PrisonerID != null)
            {
                Model.Prisoner = new PrisonerEntity();

                Model.Prisoner.ID = PrisonerID;
                Model.Prisoner.FingerprintsTattoosScars = BusinessLayer_PrisonerService.GetFingerprintsTattoosScars(Model.Prisoner.ID.Value);
            }
        
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ մատնահետքերը", RequestData: PrisonerID.ToString()));

            return PartialView("FingerRows", Model);
        }
        public ActionResult Get_TatooRows(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            if (PrisonerID != null)
            {
                Model.Prisoner = new PrisonerEntity();

                Model.Prisoner.ID = PrisonerID;

                Model.Prisoner.FingerprintsTattoosScars = BusinessLayer_PrisonerService.GetFingerprintsTattoosScars(Model.Prisoner.ID.Value);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ դաջվածքնեը", RequestData: PrisonerID.ToString()));

            return PartialView("TatooRows", Model);
        }
        public ActionResult Get_ScarRows(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            if (PrisonerID != null)
            {
                Model.Prisoner = new PrisonerEntity();

                Model.Prisoner.ID = PrisonerID;

                Model.Prisoner.FingerprintsTattoosScars = BusinessLayer_PrisonerService.GetFingerprintsTattoosScars(Model.Prisoner.ID.Value);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ սպիները", RequestData: PrisonerID.ToString()));

            return PartialView("ScarRows", Model);
        }
        // CODE: get army with libs
        public ActionResult Get_Army(int? id)
        {
            ArmyViewModel Model = new ArmyViewModel();

            Model.MilitaryType = BusinessLayer_LibsLayer.GetLibsLocalTreeByLibPathID((int)LibsEntity.LIBS.ARMY_TYPE);
            Model.MilitaryPosition = BusinessLayer_LibsLayer.GetLibsLocalTreeByLibPathID((int)LibsEntity.LIBS.ARMY_POSTITION);
            Model.MilitaryProfession = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ARMY_PROFESSION);
            Model.MilitaryRank = BusinessLayer_LibsLayer.GetLibsLocalTreeByLibPathID((int)LibsEntity.LIBS.ARMY_RANK);

            if (id != null)
            {
                Model.armyEntity = BusinessLayer_PrisonerService.GetArmy(new ArmyEntity(ID: id.Value)).First();
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ծառայությունը (get)", RequestData: id.ToString()));

            return PartialView("Army", Model);
        }

        public ActionResult Draw_ArmyTableRows(List<ArmyEntity> Army)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.MilitaryService = new MilitaryServiceEntity();

            Model.Prisoner.MilitaryService.Army = Army;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել ծառայությունը", RequestData: JsonConvert.SerializeObject(Army)));

            return PartialView("Army_Table_Row", Model);
        }

        public ActionResult Get_ArmyTableRows(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.MilitaryService = new MilitaryServiceEntity();

            Model.Prisoner.MilitaryService.Army = BusinessLayer_PrisonerService.GetArmy(new ArmyEntity(PrisonerID: PrisonerID.Value));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել ծառայությունները (get)", RequestData: PrisonerID.ToString()));

            return PartialView("Army_Table_Row", Model);
        }


        public ActionResult Get_ItemsObjects(int? id)
        {
            ItemsViewModel Model = new ItemsViewModel();

            Model.ItemsObjectLibs = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ITEMS_OBJECTS);

            if (id != null)
            {
                Model.Item = BusinessLayer_PrisonerService.GetItems(new ItemEntity(ID: id.Value)).First();
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ իրերը/առարկաները", RequestData: id.ToString()));

            return PartialView("ItemsObjects", Model);
        }


        public ActionResult Get_ItemsObjectsTableRows(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ItemsObjects = BusinessLayer_PrisonerService.GetItemObject(PrisonerID.Value);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել իրերը/առարկաները (get)", RequestData: PrisonerID.ToString()));

            return PartialView("ItemsObjects_Table_Row", Model);
        }

        public ActionResult Draw_ItemsObjectsTableRows(List<ItemEntity> Items)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ItemsObjects = new ItemObjectEntity();

            Model.Prisoner.ItemsObjects.Items = Items;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել իրերը/առարկաները", RequestData: JsonConvert.SerializeObject(Items)));

            return PartialView("ItemsObjects_Table_Row", Model);
        }
        public ActionResult Get_CellCard(int? id)
        {
            CameraCardViewModel Model = new CameraCardViewModel();

            // TODO: change 74 to SelectedOrgUnitID
            Model.EmployeerList = BusinessLayer_OrganizatonLayer.GetOrganizationEmployees(ViewBag.SelectedOrgUnitID);

            if (id != null)
            {
                Model.Item = BusinessLayer_PrisonerService.GetCameraCardItems(new CameraCardItemEntity(ID: id.Value)).First();
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ Խցիկի քարտը", RequestData: id.ToString()));

            return PartialView("CellCard", Model);
        }
        public ActionResult Get_CellTableRows(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.CameraCard = BusinessLayer_PrisonerService.GetCamerCards(new CameraCardEntity(PrisonerID: PrisonerID.Value));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ Խցիկները", RequestData: PrisonerID.ToString()));

            return PartialView("CellCard_Table_Row", Model);
        }
        public ActionResult Draw_CellTableRows(List<CameraCardItemEntity> Cells)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.CameraCard = new CameraCardEntity();

            Model.Prisoner.CameraCard.CameraCardItem = Cells;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել խցիկները", RequestData: JsonConvert.SerializeObject(Cells)));

            return PartialView("CellCard_Table_Row", Model);
        }
        public ActionResult Get_Bans(int? id, bool? state)
        {
            BansViewModel Model = new BansViewModel();

            Model.BanItems = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.BAN_OBJECT);

            Model.BanPerson = BusinessLayer_LibsLayer.GetLibsLocalTreeByLibPathID((int)LibsEntity.LIBS.BAN_PERSON);
            if (id != null)
            {
                Model.State = false;
                Model.Item = BusinessLayer_PrisonerService.GetInjunctionItem(new InjunctionItemEntity(ID: id.Value)).First();
            }
            else
            {
                Model.State = state;
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ արգելանքները", RequestData: id.ToString()));

            return PartialView("Bans", Model);
        }
        public ActionResult Get_BansTerminate(int? id)
        {
            BansViewModel Model = new BansViewModel();

            Model.BanPerson = BusinessLayer_LibsLayer.GetLibsLocalTreeByLibPathID((int)LibsEntity.LIBS.BAN_PERSON);

            if (id != null)
            {
                Model.Item = BusinessLayer_PrisonerService.GetInjunctionItem(new InjunctionItemEntity(ID: id.Value)).First();
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ արգելանքները գործի փակման", RequestData: id.ToString()));

            return PartialView("BansTerminate", Model);
        }
        public ActionResult Get_BanTableRows(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.Injunctions = BusinessLayer_PrisonerService.GetInjunctions(PrisonerID: PrisonerID.Value);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել արգելանքները (get)", RequestData: PrisonerID.ToString()));

            return PartialView("Bans_Table_Row", Model);
        }
        public ActionResult Draw_BanTableRows(List<InjunctionItemEntity> Bans)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.Injunctions = new InjunctionsEntity();

            Model.Prisoner.Injunctions.Items = Bans;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել արգելանքները", RequestData: JsonConvert.SerializeObject(Bans)));

            return PartialView("Bans_Table_Row", Model);
        }
        
        //[OutputCache(Duration = 36000, VaryByParam = "id", Location = OutputCacheLocation.ServerAndClient)]
        public ActionResult Get_SentenceData(int? id)
        {
            EnteranceViewModel Model = new EnteranceViewModel();

            Model.SentencePerformingPersons = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.SENTENCE_PERFORM_PERSON);

            Model.SentenceCodes = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.SENTENCE_CODES);
            Model.propertyConfiscationLibs = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.SENTENCE_PROPERTY_CONFISCATION);

            Model.SentenceArticleTypeList = BusinessLayer_PropsLayer.GetProperty(1);

            if (id != null)
            {
                Model.sentenceData =  BusinessLayer_PrisonerService.GetSentencingData(new SentencingDataEntity(ID: id.Value)).First();

                //Model.sentenceData.SentencingDataArticles =  BusinessLayer_PrisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(SentencingDataID: Model.sentenceData.ID));
            }
            Model.SentencePunishmentType = new List<LibsEntity>();
            List<LibsEntity> PunishmentList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.SENTENCE_PUNISHMENT_TYPE);
            for (int i = 0; i < PunishmentList.Count; i++)
            {
                // calling getting lib path method from business layer
                PunishmentList[i].propList = BusinessLayer_PropsLayer.GetPropsList(PunishmentList[i].ID);
                if (PunishmentList[i].propList != null && PunishmentList[i].propList.Any())
                Model.SentencePunishmentType.Add(PunishmentList[i]);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ դատապարտման տվյալները", RequestData: id.ToString()));

            return PartialView("Sentence", Model);
        }
        public ActionResult Get_SentenceTableRows(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.AdoptionInformation = new AdoptionInformationEntity();

            Model.Prisoner.AdoptionInformation.SentencingData =  BusinessLayer_PrisonerService.GetSentencingData(new SentencingDataEntity(PrisonerID: PrisonerID.Value));

            for (int i = 0; i < Model.Prisoner.AdoptionInformation.SentencingData.Count; i++)
            {
                Model.Prisoner.AdoptionInformation.SentencingData[i].SentencingDataArticles =  BusinessLayer_PrisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(SentencingDataID: Model.Prisoner.AdoptionInformation.SentencingData[i].ID));
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել դատապարտման տվյալները (get)", RequestData: PrisonerID.ToString()));

            return PartialView("Sentence_Table_Row", Model);
        }
        public ActionResult Draw_SentenceTableRows(List<SentencingDataEntity> Data)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.AdoptionInformation = new AdoptionInformationEntity();

            Model.Prisoner.AdoptionInformation.SentencingData = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել դատապարտման տվյալները", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("Sentence_Table_Row", Model);
        }

        public ActionResult Get_SentenceProtocol(int? id)
        {
            EnteranceViewModel Model = new EnteranceViewModel();

            Model.DetectiveRank = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DETECTIVE_RANK);

            Model.QKHType = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.QKH_TYPE);

            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            int OrgUnitID = User.SelectedOrgUnitID;

            if (id != null)
            {
                Model.sentenceProtocol =  BusinessLayer_PrisonerService.GetPrisonAccessProtocol(new PrisonAccessProtocolEntity(ID: id.Value)).First();
            }

            Model.EmployeerList = BusinessLayer_OrganizatonLayer.GetOrganizationEmployees(OrgUnitID);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ դատապարտման արձանագրությունը", RequestData: id.ToString()));

            return PartialView("SentenceProtocol", Model);
        }
        public  ActionResult Get_SentenceProtocolTableRows(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.AdoptionInformation = new AdoptionInformationEntity();

            Model.Prisoner.AdoptionInformation.PrisonAccessProtocol =  BusinessLayer_PrisonerService.GetPrisonAccessProtocol(new PrisonAccessProtocolEntity(PrisonerID: PrisonerID.Value));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ դատապարտման արձանագրությունները", RequestData: PrisonerID.ToString()));

            return PartialView("SentenceProtocol_Table_Row", Model);
        }
        public ActionResult Draw_SentenceProtocolTableRows(List<PrisonAccessProtocolEntity> Data)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.AdoptionInformation = new AdoptionInformationEntity();

            Model.Prisoner.AdoptionInformation.PrisonAccessProtocol = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել դատապարտման արձանագրությունները", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("SentenceProtocol_Table_Row", Model);
        }

        public  ActionResult Get_ArrestData(int? id)
        {
            EnteranceViewModel Model = new EnteranceViewModel();

            if (id != null)
            {
                Model.arrestDataEntity =  BusinessLayer_PrisonerService.GetArrestData(new ArrestDataEntity(ID: id.Value)).First();
                //Model.arrestDataEntity.SentencingDataArticles =  BusinessLayer_PrisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(ArrestDataID: Model.arrestDataEntity.ID));
            }

            Model.SentenceCodes = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.SENTENCE_CODES);
            Model.SentencePerformingPersons = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.SENTENCE_PERFORM_PERSON);

            Model.VaruytMarmin = BusinessLayer_LibsLayer.GetLibsLocalTreeByLibPathID((int)LibsEntity.LIBS.VARUYT_MARMIN);

            Model.kalanavorumMarmin = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.KALANAVORMAN_MARMIN);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ ձերբակալման տվյալը", RequestData: id.ToString()));

            return PartialView("ArrestData", Model);
        }
        public ActionResult Get_ArrestDataTableRows(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.AdoptionInformation = new AdoptionInformationEntity();

            Model.Prisoner.AdoptionInformation.ArrestData = BusinessLayer_PrisonerService.GetArrestData(new ArrestDataEntity(PrisonerID: PrisonerID.Value));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ ձերբակալման տվյալները", RequestData: PrisonerID.ToString()));

            return PartialView("ArrestData_Table_Row", Model);
        }
        public ActionResult Draw_ArrestDataTableRows(List<ArrestDataEntity> Data)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.AdoptionInformation = new AdoptionInformationEntity();

            Model.Prisoner.AdoptionInformation.ArrestData = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել ձերբակալման տվյալները", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("ArrestData_Table_Row", Model);
        }


        public ActionResult Get_FamilyRelatives(int? id, int? PrisonerID, string type = null)
        {
            FamilyRelativesViewModel Model = new FamilyRelativesViewModel();
            
            if (id != null && type != null)
            {
                if (type == "spouse")
                {
                    Model.spouseEntity = BusinessLayer_PrisonerService.GetSpouses(new SpousesEntity(ID: id.Value)).First();
                    if (Model.spouseEntity.PersonID != null) Model.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: Model.spouseEntity.PersonID.Value, Full: true);
                }
                else if (type == "children")
                {
                    Model.childrenEntity = BusinessLayer_PrisonerService.GetChildren(new ChildrenEntity(ID: id.Value)).First();
                    if (Model.childrenEntity.PersonID != null) Model.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: Model.childrenEntity.PersonID.Value, Full: true);

                }
                else if (type == "parent")
                {
                    Model.parentEntity = BusinessLayer_PrisonerService.GetParents(new ParentsEntity(ID: id.Value)).First();
                    if (Model.parentEntity.PersonID != null) Model.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: Model.parentEntity.PersonID.Value, Full: true);

                }
                else if (type == "systerbrother")
                {
                    Model.systerEntity = BusinessLayer_PrisonerService.GetSisterBrother(new SisterBrotherEntity(ID: id.Value)).First();
                    if (Model.systerEntity.PersonID != null) Model.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: Model.systerEntity.PersonID.Value, Full: true);

                }
                else if (type == "otherrelative")
                {
                    Model.otherRelativesEntity = BusinessLayer_PrisonerService.GetOtherRelatives(new OtherRelativesEntity(ID: id.Value)).First();
                    if (Model.otherRelativesEntity.PersonID != null) Model.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: Model.otherRelativesEntity.PersonID.Value, Full: true);
                }
            }

            Model.currentTypeName = type;

            Model.Gender = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);
            for (int i = 0; i < Model.Gender.Count; i++)
            {
                // calling getting lib path method from business layer
                Model.Gender[i].propList = BusinessLayer_PropsLayer.GetPropsList(Model.Gender[i].ID);
            }

            Model.Nationality = BusinessLayer_LibsLayer.GetNationality();

            Model.Citizenships = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);

            Model.DocIdentity = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DOCIDENTITY);

            //Model.RelatedPersonList = BusinessLayer_PrisonerService.GetParentRelatedPersons(PrisonerID: PrisonerID.Value);

            Model.SysterBrotherType = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.SYSTER_BROTHER_TYPE);

            Model.RelativeType = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.RELATIVE_TYPE);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ ընտանիքի հարազատները", RequestData: JsonConvert.SerializeObject(new { id = id, PrisonerID = PrisonerID, type = type })));

            return PartialView("FamiltyRelatives", Model);
        }

        public ActionResult Get_SpousesTableRow(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.FamiltyRelative = new FamilyRelativeEntity();

            Model.Prisoner.FamiltyRelative.Spouses = BusinessLayer_PrisonerService.GetSpouses(new SpousesEntity(PrisonerID: PrisonerID.Value));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ ամուսինների ցուցակը", RequestData: PrisonerID.ToString()));

            return PartialView("FamilySpouses_Table_Row", Model);
        }
        public ActionResult Draw_SpousesTableRow(List<SpousesEntity> Data)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.FamiltyRelative = new FamilyRelativeEntity();

            Model.Prisoner.FamiltyRelative.Spouses = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել ամուսինների ցուցակը", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("FamilySpouses_Table_Row", Model);
        }
        public ActionResult Get_ChildrensTableRow(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.FamiltyRelative = new FamilyRelativeEntity();

            Model.Prisoner.FamiltyRelative.Childrens = BusinessLayer_PrisonerService.GetChildren(new ChildrenEntity(PrisonerID: PrisonerID.Value));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ երեխաների ցուցակը", RequestData: PrisonerID.ToString()));

            return PartialView("FamilyChildrens_Table_Row", Model);
        }

        public ActionResult Draw_ChildrensTableRow(List<ChildrenEntity> Data)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.FamiltyRelative = new FamilyRelativeEntity();

            Model.Prisoner.FamiltyRelative.Childrens = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել երեխաների ցուցակը", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("FamilyChildrens_Table_Row", Model);
        }
        public ActionResult Get_ParentsTableRow(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.FamiltyRelative = new FamilyRelativeEntity();

            Model.Prisoner.FamiltyRelative.Parents = BusinessLayer_PrisonerService.GetParents(new ParentsEntity(PrisonerID: PrisonerID.Value));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ ծնողների ցուցակը", RequestData: PrisonerID.ToString()));

            return PartialView("FamilyParents_Table_Row", Model);
        }

        public ActionResult Draw_ParentsTableRow(List<ParentsEntity> Data)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.FamiltyRelative = new FamilyRelativeEntity();

            Model.Prisoner.FamiltyRelative.Parents = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել ծնողների ցուցակը", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("FamilyParents_Table_Row", Model);
        }
        public ActionResult Get_SisterBrotherTableRow(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.FamiltyRelative = new FamilyRelativeEntity();

            Model.Prisoner.FamiltyRelative.SisterBrother = BusinessLayer_PrisonerService.GetSisterBrother(new SisterBrotherEntity(PrisonerID: PrisonerID.Value));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ քույրերի/եղբայրների ցուցակը", RequestData: PrisonerID.ToString()));

            return PartialView("FamilySisterBrother_Table_Row", Model);
        }

        public ActionResult Draw_SisterBrotherTableRow(List<SisterBrotherEntity> Data)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.FamiltyRelative = new FamilyRelativeEntity();

            Model.Prisoner.FamiltyRelative.SisterBrother = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել քույրերի/եղբայրների ցուցակը", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("FamilySisterBrother_Table_Row", Model);
        }
        public ActionResult Get_OtherRelativeTableRow(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.FamiltyRelative = new FamilyRelativeEntity();

            Model.Prisoner.FamiltyRelative.OtherRelatives = BusinessLayer_PrisonerService.GetOtherRelatives(new OtherRelativesEntity(PrisonerID: PrisonerID.Value));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ այլ հարազատների ցուցակը", RequestData: PrisonerID.ToString()));

            return PartialView("FamilyOtherRelative_Table_Row", Model);
        }

        public ActionResult Draw_OtherRelativeTableRow(List<OtherRelativesEntity> Data)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.FamiltyRelative = new FamilyRelativeEntity();

            Model.Prisoner.FamiltyRelative.OtherRelatives = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել ամուսինների ցուցակը", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("FamilyOtherRelative_Table_Row", Model);
        }
        public ActionResult Get_CaseCloseTerminate(int? id, int? PrisonerType)
        {
            TerminationViewModel Model = new TerminationViewModel();

            if (PrisonerType != null && PrisonerType.Value == 2)
            {
                Model.caseLibs = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TERMINATION_CASE_CONVICT);
            }
            else
            {
                Model.caseLibs = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TERMINATION_CASE_PRISONER);
            }

            if (id != null)
            {
                Model.currentTerminateEntity = BusinessLayer_PrisonerService.GetCaseClose(new CaseCloseEntity(ID: id.Value)).First();
            }

            Model.fromWhoomLib = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TERMINATION_BY_WHOOM);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ գործի փակումը", RequestData: id.ToString()));

            return PartialView("Terminate", Model);
        }
        public ActionResult Get_CaseOpenTerminate(int? id)
        {
            TerminationViewModel Model = new TerminationViewModel();

            Model.fromWhoomLib = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TERMINATION_BY_WHOOM);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ գործի փակումը/դադարեցումը", RequestData: id.ToString()));

            return PartialView("TerminateOpen", Model);
        }
        public ActionResult Get_TerminateTableRows(int? PrisonerID)
        {
            TerminationViewModel Model = new TerminationViewModel();

            Model.TerminateEntity = BusinessLayer_PrisonerService.GetCaseClose(new CaseCloseEntity(PrisonerID: PrisonerID.Value));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ գործի դադարեցումը", RequestData: PrisonerID.ToString()));

            return PartialView("Terminate_Table_Row", Model);
        }
        public ActionResult Draw_TerminateTableRows(List<CaseCloseEntity> Data)
        {
            TerminationViewModel Model = new TerminationViewModel();

            Model.TerminateEntity = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել գործի դադարեցումը", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("Terminate_Table_Row", Model);
        }
        public ActionResult Get_ArchiveOpenCase(int? id)
        {
            ArchiveViewModel Model = new ArchiveViewModel();
            
            if (id != null)
            {
                // UNDONE: note done
                Model.employees = new List<EmployeeEntity>();
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ գործի բացումը (արխիվ)", RequestData: id.ToString()));

            return PartialView("ArchiveOpenCase", Model);
        }
        public ActionResult Get_ArchiveTableRows(int? PrisonerID, string controllerName = null)
        {
            ArchiveViewModel Model = new ArchiveViewModel();

            Model.archives = BusinessLayer_PrisonerService.GetPrisonerArchive(PrisonerID: PrisonerID.Value);

            Model.controllerName = controllerName;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ գործիերի բացումը (արխիվ)", RequestData: JsonConvert.SerializeObject(new { PrisonerID = PrisonerID, controllerName = controllerName})));

            return PartialView("Archive_Table_Row", Model);
        }
        public ActionResult Draw_ArchiveTableRows(List<ArchiveEntity> Data, string controllerName = null)
        {
            ArchiveViewModel Model = new ArchiveViewModel();

            Model.archives = Data;

            Model.controllerName = controllerName;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել արխիվը", RequestData: JsonConvert.SerializeObject(new { Data = Data, controllerName = controllerName})));

            return PartialView("Archive_Table_Row", Model);
        }
        public ActionResult Draw_EncouragementTableRow(List<EncouragementsEntity> Data)
        {
            EncouragementsViewModel Model = new EncouragementsViewModel();

            Model.Encouragement = new EncouragementsObjectEntity();

            Model.Encouragement.Encouragements = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել խրախուսանքները", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("Encouragement_Table_Row", Model);
        }
        public ActionResult Get_EncouragementTableRows(int PrisonerID)
        {
            EncouragementsViewModel Model = new EncouragementsViewModel();

            Model.Encouragement = new EncouragementsObjectEntity();
            
            Model.Encouragement.Encouragements = BusinessLayer_PrisonerService.GetEncouragements(new EncouragementsEntity(PrisonerID: PrisonerID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ խրախուսանքների ցուցակը", RequestData: PrisonerID.ToString()));

            return PartialView("Encouragement_Table_Row", Model);
        }
        public ActionResult Get_Encouragement(int PrisonerID, int? ID = null)
        {
            EncouragementsViewModel Model = new EncouragementsViewModel();

            if (ID != null)
            {
                Model.currentEncouragement = BusinessLayer_PrisonerService.GetEncouragements(new EncouragementsEntity(ID: ID.Value)).First();
            }

            int PrisonerType = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: PrisonerID).Type.Value;
            int libid = (int)LibsEntity.LIBS.ENCOURAGEMENT_TYPE_CONVICT;
            if (PrisonerType == 3) libid = (int)LibsEntity.LIBS.ENCOURAGEMENT_TYPE_PRISONER;
            Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID(libid);

            Model.BaseLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ENCOURAGEMENT_BASE);
            Model.EmployeeList = BusinessLayer_OrganizatonLayer.GetOrganizationEmployees(ViewBag.SelectedOrgUnitID);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ խրախուսանքը", RequestData: ID.ToString()));

            return PartialView("Encouragement", Model);
        }
        public ActionResult Get_Penalty(int PrisonerID, int? ID = null)
        {
            PenaltiesViewModel Model = new PenaltiesViewModel();

            if (ID != null)
            {
                Model.currentPenalty = BusinessLayer_PrisonerService.GetPenalties(new PenaltiesEntity(ID: ID.Value)).First();
            }
            int PrisonerType = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: PrisonerID).Type.Value;
            int libid = (int)LibsEntity.LIBS.PENALTY_TYPE_CONVICT;
            if (PrisonerType == 3) libid = (int)LibsEntity.LIBS.PENALTY_TYPE_PRISONER;
            Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID(libid);
            Model.ViolationLibList = BusinessLayer_LibsLayer.GetLibsLocalTreeByLibPathID((int)LibsEntity.LIBS.PENALTY_VIOLATION);
            Model.StateLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PENALTY_STATE);
            Model.StateBaseLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PENALTY_STATE_BASE);
            Model.EmployeeList = BusinessLayer_OrganizatonLayer.GetOrganizationEmployees(ViewBag.SelectedOrgUnitID);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ տույժը", RequestData: ID.ToString()));

            return PartialView("Penalty", Model);
        }
        public ActionResult Draw_PenaltyTableRow(List<PenaltiesEntity> Data)
        {
            PenaltiesViewModel Model = new PenaltiesViewModel();

            Model.Penalty = new PenaltiesObjectEntity();

            Model.Penalty.Penalties = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել տույժերը", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("Penalty_Table_Row", Model);
        }
        public ActionResult Get_PenaltyTableRows(int PrisonerID)
        {
            PenaltiesViewModel Model = new PenaltiesViewModel();

            Model.Penalty = new PenaltiesObjectEntity();

            Model.Penalty.Penalties = BusinessLayer_PrisonerService.GetPenalties(new PenaltiesEntity(PrisonerID: PrisonerID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ տույժերի ցուցակը", RequestData: PrisonerID.ToString()));

            return PartialView("Penalty_Table_Row", Model);
        }
        public ActionResult Get_PenaltyObjection(int? ID = null)
        {
            PenaltiesViewModel Model = new PenaltiesViewModel();

            if (ID != null)
            {
                Model.currentPenalty = BusinessLayer_PrisonerService.GetPenalties(new PenaltiesEntity(ID: ID.Value)).First();
            }

            Model.AppealsContentLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PENALTY_STATE_BASE);

            List<LibsEntity> FirstList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.SENTENCE_PERFORM_PERSON);

            List<LibsEntity> SecondList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PENALTY_PROTESTER);

            Model.AppealsBodyList = FirstList;
            Model.AppealsBodyList.AddRange(SecondList);

            Model.AppealResultLibItemID = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PENALTY_PROTEST_RESULT);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ տույժերի առարկությունը", RequestData: ID.ToString()));

            return PartialView("PenaltyObjection", Model);
        }
        public ActionResult Get_PenaltyObjectionResult(int? ID = null)
        {
            PenaltiesViewModel Model = new PenaltiesViewModel();

            if (ID != null)
            {
                Model.currentPenalty = BusinessLayer_PrisonerService.GetPenalties(new PenaltiesEntity(ID: ID.Value)).First();
            }

            Model.AppealResultLibItemID = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PENALTY_PROTEST_RESULT);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ տույժերի առարկության արդյունքը", RequestData: ID.ToString()));

            return PartialView("PenaltyObjectionResult", Model);
        }
        public ActionResult Draw_EducationalCourseTableRow(List<EducationalCoursesEntity> Data)
        {
            EducationalCoursesViewModel Model = new EducationalCoursesViewModel();

            Model.EducationalCourse = new EducationalCoursesObjectEntity();

            Model.EducationalCourse.EducationCourses = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել կրթական կուրսերը", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("EducationalCourse_Table_Row", Model);
        }
        public ActionResult Get_EducationalCourseTableRows(int PrisonerID)
        {
            EducationalCoursesViewModel Model = new EducationalCoursesViewModel();

            Model.EducationalCourse = new EducationalCoursesObjectEntity();

            Model.EducationalCourse.EducationCourses = BusinessLayer_PrisonerService.GetEducationalCourses(new EducationalCoursesEntity(PrisonerID: PrisonerID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ կրթական կուրսերի ցուցակը", RequestData: PrisonerID.ToString()));

            return PartialView("EducationalCourse_Table_Row", Model);
        }
        public ActionResult Get_EducationalCourse(int? ID = null)
        {
            EducationalCoursesViewModel Model = new EducationalCoursesViewModel();

            if (ID != null)
            {
                Model.currentEducationalCourse = BusinessLayer_PrisonerService.GetEducationalCourses(new EducationalCoursesEntity(ID: ID.Value)).First();
            }

            Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.EDUCATIONAL_COURSES_TYPE);
            Model.EngagementBasisLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.EDUCATIONAL_COURSES_ENG);
            Model.ReleaseBasisLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.EDUCATIONAL_COURSES_REL);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ կրթական կուրսը", RequestData: ID.ToString()));

            return PartialView("EducationalCourse", Model);
        }

        #region WorkLoads
        public ActionResult Draw_WorkLoadTableRow(List<WorkLoadsEntity> Data)
        {
            WorkLoadsViewModel Model = new WorkLoadsViewModel();

            Model.WorkLoad = new WorkLoadsObjectEntity();

            Model.WorkLoad.WorkLoads = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել աշխատանքները", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("WorkLoad_Table_Row", Model);
        }
        public ActionResult Get_WorkLoadTableRows(int PrisonerID)
        {
            WorkLoadsViewModel Model = new WorkLoadsViewModel();

            Model.WorkLoad = new WorkLoadsObjectEntity();

            Model.WorkLoad.WorkLoads = BusinessLayer_PrisonerService.GetWorkLoads(new WorkLoadsEntity(PrisonerID: PrisonerID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ աշխատանքները", RequestData:  PrisonerID.ToString()));

            return PartialView("WorkLoad_Table_Row", Model);
        }
        public ActionResult Get_WorkLoad(int? ID = null)
        {
            WorkLoadsViewModel Model = new WorkLoadsViewModel();

            if (ID != null)
            {
                Model.currentWorkLoad = BusinessLayer_PrisonerService.GetWorkLoads(new WorkLoadsEntity(ID: ID.Value)).First();
            }

            Model.WorkTitleLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.WORKLOADS_TITLE);
            Model.JobTypeLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.WORKLOADS_TYPE);
            Model.EmployerLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.WORKLOADS_EMP);
            Model.ReleaseBasisLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.WORKLOADS_REL);
            Model.EngagementBasisLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.WORKLOADS_ENG);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ աշխատանքը", RequestData: ID.ToString()));

            return PartialView("WorkLoad", Model);
        }
        #endregion

        #region Conflicts
        public ActionResult Draw_ConflictMainTableRow(List<ConflictsEntity> Data)
        {
            ConflictsViewModel Model = new ConflictsViewModel();

            Model.Conflict = new ConflictsObjectEntity();

            Model.Conflict.Conflicts = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("ConflictMain_Table_Row", Model);
        }
        public ActionResult Get_ConflictMainTableRows(int PrisonerID)
        {
            ConflictsViewModel Model = new ConflictsViewModel();

            Model.Conflict = new ConflictsObjectEntity();

            Model.Conflict.Conflicts = BusinessLayer_PrisonerService.GetConflictsMain(new ConflictsEntity(PrisonerID: PrisonerID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: PrisonerID.ToString()));

            return PartialView("ConflictMain_Table_Row", Model);
        }
        public ActionResult Draw_ConflictTableRow(List<ConflictsEntity> Data)
        {
            ConflictsViewModel Model = new ConflictsViewModel();

            Model.Conflict = new ConflictsObjectEntity();

            foreach (var item in Data)
            {
                if (item.PersonID != null)
                {
                    item.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: item.PersonID, Full: true);
                }
            }

            Model.Conflict.Conflicts = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("Conflict_Table_Row", Model);
        }
        public ActionResult Get_ConflictTableRows(int PrisonerID)
        {
            ConflictsViewModel Model = new ConflictsViewModel();

            Model.Conflict = new ConflictsObjectEntity();

            Model.Conflict.Conflicts = BusinessLayer_PrisonerService.GetConflicts(new ConflictsEntity(PrisonerID: PrisonerID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: PrisonerID.ToString()));

            return PartialView("Conflict_Table_Row", Model);
        }
        public ActionResult Get_Conflict(int? ID = null)
        {
            ConflictsViewModel Model = new ConflictsViewModel();

            Model.currentConflict = new ConflictsEntity();

            if (ID != null)
            {
                Model.currentConflict = BusinessLayer_PrisonerService.GetConflicts(new ConflictsEntity(ID: ID.Value)).First();

                Model.Person = Model.currentConflict.Person;
            }

            Model.Nationality = BusinessLayer_LibsLayer.GetNationality();

            Model.DocIdentity = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DOCIDENTITY);
            Model.Gender = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);
            for (int i = 0; i < Model.Gender.Count; i++)
            {
                // calling getting lib path method from business layer
                Model.Gender[i].propList = BusinessLayer_PropsLayer.GetPropsList(Model.Gender[i].ID);
            }
            Model.Citizenships = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ աշխատանքը", RequestData: ID.ToString()));

            return PartialView("Conflict", Model);
        }
        #endregion
        public ActionResult Draw_OfficialVisitTableRow(List<OfficialVisitsEntity> Data)
        {
            OfficialVisitsViewModel Model = new OfficialVisitsViewModel();

            Model.OfficialVisit = new OfficialVisitsObjectEntity();

            Model.OfficialVisit.Visits = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել այցելությունները", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("OfficialVisit_Table_Row", Model);
        }
        public ActionResult Draw_DepartureTableRow(List<DepartureEntity> Data)
        {
            DepartureViewModel Model = new DepartureViewModel();

            Model.Departure = new DepartureObjectEntity();

            Model.Departure.Departures = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել մեկնումները", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("Departure_Table_Row", Model);
        }
        public ActionResult Draw_TransferTableRow(List<TransferEntity> Data)
        {
            TransferViewModel Model = new TransferViewModel();

            Model.Transfer = new TransferObjectEntity();

            Model.Transfer.Transfers = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել փոխանցումները", RequestData: JsonConvert.SerializeObject(Data)));
            Model.CurrentOrgUnitID = ViewBag.SelectedOrgUnitID;

            return PartialView("Transfer_Table_Row", Model);
        }
        public ActionResult Draw_PackagesTableRow(List<PackagesEntity> Data)
        {
            PackagesViewModel Model = new PackagesViewModel();

            Model.Package = new PackagesObjectEntity();

            Model.Package.Packages = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել փաթեթները", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("Packages_Table_Row", Model);
        }
        public ActionResult Get_DepartureTableRows(int PrisonerID)
        {
            DepartureViewModel Model = new DepartureViewModel();

            Model.Departure = new DepartureObjectEntity();

            Model.Departure.Departures = BusinessLayer_PrisonerService.GetDeparture(new DepartureEntity(PrisonerID: PrisonerID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ մեկնումների ցուցակը", RequestData: PrisonerID.ToString()));

            return PartialView("Departure_Table_Row", Model);
        }
        public ActionResult Get_TransferTableRows(int PrisonerID)
        {
            TransferViewModel Model = new TransferViewModel();

            Model.Transfer = new TransferObjectEntity();

            Model.Transfer.Transfers = BusinessLayer_PrisonerService.GetTransfer(new TransferEntity(PrisonerID: PrisonerID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ փոխանցումների ցուցակը", RequestData: PrisonerID.ToString()));

            Model.CurrentOrgUnitID = ViewBag.SelectedOrgUnitID;

            return PartialView("Transfer_Table_Row", Model);
        }
        public ActionResult Get_Departure(int? ID = null, int? PrisonerID= null)
        {
            DepartureViewModel Model = new DepartureViewModel();

            if (ID != null)
            {
                Model.currentDeparture = BusinessLayer_PrisonerService.GetDeparture(new DepartureEntity(ID: ID.Value)).First();

                if (Model.currentDeparture != null)
                {
                    Model.Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Model.currentDeparture.PrisonerID.Value);
                }
            }
            else if (PrisonerID != null)
            {
                Model.Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: PrisonerID.Value);
            }

            Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_TYPE);
            Model.PurposeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_PURPOSE);
            Model.MedLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_MED);
            Model.CaseLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_CASE);
            Model.InvestigateLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_INVESTIGATE);
            Model.MoveLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_MOVE);
            Model.CourtLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_COURT);
            Model.HospitalLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DEPARTURES_HOSPITAL);
            Model.OrgUnitList = BusinessLayer_OrganizatonLayer.GetOrgUnitByTypeID(TypeID: 3);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ մեկնումը", RequestData: ID.ToString()));

            return PartialView("Departure", Model);
        }
        public ActionResult Get_Transfer(int? ID = null, int? PrisonerID = null)
        {
            TransferViewModel Model = new TransferViewModel();

            bool ArchiveData = false;

            if (ID != null)
            {
                Model.currentTransfer = BusinessLayer_PrisonerService.GetTransfer(new TransferEntity(ID: ID.Value)).First();
                if (Model.currentTransfer != null)
                {
                    Model.currentTransfer.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: Model.currentTransfer.PersonID, Full: true);
                }

                if (Model.currentTransfer != null)
                {
                    PrisonerEntity Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Model.currentTransfer.PrisonerID.Value);
                    if (Prisoner.ArchiveData != null)
                    {
                        ArchiveData = Prisoner.ArchiveData.Value;
                    }
                }
            }
            else if (PrisonerID != null)
            {
                PrisonerEntity Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: PrisonerID.Value);
                if (Prisoner.ArchiveData != null)
                {
                    ArchiveData = Prisoner.ArchiveData.Value;
                }
            }

            Model.Prisoner = new PrisonerEntity();
            Model.Prisoner.ArchiveData = ArchiveData;

            Model.TransferTypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_TYPE);
            Model.PurposeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_PURPOSE);
            Model.TransferLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER);
            Model.HospitalLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_HOSPITAL);
            Model.CourtLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_COURT);
            Model.StateLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_STATE);
            Model.EndLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_END);
            Model.EmergencyLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_EMERGENCY);
            Model.OrgUnitList = BusinessLayer_OrganizatonLayer.GetOrgUnitByTypeID(TypeID: 3);

            Model.QnnchakanLibList = BusinessLayer_LibsLayer.GetLibsLocalTreeByLibPathID((int)LibsEntity.LIBS.VARUYT_MARMIN);
            Model.HogebujaranLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_HOGEBUJARAN);
            Model.QKHHospitalLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_QKHHOSPITAL);
            Model.BjshkakanTexLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_BJSHKAKANTEX);
            Model.VaruytTexLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_VARUYTTEX);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ փոխանցումը", RequestData: ID.ToString()));

            return PartialView("Transfer", Model);
        }
        public ActionResult Get_OfficialVisitTableRows(int PrisonerID)
        {
            OfficialVisitsViewModel Model = new OfficialVisitsViewModel();

            Model.OfficialVisit = new OfficialVisitsObjectEntity();

            Model.OfficialVisit.Visits = BusinessLayer_PrisonerService.GetOfficialVisits(new OfficialVisitsEntity(PrisonerID: PrisonerID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ պաշտոնական այցելությունների ցուցակը", RequestData: PrisonerID.ToString()));

            return PartialView("OfficialVisit_Table_Row", Model);
        }
        public ActionResult Get_OfficialVisit(int? ID = null)
        {
            OfficialVisitsViewModel Model = new OfficialVisitsViewModel();

            if (ID != null)
            {
                Model.currentOfficialVisit = BusinessLayer_PrisonerService.GetOfficialVisits(new OfficialVisitsEntity(ID: ID.Value)).First();

                if (Model.currentOfficialVisit != null)
                {
                    Model.currentOfficialVisit.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: Model.currentOfficialVisit.PersonID, Full: true);
                }
            }

            Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.OFFICIAL_VISITS_TYPE);
            Model.PositionLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.OFFICIAL_VISITS_POSITION);

            Model.Nationality = BusinessLayer_LibsLayer.GetNationality();
            Model.DocIdentity = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DOCIDENTITY);
            Model.Gender = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);
            for (int i = 0; i < Model.Gender.Count; i++)
            {
                // calling getting lib path method from business layer
                Model.Gender[i].propList = BusinessLayer_PropsLayer.GetPropsList(Model.Gender[i].ID);
            }
            Model.Citizenships = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ պաշտոնական այցելությունը", RequestData: ID.ToString()));

            return PartialView("OfficialVisit", Model);
        }
        public ActionResult Get_OfficialVisitPersonTableRow(int? PersonID = null)
        {
            OfficialVisitsViewModel Model = new OfficialVisitsViewModel();

            Model.PersonList = new List<PersonEntity>();

            if (PersonID != null)
            {
                PersonEntity Person = BusinessLayer_PrisonerService.GetPerson(PersonID: PersonID.Value);

                Model.PersonList.Add(Person);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ այցելուների ցուցակը", RequestData: PersonID.ToString()));

            return PartialView("OfficialVisitPerson_Table_Row", Model);
        }

        public ActionResult Draw_OfficialVisitPersonTableRow(List<PersonEntity> Person)
        {
            OfficialVisitsViewModel Model = new OfficialVisitsViewModel();

            Model.PersonList = Person;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել պաշտոնական այցելուների ցուցակը", RequestData: JsonConvert.SerializeObject(Person)));

            return PartialView("OfficialVisitPerson_Table_Row", Model);
        }
        public ActionResult Draw_PersonalVisitTableRow(List<PersonalVisitsEntity> Data)
        {
            PersonalVisitsViewModel Model = new PersonalVisitsViewModel();

            Model.PersonalVisit = new PersonalVisitsObjectEntity();

            Model.PersonalVisit.Visits = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել այցելուների ցուցակը", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("PersonalVisit_Table_Row", Model);
        }
        public ActionResult Get_PersonalVisitTableRows(int PrisonerID)
        {
            PersonalVisitsViewModel Model = new PersonalVisitsViewModel();

            Model.PersonalVisit = new PersonalVisitsObjectEntity();

            Model.PersonalVisit.Visits = BusinessLayer_PrisonerService.GetPersonalVisits(new PersonalVisitsEntity(PrisonerID: PrisonerID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ այցելուների ցուցակը", RequestData: PrisonerID.ToString()));

            return PartialView("PersonalVisit_Table_Row", Model);
        }
        public ActionResult Get_PersonalVisit(int? ID = null, int? PrisonerID = null)
        {
            PersonalVisitsViewModel Model = new PersonalVisitsViewModel();

            Model.Prisoner = new PrisonerEntity();

            if (ID != null)
            {
                Model.currentPersonalVisit = BusinessLayer_PrisonerService.GetPersonalVisits(new PersonalVisitsEntity(ID: ID.Value)).First();

                Model.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: Model.currentPersonalVisit.PersonID, Full: true);

                if (PrisonerID == null && Model.currentPersonalVisit != null)
                {
                    Model.Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Model.currentPersonalVisit.PrisonerID.Value);
                }
            }

            Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PERSONAL_VISITS_TYPE);
            Model.PurposeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PERSONAL_VISITS_PURPOSE);
            Model.RelativePersonList = new List<PersonEntity>();
            
            if (PrisonerID != null)
            {
                PrisonerEntity Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: PrisonerID.Value);
                Model.Prisoner = Prisoner;

                List<SpousesEntity> spouses = BusinessLayer_PrisonerService.GetSpouses(new SpousesEntity(PrisonerID: PrisonerID.Value));
                foreach (SpousesEntity curEntity in spouses)
                {
                    curEntity.Person.RelativeLabel = "Ամուսին";
                    Model.RelativePersonList.Add(curEntity.Person);
                }
                List<ChildrenEntity> children = BusinessLayer_PrisonerService.GetChildren(new ChildrenEntity(PrisonerID: PrisonerID.Value));
                foreach (ChildrenEntity curEntity in children)
                {
                    curEntity.Person.RelativeLabel = "Երեխա";
                    Model.RelativePersonList.Add(curEntity.Person);
                }
                List<ParentsEntity> parent = BusinessLayer_PrisonerService.GetParents(new ParentsEntity(PrisonerID: PrisonerID.Value));
                foreach (ParentsEntity curEntity in parent)
                {
                    curEntity.Person.RelativeLabel = "Հայր/Մայր";
                    Model.RelativePersonList.Add(curEntity.Person);
                }
                List<SisterBrotherEntity> systerbrother = BusinessLayer_PrisonerService.GetSisterBrother(new SisterBrotherEntity(PrisonerID: PrisonerID.Value));
                foreach (SisterBrotherEntity curEntity in systerbrother)
                {
                    curEntity.Person.RelativeLabel = "Քույր/Եղբայր";
                    Model.RelativePersonList.Add(curEntity.Person);
                }
                List<OtherRelativesEntity> otherrelatives = BusinessLayer_PrisonerService.GetOtherRelatives(new OtherRelativesEntity(PrisonerID: PrisonerID.Value));
                foreach (OtherRelativesEntity curEntity in otherrelatives)
                {
                    curEntity.Person.RelativeLabel = "Այլ";
                    Model.RelativePersonList.Add(curEntity.Person);
                }
            }

            Model.Nationality = BusinessLayer_LibsLayer.GetNationality();
            Model.DocIdentity = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DOCIDENTITY);
            Model.Gender = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);
            for (int i = 0; i < Model.Gender.Count; i++)
            {
                // calling getting lib path method from business layer
                Model.Gender[i].propList = BusinessLayer_PropsLayer.GetPropsList(Model.Gender[i].ID);
            }
            Model.Citizenships = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ այցելությունը", RequestData: JsonConvert.SerializeObject(new { ID = ID, PrisonerID = PrisonerID})));

            return PartialView("PersonalVisit", Model);
        }
        public ActionResult Get_PersonalVisitPersonTableRow(int? PersonID = null)
        {
            PersonalVisitsViewModel Model = new PersonalVisitsViewModel();

            Model.PersonList = new List<PersonEntity>();

            if (PersonID != null)
            {
                PersonEntity Person = BusinessLayer_PrisonerService.GetPerson(PersonID: PersonID.Value);

                Model.PersonList.Add(Person);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ այցելուների ցուցակը", RequestData: PersonID.ToString()));

            return PartialView("PersonalVisitPerson_Table_Row", Model);
        }
        public ActionResult Draw_PersonalVisitPersonTableRow(List<PersonEntity> Person)
        {
            PersonalVisitsViewModel Model = new PersonalVisitsViewModel();

            Model.PersonList = Person;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել այցելուների ցուցակը", RequestData: JsonConvert.SerializeObject(Person)));

            return PartialView("PersonalVisitPerson_Table_Row", Model);
        }

        public ActionResult Get_Package(int? ID = null, int? PrisonerID = null)
        {
            PackagesViewModel Model = new PackagesViewModel();

            if (ID != null)
            {
                Model.currentPackage = BusinessLayer_PrisonerService.GetPackages(new PackagesEntity(ID: ID.Value)).First();

                if (Model.currentPackage != null && Model.currentPackage.PersonID != null)
                {
                    if (Model.currentPackage.PersonID != null)
                        Model.currentPackage.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: Model.currentPackage.PersonID, Full: true);

                    Model.Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Model.currentPackage.PrisonerID.Value);
                }

            }
            else if (PrisonerID != null)
            {
                Model.Prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: PrisonerID.Value);
            }

            Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PACHACKE_TYPE);
            Model.TypeContentLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PACKAGE_CONTENT_TYPE);
            Model.GoodsLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PACKAGE_CONTENT_GOODS);
            Model.MeasureLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PACKAGE_CONTENT_MEASURES);
            Model.EmployeeList = BusinessLayer_OrganizatonLayer.GetOrganizationEmployees(ViewBag.SelectedOrgUnitID);

            Model.Nationality = BusinessLayer_LibsLayer.GetNationality();
            Model.DocIdentity = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DOCIDENTITY);
            Model.Gender = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);
            for (int i = 0; i < Model.Gender.Count; i++)
            {
                // calling getting lib path method from business layer
                Model.Gender[i].propList = BusinessLayer_PropsLayer.GetPropsList(Model.Gender[i].ID);
            }
            Model.Citizenships = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ փաթեթը", RequestData: JsonConvert.SerializeObject(new { ID = ID, PrisonerID = PrisonerID})));

            return PartialView("Package", Model);
        }
        public ActionResult Get_PackageApprove()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ փաթեթի հաստատումը"));

            return PartialView("PackageApprove");
        }
        public ActionResult Draw_PackageContentTableRow(List<PackageContentEntity> Package)
        {
            PackagesViewModel Model = new PackagesViewModel();

            Model.PackageList = Package;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել փաթեթի պարունակության ցուցակը", RequestData: JsonConvert.SerializeObject(Package)));

            return PartialView("PackageContent_Table_Row", Model);
        }
        public ActionResult Draw_PackageTableRow(List<PackagesEntity> Data)
        {
            PackagesViewModel Model = new PackagesViewModel();

            Model.Package = new PackagesObjectEntity();

            Model.Package.Packages = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել փաթեթների ցուցակը", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("Package_Table_Row", Model);
        }

        public ActionResult Get_PackageTableRows(int? PrisonerID = null)
        {
            PackagesViewModel Model = new PackagesViewModel();

            Model.Package = new PackagesObjectEntity();

            if (PrisonerID != null)
            {
                Model.Package.Packages = BusinessLayer_PrisonerService.GetPackages(new PackagesEntity(PrisonerID: PrisonerID.Value));

            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ փաթեթների ցուցակը", RequestData: PrisonerID.ToString()));

            return PartialView("Package_Table_Row", Model);
        }
        public ActionResult Get_DepartureApprove()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ մեկնումների հաստատումը"));

            return PartialView("PackageApprove");
        }
        public ActionResult Get_TransferApprove()
        {
            TransferViewModel Model = new TransferViewModel();
            Model.EndLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_END);
            Model.StateLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.TRANSFER_STATE);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ փոխանցումների հաստատումը"));
            return PartialView("TransferApprove",Model);
        }
        public ActionResult Get_CompleteData()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ ամբողջական տվյալները"));

            return PartialView("CompleteData");
        }
        public ActionResult Get_CompletePenalty()
        {
            PenaltiesViewModel Model = new PenaltiesViewModel();

            Model.StateBaseLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PENALTY_STATE_BASE);
            
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ ամբողջական տվյալները"));

            return PartialView("CompletePenalty", Model);
        }

        #region Complaints
        public ActionResult Get_MedicalComplaints(int? ID = null)
        {
            MedicalViewModel Model = new MedicalViewModel();

            if (ID != null)
            {
                Model.currentMedicalComplaint = BL_MedicalLayer.GetList_MedicalComplaints(new MedicalComplaintsEntity(ID: ID.Value)).First();
            }

            Model.ComplaintsLibItemList = BusinessLayer_LibsLayer.GetLibsLocalTreeByLibPathID((int)LibsEntity.LIBS.MEDICAL_COMPLAINT);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: ID.ToString()));

            return PartialView("MedicalComplaint", Model);
        }

        public ActionResult Draw_MedicalComplaintTableRow(List<MedicalComplaintsEntity> Data)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.Medical = new MedicalObjectEntity();

            Model.Medical.MedicalComplaints = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("MedicalComplaint_Table_Row", Model);
        }
        public ActionResult Get_MedicalComplaintTableRows(MedicalComplaintsEntity entity)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.Medical = new MedicalObjectEntity();

            Model.Medical.MedicalComplaints = BL_MedicalLayer.GetList_MedicalComplaints(new MedicalComplaintsEntity(PrisonerID: entity.PrisonerID, MedicalHistoryID: entity.MedicalHistoryID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: entity.PrisonerID.ToString()));

            return PartialView("MedicalComplaint_Table_Row", Model);
        }
        #endregion

        #region ExternalExaminations
        public ActionResult Get_MedicalExternalExaminations(int? ID = null)
        {
            MedicalViewModel Model = new MedicalViewModel();

            if (ID != null)
            {
                Model.currentMedicalExternalExamination = BL_MedicalLayer.GetList_MedicalExternalExamination(new MedicalExternalExaminationEntity(ID: ID.Value)).First();
            }
           
            Model.ComplaintsLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.MEDICAL_COMPLAINT);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: ID.ToString()));

            return PartialView("MedicalExternalExamination", Model);
        }

        public ActionResult Draw_ExternalExaminationTableRow(List<MedicalExternalExaminationEntity> Data)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.Medical = new MedicalObjectEntity();

            Model.Medical.MedicalExternalExaminations = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("MedicalExternalExamination_Table_Row", Model);
        }
        public ActionResult Get_ExternalExaminationTableRows(MedicalExternalExaminationEntity entity)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.Medical = new MedicalObjectEntity();

            Model.Medical.MedicalExternalExaminations = BL_MedicalLayer.GetList_MedicalExternalExamination(new MedicalExternalExaminationEntity(PrisonerID: entity.PrisonerID, MedicalHistoryID: entity.MedicalHistoryID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: entity.PrisonerID.ToString()));

            return PartialView("MedicalExternalExamination_Table_Row", Model);
        }
        #endregion

        #region Diagnosis
        public ActionResult Get_MedicalDiagnosis(int? ID = null)
        {
            MedicalViewModel Model = new MedicalViewModel();

            if (ID != null)
            {
                Model.currentMedicalDiagnosis = BL_MedicalLayer.GetList_MedicalDiagnosis(new MedicalDiagnosisEntity(ID: ID.Value)).First();
            }

            Model.DiagnosisLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.MEDICAL_DIAGNOSIS);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: ID.ToString()));

            return PartialView("MedicalDiagnosis", Model);
        }

        public ActionResult Draw_MedicalDiagnosisTableRow(List<MedicalDiagnosisEntity> Data)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.Medical = new MedicalObjectEntity();

            Model.Medical.MedicalDiagnosis = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("MedicalDiagnosis_Table_Row", Model);
        }
        public ActionResult Get_MedicalDiagnosisTableRows(MedicalDiagnosisEntity entity)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.Medical = new MedicalObjectEntity();

            Model.Medical.MedicalDiagnosis = BL_MedicalLayer.GetList_MedicalDiagnosis(new MedicalDiagnosisEntity(PrisonerID: entity.PrisonerID, MedicalHistoryID: entity.MedicalHistoryID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: entity.PrisonerID.ToString()));

            return PartialView("MedicalDiagnosis_Table_Row", Model);
        }
        #endregion

        #region Research
        public ActionResult Get_MedicalResearch(int? ID = null)
        {
            MedicalViewModel Model = new MedicalViewModel();

            if (ID != null)
            {
                Model.currentMedicalResearch = BL_MedicalLayer.GetList_MedicalResearch(new MedicalResearchEntity(ID: ID.Value)).First();
            }

            Model.ResearchLibItemList = BusinessLayer_LibsLayer.GetLibsLocalTreeByLibPathID((int)LibsEntity.LIBS.MEDICAL_RESEARCH);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: ID.ToString()));

            return PartialView("MedicalResearch", Model);
        }

        public ActionResult Draw_MedicalResearchTableRow(List<MedicalResearchEntity> Data)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.MedicalHistory = new MedicalHistoryObjectEntity();

            Model.MedicalHistory.MedicalResearch = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("MedicalResearch_Table_Row", Model);
        }
        public ActionResult Get_MedicalResearchTableRows(MedicalResearchEntity entity)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.MedicalHistory = new MedicalHistoryObjectEntity();

            Model.MedicalHistory.MedicalResearch = BL_MedicalLayer.GetList_MedicalResearch(new MedicalResearchEntity(PrisonerID: entity.PrisonerID, MedicalHistoryID: entity.MedicalHistoryID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: entity.PrisonerID.ToString()));

            return PartialView("MedicalResearch_Table_Row", Model);
        }
        #endregion

        #region Healing
        public ActionResult Get_MedicalHealing(int? ID = null)
        {
            MedicalViewModel Model = new MedicalViewModel();

            if (ID != null)
            {
                Model.currentMedicalDiagnosis = BL_MedicalLayer.GetList_MedicalDiagnosis(new MedicalDiagnosisEntity(ID: ID.Value)).First();
            }

            Model.DiagnosisLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.MEDICAL_DIAGNOSIS);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: ID.ToString()));

            return PartialView("MedicalHealing", Model);
        }

        public ActionResult Draw_MedicalHealingTableRow(List<MedicalDiagnosisEntity> Data)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.Medical = new MedicalObjectEntity();

            Model.Medical.MedicalDiagnosis = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("MedicalHealing_Table_Row", Model);
        }
        public ActionResult Get_MedicalHealingTableRows(MedicalDiagnosisEntity entity)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.Medical = new MedicalObjectEntity();

            Model.Medical.MedicalDiagnosis = BL_MedicalLayer.GetList_MedicalDiagnosis(new MedicalDiagnosisEntity(PrisonerID: entity.PrisonerID, MedicalHistoryID: entity.MedicalHistoryID));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: entity.PrisonerID.ToString()));

            return PartialView("MedicalHealing_Table_Row", Model);
        }
        #endregion

        #region MedicalHistory

        public ActionResult Get_MedicalHistoryResult(int? ID = null)
        {
            MedicalViewModel Model = new MedicalViewModel();
            
            if (ID != null)
            {
                Model.currentMedicalHistory = BL_MedicalLayer.GetList_MedicalHistory(new MedicalHistoryEntity(ID: ID.Value)).First();
            }

            Model.ResultLibItemList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.MEDICAL_HISTORY_RESULT);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: ID.ToString()));

            return PartialView("MedicalHistoryResult", Model);
        }
        public ActionResult Get_MedicalHistory(int? ID = null, int? HistoryType = null)
        {
            MedicalViewModel Model = new MedicalViewModel();

            if (ID != null)
            {
                Model.currentMedicalHistory = BL_MedicalLayer.GetList_MedicalHistory(new MedicalHistoryEntity(ID: ID.Value)).First();
                Model.HistoryType = Model.currentMedicalHistory.Type;
            }
            else
            {
                Model.HistoryType = HistoryType;
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: ID.ToString()));

            return PartialView("MedicalHistory", Model);
        }

        public ActionResult Draw_MedicalHistoryTableRow(List<MedicalHistoryEntity> Data)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.Medical = new MedicalObjectEntity();

            Model.Medical.MedicalHistories = Data;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: JsonConvert.SerializeObject(Data)));

            return PartialView("MedicalHistory_Table_Row", Model);
        }
        public ActionResult Get_MedicalHistoryTableRows(MedicalHistoryEntity entity)
        {
            MedicalViewModel Model = new MedicalViewModel();

            Model.Medical = new MedicalObjectEntity();

            Model.Medical.MedicalHistories = BL_MedicalLayer.GetList_MedicalHistory(new MedicalHistoryEntity(PrisonerID: entity.PrisonerID, Type: entity.Type));

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: entity.PrisonerID.ToString()));

            return PartialView("MedicalHistory_Table_Row", Model);
        }
        #endregion

        #region PreviousConviction
        public ActionResult Get_PrevConviction(int? id)
        {
            PrevConvictionViewModel Model = new PrevConvictionViewModel();

            Model.prevConvictionData = new PreviousConvictionsEntity();

            Model.SentenceCodes = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.SENTENCE_CODES);

            for (int i = 0; i < Model.SentenceCodes.Count; i++)
            {
                Model.SentenceCodes[i].subitems = BusinessLayer_LibsLayer.GetLibsLocalTreeWithPropertiesByLibParentID(Model.SentenceCodes[i].ID);
            }

            if (id != null)
            {
                Model.prevConvictionData.ID = id;

                Model.prevConvictionData = BusinessLayer_PrisonerService.GetPreviousConviction(new PreviousConvictionsEntity(ID: id.Value)).First();

                //Model.prevConvictionData.SentencingDataArticles = BusinessLayer_PrisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(PrevConvictionDataID: id));
            }

            Model.SentencePunishmentType = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.SENTENCE_PUNISHMENT_TYPE);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: id.ToString()));

            return PartialView("PrevConviction", Model);
        }

        public ActionResult Get_PreviousConvictionsTableRows(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.MainData = new MainDataEntity();

            if (PrisonerID != null)
            {
                Model.Prisoner.MainData.PreviousConvictions = BusinessLayer_PrisonerService.GetPreviousConviction(new PreviousConvictionsEntity(PrisonerID: PrisonerID.Value));

                List<int> Codes = new List<int>();
                if (Model.Prisoner.MainData.PreviousConvictions != null && Model.Prisoner.MainData.PreviousConvictions.Any())
                {
                    foreach (PreviousConvictionsEntity item in Model.Prisoner.MainData.PreviousConvictions)
                    {
                        if (!Codes.FindAll(r => r == item.CodeLibItemID).Any())
                        {
                            item.SentencingArticles = BusinessLayer_LibsLayer.GetLibsLocalTreeWithPropertiesByLibParentID(item.CodeLibItemID.Value);
                            Codes.Add(item.CodeLibItemID.Value);
                        }
                        else
                        {
                            if (Model.Prisoner.MainData.PreviousConvictions.FindAll(r => r.CodeLibItemID.Value == item.CodeLibItemID.Value && r.SentencingArticles != null && r.SentencingArticles.Any()).Any())
                            {
                                item.SentencingArticles = Model.Prisoner.MainData.PreviousConvictions.FindAll(r => r.CodeLibItemID.Value == item.CodeLibItemID.Value && r.SentencingArticles != null && r.SentencingArticles.Any()).First().SentencingArticles;
                            }
                        }
                    }

                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ դաջվածքնեը", RequestData: PrisonerID.ToString()));

            return PartialView("PrevConviction_Table_Row", Model);
        }

        public ActionResult Draw_PreviousConvictionsTableRow(List<PreviousConvictionsEntity> PreviousConvictions)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.MainData = new MainDataEntity();
            List<int> Codes = new List<int>();
            if (PreviousConvictions != null && PreviousConvictions.Any())
            {
                foreach (PreviousConvictionsEntity item in PreviousConvictions)
                {
                    if (!Codes.FindAll(r=>r == item.CodeLibItemID).Any())
                    {
                        item.SentencingArticles = BusinessLayer_LibsLayer.GetLibsLocalTreeWithPropertiesByLibParentID(item.CodeLibItemID.Value);
                        Codes.Add(item.CodeLibItemID.Value);
                    }
                    else
                    {
                        if (PreviousConvictions.FindAll(r=>r.CodeLibItemID.Value == item.CodeLibItemID.Value && r.SentencingArticles != null && r.SentencingArticles.Any()).Any())
                        {
                            item.SentencingArticles = PreviousConvictions.FindAll(r => r.CodeLibItemID.Value == item.CodeLibItemID.Value && r.SentencingArticles != null && r.SentencingArticles.Any()).First().SentencingArticles;
                        }
                    }
                }

            }

            Model.Prisoner.MainData.PreviousConvictions = PreviousConvictions;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դիտել նախկին դատվածությունը", RequestData: JsonConvert.SerializeObject(PreviousConvictions)));

            return PartialView("PrevConviction_Table_Row", Model);
        }

        public ActionResult Get_SentencingDataArticles(int ParentID, PreviousConvictionsEntity prevData = null, SentencingDataEntity sentenceData = null, ArrestDataEntity arrestData = null)
        {
            SentencingArticlesViewModel Model = new SentencingArticlesViewModel();

            Model.SentenceCodesArticles = BusinessLayer_LibsLayer.GetLibsLocalTreeWithPropertiesByLibParentID(ParentID);

            if (prevData != null && prevData.ID != null)
            {
                Model.prevData = prevData;
                Model.selectedSentencingDataArticles = BusinessLayer_PrisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(PrevConvictionDataID: prevData.ID.Value));
            }
            else if (sentenceData != null && sentenceData.ID != null)
            {
                Model.sentenceData = sentenceData;
                Model.selectedSentencingDataArticles = BusinessLayer_PrisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(SentencingDataID: sentenceData.ID.Value));
            }
            else if (arrestData != null && arrestData.ID != null)
            {
                Model.arrestData = arrestData;
                Model.selectedSentencingDataArticles = BusinessLayer_PrisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(ArrestDataID: arrestData.ID.Value));
            }

            return PartialView("SentencingDataArticles", Model);
        }
        #endregion

        #region Permissions
        public ActionResult Get_AddPermission(int? ID = null)
        {
            PermissionsViewModel Model = new PermissionsViewModel();

            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];

            List<PermissionsDashboardEntity> Departures = BusinessLayer_PermLayer.GetPermissionedPositions((int)PermissionsHardCodedIds.Departures, User.SelectedOrgUnitID, true);
            List<PermissionsDashboardEntity> OfficialVisits = BusinessLayer_PermLayer.GetPermissionedPositions((int)PermissionsHardCodedIds.OfficialVisits, User.SelectedOrgUnitID, true);
            List<PermissionsDashboardEntity> PersonalVisits = BusinessLayer_PermLayer.GetPermissionedPositions((int)PermissionsHardCodedIds.PersonalVisits, User.SelectedOrgUnitID, true);
            List<PermissionsDashboardEntity> Packages = BusinessLayer_PermLayer.GetPermissionedPositions((int)PermissionsHardCodedIds.Packages, User.SelectedOrgUnitID, true);

            //List<PermissionsGurdEntity> positions = BusinessLayer_PrisonerService.GetPermissionGuard(User.SelectedOrgUnitID);
            List<OrgUnitEntity> positions = BusinessLayer_OrganizatonLayer.GetPositionsByOrgUnitID(User.SelectedOrgUnitID);
            positions.RemoveAll(OrgUnit => Departures.Exists(entity => entity.Position.ID == OrgUnit.ID));
            positions.RemoveAll(OrgUnit => OfficialVisits.Exists(entity => entity.Position.ID == OrgUnit.ID));
            positions.RemoveAll(OrgUnit => PersonalVisits.Exists(entity => entity.Position.ID == OrgUnit.ID));
            positions.RemoveAll(OrgUnit => Packages.Exists(entity => entity.Position.ID == OrgUnit.ID));
            
            Model.OrgUnitPositions = positions;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել վերակարգ", RequestData: ID.ToString()));

            return PartialView("AddPermission", Model);
        }
        #endregion

        #region Merge

        public ActionResult Get_MergeApprove(int? ID = null)
        {
            MergeApproveViewModel Model = new MergeApproveViewModel();

            List<MergeEntity> list = BusinessLayer_MergeLayer.GetModificationInfo(new MergeEntity() { ID = ID });

            if(list != null && list.Count > 0)
            {
                Model.CurrentMerge = list.First();
                bool isAdd = Model.CurrentMerge.Type.Value == 1;
                if ((Model.CurrentMerge.EntityID != null || Model.CurrentMerge.EID == (int)MergeEntity.TableIDS.ARMY_CONTENT) || isAdd)
                {

                    string EID = Model.CurrentMerge.EID.ToString();

                    Model.Unit = int.Parse(EID.Substring(1, 2));
                    Model.SubUnit = int.Parse(EID.Substring(3, 2));
                    Model.Table = int.Parse(EID.Substring(5, 2));

                    // variable that are used in switch case
                    AdditionalFileEntity oldFiles = new AdditionalFileEntity();

                    switch ((MergeEntity.TableIDS)Model.CurrentMerge.EID)
                    {
                        // Camera Card
                        case MergeEntity.TableIDS.CELL_MAIN:
                            List<CameraCardItemEntity> ccItemList = BusinessLayer_PrisonerService.GetCameraCardItems(new CameraCardItemEntity(ID: Model.CurrentMerge.EntityID));
                            if (ccItemList.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { CameraCardItem = ccItemList.First() };
                            }
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { CameraCardItem = JsonConvert.DeserializeObject<CameraCardItemEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Doc Identity
                        case MergeEntity.TableIDS.INITIAL_DOC_IDENTITY:
                            List<IdentificationDocument> inDocItemList = BusinessLayer_PrisonerService.GetIdentificationDocuments(new IdentificationDocument(ID: Model.CurrentMerge.EntityID));
                            if (inDocItemList.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { IdDocItem = inDocItemList.First() };
                            }
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { IdDocItem = JsonConvert.DeserializeObject<IdentificationDocument>(Model.CurrentMerge.Json) };

                            break;

                        // Prev Conviction
                        case MergeEntity.TableIDS.PRIMARY_PREVENTION:
                            List<PreviousConvictionsEntity> prevConvItemList = BusinessLayer_PrisonerService.GetPreviousConviction(new PreviousConvictionsEntity(ID: Model.CurrentMerge.EntityID));
                            prevConvItemList.First().SentencingDataArticles = BusinessLayer_PrisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(PrevConvictionDataID: prevConvItemList.First().ID));
                            if (prevConvItemList.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel()
                                {

                                    PrevConvItem = prevConvItemList.First(),
                                    SentencingArticles = BusinessLayer_LibsLayer.GetLibsLocalTreeWithPropertiesByLibParentID(prevConvItemList.First().CodeLibItemID.Value)
                                };
                            }

                            PreviousConvictionsEntity prevConvItem = JsonConvert.DeserializeObject<PreviousConvictionsEntity>(Model.CurrentMerge.Json);
                            Model.New_PrisonerViewModel = new PrisonersViewModel()
                            {
                                PrevConvItem = prevConvItem,
                                SentencingArticles = BusinessLayer_LibsLayer.GetLibsLocalTreeWithPropertiesByLibParentID(prevConvItem.CodeLibItemID.Value)
                            };

                            break;

                        // Physical Data Height Weight
                        case MergeEntity.TableIDS.PHYSICAL_HEIGHT_WEIGHT:
                            List<HeightWeightEntity> HeightWeightItemList = BusinessLayer_PrisonerService.GetHeightWeight(new HeightWeightEntity(ID: Model.CurrentMerge.EntityID));
                            if (HeightWeightItemList.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { HeightWeightItem = HeightWeightItemList.First() };
                            }
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { HeightWeightItem = JsonConvert.DeserializeObject<HeightWeightEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Physical Data Content
                        case MergeEntity.TableIDS.PHYSICAL_CONTENT:
                            PhysicalDataEntity PhysicalContentItem = BusinessLayer_PrisonerService.GetPhysicalData(PrisonerID: Model.CurrentMerge.PrisonerID.Value);
                            if (PhysicalContentItem != null && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { PhysicalContentItem = PhysicalContentItem };
                            }
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { PhysicalContentItem = JsonConvert.DeserializeObject<PhysicalDataEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Physical Data Invalids
                        case MergeEntity.TableIDS.PHYSICAL_INVALIDS:
                            List<InvalidEntity> InvalidItem = BusinessLayer_PrisonerService.GetInvalides(new InvalidEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (InvalidItem != null && InvalidItem.Any() && !isAdd)
                            {
                                InvalidEntity oldInvalid = InvalidItem.First();
                                if (oldInvalid.CertificateFileID != null)
                                {
                                    oldInvalid.FileSrc = BL_Files.getInstance().GetLink(oldInvalid.CertificateFileID);
                                }
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { InvalidItem = oldInvalid };
                            }
                            InvalidEntity curInvalid = JsonConvert.DeserializeObject<InvalidEntity>(Model.CurrentMerge.Json);

                            if (curInvalid.CertificateFileID != null)
                            {
                                curInvalid.FileSrc = BL_Files.getInstance().GetLink(curInvalid.CertificateFileID);
                            }
                            else
                            {
                                curInvalid.FileSrc = Model.Old_PrisonerViewModel.InvalidItem.FileSrc;
                            }
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { InvalidItem = curInvalid };

                            break;

                        // Physical Data Leanings
                        case MergeEntity.TableIDS.PHYSICAL_LEANINGS:
                            List<LeaningEntity> LeaningItemList = BusinessLayer_PrisonerService.GetLeaning(new LeaningEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (LeaningItemList != null && LeaningItemList.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { LeaningItem = LeaningItemList.First() };
                            }
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { LeaningItem = JsonConvert.DeserializeObject<LeaningEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Files
                        case MergeEntity.TableIDS.PHYSICAL_FILES:
                        case MergeEntity.TableIDS.PRIMARY_IMAGES:
                        case MergeEntity.TableIDS.PRIMARY_FIELS:
                        case MergeEntity.TableIDS.MEDICAL_FILES:
                        case MergeEntity.TableIDS.FAMILY_FILES:
                        case MergeEntity.TableIDS.ARMY_FILES:
                        case MergeEntity.TableIDS.ENTERANCE_FILES_ARREST:
                        case MergeEntity.TableIDS.ENTERANCE_FILES:
                        case MergeEntity.TableIDS.CELL_FILES:
                        case MergeEntity.TableIDS.ITEMS_FILES:
                        case MergeEntity.TableIDS.BAN_FILES:
                        case MergeEntity.TableIDS.ENCOURAGEMENTS_FILES:
                        case MergeEntity.TableIDS.PENALTY_FILES:
                        case MergeEntity.TableIDS.EDUCATIONAL_COURSES_FILES:
                        case MergeEntity.TableIDS.WORKLOADS_FILES:
                        case MergeEntity.TableIDS.OFFICIAL_VISITS_FILES:
                        case MergeEntity.TableIDS.PERSONAL_VISITS_FILES:
                        case MergeEntity.TableIDS.PACKAGES_FILES:
                        case MergeEntity.TableIDS.DEPARTURES_FILES:
                        case MergeEntity.TableIDS.TRANSFERS_FILES:
                        case MergeEntity.TableIDS.CONFLICTS_FILES:
                            oldFiles = BL_Files.getInstance().GetFile(id: Model.CurrentMerge.EntityID.Value);
                            if (oldFiles != null && oldFiles.ID != null && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { FileItem = oldFiles };
                            }
                            AdditionalFileEntity FileItem = JsonConvert.DeserializeObject<AdditionalFileEntity>(Model.CurrentMerge.Json);
                            if (oldFiles != null) FileItem.FileName = oldFiles.FileName;
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { FileItem = FileItem };

                            break;

                        // Medical Preliminary Content
                        case MergeEntity.TableIDS.MEDICAL_PRELIMINARY_CONTENT:
                            List<MedicalPrimaryEntity> MedicalContentItem = BL_MedicalLayer.GetList_MedicalPrimary(new MedicalPrimaryEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (MedicalContentItem != null && MedicalContentItem.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { MedicalContentItem = MedicalContentItem.First() };
                            }
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { MedicalContentItem = JsonConvert.DeserializeObject<MedicalPrimaryEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Medical Complaints
                        case MergeEntity.TableIDS.MEDICAL_PRELIMINARY_COMPLATIN:
                        case MergeEntity.TableIDS.MEDICAL_AMBULATOR_HISTORY_COMPLATIN:
                        case MergeEntity.TableIDS.MEDICAL_STATIONARY_HISTORY_COMPLATIN:
                            List<MedicalComplaintsEntity> MedicalComplantItem = BL_MedicalLayer.GetList_MedicalComplaints(new MedicalComplaintsEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (MedicalComplantItem != null && MedicalComplantItem.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { MedicalComplaintItem = MedicalComplantItem.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { MedicalComplaintItem = JsonConvert.DeserializeObject<MedicalComplaintsEntity>(Model.CurrentMerge.Json) };

                            break;


                        // Medical External Examination
                        case MergeEntity.TableIDS.MEDICAL_PRELIMINARY_EXTERNAL_EXAMINATION:
                            List<MedicalExternalExaminationEntity> MedicalExternalExaminationItem = BL_MedicalLayer.GetList_MedicalExternalExamination(new MedicalExternalExaminationEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (MedicalExternalExaminationItem != null && MedicalExternalExaminationItem.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { MedicalExternalExaminationItem = MedicalExternalExaminationItem.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { MedicalExternalExaminationItem = JsonConvert.DeserializeObject<MedicalExternalExaminationEntity>(Model.CurrentMerge.Json) };

                            break;


                        // Medical Diagnosis
                        case MergeEntity.TableIDS.MEDICAL_PRELIMINARY_DIAGNOISIS:
                        case MergeEntity.TableIDS.MEDICAL_AMBULATOR_HISTORY_DIAGNOSIS:
                        case MergeEntity.TableIDS.MEDICAL_STATIONARY_HISTORY_DIAGNOSIS:
                            List<MedicalDiagnosisEntity> MedicalDiagnosisItem = BL_MedicalLayer.GetList_MedicalDiagnosis(new MedicalDiagnosisEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (MedicalDiagnosisItem != null && MedicalDiagnosisItem.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { MedicalDiagnosisItem = MedicalDiagnosisItem.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { MedicalDiagnosisItem = JsonConvert.DeserializeObject<MedicalDiagnosisEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Medical Research
                        case MergeEntity.TableIDS.MEDICAL_AMBULATOR_HISTORY_RESEARCH:
                        case MergeEntity.TableIDS.MEDICAL_STATIONARY_HISTORY_RESEARCH:
                            List<MedicalResearchEntity> MedicalResearchItem = BL_MedicalLayer.GetList_MedicalResearch(new MedicalResearchEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (MedicalResearchItem != null && MedicalResearchItem.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { MedicalResearchItem = MedicalResearchItem.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { MedicalResearchItem = JsonConvert.DeserializeObject<MedicalResearchEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Medical History
                        case MergeEntity.TableIDS.MEDICAL_AMBULATOR_HISTORY:
                        case MergeEntity.TableIDS.MEDICAL_STATIONARY_HISTORY:
                            List<MedicalHistoryEntity> MedicalHistoryItem = BL_MedicalLayer.GetList_MedicalHistory(new MedicalHistoryEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (MedicalHistoryItem != null && MedicalHistoryItem.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { MedicalHistoryItem = MedicalHistoryItem.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { MedicalHistoryItem = JsonConvert.DeserializeObject<MedicalHistoryEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Fingers Tatoos Scars
                        case MergeEntity.TableIDS.FINGERTATOOSCARS_FINGERS:
                        case MergeEntity.TableIDS.FINGERTATOOSCARS_SCARS:
                        case MergeEntity.TableIDS.FINGERTATOOSCARS_TATOOS:
                            List<FingerprintsTattoosScarsItemEntity> FingersTatooItem = BusinessLayer_PrisonerService.GetFingerprintsTattoosScarsItem(ID: Model.CurrentMerge.EntityID.Value);
                            if (FingersTatooItem != null && FingersTatooItem.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { FingerTatooScarItem = FingersTatooItem.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { FingerTatooScarItem = JsonConvert.DeserializeObject<FingerprintsTattoosScarsItemEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Familty Relatives Content
                        case MergeEntity.TableIDS.FAMILY_CONTENT:
                            FamilyRelativeEntity FamiltyRelativeItem = BusinessLayer_PrisonerService.GetFamilyRelations(new FamilyRelativeEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (FamiltyRelativeItem != null && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { FamiltyRelativeItem = FamiltyRelativeItem };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { FamiltyRelativeItem = JsonConvert.DeserializeObject<FamilyRelativeEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Familty Relatives Spouses
                        case MergeEntity.TableIDS.FAMILY_SPOUSES:
                            List<SpousesEntity> SpouseItemList = BusinessLayer_PrisonerService.GetSpouses(new SpousesEntity(ID: Model.CurrentMerge.EntityID.Value));

                            if (SpouseItemList != null && SpouseItemList.Any() && !isAdd)
                            {
                                SpousesEntity tempItem = SpouseItemList.First();
                                PersonEntity Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempItem.PersonID.Value, Full: true);
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { FamilySpouseItem = tempItem, Person = Person };
                            }

                            SpousesEntity tempFamiltyItemSecond = JsonConvert.DeserializeObject<SpousesEntity>(Model.CurrentMerge.Json);

                            if (tempFamiltyItemSecond.Person == null && SpouseItemList != null && SpouseItemList.Any())
                            {
                                tempFamiltyItemSecond.PersonID = SpouseItemList.First().PersonID;
                                Model.New_PrisonerViewModel.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempFamiltyItemSecond.PersonID.Value, Full: true);
                            }
                            PersonEntity SpousesPerson = tempFamiltyItemSecond.Person;
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { FamilySpouseItem = tempFamiltyItemSecond, Person = SpousesPerson };
                            
                            break;


                        // Familty Relatives Children
                        case MergeEntity.TableIDS.FAMILY_CHILDRENS:
                            List<ChildrenEntity> ChilrenItemList = BusinessLayer_PrisonerService.GetChildren(new ChildrenEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (ChilrenItemList != null && ChilrenItemList.Any() && !isAdd)
                            {
                                ChildrenEntity tempItem = ChilrenItemList.First();
                                PersonEntity Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempItem.PersonID.Value, Full: true);
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { FamilyChildrenItem = tempItem, Person = Person };

                            }

                            ChildrenEntity tempFamiltyChildrenItemSecond = JsonConvert.DeserializeObject<ChildrenEntity>(Model.CurrentMerge.Json);

                            if (tempFamiltyChildrenItemSecond.Person == null && ChilrenItemList != null && ChilrenItemList.Any())
                            {
                                tempFamiltyChildrenItemSecond.PersonID = ChilrenItemList.First().PersonID;
                                Model.New_PrisonerViewModel.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempFamiltyChildrenItemSecond.PersonID.Value, Full: true);
                            }
                            PersonEntity ChildPerson = tempFamiltyChildrenItemSecond.Person;
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { FamilyChildrenItem = tempFamiltyChildrenItemSecond, Person = ChildPerson };

                            break;

                        // Familty Relatives Parents
                        case MergeEntity.TableIDS.FAMILY_PARENTS:
                            List<ParentsEntity> ParentItemList = BusinessLayer_PrisonerService.GetParents(new ParentsEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (ParentItemList != null && ParentItemList.Any() && !isAdd)
                            {
                                ParentsEntity tempItem = ParentItemList.First();
                                PersonEntity Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempItem.PersonID.Value, Full: true);
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { FamilyParentItem = tempItem, Person = Person };
                            }

                            ParentsEntity tempFamiltyParentItemSecond = JsonConvert.DeserializeObject<ParentsEntity>(Model.CurrentMerge.Json);

                            if (tempFamiltyParentItemSecond.Person == null && ParentItemList != null && ParentItemList.Any())
                            {
                                tempFamiltyParentItemSecond.PersonID = ParentItemList.First().PersonID;
                                Model.New_PrisonerViewModel.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempFamiltyParentItemSecond.PersonID.Value, Full: true);
                            }
                            PersonEntity ParetsPerson = tempFamiltyParentItemSecond.Person;
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { FamilyParentItem = tempFamiltyParentItemSecond, Person = ParetsPerson };
                            
                            break;

                        // Familty Relatives Syster Brother
                        case MergeEntity.TableIDS.FAMILY_SYSTER_BROTHER:
                            List<SisterBrotherEntity> SysterBrotherItemList = BusinessLayer_PrisonerService.GetSisterBrother(new SisterBrotherEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (SysterBrotherItemList != null && SysterBrotherItemList.Any() && !isAdd)
                            {

                                SisterBrotherEntity tempItem = SysterBrotherItemList.First();
                                PersonEntity Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempItem.PersonID.Value, Full: true);
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { SysterBrotherParentItem = tempItem, Person = Person };

                            }

                            SisterBrotherEntity tempFamiltySysterBrotherItemSecond = JsonConvert.DeserializeObject<SisterBrotherEntity>(Model.CurrentMerge.Json);

                            if (tempFamiltySysterBrotherItemSecond.Person == null && SysterBrotherItemList != null && SysterBrotherItemList.Any())
                            {
                                tempFamiltySysterBrotherItemSecond.PersonID = SysterBrotherItemList.First().PersonID;
                                Model.New_PrisonerViewModel.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempFamiltySysterBrotherItemSecond.PersonID.Value, Full: true);
                            }
                            PersonEntity SysterBrotherPerson = tempFamiltySysterBrotherItemSecond.Person;
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { SysterBrotherParentItem = tempFamiltySysterBrotherItemSecond };

                            break;

                        // Familty Relatives Others
                        case MergeEntity.TableIDS.FAMILY_OTHERS:
                            List<OtherRelativesEntity> OtherItemList = BusinessLayer_PrisonerService.GetOtherRelatives(new OtherRelativesEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (OtherItemList != null && OtherItemList.Any() && !isAdd)
                            {

                                OtherRelativesEntity tempItem = OtherItemList.First();
                                PersonEntity Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempItem.PersonID.Value, Full: true);
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { OtherParentItem = tempItem, Person = Person };

                            }

                            OtherRelativesEntity tempFamiltyOtherItemSecond = JsonConvert.DeserializeObject<OtherRelativesEntity>(Model.CurrentMerge.Json);

                            if (tempFamiltyOtherItemSecond.Person == null && OtherItemList != null && OtherItemList.Any())
                            {
                                tempFamiltyOtherItemSecond.PersonID = OtherItemList.First().PersonID;
                                Model.New_PrisonerViewModel.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempFamiltyOtherItemSecond.PersonID.Value, Full: true);
                            }
                            PersonEntity FamilyOtherPerson = tempFamiltyOtherItemSecond.Person;
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { OtherParentItem = tempFamiltyOtherItemSecond, Person = FamilyOtherPerson };
                          
                            break;

                        // Education
                        case MergeEntity.TableIDS.EDUCATION:
                            EducationsProfessionsEntity EducationItem = BusinessLayer_PrisonerService.GetEducationsProfessions(new EducationsProfessionsEntity(PrisonerID: Model.CurrentMerge.PrisonerID, ID: Model.CurrentMerge.EntityID.Value), grouped: true);
                            if (EducationItem != null && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { EducationItem = EducationItem };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { EducationItem = JsonConvert.DeserializeObject<EducationsProfessionsEntity>(Model.CurrentMerge.Json) };

                            Model.New_PrisonerViewModel.Education = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.EDUCATION);
                            Model.New_PrisonerViewModel.Preferance = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PREFERANCE);
                            Model.New_PrisonerViewModel.Languages = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.LAGNUAGES);
                            Model.New_PrisonerViewModel.Profession = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PROFESSION);
                            Model.New_PrisonerViewModel.Abilities = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ABILITIES);
                            Model.New_PrisonerViewModel.AcademicDegree = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.ACADEMIC_DEGREE);
                            Model.New_PrisonerViewModel.ScienceDegree = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.SCIENCE_DEGREE);
                            Model.New_PrisonerViewModel.OfficialGifts = BusinessLayer_LibsLayer.GetLibsLocalTreeByLibPathID((int)LibsEntity.LIBS.OFFICIAL_GIFTS);
                            if (EducationItem != null && !isAdd)
                            {
                                Model.Old_PrisonerViewModel.Education = Model.New_PrisonerViewModel.Education;
                                Model.Old_PrisonerViewModel.Preferance = Model.New_PrisonerViewModel.Preferance;
                                Model.Old_PrisonerViewModel.Languages = Model.New_PrisonerViewModel.Languages;
                                Model.Old_PrisonerViewModel.Profession = Model.New_PrisonerViewModel.Profession;
                                Model.Old_PrisonerViewModel.Abilities = Model.New_PrisonerViewModel.Abilities;
                                Model.Old_PrisonerViewModel.AcademicDegree = Model.New_PrisonerViewModel.AcademicDegree;
                                Model.Old_PrisonerViewModel.ScienceDegree = Model.New_PrisonerViewModel.ScienceDegree;
                                Model.Old_PrisonerViewModel.OfficialGifts = Model.New_PrisonerViewModel.OfficialGifts;
                            }
                            

                            break;
                            
                        // Army Content
                        case MergeEntity.TableIDS.ARMY_CONTENT:
                            MilitaryServiceEntity MilitaryItem = BusinessLayer_PrisonerService.GetMilitaryServiceData(PrisonerID: Model.CurrentMerge.PrisonerID.Value);
                            if (MilitaryItem != null && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { MilitaryItem = MilitaryItem };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { MilitaryItem = JsonConvert.DeserializeObject<MilitaryServiceEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Army Main
                        case MergeEntity.TableIDS.ARMY_MAIN:
                            List<ArmyEntity> ArmyItem = BusinessLayer_PrisonerService.GetArmy(new ArmyEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (ArmyItem != null && ArmyItem.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { ArmyItem = ArmyItem.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { ArmyItem = JsonConvert.DeserializeObject<ArmyEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Enterance Arrest Data
                        case MergeEntity.TableIDS.ENTERANCE_ARREST_DATA:
                            List<ArrestDataEntity> ArrestDataItem = BusinessLayer_PrisonerService.GetArrestData(new ArrestDataEntity(ID: Model.CurrentMerge.EntityID.Value));
                            ArrestDataItem.First().SentencingDataArticles = BusinessLayer_PrisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(ArrestDataID: ArrestDataItem.First().ID));
                            if (ArrestDataItem != null && ArrestDataItem.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { ArrestDataItem = ArrestDataItem.First(),
                                SentencingArticles = BusinessLayer_LibsLayer.GetLibsLocalTreeWithPropertiesByLibParentID(ArrestDataItem.First().CodeLibItem_ID.Value)
                                };
                            }
                            ArrestDataEntity ArrestItem = JsonConvert.DeserializeObject<ArrestDataEntity>(Model.CurrentMerge.Json);

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { ArrestDataItem = ArrestItem,
                            SentencingArticles = BusinessLayer_LibsLayer.GetLibsLocalTreeWithPropertiesByLibParentID(ArrestItem.CodeLibItem_ID.Value)
                            };

                            break;
                        // Enterance Prisoner Type Status
                        case MergeEntity.TableIDS.ENTERANCE_PrisonerTypeStatus:
                            List<PrisonerTypeStatusEntity> PrisonerTypeStatusItem = BusinessLayer_PrisonerService.GetPrisonerTypeStatus(new PrisonerTypeStatusEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (PrisonerTypeStatusItem != null && PrisonerTypeStatusItem.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { PrisonerTypeStatusItems = PrisonerTypeStatusItem.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { PrisonerTypeStatusItems = JsonConvert.DeserializeObject<PrisonerTypeStatusEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Enterance Prisoner Punishment Type
                        case MergeEntity.TableIDS.ENTERANCE_PRISONERPUNISHMENTTYPE:
                            List<PrisonerPunishmentTypeEntity> PrisonerPunishmentTypeItem = BusinessLayer_PrisonerService.GetPrisonerPunishmentType(new PrisonerPunishmentTypeEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (PrisonerPunishmentTypeItem != null && PrisonerPunishmentTypeItem.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { PrisonerPunishmentTypeItems = PrisonerPunishmentTypeItem.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { PrisonerPunishmentTypeItems = JsonConvert.DeserializeObject<PrisonerPunishmentTypeEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Enterance Sentence
                        case MergeEntity.TableIDS.ENTERANCE_SENTENCE:
                            List<SentencingDataEntity> SentenceItem = BusinessLayer_PrisonerService.GetSentencingData(new SentencingDataEntity(ID: Model.CurrentMerge.EntityID.Value));
                            SentenceItem.First().SentencingDataArticles = BusinessLayer_PrisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(SentencingDataID: SentenceItem.First().ID));
                            if (SentenceItem != null && SentenceItem.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { SentenceDataItem = SentenceItem.First(),
                                SentencingArticles = BusinessLayer_LibsLayer.GetLibsLocalTreeWithPropertiesByLibParentID(SentenceItem.First().CodeLibItem_ID.Value)
                                };
                            }

                            SentencingDataEntity SentencingItem = JsonConvert.DeserializeObject<SentencingDataEntity>(Model.CurrentMerge.Json);

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { SentenceDataItem = SentencingItem,
                                SentencingArticles = BusinessLayer_LibsLayer.GetLibsLocalTreeWithPropertiesByLibParentID(SentencingItem.CodeLibItem_ID.Value)
                            };

                            break;

                        // Enterance Protocol
                        case MergeEntity.TableIDS.ENTERANCE_PROTOCOL:
                            List<PrisonAccessProtocolEntity> ProtocolItemList = BusinessLayer_PrisonerService.GetPrisonAccessProtocol(new PrisonAccessProtocolEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (ProtocolItemList != null && ProtocolItemList.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { ProtocolItem = ProtocolItemList.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { ProtocolItem = JsonConvert.DeserializeObject<PrisonAccessProtocolEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Items Main
                        case MergeEntity.TableIDS.ITEMS_MAIN:
                            List<ItemEntity> ItemList = BusinessLayer_PrisonerService.GetItems(new ItemEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (ItemList != null && ItemList.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { ItemItems = ItemList.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { ItemItems = JsonConvert.DeserializeObject<ItemEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Ban Items
                        case MergeEntity.TableIDS.BAN_MAIN:
                            List<InjunctionItemEntity> BanList = BusinessLayer_PrisonerService.GetInjunctionItem(new InjunctionItemEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (BanList != null && BanList.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { BanListItems = BanList.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { BanListItems = JsonConvert.DeserializeObject<InjunctionItemEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Encouragements Main
                        case MergeEntity.TableIDS.ENCOURAGEMENTS_MAIN:
                            List<EncouragementsEntity> EncouragementList = BusinessLayer_PrisonerService.GetEncouragements(new EncouragementsEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (EncouragementList != null && EncouragementList.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { EncouragementItems = EncouragementList.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { EncouragementItems = JsonConvert.DeserializeObject<EncouragementsEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Penalty Main
                        case MergeEntity.TableIDS.PENALTY_MAIN:
                            List<PenaltiesEntity> PenaltyList = BusinessLayer_PrisonerService.GetPenalties(new PenaltiesEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (PenaltyList != null && PenaltyList.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { PenaltyItems = PenaltyList.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { PenaltyItems = JsonConvert.DeserializeObject<PenaltiesEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Educational Courses
                        case MergeEntity.TableIDS.EDUCATIONAL_COURSES_MAIN:
                            List<EducationalCoursesEntity> EducationalList = BusinessLayer_PrisonerService.GetEducationalCourses(new EducationalCoursesEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (EducationalList != null && EducationalList.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { EducationalCoursesItems = EducationalList.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { EducationalCoursesItems = JsonConvert.DeserializeObject<EducationalCoursesEntity>(Model.CurrentMerge.Json) };

                            break;


                        // Workloads main
                        case MergeEntity.TableIDS.WORKLOADS_MAIN:
                            List<WorkLoadsEntity> WorkloadList = BusinessLayer_PrisonerService.GetWorkLoads(new WorkLoadsEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (WorkloadList != null && WorkloadList.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { WorkLoadItems = WorkloadList.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { WorkLoadItems = JsonConvert.DeserializeObject<WorkLoadsEntity>(Model.CurrentMerge.Json) };

                            break;


                        // Official Visits Main
                        case MergeEntity.TableIDS.OFFICIAL_VISITS_MAIN:
                            List<OfficialVisitsEntity> OfficialVisitList = BusinessLayer_PrisonerService.GetOfficialVisits(new OfficialVisitsEntity(ID: Model.CurrentMerge.EntityID.Value));
                             if (OfficialVisitList != null && OfficialVisitList.Any() && !isAdd)
                            {
                                OfficialVisitsEntity tempOfficialVisitItem = OfficialVisitList.First();
                                tempOfficialVisitItem.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempOfficialVisitItem.PersonID.Value, Full: true);
                                
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { OfficialVisitItems = tempOfficialVisitItem };
                            }
                            
                            OfficialVisitsEntity tempOfficialVisitSecond = JsonConvert.DeserializeObject<OfficialVisitsEntity>(Model.CurrentMerge.Json);

                            if (tempOfficialVisitSecond.Person == null && OfficialVisitList != null && OfficialVisitList.Any())
                            {
                                tempOfficialVisitSecond.PersonID = OfficialVisitList.First().PersonID;
                                tempOfficialVisitSecond.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempOfficialVisitSecond.PersonID.Value, Full: true);
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { OfficialVisitItems = tempOfficialVisitSecond };

                            break;


                        // Personal Visits Main
                        case MergeEntity.TableIDS.PERSONAL_VISITS_MAIN:
                            List<PersonalVisitsEntity> PersonalVisitList = BusinessLayer_PrisonerService.GetPersonalVisits(new PersonalVisitsEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (PersonalVisitList != null && PersonalVisitList.Any() && !isAdd)
                            {
                                PersonalVisitsEntity tempPersonalVisititem = PersonalVisitList.First();
                                tempPersonalVisititem.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempPersonalVisititem.PersonID.Value, Full: true);
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { PersonalVisitItems = tempPersonalVisititem };
                            }
                            
                            PersonalVisitsEntity tempPersonalVisitSecond = JsonConvert.DeserializeObject<PersonalVisitsEntity>(Model.CurrentMerge.Json);
                            if (tempPersonalVisitSecond.Person == null && PersonalVisitList != null && PersonalVisitList.Any())
                            {
                                tempPersonalVisitSecond.PersonID = PersonalVisitList.First().PersonID;
                                tempPersonalVisitSecond.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempPersonalVisitSecond.PersonID.Value, Full: true);
                            }
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { PersonalVisitItems = tempPersonalVisitSecond };

                            break;

                        // Packages Main
                        case MergeEntity.TableIDS.PACKAGES_MAIN:
                            List<PackagesEntity> PackageList = BusinessLayer_PrisonerService.GetPackages(new PackagesEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (PackageList != null && PackageList.Any() && !isAdd)
                            {
                                PackagesEntity tempPackageitem = PackageList.First();
                                tempPackageitem.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempPackageitem.PersonID.Value, Full: true);
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { PackageItems = tempPackageitem };
                            }

                            
                            PackagesEntity tempPackagesSecond = JsonConvert.DeserializeObject<PackagesEntity>(Model.CurrentMerge.Json);
                            if (tempPackagesSecond.Person == null && PackageList != null && PackageList.Any())
                            {
                                tempPackagesSecond.PersonID = PackageList.First().PersonID;
                                tempPackagesSecond.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempPackagesSecond.PersonID.Value, Full: true);
                            }
                            
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { PackageItems = tempPackagesSecond };

                            break;


                        // Departures Main
                        case MergeEntity.TableIDS.DEPARTURES_MAIN:
                            List<DepartureEntity> DepartureList = BusinessLayer_PrisonerService.GetDeparture(new DepartureEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (DepartureList != null && DepartureList.Any() && !isAdd)
                            {
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { DepartureItems = DepartureList.First() };
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { DepartureItems = JsonConvert.DeserializeObject<DepartureEntity>(Model.CurrentMerge.Json) };

                            break;

                        // Transfers Main
                        case MergeEntity.TableIDS.TRANSFERS_MAIN:
                            List<TransferEntity> TransferList = BusinessLayer_PrisonerService.GetTransfer(new TransferEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (TransferList != null && TransferList.Any() && !isAdd)
                            {
                                TransferEntity tempTransferItem = TransferList.First();
                                tempTransferItem.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempTransferItem.PersonID.Value, Full: true);
                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { TransferItems = TransferList.First() };
                            }

                            TransferEntity tempTransferItemSecond = JsonConvert.DeserializeObject<TransferEntity>(Model.CurrentMerge.Json);
                            if (tempTransferItemSecond.Person == null && TransferList != null && TransferList.Any())
                            {
                                tempTransferItemSecond.PersonID = TransferList.First().PersonID;
                                tempTransferItemSecond.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempTransferItemSecond.PersonID.Value, Full: true);
                            }
                            Model.New_PrisonerViewModel = new PrisonersViewModel() { TransferItems = tempTransferItemSecond };

                            break;


                        // Conflicts Main
                        case MergeEntity.TableIDS.CONFLICTS_MAIN:
                            List<ConflictsEntity> ConflictList = BusinessLayer_PrisonerService.GetConflicts(new ConflictsEntity(ID: Model.CurrentMerge.EntityID.Value));
                            if (ConflictList != null && ConflictList.Any() && !isAdd)
                            {
                                ConflictsEntity tempConflictItem = ConflictList.First();
                                tempConflictItem.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempConflictItem.PersonID.Value, Full: true);

                                Model.Old_PrisonerViewModel = new PrisonersViewModel() { ConflictItems = ConflictList.First() };
                            }

                            ConflictsEntity tempConflictItemSecond = JsonConvert.DeserializeObject<ConflictsEntity>(Model.CurrentMerge.Json);

                            if (tempConflictItemSecond.Person == null && ConflictList != null && ConflictList.Any())
                            {
                                tempConflictItemSecond.PersonID = ConflictList.First().PersonID;
                                tempConflictItemSecond.Person = BusinessLayer_PrisonerService.GetPerson(PersonID: tempConflictItemSecond.PersonID.Value, Full: true);
                            }

                            Model.New_PrisonerViewModel = new PrisonersViewModel() { ConflictItems = tempConflictItemSecond };

                            break;

                    }
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հաստատման ուղարկված փոփոխության դիտում", RequestData: (ID == null) ? "" : ID.ToString()));

            return PartialView("MergeApprove", Model);
        }
        #endregion

        #region PrisonerTypeStatus

        public ActionResult Get_PrisonerTypeStatus(int? id)
        {
            EnteranceViewModel Model = new EnteranceViewModel();

            if (id != null)
            {
                Model.prisonerTypeStatusData = BusinessLayer_PrisonerService.GetPrisonerTypeStatus(new PrisonerTypeStatusEntity(ID: id.Value)).First();
            }


            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: id.ToString()));

            return PartialView("PrisonerTypeStatus", Model);
        }


        public ActionResult Get_PrisonerTypeStatusTableRow(int? PrisonerID)
        {
            EnteranceViewModel Model = new EnteranceViewModel();

            
            if (PrisonerID != null)
            {
                Model.PrisonerTypeStatusList = BusinessLayer_PrisonerService.GetPrisonerTypeStatus(new PrisonerTypeStatusEntity(PrisonerID: PrisonerID.Value));
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: PrisonerID.ToString()));

            return PartialView("PrisonerTypeStatus_Table_Row", Model);
        }

        public ActionResult Draw_PrisonerTypeStatusTableRow(List<PrisonerTypeStatusEntity> Data)
        {
            EnteranceViewModel Model = new EnteranceViewModel();

            Model.PrisonerTypeStatusList = Data;

            //BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: PrisonerID.ToString()));

            return PartialView("PrisonerTypeStatus_Table_Row", Model);
        }
        #endregion

        #region PrisonerPunishmentType

        public ActionResult Get_PrisonerPunishmentType(int? id)
        {
            EnteranceViewModel Model = new EnteranceViewModel();

            if (id != null)
            {
                Model.prisonerPunishmentTypeData = BusinessLayer_PrisonerService.GetPrisonerPunishmentType(new PrisonerPunishmentTypeEntity(ID: id.Value)).First();
            }

            Model.PrisonerPunishmentTypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PRISONER_PUNISHMENT_TYPE);
            Model.PrisonerPunishmentBaseLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.PRISONER_PUNISHMENT_BASE);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: id.ToString()));

            return PartialView("PrisonerPunishmentType", Model);
        }


        public ActionResult Get_PrisonerPunishmentTypeTableRow(int? PrisonerID)
        {
            EnteranceViewModel Model = new EnteranceViewModel();


            if (PrisonerID != null)
            {
                Model.PrisonerPunishmentTypeList = BusinessLayer_PrisonerService.GetPrisonerPunishmentType(new PrisonerPunishmentTypeEntity(PrisonerID: PrisonerID.Value));
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: PrisonerID.ToString()));

            return PartialView("PrisonerPunishmentType_Table_Row", Model);
        }

        public ActionResult Draw_PrisonerPunishmentTypeTableRow(List<PrisonerPunishmentTypeEntity> Data)
        {
            EnteranceViewModel Model = new EnteranceViewModel();

            Model.PrisonerPunishmentTypeList = Data;

            //BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "unknown", RequestData: PrisonerID.ToString()));

            return PartialView("PrisonerPunishmentType_Table_Row", Model);
        }
        #endregion
    }
}