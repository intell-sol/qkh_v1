﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.DataArchive)]
    public class _Data_Archive_Controller : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public _Data_Archive_Controller()
        {
            PermissionHCID = PermissionsHardCodedIds.DataArchive;
        }
        /// <summary>
        /// Primary data add partial view
        /// </summary>
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել արխիվ"));

            return PartialView("Index");
        }

        [HttpPost]
        /// <summary>
        /// CaseClose data edit partial view
        /// </summary>
        public ActionResult Edit(int? PrisonerID, int? PrisonerType)
        {
            ArchiveViewModel Model = new ArchiveViewModel();

            Model.archives = BusinessLayer_PrisonerService.GetPrisonerArchive(PrisonerID: PrisonerID.Value);

            if (PrisonerType != null && PrisonerType.Value == 3)
            {
                Model.controllerName = "Prisoners";
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել արխիվ", RequestData: JsonConvert.SerializeObject(new { PrisonerID = PrisonerID, PrisonerType = PrisonerType }))); 

            return PartialView("Index", Model);
        }

        public ActionResult SaveTermination(CaseCloseEntity Data)
        {
            bool status = false;
            int? id = null;

            if (Data.PrisonerID != null)
            {
                CaseCloseEntity activeCase = BusinessLayer_PrisonerService.GetActiveCase(new CaseCloseEntity(PrisonerID: Data.PrisonerID.Value)).First();

                if (activeCase.PCN == Data.PCN)
                {
                    activeCase.CloseTypeLibItemID = Data.CloseTypeLibItemID;
                    activeCase.CloseDate = Data.CloseDate;
                    activeCase.CommandNumber = Data.CommandNumber;
                    activeCase.CloseFromWhomID = Data.CloseFromWhomID;
                    activeCase.Notes = Data.Notes;

                    id = BusinessLayer_PrisonerService.AddCaseClose(activeCase);
                }
            }

            if (id != null) status = true;
            
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Վերջնական պահպանում", RequestData: JsonConvert.SerializeObject(Data))); 

            return Json(new { status = status });
        }

    }
}