﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.DataBans)]
    public class _Data_Ban_Controller : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public _Data_Ban_Controller()
        {
            PermissionHCID = PermissionsHardCodedIds.DataBans;
        }
        /// <summary>
        /// Primary data add partial view
        /// </summary>
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Արգելանքի ավելացում"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }

        /// <summary>
        /// Primary data edit partial view
        /// </summary>
        public ActionResult Edit(int? PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;

            Model.Prisoner.Injunctions = BusinessLayer_PrisonerService.GetInjunctions(PrisonerID.Value);
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Արգելանքի խմբագրում", RequestData: PrisonerID.ToString()));

            return PartialView("Index", Model);
        }

        public ActionResult AddBan(InjunctionItemEntity Item)
        {
            bool status = false;
            int? id = null;
            if (Item.PrisonerID != null)
            {
                id = BusinessLayer_PrisonerService.AddInjunctionItem(Item);
            }

            if (id != null) status = true;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Արգելանքի ավելացում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }

        public ActionResult EditBan(InjunctionItemEntity Item)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetInjunctionItem(new InjunctionItemEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.BAN_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateInjunctions(Item);
            }
            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult RemBan(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetInjunctionItem(new InjunctionItemEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                InjunctionItemEntity oldEntity = BusinessLayer_PrisonerService.GetInjunctionItem(new InjunctionItemEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.BAN_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateInjunctions(new InjunctionItemEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Արգելանքի հեռացում", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult SaveTermination(InjunctionItemEntity Item)
        {
            bool status = false;

            if (Item.ID != null)
            {
                Item.State = false;
                status = BusinessLayer_PrisonerService.UpdateInjunctions(Item);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Արգելանքի վերջնական պահպանում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }


    }
}