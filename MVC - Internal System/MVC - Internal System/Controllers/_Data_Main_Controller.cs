﻿using CommonLayer.BusinessEntities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using BusinessLayer.BusinessServices;
using System.Linq;
using System.IO;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.MainData)]
    public class _Data_Main_Controller : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public _Data_Main_Controller()
        {
            PermissionHCID = PermissionsHardCodedIds.MainData;
        }
        // GET: Main
        [HttpPost]
        public ActionResult Index()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ընդհանուր տվյալներ"));

            return Json(new { status = "success" });
        }

        [HttpPost]
        public ActionResult GetLibs(int LibPathID)
        {
            List<LibsEntity> libs = BusinessLayer_LibsLayer.GetLibsByLibPathID(LibPathID);

            Object obj;
            if (libs != null)
                obj = new { status = "success", data = libs };
            else
                obj = new { status = "error" };
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ դասակարգիչները", RequestData: LibPathID.ToString()));

            return Json(obj);
        }

        [HttpPost]
        public ActionResult GetLibsByPathList(List<int> IDList)
        {
            try
            {
                Dictionary<int, List<LibsEntity>> libs = BusinessLayer_LibsLayer.GetLibsByLibPathID(IDList);

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Վերցնել դասախարգիչները ճանապարհի ցուցակի միջոցով", RequestData: JsonConvert.SerializeObject(IDList)));

                return Json(new { status = "success", data = libs });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                
                return Json(new { status = "error" });
            }
        }

        [HttpPost]
        public ActionResult CheckEmployee(String FirstName, String LastName, String BirthDate)
        {
            AVVPerson person = null;
            person = BusinessLayer_PoliceService.getBYDocumentPersonalData(LastName.ToUpper(), FirstName.ToUpper(), BirthDate);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստուգել աշխատակցին փաստաթղթի համարով", RequestData: JsonConvert.SerializeObject(new { FirstName = FirstName, LastName = LastName, BirthDate = BirthDate })));

            //return transferPerson(person);
            return transferPerson(person);
        }
        

        private ActionResult transferPerson(AVVPerson person) {

            //if (person != null && person.PNum != null && person.AVVAddresses != null)
            //{
            //    Document NonBioPassport = person.AVVDocuments.Document.Find(d => d.Document_Status == Document_Status.ACTIVE && d.Document_Type == Document_Type.NON_BIOMETRIC_PASSPORT);
            //    Document BioPassport = person.AVVDocuments.Document.Find(d => d.Document_Status == Document_Status.ACTIVE && d.Document_Type == Document_Type.BIOMETRIC_PASSPORT);
            //    Document IDCard = person.AVVDocuments.Document.Find(d => d.Document_Status == Document_Status.ACTIVE && d.Document_Type == Document_Type.ID_CARD);
            //    Document document = BioPassport ?? NonBioPassport ?? IDCard;

            //    document = document ?? person.AVVDocuments.Document.First();

            //    AVVAddress PermanentAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Code == "Ն");
            //    PermanentAddress = PermanentAddress ?? person.AVVAddresses.AVVAddress.First();
            //    AVVAddress ActiveAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.A);
            //    AVVAddress TemporaryAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.T);

            //    //=======================
            //    // make document list for _Data_Initial_ subcontroller
            //    List<IdentificationDocument> docIdentities = new List<IdentificationDocument>();
            //    foreach (Document tempDoc in person.AVVDocuments.Document)
            //    {
            //        IdentificationDocument currentDocumentIdentity = new IdentificationDocument();

            //        Document_Type docType = tempDoc.Document_Type;
            //        string docTypeName = BusinessLayer_LibsLayer.GetLibByID((int)docType).Name;

            //        LibsEntity citizenshipLib = this.GetCitizenshipLibByCode((string)tempDoc.Person.Citizenship);

            //        currentDocumentIdentity.TypeLibItem_ID = (int)docType;
            //        currentDocumentIdentity.TypeLibItem_Name = docTypeName;
            //        currentDocumentIdentity.CitizenshipLibItem_ID = citizenshipLib.ID;
            //        currentDocumentIdentity.CitizenshipLibItem_Name = citizenshipLib.Name;
            //        currentDocumentIdentity.Number = tempDoc.Document_Number;
            //        currentDocumentIdentity.FromWhom = tempDoc.Document_Department;
            //        currentDocumentIdentity.Type = true;
            //        currentDocumentIdentity.Date = DateTime.ParseExact(tempDoc.PassportData.Passport_Issuance_Date, "dd/MM/yyyy", null);
            //        currentDocumentIdentity.ID = int.Parse(currentDocumentIdentity.TypeLibItem_ID.ToString() + currentDocumentIdentity.CitizenshipLibItem_ID.ToString());

            //        docIdentities.Add(currentDocumentIdentity);
            //    }
            //    //===========================           

            //    // default image
            //    bool photoExistThere = true;
            //    if (person.Photo_ID == null)
            //    {
            //        photoExistThere = false;
            //        person.Photo_ID = "/9j/4AAQSkZJRgABAQAAAQABAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2NjIpLCBxdWFsaXR5ID0gOTAK/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgBLAEsAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A9Dop2B6UYHpQA2inYHpRgelADaKdgelGB6UANop2B6UYHpQA2inYHpRgelADaKdgelGB6UANop2B6UYHpQA2inYHpRgelADaK0dP8PanquPsenXNyp/ijiYr+eMVvW3wp8S3Iz/Z3lL6yTIP0zmgDkKK7tfg14hYcpbL9Zv8BTH+DviRAdtvBJ/uzgfzxQBw9FdNd/DjxJZKTJpE7Y/55ESf+gk1gXNnNZyGO4gkgkH8EilT+RoAgop2B6UYHpQA2inYHpRgelADaKdgelGB6UANop2B6UYHpQA2inYHpRgelADaKdgelGB6UANop2B6UYHpQA2inYHpRgelABz6Uc+lPooAZz6Uc+lPooAZz6Uc+lPooAZz6Uc+lPooAZz6Uc+lPooAZz6Uc+lPooAZz6U6ON5ZFREZ3Y4CqMkn2rS0Hw/e+JNQSzsYjJIeWY8Ki+rHsK928HfD7T/CUSyKBdX5HzXLryPUKP4R+tAHnPhn4NahqqpPqkn9nW55EQGZWH06L+PPtXpmifD3QdBVTBYJLMP+W1wPMfPrzwPwAro6KAEVQowKWiigAooooAKgvLG21CExXVvFcxHqkyBl/I1PRQBwPiD4OaNqis9iW0y4PI2fNGT7qen4EV5X4n8C6t4UYtdwb7bOFuYvmQ/XuD9a+kqZNDHcRtHKiyRsNrK4BBHoQaAPk/n0o59K9Y8d/CXylkv9CQkctJZDnHun/wAT+XpXlRGDg8EdQe1ADOfSjn0p9FADOfSjn0p9FADOfSjn0p9FADOfSjn0p9FADOfSjn0p9FADOfSjn0p9FAC7T6UbT6U6igBu0+lG0+lOooAbtPpRtPpTqKAG7T6UbT6U6igBu0+lG0+lOooAbtPpWhoOg3fiLVIbG0TMkh5Y/dRe7H2FUa99+GvhAeGdEEsyAahdAPKSOUHZPw7+/wBBQBseFvC1n4T01bS0XLHBlmYfNI3qf6DtWxRRQAUUUUAFFFFABRRRQAUUUUAFFFFABXmXxP8Ahwt9HLq+lwgXSgtcQIP9YO7Af3v5/Xr6bRQB8obT6UbT6V3fxV8IDQNWW9tU22N4Sdq9I5OpHsD1H4+lcNQA3afSjafSnUUAN2n0o2n0p1FADdp9KNp9KdRQA3afSjafSnUUAN2n0o2n0p1FADqKdto20ANop22jbQA2inbaNtADaKdto20ANop22jbQB1Hw20Aa94ptxIu63th58nHBx90ficfhmvoGvO/gtpQt9Du79lxJcy7AcfwKP8S35V6JQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAYvjDQl8R+HruywDKyb4j6OOR/h+NfN5BUkMMMDgg9q+qq+d/iBpQ0nxdqMSjEcj+cn0b5v5kj8KAOcop22jbQA2inbaNtADaKdto20ANop22jbQA2inbaNtADttG2l59KOfSgBNtG2l59KOfSgBNtG2l59KOfSgBNtG2l59KOfSgBNtG2l59KOfSgD6H8BWYsfB+lRgYzCJD/AMC+b+tb9VNIi8jSrOLGBHCi/koq3QAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFePfGqzCazYXIGPNgKE+u1s/+zV7DXmfxuh3WOlS45WSRfzA/wAKAPJNtG2l59KOfSgBNtG2l59KOfSgBNtG2l59KOfSgBNtG2l59KOfSgBNtG2l59KOfSgCTafSjafSnUUAN2n0o2n0p1FADdp9KNp9KdRQA3afSjafSnUUAN2n0o2n0p1FAH05bgCCMDkBRg/hUlV9OkEun2zj+KNT+gqxQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFed/Gn/kDaf8A9fB/9BNeiV5x8apQum6Yn96Zm/Jf/r0AeSbT6UbT6U6igBu0+lG0+lOooAbtPpRtPpTqKAG7T6UbT6U6igBu0+lG0+lOooAk2n0o2n0paKAE2n0o2n0paKAE2n0o2n0paKAE2n0o2n0paKAE2n0o2n0paKAPovw62/QNMb+9axH/AMcFaFYfgi4+0+EtKfOcQKn/AHz8v9K3KACiiigAooooAKKKKACiiigAooooAKKKKACiiigArzD41HcNHQdcyk/+Of8A169Pryb4yXAbVdPg/uQF/wDvpsf+y0AeebT6UbT6UtFACbT6UbT6UtFACbT6UbT6UtFACbT6UbT6UtFACbT6UbT6UtFAD6KfRQAyin0UAMop9FADKKfRQAyin0UAex/Ci9+0+FhCT81vM6Y9j83/ALMa7KvKvg/qPk6je2LH/XRiRR7qcH9G/SvVaACiiigAooooAKKKKACiiigAooooAKKKKACiiigArxD4mXn2vxdcqDlYUSIfgMn9Sa9smlWCJ5HO1EBZiewHU187alenUdRurpvvTSM5B7ZOaAKdFPooAZRT6KAGUU+igBlFPooAZRT6KAH7T6UbT6U6igBu0+lG0+lOooAbtPpRtPpTqKAG7T6UbT6U6igBu0+lG0+lOooA0PDmptouuWd5yEjkAfH908N+hNe/qwdQwIIPIIPUV84V7L8ONd/tfQEhds3FpiJvUr/Cfy4/CgDq6KKKACiiigAooooAKKKKACiiigAooooAKKKKAOX+I+r/ANl+GZ41OJro+So9j979Mj8a8V2n0rrviXrY1bXjbxtmC0HljHQt/Ef6fhXJ0AN2n0o2n0p1FADdp9KNp9KdRQA3afSjafSnUUAN2n0o2n0p1FADdp9KNp9KdRQBJtPpRtPpS0UAJtPpRtPpS0UAJtPpRtPpS0UAJtPpRtPpS0UAJtPpRtPpS0UAJtPpW54N19vDutRTOSLaT93MP9k9/wAOtYlFAH0SjiRQykMpAIIOQRTq4P4aeKPtdsNKuW/fQj9wx/iT0+o/l9K7ygAooooAKKKKACiiigAooooAKKKKACsLxn4hHh3RZJkI+0yZjhH+0e/4dfyrammS3ieWRgkaAszE4AA6mvEvFviJ/EmqvMMi2j+SFD2X1+p/w9KAMMhmJJySTkk96Np9KWigBNp9KNp9KWigBNp9KNp9KWigBNp9KNp9KWigBNp9KNp9KWigBNp9KNp9KWigB/PpRz6U+igBnPpRz6U+igBnPpRz6U+igBnPpRz6U+igBnPpRz6U+igBnPpRz6U+igBba4ms7iOeFjHLGwZWHUGvaPCfieLxJp4k+WO5jGJoweh9R7GvFqs6bqdzpF4lzaymKVO46EehHcUAe+UVneHtTfWdHtb10EbyrkqpyAQSP6Vo0AFFFFABRRRQAUUUUAFFFcL8SPEl3pxisLZvKE0ZZ5VPzYyRgenSgDL+Ini77a7aXZvmBD++kU8Of7o9h/OuF59KfRQAzn0o59KfRQAzn0o59KfRQAzn0o59KfRQAzn0o59KfRQAzn0o59KfRQAzn0o59KfRQA/afSjafSnUUAN2n0o2n0p1FADdp9KNp9KdRQA3afSjafSnUUAN2n0o2n0p1FADdp9KNp9KcASQACSewrZ07wdq+pgGOzeND/HN8g/XmgDIgtZrqQRwxPLIeiopYn8BXS6f8ONXvYi8oitOOFlbk/gM4r0Pwxoo0PSYbdliM4B8x4x945PfgmtagDJ8K2UunaDa2067Jotysv8AwI1rUUUAFFFFABRRRQAUUUUAFcZ4w8JXniXVongZIoooMF5M4J3HgYrs6KAPFdV8IarpG5prVniH/LWL5l/+t+NY+0+lfQVcN408FXWqXy3WnxQf6sB0BCMzZPPp0I70AebbT6UbT6Vd1DSL7S2xd2ksH+0ynH59KqUAN2n0o2n0p1FADdp9KNp9KdRQA3afSjafSnUUAN2n0o2n0p1FADdp9KNp9KdRQA7n0o59KfRQAzn0o59KfRQAzn0o59KfRQAzn0o59K6LQvBeoa0FkKfZbY/8tZR1HsOp/lXeaP4I0zSdrmP7VOP+Wk3P5L0FAHm+leF9T1jabe2YRn/lrJ8qfn3/AArr9M+GMKYa/uWlbvHD8q/meT+ldz0ooAoadoVhpQxa2scR/vgZb8zzV+iigAooooAKKKKACiiigAooooAKKKKACiiigAooooARlDqQwDA9QehrC1LwRpGp5Y2wt5D/AB252fp0/St6igDzTU/hleWwZrKdbpeojf5X/wAD+lcpeWFzp8vl3NvJA/o4xn6ete7VDd2UF9EYriJJoz/C6gigDwfn0o59K9I1n4a28+6TTpTA5/5ZSElD9D1H61wup6Vd6PP5V3A0Tdifut9D0NAFHn0o59KfRQAzn0o59KfRQAzn0o59KfRQA/n0o59KfRQAzn0o59KfVnTtPm1W9itbdd0jn8AO5PtQBHp+n3Gp3SW9tEZJW7DsPUnsK9H8O+BbXSws10Fu7vr8w+RD7Dv9TWtoOg2+g2ghhG6Q/wCslI5c/wCHtWnQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVDeWUF/A0NxEs0TdVcZqaigDzfxL4ClsQ9zp+6eActCeXQe3qP1rj+fSveK4jxt4QWSOTUbJAsi5aaJRww7sB6+tAHn3PpRz6U+igBnPpRz6U+igB2B6UYHpT8D0owPSgBmB6V6L8O9IW105r5lxLOcKSOiD/E/yFefxQmaVI0GWdgoHua9osrVbG0ht0+5EgQfgKAJqKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA8l8W6Quka1KiLiGUeZGB0APb8DmsbA9K9E+I1gJtOt7oDLQvsP8Aun/64H5159gelADMD0owPSn4HpRgelAC7T6UbT6U/bRtoA1vB9p9q8RWgI+VCZD+AyP1xXq1ef8Aw7tw2pXMxGdkQUH6kf4V6BQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQBm+I7T7dod7FjJMRYfUcj9RXkW0+le3MoYEEZBGDXjFzB9nuJYieUYqfwNAEG0+lG0+lP20baAFop22jbQB3Hw4jAtr2T+86r+QP+NdjXL/D1NujTH+9Of/QVrqKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACvJfEUQh12+X/pqx/M5r1qvL/GKbfEd52yVP5qKAMSinbaNtADtp9KNp9KdRQB6H4DAGhf8AbVv6V0Vc74E/5AX/AG1b+ldFQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAV5r42U/wDCQznHVV/lXpVeb+NjnxBN/ur/ACoAwNp9KNp9KdRQAUU6igDuvh9OH0ueLukufwIH+Brqa4f4fyML26QH5WjBI9wf/rmu4oAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK8w8Vzi48QXjDorBPyAB/UV6fXkNy5lupnY5ZnLE+5NAEFFOooA//2Q==";
            //    }
            if (person != null)
            {

                PersonEntity Person = person.ToPerson();
                NationalityEntity Nationality = BusinessLayer_LibsLayer.GetNationality(ID: Person.NationalityID.Value).First();
                Person.NationalityLabel = (Nationality.Label != null) ? Nationality.Label : "";
                string Citizenship = "";
                if(Person.IdentificationDocuments.Any() && Person.IdentificationDocuments.First().CitizenshipLibItem_ID != null) {

                    LibsEntity CitizenshipLib = BusinessLayer_LibsLayer.GetLibByID(Person.IdentificationDocuments.First().CitizenshipLibItem_ID.Value);
                    Citizenship = (CitizenshipLib.Name != null) ? CitizenshipLib.Name : "";
                }
                return Json(new
                {
                    Birthday = Person.Birthday.Value.ToString("dd/MM/yyyy"),
                    FirstName = Person.FirstName,
                    IdentificationDocuments = Person.IdentificationDocuments,
                    isCustom = Person.isCustom,
                    isEditable = Person.isEditable,
                    LastName = Person.LastName,
                    Living_Entity = Person.Living_Entity,
                    Living_ID = Person.Living_ID,
                    Living_place = Person.Living_place,
                    MiddleName = Person.MiddleName,
                    NationalityID = Person.NationalityID,
                    NationalityLabel = Person.NationalityLabel,
                    Personal_ID = Person.Personal_ID,
                    PhotoLink = (Person.Photo_ID == null || Person.Photo_ID == "") ? "" : "data:image/jpeg;base64," + Person.Photo_ID,
                    Photo_ID = Person.Photo_ID,
                    Registration_address = Person.Registration_address,
                    Registration_Entity = Person.Registration_Entity,
                    Registration_ID = Person.Registration_ID,
                    RelativeLabel = Person.RelativeLabel,
                    SexLibItem_ID = Person.SexLibItem_ID,
                    Status = Person.Status,
                    Citizenship = Citizenship,
                });
            }
            else
            {
                return Json(new { Status = false });
            }
        }

        [HttpPost]
        public ActionResult CheckEmployeeByDocNumber(String DocNumber, bool Type)
        {
            //bool result = BusinessLayer_OrgLayer.AddEmployee(OrgUnitID, FirstName, LastName);

            AVVPerson person = null;
            if (Type)
                person = BusinessLayer_PoliceService.getBYDocumentNumber(DocNumber);
            else
                person = BusinessLayer_PoliceService.getBYPersonalNumber(DocNumber);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստուգել աշխատակցին փաստաթղթի համարով", RequestData: JsonConvert.SerializeObject(new { DocNumber = DocNumber, Type = Type })));

            return transferPerson(person);


        }

        private LibsEntity GetCitizenshipLibByCode(string Code)
        {
            LibsEntity finalLib = null;
            try
            {
                List<LibsEntity> list = BusinessLayer_LibsLayer.GetCitizenshipLibsByCode(Code);
                if (list != null && list.Any())
                {
                    finalLib = list.First();
                }
                    

                // TODO: Later: if needed add properties tu finalLip.propList
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ստանալ քաղաքացիությունը", RequestData: Code));

            return finalLib;
        }

        public ActionResult AddFile(int? PrisonerID, string Description, bool State, int TypeID)
        {
            bool status = false;
            int? fileID = null;

            if (PrisonerID != null)
            {
            PrisonerEntity currentPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: PrisonerID.Value);
                // adding file
                if (Request.Files.Count > 0)
                {
                    //BusinessLayer_PrisonerService.ChangeFileType

                    // TODO: get prisoner Personal_ID and save to file
                    PrisonerEntity prisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value,true);

                    HttpPostedFileBase file = Request.Files[0];
                    AdditionalFileEntity fileEntry = new AdditionalFileEntity(file.ContentLength);

                    fileEntry.FileName = Path.GetFileName(file.FileName);

                    fileEntry.FileExtension = Path.GetExtension(file.FileName);
                    fileEntry.ContentType = file.ContentType;
                    fileEntry.PersonID = (int)currentPrisoner.PersonID;
                    fileEntry.PrisonerID = PrisonerID;
                    fileEntry.Description = Description;
                    fileEntry.State = State;
                    fileEntry.isPrisoner = true;
                    fileEntry.PNum = prisoner.Person.Personal_ID;
                    fileEntry.TypeID = (FileType)TypeID;
                    //if (Type == "image") fileEntry.TypeID = FileType.MAIN_DATA_ATTACHED_IMAGES;
                    //else if (Type == "main") fileEntry.TypeID = FileType.MAIN_DATA_ATTACHED_FILES;
                    //else if (Type == "physical") fileEntry.TypeID = FileType.PHYSICAL_DATA_ATTACHED_FILES;
                    //else if (Type == "army") fileEntry.TypeID = FileType.ARMY_ATTACHED_FILES;
                    //else if (Type == "items") fileEntry.TypeID = FileType.ITEMS_ATTACHED_FILES;
                    //else if (Type == "cell") fileEntry.TypeID = FileType.CAMERA_CARD_ATTACHED_FILES;
                    //else if (Type == "bans") fileEntry.TypeID = FileType.INJUNCTIONS_ATTACHED_FILES;
                    //else if (Type == "adfile") fileEntry.TypeID = FileType.ADOPTION_INFORMATION_ATTACHED_FILES;
                    //else if (Type == "adsfile") fileEntry.TypeID = FileType.ADOPTION_INFORMATION_ARREST_RECORD;
                    //else if (Type == "family") fileEntry.TypeID = FileType.FAMILY_ATTACHED_FILES;
                    file.InputStream.Read(fileEntry.FileContent, 0, fileEntry.FileContent.Length);

                    fileID = BL_Files.getInstance().AddFile(fileEntry);
                }

                if (fileID != null)
                {
                    status = true;
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել ֆայլ", RequestData: JsonConvert.SerializeObject(new { PrisonerID = PrisonerID, Description = Description, State = State, TypeID = TypeID })));

            return Json(new
            {
                status = status,
                type = TypeID
            });
        }

        public ActionResult RemoveFile(int ID, int TypeID)
        {
            bool status = false;
            bool needApprove = false;

            AdditionalFileEntity curEntity = new AdditionalFileEntity();
            curEntity.ID = ID;
            curEntity.Status = false;

            int? ViewType = null;
            switch (TypeID) {
                case (int)FileType.PHYSICAL_DATA_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.PHYSICAL_FILES;
                    break;
                case (int)FileType.MAIN_DATA_ATTACHED_IMAGES:
                    ViewType = (int)MergeEntity.TableIDS.PRIMARY_IMAGES;
                    break;
                case (int)FileType.MAIN_DATA_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.PRIMARY_FIELS;
                    break;
                case (int)FileType.MEDICAL_AMBULATOR:
                case (int)FileType.MEDICAL_PRELIMINARY:
                case (int)FileType.MEDICAL_STATIONARY:
                    ViewType = (int)MergeEntity.TableIDS.MEDICAL_FILES;
                    break;
                case (int)FileType.FAMILY_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.FAMILY_FILES;
                    break;
                case (int)FileType.ARMY_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.ARMY_FILES;
                    break;
                case (int)FileType.ADOPTION_INFORMATION_ARREST_RECORD:
                    ViewType = (int)MergeEntity.TableIDS.ENTERANCE_FILES_ARREST;
                    break;
                case (int)FileType.ADOPTION_INFORMATION_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.ENTERANCE_FILES;
                    break;
                case (int)FileType.CAMERA_CARD_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.CELL_FILES;
                    break;
                case (int)FileType.ITEMS_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.ITEMS_FILES;
                    break;
                case (int)FileType.INJUNCTIONS_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.BAN_FILES;
                    break;
                case (int)FileType.ENCOURAGEMENTS:
                    ViewType = (int)MergeEntity.TableIDS.ENCOURAGEMENTS_FILES;
                    break;
                case (int)FileType.PENALTIES:
                    ViewType = (int)MergeEntity.TableIDS.PENALTY_FILES;
                    break;
                case (int)FileType.EDUCATIONAL_COURSES:
                    ViewType = (int)MergeEntity.TableIDS.EDUCATIONAL_COURSES_FILES;
                    break;
                case (int)FileType.WORKLOADS:
                    ViewType = (int)MergeEntity.TableIDS.WORKLOADS_FILES;
                    break;
                case (int)FileType.OFFICIAL_VISITS:
                    ViewType = (int)MergeEntity.TableIDS.OFFICIAL_VISITS_FILES;
                    break;
                case (int)FileType.PERSONAL_VISITS:
                    ViewType = (int)MergeEntity.TableIDS.PERSONAL_VISITS_FILES;
                    break;
                case (int)FileType.PACKAGES:
                    ViewType = (int)MergeEntity.TableIDS.PACKAGES_FILES;
                    break;
                case (int)FileType.DEPARTURES:
                    ViewType = (int)MergeEntity.TableIDS.DEPARTURES_FILES;
                    break;
                case (int)FileType.TRANSFERS:
                    ViewType = (int)MergeEntity.TableIDS.TRANSFERS_FILES;
                    break;
                case (int)FileType.CONFLICTS:
                    ViewType = (int)MergeEntity.TableIDS.CONFLICTS_FILES;
                    break;
            }
            AdditionalFileEntity oldEntity = BL_Files.getInstance().GetFile(id: curEntity.ID.Value);
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(oldEntity.PrisonerID.Value, false);
            if ((int)System.Web.HttpContext.Current.Session["PPType"] == 2 && ViewType != null && curPrisoner.State.Value)
            {
                MergeEntity curMergeEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(curEntity),
                    Type: 2,
                    EntityID: curEntity.ID,
                    EID: ViewType);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curMergeEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {

                status = BL_Files.getInstance().UpdateFile(curEntity);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել ֆայլը", RequestData: JsonConvert.SerializeObject(new { ID = ID, TypeID = TypeID })));

            return Json(new { status = status, type = TypeID, needApprove = needApprove });
        }

        public ActionResult EditFile(AdditionalFileEntity File)
        {
            bool status = false;
            bool needApprove = false;

            AdditionalFileEntity curEntity = File;
            AdditionalFileEntity oldEntity = BL_Files.getInstance().GetFile(id: curEntity.ID.Value);
            curEntity.TypeID = oldEntity.TypeID;
            curEntity.ModifyDate = DateTime.Now;
            curEntity.ContentLength = oldEntity.ContentLength;
            int? ViewType = null;
            switch ((int)curEntity.TypeID)
            {
                case (int)FileType.PHYSICAL_DATA_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.PHYSICAL_FILES;
                    break;
                case (int)FileType.MAIN_DATA_ATTACHED_IMAGES:
                    ViewType = (int)MergeEntity.TableIDS.PRIMARY_IMAGES;
                    break;
                case (int)FileType.MAIN_DATA_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.PRIMARY_FIELS;
                    break;
                case (int)FileType.MEDICAL_AMBULATOR:
                case (int)FileType.MEDICAL_PRELIMINARY:
                case (int)FileType.MEDICAL_STATIONARY:
                    ViewType = (int)MergeEntity.TableIDS.MEDICAL_FILES;
                    break;
                case (int)FileType.FAMILY_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.FAMILY_FILES;
                    break;
                case (int)FileType.ARMY_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.ARMY_FILES;
                    break;
                case (int)FileType.ADOPTION_INFORMATION_ARREST_RECORD:
                    ViewType = (int)MergeEntity.TableIDS.ENTERANCE_FILES_ARREST;
                    break;
                case (int)FileType.ADOPTION_INFORMATION_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.ENTERANCE_FILES;
                    break;
                case (int)FileType.CAMERA_CARD_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.CELL_FILES;
                    break;
                case (int)FileType.ITEMS_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.ITEMS_FILES;
                    break;
                case (int)FileType.INJUNCTIONS_ATTACHED_FILES:
                    ViewType = (int)MergeEntity.TableIDS.BAN_FILES;
                    break;
                case (int)FileType.ENCOURAGEMENTS:
                    ViewType = (int)MergeEntity.TableIDS.ENCOURAGEMENTS_FILES;
                    break;
                case (int)FileType.PENALTIES:
                    ViewType = (int)MergeEntity.TableIDS.PENALTY_FILES;
                    break;
                case (int)FileType.EDUCATIONAL_COURSES:
                    ViewType = (int)MergeEntity.TableIDS.EDUCATIONAL_COURSES_FILES;
                    break;
                case (int)FileType.WORKLOADS:
                    ViewType = (int)MergeEntity.TableIDS.WORKLOADS_FILES;
                    break;
                case (int)FileType.OFFICIAL_VISITS:
                    ViewType = (int)MergeEntity.TableIDS.OFFICIAL_VISITS_FILES;
                    break;
                case (int)FileType.PERSONAL_VISITS:
                    ViewType = (int)MergeEntity.TableIDS.PERSONAL_VISITS_FILES;
                    break;
                case (int)FileType.PACKAGES:
                    ViewType = (int)MergeEntity.TableIDS.PACKAGES_FILES;
                    break;
                case (int)FileType.DEPARTURES:
                    ViewType = (int)MergeEntity.TableIDS.DEPARTURES_FILES;
                    break;
                case (int)FileType.TRANSFERS:
                    ViewType = (int)MergeEntity.TableIDS.TRANSFERS_FILES;
                    break;
                case (int)FileType.CONFLICTS:
                    ViewType = (int)MergeEntity.TableIDS.CONFLICTS_FILES;
                    break;
            }
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(oldEntity.PrisonerID.Value, false);
            if ((int)System.Web.HttpContext.Current.Session["PPType"] == 2 && ViewType != null && curPrisoner.State.Value)
            {

                MergeEntity curMergeEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(curEntity),
                    Type: 3,
                    EntityID: curEntity.ID,
                    EID: ViewType);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curMergeEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BL_Files.getInstance().UpdateFile(File);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել ֆայլը", RequestData: JsonConvert.SerializeObject(File)));

            return Json(new { status = status, needApprove = needApprove });
        }

        /// <summary>
        /// check if there is merge for approve or not
        /// </summary>
        [HttpPost]
        public ActionResult checkMergeApprove(int PrisonerID)
        {
            bool status = true;
            bool needApprove = true;

            MergeObjectEntity mergeObject = BusinessLayer_MergeLayer.GetPrisonerActiveModifications(PrisonerID: PrisonerID);
            needApprove = (mergeObject.Merge != null && mergeObject.Merge.Any()) ? true : false;

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult CheckMergeEIDs(int PrisonerID)
        {
            List<string> EIds = BusinessLayer_MergeLayer.GetMergEIDs(PrisonerID);

            return Json(new { EIds = EIds });
        }
    }
}
