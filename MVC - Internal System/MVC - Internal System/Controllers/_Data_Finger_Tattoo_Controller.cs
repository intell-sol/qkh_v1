﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.IO;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.FingerprintsTattoosScars)]
    public class _Data_Finger_Tattoo_Controller : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public _Data_Finger_Tattoo_Controller()
        {
            PermissionHCID = PermissionsHardCodedIds.FingerprintsTattoosScars;
        }
        /// <summary>
        /// Primary data add partial view
        /// </summary>
        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել մատնահետք/դաջվածք"));

            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }

        [HttpPost]
        /// <summary>
        /// Primary data edit partial view
        /// </summary>
        public ActionResult Edit(int PrisonerID)
        {
            PrisonersViewModel Model = new PrisonersViewModel();

            Model.Prisoner = new PrisonerEntity();

            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            Model.Prisoner.ID = PrisonerID;

            Model.Prisoner.FingerprintsTattoosScars = BusinessLayer_PrisonerService.GetFingerprintsTattoosScars(Model.Prisoner.ID.Value);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել մատնահետքը/դաջվածքը", RequestData: PrisonerID.ToString()));

            return PartialView("Index", Model);
        }

        public ActionResult addFingerTatooScar(FingerprintsTattoosScarsItemEntity Entity)
        {
            bool status = false;

            int? id = null;
            int? fileID = null;

            if (Entity.PrisonerID != null)
            {
            PrisonerEntity currentPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID: Entity.PrisonerID.Value);
                if (Request.Files.Count > 0)
                {
                    //BusinessLayer_PrisonerService.ChangeFileType

                    PrisonerEntity prisoner = BusinessLayer_PrisonerService.GetPrisoner(Entity.PrisonerID.Value, true);

                    HttpPostedFileBase file = Request.Files[0];
                    AdditionalFileEntity fileEntry = new AdditionalFileEntity(file.ContentLength);

                    fileEntry.FileName = Path.GetFileName(file.FileName);

                    fileEntry.FileExtension = Path.GetExtension(file.FileName);
                    fileEntry.ContentType = file.ContentType;
                    fileEntry.PersonID = currentPrisoner.PersonID;
                    fileEntry.Description = Entity.Description;
                    fileEntry.State = Entity.State;
                    fileEntry.isPrisoner = true;
                    fileEntry.PNum = prisoner.Person.Personal_ID;
                    fileEntry.TypeID = (FileType)Entity.TypeID;
                    //if (Entity.TypeID == FTS_TYPE.Fingerprint) fileEntry.TypeID = FileType.FINGERTATOOSCARS_FINGER;
                    //else if (Entity.TypeID == FTS_TYPE.Tattoo) fileEntry.TypeID = FileType.FINGERTATOOSCARS_TATOO;
                    //else if (Entity.TypeID == FTS_TYPE.Scar) fileEntry.TypeID = FileType.FINGERTATOOSCARS_SCAR;
                    file.InputStream.Read(fileEntry.FileContent, 0, fileEntry.FileContent.Length);

                    fileID = BL_Files.getInstance().AddFile(fileEntry);

                    Entity.FileID = fileID;
                }

                id = BusinessLayer_PrisonerService.AddFingerprintsTattoosScars(Entity);

                if (id != null) status = true;
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ավելացնել մատնահետք/դաջվածք/սպի", RequestData: JsonConvert.SerializeObject(Entity)));

            return Json(new { status = status, type = Entity.TypeID });
        }

        public ActionResult remFingerTatooScar(int ID, int TypeID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetFingerprintsTattoosScarsItem(ID: ID).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                FingerprintsTattoosScarsItemEntity oldEntity = BusinessLayer_PrisonerService.GetFingerprintsTattoosScarsItem(ID: ID).First();
                int EID = (int)MergeEntity.TableIDS.FINGERTATOOSCARS_FINGERS;
                switch (oldEntity.TypeID)
                {
                    case 18:
                        EID = (int)MergeEntity.TableIDS.FINGERTATOOSCARS_FINGERS;
                        break;
                    case 20:
                        EID = (int)MergeEntity.TableIDS.FINGERTATOOSCARS_TATOOS;
                        break;
                    case 22:
                        EID = (int)MergeEntity.TableIDS.FINGERTATOOSCARS_SCARS;
                        break;

                }

                oldEntity.PrisonerID = oldEntity.PrisonerID;
                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: oldEntity.ID,
                    EID: EID);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateFingerprintsTattoosScarsItem(new FingerprintsTattoosScarsItemEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Հեռացնել մատնահետք/դաջվածք/սպի", RequestData: JsonConvert.SerializeObject(new { ID = ID, TypeID = TypeID})));

            return Json(new { status = status, type = TypeID, needApprove = needApprove });
        }

        public ActionResult editFingerTatooScar(FingerprintsTattoosScarsItemEntity Item)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetFingerprintsTattoosScarsItem(ID: Item.ID).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                FingerprintsTattoosScarsItemEntity oldEntity = BusinessLayer_PrisonerService.GetFingerprintsTattoosScarsItem(ID: Item.ID).First();
                int EID = (int)MergeEntity.TableIDS.FINGERTATOOSCARS_FINGERS;
                switch (oldEntity.TypeID)
                {
                    case 18:
                        EID = (int)MergeEntity.TableIDS.FINGERTATOOSCARS_FINGERS;
                        break;
                    case 20:
                        EID = (int)MergeEntity.TableIDS.FINGERTATOOSCARS_TATOOS;
                        break;
                    case 22:
                        EID = (int)MergeEntity.TableIDS.FINGERTATOOSCARS_SCARS;
                        break;

                }

                Item.PrisonerID = oldEntity.PrisonerID;
                Item.FileID = oldEntity.FileID;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: EID);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateFingerprintsTattoosScarsItem(Item);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Խմբագրել մատնահետքը/դաջվածքը/սպին", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status, needApprove = needApprove });
        }
    }
}