﻿using BusinessLayer.BusinessServices;
using CommonLayer.BusinessEntities;
using MVC___Internal_System.Attributes;
using MVC___Internal_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.ViewLogs)]
    public class ViewLogsController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public ViewLogsController()
        {
            PermissionHCID = PermissionsHardCodedIds.ViewLogs;
        }

        // GET: ViewLog
        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Լոգերի դիտում";

                ViewLogsViewModel Model = new ViewLogsViewModel();
                
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];

                List<EmployeeEntity> employees = BusinessLayer_OrganizatonLayer.GetOrganizationEmployees(User.SelectedOrgUnitID);
                Model.OrgUnitEmployees = employees;
                List<OrgUnitEntity> organisations = BusinessLayer_OrganizatonLayer.GetPositionsByOrgUnitID(User.SelectedOrgUnitID);
                Model.OrgUnitPositions = organisations;
                
                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        public ActionResult Draw_ViewLogsTableRows()
        {
            FilterLogAcionEntity FilterViewLog = new FilterLogAcionEntity();
            if ((System.Web.HttpContext.Current.Session["FilterViewLogs"] as FilterLogAcionEntity) != null)
                FilterViewLog = (FilterLogAcionEntity)System.Web.HttpContext.Current.Session["FilterViewLogs"];

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            FilterViewLog.paging = new FilterLogAcionEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            LogActionEntity entity = new LogActionEntity();

            entity.EmployeeID = FilterViewLog.EmployeeID;
            entity.PositionID = FilterViewLog.PositionID;

            List<LogActionEntity> LogActionList = BL_Log.getInstance().GetLogActionsForDashboard(FilterViewLog);
            
            ViewLogsViewModel Model = new ViewLogsViewModel();

            Model.LogActionList = LogActionList;
            Model.LogActionEntityPaging = FilterViewLog.paging;
            
            return PartialView("ViewLogs_Table_Row", Model);
        }

        [HttpPost]
        public ActionResult Draw_ViewLogsTableRowsByFilter(FilterLogAcionEntity FilterViewLog)
        {
            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterViewLogs"] = FilterViewLog;

            FilterViewLog.paging = new FilterLogAcionEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<LogActionEntity> LogActionList = BL_Log.getInstance().GetLogActionsForDashboard(FilterViewLog);

            ViewLogsViewModel Model = new ViewLogsViewModel();

            Model.LogActionList = LogActionList;

            //Model.ListPrisoners = PrisonersList;
            Model.FilterLogAction = FilterViewLog;
            Model.LogActionEntityPaging = FilterViewLog.paging;

            //BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Դատապարտյալներ Որոնում", RequestData: JsonConvert.SerializeObject(FilterViewLog)));

            return PartialView("ViewLogs_Table_Row", Model);
        }

    }
}