﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using System.Security.Cryptography.X509Certificates;

namespace MVC___Internal_System.Controllers
{
    public class LoginController : BaseController
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {            
            //Need not do anything here!!!
        }

        /// <summary>
        /// Main function for working with login page.
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            
                //BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Մուտք", RequestData: JsonConvert.SerializeObject(Request.ClientCertificate)));
            try
            {
                // check if user is logged in or not
                if ((Session["User"] as BEUser) != null)
                {
                    // dont go to login page, redirect to lists
                    return RedirectToAction("Index", "Convicts");
                }
                else
                {
                    // page title
                    ViewBag.Title = "Մուտք";

                    return View();
                }
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        /// <summary>
        /// Login action method
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogIn(LoginViewModel model)
        {
            try
            {
                string strUserName = model.UserName;
                string strPassword = model.Password;

                // calling user login method from business layer
                BEUser User = BusinesLayer_UsersAuthentication.UserLogIn(strUserName, strPassword);
                if (User != null)
                {
                    EmployeeEntity employee = BusinessLayer_OrganizatonLayer.GetEmployee(User.ID);
                    OrgUnitEntity employeeOrgUnitEntity = BusinessLayer_OrganizatonLayer.GetOrgUnit(ID: employee.OrgUnitID, ParentID: null);
                    List<OrgUnitEntity> qkhOrgUnits = BusinessLayer_OrganizatonLayer.GetOrgUnitByTypeID(3);
                    Dictionary<int, string> VisibleOrgUnits = new Dictionary<int, string>();
                    foreach (int VisibleOrgUnitID in employeeOrgUnitEntity.VisibleOrgUnits)
                    {
                        OrgUnitEntity e = qkhOrgUnits.Find(orgUnit => orgUnit.ID == VisibleOrgUnitID);
                        VisibleOrgUnits.Add(VisibleOrgUnitID, e.Label);
                    }
                    User.PositionOrgUnitID = employeeOrgUnitEntity.ID;
                    User.SelectedOrgUnitID = VisibleOrgUnits.First().Key;
                    User.VisibleOrgUnits = VisibleOrgUnits;
                    
                    List<PermEntity> permissions = BusinessLayer_PermLayer.GetPermissionList(employeeOrgUnitEntity.ID);
                    User.Permissions = permissions;
                    User.PermissionGuards = BusinessLayer_PrisonerService.GetPermissionGuard(PositionID: employee.OrgUnitID, Full: false);

                    // store user in session
                    Session["User"] = User;
                    BL_Log.getInstance().LogAction(new LogActionEntity( ActionName: "Մուտք", RequestData: JsonConvert.SerializeObject(User)));
                    // redirect to lists
                    return RedirectToAction("Index", "Dashboard");
                }
                else
                {
                    // wrong login
                    ViewBag.ErrorMessage = "Մուտքագրեք ճիշտ տվյալներ";

                    return View();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return View("Error");
            }
        }

        /// <summary>
        /// LogOut action method
        /// </summary>
        public ActionResult Logout()
        {
            try
            {
                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Ելք"));
                // making login session null
                Session["User"] = null;

                return RedirectToAction("Login", "Login");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                // if Session key changed
                System.Web.HttpContext.Current.Session["User"] = null;

                return RedirectToAction("Login", "Login");
            }
        }
    }
}