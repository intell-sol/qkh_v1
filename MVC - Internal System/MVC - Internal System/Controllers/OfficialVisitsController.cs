﻿using MVC___Internal_System.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BusinessLayer.BusinessServices;
using Newtonsoft.Json;
using MVC___Internal_System.Attributes;
using Microsoft.Reporting.WebForms;

namespace MVC___Internal_System.Controllers
{
    [PermissionLevel(PermissionHCID = PermissionsHardCodedIds.OfficialVisits)]
    public class OfficialVisitsController : BaseController
    {
        protected override PermissionsHardCodedIds PermissionHCID { get; set; }

        public OfficialVisitsController()
        {
            PermissionHCID = PermissionsHardCodedIds.OfficialVisits;
        }

        public ActionResult Index()
        {
            try
            {
                // page title
                ViewBag.Title = "Այցելություններ";

                OfficialVisitsViewModel Model = new OfficialVisitsViewModel();
                
                Model.FilterOfficialVisits = new FilterOfficialVisitsEntity();
                if ((System.Web.HttpContext.Current.Session["FilterOfficialVisits"] as FilterOfficialVisitsEntity) != null)
                    Model.FilterOfficialVisits = (FilterOfficialVisitsEntity)System.Web.HttpContext.Current.Session["FilterOfficialVisits"];

                Model.TypeLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.OFFICIAL_VISITS_TYPE);
                Model.PositionLibList = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.OFFICIAL_VISITS_POSITION);

                BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Այցելություններ"));

                return View(Model);
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        public ActionResult Add()
        {
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Այցելության Ավելացում"));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            return PartialView("Index");
        }

        [HttpPost]
        public ActionResult Edit(int? PrisonerID)
        {
            OfficialVisitsViewModel Model = new OfficialVisitsViewModel();

            Model.OfficialVisit = BusinessLayer_PrisonerService.GetOfficialVisit(PrisonerID: PrisonerID.Value);

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Այցելության Խմբագրում", RequestData: PrisonerID.ToString()));
            System.Web.HttpContext.Current.Session["PPType"] = ViewBag.PPType;

            Model.Prisoner = new PrisonerEntity();

            Model.Prisoner.ID = PrisonerID.Value;
            return PartialView("Add", Model);
        }
        public ActionResult Draw_ResetOfficialVisitTableRow()
        {
            Session["FilterOfficialVisits"] = null;
            return Json(new { status = true });
        }
        public ActionResult Draw_OfficialVisitTableRow(int? id = null)
        {
            // UNDONE: watch this section
            FilterOfficialVisitsEntity FilterEntity = new FilterOfficialVisitsEntity();
            if ((System.Web.HttpContext.Current.Session["FilterOfficialVisits"] as FilterOfficialVisitsEntity) != null)
                FilterEntity = (FilterOfficialVisitsEntity)System.Web.HttpContext.Current.Session["FilterOfficialVisits"];

            int page = id ?? 1;

            FilterEntity.paging = new FilterOfficialVisitsEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<OfficialVisitsDashboardEntity> ListDashboardOfficialVisits = BusinessLayer_PrisonerService.GetOfficialVisitsForDashboard(FilterEntity);

            OfficialVisitsViewModel Model = new OfficialVisitsViewModel();

            Model.ListDashboardOfficialVisits = ListDashboardOfficialVisits;
            Model.OfficialVisitsEntityPaging = FilterEntity.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Այցելությունների Որոնում"));

            return PartialView("OfficialVisits_Table_Row", Model);
        }

        public ActionResult Draw_OfficialVisitTableRowByFilter(FilterOfficialVisitsEntity FilterData)
        {
            //FilterPrisonerEntity FilterPrisoner = new FilterPrisonerEntity();

            int page = RouteData.Values.ContainsKey("id") ? int.Parse(RouteData.Values["id"].ToString()) : 1;

            System.Web.HttpContext.Current.Session["FilterOfficialVisits"] = FilterData;

            FilterData.paging = new FilterOfficialVisitsEntity.Paging(CurrentPage: page, ViewCount: CommonLayer.HelperClasses.ConfigurationHelper.GetPrisonerDashboardListViewCount());

            List<OfficialVisitsDashboardEntity> EntityList = BusinessLayer_PrisonerService.GetOfficialVisitsForDashboard(FilterData);

            OfficialVisitsViewModel Model = new OfficialVisitsViewModel();

            Model.ListDashboardOfficialVisits = EntityList;
            Model.OfficialVisitsEntityPaging = FilterData.paging;

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Այցելությունների Որոնում", RequestData: JsonConvert.SerializeObject(FilterData)));

            return PartialView("OfficialVisits_Table_Row", Model);
        }

        public ActionResult AddData(OfficialVisitsEntity Item)
        {
            bool status = false;
            int? id = null;

            if (Item.PrisonerID != null)
            {
                BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];

                if (Item.VisitorList != null && Item.VisitorList.Any())
                {
                    status = true;

                    for (int i = 0; i < Item.VisitorList.Count; i++)
                    {
                        Item.VisitorList[i].Person = BusinessLayer_PrisonerService.AddPersonAndGet(Person: Item.VisitorList[i].Person);
                    }
                }

                if (status)
                {
                    foreach (VisitorEntity curEntity in Item.VisitorList)
                    {
                        OfficialVisitsEntity tempEnt = Item;
                        tempEnt.PositionLibItemID = curEntity.PositionLibItemID;
                        tempEnt.PersonID = curEntity.Person.ID;
                        id = BusinessLayer_PrisonerService.AddOfficialVisit(tempEnt);
                    }
                }
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Այցելության Ավելացում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status });
        }

        public ActionResult RemData(int ID)
        {
            bool status = false;
            bool needApprove = false;

            int? PrisonerID = BusinessLayer_PrisonerService.GetOfficialVisits(new OfficialVisitsEntity(ID: ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && curPrisoner.State.Value)
            {
                OfficialVisitsEntity oldEntity = BusinessLayer_PrisonerService.GetOfficialVisits(new OfficialVisitsEntity(ID: ID)).First();

                oldEntity.Status = false;
                MergeEntity curEntity = new MergeEntity(PrisonerID: oldEntity.PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(oldEntity),
                    Type: 2,
                    EntityID: ID,
                    EID: (int)MergeEntity.TableIDS.OFFICIAL_VISITS_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);

                needApprove = true;

                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateOfficialVisit(new OfficialVisitsEntity(ID: ID, Status: false));
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Այցելության Հեռացում", RequestData: ID.ToString()));

            return Json(new { status = status, needApprove = needApprove });
        }

        public ActionResult EditData(OfficialVisitsEntity Item)
        {
            bool status = false;
            bool needApprove = false;
            int? PrisonerID = BusinessLayer_PrisonerService.GetOfficialVisits(new OfficialVisitsEntity(ID: Item.ID)).First().PrisonerID;
            PrisonerEntity curPrisoner = BusinessLayer_PrisonerService.GetPrisoner(PrisonerID.Value, false);
            if (ViewBag.PPType == 2 && PrisonerID.HasValue && curPrisoner.State.Value)
            {
                MergeEntity curEntity = new MergeEntity(PrisonerID: PrisonerID.Value,
                    EmployeeID: ViewBag.UserID,
                    Json: JsonConvert.SerializeObject(Item),
                    Type: 3,
                    EntityID: Item.ID,
                    EID: (int)MergeEntity.TableIDS.OFFICIAL_VISITS_MAIN);

                int? tempID = BusinessLayer_MergeLayer.AddModification(curEntity);
                needApprove = true;
                status = tempID != null;
            }
            else
            {
                status = BusinessLayer_PrisonerService.UpdateOfficialVisit(Item);
            }
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Այցելության Խմբագրում", RequestData: JsonConvert.SerializeObject(Item)));

            return Json(new { status = status, needApprove = needApprove });
        }
        [ReadWrite]
        public ActionResult CompleteData(int? id)
        {
            bool status = false;

            if (id != null)
            {
                OfficialVisitsEntity curEntity = new OfficialVisitsEntity(ID: id.Value, EndDate: DateTime.Now);

                status = BusinessLayer_PrisonerService.UpdateOfficialVisit(curEntity);
            }

            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Այցելության հաստատում", RequestData: id.ToString()));


            return Json(new { status = status });
        }
        #region Report
        [HttpPost]
        public ActionResult OfficialVisitReportListData(OfficialVisitReportEntity Item)
        {
            string key = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            Session.Add(key, Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Այցելություններ report data ", RequestData: JsonConvert.SerializeObject(Item)));
            return Json(new { status = true, key = key });
        }
        [HttpGet]
        public ActionResult OfficialVisitReportList(string key)
        {
            OfficialVisitReportEntity Item = (OfficialVisitReportEntity)Session[key];
            SetOfficialVisitReportList(Item);
            BL_Log.getInstance().LogAction(new LogActionEntity(ActionName: "Այցելություններ(get)", RequestData: JsonConvert.SerializeObject(Item)));
            return View("OfficialVisitReportList");
        }


        private void SetOfficialVisitReportList(OfficialVisitReportEntity entity)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;

            List<OfficialVisitReportEntity> ent = BL_ReportLayer.FillOfficialVisitReport_List(entity);
            reportViewer.LocalReport.DisplayName = "Այցելություն";
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\OfficialVisitReport.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("OfficialVisitDS", ent));
            ViewBag.OfficialVisitReportViewer = reportViewer;
        }
        #endregion
    }
}