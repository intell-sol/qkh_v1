﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class TransferViewModel
    {
        public PrisonerEntity Prisoner { get; set; }
        public FilterTransferEntity FilterTransfer { get; set; }
        public FilterTransferEntity.Paging TransferEntityPaging { get; set; }
        public TransferObjectEntity Transfer { get; set; }
        public TransferEntity currentTransfer { get; set; }
        public List<TransferDashboardEntity> ListDashboardTransfer { get; set; }
        public List<PersonEntity> PersonList { get; set; }
        public List<LibsEntity> PurposeLibList { get; set; }
        public List<LibsEntity> TransferLibList { get; set; }
        public List<LibsEntity> HospitalLibList { get; set; }
        public List<LibsEntity> CourtLibList { get; set; }
        public List<LibsEntity> StateLibList { get; set; }
        public List<LibsEntity> EndLibList { get; set; }
        public List<LibsEntity> QnnchakanLibList { get; set; }
        public List<LibsEntity> QKHHospitalLibList { get; set; }
        public List<LibsEntity> BjshkakanTexLibList { get; set; }
        public List<LibsEntity> VaruytTexLibList { get; set; }
        public List<TransferEntity> TransferList { get; set; }
        public List<LibsEntity> EmergencyLibList { get; set; }
        public List<LibsEntity> TransferTypeLibList { get; set; }
        public List<LibsEntity> HogebujaranLibList { get; set; }
        public List<OrgUnitEntity> OrgUnitList { get; set; }
        public int? CurrentOrgUnitID { get; set; }
        public PersonEntity Person { get; set; }

        public bool? ArchiveStatus { get; set; }
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}