﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class CameraCardViewModel
    {
        public CameraCardItemEntity Item { get; set; }
        public List<EmployeeEntity> EmployeerList { get; set; }
        public CameraCardViewModel()
        {
            Item = null;
        }
    }
}