﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class InvalidsViewModel
    {
        public string Title { get; set; }
        public InvalidEntity currentInvalid { get; set; }
        public List<LibsEntity> InvalidsDegree { get; set; }
        public string FileSrc { get; set; }
        public InvalidsViewModel()
        {
        }
    }
}