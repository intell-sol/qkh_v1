﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class _Popup_PrevConviction_ViewModel
    {
        public PreviousConvictionsEntity currentPrevConv { get; set; }
        public List<PreviousConvictionsEntity> PrevConv { get; set; }

        public _Popup_PrevConviction_ViewModel()
        {
            currentPrevConv = null;
            PrevConv = null;
        }
    }
}