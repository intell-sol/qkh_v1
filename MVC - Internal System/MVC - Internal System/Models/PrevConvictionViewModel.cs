﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class PrevConvictionViewModel
    {
        public PreviousConvictionsEntity prevConvictionData { get; set; }
        public List<LibsEntity> SentenceCodes { get; set; }
        public List<LibsEntity> SentencePunishmentType { get; set; }
        public PreviousConvictionsEntity Item { get; set; }
        public PrevConvictionViewModel()
        {
            this.SentenceCodes = null;
            this.prevConvictionData = prevConvictionData;
        }
    }
}