﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    [Serializable]
    public class PrisonersViewModel
    {
        public int? PPType { get; set; }
        public List<PrisonerEntity> ListPrisoners { get; set; }
        public PrisonerEntity Prisoner { get; set; }
        public FilterPrisonerEntity FilterPrisoner { get; set; }
        public List<LibsEntity> Gender { get; set; }
        public List<LibsEntity> Citizenships { get; set; }
        public List<LibsEntity> DocIdentity { get; set; }
        public List<LibsEntity> InvalidsDegree { get; set; }
        public List<LibsEntity> LeaningsType { get; set; }
        public List<LibsEntity> LeaningsSubType { get; set; }
        public List<LibsEntity> SkinColor { get; set; }
        public List<LibsEntity> EyesColor { get; set; }
        public List<LibsEntity> HairColor { get; set; }
        public List<LibsEntity> BloodType { get; set; }
        public List<LibsEntity> Education { get; set; }
        public List<LibsEntity> Profession { get; set; }
        public List<LibsEntity> Preferance { get; set; }
        public List<LibsEntity> Abilities { get; set; }
        public List<LibsEntity> Languages { get; set; }
        public List<LibsEntity> AcademicDegree { get; set; }
        public List<LibsEntity> ScienceDegree { get; set; }
        public List<LibsEntity> OfficialGifts { get; set; }
        public List<LibsEntity> NoMilitaryServe { get; set; }
        public List<LibsEntity> MilitaryAwards { get; set; }
        public List<LibsEntity> MilitarWars { get; set; }
        public List<LibsEntity> MilitaryType { get; set; }
        public List<LibsEntity> MilitaryProfession { get; set; }
        public List<LibsEntity> MilitaryRank { get; set; }
        public List<LibsEntity> MilitaryPosition { get; set; }
        public List<EmployeeEntity> EmployeerList { get; set; }
        public List<LibsEntity> ItemsObjects { get; set; }
        public List<LibsEntity> BanItems { get; set; }
        public List<LibsEntity> BanPerson { get; set; }
        public List<LibsEntity> SentencePerformingPersons { get; set; }
        public List<LibsEntity> SentenceCodes { get; set; }
        public List<LibsEntity> SentencePunishmentType { get; set; }
        public List<LibsEntity> QKHType { get; set; }
        public List<LibsEntity> DetectiveRank { get; set; }
        public List<LibsEntity> VaruytMarmin { get; set; }
        public List<LibsEntity> kalanavorumMarmin { get; set; }
        public List<LibsEntity> SysterBrotherType { get; set; }
        public List<LibsEntity> RelativeType { get; set; }

        public List<PersonEntity> RelatedPersonList { get; set; }
        public List<NationalityEntity> Nationality { get; set; }
        public CaseCloseEntity TerminateEntity { get; set; }
        public List<string> Images { get; set; }
        public bool? ConflictPersons { get; set; }
        public PhysicalDataEntity PhysicalContentItem { get; set; }
        public FingerprintsTattoosScarsItemEntity FingerTatooScarItem { get; set; }
        public List<FingerprintsTattoosScarsItemEntity> FingerTatooScars { get; set; }
        public FamilyRelativeEntity FamiltyRelativeItem { get; set; }
        public SpousesEntity FamilySpouseItem { get; set; }
        public ChildrenEntity FamilyChildrenItem { get; set; }
        public ParentsEntity FamilyParentItem { get; set; }
        public SisterBrotherEntity SysterBrotherParentItem { get; set; }
        public OtherRelativesEntity OtherParentItem { get; set; }
        public EducationsProfessionsEntity EducationItem { get; set; }
        public ArmyEntity ArmyContentItem { get; set; }
        public CameraCardItemEntity CameraCardItem { get; set; }
        public IdentificationDocument IdDocItem { get; set; }
        public PreviousConvictionsEntity PrevConvItem { get; set; }
        public HeightWeightEntity HeightWeightItem { get; set; }
        public InvalidEntity InvalidItem { get; set; }
        public LeaningEntity LeaningItem { get; set; }
        public AdditionalFileEntity FileItem { get; set; }
        public MedicalPrimaryEntity MedicalContentItem { get; set; }
        public MedicalComplaintsEntity MedicalComplaintItem { get; set; }
        public MedicalExternalExaminationEntity MedicalExternalExaminationItem { get; set; }
        public MedicalDiagnosisEntity MedicalDiagnosisItem { get; set; }
        public MedicalResearchEntity MedicalResearchItem { get; set; }
        public MedicalHistoryEntity MedicalHistoryItem { get; set; }
        public MilitaryServiceEntity MilitaryItem { get; set; }
        public ArmyEntity ArmyItem { get; set; }
        public ArrestDataEntity ArrestDataItem { get; set; }
        public SentencingDataEntity SentenceDataItem { get; set; }
        public PrisonAccessProtocolEntity ProtocolItem { get; set; }
        public ItemEntity ItemItems { get; set; }
        public InjunctionItemEntity BanListItems { get; set; }
        public EncouragementsEntity EncouragementItems { get; set; }
        public PenaltiesEntity PenaltyItems { get; set; }
        public EducationalCoursesEntity EducationalCoursesItems { get; set; }
        public WorkLoadsEntity WorkLoadItems { get; set; }
        public OfficialVisitsEntity OfficialVisitItems { get; set; }
        public PersonalVisitsEntity PersonalVisitItems { get; set; }
        public PackagesEntity PackageItems { get; set; }
        public DepartureEntity DepartureItems { get; set; }
        public TransferEntity TransferItems { get; set; }
        public ConflictsEntity ConflictItems { get; set; }
        public PrisonerTypeStatusEntity PrisonerTypeStatusItems { get; set; }
        public PrisonerPunishmentTypeEntity PrisonerPunishmentTypeItems { get; set; }
        public PersonEntity Person { get; set; }
        public List<LibsEntity> SentencingArticles { get; set; }
        public bool? MergeStatus { get; set; }

        public FilterPrisonerEntity.Paging PrisonerEntityPaging { get; set; }

        public PrisonersViewModel() {
            this.Images = new List<string>();
        }
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            //return p.Exists(permission => checkEquality(permission, (int)i));
            if (p == null) return false;
            return p.Exists(permission => permission.ID.Equals((int)i));
        }
        public static bool existGuard(List<PermissionsGurdEntity> p, PermissionsHardCodedIds i)
        {
            if (p == null) return false;
            return p.Exists(permission => permission.PermissionIDs.Equals((int)i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}