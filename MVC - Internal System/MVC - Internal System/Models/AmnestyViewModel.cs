﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class AmnestyViewModel
    {
        public FilterAmnestyEntity FilterAmnesty { get; set; }
        public FilterAmnestyEntity.Paging AmnestyEntityPaging { get; set; }
        public List<AmnestyDashboardEntity> ListDashboardAmnesty { get; set; }
        public List<PrisonerEntity> AmnestiesPersons { get; set; }
        public AmnestyObjectEntity Amnesty { get; set; }
        public AmnestyEntity currentAmnesty { get; set; }
        public List<AmnestyEntity> Amnesties { get; set; }
        public List<LibsEntity> TypeLibList { get; set; }
        public List<LibsEntity> ReciepeLibList { get; set; }
        public List<LibsEntity> SignPersonLibList { get; set; }        
        public List<AmnestyPointsEntity> AmnestyPoints { get; set; }
        public List<LibsEntity> AcceptPersonLibItemList { get; set; }
        public int? CurrentPage { get; set; }
        public int? ViewCount { get; set; }

        public bool? ArchiveStatus { get; set; }
        
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
   

}