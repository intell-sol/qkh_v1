﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class PermissionsViewModel
    {
        public PermissionsViewModel()
        {
            //this.PersonalVisitsPositions = new List<PermissionsGurdEntity>();
        }
        public List<OrgUnitEntity> OrgUnitPositions { get; set; }

        public List<PermissionsGurdEntity> OfficialVisitsPositions { get; set; }

        public List<PermissionsGurdEntity> PersonalVisitsPositions { get; set; }

        public List<PermissionsGurdEntity> PackagesPositions { get; set; }

        public List<PermissionsGurdEntity> DeparturesPositions { get; set; }

        public List<PermissionsGurdEntity> TransferPositions { get; set; }
        public List<EmployeeEntity> Employees { get; set; }


        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}