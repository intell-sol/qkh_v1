﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class ViewLogsViewModel
    {
        public FilterLogAcionEntity FilterLogAction { get; set; }

        public FilterEmployeeEntity FilterEmployee { get; set; }

        //public FilterPositionEntity FilterPosition { get; set; }

        public List<EmployeeEntity> OrgUnitEmployees { get; set; }

        public List<OrgUnitEntity> OrgUnitPositions { get; set; }

        public List<LogActionEntity> LogActionList { get; set; }

        public FilterLogAcionEntity.Paging LogActionEntityPaging { get; set; }

        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}