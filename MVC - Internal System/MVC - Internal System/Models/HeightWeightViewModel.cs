﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class HeightWeightViewModel
    {
        public List<HeightWeightEntity> List { get; set; }
        public int? PrisonerID { get; set; }
        public int? PhysicalDataID { get; set; }
        public HeightWeightEntity Item { get; set; }
        public HeightWeightViewModel() {
            List = new List<HeightWeightEntity>();
        }
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}