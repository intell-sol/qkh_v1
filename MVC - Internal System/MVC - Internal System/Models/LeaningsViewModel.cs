﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class LeaningsViewModel
    {
        public string Title { get; set; }
        public LeaningEntity currentLeaning { get; set; }
        public List<LibsEntity> LeaningsType { get; set; }
        public LeaningsViewModel()
        {
        }
    }
}