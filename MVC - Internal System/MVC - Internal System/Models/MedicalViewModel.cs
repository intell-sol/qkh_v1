﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class MedicalViewModel
    {
        public string ActionName { get; set; }
        public FilterWorkLoadsEntity FilterWorkLoads { get; set; }
        public FilterWorkLoadsEntity.Paging WorkLoadsEntityPaging { get; set; }
        public MedicalObjectEntity Medical { get; set; }
        public PrisonerEntity Prisoner { get; set; }
        public MedicalHistoryObjectEntity MedicalHistory { get; set; }
        public MedicalComplaintsEntity currentMedicalComplaint { get; set; }
        public MedicalHistoryEntity currentMedicalHistory { get; set; }
        public MedicalExternalExaminationEntity currentMedicalExternalExamination { get; set; }
        public MedicalDiagnosisEntity currentMedicalDiagnosis { get; set; }
        public MedicalResearchEntity currentMedicalResearch { get; set; }
        public MedicalHealingEntity currentMedicalHealing { get; set; }
        public WorkLoadsEntity currentWorkLoad { get; set; }
        public List<LibsEntity> HealthLibItemList { get; set; }
        public List<LibsEntity> ComplaintsLibItemList { get; set; }
        public List<LibsEntity> ResearchLibItemList { get; set; }
        public List<LibsEntity> ExternalExaminationLibItemList { get; set; }
        public List<LibsEntity> DiagnosisLibItemList { get; set; }
        public List<LibsEntity> HealingNameLibItemList { get; set; }
        public List<LibsEntity> ResultLibItemList { get; set; }
        public MedicalPrimaryEntity MedicalContentItem { get; set; }
        public int? PrisonerID { get; set; }
        public int? HistoryType { get; set; }

        public bool? ArchiveStatus { get; set; }
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}