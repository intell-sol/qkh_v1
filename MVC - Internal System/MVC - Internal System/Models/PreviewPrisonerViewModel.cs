﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class PreviewPrisonerViewModel
    {
        public int PrisonerID { get; set; }
        public bool? canApprove { get; set; }
    }
}