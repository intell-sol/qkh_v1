﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class _Popup_Files_ViewModel
    {
        public AdditionalFileEntity currentFile { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public bool isFingerTatoo { get; set; }
        public int? PrisonerID { get; set; }
        public string Name  { get; set; }
        public FingerprintsTattoosScarsItemEntity curFingerTattoScarItem { get; set; }
        public _Popup_Files_ViewModel() {
            currentFile = null;
            Files = null;
            isFingerTatoo = false;
        }
    }
}