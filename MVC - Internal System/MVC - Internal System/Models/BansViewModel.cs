﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class BansViewModel
    {
        public BansViewModel()
        {
            Item = null;
        }
        public bool? State { get; set; }
        public InjunctionItemEntity Item { get; set; }
        public List<LibsEntity> BanItems { get; set; }
        public List<LibsEntity> BanPerson { get; set; }
       
    }
}