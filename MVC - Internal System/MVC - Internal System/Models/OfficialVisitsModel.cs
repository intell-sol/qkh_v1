﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class OfficialVisitsViewModel
    {
        public PrisonerEntity Prisoner { get; set; }
        public FilterOfficialVisitsEntity FilterOfficialVisits { get; set; }
        public FilterOfficialVisitsEntity.Paging OfficialVisitsEntityPaging { get; set; }
        public OfficialVisitsObjectEntity OfficialVisit { get; set; }
        public OfficialVisitsEntity currentOfficialVisit { get; set; }
        public List<OfficialVisitsDashboardEntity> ListDashboardOfficialVisits { get; set; }
        public List<PersonEntity> PersonList { get; set; }

        public List<LibsEntity> TypeLibList { get; set; }
        public List<LibsEntity> PositionLibList { get; set; }
        public List<LibsEntity> Gender { get; set; }
        public List<NationalityEntity> Nationality { get; set; }
        public List<LibsEntity> Citizenships { get; set; }
        public PersonEntity Person { get; set; }
        public List<LibsEntity> DocIdentity { get; set; }

        public bool? ArchiveStatus { get; set; }
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}