﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class EducationalCoursesViewModel
    {
        public FilterEducationalCoursesEntity FilterEducationalCourses { get; set; }
        public FilterEducationalCoursesEntity.Paging EducationalCoursesEntityPaging { get; set; }
        public EducationalCoursesObjectEntity EducationalCourse { get; set; }
        public EducationalCoursesEntity currentEducationalCourse { get; set; }
        public List<EducationalCoursesDashboardEntity> ListDashboardEducationalCourses { get; set; }
        public List<LibsEntity> TypeLibList { get; set; }
        public List<LibsEntity> EngagementBasisLibItemList { get; set; }
        public PrisonerEntity Prisoner { get; set; }
        public List<LibsEntity> ReleaseBasisLibItemList { get; set; }
        public bool? ArchiveStatus { get; set; }
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}