﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    [Serializable]
    public class CustomReportViewModel
    {
        public CustomReportViewModel()
        {
            Prisoner =  new PrisonersViewModel();
            currentReport = new CustomReportEntity();
            Filter = new CustomReportEntity();
        }
        public CustomReportEntity currentReport { get; set; }
        public CustomReportEntity Filter { get; set; }
        public bool ID { get; set; }
        public bool PersonalMatterID { get; set; }
        public bool NameMidNameSurName { get; set; }
        public bool PrisonerType { get; set; }
        public bool Birthday { get; set; }
        public bool Sex { get; set; }
        public bool Nationality { get; set; }
        public bool Citizenship { get; set; }
        public bool DetentionStartDate { get; set; }
        public bool SentencingStartDate { get; set; }
        public bool OrgUnitAcceptDate { get; set; }
        public bool Penalty { get; set; }
        public bool Encouragment { get; set; }
        public bool ForbiddenItemCount { get; set; }
        public bool ColseCaseBasis { get; set; }
        public bool TransferCount { get; set; }
        public bool StorageRegimeType { get; set; }
        public bool PunishmentWeight { get; set; }
        public bool CurrentPunishmentArticle { get; set; }
        public bool InconsistentPersons { get; set; }
        public bool CurrentCameraCard { get; set; }
        public bool FamiliyRelatives { get; set; }
        public bool EducationProfession { get; set; }
        public bool Workloads { get; set; }
        public bool PrevConvictions { get; set; }
        public bool Data_Physical { get; set; }
        public PrisonersViewModel Prisoner { get; set;}

        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
      
    }
   

}