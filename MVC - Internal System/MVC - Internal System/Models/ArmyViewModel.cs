﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class ArmyViewModel
    {
        public string Title { get; set; }
        public ArmyEntity armyEntity { get; set; }
        public List<LibsEntity> MilitaryType { get; set; }
        public List<LibsEntity> MilitaryPosition { get; set; }
        public List<LibsEntity> ItemsObjectLibs { get; set; }
        public List<LibsEntity> MilitaryProfession { get; set; }
        public List<LibsEntity> MilitaryRank { get; set; }
        public ArmyViewModel()
        {
            Title = null;
            armyEntity = null;
            MilitaryType = null;
            MilitaryPosition = null;
            MilitaryProfession = null;
            MilitaryRank = null;
        }
    }
}