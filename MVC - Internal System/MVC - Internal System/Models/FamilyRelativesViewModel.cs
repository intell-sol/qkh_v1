﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class FamilyRelativesViewModel
    {
        public string Title { get; set; }
        public PersonEntity Person { get; set; }
        public SpousesEntity spouseEntity { get; set; }
        public ChildrenEntity childrenEntity { get; set; }
        public ParentsEntity parentEntity { get; set; }
        public List<NationalityEntity> Nationality { get; set; }
        public List<LibsEntity> Citizenships { get; set; }
        public List<LibsEntity> DocIdentity { get; set; }
        public List<LibsEntity> Gender { get; set; }
        public SisterBrotherEntity systerEntity { get; set; }
        public OtherRelativesEntity otherRelativesEntity { get; set; }
        public List<LibsEntity> ItemsObjectLibs { get; set; }
        public List<PersonEntity> RelatedPersonList { get; set; }
        public List<LibsEntity> SysterBrotherType { get; set; }
        public List<LibsEntity> RelativeType { get; set; }
        public string currentTypeName { get; set; }
        public FamilyRelativesViewModel()
        {
            Title = null;
            Person = null;
            spouseEntity = null;
            childrenEntity = null;
            parentEntity = null;
            systerEntity = null;
            otherRelativesEntity = null;
            ItemsObjectLibs = null;
            RelatedPersonList = null;
            SysterBrotherType = null;
            RelativeType = null;
        }
    }
}