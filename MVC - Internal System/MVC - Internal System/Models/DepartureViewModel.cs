﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class DepartureViewModel
    {
        public FilterDepartureEntity FilterDeparture { get; set; }
        public FilterDepartureEntity.Paging DepartureEntityPaging { get; set; }
        public DepartureObjectEntity Departure { get; set; }
        public DepartureEntity currentDeparture { get; set; }
        public List<DepartureDashboardEntity> ListDashboardDeparture { get; set; }
        public List<PersonEntity> PersonList { get; set; }
        public PrisonerEntity Prisoner { get; set; }
        public List<LibsEntity> TypeLibList { get; set; }
        public List<LibsEntity> PurposeLibList { get; set; }
        public List<LibsEntity> MedLibList { get; set; }
        public List<LibsEntity> CaseLibList { get; set; }
        public List<LibsEntity> MoveLibList { get; set; }
        public List<LibsEntity> HospitalLibList { get; set; }
        public List<LibsEntity> CourtLibList { get; set; }
        public List<OrgUnitEntity> OrgUnitList { get; set; }
        public List<LibsEntity> InvestigateLibList { get; set; }

        public bool? ArchiveStatus { get; set; }
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}