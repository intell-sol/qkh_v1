﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class PackagesViewModel
    {
        public PrisonerEntity Prisoner { get; set; }
        public FilterPackagesEntity FilterPackages { get; set; }
        public FilterPackagesEntity.Paging PackagesEntityPaging { get; set; }
        public PackagesObjectEntity Package { get; set; }
        public PackagesEntity currentPackage { get; set; }
        public List<PackagesDashboardEntity> ListDashboardPackages { get; set; }
        public List<PersonEntity> PersonList { get; set; }
        public List<LibsEntity> TypeLibList { get; set; }
        public List<LibsEntity> TypeContentLibList { get; set; }
        public List<LibsEntity> GoodsLibList { get; set; }
        public List<LibsEntity> MeasureLibList { get; set; }
        public List<EmployeeEntity> EmployeeList { get; set; }
        public List<PackageContentEntity> PackageList { get; set; }
        public List<LibsEntity> Gender { get; set; }
        public List<NationalityEntity> Nationality { get; set; }
        public List<LibsEntity> Citizenships { get; set; }
        public PersonEntity Person { get; set; }
        public List<LibsEntity> DocIdentity { get; set; }
        public bool? ArchiveStatus { get; set; }
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}