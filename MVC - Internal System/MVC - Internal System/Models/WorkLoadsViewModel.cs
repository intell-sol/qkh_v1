﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class WorkLoadsViewModel
    {
        public PrisonerEntity Prisoner { get; set; }
        public FilterWorkLoadsEntity FilterWorkLoads { get; set; }
        public FilterWorkLoadsEntity.Paging WorkLoadsEntityPaging { get; set; }
        public WorkLoadsObjectEntity WorkLoad { get; set; }
        public WorkLoadsEntity currentWorkLoad { get; set; }
        public List<WorkLoadsDashboardEntity> ListDashboardWorkLoads { get; set; }

        public List<LibsEntity> JobTypeLibItemList { get; set; }
        public List<LibsEntity> WorkTitleLibItemList { get; set; }
        public List<LibsEntity> EmployerLibItemList { get; set; }
        public List<LibsEntity> ReleaseBasisLibItemList { get; set; }
        public List<LibsEntity> EngagementBasisLibItemList { get; set; }

        public bool? ArchiveStatus { get; set; }
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}