﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class ItemsViewModel
    {
        public ItemEntity Item { get; set; }
        public List<LibsEntity> ItemsObjectLibs { get; set; }
        public ItemsViewModel()
        {
            Item = null;
        }
    }
}