﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class EncouragementsViewModel
    {
        public FilterEncouragementsEntity FilterEncouragements { get; set; }
        public FilterEncouragementsEntity.Paging EncouragementsEntityPaging { get; set; }
        public EncouragementsObjectEntity Encouragement { get; set; }
        public EncouragementsEntity currentEncouragement { get; set; }
        public List<EncouragementsDashboardEntity> ListDashboardEncouragements { get; set; }
        public List<LibsEntity> TypeLibList { get; set; }
        public List<LibsEntity> BaseLibList { get; set; }
        public List<EmployeeEntity> EmployeeList { get; set; }
        public bool? ArchiveStatus { get; set; }
        public List<LibsEntity> TypeLibListPrisoners { get; set; }
        public PrisonerEntity Prisoner { get; set; }
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}