﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class SentencingArticlesViewModel
    {
        public PreviousConvictionsEntity prevData { get; set; }
        public List<LibsEntity> SentenceCodesArticles { get; set; }
        public SentencingDataEntity sentenceData { get; set; }
        public ArrestDataEntity arrestData { get; set; }
        public List<SentencingDataArticleLibsEntity> selectedSentencingDataArticles { get; set; }
        public SentencingArticlesViewModel()
        {

        }
    }
}