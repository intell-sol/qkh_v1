﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class EnteranceViewModel
    {
        public string Title { get; set; }
        public ArrestDataEntity arrestDataEntity { get; set; }
        public PrisonAccessProtocolEntity sentenceProtocol { get; set; }
        public List<LibsEntity> SentencingArticles { get; set; }

        public SentencingDataEntity sentenceData { get; set; }
        public PrisonerTypeStatusEntity prisonerTypeStatusData { get; set; }
        public PrisonerPunishmentTypeEntity prisonerPunishmentTypeData { get; set; }
        public List<LibsEntity> VaruytMarmin { get; set; }
        public List<LibsEntity> kalanavorumMarmin { get; set; }
        public List<LibsEntity> DetectiveRank { get; set; }
        public List<LibsEntity> QKHType { get; set; }
        public List<EmployeeEntity> EmployeerList { get; set; }
        public List<LibsEntity> SentencePerformingPersons { get; set; }
        public List<LibsEntity> SentenceCodes { get; set; }
        public List<LibsEntity> SentencePunishmentType { get; set; }
        public List<LibsEntity> PrisonerPunishmentTypeLibList { get; set; }
        public List<LibsEntity> PrisonerPunishmentBaseLibList { get; set; }
        public List<PropValuesEntity> SentenceArticleTypeList { get; set; }
        public List<PrisonerTypeStatusEntity> PrisonerTypeStatusList { get; set; }
        public List<PrisonerPunishmentTypeEntity> PrisonerPunishmentTypeList { get; set; }
        public List<LibsEntity> propertyConfiscationLibs { get; set; }
        public EnteranceViewModel()
        {
            SentencingArticles = null;
            Title = null;
            SentencePerformingPersons = null;
            kalanavorumMarmin = null;
            VaruytMarmin = null;
            arrestDataEntity = null;
            DetectiveRank = null;
            QKHType = null;
            EmployeerList = null;
            SentenceArticleTypeList = null;
            SentenceCodes = null;
            SentencePunishmentType = null;
            sentenceData = null;
        }
    }
}