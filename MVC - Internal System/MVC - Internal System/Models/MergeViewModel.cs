﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class MergeViewModel
    {
        public int PrisonerID { get; set; }
        public PersonEntity Person { get; set; }

        public FilterMergeEntity FilterMerge { get; set; }
        public FilterMergeEntity.Paging MergeEntityPaging { get; set; }
        public MergeObjectEntity Merge { get; set; }
        public MergeEntity currentMerge { get; set; }
        public List<MergeDashboardEntity> ListDashboardMerge { get; set; }

        public List<LibsEntity> JobTypeLibItemList { get; set; }
        public List<LibsEntity> WorkTitleLibItemList { get; set; }
        public List<LibsEntity> EmployerLibItemList { get; set; }
        public List<LibsEntity> ReleaseBasisLibItemList { get; set; }
        public List<LibsEntity> EngagementBasisLibItemList { get; set; }

        public bool? ArchiveStatus { get; set; }
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}