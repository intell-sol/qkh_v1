﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class TerminationViewModel
    {
        public string Title { get; set; }
        public List<CaseCloseEntity> TerminateEntity { get; set; }
        public CaseCloseEntity currentTerminateEntity { get; set; }
        public CaseOpenEntity openTerminationEntity { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public List<LibsEntity> caseLibs { get; set; }
        public List<LibsEntity> fromWhoomLib { get; set; }
        public PrisonerEntity Prisoner { get; set; }
        public TerminationViewModel()
        {
            Title = null;
            TerminateEntity = null;
            Files = null;
            caseLibs = null;
            currentTerminateEntity = null;
            openTerminationEntity = null;
        }
    }
}