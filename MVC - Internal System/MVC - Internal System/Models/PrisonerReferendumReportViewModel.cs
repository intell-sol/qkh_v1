﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class PrisonerReferendumReportViewModel
    {
        public PrisonersReferendumReportEntity currentReport { get; set; }
        public PrisonersReferendumReportEntity Filter { get; set; }

        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}