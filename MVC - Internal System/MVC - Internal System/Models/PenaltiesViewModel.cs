﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class PenaltiesViewModel
    {
        public PrisonerEntity Prisoner { get; set; }
        public FilterPenaltiesEntity FilterPenalties { get; set; }
        public FilterPenaltiesEntity.Paging PenaltiesEntityPaging { get; set; }
        public PenaltiesObjectEntity Penalty { get; set; }
        public PenaltiesEntity currentPenalty { get; set; }
        public List<PenaltiesDashboardEntity> ListDashboardPenalties { get; set; }
        public List<LibsEntity> TypeLibList { get; set; }
        public List<LibsEntity> ViolationLibList { get; set; }
        public List<LibsEntity> StateLibList { get; set; }
        public List<LibsEntity> StateBaseLibList { get; set; }
        public List<LibsEntity> StatusChangeBasisLibList { get; set; }
        public List<LibsEntity> AppealsContentLibItemList { get; set; }
        public List<LibsEntity> AppealsBodyList { get; set; }
        public List<LibsEntity> AppealResultLibItemID { get; set; }
        public List<EmployeeEntity> EmployeeList { get; set; }
        public bool? ArchiveStatus { get; set; }
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}