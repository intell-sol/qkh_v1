﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class _Popup_DocIdentity_ViewModel
    {
        public List<LibsEntity> Citizenship { get; set; }

        public List<LibsEntity> DocIdentity { get; set; }
        public int? PPtype { get; set; }
        public List<IdentificationDocument> idDocuments { get; set; }
        public IdentificationDocument Item { get; set; }
    }
}