﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class MergeApproveViewModel
    {
        public MergeEntity CurrentMerge { get; set; }

        public int Unit { get; set; }
        
        public int SubUnit { get; set; }

        public int Table { get; set; }

        public CameraCardViewModel Old_CameraCardViewModel { get; set; }
        public CameraCardViewModel New_CameraCardViewModel { get; set; }
        public BansViewModel Old_BansViewModel { get; set; }
        public BansViewModel New_BansViewModel { get; set; }
        public _Popup_DocIdentity_ViewModel Old_DocIdentityViewModel { get; set; }
        public _Popup_DocIdentity_ViewModel New_DocIdentityViewModel { get; set; }
        public PrevConvictionViewModel Old_PrevConvViewModel { get; set; }
        public PrevConvictionViewModel New_PrevConvViewModel { get; set; }
        public _Popup_Files_ViewModel Old_FilesViewModel { get; set; }
        public _Popup_Files_ViewModel New_FilesViewModel { get; set; }
        public HeightWeightViewModel Old_HeightWeightViewModel { get; set; }
        public HeightWeightViewModel New_HeightWeightViewModel { get; set; }
        public PrisonersViewModel Old_PrisonerViewModel { get; set; }
        public PrisonersViewModel New_PrisonerViewModel { get; set; }
        public InvalidsViewModel Old_InvalidsViewModel { get; set; }
        public InvalidsViewModel New_InvalidsViewModel { get; set; }
        public LeaningsViewModel Old_LeaningsViewModel { get; set; }
        public LeaningsViewModel New_LeaningsViewModel { get; set; }
        public MedicalViewModel Old_MedicalViewModel { get; set; }
        public MedicalViewModel New_MedicalViewModel { get; set; }
        public FamilyRelativesViewModel Old_FamilyRelativesViewModel { get; set; }
        public FamilyRelativesViewModel New_FamilyRelativesViewModel { get; set; }
    }
}