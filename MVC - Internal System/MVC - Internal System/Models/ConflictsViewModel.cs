﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class ConflictsViewModel
    {
        public ConflictsObjectEntity Conflict { get; set; }
        public ConflictsEntity currentConflict { get; set; }
        public List<LibsEntity> Gender { get; set; }
        public List<NationalityEntity> Nationality { get; set; }
        public List<LibsEntity> Citizenships { get; set; }
        public PersonEntity Person { get; set; }
        public PrisonerEntity Prisoner { get; set; }
        public List<LibsEntity> DocIdentity { get; set; }
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
    }
}