﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class ArchiveViewModel
    {
        public string Title { get; set; }
        public List<ArchiveEntity> archives { get; set; }
        public string controllerName { get; set; }
        public List<EmployeeEntity> employees { get; set; }
        public ArchiveViewModel()
        {
            Title = null;
            archives = new List<ArchiveEntity>();
            employees = new List<EmployeeEntity>();
            controllerName = "Convicts";
        }
    }
}