﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC___Internal_System.Models
{
    public class OpenTypeReportViewModel
    {
        public OpenTypeReportEntity currentReport { get; set; }
        public OpenTypeReportEntity Filter { get; set; }     
        public List<LibsEntity> QKHType { get; set; }
        public PrisonAccessProtocolEntity sentenceProtocol { get; set; }
        public OpenTypeReportViewModel()
        {
            QKHType = null;
        }
        public static bool exists(List<PermEntity> p, PermissionsHardCodedIds i)
        {
            return p.Exists(permission => checkEquality(permission, i));
        }
        public static bool checkEquality(PermEntity p, PermissionsHardCodedIds i)
        {
            bool r = p.ID.Equals((int)i);
            return r;
        }
      
    }
   

}