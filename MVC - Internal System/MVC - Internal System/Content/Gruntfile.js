module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    var userConfig = {
        buildDir: "js",
        srcDir: "src"
    };

    var taskConfig = {

        copy: {
            main: {
                files: [
                    { expand: true, cwd: 'src/js/', src: ['_mvc.core.js'], dest: 'js/' },
                    { expand: true, cwd: 'src/js/', src: ['_mvc.boot.js'], dest: 'js/' }
                    //{ expand: true, cwd: 'src/js/mvc/', src: ['**'], dest: 'js/mvc/' }
                ]
            }
        },

        concat: {
            options: {
                separator: '\n\n'
            },
            dist: {
                src: ['src/js/mvc/**/*.js'],
                dest: 'js/_mvc.main.js'
            }
        },

        uglify: {
            my_target: {
                files: {
                    'js/_mvc.main.min.js': ['js/_mvc.main.js']
                }
            }
        },

        diff: {
            scripts: {
                files: ['src/js/mvc/**/*.js', 'src/js/*.js'],
                tasks: ['copy', 'concat'],
                options: {
                    reload: true
                }
            }
        }
    };

    grunt.initConfig(grunt.util._.extend(taskConfig, userConfig));

    grunt.renameTask('watch', 'diff');

    grunt.registerTask('default', ['copy', 'concat', 'diff']);
    grunt.registerTask('watch', ['copy', 'concat', 'diff']);
};
