$(function () {

	$(".selectize").each(function () {
		$(this).selectize({
			create: false
		});
	});

	$(".selectize-tags").each(function () {
		$(this).selectize({
			create: true
		});
	});

	$(".scrollTable").each(function () {
		$(this).tableScroll({
			flush: false,
			height: 136
		});
	});

	$(".modal").on("show.bs.modal", function (e) {
	    $("html").addClass("noScroll");
	});

	$(".modal").on("hide.bs.modal", function (e) {
	    $("html").removeClass("noScroll");
	    $(".daterangepicker").css("display", "none");
	});

	var headerNames = $(".headerNames");
	var headerNamesDropWrap = $(".headerNamesDropRight");
	var headerNamesDrop = $("#headerNamesDrop");

	headerNamesDrop.click(function (event) {
		headerNamesDropWrap.toggleClass("open");
	});

	var openClass = $(".open");

	$("body").on("click", function (e) {
		if (!headerNames.is(e.target) && headerNames.has(e.target).length === 0 && openClass.has(e.target).length === 0) {
			headerNamesDropWrap.removeClass("open");
		}
	});

	var formsWrapTabsLeftHeight = $(".formsWrapTabsLeft").outerHeight();
	if ($(".formsWrapTabsRight").length) {
		$(".formsWrapTabsRight").css("min-height", formsWrapTabsLeftHeight);
	}


});