/**** BOOT PART ****/

$(function () {
    // getting controller and action

    var currentControllerName = getControllerNameFromUrl();
    var currentActionName = getActionNameFromUrl();
    var currentId = getIdFromUrl();
    var selectOrgUnitURL = '/Base/SelectOrgUnit';
    // load selectize no delete
    loadSelectizePlugins();

    // for Convicts and Prisoners
    PrisonerType = {};
    PrisonerType['Convicts'] = 2;
    PrisonerType['Prisoners'] = 3;


    // methods
    function getControllerNameFromUrl() {
        var manJSCmon = location.pathname.split("/");
        //var local = location.protocol + '//' + location.host + "/" + manJSCmon[1] + "/" + manJSCmon[2];
        return manJSCmon[1] || null;
    }
    function getActionNameFromUrl() {
        var manJSCmon = location.pathname.split("/");
        //var local = location.protocol + '//' + location.host + "/" + manJSCmon[1] + "/" + manJSCmon[2];
        return manJSCmon[2] || null;
    }
    function getIdFromUrl() {
        var manJSCmon = location.pathname.split("/");
        //var local = location.protocol + '//' + location.host + "/" + manJSCmon[1] + "/" + manJSCmon[2];
        return manJSCmon[3] || null;
    }

    if (currentControllerName != null) {
        // default action name
        if (currentActionName == null) currentActionName = 'Index';

        //getting controller
        var currentControllerObject = _core.getController((currentControllerName == 'Prisoners') ? 'Convicts' : currentControllerName);
        if (currentControllerObject != null) {
            var currentController = currentControllerObject.Controller;
            if (currentActionName == 'Index' || currentActionName == 'index') currentController.Index(PrisonerType[currentControllerName]); // calling Index
            else if (currentActionName == 'Add' || currentActionName == 'add') currentController.Add(PrisonerType[currentControllerName]); // calling add
            else if (currentActionName == 'Edit' || currentActionName == 'edit') currentController.Edit(PrisonerType[currentControllerName], currentId); // calling edit
            else if (currentActionName == 'Preview' || currentActionName == 'preview') currentController.View(PrisonerType[currentControllerName], currentId); // calling view
            else if (currentActionName == "Prisoner") currentController.Prisoner(currentId);

            // change tab
            changeTab(currentControllerName);
            bindOrgUnitEvents();
        }
        else {
            console.warn('no controller registered by name ', currentControllerName);
        }
    }
    else {
        console.warn('could not get controller or action name from pathname');
        $(document).keypress(function (e) {
            if (e.which == 13) {
                $('#loginBtn').click();
            }
        });
    }

    function changeTab(controllerName) {
        var menuTab = $('.menutab');

        menuTab.removeClass('selected');

        menuTab.each(function () {
            console.log($(this).attr('name'));
            if ($(this).attr('name') == controllerName) {
                $(this).addClass('selected');
            }
        })
    }

    function loadSelectizePlugins() {

        Selectize.define('no-delete', function (options) {
            this.deleteSelection = function () { };
        });
    }


    // bind org unit select events
    function bindOrgUnitEvents() {
        var orgUnitSelectBtn = $('#header-org-select');

        orgUnitSelectBtn.unbind();
        orgUnitSelectBtn.change(function (e) {
            e.preventDefault();

            var id = $(this).val();

            var postService = _core.getService('Post').Service;

            postService.postJson({ 'OrgUnitID': id }, selectOrgUnitURL, function (res) {
                if (res != null && res.status) {
                    location.href = res.url;
                }
            });
        });

        // selectize org unit
        $(".selectizeOrgUnit").each(function () {
            $(this).selectize({
                create: false
            });
        });


    }
});