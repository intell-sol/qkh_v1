﻿// FingerTatooScar widget

/*****Main Function******/
var FingerTatooScarWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.PrisonerID = null;
    _self.type = null;
    _self.id = null;
    _self.mode = null;
    _self.modalFingerTattoScarsUrl = "/_Popup_/Get_AddFingerTattoScar";
    _self.finalFormData = new FormData();
    _self.remFingerTatooScarsURL = '/_Data_Finger_Tattoo_/remFingerTatooScar';
    _self.addFingerTatooScarsURL = '/_Data_Finger_Tattoo_/addFingerTatooScar';
    _self.editFingerTatooScarsURL = '/_Data_Finger_Tattoo_/editFingerTatooScar';
    _self.viewUrl = "/_Popup_Views_/Get_AddFingerTattoScar";
    // action list
    _self.actionList = {
    };

    _self.init = function () {
        console.log('File Widget Inited');

        _self.finalFormData = new FormData();
        _self.callback = null;
        _self.content = null;
        _self.mode = null;
        _self.id = null;
        _self.PrisonerID = null;
        _self.type = null;
    };

    // ask action
    _self.Add = function (data, callback) {

        // initilize
        _self.init();

        console.log('File Add called');

        // save callback
        _self.callback = callback;

        _self.mode = 'Add';

        _self.type = data.TypeID;

        _self.PrisonerID = data.PrisonerID;

        // loading view
        loadView(_self.modalFingerTattoScarsUrl);
    };
    _self.Edit = function (data, callback)
    {

        // initilize
        _self.init();

        console.log('File Edit called');

        // save callback
        _self.callback = callback;

        _self.mode = 'Edit';

        _self.type = data.TypeID;

        _self.id = data.ID;

        // loading view
        loadView(_self.modalFingerTattoScarsUrl);
    };
    // ask action
    _self.View = function (data, callback) {

        // initilize
        _self.init();

        console.log('File Edit called');

        // save callback
        _self.callback = callback;

        _self.mode = 'View';

        _self.type = data.TypeID;

        _self.id = data.ID;

        // loading view
        loadView(_self.viewUrl);
    };

    // remove file Action
    _self.Remove = function (data, callback) {

        // initilize
        _self.init();

        _self.type = data.TypeID;

        _core.getService('Loading').Service.enableBlur('loaderClass');

        // yes or no widget
        _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

            if (res) {

                var url = _self.remFingerTatooScarsURL;

                _core.getService('Post').Service.postJson(data, url, function (res) {
                    if (res != null) {
                        callback(res);
                    }
                });
            }
        });
    }

    // loading modal from Views
    function loadView(url) {
        var data = {};

        if (_self.id != null && (_self.mode == "Edit" || _self.mode == "View")) {
            data.ID = _self.id;
        }

        data.TypeID = _self.type;

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _self.content = res;
            
            _core.getService('Loading').Service.disableBlur('loaderClass');

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();
            // showing modal
            $('.add-file-modal').modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var addFileToggle = $('#add-file-toggle');
        var acceptBtn = $('#acceptBtn');
        var checkItems = $('.checkValidateAddFile');
        var fileName = $('#filename');
        _self.getItems = $('.getItemsFile');

        // unbind
        addFileToggle.unbind();
        acceptBtn.unbind();

        // bind file change
        addFileToggle.bind('change', function () {
            _self.finalFormData.delete('fileImage');
            _self.finalFormData.append('fileImage', $(this)[0].files[0], $(this).val());

            // change filename fields and trigger change
            fileName.val($(this).val());
            fileName.html($(this).val());
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            // adding file
            if (_self.id == null) {

                // default add file
                var url = _self.addFingerTatooScarsURL;

                // append prisonerid and type
                data.append('PrisonerID', _self.PrisonerID);
                data.append('TypeID', _self.type);
            }
            else {

                // default edit file
                url = _self.editFingerTatooScarsURL;

                // append id
                data.append('ID', _self.id);
            }

            //// log it
            //for (var pair of data.entries()) {
            //    console.log(pair[0] + ', ' + pair[1]);
            //}

            // post service

            var curName = '';
            if (parseInt(_self.type) == 18) curName = 'fingerLoader';
            else if (parseInt(_self.type) == 20) curName = 'tatooLoader';
            else if (parseInt(_self.type) == 22) curName = 'scarLoader';

            _core.getService('Loading').Service.enableBlur(curName);

            var postService = _core.getService('Post').Service;
          
            postService.postFormData(data, url, function (res) {

                _core.getService('Loading').Service.enableBlur(curName);

                if (res != null) {
                    _self.callback(res);

                }
                else alert('serious eror on adding file');
              
            })

            //hiding modal
            $('.add-file-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // selectize
        $(".selectizeAddFile").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // collect data
    function collectData() {

        // collect data
        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            _self.finalFormData.delete(name);
            _self.finalFormData.append(name, $(this).val());

        });

        return _self.finalFormData;
    }

}
/************************/


// creating class instance
var FingerTatooScarWidget = new FingerTatooScarWidgetClass();

// creating object
var FingerTatooScarWidgetObject = {
    // important
    Name: 'FingerTatooScar',

    Widget: FingerTatooScarWidget
}

// registering widget object
_core.addWidget(FingerTatooScarWidgetObject);