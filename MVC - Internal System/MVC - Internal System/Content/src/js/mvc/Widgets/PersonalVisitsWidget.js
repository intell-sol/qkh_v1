﻿// Add PersonalVisits widget

/*****Main Function******/
var PersonalVisitsWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.PrisonerID = null;
    _self.id = null;
    _self.mode = null;
    _self.PersonList = [];
    _self.currentPersonalID = '';
    _self.enddate = null;
    _self.currentPerson = null;

    _self.drawPersonListURL = "/_Popup_/Draw_PersonalVisitPersonTableRow";
    _self.modalUrl = "/_Popup_/Get_PersonalVisit";
    _self.viewUrl = "/_Popup_Views_/Get_PersonalVisit";
    _self.addUrl = '/PersonalVisits/AddData';
    _self.editUrl = '/PersonalVisits/EditData';
    _self.removeUrl = '/PersonalVisits/RemData';
    _self.completeUrl = '/PersonalVisits/CompleteData';
    _self.declineUrl = '/PersonalVisits/DeclineData';
    _self.approveUrl = '/PersonalVisits/ApproveData';
    _self.ModalName = 'private-visit-add-modal';
    _self.finalData = {};
    
    _self.init = function () {
        console.log('Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.PrisonerID = null;
        _self.id = null;
        _self.mode = null;
        _self.enddate = null;
        _self.PersonList = [];
        _self.currentPersonalID = '';
    };

    // add action
    _self.Add = function (PrisonerID, callback) {

        // initilize
        _self.init();

        console.log('Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = 'Add';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    }
    _self.View = function (id, callback)
    {
        // initilize
        _self.init();

        console.log('View called');

        _self.id = id;

        _self.mode = 'View';

        // save callback
        _self.callback = callback;

        // loading view
         loadView(_self.viewUrl);
    };
    // edit action
    _self.Edit = function (id, callback) {
        // initilize
        _self.init();

        console.log('Edit called');

        _self.id = id;

        _self.mode = 'Edit';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // remove action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        removeData();
    };

    // complete action
    _self.Complete = function (id, data, callback) {
        // initilize
        _self.init();
       
        console.log('Remove called');

        _self.id = id;
        _self.enddate = data;

        // save callback
        _self.callback = callback;
        
        // remove
        CompleteData();
    };

    // decline action
    _self.Decline = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        DeclineData();
    };

    // approve action
    _self.Approve = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        ApproveData();
    };

    // loading modal from Views
    function loadView(url) {

        var data = {};

        if (_self.id != null && (_self.mode == "Edit"||_self.mode=="View")) {
            data.ID = _self.id;
        }

        data.PrisonerID = _self.PrisonerID;

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _self.content = res;

            _core.getService('Loading').Service.disableBlur('loaderClass');

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.'+_self.ModalName).modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnModal');
        var addPersonBtn = $('#addPersonBtnModal');
        var findPersonAgainBtn = $('#findpersonAgain');
        var nextBtn = $('#nextBtnModal');
        var prevBtn = $('#prevBtnModal');
        var checkItemsFirstPart = $('.checkValidateFirstPartModal');
        var checkItemsSecondPart = $('.checkValidateSecondPartModal');
        var PositionLibItemElem = $('#PositionLibItemID');
        var OtherPersons = $('#private-visit-other-visitors');
        var dates = $('.DatesModal');
        _self.getItems = $('.getItemsFirstPartModal');

        // unbind
        acceptBtn.unbind();
        checkItemsFirstPart.unbind();
        checkItemsSecondPart.unbind();

        // check validate and toggle accept button
        checkItemsFirstPart.bind('change keyup', function () {
            var action = false;

            _self.RelativePersonList = [];

            checkItemsFirstPart.each(function () {
                // trigger relative list change
                if ($(this).attr('id') == "RelativePersonList") {
                    var curValues = $(this).val();
                    if (curValues == null || curValues.length == 0) {
                        _self.RelativePersonList = [];
                    }
                    else {
                        for (var i in curValues) {
                            var data = {};
                            data.PersonalID = parseInt(curValues[i]);
                            removePerson(data);
                            _self.RelativePersonList.push(data);
                        }
                    }
                }
                if (($(this).val() == '' || $(this).val() == null) && $(this).attr('id') != "RelativePersonList") {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            _self.FinalList = _self.PersonList.concat(_self.RelativePersonList);

            if (OtherPersons.val() != "" && OtherPersons.val() == "true")
                nextBtn.attr('disabled', action);

            if (action == false && (_self.FinalList.length != 0 || _self.mode == 'Edit')) {
                acceptBtn.attr("disabled", action);
            }
            else {
                acceptBtn.attr('disabled', true);
            }
        });

        // check validate and toggle accept button
        checkItemsSecondPart.bind('change keyup', function () {
            var action = false;

            checkItemsSecondPart.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            if (action == false && _self.currentPersonalID != null && _self.currentPersonalID != '') {
                addPersonBtn.attr('disabled', action);
            }
            else {
                addPersonBtn.attr('disabled', true);
            }
        });

        // date range picker
        _core.getService("Date").Service.Bind(dates, true);

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == 'Add') {

                data.PrisonerID = _self.PrisonerID;
                
                // add
                addData(data);
            }
            else if (_self.mode == 'Edit') {

                data.ID = _self.id;

                // edit
                editData(data);
            };

            //hiding modal
            $('.'+_self.ModalName).modal('hide');
        });

        // bind next
        nextBtn.unbind().click(function () {
            $('.part1').addClass('hidden');
            $('.part2').removeClass('hidden');
            checkItemsSecondPart.trigger('change');
            nextBtn.attr('disabled', true);
            prevBtn.attr('disabled', false);
        });

        // bind prev
        prevBtn.unbind().click(function () {
            $('.part1').removeClass('hidden');
            $('.part2').addClass('hidden');
            checkItemsFirstPart.trigger('change');
            nextBtn.attr('disabled', false);
            prevBtn.attr('disabled', true);
        });

        // bind add Person button
        addPersonBtn.unbind().click(function () {
            
            if (_self.currentPersonalID != null && _self.currentPersonalID != '' && PositionLibItemElem.val() != '') {
                var data = {};
                data.PersonalID = _self.currentPersonalID;
                data.Person = {};
                data.Person['FirstName'] = _self.currentPerson.FirstName;
                data.Person['LastName'] = _self.currentPerson.LastName;
                data.Person['MiddleName'] = _self.currentPerson.MiddleName;
                data.Person['Birthday'] = _self.currentPerson.BirthDate;
                data.Person['Personal_ID'] = _self.currentPerson.SocialCardNumber;
                data.Person['IdentificationDocuments'] = _self.currentPerson.DocIdentities;
                removePerson(data);
                // add
                _self.PersonList.push(data);
                addPersonBtn.attr('disabled', true);
                // draw added personlist
                drawPersonList();
            }
            else {
                console.warn('some serious warning about adding person to list');
            }
            checkItemsFirstPart.trigger('change');
        });
        
        findPersonAgainBtn.unbind().click(function () {

            // clean search
            cleanSearch();

            if (_self.currentPersonalID != null && _self.currentPersonalID != '') {
                addPersonBtn.attr('disabled', false);
            }
            else {
                addPersonBtn.attr('disabled', true);
            }
        });

        // trigger change
        //_self.getItems.bind('change keyup', function () {
        //    checkItems.trigger('change');
        //});

        // bind find person part to FindPerson Widget
        _core.getWidget('FindPerson').Widget.BindPersonSearch(function (status) {
            if (status) {
                var data = _core.getWidget('FindPerson').Widget.getPersonData();
                _self.currentPersonalID = _core.getWidget('FindPerson').Widget.getPersonData().Person.SocialCardNumber;
                _self.currentPerson = _core.getWidget('FindPerson').Widget.getPersonData().Person;
            }
            else {
                _self.currentPersonalID = null;
                _self.currentPerson = null;
            }

            if (_self.currentPersonalID != null && _self.currentPersonalID != '') {
                addPersonBtn.attr('disabled', false);
            }
            else {
                addPersonBtn.attr('disabled', true);
            }

            checkItemsFirstPart.trigger('change');
        });

        // bind hide and shows
        OtherPersons.bind('change', function () {
            var value = $(this).val();

            if (value != null && value == "true") {
                checkItemsFirstPart.trigger('change');
            }
            else {
                nextBtn.attr('disabled', true);
            }
        });

        // selectize
        $(".selectizeModal").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // bind person list events
    function bindPersonListEvents() {
        var removeRow = $('.removePersonListRow');

        removeRow.unbind().click(function () {
            var personid = parseInt($(this).data('id'));

            var data = {};
            data.PersonalID = personid;

            // remove person
            removePerson(data);

            // draw
            drawPersonList();
        });
    }

    // add
    function addData(data) {

        // post service
        var postService = _core.getService('Post').Service;
        
        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.addUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding');
        })
    }

    // edit
    function editData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.editUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing');
        })
    }

    // remove
    function removeData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.removeUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on removing');
        })
    }

    // complete
    function CompleteData() {

        var data = {};
        data.ID = _self.id;
        data.EndDate = _self.enddate;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.completeUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on completing');
        })
    }

    // recline
    function DeclineData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.declineUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on declining');
        })
    }

    // approve
    function ApproveData() {

        var data = {};
        data.id = _self.id;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.approveUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on approving');
        })
    }

    // collect data
    function collectData() {

        // array of violation list
        //_self.finalData["ViolationList"] = [];

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            if (name == "ViolationList") {
                //var tempValue = $(this).val();
                //for (var i in tempValue) {
                //    var temp = {};
                //    temp["LibItemID"] = parseInt(tempValue[i]);
                //    _self.finalData[name].push(temp);
                //}
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }

        });

        _self.finalData['VisitorList'] = [];

        //for (var i in _self.PersonList) {
        //    var data = {};
        //    data['PersonalID'] = _self.PersonList[i];
        //    _self.finalData['VisitorList'].push(data);
        //}
        for (var i in _self.finalData['RelativePersonList']) {
            var data = {};
            data['Person'] = {};
            data['Person']['ID'] = _self.finalData['RelativePersonList'][i];
            _self.finalData['VisitorList'].push(data);
        }

        return _self.finalData;
    }

    // remove soc id from list
    function removePerson(data) {
        for (var i in _self.PersonList) {
            if (parseInt(_self.PersonList[i].PersonalID) == parseInt(data.PersonalID)) {
                _self.PersonList.splice(i, 1);
            }
        }
    }

    // clean search
    function cleanSearch() {

        // hide main row for loader show (it is new user search)
        $('#foundusertable').addClass('hidden');
        $('#foundusererrorp').addClass('hidden');

        // disableing or enabling fields
        $('#modal-person-name-1').val('');
        $('#modal-person-name-2').val('');
        $('#modal-person-name-3').val('');
        $('#modal-person-bd').val('');
        $('#modal-person-social-id').val('');
        $('#modal-person-passport').val('');

        $('#modal-person-name-1').attr('disabled', false);
        $('#modal-person-name-2').attr('disabled', false);
        $('#modal-person-name-3').attr('disabled', false);
        $('#modal-person-bd').attr('disabled', false);
        $('#modal-person-social-id').attr('disabled', false);
        $('#modal-person-passport').attr('disabled', false);

        _self.currentPersonalID = null;
    }

    // draw person list
    function drawPersonList() {
        var data = [];
        for (var i in _self.PersonList)
        {
            for (var j in _self.PersonList) {
                data.push(_self.PersonList[j].Person);
            }

        }
        _core.getService('Post').Service.postPartial(data, _self.drawPersonListURL, function (res) {
            if (res != null) {
                $('#personListTableBody').empty();
                $('#personListTableBody').append(res);

                // bind draw PersonList events
                bindPersonListEvents();
            }
        })
    }

}
/************************/


// creating class instance
var PersonalVisitsWidget = new PersonalVisitsWidgetClass();

// creating object
var PersonalVisitsWidgetObject = {
    // important
    Name: 'PersonalVisits',

    Widget: PersonalVisitsWidget
}

// registering widget object
_core.addWidget(PersonalVisitsWidgetObject);