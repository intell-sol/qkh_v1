﻿// Add Conflicts widget

/*****Main Function******/
var ConflictsWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.PrisonerID = null;
    _self.id = null;
    _self.mode = null;
    _self.currentPersonalID = null;
    _self.CheckConflictUrl = '/_Data_Cell_/GetConflictPersons';

    _self.modalUrl = "/_Popup_/Get_Conflict";
    _self.viewUrl = "/_Popup_Views_/Get_Conflict";
    _self.addUrl = '/Conflicts/AddData';
    _self.editUrl = '/Conflicts/EditData';
    _self.removeUrl = '/Conflicts/RemData';
    _self.ModalName = 'conflictsModal';
    _self.finalData = {};
    
    _self.init = function () {
        console.log('Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.PrisonerID = null;
        _self.currentPersonalID = null;
        _self.id = null;
        _self.mode = null;
    };

    // add action
    _self.Add = function (PrisonerID, callback) {

        // initilize
        _self.init();

        console.log('Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = 'Add';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    }

    // view action
    _self.View = function (id, callback) {
        // initilize
        _self.init();

        console.log('view called');

        _self.id = id;

        _self.mode = 'View';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewUrl);
    };
    // edit action
    _self.Edit = function (id, callback)
    {
        // initilize
        _self.init();

        console.log('Edit called');

        _self.id = id;

        _self.mode = 'Edit';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // edit action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        removeData();
    };

    // loading modal from Views
    function loadView(url) {

        var data = {};

        if (_self.id != null && (_self.mode == "Edit"||_self.mode=="View")) {
            data.ID = _self.id;
        }

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _self.content = res;

            _core.getService('Loading').Service.disableBlur('loaderClass');

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            if (_self.mode == 'Edit') {
                editModalEvents();
            }

            // showing modal
            $('.'+_self.ModalName).modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnModal');
        var addPersonBtn = $('#addPersonBtnModal');
        var findPersonAgainBtn = $('#findpersonAgain');
        var checkItems = $('.checkValidateModal');
        var dates = $('.DatesModal');
        _self.getItems = $('.getItemsModal');

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind accept
        acceptBtn.bind('click', function ()
        {

            var data = {};
            debugger;
            data = collectData();
            data.PrisonerID = _self.PrisonerID;
            data.PersonalID = _self.currentPersonalID;
            var postService = _core.getService('Post').Service;
            _core.getService('Loading').Service.enableBlur('loaderClass');

            postService.postJson(data, _self.addUrl, function (res)
            {
                debugger;
                console.log(res);
                 if (_self.mode == 'Edit')
                    {

                        data.ID = _self.id;

                        // edit
                        editData(data);
                        $('.' + _self.ModalName).modal('hide');
                        _core.getService('Loading').Service.disableBlur('loaderClass');
                    }else if (res != null)
                {
                        if (res.msg == 2){
                            $('.warningClass').removeClass('hidden');
                            _core.getService('Loading').Service.disableBlur('loaderClass');
                        }
                       

                    else
                    {
                        $('.warningClass').addClass('hidden');
                        $('.' + _self.ModalName).modal('hide');
                        _core.getService('Loading').Service.disableBlur('loaderClass');                        
                    }
                    _self.callback({"status": true});
                }
                //if (res != null && res.msg != 2)
                //{
                //    var data = collectData();

                //    if (_self.mode == 'Add')
                //    {

                //        data.PrisonerID = _self.PrisonerID;
                //        data.PersonalID = _self.currentPersonalID;

                //        // add
                //        addData(data);
                //        
                //    }
                //    else if (_self.mode == 'Edit')
                //    {

                //        data.ID = _self.id;

                //        // edit
                //        editData(data);
                //        $('.' + _self.ModalName).modal('hide');
                //    };
                //}
            });

           

           // hiding modal
          
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });
            debugger
            if (_self.mode != 'Edit' && !_core.getWidget("FindPerson").Widget.isValidData()) action = true;

            acceptBtn.attr("disabled", action);
        });

       // trigger change
        //_self.getItems.bind('change keyup', function () {
        //    checkItems.trigger('change');
        //});

        // bind find person part to FindPerson Widget

        _core.getWidget('FindPerson').Widget.BindPersonSearch(function (status) {
            if (status) {

                var data = _core.getWidget('FindPerson').Widget.getPersonData();
                _self.currentPersonalID = _core.getWidget('FindPerson').Widget.getPersonData().Person.Personal_ID;
                _self.currentPerson = _core.getWidget('FindPerson').Widget.getPersonData().Person;
            }

            checkItems.trigger('change');
        });

        // selectize
        $(".selectizeModal").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }
    // edit modal events
    function editModalEvents() {

        // remove next button disabled
        $('#findperson').attr('disabled', true);

        // collapse table
        var collapseTable = $(".collapseTable");
        var collapseTableTr = collapseTable.find("tbody tr:not(.collapse)");

        collapseTableTr.click(function () {
            if ($(this).next("tr.collapse").hasClass("in")) {
                $(this).parents(".collapseTable").find("tr.collapse.in").removeClass("in");
            }
            else {
                $(this).parents(".collapseTable").find("tr.collapse.in").removeClass("in");
                $(this).parents(".collapseTable").find("input[name='modal-person-radio']").prop("checked", false);
                $(this).next("tr.collapse").addClass("in");
                $(this).find("input[name='modal-person-radio']").prop("checked", true);
            }
        });

        collapseTableTr.trigger('click');

        // disable fileds editing
        $('#modal-person-name-1').attr('disabled', true);
        $('#modal-person-name-2').attr('disabled', true);
        $('#modal-person-name-3').attr('disabled', true);
        $('#modal-person-bd').attr('disabled', true);
        $('#modal-person-social-id').attr('disabled', true);
        $('#modal-person-passport').attr('disabled', true);
    }
   
    // add
    function addData(data) {

        // post service
        var postService = _core.getService('Post').Service;
        
        postService.postJson(data, _self.addUrl, function (res)
        {
            _core.getService('Loading').Service.disableBlur('loaderClass');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding');
        })
    }

    // edit
    function editData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.editUrl, function (res) {
            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing');
        })
    }

    // remove
    function removeData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.removeUrl, function (res)
        {
            _core.getService('Loading').Service.disableBlur('loaderClass');
            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on removing');
        })
    }
    // clean search
    function cleanSearch() {

        // hide main row for loader show (it is new user search)
        $('#foundusertable').addClass('hidden');
        $('#foundusererrorp').addClass('hidden');

        // disableing or enabling fields
        $('#modal-person-name-1').val('');
        $('#modal-person-name-2').val('');
        $('#modal-person-name-3').val('');
        $('#modal-person-bd').val('');
        $('#modal-person-social-id').val('');
        $('#modal-person-passport').val('');

        $('#modal-person-name-1').attr('disabled', false);
        $('#modal-person-name-2').attr('disabled', false);
        $('#modal-person-name-3').attr('disabled', false);
        $('#modal-person-bd').attr('disabled', false);
        $('#modal-person-social-id').attr('disabled', false);
        $('#modal-person-passport').attr('disabled', false);

        _self.currentPersonalID = null;
    }


    // collect data
    function collectData() {

        // array of violation list
        //_self.finalData["ViolationList"] = [];

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            if (name == "ViolationList") {
                //var tempValue = $(this).val();
                //for (var i in tempValue) {
                //    var temp = {};
                //    temp["LibItemID"] = parseInt(tempValue[i]);
                //    _self.finalData[name].push(temp);
                //}
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }

        });

        _self.finalData["Person"] = _core.getWidget('FindPerson').Widget.getPersonData().Person;

        return _self.finalData;
    }

}
/************************/


// creating class instance
var ConflictsWidget = new ConflictsWidgetClass();

// creating object
var ConflictsWidgetObject = {
    // important
    Name: 'Conflicts',

    Widget: ConflictsWidget
}

// registering widget object
_core.addWidget(ConflictsWidgetObject);