﻿// Add PrevConvictions widget

/*****Main Function******/
var PrevConvictionsWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.PrisonerID = null;
    _self.id = null;
    _self.mode = null;

    _self.modalUrl = "/_Popup_/Get_PrevConviction";
    _self.addUrl = '/_Data_Primary_/AddPrevConviction';
    _self.editUrl = '/_Data_Primary_/EditPrevConviction';
    _self.removeUrl = '/_Data_Primary_/RemPrevConviction';
    _self.ModalName = 'personal-old-sentence-add-modal';
    _self.viewUrl = "/_Popup_Views_/Get_PrevConviction";

    _self.finalData = {};
    
    _self.init = function () {
        console.log('Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.PrisonerID = null;
        _self.id = null;
        _self.mode = null;
    };

    // add action
    _self.Add = function (PrisonerID, callback) {

        // initilize
        _self.init();

        console.log('Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = 'Add';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    }

    // edit action
    _self.Edit = function (id, callback) {
        // initilize
        _self.init();

        console.log('Edit called');

        _self.id = id;

        _self.mode = 'Edit';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // edit action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        removeData();
    };
    // edit action
    _self.View = function (id, callback)
    {
        // initilize
        _self.init();

        console.log('Edit called');

        _self.id = id;

        _self.mode = 'View';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewUrl);
    };
    // loading modal from Views
    function loadView(url)
    {

        var data = {};
       

        if (_self.id != null && (_self.mode == "Edit" || _self.mode == "View")) {
            data.ID = _self.id;
        }

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _self.content = res;

            _core.getService('Loading').Service.disableBlur('loaderClass');

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.'+_self.ModalName).modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnModal');
        _self.checkItems = $('.checkValidateModal');
        var dates = $('.DatesModal');
        var codesElem = $('#prevCodeLibItemID');
        _self.getItems = $('.getItemsModal');
        _self.sentenceCode = $('#entrance-sentence-code');

        // unbind
        acceptBtn.unbind();
        _self.checkItems.unbind();

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == 'Add') {

                data.PrisonerID = _self.PrisonerID;
                
                // add
                addData(data);
            }
            else if (_self.mode == 'Edit') {

                data.ID = _self.id;

                // edit
                editData(data);
            };

            //hiding modal
            $('.'+_self.ModalName).modal('hide');
        });

        // check validate and toggle accept button
        _self.checkItems.bind('change keyup', function () {
            var action = false;

            _self.checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var name = $(this).attr('name');
                    if (name != 'SentencingDataArticles') {
                        //var id = parseInt($(this).data('parentid'));
                        //if (id == parseInt(_self.sentenceCode.val())) {
                        //}
                        var curName = $(this).attr('name');
                        if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                        else if (curName != 'StartDate' && curName != 'EndDate') action = true;
                    }
                }
            });
            

            try{
                if (_core.getService("SelectMenu").Service.collectData('SentencingDataArticles').length == 0)
                    action = true;
            }
            catch (e) {
                action = true;
            }

            acceptBtn.attr("disabled", action);
        });

        codesElem.bind('change keyup', function () {

            // load content of articles
            var ParentID = $(this).val();

            if (ParentID != null && ParentID != "") {

                _core.getService('Loading').Service.enableBlur('modal-body');

                var data = {};

                data.ID = _self.id;

                _core.getService("SelectMenu").Service.loadContent(ParentID, 'prevData', data, function (res) {

                    if (res != null) {
                        _core.getService("SelectMenu").Service.bindSelectEvents('SentencingDataArticles');
                    }

                    $('.checkItemsCustom').change(function () {
                        $('#PenaltyLibItemID').trigger('change');
                    })

                    _core.getService('Loading').Service.disableBlur('modal-body');
                });

            }
            else {
                $('.articlesHolder').empty();
            }
        });

        // selectize
        $(".selectizeModal").each(function () {
            $(this).selectize({
                create: false
            });
        });

        if (_self.id != null) {
            codesElem.trigger('change');
        }
    }

    // add
    function addData(data) {

        // post service
        var postService = _core.getService('Post').Service;
        
        postService.postJson(data, _self.addUrl, function (res) {
            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding');
        })
    }

    // edit
    function editData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.editUrl, function (res) {
            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing');
        })
    }

    // remove
    function removeData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.removeUrl, function (res) {
            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on removing');
        })
    }

    // collect data
    function collectData() {

        // array of violation list
        //_self.finalData["ViolationList"] = [];

        $('.getItemsModal').each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            if (name == 'SentencingDataArticles') {
                //var id = _self.sentenceCode.val();
                //var thisParentId = $(this).data('parentid');
                //if (parseInt(id) == parseInt(thisParentId)) {
                //    var value = $(this).val();
                //    _self.finalData[name] = [];
                //    for (var i in value) {
                //        var data = {};
                //        data.LibItem_ID = value[i];
                //        _self.finalData[name].push(data);
                //    }
                //}

                var value = _core.getService("SelectMenu").Service.collectData(name);

                _self.finalData[name] = [];
                for (var i in value) {
                    var data = {};
                    data.LibItem_ID = value[i];
                    _self.finalData[name].push(data);
                }
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }

        });

        return _self.finalData;
    }

}
/************************/


// creating class instance
var PrevConvictionsWidget = new PrevConvictionsWidgetClass();

// creating object
var PrevConvictionsWidgetObject = {
    // important
    Name: 'PrevConvictions',

    Widget: PrevConvictionsWidget
}

// registering widget object
_core.addWidget(PrevConvictionsWidgetObject);