﻿
/*****Main Function******/
var DeparturesWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.PrisonerID = null;
    _self.selectedPurpose = null;
    _self.id = null;
    _self.mode = null;

    _self.modalUrl = "/_Popup_/Get_Departure";
    _self.viewUrl = "/_Popup_Views_/Get_Departure";
    _self.addUrl = '/Departures/AddData';
    _self.editUrl = '/Departures/EditData';
    _self.removeUrl = '/Departures/RemData';
    _self.completeUrl = '/Departures/CompleteData';
    _self.declineUrl = '/Departures/DeclineData';
    _self.approveUrl = '/Departures/ApproveData';
    _self.ModalName = 'departure-add-modal';
    _self.finalData = {};
    
    _self.init = function () {
        console.log('Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.PrisonerID = null;
        _self.selectedPurpose = null;
        _self.id = null;
        _self.mode = null;
    };

    // add action
    _self.Add = function (PrisonerID, callback) {

        // initilize
        _self.init();

        console.log('Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = 'Add';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    }
    // view action
    _self.View = function (id, callback)
    {
        // initilize
        _self.init();

        console.log('View called');

        _self.id = id;

        _self.mode = 'View';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewUrl);
    };
    // edit action
    _self.Edit = function (id, callback) {
        // initilize
        _self.init();

        console.log('Edit called');

        _self.id = id;

        _self.mode = 'Edit';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // edit action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        removeData();
    };

    _self.Complete = function (id, EndDate, callback) {
        // initilize
        _self.init();

        console.log('Complete called');

        _self.id = id;
        _self.EndDate = EndDate

        // save callback
        _self.callback = callback;

        // complete
        completeData();
    };
    // decline action
    _self.Decline = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        DeclineData();
    };

    // approve action
    _self.Approve = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        ApproveData();
    };
    // loading modal from Views
    function loadView(url) {

        var data = {};

        if (_self.id != null && (_self.mode == "Edit"||_self.mode=="View")) {
            data.ID = _self.id;
        }

        data.PrisonerID = _self.PrisonerID;

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _core.getService('Loading').Service.disableBlur('loaderClass');

            _self.content = res;

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.'+_self.ModalName).modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnModal');
        var checkItems = $('.checkValidateModal');
        var dates = $('.DatesModal');
        _self.getItems = $('.getItemsModal');

        var orgid = $('#OrgUnitID');
        var hospitallib = $('#HospitalLibItemID');
        var courtlib = $('#CourtLibItemID');
        var investigatelib = $('#InvestigateLibItemID');
        var movelib = $('#MoveLibItemID');
        var medlib = $('#MedLibItemID');
        var caselib = $('#CaseLibItemID');

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            timePicker: true,
            timePicker24Hour: true,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(true),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker)
        {
            $(this).val(picker.startDate.format("DD/MM/YYYY HH:mm:ss"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker)
        {
            $(this).val("");
            dates.trigger('change');
        });
        dates.each(function () {
            if ($(this).val() == "")
                $(this).data('daterangepicker').setStartDate(new Date($.now()));
            else {
                function dateString2Date(dateString) {
                    var dt = dateString.split(/\/|\s/);
                    return new Date(dt.slice(0, 3).reverse().join('/') + ' ' + dt[3]);
                }
                var dateString = $(this).val();
                dateString = dateString2Date(dateString);
                $(this).data('daterangepicker').setStartDate(dateString);
            }
        })
        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == 'Add') {

                data.PrisonerID = _self.PrisonerID;
                
                // add
                addData(data);
            }
            else if (_self.mode == 'Edit') {

                data.ID = _self.id;

                // edit
                editData(data);
            };

            //hiding modal
            $('.'+_self.ModalName).modal('hide');
        });


        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            // working with purpose parts
            if ($(this).attr('id') == 'PurposeLibItemID') {
                purposePartWorker(parseInt($(this).val()));
            }

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Place') $(this).val(' ');
                    else action = true;
                }
            });

            if (_self.selectedPurpose == 'med') {
                if (parseInt(medlib.val()) == 705 && hospitallib.val() == '')
                    action = true;
                else if (parseInt(medlib.val()) == 704 && orgid.val() == '')
                    action = true;
                else if (medlib.val() == '') action = true
            }
            else if (_self.selectedPurpose == 'case') {
                if (parseInt(caselib.val()) == 707 && investigatelib.val() == '')
                    action = true;
                else if (parseInt(caselib.val()) == 706 && courtlib.val() == '')
                    action = true;
                else if (caselib.val() == '') action = true
        }
            else if (_self.selectedPurpose == 'move' && movelib.val() == '') {
                action = true;
            }

            acceptBtn.attr("disabled", action);
        });

        hospitallib.change(function () {
            checkItems.trigger('change');
        })

        orgid.change(function () {
            checkItems.trigger('change');
        })

        investigatelib.change(function () {
            checkItems.trigger('change');
        })
        movelib.change(function () {
            checkItems.trigger('change');
        })
        courtlib.change(function () {
            checkItems.trigger('change');
        })
        // bind chages on selectes
        medlib.bind('change', function () {
            checkItems.trigger('change');
            var curValue = $(this).val();

            if (curValue != '') {
                curValue = parseInt(curValue);
                if (curValue == 705) {
                    $('#departure-purpose-health-hospital-wrap').removeClass('hidden');
                    $('#departure-purpose-health-department-wrap').addClass('hidden');
                }
                else if (curValue == 704) {
                    $('#departure-purpose-health-department-wrap').removeClass('hidden');
                    $('#departure-purpose-health-hospital-wrap').addClass('hidden');
                }
                else {
                    $('#departure-purpose-health-department-wrap').addClass('hidden');
                    $('#departure-purpose-health-hospital-wrap').addClass('hidden');
                }
            }
        });

        // bind chages on selectes
        caselib.bind('change',function () {
            checkItems.trigger('change');
            var curValue = $(this).val();

            if (curValue != '') {
                curValue = parseInt(curValue);
                if (curValue == 707) {
                    $('#departure-purpose-proceeding-investigative-wrap').removeClass('hidden');
                    $('#departure-purpose-proceeding-court-wrap').addClass('hidden');
                }
                else if (curValue == 706) {
                    $('#departure-purpose-proceeding-court-wrap').removeClass('hidden');
                    $('#departure-purpose-proceeding-investigative-wrap').addClass('hidden');
                }
                else {
                    $('#departure-purpose-proceeding-court-wrap').addClass('hidden');
                    $('#departure-purpose-proceeding-investigative-wrap').addClass('hidden');
                }
            }
        });

        // selectize
        $(".selectizeModal").each(function () {
            $(this).selectize({
                create: false
            });
        });

        // trigger change
        checkItems.trigger('change');
    }

    // add
    function addData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.addUrl, function (res) {

            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding');
        })
    }

    // decline
    function DeclineData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.declineUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on declining');
        })
    }

    // approve
    function ApproveData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.approveUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on approving');
        })
    }
    // edit
    function editData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.editUrl, function (res) {

            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing');
        })
    }

    // remove
    function removeData() {

        var data = {};
        data.id = _self.id;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.removeUrl, function (res) {

            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on removing');
        })
    } 
    function completeData() {

        var data = {};
        data.ID = _self.id;
        data['EndDate'] = _self.EndDate;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.completeUrl, function (res) {
            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on completing');
        })
    } 

    // purpose part worker
    function purposePartWorker(id) {
        var medpart = $('#medpart');
        var casepart = $('#casepart');
        var movepart = $('#movepart');

        if (id != null) {
            if (id == 692) {
                medpart.removeClass('hidden');
                casepart.addClass('hidden');
                movepart.addClass('hidden');
                _self.selectedPurpose = 'med';
            }
            else if (id == 693) {
                casepart.removeClass('hidden');
                medpart.addClass('hidden');
                movepart.addClass('hidden');
                _self.selectedPurpose = 'case';
            }
            else if (id == 694) {
                movepart.removeClass('hidden');
                medpart.addClass('hidden');
                casepart.addClass('hidden');
                _self.selectedPurpose = 'move';
            }
        }
    }

    // collect data
    function collectData() {

        // array of violation list
        //_self.finalData["ViolationList"] = [];

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            if (name == "ViolationList") {
                //var tempValue = $(this).val();
                //for (var i in tempValue) {
                //    var temp = {};
                //    temp["LibItemID"] = parseInt(tempValue[i]);
                //    _self.finalData[name].push(temp);
                //}
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }

        });

        return _self.finalData;
    }

}
/************************/


// creating class instance
var DeparturesWidget = new DeparturesWidgetClass();

// creating object
var DeparturesWidgetObject = {
    // important
    Name: 'Departures',

    Widget: DeparturesWidget
}

// registering widget object
_core.addWidget(DeparturesWidgetObject);