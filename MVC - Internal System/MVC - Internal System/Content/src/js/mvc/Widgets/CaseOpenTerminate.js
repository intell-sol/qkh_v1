﻿// case open terminate widget

/*****Main Function******/
var CaseOpenTerminateWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null;
    _self.PrisonerID = null;
    _self.number = null;
    _self.id = null;

    _self.modalUrl = "/_Popup_/Get_CaseOpenTerminate";
    _self.openTerminationURL = '/_Data_Terminate_/OpenTermination';
    _self.finalData = {};
    

    _self.init = function () {
        console.log('CaseOpenTerminate Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.number = null;
        _self.PrisonerID = null;
        _self.id = null;
    };

    // ask action
    _self.Show = function (PrisonerID, callback) {
        // initilize
        _self.init();

        console.log('CaseOpenTerminate Ask called');

        // save callback
        _self.callback = callback;

        _self.mode = "Edit"

        // bind prisoner id
        _self.PrisonerID = PrisonerID;

        // loading view
        loadView();
    };

    // loading modal from Views
    function loadView() {

        var url = _self.modalUrl;

        var data = {};

        if (_self.mode = "Edit") {
            data.id = _self.id;
        }
        
        // postService
        _core.getService('Post').Service.postPartial(data, url, function (res) {
            if (res != null) {
            
                _core.getService('Loading').Service.disableBlur('loaderClass');

                // save content for later usage
                _self.content = res;

                // content
                $('#widgetHolder').html(_self.content);

                // bind events
                bindEvents();

                // showing modal
                $('.open-case-modal').modal('show');
            }
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtn');
        var checkItems = $('.checkValidateBanT');
        var dates = $('.DatesBanT');
        _self.getItems = $('.getItemsBanT');

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind accept
        acceptBtn.bind('click', function () {
            
            var data = collectData();
            
            // save termination data
            openTemination(data);

            //hiding modal
            $('.open-case-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // selectize
        $(".selectizeTerminate").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // save termination function
    function openTemination(data) {

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.openTerminationURL, function (res) {
            if (res != null) {

                // callback
                _self.callback(res);
            }
            else alert('serious eror on opening ban terminate');
        })
    }

    // collect data
    function collectData() {

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            // appending to Data
            _self.finalData[name] = $(this).val();
        });

        // bind ID
        _self.finalData['PrisonerID'] = _self.PrisonerID;

        return _self.finalData;
    }

}
/************************/


// creating class instance
var CaseOpenTerminateWidget = new CaseOpenTerminateWidgetClass();

// creating object
var CaseOpenTerminateWidgetObject = {
    // important
    Name: 'CaseOpenTerminate',

    Widget: CaseOpenTerminateWidget
}

// registering widget object
_core.addWidget(CaseOpenTerminateWidgetObject);