﻿// Invalids widget

/*****Main Function******/
var InvalidsWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.modal = $('.personal-old-sentence-add-modal');
    _self.modalUrl = "/_Popup_/Get_Invalids";
    _self.editUrl = "/_Data_Physical_/EditInvalids"
    _self.viewUrl = "/_Popup_Views_/Get_Invalids";

    _self.finalFormData = new FormData();
    

    // action list
    _self.actionList = {
    };

    _self.init = function () {
        console.log('Invalids Widget Inited');

        _self.finalFormData = new FormData();
        _self.callback = null;
        _self.content = null;
        _self.id = null;
        _self.PrisonerID = null;
        _self.filechange = false;
        _self.mode = 'Add';
    };

    // ask action
    _self.Show = function (type, callback) {
        // initilize
        _self.init();

        console.log('Invalids Ask called');

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };
    _self.View = function (id, PrisonerID, callback)
    {
        // initilize
        _self.init();

        _self.mode = 'View';

        console.log('Invalids Edit called');

        _self.id = id;
        _self.PrisonerID = PrisonerID;

        _self.mode = 'View';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewUrl);
    };
    // edit action
    _self.Edit = function (id, PrisonerID, callback) {
        // initilize
        _self.init();

        _self.mode = 'Edit';

        console.log('Invalids Edit called');

        _self.id = id;
        _self.PrisonerID = PrisonerID;

        _self.mode = 'Edit';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // loading modal from Views
    function loadView(url) {
        
           
        if (_self.id != null && (_self.mode == "Edit" || _self.mode == "View")) url = url + '/' + _self.id;

            $.get(url, function (res) {

                // save content for later usage
                _self.content = res;

                _core.getService('Loading').Service.disableBlur('loaderClass');

                // content
                $('#widgetHolder').html(_self.content);

                // bind events for yes and no
                bindEvents();

                // showing modal
                $('.invalids-modal').modal('show');
            }, 'html');
        }
       
   

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtn');
        var checkItems = $('.checkValidatePopup');
        _self.getItems = $('.getItemsPopup');
        var dates = $('.datePick');

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();
        _self.getItems.unbind();


        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
        });

        // bind accept
        acceptBtn.bind('click', function () {
            // calling callback
            if (_self.mode == 'Add') {
                if (_self.callback != null)
                    _self.callback(collectData());
            }
            else if (_self.mode == 'Edit') {
                data = collectData();
                data.append('ID', _self.id);
                data.append('PrisonerID', _self.PrisonerID);

                // edit
                editInvalid(data);
            }

            //hiding modal
            $('.invalids-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                var curName = $(this).attr('name');
                if (curName == 'CertificateFileID') {
                    readURL(this);
                }
                else if ($(this).val() == '' || $(this).val() == null) {
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // get items
        _self.getItems.bind('change', function () {
            
            _self.getItems.each(function () {

                // name of Field (same as in Entity)
                var name = $(this).attr('name');

                // clean FormData
                _self.finalFormData.delete(name);

                // append to FormData
                if (name == 'CertificateFileID') {
                    _self.finalFormData.append(name, $(this)[0].files[0], $(this).val());
                }
                else {
                    _self.finalFormData.append(name, $(this).val());
                }
            });
        });

        // selectize
        $(".selectize").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // edit ban
    function editInvalid(data) {

        //// logit
        //console.log(data);

        // post service
        var postService = _core.getService('Post').Service;

        postService.postFormData(data, _self.editUrl, function (res) {

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing invalid');
        })
    }

    // collect data
    function collectData() {
        if (_self.mode == 'Edit') {
            _self.getItems.each(function () {

                // name of Field (same as in Entity)
                var name = $(this).attr('name');

                // clean FormData
                _self.finalFormData.delete(name);

                // append to FormData
                if (name == 'CertificateFileID' && _self.filechange) {
                    _self.finalFormData.append(name, $(this)[0].files[0], $(this).val());
                }
                else {
                    _self.finalFormData.append(name, $(this).val());
                }
            });
        }
        return _self.finalFormData;
    }

    // preview & update an image before it is uploaded
    function readURL(input) {
        if (input.files && input.files[0]) {

            var photoElem = $('#modal-invalid-photolink');

            photoElem.removeClass('hidden');

            var reader = new FileReader();

            reader.onload = function (e) {
                photoElem.attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);

            _self.filechange = true;
        }
    }

}
/************************/


// creating class instance
var InvalidsWidget = new InvalidsWidgetClass();

// creating object
var InvalidsWidgetObject = {
    // important
    Name: 'Invalids',

    Widget: InvalidsWidget
}

// registering widget object
_core.addWidget(InvalidsWidgetObject);