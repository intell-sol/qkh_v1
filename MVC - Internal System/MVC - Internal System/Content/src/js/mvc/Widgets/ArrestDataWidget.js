﻿// ArrestData widget

/*****Main Function******/
var ArrestDataWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.container = null; // dom container
    _self.getItems = null;
    _self.PrisonerID = null;
    _self.mode = null;
    _self.id = null;
    _self.modalUrl = "/_Popup_/Get_ArrestData";
    _self.viewUrl = "/_Popup_Views_/Get_ArrestData";
    _self.addArrestDataURL = '/_Data_Enterance_/AddArrestData';
    _self.editArrestDataURL = '/_Data_Enterance_/EditArrestData';
    _self.remArrestDataURL = '/_Data_Enterance_/RemArrestData';
    _self.finalData = {};

    _self.init = function () {
        console.log('ArrestData Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.mode = null;
        _self.id = null;
        _self.getItems = null;
        _self.PrisonerID = null;
    };

    // add action
    _self.Add = function (PrisonerID, callback) {
        // initilize
        _self.init();

        console.log('ArrestData Add called');

        // save callback
        _self.callback = callback;

        _self.mode = 'Add';

        // prisoner id
        _self.PrisonerID = PrisonerID;

        // loading view
        loadView(_self.modalUrl);
    };
    // view action
    _self.View = function (id, callback)
    {
        // initilize
        _self.init();

        console.log('ArrestData View called');

        // save callback
        _self.callback = callback;

        _self.mode = 'View';

        // id
        _self.id = id;

        // loading view
        loadView(_self.viewUrl);
    };
    // edit action
    _self.Edit = function (id, callback) {
        // initilize
        _self.init();

        console.log('ArrestData Edit called');

        // save callback
        _self.callback = callback;

        _self.mode = 'Edit';

        // id
        _self.id = id;

        // loading view
        loadView(_self.modalUrl);
    };

    // remove action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('ArrestData Remove called');

        // save callback
        _self.callback = callback;

        // id
        _self.id = id;

        // yes or no widget
        _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {
            if (res) {

                // remove ArrestData
                removeArrestData();
            }
        });
    };

    // loading modal from Views
    function loadView(url) {

        var data = {};

        if ((_self.mode == "Edit" || _self.mode == "View"))
        {
            data.id = _self.id;
        }

        // postService
        _core.getService('Post').Service.postPartial(data, url, function (res) {
            if (res != null) {
                _self.content = res;

                _core.getService('Loading').Service.disableBlur('loaderClass');

                // content
                _self.container = $(_self.content);
                $('#widgetHolder').html(_self.container);

                // bind events for yes and no
                bindEvents();

                // showing modal
                $('.entrance-court-modal').modal('show');
            }
        })
    }

    // binding buttons for Modal
    function bindEvents() {
        // buttons and fields
        var acceptBtn = _self.container.find('#acceptBtn');
        var addProceedingBtn = $('#addProceedingBtn');
        _self.checkItems = $('.checkValidateModal');
        var checkItemsProceedings = $('.checkValidateProceedings');
        var codesElem = $('#entrance-sentence-code');
        _self.getItems = $('.getItemsModal');
        _self.getItemsProceedings = $('.getItemsProceedings');
        _self.getItemsProceedings = $('.getItemsProceedings');
        var dates = $('.DatesSentence');

        // unbind
        acceptBtn.unbind();
        addProceedingBtn.unbind();
        _self.checkItems.unbind();
        checkItemsProceedings.unbind();

        _core.getService("SelectMenu").Service.bindSelectEvents('SentencingDataArticles');
        // bind custom select
        _core.getService("SelectMenu").Service.bindSelectEvents('ProceedLibItemID');

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == "Add") {
                data.PrisonerID = _self.PrisonerID;
            }
            else if (_self.mode == "Edit") {
                data.ID = _self.id;
            }

            // save Arrest data
            saveArrestData(data);

            //hiding modal
            $('.entrance-court-modal').modal('hide');
        });

        // bind add proceeding
        addProceedingBtn.bind('click', function () {

            var data = collectProceedings();
            if (data != null) {

            var curHtml = '<tr class="getItemsCollectPoints" data-libid="' + data['ProceedLibItemID'] + '" data-date="' + data['Date'] + '">';
            curHtml += '<td>' + data['ProceedLibItemLabel'] + '</td>';
            curHtml += '<td>' + data['Date'] + '</td>';
            curHtml += '<td><button class="btn btn-sm btn-default btn-flat removeProceed" title="Հեռացնել" ><i class="fa fa-trash-o"></i></button></td>';
            curHtml += '</tr>';

            $('#proceedingsTableRow').append(curHtml);

            bindProceedEvents();

            addProceedingBtn.attr('disabled', true);

            dates.trigger('change');

            }
        });

        // check validate and toggle accept button
        _self.checkItems.bind('change keyup', function () {
            var action = false;

            _self.checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            try {
                if (_core.getService("SelectMenu").Service.collectData('SentencingDataArticles').length == 0)
                    action = true;
            }
            catch (e) {
                action = true;

            }

            acceptBtn.attr("disabled", action);
        });

        // check validate and toggle accept button
        checkItemsProceedings.bind('change keyup', function () {
            var action = false;

            checkItemsProceedings.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            addProceedingBtn.attr("disabled", action);
        });

        codesElem.bind('change keyup', function () {

            // load content of articles
            var ParentID = $(this).val();

            if (ParentID != null && ParentID != "") {

                _core.getService('Loading').Service.enableBlur('modal-body')

                var data = {};

                data.ID = _self.id;

                _core.getService("SelectMenu").Service.loadContent(ParentID, 'arrestData', data, function (res) {

                    if (res != null) {
                        _core.getService("SelectMenu").Service.bindSelectEvents('SentencingDataArticles');
                    }

                    $('.checkItemsCustom').change(function () {
                        $('#entrance-verdict-number').trigger('change');
                    })

                    _core.getService('Loading').Service.disableBlur('modal-body');
                });

            }
            else {
                $('.articlesHolder').empty();
            }
        });

        // selectize
        $(".selectizeSentence").each(function () {
            $(this).selectize({
                create: false
            });
        });

        if (_self.id != null) {
            codesElem.trigger('change');
        }
        // bindings
        bindProceedEvents();
    }

    function bindProceedEvents() {
        $('.removeProceed').unbind().bind('click', function (e) {
            e.preventDefault();

            $(this).parent().parent().remove();

            //$('#entrance-verdict-number').trigger('change');
        });
    }

    // Add arrest data to database
    function saveArrestData(data) {

        var url = _self.addArrestDataURL;
        if (_self.mode == "Edit") url = _self.editArrestDataURL;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('arrestDataLoader');

        postService.postJson(data, url, function (res) {

            _core.getService('Loading').Service.disableBlur('arrestDataLoader');

            if (res != null) {

                // callback
                _self.callback(res);
            }
            else alert('serious eror on saving data');
        });
    }

    // remove arrestData
    function removeArrestData() {

        if (_self.id != null) {

            var data = {};
            data.ID = _self.id

            _core.getService('Loading').Service.enableBlur('arrestDataLoader');

            // postService
            _core.getService('Post').Service.postJson(data, _self.remArrestDataURL, function (res) {

                _core.getService('Loading').Service.disableBlur('arrestDataLoader');

                if (res != null) {
                    _self.callback(res);
                }
            })
        }
    }

    // collect data
    function collectData() {

        $('.getItemsModal').each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            if (name == 'SentencingDataArticles') {
                //var id = _self.sentenceCode.val();
                //var thisParentId = $(this).data('parentid');
                //if (parseInt(id) == parseInt(thisParentId)) {
                //    var value = $(this).val();
                //    _self.finalData[name] = [];
                //    for (var i in value) {
                //        var data = {};
                //        data.LibItem_ID = value[i];
                //        _self.finalData[name].push(data);
                //    }
                //}

                var value = _core.getService("SelectMenu").Service.collectData(name);
                _self.finalData[name] = [];
                for (var i in value) {
                    var data = {};
                    data.LibItem_ID = value[i];
                    _self.finalData[name].push(data);
                }
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }
        });

        _self.finalData['ArrestDataProceedings'] = collectProceedingsData();

        return _self.finalData;
    }


    // collect proceeding
    function collectProceedings() {

        var data = {};
        var isNull = false;

        _self.getItemsProceedings.each(function () {

            var ProceedLibItemID = $(this).hasClass('ProceedLibItemID');

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            // appending to Data
            if (ProceedLibItemID) {
                var curValues = _core.getService("SelectMenu").Service.collectFullData(name);
                if (curValues == null || curValues == '' || curValues.length == 0) {
                    isNull = true;
                    return;
                }
                for (var i in curValues) {
                    data[name] = curValues[i]['ID'];
                    data['ProceedLibItemLabel'] = curValues[i]['Name'];
                    break;
                }
            }
            else {
                // appending to Data
                data[name] = $(this).val();
            }
        });

        if (isNull) return null;
        return data;
    }

    // collect proceeding
    function collectProceedingsData() {

        var list = [];

        $('.getItemsCollectPoints').each(function () {

            var data = {};

            data['ProceedLibItemID'] = $(this).data('libid');
            data['Date'] = $(this).data('date');

            list.push(data);
        });

        return list;
    }

}
/************************/


// creating class instance
var ArrestDataWidget = new ArrestDataWidgetClass();

// creating object
var ArrestDataWidgetObject = {
    // important
    Name: 'ArrestData',

    Widget: ArrestDataWidget
}

// registering widget object
_core.addWidget(ArrestDataWidgetObject);
