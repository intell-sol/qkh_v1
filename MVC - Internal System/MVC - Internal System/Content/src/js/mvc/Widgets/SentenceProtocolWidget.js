﻿// ArrestData widget

/*****Main Function******/
var SentenceProtocolWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null;
    _self.PrisonerID = null;
    _self.id = null;
    _self.mode = null;
    _self.modalUrl = "/_Popup_/Get_SentenceProtocol";
    _self.viewlUrl = "/_Popup_Views_/Get_SentenceProtocol";

    _self.addProtocolURL = '/_Data_Enterance_/AddProtocol';
    _self.editProtocolURL = '/_Data_Enterance_/EditProtocol';
    _self.remProtocolURL = '/_Data_Enterance_/RemProtocol';

    _self.finalData = {};
    

    // action list
    _self.actionList = {
    };

    _self.init = function () {
        console.log('SentenceProtocol Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.PrisonerID = null;
        _self.id = null;
        _self.mode = null;
        _self.getItems = null;
    };

    // add action
    _self.Add = function (PrisonerID, callback) {
        // initilize
        _self.init();

        console.log('SentenceProtocol Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = "Add";

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };
    // view action
    _self.View = function (id, callback)
    {
        // initilize
        _self.init();

        console.log('SentenceProtocol view called');

        _self.id = id;

        _self.mode = "View";

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewlUrl);
    };
    // edit action
    _self.Edit = function (id, callback) {
        // initilize
        _self.init();

        console.log('SentenceProtocol Edit called');

        _self.id = id;

        _self.mode = "Edit";

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // remove action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('SentenceProtocol Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // yes or no widget
        _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {
            if (res) {

                // remove Protocol
                removeProtocol();
            }
        });
    };

    // loading modal from Views
    function loadView(url) {
        
        var data = {};

        if (_self.mode == "Edit"||_self.mode=="View") {
            data.id = _self.id;
        }

        // postService
        _core.getService('Post').Service.postPartial(data, url, function (res) {
            if (res != null) {

                // save content for later usage
                _self.content = res;

                _core.getService('Loading').Service.disableBlur('loaderClass');

                // content
                $('#widgetHolder').html(_self.content);

                // bind events for yes and no
                bindEvents();

                // showing modal
                $('.entrance-report-modal').modal('show');
            }
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtn');
        var checkItems = $('.checkValidateSentence');
        _self.getItems = $('.getItemsSentence');
        var dates = $('.DatesSentence');

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == "Add") {
                data.PrisonerID = _self.PrisonerID;
            }
            else if (_self.mode == "Edit") {
                data.ID = _self.id;
            }

            // save Protocol data
            saveProtocolData(data);

            //hiding modal
            $('.entrance-report-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // selectize
        $(".selectizeSentence").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // save data
    function saveProtocolData(data) {

        var url = _self.addProtocolURL;
        if (_self.mode == "Edit") url = _self.editProtocolURL;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('arrestProtocolLoader');

        postService.postJson(data, url, function (res) {

            _core.getService('Loading').Service.disableBlur('arrestProtocolLoader');

            if (res != null) {

                // callback
                _self.callback(res);
            }
            else alert('serious eror on saving data');
        });
    }

    // remove data
    function removeProtocol() {

        if (_self.id != null) {

            var data = {};
            data.ID = _self.id

            _core.getService('Loading').Service.enableBlur('arrestProtocolLoader');

            // postService
            _core.getService('Post').Service.postJson(data, _self.remProtocolURL, function (res) {

                _core.getService('Loading').Service.disableBlur('arrestProtocolLoader');

                if (res != null) {

                    // callback
                    _self.callback(res);
                }
            })
        }
    }

    // collect data
    function collectData() {

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            if (name == 'SentencingDataArticles') {
                var id = _self.sentenceCode.val();
                var thisParentId = $(this).data('parentid');
                if (parseInt(id) == parseInt(thisParentId)) {
                    var value = $(this).val();
                    _self.finalData[name] = [];
                    for (var i in value) {
                        var data = {};
                        data.LibItem_ID = value[i];
                        _self.finalData[name].push(data);
                    }
                }
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }
        });

        return _self.finalData;
    }

}
/************************/


// creating class instance
var SentenceProtocolWidget = new SentenceProtocolWidgetClass();

// creating object
var SentenceProtocolWidgetObject = {
    // important
    Name: 'SentenceProtocol',

    Widget: SentenceProtocolWidget
}

// registering widget object
_core.addWidget(SentenceProtocolWidgetObject);