﻿// bans widget

/*****Main Function******/
var BansWidgetClass = function ()
{
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null;
    _self.PrisonerID = null;
    _self.id = null;
    _self.mode = null;
    _self.State == null;
    _self.modalUrl = "/_Popup_/Get_Bans";
    _self.viewUrl = "/_Popup_Views_/Get_Bans";
    _self.addBanUrl = '/_Data_Ban_/AddBan';
    _self.editBanUrl = '/_Data_Ban_/EditBan';
    _self.removeBanUrl = '/_Data_Ban_/RemBan';
    _self.finalData = {};


    // action list
    _self.actionList = {
    };

    _self.init = function ()
    {
        console.log('Bans Widget Inited');
        _self.State == null;
        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.PrisonerID = null;
        _self.id = null;
        _self.mode = null;
    };

    // add action
    _self.Add = function (PrisonerID, callback)
    {
        // initilize
        _self.init();

        console.log('Bans Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = 'Add';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };
    // view action
    _self.View = function (id, callback)
    {
        // initilize
        _self.init();

        console.log('Bans View called');

        _self.id = id;

        _self.mode = 'View';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewUrl);
    };
    // edit action
    _self.Edit = function (id, callback)
    {
        _self.State = false;
        // initilize
        _self.init();
        console.log('Bans Edit called');
        _self.id = id;
        _self.mode = 'Edit';
        
        // save callback
        _self.callback = callback;
        // loading view
        loadView(_self.modalUrl);
    };

    // remove action
    _self.Remove = function (id, callback)
    {
        // initilize
        _self.init();

        console.log('Bans Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove ban function call
        removeBan()
    };

    // loading modal from Views
    function loadView(url)
    {

        if (_self.id != null && (_self.mode == "Edit" || _self.mode == "View")) url = url + '/' + _self.id;

        $.get(url, function (res)
        {

            _core.getService('Loading').Service.disableBlur('loaderClass');

            // save content for later usage
            _self.content = res;

            // content
            $('#widgetHolder').html(_self.content);
            
            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.ban-add-modal').modal('show');
        }, 'html');
  
        bindHideEvents();
    }

    // binding buttons for Modal
    function bindEvents()
    {

        // buttons and fields
        var acceptBtn = $('#acceptBtnban');
        var checkItems = $('.checkValidateBan');
        var stateElem = $("#state");
        var dates = $('.Dates');
        _self.getItems = $('.getItemsBan');
        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        // bind custom select
        _core.getService("SelectMenu").Service.bindSelectEvents('VoroshoxLibItem_ID');
        _core.getService("SelectMenu").Service.bindSelectEvents('TerminationVoroshoxLibItem_ID');

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker)
        {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker)
        {
            $(this).val("");
            dates.trigger('change');
        });

        // bind accept
        acceptBtn.bind('click', function ()
        {

            var data = collectData();
            data["State"] = _self.State
            if (_self.mode == 'Add')
            {

                data.PrisonerID = _self.PrisonerID;

                // add
                addBan(data);
            }
            else if (_self.mode == 'Edit')
            {

                data.ID = _self.id;
                data.State = $("#state").val();
                debugger;
                // edit
                editBan(data);
            }

            //hiding modal
            $('.ban-add-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function ()
        {
            var action = false;

            checkItems.each(function ()
            {
                if ($(this).val() == '' || $(this).val() == null)
                {
                    debugger
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Voroshman_Hamar' || curName == 'Notes') $(this).val(' ');
                    else if (curName == 'VoroshoxLibItem_ID')
                    {
                        if (_core.getService("SelectMenu").Service.collectData(curName).length == 0) action = true;
                    } else if (curName == 'TerminationVoroshoxLibItem_ID')
                    {
                        if (_core.getService("SelectMenu").Service.collectData(curName).length == 0 && stateElem.val() == "false") action = true;
                    }
                    else if ($(this).hasClass("checkValidateBanT"))
                    {
                        if (stateElem.val() == "false") action = true;
                    }
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });
        bindHideEvents();
        // selectize
        $(".selectizeBan").each(function ()
        {
            $(this).selectize({
                create: false
            });
        });

        $('.customSelectInputVoroshoxLibItem_ID').bind('change', function ()
        {
            checkItems.trigger('change');
        })
    }
    function bindHideEvents()
    {
        var state = $('#state');
        
        state.bind('change keyup load', function ()
        {
            var flag = state.val() == "true";
            debugger;
            console.log(state.val());
            if (flag)
            {
                $(".showhide").addClass('hidden');
            }
            else
            {
                $(".showhide").removeClass('hidden');

            }
        });
    }
    // add ban
    function addBan(data)
    {

        //// logit
        //console.log(data);

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('addbantablebody');

        postService.postJson(data, _self.addBanUrl, function (res)
        {
            _core.getService('Loading').Service.disableBlur('addbantablebody');

            if (res != null)
            {
                _self.callback(res);
            }
            else alert('serious eror on adding ban');
        })
    }

    // edit ban
    function editBan(data)
    {

        //// logit
        //console.log(data);

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('addbantablebody');

        postService.postJson(data, _self.editBanUrl, function (res)
        {
            _core.getService('Loading').Service.disableBlur('addbantablebody');


            if (res != null)
            {
                _self.callback(res);
            }
            else alert('serious eror on editing ban');
        })
    }

    // remove ban
    function removeBan()
    {

        var data = {};
        data.ID = _self.id;

        //// logit
        //console.log(data);

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('addbantablebody');

        postService.postJson(data, _self.removeBanUrl, function (res)
        {
            _core.getService('Loading').Service.disableBlur('addbantablebody');

            if (res != null)
            {
                _self.callback(res);
            }
            else alert('serious eror on removing ban');
        })
    }

    // collect data
    function collectData()
    {

        // array of violation list
        _self.finalData["Objects"] = [];

        _self.getItems.each(function ()
        {

            var VoroshoxLibItem_ID = $(this).hasClass('VoroshoxLibItem_ID');
            var TerminationVoroshoxLibItem_ID = $(this).hasClass('TerminationVoroshoxLibItem_ID');

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            if (name == "Objects")
            {
                var tempValue = $(this).val();
                for (var i in tempValue)
                {
                    var temp = {};
                    temp["LibItem_ID"] = parseInt(tempValue[i]);
                    _self.finalData[name].push(temp);
                }
            }
            else if (VoroshoxLibItem_ID)
            {
                var curValues = _core.getService("SelectMenu").Service.collectData(name);
                for (var i in curValues)
                {
                    _self.finalData[name] = curValues[i];
                    break;
                }
            }
            else if (TerminationVoroshoxLibItem_ID)
            {
                var curValues = _core.getService("SelectMenu").Service.collectData(name);
                for (var i in curValues)
                {
                    _self.finalData[name] = curValues[i];
                    break;
                }
            }
            else
            {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }

        });

        return _self.finalData;
    }

}
/************************/


// creating class instance
var BansWidget = new BansWidgetClass();

// creating object
var BansWidgetObject = {
    // important
    Name: 'Bans',

    Widget: BansWidget
}

// registering widget object
_core.addWidget(BansWidgetObject);