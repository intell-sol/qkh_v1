﻿// FamiltyRelative widget

/*****Main Function******/
var FamiltyRelativeWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.modal = $('.modal-familtyrelative');
    _self.modalUrl = "/_Popup_/Get_FamilyRelatives/";
    _self.viewUrl = "/_Popup_Views_/Get_FamilyRelatives/";
    _self.searchByDocUrl = "/_Data_Main_/CheckEmployeeByDocNumber";
    _self.searchByNameUrl = "/_Data_Main_/CheckEmployee";
    _self.currentPerson = null;
    _self.finalData = {};
    _self.response = {};
    _self.response.status = false;
    _self.response.data = null;
    _self.type = null;
    _self.PrisonerID = null;
    _self.mode = null;
    _self.id = null;
    _self.getItems = {};
    _self.checkItem = {};

    _self.getClass = {};
    _self.getClass['spouse'] = 'spouseLoader';
    _self.getClass['children'] = 'childrenLoader';
    _self.getClass['parent'] = 'parentLoader';
    _self.getClass['systerbrother'] = 'systerLoader';
    _self.getClass['otherrelative'] = 'otherLoader';

    
    // get list
    _self.addFamilyURL = {};
    _self.addFamilyURL['spouse'] = '/_Data_Family_/AddSpouse';
    _self.addFamilyURL['children'] = '/_Data_Family_/AddChildren';
    _self.addFamilyURL['parent'] = '/_Data_Family_/AddParent';
    _self.addFamilyURL['systerbrother'] = '/_Data_Family_/AddSysterBrother';
    _self.addFamilyURL['otherrelative'] = '/_Data_Family_/AddOtherRelative';

    // get list
    _self.editFamilyURL = {};
    _self.editFamilyURL['spouse'] = '/_Data_Family_/EditSpouse';
    _self.editFamilyURL['children'] = '/_Data_Family_/EditChildren';
    _self.editFamilyURL['parent'] = '/_Data_Family_/EditParent';
    _self.editFamilyURL['systerbrother'] = '/_Data_Family_/EditSysterBrother';
    _self.editFamilyURL['otherrelative'] = '/_Data_Family_/EditOtherRelative';
    
    // remove button
    _self.remRelative = {};
    _self.remRelative['spouse'] = '/_Data_Family_/RemSpouse';
    _self.remRelative['children'] = '/_Data_Family_/RemChildren';
    _self.remRelative['parent'] = '/_Data_Family_/RemParent';
    _self.remRelative['systerbrother'] = '/_Data_Family_/RemSysterBrother';
    _self.remRelative['otherrelative'] = '/_Data_Family_/RemOtherRelative';

    // action list
    _self.actionList = {
    };

    _self.init = function () {
        console.log('FamiltyRelative Widget Inited'); 

        _self.getItems = {};
        _self.checkItem = {};
        _self.finalData = {};
        _self.type = null;
        _self.PrisonerID = null;
        _self.mode = null;
        _self.id = null;
    };

    // add action
    _self.Add = function (type, PrisonerID, callback) {
        // initilize
        _self.init();

        console.log('FamiltyRelative Add called');

        // save callback
        _self.callback = callback;

        _self.PrisonerID = PrisonerID

        _self.mode = 'Add';

        _self.type = type;

        // loading view
        loadView(_self.modalUrl);
    };
    // view action
    _self.View = function (id, type, PrisonerID, callback)
    {
        // initilize
        _self.init();

        console.log('FamiltyRelative View called');

        // save callback
        _self.callback = callback;

        _self.id = id;

        _self.type = type;

        _self.PrisonerID = PrisonerID;

        _self.mode = 'View';

        // loading view
        loadView(_self.viewUrl);
    };

    // edit action
    _self.Edit = function (id, type, PrisonerID, callback) {
        // initilize
        _self.init();

        console.log('FamiltyRelative Edit called');

        // save callback
        _self.callback = callback;

        _self.id = id;

        _self.type = type;

        _self.PrisonerID = PrisonerID;

        _self.mode = 'Edit';

        // loading view
        loadView(_self.modalUrl);
    };

    // edit action
    _self.Remove = function (id, type, callback) {
        // initilize
        _self.init();
        
        console.log('FamiltyRelative Remove called');

        // save callback
        _self.callback = callback;

        _self.id = id;

        _self.type = type;

        // removing relative
        remRelative(_self.id, _self.type);
    };

    // loading modal from Views
    function loadView(url) {
        
        var data = {};
        
        if (_self.mode == 'Edit'||_self.mode=='View') {
            data.id = _self.id;
        }

        data.type = _self.type;

        data.PrisonerID = _self.PrisonerID;

        // post Service
        _core.getService('Post').Service.postPartial(data,url, function (res) {

            _core.getService('Loading').Service.disableBlur('loaderClass');

            // save content for later usage
            _self.content = res;

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // edit events
            if (_self.mode == 'Edit') {
                editModalEvents();
            }

            // showing modal
            $('.modal-familtyrelative').modal('show');
        });

    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons
        var findPersonBrn = $('#findperson');
        var saveBtn = $('#acceptBtnModal');
        var birthDate = $('#modal-person-bd');
        var backBtn = $('#backbutton');
        var nextBtn = $('#nextbutton');

        _self.getItems[_self.type] = $('.getItems-' + _self.type);
        _self.checkItem[_self.type] = $('.checkValidate-' + _self.type);

        // check validate and toggle accept button
        _self.checkItem[_self.type].unbind();
        _self.checkItem[_self.type].bind('change keyup', function () {
            var action = false;

            _self.checkItem[_self.type].each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            if (_self.mode == 'Add') {
                if (action == false) action = !_core.getWidget('FindPerson').Widget.isValidData();
            }

            $('#acceptBtnModal').attr('disabled', action);
        });

        nextBtn.unbind();
        nextBtn.bind('click', function () {
            $('.part1').addClass('hidden'); 
            $('.part3').addClass('hidden');
            $('#family-' + _self.type).removeClass('hidden');
            backBtn.attr('disabled', false);
            nextBtn.attr('disabled', true);
            _self.checkItem[_self.type].trigger('change');
        });

        backBtn.unbind();
        backBtn.bind('click', function () {
            $('.part1').removeClass('hidden');
            $('.part3').addClass('hidden');
            backBtn.attr('disabled', true);
            nextBtn.attr('disabled', false);
        });

        // daterangepicker
        birthDate.unbind();
        birthDate.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        birthDate.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
        });
        birthDate.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
        });

        _core.getWidget('FindPerson').Widget.BindPersonSearch(function (status) {
            if (status) {
                if (_self.type == 'systerbrother' || _self.type == 'parent')
                    saveBtn.attr('disabled', false)
                else {
                    nextBtn.attr('disabled', false);
                    _self.checkItem[_self.type].trigger('change');
                }
            }
            else {
                if (_self.type == 'systerbrother' || _self.type == 'parent')
                    saveBtn.attr('disabled', true)
                else
                    nextBtn.attr('disabled', true);
            }
        });

        // save button
        saveBtn.unbind();
        saveBtn.bind('click', function () {
            
                var data = collectData();

                if (_self.mode == 'Add') {
                    data['Data']['PrisonerID'] = _self.PrisonerID;
                }
                else if (_self.mode == 'Edit') {
                    data['Data']['ID'] = _self.id;
                }

                // adding or editing
                saveFamiltyRelative(data, _self.type);

                // hide modal
                $('.modal-familtyrelative').modal('hide');
        });

        // selectize
        $(".selectizePopup").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // edit modal events
    function editModalEvents() {

        // remove next button disabled
        $('#nextbutton').attr('disabled', false);

        // collapse table
        var collapseTable = $(".collapseTable");
        var collapseTableTr = collapseTable.find("tbody tr:not(.collapse)");

        collapseTableTr.click(function () {
            if ($(this).next("tr.collapse").hasClass("in")) {
                $(this).parents(".collapseTable").find("tr.collapse.in").removeClass("in");
            }
            else {
                $(this).parents(".collapseTable").find("tr.collapse.in").removeClass("in");
                $(this).parents(".collapseTable").find("input[name='modal-person-radio']").prop("checked", false);
                $(this).next("tr.collapse").addClass("in");
                $(this).find("input[name='modal-person-radio']").prop("checked", true);
            }
        });

        collapseTableTr.trigger('click');

        // disable fileds editing
        //$('#modal-person-name-1').attr('disabled', true);
        //$('#modal-person-name-2').attr('disabled', true);
        //$('#modal-person-name-3').attr('disabled', true);
        //$('#modal-person-bd').attr('disabled', true);
        //$('#modal-person-social-id').attr('disabled', true);
        //$('#modal-person-passport').attr('disabled', true);
    }

    // saving relative to database
    function saveFamiltyRelative(data, type) {
        
        var postService = _core.getService('Post').Service;

        var url = _self.addFamilyURL[type];

        if (_self.mode == 'Edit') {
            url = _self.editFamilyURL[type];
        }

        _core.getService('Loading').Service.enableBlur(_self.getClass[type]);

        postService.postJson(data, url, function (res) {
            _core.getService('Loading').Service.disableBlur(_self.getClass[type]);

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding or editing relative of type ' + type);
        });
    }

    // remove relative
    function remRelative(id, type) {

        var data = {};
        data.ID = id;

        _core.getService('Loading').Service.enableBlur('loaderClass');

        // yes or no widget
        _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {
            if (res) {

                // postService
                var postService = _core.getService('Post').Service;

                _core.getService('Loading').Service.enableBlur(_self.getClass[type]);

                postService.postJson(data, _self.remRelative[type], function (res) {

                    _core.getService('Loading').Service.disableBlur(_self.getClass[type]);

                    if (res != null) {
                        _self.callback(res);
                    }
                    else alert('serious error on removing relative of type ' + type);
                });
            }
        });
    }

    // collect data
    function collectData() {

        var data = {};

        _self.getItems[_self.type].each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            // appending to Data
            data[name] = $(this).val();
        });

        if (_self.mode == 'Edit') {
            data.ID = _self.ID;
        }

        var personData = _core.getWidget('FindPerson').Widget.getPersonData();

        _self.finalData['Data'] = data;
        _self.finalData['Data']['Person'] = personData.Person;

        return _self.finalData;
    }

    // get person from police database
    function getPerson() {

        // data to send to server
        var data = {};

        // hide main row for loader show (it is new user search)
        $('#foundusertable').addClass('hidden');
        $('#foundusererrorp').addClass('hidden');

        // making user form save button disabled
        $('#acceptBtnModal').attr('disabled', true);

        // post service
        var postService = _core.getService('Post').Service;

        // loader
        var loadingService = _core.getService('Loading').Service;
        loadingService.enable('searchuserloader', 'foundusertable');

        // getting fields
        var firstname = $('#modal-person-name-1').val();
        var lastname = $('#modal-person-name-2').val();
        var bday = $('#modal-person-bd').val();
        var passport = $('#modal-person-passport').val();
        var socId = $('#modal-person-social-id').val();

        // first search by socid
        if (socId != '' && socId != null) {
            data['DocNumber'] = socId;
            data['Type'] = false;
            postService.postJson(data, _self.searchByDocUrl, function (res) {
                matchPersonDataToTable(res);
            })
        }
            // else search by pasport
        else if (passport != '' && passport != null) {
            data['DocNumber'] = passport;
            data['Type'] = true;
            postService.postJson(data, _self.searchByDocUrl, function (res) {
                matchPersonDataToTable(res);
            });
        }
            // then search by firstname lastname bday
        else if (firstname != '' && lastname != '' && bday != '') {
            data['FirstName'] = firstname;
            data['LastName'] = lastname;
            data['BirthDate'] = bday;
            postService.postJson(data, _self.searchByNameUrl, function (res) {
                matchPersonDataToTable(res);
            })
        }
        else {
            // disable loader
            var loadingService = _core.getService('Loading').Service;
            loadingService.disable('searchuserloader', 'foundusertable', false);
        }
    }

    // match data to table
    function matchPersonDataToTable(status) {

        // if user found
        if (typeof status != 'undefined' && status != null) {

            if (status['status'] == 'error') {

                // current person is null
                _self.currentPerson = null;

                // hide table and show error <p>
                $('#foundusertable').addClass('hidden');
                $('#foundusererrorp').removeClass('hidden');
                $('#foundusererrorp').html('Որոնման արդյունքում ոչ մի անձ չի գտնվել');
            }
            else if (status['status'] == 'success') {

                // current person
                _self.currentPerson = status;

                $('#nextbutton').attr('disabled', false);

                $('#backbutton').attr('disabled', true);

                // toggle message table and <p>
                $('#foundusertable').removeClass('hidden');
                $('#foundusererrorp').addClass('hidden');

                // bind values to table
                drawUserList(status);

                // draw fields
                drawUserFields(status, false);

                // bind collapse table info
                bindCollapseTable();
            }

            // disable loader
            var loadingService = _core.getService('Loading').Service;
            loadingService.disable('searchuserloader', 'foundusertable', false);
        }
        else {
            console.error('server returned null from server while searching for user')
        }
    }

    // draw user table row
    function drawUserList(user) {
        //console.log(user);
        var tbody = $('#searchuserinfoshort');

        // empty
        tbody.empty();

        curHtml = '';
        curHtml += '<tr><td data-title="#">';
        curHtml += '0</td>';
        curHtml += '<td data-title="Անուն Ազգանուն Հայրանուն">' + user.FirstName + ' ' + user.MiddleName + ' ' + user.LastName + '</td>';
        curHtml += '<td data-title="Ծննդ. ամսաթիվ">' + user.BirthDate + '</td>';
        curHtml += '<td data-title="Սոց. քարտ">' + user.SocialCardNumber + '</td>';
        curHtml += '<td data-title="Անձնագիր">' + user.PassportNumber + '</td></tr>';

        var reg = user.RegistrationAddress;
        var registerationData = reg.Registration_Region + ', ' + reg.Registration_Community + ' ' + (reg.Registration_Apartment != null ? reg.Registration_Apartment : "") + ', ' + reg.Registration_Street + ' ' + reg.Registration_Building_Type + ' ' + reg.Registration_Building;
        reg = user.LivingAddress;
        var livingData = reg.Registration_Region + ', ' + reg.Registration_Community + ' ' + (reg.Registration_Apartment != null ? reg.Registration_Apartment : "") + ', ' + reg.Registration_Street + ' ' + reg.Registration_Building_Type + ' ' + reg.Registration_Building;

        if (user.Gender == "F") user.GenderName = 'Իգական';
        else user.GenderName = 'Արական';

        curHtml += ' <tr class="collapse"><td colspan="5"> <div class="row"> <div class="col-xs-12 col-md-4"><img class="img-responsive" src="' + user.PhotoLink + '" alt="" /></div> <div class="col-xs-12 col-md-8">';
        curHtml += ' <div class="form-group"> <label>Անձնագիր՝</label> <p>' + user.PassportNumber + ', ' + user.PassportIssuanceDate + ', ' + user.PassportDepartment + '</p> </div> <div class="form-group"> <label>Ազգություն՝</label> <p>' + user.NationalityLabel + '</p> </div> <div class="form-group"> <label>Քաղաքացիությւն՝</label> <p>' + user.Citizenship + '</p> </div> <div class="form-group"> <label>Սեռ՝</label> <p>' + user.GenderName + '</p> ';
        curHtml += '</div> <div class="form-group"> <label>Գրանցման հասցե՝</label> <p>' + registerationData + '</p> </div> <div class="form-group"> <label>Բնակության հասցե՝</label> <p>' + livingData + '</p> </div> </div> </div> </td> </tr>';

        tbody.append(curHtml);
    }

    // draw user fields
    function drawUserFields(user, disabled) {

        // fiving values
        $('#modal-person-name-1').val(user.FirstName);
        $('#modal-person-name-2').val(user.MiddleName);
        $('#modal-person-name-3').val(user.LastName);
        $('#modal-person-bd').val(typeof user.BirthDate != 'undefined' ? user.BirthDate : user.BirthDay);
        $('#modal-person-social-id').val(typeof user.SocialCardNumber != 'undefined' ? user.SocialCardNumber : user.PSN);
        $('#modal-person-passport').val(typeof user.PassportNumber != 'undefined' ? user.PassportNumber : user.Passport);

        // disabling or enabling
        $('#modal-person-name-1').attr('disabled', disabled);
        $('#modal-person-name-2').attr('disabled', disabled);
        $('#modal-person-name-3').attr('disabled', disabled);
        $('#modal-person-bd').attr('disabled', disabled);
        $('#modal-person-social-id').attr('disabled', disabled);
        $('#modal-person-passport').attr('disabled', disabled);
    }

    // bind table collapse
    function bindCollapseTable() {

        // collapse table
        var collapseTable = $(".collapseTable");
        var collapseTableTr = collapseTable.find("tbody tr:not(.collapse)");

        collapseTableTr.click(function () {
            if ($(this).next("tr.collapse").hasClass("in")) {
                $(this).parents(".collapseTable").find("tr.collapse.in").removeClass("in");
            }
            else {
                $(this).parents(".collapseTable").find("tr.collapse.in").removeClass("in");
                $(this).parents(".collapseTable").find("input[name='modal-person-radio']").prop("checked", false);
                $(this).next("tr.collapse").addClass("in");
                $(this).find("input[name='modal-person-radio']").prop("checked", true);
            }
        });

        collapseTableTr.trigger('click');

    }
}
/************************/


// creating class instance
var FamiltyRelativeWidget = new FamiltyRelativeWidgetClass();

// creating object
var FamiltyRelativeWidgetObject = {
    // important
    Name: 'FamiltyRelative',

    Widget: FamiltyRelativeWidget
}

// registering widget object
_core.addWidget(FamiltyRelativeWidgetObject);