﻿// Add OfficialVisits widget

/*****Main Function******/
var OfficialVisitsWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.PrisonerID = null;
    _self.id = null;
    _self.mode = null;
    _self.PersonList = [];
    _self.currentPersonalID = '';
    _self.currentPerson = null;
    _self.isPersonValid = false;

    _self.drawPersonListURL = "/_Popup_/Draw_OfficialVisitPersonTableRow";
    _self.modalUrl = "/_Popup_/Get_OfficialVisit";
    _self.viewUrl = "/_Popup_Views_/Get_OfficialVisit";
    _self.addUrl = '/OfficialVisits/AddData';
    _self.editUrl = '/OfficialVisits/EditData';
    _self.removeUrl = '/OfficialVisits/RemData';
    _self.completeUrl = '/OfficialVisits/CompleteData';
    _self.ModalName = 'gov-body-visit-add-modal';
    _self.finalData = {};
    
    _self.init = function () {
        console.log('Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.isPersonValid = false;
        _self.getItems = null;
        _self.PrisonerID = null;
        _self.id = null;
        _self.mode = null;
        _self.PersonList = [];
        _self.currentPersonalID = '';
    };

    // add action
    _self.Add = function (PrisonerID, callback) {

        // initilize
        _self.init();

        console.log('Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = 'Add';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    }
    _self.View = function (id, callback)
    {
        // initilize
        _self.init();

        console.log('View called');

        _self.id = id;

        _self.mode = 'View';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewUrl);
    };
    // edit action
    _self.Edit = function (id, callback) {
        // initilize
        _self.init();

        console.log('Edit called');

        _self.id = id;

        _self.mode = 'Edit';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // edit action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        removeData();
    };

    // edit action
    _self.Complete = function (id,url, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        CompleteData();
    };

    // loading modal from Views
    function loadView(url) {

        var data = {};

        if (_self.id != null &&( _self.mode == "Edit"|| _self.mode == "View")) {
            data.ID = _self.id;
        }

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _core.getService('Loading').Service.disableBlur('loaderClass');

            _self.content = res;

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.'+_self.ModalName).modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnModal');
        var addPersonBtn = $('#addPersonBtnModal');
        var findPersonAgainBtn = $('#findpersonAgain');
        var nextBtn = $('#nextBtnModal');
        var prevBtn = $('#prevBtnModal');
        var checkItemsFirstPart = $('.checkValidateFirstPartModal');
        var checkItemsSecondPart = $('.checkValidateSecondPartModal');
        var PositionLibItemElem = $('#PositionLibItemID');
        var dates = $('.DatesModal');
        _self.getItems = $('.getItemsFirstPartModal');

        // unbind
        acceptBtn.unbind();
        checkItemsFirstPart.unbind();
        checkItemsSecondPart.unbind();

        // check validate and toggle accept button
        checkItemsFirstPart.bind('change keyup', function () {
            var action = false;

            checkItemsFirstPart.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            //if (_self.mode == 'Add')
                nextBtn.attr('disabled', action);

            var lenghtCheck = 1;
            if (_self.mode == 'Add') {
                if (collectPersonList().length == 0) action = true;
            }
            
            acceptBtn.attr("disabled", action);
        });

        // check validate and toggle accept button
        checkItemsSecondPart.bind('change keyup', function () {
            var action = false;

            checkItemsSecondPart.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            if (action == false && _self.isPersonValid) {
                addPersonBtn.attr('disabled', action);
            }
            else {
                addPersonBtn.attr('disabled', true);
            }
        });

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            timePicker: true,
            timePicker24Hour: true,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(true),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY HH:mm:ss"));
            checkItemsFirstPart.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            checkItemsFirstPart.trigger('change');
        });
        dates.each(function () {
            if ($(this).val() == "")
                $(this).data('daterangepicker').setStartDate(new Date($.now()));
            else {
                function dateString2Date(dateString) {
                    var dt = dateString.split(/\/|\s/);
                    return new Date(dt.slice(0, 3).reverse().join('/') + ' ' + dt[3]);
                }
                var dateString = $(this).val();
                dateString = dateString2Date(dateString);
                $(this).data('daterangepicker').setStartDate(dateString);
            }
        })

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == 'Add') {

                data.PrisonerID = _self.PrisonerID;
                
                // add
                addData(data);
            }
            else if (_self.mode == 'Edit') {

                data.ID = _self.id;

                // edit
                editData(data);
            };

            //hiding modal
            $('.'+_self.ModalName).modal('hide');
        });

        // bind next
        nextBtn.unbind().click(function () {
            $('.part1').addClass('hidden');
            $('.part2').removeClass('hidden');
            checkItemsSecondPart.trigger('change');
            nextBtn.attr('disabled', true);
            prevBtn.attr('disabled', false);
        });

        // bind prev
        prevBtn.unbind().click(function () {
            $('.part1').removeClass('hidden');
            $('.part2').addClass('hidden');
            checkItemsFirstPart.trigger('change');
            nextBtn.attr('disabled', false);
            prevBtn.attr('disabled', true);
        });

        // bind add Person button
        addPersonBtn.unbind().click(function () {

            if (_self.isPersonValid && PositionLibItemElem.val() != '') {
                var data = {};
                var PositionLibItemID = $('#PositionLibItemID')[0].selectize;
                data.PositionLibItemID = parseInt(PositionLibItemElem.val());
                data.PositionLibItemName = PositionLibItemID.getItem(PositionLibItemID.getValue())[0].innerHTML
                data.Person = _core.getWidget('FindPerson').Widget.getPersonData().Person;

                // draw added personlist
                drawPersonList(data);
            }
            else {
                console.warn('some serious warning about adding person to list');
            }
            checkItemsFirstPart.trigger('change');
        });
        
        findPersonAgainBtn.unbind().click(function () {

            // clean search
            cleanSearch();

            // trigger of change
            checkItemsSecondPart.trigger('change');
        });

        // trigger change
        //_self.getItems.bind('change keyup', function () {
        //    checkItems.trigger('change');
        //});

        // bind find person part to FindPerson Widget
        _core.getWidget('FindPerson').Widget.BindPersonSearch(function (status) {
            _self.isPersonValid = status;
            
            checkItemsFirstPart.trigger('change');
            checkItemsSecondPart.trigger('change');
        });

        // selectize
        $(".selectizeModal").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // bind person list events
    function bindPersonListEvents() {
        var removeRow = $('.removePersonListRow');

        removeRow.unbind().click(function () {
            $(this).parent().parent().remove();
        });
    }

    // add
    function addData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.addUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding');
        })
    }

    // edit
    function editData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.editUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing');
        })
    }

    // remove
    function removeData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.removeUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on removing');
        })
    }

    // remove
    function CompleteData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.completeUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on completing');
        })
    }

    // collect data
    function collectData() {

        // array of violation list
        //_self.finalData["ViolationList"] = [];

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            if (name == "ViolationList") {
                //var tempValue = $(this).val();
                //for (var i in tempValue) {
                //    var temp = {};
                //    temp["LibItemID"] = parseInt(tempValue[i]);
                //    _self.finalData[name].push(temp);
                //}
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }

        });

        if (_self.mode == "Add") {
            _self.finalData['VisitorList'] = collectPersonList();
        }
        else {
            _self.finalData['PositionLibItemID'] = $('#PositionLibItemID')[0].selectize.getValue();
            _self.finalData['Person'] = _core.getWidget('FindPerson').Widget.getPersonData().Person;
        }

        return _self.finalData;
    }

    // remove soc id from list
    function removePerson(data) {
        for (var i in _self.PersonList) {
            if (parseInt(_self.PersonList[i].FirstName) == parseInt(data.FirstName)) {
                _self.PersonList.splice(i, 1);
            }
        }
    }

    // clean search
    function cleanSearch() {

        // hide main row for loader show (it is new user search)
        $('#foundusertable').addClass('hidden');
        $('#foundusererrorp').addClass('hidden');

        // disableing or enabling fields
        $('#modal-person-name-1').val('');
        $('#modal-person-name-2').val('');
        $('#modal-person-name-3').val('');
        $('#modal-person-bd').val('');
        $('#modal-person-social-id').val('');
        $('#modal-person-passport').val('');

        $('#modal-person-name-1').attr('disabled', false);
        $('#modal-person-name-2').attr('disabled', false);
        $('#modal-person-name-3').attr('disabled', false);
        $('#modal-person-bd').attr('disabled', false);
        $('#modal-person-social-id').attr('disabled', false);
        $('#modal-person-passport').attr('disabled', false);

        _self.currentPersonalID = null;
    }

    // draw person list
    function drawPersonList(data) {

        var curHtml = "<tr class='getPersonListRow' data-id='' data-person='" + JSON.stringify(data.Person) + "' data-positionlibitemid='" + data.PositionLibItemID + "'>";
        curHtml += "<td>"+data.Person.FirstName+" " +data.Person.LastName+" "+ data.Person.MiddleName+ "</td>";
        curHtml += "<td>"+((typeof data.Person.BirthDay != "undefined") ? data.Person.BirthDay : "")+"</td>";
        curHtml += "<td>"+((typeof data.Person.Personal_ID != "undefined") ? data.Person.Personal_ID : "-")+"</td>";
        curHtml += "<td>" + ((data.Person.IdentificationDocuments.length != 0) ? data.Person.IdentificationDocuments[0].Number : "") + "</td>";
        curHtml += "<td>" + data.PositionLibItemName + "</td>";
        curHtml += '<td><button data-id="" class="btn btn-sm btn-default btn-flat removePersonListRow" title="Հեռացնել"><i class="fa fa-trash-o" ></i></button></td>';
        curHtml += "</tr>";
        $('#personListTableBody').append(curHtml);

        // bind draw PersonList events
        bindPersonListEvents();
    }

    // collect person list data
    function collectPersonList() {
        var list = [];

        $('.getPersonListRow').each(function () {
            try{
                var data = {};
                data.ID = $(this).data('id');
                data.Person = $(this).data('person');
                data.PositionLibItemID = $(this).data('positionlibitemid');
                list.push(data);
            } catch (e) {
                console.error(e);
            }
        });

        return list;
    }

}
/************************/


// creating class instance
var OfficialVisitsWidget = new OfficialVisitsWidgetClass();

// creating object
var OfficialVisitsWidgetObject = {
    // important
    Name: 'OfficialVisits',

    Widget: OfficialVisitsWidget
}

// registering widget object
_core.addWidget(OfficialVisitsWidgetObject);