﻿// Add PersonalVisits widget

/*****Main Function******/
var PackagesWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.PrisonerID = null;
    _self.id = null;
    _self.mode = null;
    _self.getItemsContent = null;
    _self.ContentList = [];
    _self.currentPersonalID = '';
    _self.acceptdate = null;
    _self.currentPerson = null;

    _self.drawListURL = "/_Popup_/Draw_PackageContentTableRow";
    _self.modalUrl = "/_Popup_/Get_Package";
    _self.viewUrl = "/_Popup_Views_/Get_Package";
    _self.addUrl = '/Packages/AddData';
    _self.editUrl = '/Packages/EditData';
    _self.removeUrl = '/Packages/RemData';
    _self.completeUrl = '/Packages/CompleteData';
    _self.ModalName = 'package-add-modal';
    _self.declineUrl = '/Packages/DeclineData';
    _self.approveUrl = '/Packages/ApproveData';
    _self.drawContentListURL = "/_Popup_/Draw_PackageContentTableRow";
    _self.finalData = {};

    
    _self.init = function () {
        console.log('Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.PrisonerID = null;
        _self.acceptdate = null;
        _self.id = null;
        _self.mode = null;
        _self.ContentList = [];
        _self.currentPersonalID = '';
    };

    // add action
    _self.Add = function (PrisonerID, callback) {

        // initilize
        _self.init();

        console.log('Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = 'Add';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    }
    // view action
    _self.View = function (id, callback)
    {
        // initilize
        _self.init();

        console.log('View called');

        _self.id = id;

        _self.mode = 'View';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewUrl);
    };

    // edit action
    _self.Edit = function (id, callback) {
        // initilize
        _self.init();

        console.log('Edit called');

        _self.id = id;

        _self.mode = 'Edit';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // remove action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        removeData();
    };

    // complete action
    _self.Complete = function (id, data, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;
        _self.acceptdate = data;

        // save callback
        _self.callback = callback;

        // remove
        CompleteData();
    };

    // decline action
    _self.Decline = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        DeclineData();
    };

    // approve action
    _self.Approve = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        ApproveData();
    };
    // decline action
 
    // loading modal from Views
    function loadView(url) {

        var data = {};

        if (_self.id != null && (_self.mode == "Edit"||_self.mode=="View")) {
            data.ID = _self.id;
        }

        data.PrisonerID = _self.PrisonerID;

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _core.getService('Loading').Service.disableBlur('loaderClass');

            _self.content = res;

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.'+_self.ModalName).modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnModal');
        var addListBtn = $('#addListBtnModal');
        var findPersonAgainBtn = $('#findpersonAgain');
        var nextBtn = $('#nextBtnModal');
        var prevBtn = $('#prevBtnModal');
        var checkItemsFirstPart = $('.checkValidateFirstPartModal');
        var checkItemsSecondPart = $('.checkValidateSecondPartModal');
        var checkValidateContent = $('.checkValidateContent');
        _self.getItemsContent = $('.getItemsContent');
        var PositionLibItemElem = $('#PositionLibItemID');
        var dates = $('.DatesModal');
        _self.getItems = $('.getItemsSecondPartModal');

        // unbind
        acceptBtn.unbind();
        checkItemsSecondPart.unbind();
        checkItemsFirstPart.unbind();

        // check validate and toggle accept button
        checkItemsSecondPart.bind('change keyup', function () {
            var action = false;

            checkItemsSecondPart.each(function () {
                // trigger relative list change
                if (($(this).val() == '' || $(this).val() == null) && $(this).attr('id') != "ContentList") {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            if (action == false) { //  && _self.ContentList.length != 0
                acceptBtn.attr("disabled", action);
            }
            else {
                acceptBtn.attr('disabled', true);
            }
        });

        // check validate and toggle accept button
        checkItemsFirstPart.bind('change keyup', function () {
            var action = false;

            checkItemsFirstPart.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            if (action == false && _self.currentPersonalID != null && _self.currentPersonalID != '') {
                nextBtn.attr('disabled', action);
            }
            else {
                nextBtn.attr('disabled', true);
            }
        });

        // check content buttons
        checkValidateContent.bind('change keyup', function () {
            var action = false;

            checkValidateContent.each(function () {
                if (($(this).val() == '' || $(this).val() == null)) {
                    action = true;
                }
            });

            addListBtn.attr('disabled',action);
        });

        // date range picker
        _core.getService("Date").Service.Bind(dates, true);

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == 'Add') {

                data.PrisonerID = _self.PrisonerID;
                
                // add
                addData(data);
            }
            else if (_self.mode == 'Edit') {

                data.ID = _self.id;

                // edit
                editData(data);
            };

            //hiding modal
            $('.'+_self.ModalName).modal('hide');
        });

        // bind next
        nextBtn.unbind().click(function () {
            $('.part1').addClass('hidden');
            $('.part2').removeClass('hidden');
            checkItemsFirstPart.trigger('change');
            nextBtn.attr('disabled', true);
            prevBtn.attr('disabled', false);
        });

        // bind prev
        prevBtn.unbind().click(function () {
            $('.part1').removeClass('hidden');
            $('.part2').addClass('hidden');
            checkItemsFirstPart.trigger('change');
            nextBtn.attr('disabled', false);
            prevBtn.attr('disabled', true);
        });

        // bind add Person button
        addListBtn.unbind().click(function () {
            
            addListBtn.attr('disabled', true);

            var data = getContentData();

            // draw added ContentList
            drawContentList(data);

            checkItemsSecondPart.trigger('change');
        });
        

        // trigger change
        //_self.getItems.bind('change keyup', function () {
        //    checkItems.trigger('change');
        //});

        _core.getWidget('FindPerson').Widget.BindPersonSearch(function (status) {
            if (status) {
                nextBtn.attr('disabled', false);
            }
            else {
                nextBtn.attr('disabled', true);
            }
        });

        // selectize
        $(".selectizeModal").each(function () {
            $(this).selectize({
                create: false
            });
        });

        // content table row events
        bindContentListEvents();
        
    }

    // bind person list events
    function bindContentListEvents() {
        var removeRow = $('.removeContentListRow');

        removeRow.unbind().click(function () {
            
            $(this).parent().parent().remove();
        });
    }

    // add
    function addData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.addUrl, function (res) {

            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding');
        })
    }

    // edit
    function editData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.editUrl, function (res) {
        
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing');
        })
    }

    // remove
    function removeData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.removeUrl, function (res) {
            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on removing');
        })
    }

    // complete
    function CompleteData() {

        var data = {};
        data.ID = _self.id;
        data.AcceptDate = _self.acceptdate;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.completeUrl, function (res) {
            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on completing');
        })
    }

    // collect data
    function collectData() {

        // array of violation list
        //_self.finalData["ViolationList"] = [];

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            if (name == "ViolationList") {
                //var tempValue = $(this).val();
                //for (var i in tempValue) {
                //    var temp = {};
                //    temp["LibItemID"] = parseInt(tempValue[i]);
                //    _self.finalData[name].push(temp);
                //}
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }

        });

        _self.finalData['ContentList'] = collectContentData();
        _self.finalData.Person = _core.getWidget('FindPerson').Widget.getPersonData().Person;

        return _self.finalData;
    }

    // collect content data
    function collectContentData() {
        var list = [];
        $('.getContentListRow').each(function () {
            var data = {};

            data.TypeLibItemID = $(this).data('typelibitemid');
            data.TypeLibItemLabel = $(this).data('typelibitemlabel');

            data.GoodsLibItemID = $(this).data('goodslibitemid');
            data.GoodsLibItemLabel = $(this).data('goodslibitemlabel');

            data.MeasureLibItemID = $(this).data('measurelibitemid');
            data.MeasureLibItemLabel = $(this).data('measurelibitemlabel');

            data.WeightCount = $(this).data('weightcount');

            list.push(data);
        });
        return list;
    }

    // get content data
    function getContentData() {
        var data = {};

        var TypeLibItemID = $("#ContentTypeLibItemID")[0].selectize;
        var GoodsLibItemID = $("#GoodsLibItemID")[0].selectize;
        var MeasureLibItemID = $("#MeasureLibItemID")[0].selectize;
        var WeightCount = $("#WeightCount");
        

        data.TypeLibItemID = TypeLibItemID.getValue();
        data.TypeLibItemLabel = TypeLibItemID.getItem(TypeLibItemID.getValue())[0].innerHTML;

        data.GoodsLibItemID = GoodsLibItemID.getValue();
        data.GoodsLibItemLabel = GoodsLibItemID.getItem(GoodsLibItemID.getValue())[0].innerHTML;

        data.MeasureLibItemID = MeasureLibItemID.getValue();
        data.MeasureLibItemLabel = MeasureLibItemID.getItem(MeasureLibItemID.getValue())[0].innerHTML;

        data.WeightCount = WeightCount.val();

        return data;
    }

    // draw person list
    function drawContentList(data) {

        var curHtml = "<tr class='getContentListRow' data-weightcount='" + data.WeightCount + "' data-measurelibitemlabel='" + data.MeasureLibItemLabel + "' data-measurelibitemid='" + data.MeasureLibItemID + "' data-goodslibitemlabel='" + data.GoodsLibItemLabel + "' data-goodslibitemid='" + data.GoodsLibItemID + "' data-typelibitemlabel='" + data.TypeLibItemLabel + "' data-typelibitemid='" + data.TypeLibItemID + "' data-id='' data-packageid='' >";
        curHtml += "<td>" + data.TypeLibItemLabel + "</td>";
        curHtml += "<td>" + data.GoodsLibItemLabel + "</td>";
        curHtml += "<td>" + data.MeasureLibItemLabel + "</td>";
        curHtml += "<td>" + data.WeightCount + "</td>";
        curHtml += '<td><button data-id="" class="btn btn-sm btn-default btn-flat removeContentListRow" title="Հեռացնել"><i class="fa fa-trash-o" ></i></button></td>';
        curHtml += "</tr>";

        $('#ContentListTableBody').append(curHtml);

        // bind draw ContentList events
        bindContentListEvents();
    }

    // approve
    function ApproveData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.approveUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on approving');
        })
    }

    // decline
    function DeclineData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.declineUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on declining');
        })
    }
}
/************************/


// creating class instance
var PackagesWidget = new PackagesWidgetClass();

// creating object
var PackagesWidgetObject = {
    // important
    Name: 'Packages',

    Widget: PackagesWidget
}

// registering widget object
_core.addWidget(PackagesWidgetObject);