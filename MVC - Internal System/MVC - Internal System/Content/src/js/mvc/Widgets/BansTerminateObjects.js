﻿// ban terminate widget

/*****Main Function******/
var BanTerminateWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.ID = null;

    _self.modalUrl = "/_Popup_/Get_BansTerminate";
    _self.updateTerminationURL = '/_Data_ban_/SaveTermination';
    _self.finalData = {};
    

    _self.init = function () {
        console.log('BanT Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null
        _self.ID = null;
    };

    // ask action
    _self.Show = function (id, callback) {
        // initilize
        _self.init();

        console.log('BanT Ask called');

        // save callback
        _self.callback = callback;

        // bind ID
        _self.ID = id;

        // loading view
        loadView();
    };

    // loading modal from Views
    function loadView() {
        if (_self.content == null || _self.ID != null) {

            var url = _self.modalUrl;
            if (_self.ID != null) url = url + '/' + _self.ID;

            $.get(url, function (res) {
                // save content for later usage
                _self.content = res;

                _core.getService('Loading').Service.disableBlur('loaderClass');

                // content
                $('#widgetHolder').html(_self.content);

                // bind events for yes and no
                bindEvents();

                // showing modal
                $('.ban-cancel-modal').modal('show');
            }, 'html');
        }
        else {
            _core.getService('Loading').Service.disableBlur('loaderClass');
            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.ban-cancel-modal').modal('show');

        }
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnbanT');
        var checkItems = $('.checkValidateBanT');
        var dates = $('.DatesBanT');
        _self.getItems = $('.getItemsBanT');

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        // bind custom select
        _core.getService("SelectMenu").Service.bindSelectEvents('TerminationVoroshoxLibItem_ID');

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            data.ID = _self.ID;
            
            // save termination data
            saveTemination(data);

            //hiding modal
            $('.ban-cancel-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else if (curName == 'TerminationVoroshoxLibItem_ID') {
                        if (_core.getService("SelectMenu").Service.collectData(curName).length == 0) action = true;
                    }
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // selectize
        $(".selectizeBanT").each(function () {
            $(this).selectize({
                create: false
            });
        });

        $('.customSelectInputTerminationVoroshoxLibItem_ID').bind('change', function () {
            checkItems.trigger('change');
        })
    }

    // save termination function
    function saveTemination(data) {

        //// logit
        //console.log(data);

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('addbantablebody');

        postService.postJson(data, _self.updateTerminationURL, function (res) {
            _core.getService('Loading').Service.disableBlur('addbantablebody');

            if (res != null) {

                // callback
                _self.callback(res);
            }
            else alert('serious eror on adding ban terminate');
        })

    }

    // collect data
    function collectData() {

        _self.getItems.each(function () {

            var TerminationVoroshoxLibItem_ID = $(this).hasClass('TerminationVoroshoxLibItem_ID');

            // name of Field (same as in Entity)
            var name = $(this).attr('name');
            
            if (TerminationVoroshoxLibItem_ID) {
                var curValues = _core.getService("SelectMenu").Service.collectData(name);
                for (var i in curValues) {
                    _self.finalData[name] = curValues[i];
                    break;
                }
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }
        });

        // bind ID
        _self.finalData['ID'] = _self.ID;

        return _self.finalData;
    }

}
/************************/


// creating class instance
var BanTerminateWidget = new BanTerminateWidgetClass();

// creating object
var BanTerminateWidgetObject = {
    // important
    Name: 'BanTerminate',

    Widget: BanTerminateWidget
}

// registering widget object
_core.addWidget(BanTerminateWidgetObject);