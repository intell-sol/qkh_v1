﻿// ItemsObjects widget

/*****Main Function******/
var ItemsObjectsWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.mode = null;
    _self.PrisonerID = null;
    _self.id = null;
    _self.modalUrl = "/_Popup_/Get_ItemsObjects";
    _self.viewUrl = "/_Popup_Views_/Get_ItemsObjects";
    _self.addItemUrl = '/_Data_Items_/AddItem';
    _self.editItemUrl = '/_Data_Items_/EditItem';
    _self.remItemURL = '/_Data_Items_/RemItem';
    _self.finalData = {};


    // action list
    _self.actionList = {
    };

    _self.init = function () {
        console.log('ItemsObjects Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null
        _self.PrisonerID = null;
        _self.id = null;
        _self.mode = null;
    };

    // ask action
    _self.Add = function (PrisonerID, callback) {

        // initilize
        _self.init();

        console.log('ItemsObjects Add called');

        _self.mode = 'Add';

        _self.PrisonerID = PrisonerID;

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };
    //view action
      _self.View = function (id, callback) {

        // initilize
        _self.init();

        console.log('ItemsObjects View called');

        _self.mode = 'View';

        // save callback
        _self.callback = callback;

        _self.id = id;

        // loading view
        loadView(_self.viewUrl);
    };
    // edit action
    _self.Edit = function (id, callback) {

        // initilize
        _self.init();

        console.log('ItemsObjects Add called');

        _self.mode = 'Edit';

        // save callback
        _self.callback = callback;

        _self.id = id;

        // loading view
        loadView(_self.modalUrl);
    };

    _self.Remove = function (id, callback) {

        // initilize
        _self.init();

        console.log('ItemsObjects Remove called');

        // save callback
        _self.callback = callback;

        // set id
        _self.id = id;

        // remove item
        removeItem();
    }

    // loading modal from Views
    function loadView(url) {
       
        if (_self.id != null && (_self.mode == "Edit" || _self.mode == "View")) url = url + '/' + _self.id;

            $.get(url, function (res) {

                _core.getService('Loading').Service.disableBlur('loaderClass');

                // save content for later usage
                _self.content = res;

                // content
                $('#widgetHolder').html(_self.content);

                // bind events for yes and no
                bindEvents();

                // showing modal
                $('.itemsobjects-modal').modal('show');
            }, 'html');       
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnItems');
        var checkItems = $('.checkValidateItems');
        _self.getItems = $('.getItemsItems');
        
    var dates = $('.Dates');
        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == 'Add') {

                // appending prisoner id to formdata
                data['PrisonerID'] = _self.PrisonerID;

                // add
                addItem(data);
            }
            else if (_self.mode == 'Edit') {

                // bind id
                data.ID = _self.id;

                // edit
                editItem(data);
            }

            //hiding modal
            $('.itemsobjects-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // selectize
        $(".selectizeItems").each(function () {
            $(this).selectize({
                create: false
            });
        });
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');

        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');

        });
    }

    // Add item to database
    function addItem(data) {

        //// logit
        //console.log(data);

        // post service
        var postService = _core.getService('Post').Service;
        _core.getService('Loading').Service.enableBlur('itemsobjectstablebody');


        postService.postJson(data, _self.addItemUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('itemsobjectstablebody');

            if (res.status) {
                _self.callback(res);
            }
            else alert('serious eror on adding item');
        })
    }

    // Edit item
    function editItem(data) {

        //// logit
        //console.log(data);

        // post service
        _core.getService('Loading').Service.enableBlur('itemsobjectstablebody');

        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.editItemUrl, function (res) {
        _core.getService('Loading').Service.disableBlur('itemsobjectstablebody');

            if (res.status) {
                _self.callback(res);
            }
            else alert('serious eror on adding item');
        })
    }

    // remove item
    function removeItem() {

        var data = {};
        data['ID'] = _self.id;

        _core.getService('Loading').Service.enableBlur('itemsobjectstablebody');

        // postService
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.remItemURL, function (res) {
        _core.getService('Loading').Service.disableBlur('itemsobjectstablebody');

            if (res != null) {
                _self.callback(res);
            } else {
                alert('seroius error on removing item');
            }
        })
    }

    // collect data
    function collectData() {

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            // appending to Data
            _self.finalData[name] = $(this).val();
        });

        return _self.finalData;
    }

}
/************************/


// creating class instance
var ItemsObjectsWidget = new ItemsObjectsWidgetClass();

// creating object
var ItemsObjectsWidgetObject = {
    // important
    Name: 'ItemsObjects',

    Widget: ItemsObjectsWidget
}

// registering widget object
_core.addWidget(ItemsObjectsWidgetObject);