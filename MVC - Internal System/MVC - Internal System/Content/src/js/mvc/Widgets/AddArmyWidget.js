﻿// Army widget

/*****Main Function******/
var AddArmyWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.PrisonerID = null;
    _self.id = null;
    _self.mode = null;
    _self.modalUrl = "/_Popup_/Get_Army";
    _self.viewUrl = "/_Popup_Views_/Get_Army";
    _self.addAryURL = '/_Data_Army_/AddArmy';
    _self.editAryURL = '/_Data_Army_/EditArmy';
    _self.remArmyRowURL = '/_Data_Army_/RemArmy';
    _self.finalData = {};
    

    _self.init = function () {
        console.log('AddArmy Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null
        _self.PrisonerID = null;
        _self.id = null;
        _self.mode = null;
    };


    // add action
    _self.Add = function (PrisonerID, callback) {
        // initilize
        _self.init();

        console.log('Army Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = "Add";

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // edit action
    _self.View = function (id, callback) {
        // initilize
        _self.init();

        console.log('Army View called');

        _self.id = id;

        _self.mode = "View";

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewUrl);
    };
    _self.Edit = function (id, callback)
    {
        // initilize
        _self.init();

        console.log('Army Edit called');

        _self.id = id;

        _self.mode = "Edit";

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };
    // remove action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('Army Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        _core.getService('Loading').Service.enableBlur('loaderClass');

        // yes or no widget
        _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {
            if (res) {

                // remove Army Data
                removeArmyData();
            }
        });
    };

    // loading modal from Views
    function loadView(url) {
        
        var data = {};
    

        if (_self.id != null && (_self.mode == "Edit" || _self.mode == "View"))
        {
            data.id = _self.id;
        }

        // postService
        _core.getService('Post').Service.postPartial(data, url, function (res) {
            if (res != null) {

                _core.getService('Loading').Service.disableBlur('loaderClass');

                // save content for later usage
                _self.content = res;

                // content
                $('#widgetHolder').html(_self.content);

                // bind events for yes and no
                bindEvents();

                // showing modal
                $('.military-modal').modal('show');
            }
        });
    }


    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnArmy');
        var checkItems = $('.checkValidateArmy');
        _self.getItems = $('.getItemsArmy');
        var dates = $('.DatesModal');

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind custom select
        _core.getService("SelectMenu").Service.bindSelectEvents('TypeLibItemID');
        _core.getService("SelectMenu").Service.bindSelectEvents('RankLibItemID');
        _core.getService("SelectMenu").Service.bindSelectEvents('PositionLibItemID');

        // selectize
        $(".selectizeArmy").each(function () {
            $(this).selectize({
                create: false
            });
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == "Add") {
                data.PrisonerID = _self.PrisonerID;
            }
            else if (_self.mode == "Edit") {
                data.ID = _self.id;
            }

            // save Army data
            saveArmyData(data);

            //hiding modal
            $('.military-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

    }

    // save army data
    function saveArmyData(data) {

        var url = _self.addAryURL;
        if (_self.mode == "Edit") url = _self.editAryURL;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('armyLoader');

        postService.postJson(data, url, function (res) {

            _core.getService('Loading').Service.disableBlur('armyLoader');

            if (res != null) {

                // callback
                _self.callback(res);
            }
            else alert('serious eror on saving data');
        });
    }

    // remove army data
    function removeArmyData() {

        if (_self.id != null) {

            var data = {};
            data.ID = _self.id

            _core.getService('Loading').Service.enableBlur('armyLoader');

            // postService
            _core.getService('Post').Service.postJson(data, _self.remArmyRowURL, function (res) {
            
            _core.getService('Loading').Service.enableBlur('armyLoader');

                if (res != null) {

                    // callback
                    _self.callback(res);
                }
            })
        }
    }

    // collect data
    function collectData() {

        _self.getItems.each(function () {
            
            var TypeLibItemID = $(this).hasClass('TypeLibItemID');
            var RankLibItemID = $(this).hasClass('RankLibItemID');
            var PositionLibItemID = $(this).hasClass('PositionLibItemID');

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            // appending to Data
            if (TypeLibItemID || PositionLibItemID || RankLibItemID) {
                var curValues = _core.getService("SelectMenu").Service.collectData(name);
                for (var i in curValues) {
                    _self.finalData[name] = curValues[i];
                    break;
                }
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }

        });

        return _self.finalData;
    }

}
/************************/


// creating class instance
var AddArmyWidget = new AddArmyWidgetClass();

// creating object
var AddArmyWidgetObject = {
    // important
    Name: 'AddArmy',

    Widget: AddArmyWidget
}

// registering widget object
_core.addWidget(AddArmyWidgetObject);