﻿// SentenceData widget

/*****Main Function******/
var SentenceDataWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.sentenceCode = null;
    _self.PrisonerID = null;
    _self.id = null;
    _self.mode = null;

    _self.modalUrl = "/_Popup_/Get_SentenceData";
    _self.viewUrl = "/_Popup_Views_/Get_SentenceData";

    _self.addSentenceURL = '/_Data_Enterance_/AddSentence';
    _self.editSentenceURL = '/_Data_Enterance_/EditSentence';
    _self.remSentenceURL = '/_Data_Enterance_/RemSentence';
    _self.finalData = {};
    
    _self.init = function () {
        console.log('SentenceData Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.PrisonerID = null;
        _self.id = null;
        _self.mode = null;
        _self.getItems = null;
        _self.sentenceCode = null;
    };


    // add action
    _self.Add = function (PrisonerID, callback) {
        // initilize
        _self.init();

        console.log('Sentence Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = "Add";

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };
    _self.View = function (id, callback)
    {
        // initilize
        _self.init();

        console.log('Sentence view called');

        _self.id = id;

        _self.mode = "View";

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewUrl);
    };
    // edit action
    _self.Edit = function (id, callback) {
        // initilize
        _self.init();

        console.log('Sentence Edit called');

        _self.id = id;

        _self.mode = "Edit";

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // remove action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('Sentence Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // yes or no widget
        _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {
            if (res) {

                // remove Sentence
                removeSentence();
            }
        });
    };

    // loading modal from Views
    function loadView(url) {
        
        var data = {};

        if (_self.mode == "Edit"||_self.mode=="View") {
            data.id = _self.id;
        }

        // postService
        _core.getService('Post').Service.postPartial(data, url, function (res) {
            if (res != null) {

                // save content for later usage
                _self.content = res;

                // content
                $('#widgetHolder').html(_self.content);

                _core.getService('Loading').Service.disableBlur('loaderClass');

                // bind events for yes and no
                bindEvents();

                // showing modal
                $('.entrance-sentence-modal').modal('show');
            }
            

        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtn');
        _self.checkItems = $('.checkValidateModal');
        _self.getItems = $('.getItemsModal');
        var dates = $('.DatesSentence');
        var codesElem = $('#entrance-sentence-code');
        _self.sentenceCode = $('#entrance-sentence-code');
        var LowSuit = $('#LowSuit');
        var FineHas = $('#FineHas');
        var PropertyConfiscationLibItemID = $('#PropertyConfiscationLibItemID');

        // unbind
        acceptBtn.unbind();
        _self.checkItems.unbind();
        codesElem.unbind();
        LowSuit.unbind();
        PropertyConfiscationLibItemID.unbind();

        _core.getService("SelectMenu").Service.bindSelectEvents('SentencingDataArticles');

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == "Add") {
                data.PrisonerID = _self.PrisonerID;
            }
            else if (_self.mode == "Edit") {
                data.ID = _self.id;
            }

            // save Sentence data
            saveSentenceData(data);

            //hiding modal
            $('.entrance-sentence-modal').modal('hide');
        });

        // check validate and toggle accept button
        _self.checkItems.bind('change keyup', function () {
            var action = false;

            _self.checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var name = $(this).attr('name');
                    if (name != 'SentencingDataArticles') {
                        //var id = parseInt($(this).data('parentid'));
                        //if (id == parseInt(_self.sentenceCode.val())) {
                        //}
                        var curName = $(this).attr('name');
                        if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                        else action = true;
                    }
                }
            });
            

            try{
                if (_core.getService("SelectMenu").Service.collectData('SentencingDataArticles').length == 0)
                    action = true;
            }
            catch (e) {
                action = true;
            }

            acceptBtn.attr("disabled", action);
        });

        codesElem.bind('change keyup', function () {

            // load content of articles
            var ParentID = $(this).val();

            if (ParentID != null && ParentID != "") {

                _core.getService('Loading').Service.enableBlur('modal-body')

                var data = {};

                data.ID = _self.id;

                _core.getService("SelectMenu").Service.loadContent(ParentID, 'sentenceData', data, function (res) {

                    if (res != null) {
                        _core.getService("SelectMenu").Service.bindSelectEvents('SentencingDataArticles');
                    }

                    $('.checkItemsCustom').change(function () {
                        $('#entrance-sentence-number').trigger('change');
                    });

                    _core.getService('Loading').Service.disableBlur('modal-body');
                });

            }
            else {
                $('.articlesHolder').empty();
            }
        });

        // bind hide and shows
        LowSuit.bind('change', function () {
            var value = $(this).val();

            if (value != null && value == "true") {
                $('#LowSuitIn').removeClass('hidden')
            }
            else {
                $('#LowSuitIn').addClass('hidden')
            }
        });

        // bind hide and shows
        FineHas.bind('change', function () {
            var value = $(this).val();

            if (value != null && value == "true") {
                $('#FineHasIn').removeClass('hidden')
            }
            else {
                $('#FineHasIn').addClass('hidden')
            }
        });

        PropertyConfiscationLibItemID.bind('change', function () {
            var value = $(this).val();

            if (value != null && value == "2921") {
                $('#PropertyConfiscationValueIn').addClass('hidden')
            }
            else {
                $('#PropertyConfiscationValueIn').removeClass('hidden')
            }
        });

        // selectize
        $(".selectizeSentence").each(function () {
            $(this).selectize({
                create: false
            });
        });

        if (_self.id != null) {
            codesElem.trigger('change');
        }

        if (_self.mode == 'Edit') {
            PropertyConfiscationLibItemID.trigger('change');
            LowSuit.trigger('change');
            FineHas.trigger('change');
        }
    }

    // save data
    function saveSentenceData(data) {

        var url = _self.addSentenceURL;
        if (_self.mode == "Edit") url = _self.editSentenceURL;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('sentenceLoader');

        postService.postJson(data, url, function (res) {
        
            _core.getService('Loading').Service.disableBlur('sentenceLoader');

            if (res != null) {

                // callback
                _self.callback(res);
            }
            else {
                alert('Լրացնել բոլոր դաշտերը։');
                $('.entrance-sentence-modal').modal('show');
            }
        });
    }

    // remove data
    function removeSentence() {

        if (_self.id != null) {

            var data = {};
            data.ID = _self.id

            _core.getService('Loading').Service.enableBlur('sentenceLoader');

            // postService
            _core.getService('Post').Service.postJson(data, _self.remSentenceURL, function (res) {
            
                _core.getService('Loading').Service.disableBlur('sentenceLoader');

                if (res != null) {

                    // callback
                    _self.callback(res);
                }
            })
        }
    }

    // collect data
    function collectData() {

        $('.getItemsModal').each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            if (name == 'SentencingDataArticles') {
                //var id = _self.sentenceCode.val();
                //var thisParentId = $(this).data('parentid');
                //if (parseInt(id) == parseInt(thisParentId)) {
                //    var value = $(this).val();
                //    _self.finalData[name] = [];
                //    for (var i in value) {
                //        var data = {};
                //        data.LibItem_ID = value[i];
                //        _self.finalData[name].push(data);
                //    }
                //}

                var value = _core.getService("SelectMenu").Service.collectData(name);
                _self.finalData[name] = [];
                for (var i in value) {
                    var data = {};                    
                    data.LibItem_ID = value[i];
                    _self.finalData[name].push(data);
                }
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }
        });

        return _self.finalData;
    }

}
/************************/


// creating class instance
var SentenceDataWidget = new SentenceDataWidgetClass();

// creating object
var SentenceDataWidgetObject = {
    // important
    Name: 'SentenceData',

    Widget: SentenceDataWidget
}

// registering widget object
_core.addWidget(SentenceDataWidgetObject);