﻿// Add Transferss widget

/*****Main Function******/
var TransfersWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.PrisonerID = null;
    _self.id = null;
    _self.mode = null;
    _self.PersonList = [];
    _self.personFound = false;

    _self.drawPersonListURL = "/_Popup_/Draw_TransfersPersonTableRow";
    _self.modalUrl = "/_Popup_/Get_Transfer";
    _self.viewUrl = "/_Popup_Views_/Get_Transfer";
    _self.addUrl = '/Transfers/AddData';
    _self.editUrl = '/Transfers/EditData';
    _self.removeUrl = '/Transfers/RemData';
    _self.completeUrl = '/Transfers/CompleteData';
    _self.declineUrl = '/Transfers/DeclineData';
    _self.approveUrl = '/Transfers/ApproveData';
    _self.ModalName = 'transfer-add-modal';
    _self.finalData = {};

    _self.init = function () {
        console.log('Widget Inited');
        
        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.PrisonerID = null;
        _self.id = null;
        _self.personFound = false;
        _self.mode = null;
        _self.PersonList = [];
    };

    // add action
    _self.Add = function (PrisonerID, callback) {

        // initilize
        _self.init();

        console.log('Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = 'Add';

        // save callback
        _self.callback = callback;
        // loading view
        loadView(_self.modalUrl);
    }
    // view action
    _self.View = function (id, callback)
    {
        // initilize
        _self.init();

        console.log('View called');

        _self.id = id;

        _self.mode = 'View';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewUrl);
    };
    // edit action
    _self.Edit = function (id, callback) {
        // initilize
        _self.init();

        console.log('Edit called');

        _self.id = id;

        _self.mode = 'Edit';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // edit action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        removeData();
    };

    // edit action
    _self.Complete = function (id, data, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        var curData = {};
        curData.EndDate = data;
        curData.ID = _self.id;

        // remove
        CompleteData(curData);
    };

    // decline action
    _self.Decline = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        DeclineData();
    };

    // approve action
    _self.Approve = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        ApproveData();
    };


    // loading modal from Views
    function loadView(url) {
        
        var data = {};

        if (_self.id != null && (_self.mode == "Edit"||_self.mode=="View")) {
            data.ID = _self.id;
        }
        data.PrisonerID = _self.PrisonerID;

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _self.content = res;

            // content
            $('#widgetHolder').html(_self.content);

            _core.getService('Loading').Service.disableBlur('loaderClass');

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.' + _self.ModalName).modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {
        
        // buttons and fields
        var acceptBtn = $('#acceptBtnModal');
        var addPersonBtn = $('#addPersonBtnModal');
        var findPersonAgainBtn = $('#findpersonAgain');
        var nextBtn = $('#nextBtnModal');
        var prevBtn = $('#prevBtnModal');
        var checkItemsFirstPart = $('.checkValidateFirstPartModal');
        var checkItemsSecondPart = $('.checkValidateSecondPartModal');
        var PositionLibItemElem = $('#PositionLibItemID');
        var StateLibItemID = $('#StateLibItemID');
        var TransferLibItemID = $('#TransferLibItemID');
        var TransferLibItem139 = $('#TransferLibItem139');
        var TransferLibItem165 = $('#TransferLibItem165');
        var TransferLibItem166 = $('#TransferLibItem166');
        var FindPerson = $('#findperson');
        var EmergencyLibItemID = $('#EmergencyLibItemID');
        var PurposeLibItemID = $('#PurposeLibItemID');
        var dates = $('.DatesModal');
        _self.getItems = $('.getItemsModal');
        var firstRun = true;

        // unbind
        acceptBtn.unbind();
        checkItemsFirstPart.unbind();
        checkItemsSecondPart.unbind();
        $('.TransferClass').unbind();

        // bind custom select
        _core.getService("SelectMenu").Service.bindSelectEvents('QnnchakanLibItemID');
        //$('#QnnchakanLibRow').addClass('hidden');

        PurposeLibItemID.unbind().bind('change', function () {
            var value = $(this).val();

            clearFileds();

            $('.TransferClass').addClass('hidden');
            if (value == "1558") {
                $('.TransferClass166').removeClass('hidden');
                if (!firstRun)
                    $('.TransferClass166')[0].selectize.clear();
            }
            else if (value == "1559") {
                $('.TransferClass165').removeClass('hidden');
                if (!firstRun)
                    $('.TransferClass165')[0].selectize.clear();
                $('#EmergencyLibRow').addClass('hidden');
            }
            else if (value == "1560") {
                $('.TransferClass139').removeClass('hidden');
                if (!firstRun)
                    $('.TransferClass139')[0].selectize.clear();
                $('#EmergencyLibRow').addClass('hidden');
            }
        });

        TransferLibItem139.bind('change', function () {
            var value = $(this).val();
            clearFileds();
            if (value == "1561") {
                $('#UrgUnitRow').removeClass('hidden');
                $('#HospitalLibRow').addClass('hidden');
                $('#QKHHospitalLibRow').addClass('hidden');
                $('#CourtLibRow').addClass('hidden');
            }
            else {
                $('#UrgUnitRow').addClass('hidden');
                $('#QKHHospitalLibRow').addClass('hidden');
                $('#HospitalLibRow').addClass('hidden');
                $('#CourtLibRow').addClass('hidden');
            }
        })

        TransferLibItem165.bind('change', function () {
            var value = $(this).val();
            clearFileds();
            if (value == "3508") {
                $('#HospitalLibRow').removeClass('hidden');
                $('#EmergencyLibRow').removeClass('hidden');
                $('#UrgUnitRow').addClass('hidden');
                $('#CourtLibRow').addClass('hidden');
                $('#QKHHospitalLibRow').addClass('hidden');
                $('#HogebujaranLibRow').addClass('hidden');
                $('#QnnchakanLibRow').addClass('hidden');
            }
            else if (value == "3507") {
                $('#HospitalLibRow').addClass('hidden');
                $('#EmergencyLibRow').addClass('hidden');
                $('#UrgUnitRow').addClass('hidden');
                $('#CourtLibRow').addClass('hidden');
                $('#HogebujaranLibRow').removeClass('hidden');
                $('#QKHHospitalLibRow').addClass('hidden');
                $('#QnnchakanLibRow').addClass('hidden');
            }
            else if (value == "3506") {
                $('#HospitalLibRow').addClass('hidden');
                $('#EmergencyLibRow').addClass('hidden');
                $('#UrgUnitRow').addClass('hidden');
                $('#CourtLibRow').removeClass('hidden');
                $('#HogebujaranLibRow').addClass('hidden');
                $('#QKHHospitalLibRow').addClass('hidden');
                $('#QnnchakanLibRow').addClass('hidden');
            }
            else if (value == "3505") {
                $('#HospitalLibRow').addClass('hidden');
                $('#EmergencyLibRow').addClass('hidden');
                $('#UrgUnitRow').addClass('hidden');
                $('#CourtLibRow').addClass('hidden');
                $('#QKHHospitalLibRow').addClass('hidden');
                $('#HogebujaranLibRow').addClass('hidden');
                $('#QnnchakanLibRow').removeClass('hidden');
            }
        })

        TransferLibItem166.bind('change', function () {
            var value = $(this).val();
            clearFileds();
            if (value == "3504") {
                $('#HospitalLibRow').removeClass('hidden');
                $('#EmergencyLibRow').removeClass('hidden');
                $('#UrgUnitRow').addClass('hidden');
                $('#CourtLibRow').addClass('hidden');
                $('#HogebujaranLibRow').addClass('hidden');
                $('#QnnchakanLibRow').addClass('hidden');
            }
            else if (value == "3503") {
                $('#HospitalLibRow').addClass('hidden');
                $('#QKHHospitalLibRow').removeClass('hidden');
                $('#EmergencyLibRow').removeClass('hidden');
                $('#UrgUnitRow').addClass('hidden');
                $('#CourtLibRow').addClass('hidden');
                $('#HogebujaranLibRow').addClass('hidden');
                $('#QnnchakanLibRow').addClass('hidden');
            }
            
        })

        function clearFileds() {

            $('#QKHHospitalLibRow').addClass('hidden');
            $('#HospitalLibRow').addClass('hidden');
            $('#UrgUnitRow').addClass('hidden');
            $('#CourtLibRow').addClass('hidden');
            $('#HogebujaranLibRow').addClass('hidden');
            $('#QnnchakanLibRow').addClass('hidden');

            if (!firstRun) {
                $('#QKHHospitalLibItemID')[0].selectize.clear();
                $('#HospitalLibItemID')[0].selectize.clear();
                $('#OrgUnitID')[0].selectize.clear();
                $('#CourtLibItemID')[0].selectize.clear();
                $('#HogebujaranLibItemID')[0].selectize.clear();
                _core.getService("SelectMenu").Service.clearData('QnnchakanLibItemID');
            }
        }



        StateLibItemID.unbind().bind('change', function () {
            var value = $(this).val();
            if (value == "1552") {
                $('#EndLibRow').removeClass('hidden');
            }
            else {
                $('#EndLibRow').addClass('hidden');
            }
        });
        //function removeValidate() {
        //    if (($(this).attr('id') != '')) {
        //        EmergencyLibItemID.removeClass('checkValidateFirstPartModal');
        //    }
        //}
        // check validate and toggle accept button
        
        checkItemsFirstPart.bind('change keyup', function () {
            var action = false;
            checkItemsFirstPart.each(function () {

                var passvalid = true;
                
                if (($(this).attr('id') == "OrgUnitID") && (TransferLibItemID.val() != 1561)) passvalid = false;
                else if (($(this).attr('id') == "HospitalLibItemID") && (TransferLibItemID.val() != 1562)) passvalid = false;
                else if (($(this).attr('id') == "CourtLibItemID") && (TransferLibItemID.val() != 1563)) passvalid = false;
                else if (($(this).attr('id') == "EndLibItemID") && (StateLibItemID.val() != 1550 || StateLibItemID.val() != 1549)) passvalid = false;
                //if (($(this).attr('id') == 'PurposeLibItemID') && $(this).val() == "1558") {
                //    if (EmergencyLibItemID.val() == "") action = true;
                //}
                
                if (passvalid && $(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            nextBtn.attr('disabled', action);
        });

        // check validate and toggle accept button
        checkItemsSecondPart.bind('change keyup', function () {
            var action = false;
            checkItemsSecondPart.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            if (action == false && _self.personFound) {
                addPersonBtn.attr('disabled', action);
                acceptBtn.attr('disabled',action)
            }
            else
            {
                addPersonBtn.attr('disabled', true);
            }
        });
        
       
        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            timePicker: true,
            timePicker24Hour: true,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(true),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY HH:mm:ss"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });
        dates.each(function () {
            if ($(this).val() == "")
                $(this).data('daterangepicker').setStartDate(new Date($.now()));
            else {
                var dateString = $(this).val();
                dateString = dateString2Date(dateString);
                $(this).data('daterangepicker').setStartDate(dateString);
            }
        })
        function dateString2Date(dateString) {
            var dt = dateString.split(/\/|\s/);
            return new Date(dt.slice(0, 3).reverse().join('/') + ' ' + dt[3]);
        }
       
        // bind accept
        acceptBtn.bind('click', function () {
            var data = collectData();
            var value = PurposeLibItemID.val();
            var TransferValue = null;
            if (value == "1558") {
                TransferValue = $('.TransferClass166').val();
            }
            else if (value == "1559") {
                TransferValue = $('.TransferClass165').val();
            }
            else if (value == "1560") {
                TransferValue = $('.TransferClass139').val();
            }

            data['TransferLibItemID'] = TransferValue;

            if (_self.mode == 'Add') {

                data.PrisonerID = _self.PrisonerID;

                // add
                addData(data);
            }
            else if (_self.mode == 'Edit') {

                data.ID = _self.id;

                // edit
                editData(data);
            };

            //hiding modal
            $('.' + _self.ModalName).modal('hide');
        });

        // bind next
        nextBtn.unbind().click(function () {
            $('.part1').addClass('hidden');
            $('.part2').removeClass('hidden');
            checkItemsSecondPart.trigger('change');
            nextBtn.attr('disabled', true);
            console.log($('.transferTitle'))
            $('.transferTitle').html(' (Ներկայացնող Անձ)');
            prevBtn.attr('disabled', false);
        });

        // bind prev
        prevBtn.unbind().click(function () {
            $('.part1').removeClass('hidden');
            $('.part2').addClass('hidden');
            nextBtn.attr('disabled', false);
            $('.transferTitle').html('');
            prevBtn.attr('disabled', true);
        });

        findPersonAgainBtn.unbind().click(function () {
            acceptBtn.attr('disabled', true);
            // clean search
            cleanSearch();

            // trigger of change
            acceptBtn.attr('disabled', true);
        });

        // trigger change
        //_self.getItems.bind('change keyup', function () {
        //    checkItems.trigger('change');
        //});
      
        // bind find person part to FindPerson Widget
        _core.getWidget('FindPerson').Widget.BindPersonSearch(function (status) {
            _self.personFound = status;
            if (status) {
                acceptBtn.attr('disabled', false);
            }
            else {
                acceptBtn.attr('disabled', true);
            }
        });

        // selectize
        $(".selectizeModal").each(function () {
            $(this).selectize({
                create: false
            });
        });

        if (_self.mode == 'Edit') {
            PurposeLibItemID.trigger('change');
            var value = PurposeLibItemID.val();

            if (value == "1558") {
                $('.TransferClass166').removeClass('hidden');
                TransferLibItem166.trigger('change');
            }
            else if (value == "1559") {
                $('.TransferClass165').removeClass('hidden');
                TransferLibItem165.trigger('change');
            }
            else if (value == "1560") {
                TransferLibItem139.trigger('change');
            }
        }

        firstRun = false;
    }
    // bind person list events
    function bindPersonListEvents() {
        var removeRow = $('.removePersonListRow');

        removeRow.unbind().click(function () {
            var personid = parseInt($(this).data('id'));

            var data = {};
            data.PersonalID = personid;

            // remove person
            removePerson(data);

            // draw
            drawPersonList();
        });
    }

    // add
    function addData(data) {
        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.addUrl, function (res) {

            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding');
        })
    }

    // edit
    function editData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.editUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing');
        })
    }

    // remove
    function removeData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;
        
        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.removeUrl, function (res) {
        _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on removing');
        })
    }


    // decline
    function DeclineData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.declineUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on declining');
        })
    }

    // approve
    function ApproveData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        postService.postJson(data, _self.approveUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('maintablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on approving');
        })
    }

    // complete
    function CompleteData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.completeUrl, function (res) {
            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on completing');
        });
    }

    // collect data
    function collectData() {

        // array of violation list
        //_self.finalData["ViolationList"] = [];
        _self.getItems.each(function () {

            var QnnchakanLibItemID = $(this).hasClass('QnnchakanLibItemID');

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            _self.finalData['HospitalLibItemID'] = $('#HospitalLibItemID').val();
            // appending to Data
            if (QnnchakanLibItemID) {
                var curValues = _core.getService("SelectMenu").Service.collectData(name);
                for (var i in curValues) {
                    _self.finalData[name] = curValues[i];
                    break;
                }
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }

        });

        _self.finalData["Person"] =  _core.getWidget('FindPerson').Widget.getPersonData().Person;

        return _self.finalData;
      
    }

    // remove soc id from list
    function removePerson(data) {
        for (var i in _self.PersonList) {
            if (parseInt(_self.PersonList[i].PersonalID) == parseInt(data.PersonalID)) {
                _self.PersonList.splice(i, 1);
            }
        }
    }

    // clean search
    function cleanSearch() {

        // hide main row for loader show (it is new user search)
        $('#foundusertable').addClass('hidden');
        $('#foundusererrorp').addClass('hidden');

        // disableing or enabling fields
        $('#modal-person-name-1').val('');
        $('#modal-person-name-2').val('');
        $('#modal-person-name-3').val('');
        $('#modal-person-bd').val('');
        $('#modal-person-social-id').val('');
        $('#modal-person-passport').val('');

        $('#modal-person-name-1').attr('disabled', false);
        $('#modal-person-name-2').attr('disabled', false);
        $('#modal-person-name-3').attr('disabled', false);
        $('#modal-person-bd').attr('disabled', false);
        $('#modal-person-social-id').attr('disabled', false);
        $('#modal-person-passport').attr('disabled', false);
    }
    function EnablecceptButton() {
        var action = false;
        if (action == false && _self.personFound) {
            addPersonBtn.attr('disabled', action);
            acceptBtn.attr('disabled', action)
        }
    }
    // draw person list
    function drawPersonList() {
        var data = [];
        for (var i in _self.PersonList) {
            for (var j in _self.PersonList) {
                data.push(_self.PersonList[j].Person);
            }
           
        }
        _core.getService('Post').Service.postPartial(data, _self.drawPersonListURL, function (res) {
            if (res != null) {
                $('#personListTableBody').empty();
                $('#personListTableBody').append(res);

                // bind draw PersonList events
                bindPersonListEvents();
            }
        })
    }

}
/************************/


// creating class instance
var TransfersWidget = new TransfersWidgetClass();

// creating object
var TransfersWidgetObject = {
    // important
    Name: 'Transfers',

    Widget: TransfersWidget
}

// registering widget object
_core.addWidget(TransfersWidgetObject);