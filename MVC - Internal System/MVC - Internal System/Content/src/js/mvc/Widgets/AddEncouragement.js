﻿// Add Encouragements widget

/*****Main Function******/
var AddEncouragementWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.PrisonerID = null;
    _self.id = null;
    _self.mode = null;

    _self.modalUrl = "/_Popup_/Get_Encouragement";
    _self.viewUrl = "/_Popup_Views_/Get_Encouragement";
    _self.addEncUrl = '/Encouragements/AddEnc';
    _self.editEncUrl = '/Encouragements/EditEnc';
    _self.removeEncUrl = '/Encouragements/RemEnc';
    _self.finalData = {};
    
    _self.init = function () {
        console.log('AddEncouragement Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.PrisonerID = null;
        _self.id = null;
        _self.mode = null;
    };

    // add action
    _self.Add = function (PrisonerID, callback) {

        // initilize
        _self.init();
        console.log('AddEncouragement Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = 'Add';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    }
    // view action
    _self.View = function (PrisonerID, id, callback)
    {
        // initilize
        _self.init();

        console.log('AddEncouragement Edit called');

        _self.id = id;

        _self.mode = 'view';

        _self.PrisonerID = PrisonerID;

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewUrl);
    };
    // edit action
    _self.Edit = function (PrisonerID, id, callback) {
        // initilize
        _self.init();

        console.log('AddEncouragement Edit called');

        _self.id = id;

        _self.mode = 'Edit';

        _self.PrisonerID = PrisonerID;

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // edit action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('AddEncouragement Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        removeEnc();
    };

    // loading modal from Views
    function loadView(url) {

        var data = {};

        if (_self.id != null && (_self.mode == "Edit" || _self.mode == "view"))
        {
            data.ID = _self.id;
        }

        data.PrisonerID = _self.PrisonerID;

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _self.content = res;

            _core.getService('Loading').Service.disableBlur('loaderClass');

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.benefit-add-modal').modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnEnc');
        var checkItems = $('.checkValidateEnc');
        var dates = $('.Dates');
        _self.getItems = $('.getItemsEnc');

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == 'Add') {

                data.PrisonerID = _self.PrisonerID;
                
                // add
                addEnc(data);
            }
            else if (_self.mode == 'Edit') {

                data.ID = _self.id;

                // edit
                editEnc(data);
            };

            //hiding modal
            $('.benefit-add-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // selectize
        $(".selectizeEnc").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // add
    function addEnc(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('enctablebody');

        postService.postJson(data, _self.addEncUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('enctablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding encouragement');
        })
    }

    // edit
    function editEnc(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('enctablebody');

        postService.postJson(data, _self.editEncUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('enctablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing encouragement');
        })
    }

    // remove
    function removeEnc() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('enctablebody');

        postService.postJson(data, _self.removeEncUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('enctablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on removing encouragement');
        })
    }

    // collect data
    function collectData() {

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            // appending to Data
            _self.finalData[name] = $(this).val();
        });

        return _self.finalData;
    }

}
/************************/


// creating class instance
var AddEncouragementWidget = new AddEncouragementWidgetClass();

// creating object
var AddEncouragementWidgetObject = {
    // important
    Name: 'AddEncouragement',

    Widget: AddEncouragementWidget
}

// registering widget object
_core.addWidget(AddEncouragementWidgetObject);