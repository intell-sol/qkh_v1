﻿// case close terminate widget

/*****Main Function******/
var CaseCloseTerminateWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null;
    _self.PrisonerID = null;
    _self.number = null;
    _self.id = null;
    _self.SentenceID = null
    _self.ArrestID = null;
    _self.DataType = null;

    _self.modalUrl = "/_Popup_/Get_CaseCloseTerminate";
    _self.updateTerminationURL = '/_Data_Terminate_/SaveTermination';
    _self.finalData = {};
    

    _self.init = function () {
        console.log('CaseCloseTerminate Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.number = null;
        _self.PrisonerID = null;
        _self.PrisonerType = null;
        _self.id = null;
        _self.SentenceID = null
        _self.ArrestID = null;
        _self.DataType = null;
    };

    // ask action
    _self.Show = function (id, callback) {
        // initilize
        _self.init();

        console.log('CaseCloseTerminate Ask called');

        // save callback
        _self.callback = callback;

        _self.mode = "Edit"

        // bind ID
        _self.id = id;

        // loading view
        loadView();
    };

    // terminate action
    _self.Terminate = function (data,  callback) {
        // initilize
        _self.init();

        console.log('CaseCloseTerminate Ask called');

        // save callback
        _self.callback = callback;

        _self.mode = "Add";

        // bind number
        _self.number = data['Number'];
        _self.SentenceID = data['SentenceID'];
        _self.ArrestID = data['ArrestID'];
        _self.DataType = data['DataType'] == "True";
        _self.PrisonerID = data['PrisonerID'];
        _self.PrisonerType = data['PrisonerType'];

        // loading view
        loadView();
    };

    // loading modal from Views
    function loadView() {

        var url = _self.modalUrl;

        var data = {};

        if (_self.mode = "Edit") {
            data.id = _self.id;
        }


        data.PrisonerType = _self.PrisonerType;
        
        // postService
        _core.getService('Post').Service.postPartial(data, url, function (res) {
            if (res != null) {

                _core.getService('Loading').Service.disableBlur('loaderClass');

                // save content for later usage
                _self.content = res;

                // content
                $('#widgetHolder').html(_self.content);

                // bind events
                bindEvents();

                // showing modal
                $('.terminate-case-modal').modal('show');
            }
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptTerminationBtn');
        var checkItems = $('.checkValidateBanT');
        var dates = $('.DatesBanT');
        _self.getItems = $('.getItemsBanT');
        var CloseTypeLibItemID = $('#CloseTypeLibItemID');

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            // bind some other infox
            data.PCN = _self.number;
            data.PrisonerID = _self.PrisonerID;

            data['DataType'] = _self.DataType;
            data['SentenceID'] = _self.SentenceID;
            data['ArrestID'] = _self.ArrestID;

            // save termination data
            saveTemination(data);

            //hiding modal
            $('.terminate-case-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                var curName = $(this).attr('name');
                if ($(this).val() == '' || $(this).val() == null) {
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
                if (curName == "CloseTypeLibItemID") {
                    if ($(this).val() == "2850") {
                        $('#selfkillwrap').removeClass("hidden");
                    }
                    else {
                        $('#selfkillwrap').addClass("hidden");
                    }
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // selectize
        $(".selectizeTerminate").each(function () {
            $(this).selectize({
                create: false
            });
        });

        checkItems.trigger('change');
    }

    // save termination function
    function saveTemination(data) {

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.updateTerminationURL, function (res) {
            if (res != null) {

                // callback
                _self.callback(res);
            }
            else alert('serious eror on adding ban terminate');
        })
    }

    // collect data
    function collectData() {

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            // appending to Data
            _self.finalData[name] = $(this).val();
        });

        // bind ID
        _self.finalData['ID'] = _self.ID;

        return _self.finalData;
    }

}
/************************/


// creating class instance
var CaseCloseTerminateWidget = new CaseCloseTerminateWidgetClass();

// creating object
var CaseCloseTerminateWidgetObject = {
    // important
    Name: 'CaseCloseTerminate',

    Widget: CaseCloseTerminateWidget
}

// registering widget object
_core.addWidget(CaseCloseTerminateWidgetObject);