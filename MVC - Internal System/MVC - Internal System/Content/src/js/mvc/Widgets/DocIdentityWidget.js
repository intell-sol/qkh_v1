﻿// document identity widget

/*****Main Function******/
var DocIdentityWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.modal = $('.id-doc-modal');
    _self.modalUrl = "/_Popup_/Get_DocIdentity";
    _self.viewUrl = "/_Popup_Views_/Get_DocIdentity";
    _self.response = {};
    _self.response.status = false;
    _self.response.data = null;
    

    // action list
    _self.actionList = {
    };

    _self.init = function () {
        console.log('DocIdentity Widget Inited');

        _self.callback = null;
        _self.response.status = false;
        _self.response.data = null;
    };

    // ask action
    _self.Show = function (callback) {
        // initilize
        _self.init();

        console.log('DocIdentity Ask called');

        _self.mode == "Add";

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    _self.View = function (id)
    {
        _self.init();

        _self.mode = "View";
        _self.id = id;
        // loading view
        loadView(_self.viewUrl);
    }

    // loading modal from Views
    function loadView(url)
    {
        var data = {};
        if (_self.id != null && (_self.mode == "Edit" || _self.mode == "View"))
        {
            data.ID = _self.id;
        }

        _core.getService("Post").Service.postPartial(data, url, function (res)
        {

            _core.getService('Loading').Service.disableBlur('loaderClass');

            _self.content = res;

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.id-doc-modal').modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {
        var docDate = $('#id-doc-date');
        var saveBtn = $('#savedocidentity');
        var toValidateFields = $('.tovalidate');

        // selectize
        $(".selectizeWidget").each(function () {
            $(this).selectize({
                create: false
            });
        });

        // daterangepicker
        docDate.unbind();
        toValidateFields.unbind(); // clear it too for later binding validation
        docDate.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        docDate.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
        });
        docDate.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
        });

        // unbind
        saveBtn.unbind();

        saveBtn.bind('click', function () {

            // call callback
            _self.response.data = collectData();
            _self.response.status = true;

            _self.callback(_self.response);

            // remove callback
            _self.callback = null;
            _self.response.status = false;

            //hiding modal
            $('.id-doc-modal').modal('hide');

        });

        // validation on every input change
        toValidateFields.bind('change keyup', function () {
            checkValidate();
        });
    }

    // collect form data
    function collectData() {
        var data = {};
        
        var citizenship = $('#id-doc-citizenship')[0].selectize;
        var docidentityselect = $('#id-doc-type')[0].selectize;
        var number = $('#id-doc-number');
        var date = $('#id-doc-date');
        var from = $('#id-doc-from');

        data.CitizenshipLibItem_Name = citizenship.getItem(citizenship.getValue())[0].innerHTML;
        data.CitizenshipLibItem_ID = citizenship.getValue();
        data.TypeLibItem_Name = docidentityselect.getItem(docidentityselect.getValue())[0].innerHTML;
        data.TypeLibItem_ID = docidentityselect.getValue();
        data.Number = number.val();
        data.Date = date.val();
        data.FromWhom = from.val();
        data.Type = false;
        data.ID = data.CitizenshipLibItem_ID + data.TypeLibItem_ID;

        return data;
    }

    // check validate
    function checkValidate() {

        var status = true;

        // accept button
        var saveBtn = $('#savedocidentity');

        // check list
        var checklist = [];
        var citizenship = $('#id-doc-type')[0].selectize;
        var docidentityselect = $('#id-doc-citizenship')[0].selectize;
        var number = $('#id-doc-number');
        var date = $('#id-doc-date');
        var from = $('#id-doc-from');

        // add to array for easier check
        checklist.push(number);
        checklist.push(date);
        checklist.push(from);

        // validate
        if (citizenship.getValue() == null || citizenship.getValue() == '') status = false;
        if (docidentityselect.getValue() == null || docidentityselect.getValue() == '') status = false;
        for (var i in checklist) {
            if (checklist[i].val() == '' || checklist[i].val() == null) {
                status = false;
                break;
            }
        }

        // toogle button
        if (status) {
            saveBtn.attr('disabled', false);
        }
        else {
            saveBtn.attr('disabled', true);
        }

        return status;
    }
}
/************************/


// creating class instance
var DocIdentityWidget = new DocIdentityWidgetClass();

// creating object
var DocIdentityWidgetObject = {
    // important
    Name: 'DocIdentity',

    Widget: DocIdentityWidget
}

// registering widget object
_core.addWidget(DocIdentityWidgetObject);