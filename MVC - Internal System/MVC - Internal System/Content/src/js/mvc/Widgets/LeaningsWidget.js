﻿// Leanings widget

/*****Main Function******/
var LeaningsWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.modal = $('.physical-addiction-modal');
    _self.modalUrl = "/_Popup_/Get_Leanings";
    _self.editUrl = "/_Data_Physical_/EditLeanings"
    _self.viewUrl = "/_Popup_Views_/Get_Leanings";

    _self.finalFormData = new FormData();
    

    // action list
    _self.actionList = {
    };

    _self.init = function () {
        console.log('Leanings Widget Inited');

        _self.finalFormData = new FormData();
        _self.callback = null;
        _self.content = null;
        _self.mode = 'Add';
        _self.id = null;
    };

    // ask action
    _self.Show = function (type, callback) {
        // initilize
        _self.init();

        console.log('Leanings Ask called');

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };
    _self.View = function (id, callback)
    {
        // initilize
        _self.init();

        _self.mode = 'View';

        console.log('Leanings Edit called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewUrl);
    };
    // edit action
    _self.Edit = function (id, callback) {
        // initilize
        _self.init();

        _self.mode = 'Edit';

        console.log('Leanings Edit called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // loading modal from Views
    function loadView(url) {
     
       
          
            if (_self.id != null && (_self.mode == "Edit" || _self.mode == "View")) url = url + '/' + _self.id;

            $.get(url, function (res) {

                _core.getService('Loading').Service.disableBlur('loaderClass');

                // save content for later usage
                _self.content = res;

                // content
                $('#widgetHolder').html(_self.content);

                // bind events for yes and no
                bindEvents();

                // showing modal
                $('.physical-addiction-modal').modal('show');
            }, 'html');
        }
       

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtn');
        var checkItems = $('.checkValidatePopup');
        _self.getItems = $('.getItemsPopup');
        var dates = $('.datePick');

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();
        _self.getItems.unbind();

        // selectize
        $(".selectize").each(function () {
            $(this).selectize({
                create: false
            });
        });

        // bind custom select
        _core.getService("SelectMenu").Service.bindSelectEvents('TypeLibItem_ID');

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind accept
        acceptBtn.bind('click', function () {

            // calling callback
            if (_self.mode == 'Add') {
                if (_self.callback != null)
                    _self.callback(collectData());
            }
            else if (_self.mode == 'Edit') {
                data = collectData();
                data.append('ID', _self.id);

                // edit
                editLeanings(data);
            }

            //hiding modal
            $('.physical-addiction-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else if (curName == 'TypeLibItem_ID') {
                        if (_core.getService("SelectMenu").Service.collectData(curName).length == 0) action = true;
                    }
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // get items
        _self.getItems.bind('change', function () {
            
            _self.getItems.each(function () {

                // name of Field (same as in Entity)
                var name = $(this).attr('name');

                // clean FormData
                _self.finalFormData.delete(name);

                // appending to FormData
                _self.finalFormData.append(name, $(this).val());
            });
        });
    }

    // edit ban
    function editLeanings(data) {

        // post service
        var postService = _core.getService('Post').Service;

        postService.postFormData(data, _self.editUrl, function (res) {

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing leaning');
        })
    }
    // collect data
    function collectData() {
            _self.getItems.each(function () {

                var TypeLibItem_ID = $(this).hasClass('TypeLibItem_ID');

                // name of Field (same as in Entity)
                var name = $(this).attr('name');

                // clean FormData
                _self.finalFormData.delete(name);

                // appending to Data
                if (TypeLibItem_ID) {
                    var curValues = _core.getService("SelectMenu").Service.collectData(name);
                    for (var i in curValues) {
                        _self.finalFormData.append(name, curValues[i]);
                        break;
                    }
                }
                else {
                    // appending to FormData
                    _self.finalFormData.append(name, $(this).val());
                }

            });
        return _self.finalFormData;
    }

}
/************************/


// creating class instance
var LeaningsWidget = new LeaningsWidgetClass();

// creating object
var LeaningsWidgetObject = {
    // important
    Name: 'Leanings',

    Widget: LeaningsWidget
}

// registering widget object
_core.addWidget(LeaningsWidgetObject);