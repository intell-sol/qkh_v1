﻿// Add Penalty widget

/*****Main Function******/
var AddPenaltyWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.PrisonerID = null;
    _self.id = null;
    _self.mode = null;

    _self.modalUrl = "/_Popup_/Get_Penalty";
    _self.viewUrl = "/_Popup_Views_/Get_Penalty";
    _self.addPenUrl = '/Penalty/AddPen';
    _self.editPenUrl = '/Penalty/EditPen';
    _self.completeUrl = '/Penalty/CompleteData';
    _self.removePenUrl = '/Penalty/RemPen';
    _self.finalData = {};
    
    _self.init = function () {
        console.log('AddPenalty Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.PrisonerID = null;
        _self.id = null;
        _self.mode = null;
    };

    // add action
    _self.Add = function (PrisonerID, callback) {

        // initilize
        _self.init();

        console.log('AddPenalty Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = 'Add';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    }
    // view action
    _self.View = function (id, PrisonerID, callback)
    {
        // initilize
        _self.init();

        console.log('AddPenalty view called');

        _self.id = id;
        _self.PrisonerID = PrisonerID;

        _self.mode = 'View';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewUrl);
    };
    // edit action
    _self.Edit = function (id, PrisonerID, callback) {
        // initilize
        _self.init();

        console.log('AddPenalty Edit called');

        _self.id = id;
        _self.PrisonerID = PrisonerID;

        _self.mode = 'Edit';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // edit action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('AddPenalty Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        removePen();
    };

    // edit action
    _self.Complete = function (id, data, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        var curData = {};
        curData.StatusChangeBasisLibItemID = data.StatusChangeBasisLibItemID;
        curData.StatusChangeBasis = data.StatusChangeBasis;
        curData.ID = _self.id;

        // remove
        CompleteData(curData);
    };

    // loading modal from Views
    function loadView(url) {

        var data = {};
       

        if (_self.id != null && (_self.mode == "Edit" || _self.mode == "View"))
        {
            data.ID = _self.id;
        }
        data.PrisonerID = _self.PrisonerID;

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _core.getService('Loading').Service.disableBlur('loaderClass');

            _self.content = res;

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.penalty-add-modal').modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnPen');
        var checkItems = $('.checkValidatePen');
        var dates = $('.Dates');
        _self.getItems = $('.getItemsPen');
        var StateLibItemID = $('#StateLibItemID');

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        _core.getService("SelectMenu").Service.bindSelectEvents('ViolationList');

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == 'Add') {

                data.PrisonerID = _self.PrisonerID;
                
                // add
                addPen(data);
            }
            else if (_self.mode == 'Edit') {

                data.ID = _self.id;
                data.PrisonerID = _self.PrisonerID;

                // edit
                editPen(data);
            };

            //hiding modal
            $('.penalty-add-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else if (curName == 'ViolationList') {
                        if (_core.getService("SelectMenu").Service.collectData(curName).length == 0) action = true;
                    }
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // binde hide and show
        StateLibItemID.bind('change', function () {
            if (StateLibItemID != null && StateLibItemID.val() == '565') {
                $('#StateLibItemIDIn').addClass('hidden');
            }
            else {
                $('#StateLibItemIDIn').removeClass('hidden');
            }
        })

        // selectize
        $(".selectizePenalty").each(function () {
            $(this).selectize({
                create: false
            });
        });

        if (_self.mode == 'Edit')
            StateLibItemID.trigger('change');

        $('.customSelectInputViolationList').bind('change', function () {
            checkItems.trigger('change');
        });
    }

    // add
    function addPen(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('pentablebody');

        postService.postJson(data, _self.addPenUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('pentablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding penalty');
        })
    }

    // edit
    function editPen(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('pentablebody');

        postService.postJson(data, _self.editPenUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('pentablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing penalty');
        })
    }

    // remove
    function removePen() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('pentablebody');

        postService.postJson(data, _self.removePenUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('pentablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on removing penalty');
        })
    }

    // add penalty
    function CompleteData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.completeUrl, function (res) {
            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on completing');
        });
    }

    // collect data
    function collectData() {

        // array of violation list
        _self.finalData["ViolationList"] = [];

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            if (name == "ViolationList") {
                var tempValue = _core.getService("SelectMenu").Service.collectData(name);
                for (var i in tempValue) {
                    var temp = {};
                    temp["LibItemID"] = parseInt(tempValue[i]);
                    _self.finalData[name].push(temp);
                }
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }

        });

        return _self.finalData;
    }

}
/************************/


// creating class instance
var AddPenaltyWidget = new AddPenaltyWidgetClass();

// creating object
var AddPenaltyWidgetObject = {
    // important
    Name: 'AddPenalty',

    Widget: AddPenaltyWidget
}

// registering widget object
_core.addWidget(AddPenaltyWidgetObject);