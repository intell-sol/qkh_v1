﻿// Add CompleteDate widget

/*****Main Function******/
var TransferApproveWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.PrisonerID = null;
    _self.id = null;
    _self.mode = null;

    _self.modalUrl = "/_Popup_/Get_TransferApprove";
    _self.addUrl = '/WorkLoads/AddData';
    _self.editUrl = '/WorkLoads/EditData';
    _self.removeUrl = '/WorkLoads/RemData';
    _self.ModalName = 'transfer-Approve-modal';
    _self.finalData = {};
    
    _self.init = function () {
        console.log('Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.PrisonerID = null;
        _self.id = null;
        _self.mode = null;
    };

    // add action
    _self.Add = function (PrisonerID, callback) {

        // initilize
        _self.init();

        console.log('Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = 'Add';

        // save callback
        _self.callback = callback;

        // loading view
        loadView();
    }

    // edit action
    _self.Edit = function (id, callback) {
        // initilize
        _self.init();

        console.log('Edit called');

        _self.id = id;

        _self.mode = 'Edit';

        // save callback
        _self.callback = callback;

        // loading view
        loadView();
    };

    // edit action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        removeData();
    };


    // edit action
    _self.Show = function (callback) {
        // initilize
        _self.init();

        console.log('Show called');

        _self.mode = 'Show';

        // save callback
        _self.callback = callback;

        // loading view
        loadView();
    };

    // loading modal from Views
    function loadView() {

        var data = {};
        var url = _self.modalUrl;

        if (_self.id != null && _self.mode == "Edit") {
            data.ID = _self.id;
        }

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _self.content = res;

            _core.getService('Loading').Service.disableBlur('loaderClass');

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.'+_self.ModalName).modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnModal');
        var checkItems = $('.checkValidateModal');
        var dates = $('.DatesModal');
        var StateLibItemID = $('#StateLibItemID');
        _self.getItems = $('.getItemsModal');

       
        // unbind
        acceptBtn.unbind();
        checkItems.unbind();
        StateLibItemID.unbind().bind('change', function () {
            if (parseInt($(this).val()) == 1552) {
                $('#ModalEndLibRow').removeClass('hidden');
            }
        });
        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });
       
        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == 'Show') {

                _self.callback(data);
            }
            //else if (_self.mode == 'Edit') {

            //    data.ID = _self.id;

            //    // edit
            //    editData(data);
            //};

            //hiding modal
            $('.'+_self.ModalName).modal('hide');
        });


        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'StateLibItemID') {
                        action = true;
                    }
                    else if (StateLibItemID.val() == 1552 || StateLibItemID.val() == '1552') {
                        action = true;
                    }
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // selectize
        $(".selectizeModal").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // add
    function addData(data) {

        // post service
        var postService = _core.getService('Post').Service;
        
        postService.postJson(data, _self.addUrl, function (res) {
            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding');
        })
    }

    // edit
    function editData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.editUrl, function (res) {
            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing');
        })
    }

    // remove
    function removeData() {

        var data = {};
        data.id = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.removeUrl, function (res) {
            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on removing');
        })
    }

    // collect data
    function collectData() {

        // array of violation list
        //_self.finalData["ViolationList"] = [];

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            if (name == "ViolationList") {
                //var tempValue = $(this).val();
                //for (var i in tempValue) {
                //    var temp = {};
                //    temp["LibItemID"] = parseInt(tempValue[i]);
                //    _self.finalData[name].push(temp);
                //}
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }

        });

        return _self.finalData;
    }

}
/************************/


// creating class instance
var TransferApproveWidget = new TransferApproveWidgetClass();

// creating object
var TransferApproveWidgetClass = {
    // important
    Name: 'TransferApprove',

    Widget: TransferApproveWidget
}

// registering widget object
_core.addWidget(TransferApproveWidgetClass);