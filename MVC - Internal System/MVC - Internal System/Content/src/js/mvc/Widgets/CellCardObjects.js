﻿// CellCard widget

/*****Main Function******/
var CellCardWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.PrisonerID = null;
    _self.id = null;
    _self.mode = null;

    _self.modalUrl = "/_Popup_/Get_CellCard";
    _self.viewUrl = "/_Popup_Views_/Get_CellCard";
    _self.addCellUrl = '/_Data_Cell_/AddCell';
    _self.editCellUrl = '/_Data_Cell_/EditCell';
    _self.removeCellURL = '/_Data_Cell_/RemCell';
    _self.CheckConflictUrl = '/_Data_Cell_/GetConflictPersons';
    _self.finalData = {};
    
    _self.init = function () {
        console.log('CellCard Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.PrisonerID = null;
        _self.id = null;
        _self.mode = null;
    };

    // ask action
    _self.Add = function (PrisonerID, callback) {
        // initilize
        _self.init();

        console.log('CellCard Add called');

        _self.PrisonerID = PrisonerID;

        _self.mode = 'Add';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };
    _self.View = function (id, PrisonerID, callback)
    {
        // initilize
        _self.init();

        console.log('CellCard Edit called');

        _self.id = id;
        _self.PrisonerID = PrisonerID;

        _self.mode = 'View';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.viewUrl);
    };
    // edit action
    _self.Edit = function (id, PrisonerID, callback) {
        // initilize
        _self.init();

        console.log('CellCard Edit called');

        _self.id = id;
        _self.PrisonerID = PrisonerID;

        _self.mode = 'Edit';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // edit action
    _self.Remove = function (id, callback) {
        // initilize
        _self.init();

        console.log('CellCard Remove called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        removeCell();
    };

    // loading modal from Views
    function loadView(url)
    {
        if (_self.id != null && (_self.mode == "Edit" || _self.mode == "View")) url = url + '/' + _self.id;

        $.get(url, function (res)
        {
            // save content for later usage
            _self.content = res;

            _core.getService('Loading').Service.disableBlur('loaderClass');

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.cell-add-modal').modal('show');
        }, 'html');
    }

    // binding buttons for Modal
    function bindEvents()
    {
        var checkConflictPerson = $('#Number');
        // buttons and fields

        var acceptBtn = $('#acceptBtnCell');
        var checkItems = $('.checkValidateCell');
        var dates = $('.Dates');
        _self.getItems = $('.getItemsCell');

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();
        checkConflictPerson.focusout(function (e)
        {
            var data = {};
            data.Number = checkConflictPerson.val();
            data.PrisonerID = _self.PrisonerID;
            var postService = _core.getService('Post').Service;
            postService.postJson(data, _self.CheckConflictUrl, function (res)
            {
                console.log(res);
                if (res != null)
                {
                   if(res.status==true)
                       $('.warningClass').removeClass('hidden');
                   else
                       $('.warningClass').addClass('hidden');
                }
            });
                      
            
        });
        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == 'Add') {

                data.PrisonerID = _self.PrisonerID;
                
                // add
                addCell(data);
            }
            else if (_self.mode == 'Edit') {

                data.ID = _self.id;

                // edit
                editCell(data);
            };

            //hiding modal
            $('.cell-add-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            acceptBtn.attr('disabled', action);
        });

        // selectize
        $(".selectizeCell").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // add
    function addCell(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('celltablebody');

        postService.postJson(data, _self.addCellUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('celltablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding cell');
        })
    }

    // edit
    function editCell(data) {

        _core.getService('Loading').Service.enableBlur('celltablebody');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.editCellUrl, function (res) {
            _core.getService('Loading').Service.disableBlur('celltablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing cell');
        })
    }

    // remove
    function removeCell() {

        var data = {};
        data.ID = _self.id;

        _core.getService('Loading').Service.enableBlur('celltablebody');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.removeCellURL, function (res) {
        _core.getService('Loading').Service.disableBlur('celltablebody');

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on removing cell');
        })
    }

    // collect data
    function collectData() {

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            // appending to Data
            _self.finalData[name] = $(this).val();
        });

        return _self.finalData;
    }

}
/************************/


// creating class instance
var CellCardWidget = new CellCardWidgetClass();

// creating object
var CellCardWidgetObject = {
    // important
    Name: 'CellCard',

    Widget: CellCardWidget
}

// registering widget object
_core.addWidget(CellCardWidgetObject);