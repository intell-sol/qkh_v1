﻿// HeightWeight widget

/*****Main Function******/
var HeightWeightWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.modal = $('.physical-weight-height-modal');
    _self.modalUrl = "/_Popup_/Get_HeightWeight";
    _self.getHeightWeightTableRowURL = "/_Popup_/Get_HeightWeightTableRows";
    _self.finalFormData = new FormData();
    _self.RemHWURL = "/_Data_Physical_/remHeightWeight";
    
    _self.PrisonerID = null;

    // action list
    _self.actionList = {
    };

    _self.init = function () {
        console.log('HeightWeight Widget Inited');

        _self.finalFormData = new FormData();
        _self.callback = null;
        _self.content = null;
    };

    // ask action
    _self.Show = function (type, PrisonerID, callback) {
        // initilize
        _self.init();

        console.log('HeightWeight Ask called');

        // set PrisonerID
        _self.PrisonerID = PrisonerID;

        // save callback
        _self.callback = callback;

        // loading view
        loadView();
    };

    // loading modal from Views
    function loadView() {
        
        var url = _self.modalUrl;
        var data = {};
        data.PrisonerID = _self.PrisonerID;

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _self.content = res;

            _core.getService('Loading').Service.disableBlur('loaderClass');

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.physical-weight-height-modal').modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtn');
        var addBtn = $('#addBtn');
        var checkItems = $('.checkValidateHW');
        var getItems = $('.getItemsHW');

        // unbind
        acceptBtn.unbind();
        addBtn.unbind();
        checkItems.unbind();
        getItems.unbind();

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            _self.callback(data);

            //hiding modal
            $('.physical-weight-height-modal').modal('hide');
        });

        // add height weight
        addBtn.bind('click', function () {

            var currentHeight = $('#physical-modal-height').val();
            var currentWeight = $('#physical-modal-weight').val();

            addHeightWeight(currentHeight, currentWeight);
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            addBtn.attr('disabled', action);
        });

        // get items
        getItems.bind('change', function () {
            
            getItems.each(function () {

                // name of Field (same as in Entity)
                var name = $(this).attr('name');

                // clean FormData
                _self.finalFormData.delete(name);

                // appending to FormData
                _self.finalFormData.append(name, $(this).val());
            });
        });

        // bind height weight remove
        bindHeightWeightRemove();
    }

    function addHeightWeight(currentHeight, currentWeight) {
        var date = _core.getService('Date').Service.CurrentDate();

        var curHtml = '<tr class="getHeightWeight" data-height="' + currentHeight + '" data-weight="' + currentWeight + '" data-id=""><td>' + date + '</td><td>' + currentHeight + '</td><td>' + currentWeight + '</td><td><button "title="Հեռացնել" class="btn btn-sm btn-default btn-flat removeHeightWeight"><i class="fa fa-trash-o"></i></button></td></tr>';

        $('#heightweighttablerow').append(curHtml);

        bindHeightWeightRemove();
    }


    function bindHeightWeightRemove() {
        $('.removeHeightWeight').unbind().bind('click', function () {
            if (!$(this).parent().parent().hasClass("hasPending") && $(this).parent().parent().data("pptype") == 1)
                $(this).parent().parent().remove();
            else {
                $(this).parent().parent().addClass("hasPending");
            }
        })
    }

    // collect data
    function collectData() {
        var list = [];
        $('.getHeightWeight').each(function () {
            if (!$(this).hasClass("hasPending")) {
                var data = {};

                data['Height'] = $(this).data('height');
                data['Weight'] = $(this).data('weight');
                data['ID'] = $(this).data('id');

                list.push(data);
            }
        })

        return list;
    }

}
/************************/


// creating class instance
var HeightWeightWidget = new HeightWeightWidgetClass();

// creating object
var HeightWeightWidgetObject = {
    // important
    Name: 'HeightWeight',

    Widget: HeightWeightWidget
}

// registering widget object
_core.addWidget(HeightWeightWidgetObject);