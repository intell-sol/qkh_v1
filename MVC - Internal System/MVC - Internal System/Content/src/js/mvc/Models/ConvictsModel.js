﻿// convicts model

/*****Main Function******/
var ConvictsModelClass = function () {
    var self = this;

    self.init = function () {
        console.log('Convicts Model Inited');
    }
}
/************************/


// creating class instance
var ConvictsModel = new ConvictsModelClass();

// creating object
var ConvictsModelObject = {
    // important
    Name: 'Convicts',

    Model: ConvictsModel
}

// registering model object
_core.addModel(ConvictsModelObject);