﻿// convicts model

/*****Main Function******/
var _Data_Primary_ModelClass = function () {
    var self = this;

    self.init = function () {
        console.log('_Data_Primary_ Model Inited');
    }
}
/************************/


// creating class instance
var _Data_Primary_Model = new _Data_Primary_ModelClass();

// creating object
var _Data_Primary_ModelObject = {
    // important
    Name: '_Data_Primary_',

    Model: _Data_Primary_Model
}

// registering model object
_core.addModel(_Data_Primary_ModelObject);