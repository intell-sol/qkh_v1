﻿// convicts model

/*****Main Function******/
var PrisonersModelClass = function () {
    var self = this;

    self.init = function () {
        console.log('Prisoners Model Inited');
    }
}
/************************/


// creating class instance
var PrisonersModel = new PrisonersModelClass();

// creating object
var PrisonersModelObject = {
    // important
    Name: 'Prisoners',

    Model: PrisonersModel
}

// registering model object
_core.addModel(PrisonersModelObject);