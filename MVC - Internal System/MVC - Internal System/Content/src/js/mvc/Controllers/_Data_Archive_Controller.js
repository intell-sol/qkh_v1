﻿// archive controller

/*****Main Function******/
var _Data_Archive_ControllerClass = function () {
    var _self = this;

    _self.PrisonerID = null;

    _self.getArchiveTableRowURL = '/_Popup_/Get_ArchiveTableRows';

    _self.mode = "Add" // initial mode


    _self.init = function (Action) {
        console.log('_Data_Archive_ Controller Inited with action', Action);

        _self.mode = Action;

        _self.callback = null;
        _self.PrisonerID = null;

        // Js Bindings
        bindButtons();
    }

    // add action
    _self.Add = function () {
        console.log('_Data_Archive_ add called');

        _self.init('Add');
    }

    // edit action
    _self.Edit = function (PrisonerID) {

        console.log('_Data_Archive_ edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;
    }

    // view action
    _self.View = function (PrisonerID) {

        console.log('_Data_Archive_ view called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;
    }

    _self.save = function (callback) {
        console.log('_Data_Archive_ data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }

    // js bindings
    function bindButtons() {

    }

    // get archive list
    function getArchiveList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getArchiveTableRowURL, function (curHtml) {

            var table = null;
            table = $('#archiveTableBody');

            table.empty();
            table.append(curHtml);
        });
    }

}
/************************/


// creating class instance
var _Data_Archive_Controller = new _Data_Archive_ControllerClass();

// creating object
var _Data_Archive_ControllerObject = {
    // important
    Name: '_Data_Archive_',

    Controller: _Data_Archive_Controller
}

// registering controller object
_core.addController(_Data_Archive_ControllerObject);