﻿// WorkLoads controller

/*****Main Function******/
var ReportControllerClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.mode = null;
    _self.currentURL = null;
    _self.redirectURL = null;
    _self.finalData = {};
    var controllerName = {};
    controllerName[2] = "Report";
    controllerName[3] = "Prisoners";

    _self.selectOrgUnitURL = '/Base/SelectOrgUnit';
    _self.getPreviewPartial = {};
    _self.getPreviewPartial[2] = '/Report/GetPreview';
    _self.getPreviewPartial[3] = '/Prisoners/GetPreview';
    _self.filterURL = '/Report/Draw_PersonalDescriptionReportTableRowsByFilter';
    _self.resetURL = '/Report/Draw_ResetPersonalDescriptionReportTableRows';

    _self.startControllerIndex = 0; // initial index
    _self.mode = 'Add'; // initial mode
    _self.action = null;
    _self.PrisonerID = null;
    _self.PrisonerType = null;

    _self.PrisonerID = null;
    _self.PrisonerType = null;

    _self.mode = "Add" // initial mode

    _self.init = function (Action) {
        console.log('Controller Inited with action', Action);
        _self.PrisonerType = null;
        _self.mode = Action;

        // Js Bindings
        bindButtons();
    }

    // index action
    _self.Index = function (PrisonerType) {
        _self.init();
        //$(".selectizeOrgUnit").each(function ()
        //{
        //    $(this).selectize({
        //        create: false
        //    });
        //});
        _self.PrisonerType = PrisonerType;

        // bind pagination


        // bind search events
        bindPaginationEvent();
        console.log('index called');
        bindSearchEvents();
        _self.init('Index');
    }

    // add action
    _self.Add = function (PrisonerType) {
        console.log('Convicts Add called');

        // calling init
        _self.init();

        // setting Add Mode
        _self.mode = 'Add';

        // real action (mode is beeing changed)
        _self.action = 'Add';

        _self.PrisonerType = PrisonerType;

        // start from first part
        _self.startControllerIndex = 0;



    }

    // edit action
    _self.Edit = function (PrisonerType, PrisonerID) {
        console.log('Convicts Edit called');

        // calling init
        _self.init();

        // setting Add Mode
        _self.mode = 'Edit';

        // real action (mode is beeing changed)
        _self.action = 'Edit';

        // setting Prisoner ID
        _self.PrisonerID = PrisonerID != null ? parseInt(PrisonerID) : importantWarning();

        _self.PrisonerType = PrisonerType;

        // start from first part

    }

    // view action
    _self.View = function (PrisonerType, PrisonerID) {
        console.log('Convicts View called');

        // calling init
        _self.init();

        // setting Add Mode
        _self.mode = 'Edit';

        // real action (mode is beeing changed)
        _self.action = 'Preview';

        // setting Prisoner ID
        _self.PrisonerID = PrisonerID != null ? parseInt(PrisonerID) : importantWarning();

        _self.PrisonerType = PrisonerType;

        // start from first part



    }

    // edit action
    function bindPaginationEvent()
    {
        
        var pagBtn = $('.paginationBtn');
        pagBtn.unbind();
        pagBtn.click(function (e)
        {
            _core.getService('Loading').Service.enableBlur('loaderClass');
            e.preventDefault();

            var url = $(this).attr('href');
            
            // post service
            var postService = _core.getService('Post').Service;

            postService.postPartial(null, url, function (res)
            {
                _core.getService('Loading').Service.disableBlur('loaderClass');
                if (res != null) {
                    $('#convictsList').empty().append(res)

                    // bind pagination buttons
                    bindAcceptBtn();
                    bindPaginationEvent();
                }
            })
        })
    }


    // bind preview events
    function bindPreviewEvents() {

        var approvePrisonerBtn = $('#approvePrisonerBtn');

        approvePrisonerBtn.unbind().click(function (e)
        {
            _core.getService('Loading').Service.enableBlur('loaderClass');
            e.preventDefault();

            // call approve prisoner function
            approvePrisoner();
        });

        disableAllEvents("previewWrapRow");
    }

    // bind search events
    function bindSearchEvents() {
        var resetBtn = $('#reloadFilterBtn');
        var acceptBtn = $('#acceptFilterPersonBtn');
        var citizenship = $('#convict-citizenship');
        var orgunitlist = $('#convict-qkc');
        _self.PrisonerType=$('#convict-type');
        acceptBtn.unbind("click").bind("click", function (e)
        {
           
            _core.getService('Loading').Service.enableBlur('loaderClass');
            e.preventDefault();
            
            var finalData = {};
            $("#filterPersonForm").serializeArray().map(function (x) { finalData[x.name] = x.value; });
            finalData['CitizenshipLibItemIDList'] = citizenship.val();
            finalData['OrgUnitList'] = orgunitlist.val();
            finalData["Type"] = _self.PrisonerType.val();
            // filter person list
            filterPersonList(finalData);
           // bindPartialEvents();


        });
        resetBtn.unbind().click(function (e)
        {
            _core.getService('Loading').Service.enableBlur('loaderClass');

            resetList(function (res)
            {
                if (res.status)
                    location.reload();
		_core.getService('Loading').Service.disableBlur('loaderClass');

            });

        });
        //$(".selectizeFilterPrisoner").each(function () {
        //    $(this).selectize({
        //        create: false,
        //    });
        //});

        //$(".selectizeFilterPrisonerArchive").each(function () {
        //    $(this).selectize({
        //        create: false,
        //        plugins: {
        //            'no-delete': {}
        //        },
        //    });
        //});
    }

    _self.save = function (callback) {
        console.log('data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }

    // js bindings
    function bindButtons() {
        
        $('.reportPartial').unbind().bind('click', function (e)
        {
            _core.getService('Loading').Service.enableBlur('loaderClass');
            e.preventDefault();

            var url = $(this).data('url');
            _self.currentURL = $(this).data('dataurl');
            _self.redirectURL = $(this).data('redirecturl');

            loadReport(url);
            bindPaginationEvent();
        })

    };
    function resetList(callback)
    {

        // postService
        var postService = _core.getService('Post').Service;

        postService.postJsonReturn(null, _self.resetURL, callback);
    }
    function loadReport(url) {

        _core.getService('Post').Service.postPartial({}, url, function (res)
        {
            _core.getService('Loading').Service.disableBlur('loaderClass');
            if (res != null) {
                $('#partialHolder').empty();
                $('#partialHolder').append(res);
                bindSearchEvents();
                bindPartialEvents();
                bindPaginationEvent();
            }
        })
    }


    function bindPartialEvents() {
        _self.getItems = $('.getItems');
        var acceptBtn = $('#acceptBtn');
        var checkItems = $('.checkValidate');
        var dates = $('.Dates');
        acceptBtn.unbind();
        checkItems.unbind();
        
        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {

                    action = true;
                }
            });


            acceptBtn.attr("disabled", action);
        });
        // bind accept
        bindAcceptBtnWithoutSearch();
        bindAcceptBtn();
        //$(".selectizeOrgUnit").each(function ()
        //{
        //    $(this).selectize({
        //        create: false
        //    });
        //});
        //// selectize
        $(".selectize").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    function bindAcceptBtn()
    {
       
        $('.acceptBtn').bind('click', function (e)
        {
            _core.getService('Loading').Service.enableBlur('loaderClass');
            e.preventDefault();
            //window.open(
            //      _self.redirectURL,
            //      '_blank');
            var data = collectData();
            if (data.PrisonnerId == undefined) {
                _self.finalData['PrisonerId'] = $(this).data('id');

            }
            
            // add
          
                addData(data);
           
            
            bindPaginationEvent();
        });
    }
    function bindAcceptBtnWithoutSearch()
    {

        $('#acceptBtn').bind('click', function (e)
        {
            _core.getService('Loading').Service.enableBlur('loaderClass');

            e.preventDefault();
            //window.open(
            //      _self.redirectURL,
            //      '_blank');
            var data = collectData();
            // add
           
                addData(data);
         
            bindPaginationEvent();
        });
    }
    // collect data
    function collectData() {
        for (var i = 0, l = _self.getItems.length; i < l; i++)
        {
            var _this = _self.getItems[i];
            var name = $(_this).attr('name');
            // appending to Data
            
            if ($(_this).attr("type") != "checkbox")
            {
                _self.finalData[name] = $(_this).val();
            } else
            {
                _self.finalData[name] = $(_this).is(":checked");
            }
        }
          
        return _self.finalData;
    }

    function filterPersonList(data) {
        
        // postService
       
        var postService = _core.getService('Post').Service;
        postService.postPartial(data, _self.filterURL, function (res) {
           
            _core.getService('Loading').Service.disableBlur('loaderClass');
            if (res != null) {
                $('#convictsList').empty().append(res)

                // bind pagination buttons
                bindAcceptBtn();
                bindPaginationEvent();
            }
        })
    }
    function importantWarning() {
        alert('Please Be carefull there is seiour error on your url');
        location.reload();
        return false;
    }
    
    function addData(data) {
        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.currentURL, function (res) {
            _core.getService('Loading').Service.disableBlur('loaderClass');
            if (res == null) {
                alert('serious error on reports');
            }
            else {
                window.open(
                  _self.redirectURL + "?key=" + res["key"],
                  '_blank' // <- This is what makes it open in a new window.
                );
            }
        });
    }
}



// creating class instance
var ReportController = new ReportControllerClass();

// creating object
var ReportControllerObject = {
    // important
    Name: "Report",

    Controller: ReportController
}

// registering controller object
_core.addController(ReportControllerObject);