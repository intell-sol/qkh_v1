﻿// convicts controller

/*****Main Function******/
var _Data_Primary_ControllerClass = function () {
    var _self = this;

    _self.getFileTableRowURL = '/_Popup_/Get_AddFileTableRow';
    _self.getMainDataURL = '/_Data_Primary_/GetData';
    _self.getPrevConvictionsTableRowURL = '/_Popup_/Get_PreviousConvictionsTableRows';
    
    _self.callback = null;
    _self.PrisonerID = null;

    _self.mode = "Add" // initial mode


    _self.init = function (Action) {
        console.log('_Data_Primary_ Controller Inited with action', Action);

        _self.mode = Action;

        // Js Bindings
        bindButtons();

        // bind file events
        bindFilesEvents();
    }

    // add action
    _self.Add = function() {
        console.log('_Data_Primary_ add called');

        _self.init('Add');

    }

    // edit action
    _self.Edit = function (PrisonerID) {

        console.log('_Data_Primary_ edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;

    }

    _self.save = function (callback) {
        console.log('_Data_Primary_ data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }

    // js bindings
    function bindButtons() {

        var addPrevConviction = $('#addPrevConviction');
        var addFileImage = $('#addfileimage');
        var addFileBtn = $('#addfileMain');
        var prevConvictionWrapChange = $('#personal-old-sentence');

        addPrevConviction.unbind();
        addPrevConviction.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var PrevConvictions = _core.getWidget('PrevConvictions').Widget;

            // run
            PrevConvictions.Add(_self.PrisonerID, function (res) {
                if (res != null) {

                    // get cell list
                    getPrevConvictionsList();
                }
            });
        });

        addFileImage.unbind();
        addFileImage.bind('click', function () {

            var data = {};
            data.TypeID = 1;
            data.PrisonerID = _self.PrisonerID;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });

        addFileBtn.unbind();
        addFileBtn.bind('click', function () {

            var data = {};
            data.TypeID = 2;
            data.PrisonerID = _self.PrisonerID;

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (data1) {
                getFileList(data.TypeID);
            });
        });

        prevConvictionWrapChange.unbind().bind('change', function () {
            var curValue = $(this).val();
            var oldSentence = $('#personal-old-sentence-wrap');

            if (curValue == "1") oldSentence.removeClass('hidden');
            else oldSentence.addClass('hidden');
        });

        // selectize
        $(".selectize").each(function () {
            $(this).selectize({
                create: false
            });
        });

        bindPrevConcvitionsRowEvents();

    }

    // bind items table rows events
    function bindPrevConcvitionsRowEvents() {
        var removeRows = $('.removePrevConvictionRow');
        var editRows = $('.editPrevConvictionRow');
        var viewPevConvButton = $('.viewPevConvButton');

        // remove row
        removeRows.unbind();
        removeRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {
                if (res) {

                    // call add file widget (image type)
                    var addCellWidget = _core.getWidget('PrevConvictions').Widget;

                    // run
                    addCellWidget.Remove(id, function (res) {
                        if (res != null) {

                            // get cell list
                            getPrevConvictionsList();
                        }
                    });
                }
            })
        });

        // edit row
        editRows.unbind();
        editRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addCellWidget = _core.getWidget('PrevConvictions').Widget;

            // run
            addCellWidget.Edit(id, function (res) {

                if (res != null) {

                    // get cell list
                    getPrevConvictionsList();
                }
            });
        });
        // edit row
        viewPevConvButton.unbind();
        viewPevConvButton.bind('click', function ()
        {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addCellWidget = _core.getWidget('PrevConvictions').Widget;

            // run
            addCellWidget.View(id, function (res)
            {
            });
        });
    }

    // bind files events
    function bindFilesEvents() {

        var removeBtn = $('.removeFileButton');
        var editBtn = $('.editFileButton');
        var viewFileButton = $('.viewFileButton');

        // remove file actin handle
        removeBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Remove(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });

        // edit file action handle
        editBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Edit(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });

        // edit file action handle
        viewFileButton.unbind().click(function ()
        {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.View(data, function (res)
            {
            })
        });
    }

    // get image file list table rows
    function getFileList(type) {
           
        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.TypeID = type;

        // post service
        var postService = _core.getService('Post').Service;

        var curclass = '';

        if (parseInt(type) == 1) curclass = 'imageLoader';
        else if (parseInt(type) == 2) curclass = 'fileLoader';

        _core.getService('Loading').Service.enableBlur(curclass);

        postService.postPartial(data, _self.getFileTableRowURL, function (curHtml) {

            var table = null;
            if (parseInt(type) == 1) table = $('#imagefileTableBody');
            else if (parseInt(type) == 2) table = $('#allfileTableBody');

            _core.getService('Loading').Service.disableBlur(curclass);

            table.empty();
            table.append(curHtml);

            // bind files events
            bindFilesEvents();
        });
    }

    // get prev conviction list

    // get cell list table rows
    function getPrevConvictionsList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('prevLoader');

        postService.postPartial(data, _self.getPrevConvictionsTableRowURL, function (curHtml) {

            var table = null;
            table = $('#PrevConvTableBody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('prevLoader');

            bindPrevConcvitionsRowEvents();
        });
    }
    
}
/************************/


// creating class instance
var _Data_Primary_Controller = new _Data_Primary_ControllerClass();

// creating object
var _Data_Primary_ControllerObject = {
    // important
    Name: '_Data_Primary_',

    Controller: _Data_Primary_Controller
}

// registering controller object
_core.addController(_Data_Primary_ControllerObject);