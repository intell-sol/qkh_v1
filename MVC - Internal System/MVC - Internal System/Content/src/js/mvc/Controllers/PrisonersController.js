﻿// prisoners controller

/*****Main Function******/
var PrisonersControllerClass = function () {
    var self = this;

    // action list
    self.actionList = {
        'Index': Index,
        'Add': Add
    };

    self.init = function (Action) {
        console.log('Prisoners Controller Inited with action', Action);
        // calling function
        self.actionList[Action]();
    }

    // index action
    function Index() {
        console.log('index called');
    }

    // add action
    function Add() {
        console.log('add called');
    }
}
/************************/


// creating class instance
var PrisonersController = new PrisonersControllerClass();

// creating object
var PrisonersControllerObject = {
    // important
    Name: 'Prisoners',

    Controller: PrisonersController
}

// registering controller object
_core.addController(PrisonersControllerObject);