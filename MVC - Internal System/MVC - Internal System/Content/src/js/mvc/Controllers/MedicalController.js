﻿// Medical controller

/*****Main Function******/
var MedicalControllerClass = function () {
    var _self = this;

    _self.getFileTableRowURL = '/_Popup_/Get_AddFileTableRow';
    _self.callback = null;
    _self.PrisonerID = null;
    _self.PrisonerType = null;
    _self.getItems = null;
    _self.HistoryID = null;
    _self.HistoryType = null;


    _self.controllerName = {};
    _self.controllerName[2] = "Convicts";
    _self.controllerName[3] = "Prisoners";

    _self.getSentenceTableRowURL = '/_Popup_/Get_SentenceTableRows';
    _self.getProtocolTableRowURL = '/_Popup_/Get_SentenceProtocolTableRows';
    _self.getComplaintsDataTableRowURL = '/_Popup_/Get_MedicalComplaintTableRows';
    _self.getDiagnosisDataTableRowURL = '/_Popup_/Get_MedicalDiagnosisTableRows';
    _self.getExternalExaminationsDataTableRowURL = '/_Popup_/Get_ExternalExaminationTableRows';
    _self.getHealingDataTableRowURL = '/_Popup_/Get_MedicalHealingTableRows';
    _self.getResearchDataTableRowURL = '/_Popup_/Get_MedicalResearchTableRows';
    _self.getHistoryDataTableRowURL = '/_Popup_/Get_MedicalHistoryTableRows';
    _self.SaveHelthURL = '/Medical/EditHealthData';
    _self.loadHistoryAmbulatorURL = '/Medical/GetHistoryAmbulator';
    _self.loadHistoryStationaryURL = '/Medical/GetHistoryStatonary';
    _self.initalURL = '/Medical/Edit';

    _self.mode = "Add" // initial mode

    _self.init = function (Action) {
        console.log('Medical Controller Inited with action', Action);

        _self.mode = Action;

        _self.callback = null;
        _self.PrisonerID = null;
        _self.PrisonerType = null;
        _self.HistoryType = null;

        _self.HistoryID = 1;

        // bind file events
        bindFilesEvents();
    }

    // add action
    _self.Add = function () {
        console.log('_Data_Enterance_ add called');

        _self.init('Add');
    }

    // edit action
    _self.Edit = function (PrisonerID, PrisonerType) {

        console.log('Medical edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;

        _self.PrisonerType = PrisonerType;

        // bind Index events
        bindIndexEvents();

        // defualt Preliminary is laoded and bind evenets
        bindPreliminaryEvents();

        bindComplaintDataEvents();
        bindExternalExaminationDataEvents();
        bindDiagnosisDataEvents();
    }

    _self.save = function (callback) {
        console.log('Medical data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }

    // bind Index events
    function bindIndexEvents() {

        var preliminaryButton = $('#preliminaryButton');
        var ambulatorButton = $('#ambulatorButton');
        var stationeryButton = $('#stationeryButton');

        preliminaryButton.unbind().click(function (e) {
            e.preventDefault();

            _self.HistoryType = null;

            data = {};
            data['url'] = '/Medical/GetPreliminary'
            data['page'] = 'preliminary';
            data['contentHolder'] = 'medicalTabContentHolder';
            data['bindFunction'] = bindPreliminaryEvents;
            loadView(data);
            tabEvents(this);
        });
        ambulatorButton.unbind().click(function (e) {
            e.preventDefault();
            
            _self.HistoryType = 2;

            data = {};
            data['url'] = '/Medical/GetAmbulator'
            data['page'] = 'ambulator';
            data['contentHolder'] = 'medicalTabContentHolder';
            data['bindFunction'] = bindAmbulatorEvents;
            loadView(data);
            tabEvents(this);
        });
        stationeryButton.unbind().click(function (e) {
            e.preventDefault();

            _self.HistoryType = 3;

            data = {};
            data['url'] = '/Medical/GetStationery'
            data['page'] = 'stationery';
            data['contentHolder'] = 'medicalTabContentHolder';
            data['bindFunction'] = bindStationeryEvents;
            loadView(data);
            tabEvents(this);
        });
    }

    // bind Preliminary events
    function bindPreliminaryEvents() {

        var acceptHealthButton = $('#acceptHealthButton');
        var addComplaintButton = $('#addComplaintButton');
        var addDiagnosisButton = $('#addDiagnosisButton');
        var addExternalExaminationButton = $('#addExternalExaminationButton');
        var addFileBtn = $('#addfileBtn');
        var checkItems = $('.checkValidateHealth');

        // get items
        _self.getItems = $('.getItemsHealth');

        addFileBtn.unbind();
        addFileBtn.bind('click', function () {

            var data = {};
            data.TypeID = 34;
            data.PrisonerID = _self.PrisonerID;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });

        // health save button
        acceptHealthButton.unbind().click(function(e){
            e.preventDefault();

            var data = {};

            data = collectPrimaryData();

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // post service
            var postService = _core.getService('Post').Service;

            postService.postJson(data, _self.SaveHelthURL, function (res) {

                _core.getService('Loading').Service.disableBlur('loaderClass');

                if (typeof res.needApprove != "undefined" && res.needApprove) {
                    $('.medicalMergeWarningClass').removeClass("hidden");
                    $('.mainMergeWarningClass').removeClass("hidden");
                }

                var acceptBtn = $('#acceptHealthButton');
                if (res.status) {
                    acceptBtn.attr('disabled', true);
                }
                else {
                    acceptBtn.attr('disabled', false);
                }
            });
        });

        // bind add Complaints button
        addComplaintButton.unbind();
        addComplaintButton.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalComplaints').Widget;

            // run
            addArrsetWidget.Add(_self.PrisonerID, _self.HistoryID, function (res) {
                if (res != null && res.status) {
                    // get arrest list
                    getComplaintsDataList();
                }
            });
        });

        // bind add Complaints button
        addDiagnosisButton.unbind();
        addDiagnosisButton.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalDiagnosis').Widget;

            // run
            addArrsetWidget.Add(_self.PrisonerID, _self.HistoryID, function (res) {
                if (res != null && res.status) {
                    // get arrest list
                    getDiagnosisDataList();
                }
            });
        });

        // bind add Complaints button
        addExternalExaminationButton.unbind();
        addExternalExaminationButton.bind('click', function () {
        
            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalExternalExaminations').Widget;

            // run
            addArrsetWidget.Add(_self.PrisonerID, _self.HistoryID, function (res) {
                if (res != null && res.status) {
                    // get arrest list
                    getExternalExaminationsDataList();
                }
            });
        });

        // checkItems validate
        // check validate and toggle accept button
        checkItems.unbind().bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    action = true;
                }
            });

            $('#acceptHealthButton').attr('disabled', action);
        });

        // selectize
        $(".selectize").each(function () {
            $(this).selectize({
                create: false
            });
        });

        // file events
        bindFilesEvents();
    }
    
    // bind Ambulator events
    function bindAmbulatorEvents() {

        var addHistoryButton = $('#addHistoryButton');

        // bind add Complaints button
        addHistoryButton.unbind();
        addHistoryButton.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalHistory').Widget;

            // run
            addArrsetWidget.Add(_self.PrisonerID, 2, function (res) {
                if (res != null && res.status) {
                    // get arrest list
                    getHistoryDataList(2);
                }
            });
        });

        // bind history data row Events
        bindHistoryDataEvents();
        bindFilesEvents();
    }

    // bind Ambulator events
    function bindStationeryEvents() {

        var addHistoryButton = $('#addHistoryButton');

        // bind add Complaints button
        addHistoryButton.unbind();
        addHistoryButton.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalHistory').Widget;

            // run
            addArrsetWidget.Add(_self.PrisonerID, 3, function (res) {
                if (res != null && res.status) {
                    // get arrest list
                    getHistoryDataList(3);
                }
            });
        });

        // bind history data row Events
        bindHistoryDataEvents();
        bindFilesEvents();
    }

    function collectPrimaryData() {
        var data = {};

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

                // appending to Data
            data[name] = $(this).val();
        });

        // appending prisonerID
        data['PrisonerID'] = _self.PrisonerID;

        return data;
    }

    // load initial
    function loadInitial(type) {

        var url = _self.initalURL;
        var contentHolder = "partialViewHolder";

        var sendData = {};

        sendData['PrisonerID'] = _self.PrisonerID
        sendData['Type'] = type;

        $('#' + contentHolder).empty();

        _core.getService("Loading").Service.enableClearByID("partialViewHolder");

        _core.getService("Post").Service.postPartial(sendData, url, function (res) {

            // content
            $('#' + contentHolder).append(res);

            // bind Index events
            bindIndexEvents();
            $('.tabMenus').removeClass('active');

            if (type == 1) {
                // defualt Preliminary is laoded and bind evenets
                bindPreliminaryEvents();

                bindComplaintDataEvents();
                bindExternalExaminationDataEvents();
                bindDiagnosisDataEvents();
                $('#preliminaryButton').parent('li').addClass('active');
            }
            else if (type == 2) {
                bindAmbulatorEvents(); 
                $('#ambulatorButton').parent('li').addClass('active');
            }
            else if (type == 3) {
                bindStationeryEvents();
                $('#stationeryButton').parent('li').addClass('active');
            }

            _core.getService("Loading").Service.disableClearByID("partialViewHolder");
        });
    }

    // load view function
    function loadView(data) {

        var url = data.url;
        var contentHolder = data.contentHolder;
        var bindCallback = data.bindFunction;
        
        var sendData = {};

        sendData['PrisonerID'] = _self.PrisonerID;

        $('#' + contentHolder).empty();
        _core.getService("Loading").Service.enableClearByID(contentHolder);

        _core.getService("Post").Service.postPartial(sendData, url, function (res) {

            // content
            $('#' + contentHolder).append(res);

            // bind events
            bindCallback();

            _core.getService("Loading").Service.disableClearByID(contentHolder);
        });
    }

    // load view function
    function loadHistoryView(data) {

        var url = _self.loadHistoryAmbulatorURL;
        if (parseInt(data.Type) == 3) url = _self.loadHistoryStationaryURL;
        var contentHolder = 'partialViewHolder';

        var sendData = data;

        sendData['PrisonerID'] = _self.PrisonerID;

        $('#' + contentHolder).empty();
        _core.getService("Loading").Service.enableClearByID("partialViewHolder");

        _core.getService("Post").Service.postPartial(sendData, url, function (res) {

            // content
            $('#' + contentHolder).empty();
            $('#' + contentHolder).append(res);

            // bind events
            bindHistoryEvents();

            bindComplaintDataEvents();
            bindResearchDataEvents();
            bindDiagnosisDataEvents();
            bindHealingDataEvents();
            bindMedicalHistoryResultEvent();

            _core.getService("Loading").Service.disableClearByID("partialViewHolder");
        });
    }

    // change tab active class
    function tabEvents(target) {
        $('.tabMenus').removeClass('active');
        $(target).parent('li').addClass('active');
    }

    // js bindings
    function bindButtons() {

        var addArrestDataBtn = $('#addarrestdataBtn');
        var addProtocolBtn = $('#addprotocolBtn');
        var addSentencingDataBtn = $('#addsentencedataBtn');
        var addSentenceFileBtn = $('#addfilesentenceBtn');
        var addFileBtn = $('#addfileBtn');

        addArrestDataBtn.unbind();
        addArrestDataBtn.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('ArrestData').Widget;

            // run
            addArrsetWidget.Add(_self.PrisonerID, function (res) {
                if (res != null && res.status) {
                    // get arrest list
                    getArrestDataList();
                }
            });
        });
        
        addProtocolBtn.unbind();
        addProtocolBtn.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addSentenceWidget = _core.getWidget('SentenceProtocol').Widget;

            // run
            addSentenceWidget.Add(_self.PrisonerID, function (res) {
                if (res != null && res.status) {

                    // get sentence protocol list
                    getProtocolList();
                }
            });
        });

        addSentencingDataBtn.unbind();
        addSentencingDataBtn.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addSentenceWidget = _core.getWidget('SentenceData').Widget;

            // run
            addSentenceWidget.Add(_self.PrisonerID, function (res) {
                if (res != null && res.status) {

                    if (res.isopen) {

                        // case open action
                        caseOpenAction()
                    }
                    else {

                        // get sentence protocol list
                        getSentenceList();
                    }
                }
            });
        });

        addSentenceFileBtn.unbind();
        addSentenceFileBtn.bind('click', function () {
        
            _core.getService('Loading').Service.enableBlur('loaderClass');

            var data = {};
            data.TypeID = 8;
            data.PrisonerID = _self.PrisonerID;

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });

        addFileBtn.unbind();
        addFileBtn.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            var data = {};
            data.TypeID = 9;
            data.PrisonerID = _self.PrisonerID;

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });
    }

    // arrest data row bindings
    function bindComplaintDataEvents() {

        var removeRows = $('.removeComplaintsRow');
        var editRows = $('.editComplaintsRow');
        var viewRows = $('.viewComplaintsRow');

        // remove
        removeRows.unbind().bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            var id = $(this).data('id');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    _core.getService('Loading').Service.enableBlur('complaintLoader');

                    // call arrest data widged
                    var addArrsetWidget = _core.getWidget('MedicalComplaints').Widget;

                    // run
                    addArrsetWidget.Remove(id, function (res) {

                        _core.getService('Loading').Service.disableBlur('complaintLoader');

                        if (res != null && res.status) {
                            // get arrest list
                            getComplaintsDataList();
                        }
                    });
                }
                });
        });
        
        // view
        viewRows.unbind().click(function ()
        {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalComplaints').Widget;

            // run
            addArrsetWidget.View(id, function (res)
            {

            
            });
        });
        // edit
        editRows.unbind().click(function () {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalComplaints').Widget;

            // run
            addArrsetWidget.Edit(id, function (res) {

                if (res != null && res.status) {
                    // get arrest list
                    getComplaintsDataList();
                }
            });
        });
    }

    // arrest data row bindings
    function bindHistoryDataEvents() {

        var removeRows = $('.removeHistoryRow');
        var loadRows = $('.loadHistoryRow');
        var editRows = $('.editHistoryRow');
        var viewRows = $('.viewHistoryRow');

        // remove
        removeRows.unbind().bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            var id = $(this).data('id');
            var type = $(this).data('type');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    _core.getService('Loading').Service.enableBlur('historyLoader');

                    // call arrest data widged
                    var addArrsetWidget = _core.getWidget('MedicalHistory').Widget;

                    // run
                    addArrsetWidget.Remove(id, function (res) {
                        _core.getService('Loading').Service.disableBlur('historyLoader');
                        if (res != null && res.status) {
                            // get arrest list
                            getHistoryDataList(type);
                        }
                    });
                }
            });
        });
        
        viewRows.unbind().click(function ()
        {

            var id = $(this).data('id');
            var type = $(this).data('type');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalHistory').Widget;

            // run
            addArrsetWidget.View(id, function (res)
            {

            });
        });
        // edit
        editRows.unbind().click(function () {

            var id = $(this).data('id');
            var type = $(this).data('type');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalHistory').Widget;

            // run
            addArrsetWidget.Edit(id, function (res) {

                if (res != null && res.status) {
                    // get arrest list
                    getHistoryDataList(type);
                }
            });
        });

        // load
        loadRows.unbind().click(function (e) {
            e.preventDefault();

            var id = $(this).data('id');
            var type = $(this).data('type')

            if (id != null && type != null) {
                var data = {};
                data['ID'] = id;
                data['Type'] = type;
                loadHistoryView(data);
                _self.HistoryID = parseInt(id);
            }
        });
    }

    // arrest data row bindings
    function bindHistoryEvents() {

        var addComplaintButton = $('#addComplaintButton');
        var addDiagnosisButton = $('#addDiagnosisButton');
        var addResearchButton = $('#addResearchButton');
        var addHealingButton = $('#addHealingButton');
        var loadBackAmbulatorButton = $('#loadBackAmbulatorButton');
        var loadBackStationeryButton = $('#loadBackStationeryButton');

        // bind add Complaints button
        addComplaintButton.unbind();
        addComplaintButton.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalComplaints').Widget;

            // run
            addArrsetWidget.Add(_self.PrisonerID, _self.HistoryID, function (res) {
                if (res != null && res.status) {
                    // get arrest list
                    getComplaintsDataList();
                }
            });
        });

        // bind add Complaints button
        addDiagnosisButton.unbind();
        addDiagnosisButton.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalDiagnosis').Widget;

            // run
            addArrsetWidget.Add(_self.PrisonerID, _self.HistoryID, function (res) {
                if (res != null && res.status) {
                    // get arrest list
                    getDiagnosisDataList();
                }
            });
        });

        // bind add Complaints button
        addResearchButton.unbind();
        addResearchButton.bind('click', function () {
            
            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalResearchs').Widget;

            // run
            addArrsetWidget.Add(_self.PrisonerID, _self.HistoryID, function (res) {
                if (res != null && res.status) {
                    // get arrest list
                    getResearchsDataList();
                }
            });
        });

        // bind add Complaints button
        addHealingButton.unbind();
        addHealingButton.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalHealings').Widget;

            // run
            addArrsetWidget.Add(_self.PrisonerID, _self.HistoryID, function (res) {
                if (res != null && res.status) {
                    // get arrest list
                    getHealingsDataList();
                }
            });
        });

        // bind go to ambulator button
        loadBackAmbulatorButton.unbind();
        loadBackAmbulatorButton.click(function (e) {
            e.preventDefault();

            loadInitial(2);
        });

        // bind go to ambulator button
        loadBackStationeryButton.unbind();
        loadBackStationeryButton.click(function (e) {
            e.preventDefault();

            loadInitial(3);
        });
    }

    // arrest data row bindings
    function bindDiagnosisDataEvents() {

        var removeRows = $('.removeDiagnosisRow');
        var editRows = $('.editDiagnosisRow');
        var viewRows = $('.viewDiagnosisRow');

        // remove
        removeRows.unbind().bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            var id = $(this).data('id');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    _core.getService('Loading').Service.enableBlur('diagnosisLoader');

                    // call arrest data widged
                    var addArrsetWidget = _core.getWidget('MedicalDiagnosis').Widget;

                    // run
                    addArrsetWidget.Remove(id, function (res) {
                        _core.getService('Loading').Service.disableBlur('diagnosisLoader');
                        if (res != null && res.status) {
                            // get arrest list
                            getDiagnosisDataList();
                        }
                    });
                }
            });
        });

        // edit
        editRows.unbind().click(function () {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalDiagnosis').Widget;

            // run
            addArrsetWidget.Edit(id, function (res) {

                if (res != null && res.status) {
                    // get arrest list
                    getDiagnosisDataList();
                }
            });
        });
        viewRows.unbind().click(function ()
        {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalDiagnosis').Widget;

            // run
            addArrsetWidget.View(id, function (res)
            {

                
            });
        });
    }

    // arrest data row bindings
    function bindResearchDataEvents() {

        var removeRows = $('.removeResearchRow');
        var editRows = $('.editResearchRow');

        // remove
        removeRows.unbind().bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            var id = $(this).data('id');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    _core.getService('Loading').Service.enableBlur('researchLoader');

                    // call arrest data widged
                    var addArrsetWidget = _core.getWidget('MedicalResearchs').Widget;

                    // run
                    addArrsetWidget.Remove(id, function (res) {

                        _core.getService('Loading').Service.disableBlur('researchLoader');

                        if (res != null && res.status) {
                            // get arrest list
                            getResearchsDataList();
                        }
                    });
                }
            });
        });

        // edit
        editRows.unbind().click(function () {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalResearchs').Widget;

            // run
            addArrsetWidget.Edit(id, function (res) {

                if (res != null && res.status) {
                    // get arrest list
                    getResearchsDataList();
                }
            });
        });
    }

    // arrest data row bindings
    function bindHealingDataEvents() {

        var removeRows = $('.removeHealingRow');
        var editRows = $('.editHealingRow');

        // remove
        removeRows.unbind().bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            var id = $(this).data('id');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    _core.getService('Loading').Service.enableBlur('healingLoader');

                    // call arrest data widged
                    var addArrsetWidget = _core.getWidget('MedicalHealings').Widget;

                    // run
                    addArrsetWidget.Remove(id, function (res) {

                        _core.getService('Loading').Service.disableBlur('healingLoader');

                        if (res != null && res.status) {
                            // get arrest list
                            getHealingsDataList();
                        }
                    });
                }
            });
        });

        // edit
        editRows.unbind().click(function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            var id = $(this).data('id');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalHealings').Widget;

            // run
            addArrsetWidget.Edit(id, function (res) {

                if (res != null && res.status) {
                    // get arrest list
                    getHealingsDataList();
                }
            });
        });
    }

    // arrest data row bindings
    function bindExternalExaminationDataEvents() {

        var removeRows = $('.removeExternalExaminationsRow');
        var editRows = $('.editExternalExaminationsRow');
        var viewRows = $('.viewExternalExaminationsRow');

        // remove
        removeRows.unbind().bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            var id = $(this).data('id');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    _core.getService('Loading').Service.enableBlur('externalLoader');

                    // call arrest data widged
                    var addArrsetWidget = _core.getWidget('MedicalExternalExaminations').Widget;

                    // run
                    addArrsetWidget.Remove(id, function (res) {

                        _core.getService('Loading').Service.disableBlur('externalLoader');

                        if (res != null && res.status) {
                            // get arrest list
                            getExternalExaminationsDataList();
                        }
                    });
                }
            });
        });
        
        // View
        viewRows.unbind().click(function ()
        {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalExternalExaminations').Widget;

            // run
            addArrsetWidget.View(id, function (res)
            {

              
            });
        });
        // edit
        editRows.unbind().click(function () {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalExternalExaminations').Widget;

            // run
            addArrsetWidget.Edit(id, function (res) {

                if (res != null && res.status) {
                    // get arrest list
                    getExternalExaminationsDataList();
                }
            });
        });
    }

    // bind medical history result events
    function bindMedicalHistoryResultEvent() {

        var historyResult = $('#medicalHistoryResultButton');

        // edit
        historyResult.unbind().click(function () {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('MedicalHistoryResult').Widget;

            // run
            addArrsetWidget.Edit(_self.HistoryID, function (res) {

                if (res != null && res.status) {
                }
            });
        });
    }

    // bind files events
    function bindFilesEvents() {

        var removeBtn = $('.removeFileButton');
        var editBtn = $('.editFileButton');
        var viewBtn = $('.viewFileButton');

        // remove file actin handle
        removeBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Remove(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
        
        // view file action handle
        viewBtn.unbind().click(function ()
        {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.View(data, function (res)
            {
               
            })
        });
        // edit file action handle
        editBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Edit(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
    }
    
    // get list table rows
    function getHistoryDataList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.Type = type;

        _core.getService('Loading').Service.enableBlur('historyLoader');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getHistoryDataTableRowURL, function (curHtml) {

            var table = null;
            table = $('#historyTableBody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('historyLoader');

            // bindings
            bindHistoryDataEvents();
        });
    }

    // get list table rows
    function getComplaintsDataList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.MedicalHistoryID = _self.HistoryID;

        _core.getService('Loading').Service.enableBlur('complaintLoader');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getComplaintsDataTableRowURL, function (curHtml) {

            var table = null;
            table = $('#complaintstable');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('complaintLoader');

            // bindings
            bindComplaintDataEvents();
        });
    }

    // get list table rows
    function getDiagnosisDataList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.MedicalHistoryID = _self.HistoryID;

        _core.getService('Loading').Service.enableBlur('diagnosisLoader');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getDiagnosisDataTableRowURL, function (curHtml) {

            var table = null;
            table = $('#diagnosisTableBody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('diagnosisLoader');

            // bindings
            bindDiagnosisDataEvents();
        });
    }

    // get list table rows
    function getResearchsDataList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.MedicalHistoryID = _self.HistoryID;

        _core.getService('Loading').Service.enableBlur('researchLoader');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getResearchDataTableRowURL, function (curHtml) {

            var table = null;
            table = $('#researchTableBody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('researchLoader');

            // bindings
            bindResearchDataEvents();
        });
    }

    // get list table rows
    function getHealingsDataList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.MedicalHistoryID = _self.HistoryID;

        _core.getService('Loading').Service.enableBlur('healingLoader');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getHealingDataTableRowURL, function (curHtml) {

            var table = null;
            table = $('#healingTableBody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('healingLoader');

            // bindings
            bindHealingDataEvents();
        });
    }

    // get list table rows
    function getExternalExaminationsDataList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.MedicalHistoryID = _self.HistoryID;

        _core.getService('Loading').Service.enableBlur('externalLoader');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getExternalExaminationsDataTableRowURL, function (curHtml) {

            var table = null;
            table = $('#externalExaminationTable');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('externalLoader');

            // bindings
            bindExternalExaminationDataEvents();
        });
    }

    // get list table rows
    function getHistoryDataList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.Type = type;

        _core.getService('Loading').Service.enableBlur('historyLoader');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getHistoryDataTableRowURL, function (curHtml) {

            var table = null;
            table = $('#historyTableBody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('historyLoader');

            // bindings
            bindHistoryDataEvents();
        });
    }

    // get file list table rows
    function getFileList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.TypeID = type;

        _core.getService('Loading').Service.enableBlur('fileLoader');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getFileTableRowURL, function (curHtml) {

            var table = null;
            table = $('#addfiletablebody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('fileLoader');

            // bind file Events
            bindFilesEvents();
        });
    }
}
/************************/


// creating class instance
var MedicalController = new MedicalControllerClass();

// creating object
var MedicalControllerObject = {
    // important
    Name: 'Medical',

    Controller: MedicalController
}

// registering controller object
_core.addController(MedicalControllerObject);