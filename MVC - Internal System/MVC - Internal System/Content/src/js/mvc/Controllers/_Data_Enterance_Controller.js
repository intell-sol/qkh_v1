﻿// Enterance controller

/*****Main Function******/
var _Data_Enterance_ControllerClass = function () {
    var _self = this;

    _self.getFileTableRowURL = '/_Popup_/Get_AddFileTableRow';
    _self.callback = null;
    _self.PrisonerID = null;
    _self.PrisonerType = null;

    _self.controllerName = {};
    _self.controllerName[2] = "Convicts";
    _self.controllerName[3] = "Prisoners";

    _self.getSentenceTableRowURL = '/_Popup_/Get_SentenceTableRows';
    _self.getPrisonerTypeStatusTableRowURL = '/_Popup_/Get_PrisonerTypeStatusTableRow';
    _self.getPrisonerPunishmentTypeTableRowURL = '/_Popup_/Get_PrisonerPunishmentTypeTableRow';
    _self.getProtocolTableRowURL = '/_Popup_/Get_SentenceProtocolTableRows';
    _self.getArrestDataTableRowURL = '/_Popup_/Get_ArrestDataTableRows';

    _self.mode = "Add" // initial mode

    _self.init = function (Action) {
        console.log('_Data_Enterance_ Controller Inited with action', Action);

        _self.mode = Action;

        _self.callback = null;
        _self.PrisonerID = null;
        _self.PrisonerType = null;

        // Js Bindings
        bindButtons();
        
        // sentence table row bindings
        bindSentenceEvents();

        // protocol table row bindings
        bindProtocolEvents();

        // arrest data table row bindings
        bindArrestDataEvents();

        // prisoner type status row bindings
        bindPrisonerTypeStatusEvents();

        // prisoner punishment type row bindings
        bindPrisonerPunishmentTypeEvents();

        // bind file events
        bindFilesEvents();
    }

    // add action
    _self.Add = function () {
        console.log('_Data_Enterance_ add called');

        _self.init('Add');
    }

    // edit action
    _self.Edit = function (PrisonerID, PrisonerType) {

        console.log('_Data_Enterance_ edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;

        _self.PrisonerType = PrisonerType;
    }

    _self.save = function (callback) {
        console.log('_Data_Enterance_ data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }

    // js bindings
    function bindButtons() {

        var addArrestDataBtn = $('#addarrestdataBtn');
        var addProtocolBtn = $('#addprotocolBtn');
        var addSentencingDataBtn = $('#addsentencedataBtn');
        var addSentenceFileBtn = $('#addfilesentenceBtn');
        var addprisonertypestatus = $('#addprisonertypestatus');
        var addprisonerpunishmenttype = $('#addprisonerpunishmenttype');
        var addFileBtn = $('#addfileBtn');

        addArrestDataBtn.unbind();
        addArrestDataBtn.bind('click', function () {

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('ArrestData').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addArrsetWidget.Add(_self.PrisonerID, function (res) {
                if (res != null && res.status) {

                    if (res.isopen) {

                        // case open action
                        caseOpenAction()
                    }
                    else {

                        // get arrest list
                        getArrestDataList();
                    }
                }
            });
        });
        
        addProtocolBtn.unbind();
        addProtocolBtn.bind('click', function () {

            // call add file widget (image type)
            var addSentenceWidget = _core.getWidget('SentenceProtocol').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addSentenceWidget.Add(_self.PrisonerID, function (res) {
                if (res != null && res.status) {

                    // get sentence protocol list
                    getProtocolList();
                }
            });
        });


        addprisonertypestatus.unbind();
        addprisonertypestatus.bind('click', function () {

            // call add file widget (image type)
            var addSentenceWidget = _core.getWidget('PrisonerTypeStatus').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addSentenceWidget.Add(_self.PrisonerID, function (res) {
                if (res != null && res.status) {

                    // get sentence protocol list
                    getPrisonerTypeStatusList();
                }
            });
        });

        addprisonerpunishmenttype.unbind();
        addprisonerpunishmenttype.bind('click', function () {

            // call add file widget (image type)
            var addSentenceWidget = _core.getWidget('PrisonerPunishmentType').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addSentenceWidget.Add(_self.PrisonerID, function (res) {
                if (res != null && res.status) {

                    // get sentence protocol list
                    getPrisonerPunishmentTypeList();
                }
            });
        });

        addSentencingDataBtn.unbind();
        addSentencingDataBtn.bind('click', function () {

            // call add file widget (image type)
            var addSentenceWidget = _core.getWidget('SentenceData').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addSentenceWidget.Add(_self.PrisonerID, function (res) {
                if (res != null && res.status) {

                    if (res.isopen) {

                        // case open action
                        caseOpenAction()
                    }
                    else {

                        // get sentence protocol list
                        getSentenceList();
                    }
                }
            });
        });

        addSentenceFileBtn.unbind();
        addSentenceFileBtn.bind('click', function () {

            var data = {};
            data.TypeID = 8;
            data.PrisonerID = _self.PrisonerID;

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });

        addFileBtn.unbind();
        addFileBtn.bind('click', function () {

            var data = {};
            data.TypeID = 9;
            data.PrisonerID = _self.PrisonerID;

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });
    }

    // sentence row bindings
    function bindSentenceEvents() {

        var removeBtn = $('.removeSentenceRow');
        var editBtn = $('.editSentenceRow');
        var viewBtn = $('.viewSentenceRow');

        // remove
        removeBtn.unbind().bind('click', function () {

            var id = $(this).data('id');

            // call arrest data widged
            var addSentenceWidget = _core.getWidget('SentenceData').Widget;
            
            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addSentenceWidget.Remove(id, function (res) {
                if (res != null && res.status) {

                    // get sentence list
                    getSentenceList();
                }
            });
        });
        // edit
        viewBtn.unbind().click(function ()
        {

            var id = $(this).data('id');

            // call arrest data widged
            var addSentenceWidget = _core.getWidget('SentenceData').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addSentenceWidget.View(id, function (res)
            {

            });
        });
        // edit
        editBtn.unbind().click(function () {

            var id = $(this).data('id');

            // call arrest data widged
            var addSentenceWidget = _core.getWidget('SentenceData').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addSentenceWidget.Edit(id, function (res) {

                if (res != null && res.status) {

                    // get sentence list
                    getSentenceList();
                }
            });
        });
    }

    // protocol row bindings
    function bindProtocolEvents() {

        var removeRows = $('.removeProtocolRow');
        var editRows = $('.editProtocolRow');
        var viewRows = $('.viewProtocolRow');

        // remove
        removeRows.unbind().bind('click', function () {

            var id = $(this).data('id');

            // call arrest data widged
            var addSentenceProtocolWidget = _core.getWidget('SentenceProtocol').Widget;
            
            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addSentenceProtocolWidget.Remove(id, function (res) {
                if (res != null && res.status) {

                    // get sentence protocol list
                    getProtocolList();
                }
            });
        });
        
        // view
        viewRows.unbind().click(function ()
        {

            var id = $(this).data('id');

            // call arrest data widged
            var addSentenceProtocolWidget = _core.getWidget('SentenceProtocol').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addSentenceProtocolWidget.View(id, function (res)
            {

            });
        });
        // edit
        editRows.unbind().click(function () {

            var id = $(this).data('id');

            // call arrest data widged
            var addSentenceProtocolWidget = _core.getWidget('SentenceProtocol').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addSentenceProtocolWidget.Edit(id, function (res) {

                if (res != null && res.status) {

                    // get sentence protocol list
                    getProtocolList();
                }
            });
        });
    }
    
    // prisoner type status row bindings
    function bindPrisonerTypeStatusEvents() {

        var removeRows = $('.removePrisonerTypeStatusRow');
        var editRows = $('.editPrisonerTypeStatusRow');
        var viewRows = $('.viewPrisonerTypeStatusRow');

        // remove
        removeRows.unbind().bind('click', function () {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    // call arrest data widged
                    var addArrsetWidget = _core.getWidget('PrisonerTypeStatus').Widget;

                    // run
                    addArrsetWidget.Remove(id, function (res) {
                        if (res != null && res.status) {
                            // get arrest list
                            getPrisonerTypeStatusList();
                        }
                    });
                }
            });
        });
        
        // view
        viewRows.unbind().click(function ()
        {

            var id = $(this).data('id');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('PrisonerTypeStatus').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addArrsetWidget.View(id, function (res)
            {

            });
        });
        // edit
        editRows.unbind().click(function () {

            var id = $(this).data('id');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('PrisonerTypeStatus').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addArrsetWidget.Edit(id, function (res) {

                if (res != null && res.status) {
                    // get arrest list
                    getPrisonerTypeStatusList();
                }
            });
        });
    }

    // prisoner punishment type row bindings
    function bindPrisonerPunishmentTypeEvents() {

        var removeRows = $('.removePrisonerPunishmentTypeRow');
        var editRows = $('.editPrisonerPunishmentTypeRow');
        var viewRows = $('.viewPrisonerPunishmentTypeRow');

        // remove
        removeRows.unbind().bind('click', function () {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    // call arrest data widged
                    var addArrsetWidget = _core.getWidget('PrisonerPunishmentType').Widget;

                    // run
                    addArrsetWidget.Remove(id, function (res) {
                        if (res != null && res.status) {
                            // get arrest list
                            getPrisonerPunishmentTypeList();
                        }
                    });
                }
            });
        });

        // view
        viewRows.unbind().click(function () {

            var id = $(this).data('id');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('PrisonerPunishmentType').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addArrsetWidget.View(id, function (res) {

            });
        });
        // edit
        editRows.unbind().click(function () {

            var id = $(this).data('id');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('PrisonerPunishmentType').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addArrsetWidget.Edit(id, function (res) {

                if (res != null && res.status) {
                    // get arrest list
                    getPrisonerPunishmentTypeList();
                }
            });
        });
    }

    // arrest data row bindings
    function bindArrestDataEvents() {

        var removeRows = $('.removeArrestDataRow');
        var editRows = $('.editArrestDataRow');
        var viewRows = $('.viewArrestDataRow');

        // remove
        removeRows.unbind().bind('click', function () {

            var id = $(this).data('id');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('ArrestData').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addArrsetWidget.Remove(id, function (res) {
                if (res != null && res.status) {
                    // get arrest list
                    getArrestDataList();
                }
            });
        });
        
        // view  
        viewRows.unbind().click(function ()
        {

            var id = $(this).data('id');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('ArrestData').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addArrsetWidget.View(id, function (res)
            {
            });
        });
      // edit  
        editRows.unbind().click(function () {

            var id = $(this).data('id');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('ArrestData').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addArrsetWidget.Edit(id, function (res) {

                if (res != null && res.status) {
                    // get arrest list
                    getArrestDataList();
                }
            });
        });
    }

    // bind files events
    function bindFilesEvents() {

        var removeBtn = $('.removeFileButton');
        var editBtn = $('.editFileButton');
        var viewBtn = $('.viewFileButton');

        // remove file actin handle
        removeBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Remove(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
        
        // view file action handle
        viewBtn.unbind().click(function ()
        {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.View(data, function (res)
            {
               
            })
        });
        // edit file action handle
        editBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Edit(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
    }

    // open case action
    function caseOpenAction() {

        var url = "/" + _self.controllerName[_self.PrisonerType] + "/Edit/" + _self.PrisonerID

        window.location.href = url;
    }

    // get sentence list table rows
    function getSentenceList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('sentenceLoader');

        postService.postPartial(data, _self.getSentenceTableRowURL, function (curHtml) {

            var table = null;
            table = $('#sentencedatatablebody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('sentenceLoader');

            // bindings
            bindSentenceEvents();
        });
    }

    // get protocol list table rows
    function getProtocolList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('arrestProtocolLoader');

        postService.postPartial(data, _self.getProtocolTableRowURL, function (curHtml) {

            var table = null;
            table = $('#protocoltablebody');

            table.empty();
            table.append(curHtml);
            
            _core.getService('Loading').Service.disableBlur('arrestProtocolLoader');

            // bindings
            bindProtocolEvents();
        });
    }
    
    // get prisoner type status table rows
    function getPrisonerTypeStatusList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('prisonertypestatusLoader');

        postService.postPartial(data, _self.getPrisonerTypeStatusTableRowURL, function (curHtml) {

            var table = null;
            table = $('#prisonertypestatustablebody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('prisonertypestatusLoader');

            // bindings
            bindPrisonerTypeStatusEvents();
        });
    }

    // get prisoner punishment type table rows
    function getPrisonerPunishmentTypeList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('prisonerpunishmenttypeLoader');

        postService.postPartial(data, _self.getPrisonerPunishmentTypeTableRowURL, function (curHtml) {

            var table = null;
            table = $('#prisonerpunishmenttypetablebody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('prisonerpunishmenttypeLoader');

            // bindings
            bindPrisonerPunishmentTypeEvents();
        });
    }

    // get protocol list table rows
    function getArrestDataList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('arrestDataLoader');

        postService.postPartial(data, _self.getArrestDataTableRowURL, function (curHtml) {

            var table = null;
            table = $('#arrestdatatablebody');

            table.empty();
            table.append(curHtml);
            
            _core.getService('Loading').Service.disableBlur('arrestDataLoader');

            // bindings
            bindArrestDataEvents();
        });
    }

    // get file list table rows
    function getFileList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.TypeID = type;

        // post service
        var postService = _core.getService('Post').Service;

        if (parseInt(type) == 9) curclass = 'fileLoader';
        else if (parseInt(type) == 8) curclass = 'fileSentenceLoader';

        _core.getService('Loading').Service.enableBlur(curclass);

        postService.postPartial(data, _self.getFileTableRowURL, function (curHtml) {

            var table = null;
            if (type == 9) table = $('#addfiletablebody');
            else if (type == 8) table = $('#addfilesentencetablebody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur(curclass);

            // bind file Events
            bindFilesEvents();
        });
    }
}
/************************/


// creating class instance
var _Data_Enterance_Controller = new _Data_Enterance_ControllerClass();

// creating object
var _Data_Enterance_ControllerObject = {
    // important
    Name: '_Data_Enterance_',

    Controller: _Data_Enterance_Controller
}

// registering controller object
_core.addController(_Data_Enterance_ControllerObject);