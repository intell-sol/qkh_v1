﻿// Cell controller

/*****Main Function******/
var _Data_Cell_ControllerClass = function () {
    var _self = this;

    _self.PrisonerID = null;

    _self.getFileTableRowURL = '/_Popup_/Get_AddFileTableRow';
    _self.getCellTableRowURL = '/_Popup_/Get_CellTableRows';


    _self.mode = "Add" // initial mode


    _self.init = function (Action) {
        console.log('_Data_Cell_ Controller Inited with action', Action);

        _self.mode = Action;

        _self.callback = null;
        _self.PrisonerID = null;

        // Js Bindings
        bindButtons();

        // items table row events
        bindCellRowEvents();

        // bind file events
        bindFilesEvents();
    }

    // add action
    _self.Add = function () {
        console.log('_Data_Cell_ add called');

        _self.init('Add');
    }

    // edit action
    _self.Edit = function (PrisonerID) {

        console.log('_Data_Cell_ edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;
    }

    _self.save = function (callback) {
        console.log('_Data_Cell_ data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }

    // js bindings
    function bindButtons() {

        var addCellBtn = $('#addcellobjectBtn');
        var addFileBtn = $('#addfileBtn');

        addCellBtn.unbind();
        addCellBtn.bind('click', function () {

            // call add file widget (image type)
            var addCellWidget = _core.getWidget('CellCard').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addCellWidget.Add(_self.PrisonerID, function (res) {
                if (res != null) {

                    // get cell list
                    getcellList();
                }
            });
        });

        addFileBtn.unbind();
        addFileBtn.bind('click', function () {

            var data = {};
            data.TypeID = 10;
            data.PrisonerID = _self.PrisonerID;
            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });
    }

    // bind items table rows events
    function bindCellRowEvents() {
        var removeRows = $('.removeCellRow');
        var editRows = $('.editCellRow');
        var viewRows = $('.viewCellRow');
        // remove row
        removeRows.unbind();
        removeRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {
                if (res) {

                    // call add file widget (image type)
                    var addCellWidget = _core.getWidget('CellCard').Widget;

                    // run
                    addCellWidget.Remove(id, function (res) {
                        if (res != null) {

                            // get cell list
                            getcellList();
                        }
                    });
                }
            })
        });
        viewRows.unbind();
        viewRows.bind('click', function ()
        {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addCellWidget = _core.getWidget('CellCard').Widget;

            // run
            addCellWidget.View(id, _self.PrisonerID, function (res)
            {

                
            });
        });
        // edit row
        editRows.unbind();
        editRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addCellWidget = _core.getWidget('CellCard').Widget;

            // run
            addCellWidget.Edit(id, _self.PrisonerID, function (res) {
                
                if (res != null) {

                    // get cell list
                    getcellList();
                }
            });
        });
    }

    // bind files events
    function bindFilesEvents() {

        var removeBtn = $('.removeFileButton');
        var editBtn = $('.editFileButton');
        var viewBtn = $('.viewFileButton');
        // remove file actin handle
        removeBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');
            _core.getService('Loading').Service.enableBlur('loaderClass');

            // postService
            _core.getWidget('AddFile').Widget.Remove(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
        viewBtn.unbind().click(function ()
        {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');
            _core.getService('Loading').Service.enableBlur('loaderClass');

            // postService
            _core.getWidget('AddFile').Widget.View(data, function (res)
            {
                
            })
        });
        // edit file action handle
        editBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');
            _core.getService('Loading').Service.enableBlur('loaderClass');

            // postService
            _core.getWidget('AddFile').Widget.Edit(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
    }

    // get file list table rows
    function getFileList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.TypeID = type;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getFileTableRowURL, function (curHtml) {

            var table = null;
            table = $('#addfileTableBody');

            table.empty();
            table.append(curHtml);

            // bind file events
            bindFilesEvents();
        });
    }

    // get cell list table rows
    function getcellList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        
        _core.getService('Loading').Service.enableBlur('celltablebody');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getCellTableRowURL, function (curHtml) {

            var table = null;
            table = $('#celltablebody');
            
            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('celltablebody');

            bindCellRowEvents();
        });
    }

}
/************************/


// creating class instance
var _Data_Cell_Controller = new _Data_Cell_ControllerClass();

// creating object
var _Data_Cell_ControllerObject = {
    // important
    Name: '_Data_Cell_',

    Controller: _Data_Cell_Controller
}

// registering controller object
_core.addController(_Data_Cell_ControllerObject);