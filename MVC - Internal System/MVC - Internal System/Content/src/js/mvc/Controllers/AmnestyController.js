﻿// Amnesty controller

/*****Main Function******/
var AmnestyControllerClass = function () {
    var self = this;
    self.mode = null;
    self.id = null;
    self.getItems = null;
    self.callback = null;
    self.addUrl = '/Amnesty/AddAmnesty';
    self.editUrl = '/Amnesty/EditAmnesty';
    self.removeUrl = '/Amnesty/RemData';
    self.finalData = {};

    self.init = function () {
        console.log('Amnesty Controller Inited with action');
        // calling function
        bindPopStateEvent();
        bindPaginationEvent();
    }

    // index action
    self.Index = function () {
        console.log('index called');
        //$(".selectizeOrgUnit").each(function ()
        //{
        //    $(this).selectize({
        //        create: false
        //    });
        //});
        bindPopStateEvent();
        bindPaginationEvent();
    }

    // add action
    self.Add = function (id, callback) {
        console.log('add called');
        self.init();

        self.mode = 'Add';
        self.callback = callback;
        self.bindEvents();
    }

    self.Edit = function (type, id, callback) {
        // initilize
        self.init();
        console.log('Edit called');
        self.id = id;
        self.mode = 'Edit';
        // save callback
        self.callback = callback;
        self.bindEvents();
    }

    self.Remove = function (id, callback) {
        // initilize
        self.init();

        console.log('Remove called');

        self.id = id;
        self.mode = 'Remove';
        // save callback
        self.callback = callback;
        removeData();
        self.bindEvents()
    };

    self.save = function (callback) {
        console.log('data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }
    self.bindEvents = function () {

        var acceptBtn = $('#acceptBtn');
        var checkItems = $('.checkValidate');
        var dates = $('.Dates');
        self.getItems = $('.getItems');
        acceptBtn.unbind();
        checkItems.unbind();

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                var name = $(this).attr('name');
                if ($(this).val() == '' || $(this).val() == null) {
                   
                    action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });
      
        // bind accept
        acceptBtn.bind('click', function (e) {
            e.preventDefault();
            var data = collectData();

            if (self.mode == 'Add') {

                // add

                addData(data);
            }
            else if (self.mode == 'Edit') {

                data.ID = self.id;

                // edit
                editData(data);
            };

        });

        // selectize
        $(".selectize").each(function () {
            $(this).selectize({
                create: false
            });
        });
        
        self.temp = $('.main>li').clone();

        events();
    };

    function events() {
        $(".addRow").unbind("click");
        $(".addRow").bind("click", function (e) {
            e.preventDefault();
            var cont = $(this).parent().parent().find(">ul");
            self.addRow(cont);
        });        
        $(".newclass").unbind("click");
        $(".newclass").bind("click", function (e) {
            e.preventDefault();          
        });
        $(".addChild").unbind("click");
        $(".addChild").bind("click", function (e) {
            e.preventDefault();
            if ($(this).attr('disabled') != "disabled") {
                var cont = $(this).parent().parent();
                self.addChild($(this));
            }
        });

        $(".removeRow").unbind("click");
        $(".removeRow").bind("click", function (e) {
            e.preventDefault();

            if ($(this).attr('disabled') != "disabled") {
                self.removeRow($(this));
            }
        });

        $('.checkValidatePoints').unbind().bind('change keyup', function () {
            var action = false;
            $('.checkValidatePoints').each(function () {
                var name = $(this).attr('name');
                if (($(this).val() == '' || $(this).val() == null) && !$(this).parent().parent().parent().hasClass('main')) {

                    action = true;
                }
            });

            $('#acceptBtn').attr("disabled", action);
            $('.checkValidate').trigger('change');
        })
    }

    self.addRow = function (ul) {
        var depth = ul.attr("data-depth");
        var lastIndex = ul.find(">li").length;
        var index = isNaN(lastIndex) ? 1 : lastIndex + 1;
        var row = self.temp.clone();

        var num = depth != "" && typeof depth != 'undefined' ? depth + "." + index : index;
        if (typeof depth != 'undefined' && depth.split(".").length >= 2) {
            row.find(".addChild").attr("disabled", "disabled");
            row.find(".addChild").removeClass("addChild").addClass('newclass');;
        }
        row.find("span").text(num);
        ul.append(row);
        row.find('input').attr('data-tempid', row.find("span").text());

        var parentRow = row.parent().parent().find("label>input").attr("data-id");
        if (parentRow != null && parentRow != "") {
            row.find('input').attr('data-id', parentRow);
            row.find('input').attr('data-parentid', row.parent().parent().find("label>input").attr("data-tempid"));
            if (row.parent().hasClass('mainone')) {
                row.find('input').attr('data-parentid', 0);
            } else {
                row.find('input').attr('data-parentid', row.parent().parent().find("label>input").attr("data-tempid"));
            }

        } else {
            row.find('input').attr('data-id', '');
            if (row.parent().hasClass('mainone')) {
                row.find('input').attr('data-parentid', 0);
            } else {
                row.find('input').attr('data-parentid', row.parent().parent().find("label>input").attr("data-tempid"));
            }

        }

        events();
    };

    self.addChild = function (button) {

        var row = button.parent().parent();
        var parent = row.parent();

        if (row.find(">ul").length > 0) {
            self.addRow(row.find(">ul"));
        } else {
            var num = row.find("span").text();
            var subUl = $("<ul class='x'></ul>");
            subUl.attr("data-depth", num);
            row.append(subUl);
            self.addRow(subUl);
        }

        events();
    };

    self.removeRow = function (button) {
        var row = button.parent().parent();
        var parent = row.parent();
        var depth = parent.attr("data-depth");
        if (row.find("span").text() != "1") {
            console.log(row.find("span").text());
            row.remove();
        }

        if (parent.find("li").length > 0) {
            parent.find(">li").each(function (index) {
                console.log(index);
                var num = depth != "" && typeof depth != 'undefined' ? depth + "." + (index + 1) : index + 1;
                $(this).find("span").text(num);
                $(this).find('>ul').find(">li").each(function (index1) {
                    var curnum = num + "." + (index1 + 1);
                    $(this).find("span").text(curnum);

                    $(this).find('li').each(function (index2) {
                        var curnum1 = curnum + "." + (index2 + 1);
                        $(this).find("span").text(curnum1);
                    });
                });
            });
        } else if (!parent.hasClass("main")) {
            parent.next().remove();
            parent.remove();
        }

        events();
    };
       
    function addData(data) {

    // post service
    var postService = _core.getService('Post').Service;

    postService.postJson(data, self.addUrl, function (res) {
        if (res != null) {
            location.href = '/Amnesty/Index';
        }
        else alert('serious eror on adding');
    })
}

    // edit
    function editData(data) {

    // post service
    var postService = _core.getService('Post').Service;

    postService.postJson(data, self.editUrl, function (res) {
        if (res != null) {
            console.log(res);
        }
        else alert('serious eror on editing');
    })
}
    function bindPaginationEvent()
    {
        var pagBtn = $('.paginationBtn');
        pagBtn.unbind();
        pagBtn.click(function (e)
        {
            _core.getService('Loading').Service.enableBlur('loaderClass');
            e.preventDefault();
            var url = $(this).attr('href');
            var element = $(this);

            // post service
            var postService = _core.getService('Post').Service;
            postService.postPartial(null, url, function (res)
            {
                _core.getService('Loading').Service.disableBlur('loaderClass');
                if (res != null)
                {
                    $('#amnesty_list').empty().append(res)
                    var tempUrl = '/Amnesty/Index/';
                    if(element.text().trim() != "" && !isNaN(element.text()))
                    {
                        window.history.pushState({ url: url }, "Page " + element.text(), tempUrl + element.text());

                    }
                    else
                    {
                        var pageNumber = element.attr('data-pagenumber');
                        window.history.pushState({ url: url }, "Page " + pageNumber, tempUrl + pageNumber);

                    }
                    // bind pagination buttons
                    bindPaginationEvent();
                }
            })
        })
    }
    // remove
    function removeData() {

    var data = {};
    data.id = _self.id;

    // post service
    var postService = _core.getService('Post').Service;

    postService.postJson(data, self.removeUrl, function (res) {
        if (res != null) {
            self.callback(res);
        }
        else alert('serious eror on removing');
    })
}
    function bindPopStateEvent()
    {
        window.addEventListener('popstate', function (e)
        {
            var character = e.state;
            _core.getService('Loading').Service.enableBlur('loaderClass');
            var postService = _core.getService('Post').Service;
            var tempUrl = '/Amnesty/Get_AmnestyTableRow';
            if (character == null)
            {
                postService.postPartial(null, tempUrl, function (res)
                {
                    _core.getService('Loading').Service.disableBlur('loaderClass');
                    if (res != null)
                    {
                        $('#amnesty_list').empty().append(res)

                        // bind pagination buttons
                        bindPaginationEvent();


                    }
                })
            } else
            {
                postService.postPartial(null, character.url, function (res)
                {
                    _core.getService('Loading').Service.disableBlur('loaderClass');
                    if (res != null)
                    {
                        $('#amnesty_list').empty().append(res)

                        // bind pagination buttons
                        bindPaginationEvent();


                    }
                })
            }

        });

    }
    // collect data
    function collectData() {

        if (self.mode == 'Edit') self.finalData['ID'] = self.id;

        self.getItems.each(function () {
            var name = $(this).attr('name');

            // appending to Data
            self.finalData[name] = $(this).val();
        });

        self.finalData['AmnestyPoints'] = collectPointsData();

        return self.finalData;
    }

    // collect data
    function collectPointsData() {

    var pointList = [];

    $('.getPoints').each(function () {
        if (!$(this).parent().parent().parent().hasClass('main')) {
            var data = {};
            data.ID = $(this).data('id');
            data.TempID = $(this).data('tempid');
            data.ParentID = $(this).data('parentid');
            data.Name = $(this).val();
            data.New = $(this).data('new');

            pointList.push(data);
        }

    });

    return pointList;
}
       
    /************************/
}

// creating class instance
var AmnestyController = new AmnestyControllerClass();

// creating object
var AmnestyControllerObject = {
    // important
    Name: 'Amnesty',

    Controller: AmnestyController
}

// registering controller object
_core.addController(AmnestyControllerObject);