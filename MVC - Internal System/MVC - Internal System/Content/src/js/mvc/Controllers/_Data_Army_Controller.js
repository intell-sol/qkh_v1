﻿// Army controller

/*****Main Function******/
var _Data_Army_ControllerClass = function () {
    var _self = this;

    _self.isReady = false;

    _self.saveContentURL = '/_Data_Army_/SaveContent';
    _self.getFileTableRowURL = '/_Popup_/Get_AddFileTableRow';
    _self.getArmyTableRowURL = '/_Popup_/Get_ArmyTableRows';

    _self.callback = null;
    _self.PrisonerID = null;
    _self.isReady = false;
    _self.finalData = {};

    _self.mode = "Add" // initial mode


    _self.init = function (Action) {
        console.log('_Data_Army_ Controller Inited with action', Action);

        _self.callback = null;
        _self.PrisonerID = null;
        _self.isReady = false;
        _self.finalData = {};

        _self.mode = Action;

        // Js Bindings
        bindButtons();

        // bind events on table rows
        bindArmyEvents();

        // custom bindings
        bindCustomEvents();

        // bind file events
        bindFilesEvents();
    }

    // add action
    _self.Add = function () {
        console.log('_Data_Army_ add called');

        _self.init('Add');

    }

    // edit action
    _self.Edit = function (PrisonerID) {

        console.log('_Data_Army_ edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;

    }

    _self.save = function (callback) {
        console.log('_Data_Army_ data saved');

        var res = {};

        res.status = true;

        // callback
        callback(res);
    }

    // js bindings
    function bindButtons() {

        var checkItems = $('.checkValidate');
        _self.getItems = $('.getItems');
        var addFileBtn = $('#addfileBtn');
        var addArmyBtn = $('#addarmyBtn');
        var toggleServe = $('#military-status');
        var saveBtn = $('.saveBtn');

        checkItems.unbind();
        addArmyBtn.unbind();
        addArmyBtn.bind('click', function () {

            // call arrest data widged
            var addArmyWidget = _core.getWidget('AddArmy').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addArmyWidget.Add(_self.PrisonerID, function (res) {
                if (res != null && res.status) {

                    // get arrest list
                    getArmyList();
                }
            });
        });

        addFileBtn.unbind();
        addFileBtn.bind('click', function () {

            var data = {};
            data.TypeID = 7;
            data.PrisonerID = _self.PrisonerID;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });

        toggleServe.unbind();
        toggleServe.bind('change', function () {
            if ($(this).val() == "1") {
                $("#no-military-wrap").addClass("hidden");
                $("#yes-military-wrap").removeClass("hidden");
            }
            else {
                $("#no-military-wrap").removeClass("hidden");
                $("#yes-military-wrap").addClass("hidden");
            }

            checkItems.trigger('change');
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {

                // if is noServe or not
                var noServe = $(this).hasClass('NoServe');

                if (($(this).val() == '' || $(this).val() == null) && (noServe && parseInt($('#military-status').val()) == 0)) {
                    if ($(this).attr('name') == 'Description') $(this).val(' ');
                    else action = true;
                }
            });

            _self.isReady = !action;
            
            // toggle save Btn
            saveBtn.attr('disabled', action);
        });

        // save btn
        saveBtn.unbind().click(function () {
            
            if (_self.isReady) {
                saveContent(function () {
                    saveBtn.attr('disabled', true);
                });
            }
        });

        // selectize
        $(".selectizeControl").each(function () {
            $(this).selectize({
                create: false
            });
        });

        $(".selectizeControl-tags").each(function () {
            $(this).selectize({
                create: true
            });
        });

        toggleServe.trigger('change');

    }

    // custom bindings
    function bindCustomEvents() {

        // bind custom select
        _core.getService("SelectMenu").Service.bindSelectEvents("ArmyAwards");
    }

    // army table row bindings
    function bindArmyEvents() {

        var removeBtn = $('.removeRow');
        var editBtn = $('.editRow');
        var viewBtn = $('.viewarmyRow');
        // remove
        removeBtn.unbind().bind('click', function () {

            var id = $(this).data('id');

            // call army widget
            var addArmyWidget = _core.getWidget('AddArmy').Widget;

            // run
            addArmyWidget.Remove(id, function (res) {
                if (res != null && res.status) {

                    // get army list
                    getArmyList();
                }
            });
        });
        //view
        viewBtn.unbind().click(function () {

            var id = $(this).data('id');

            // call army widget
            var addArmyWidget = _core.getWidget('AddArmy').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addArmyWidget.View(id, function (res) {

                
            });
        });
        // edit
        editBtn.unbind().click(function () {

            var id = $(this).data('id');

            // call army widget
            var addArmyWidget = _core.getWidget('AddArmy').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addArmyWidget.Edit(id, function (res) {

                if (res != null && res.status) {

                    // get army list
                    getArmyList();
                }
            });
        });
    }

    // bind files events
    function bindFilesEvents() {

        var removeBtn = $('.removeFileButton');
        var editBtn = $('.editFileButton');
        var viewBtn = $('.viewFileButton');
        // remove file actin handle
        removeBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Remove(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });

        // edit file action handle
        viewBtn.unbind().click(function ()
        {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.View(data, function (res)
            {
            })
        });
        editBtn.unbind().click(function ()
        {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Edit(data, function (res)
            {
                if (res != null && res.status)
                {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
    }

    // get file list table rows
    function getFileList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.TypeID = type;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('fileLoader');

        postService.postPartial(data, _self.getFileTableRowURL, function (curHtml) {

            var table = null;
            if (type == 7) table = $('#filetablebody');

            table.empty();
            table.append(curHtml);
            
            _core.getService('Loading').Service.disableBlur('fileLoader');

            // bind file Events
            bindFilesEvents();
        });
    }


    // get file list table rows
    function getArmyList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.Type = type;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('armyLoader');

        postService.postPartial(data, _self.getArmyTableRowURL, function (curHtml) {

            var table = null;
            table = $('#armytablebody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('armyLoader');

            // bind events on table rows
            bindArmyEvents();
        });
    }


    // collect data
    function collectData() {

        _self.finalData['PrisonerID'] = _self.PrisonerID;

        _self.finalData['NotServed'] = {};
        _self.finalData['ArmyAwards'] = [];
        _self.finalData['WarInvolved'] = [];

        // prisonerIDs
        _self.finalData['NotServed']['PrisonerID'] = _self.PrisonerID;


        _self.getItems.each(function () {
            
            var isNoServe = $(this).hasClass('NoServe');
            var isArmyAwards = $(this).hasClass('ArmyAwards');
            var isWarInvolved = $(this).hasClass('WarInvolved');

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            // appending to Data
            if (isArmyAwards) {
                var curValues = _core.getService("SelectMenu").Service.collectData(name);
                var value = curValues;

                for (var j in value) {
                    var ArmyData = {};
                    ArmyData['PrisonerID'] = _self.PrisonerID;
                    ArmyData['LibItemID'] = parseInt(value[j]);
                    _self.finalData['ArmyAwards'].push(ArmyData);
                }
                
            }
            else if (isNoServe) {
                _self.finalData['NotServed'][name] = $(this).val();
            }
            else if (isWarInvolved) {
                var value = $(this).val();
                for (var j in value) {
                    var ArmyInvolved = {};
                    ArmyInvolved['PrisonerID'] = _self.PrisonerID;
                    ArmyInvolved['LibItemID'] = value[j];
                    _self.finalData['WarInvolved'].push(ArmyInvolved);
                }
            }
        });

        return _self.finalData;
    }

    // save content
    function saveContent(callback) {
        
        // collect data
        var data = collectData();

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('loaderClass');

        postService.postJson(data, _self.saveContentURL, function (res) {

            _core.getService('Loading').Service.disableBlur('loaderClass');

            if (typeof res != "undefined" && typeof res.needApprove != "undefined" && res.needApprove) {
                $('.armyMergeWarningClass').removeClass("hidden");
                $('.mainMergeWarningClass').removeClass("hidden");
            }

            if (res != null) {
                callback(res);
            }
            else {
                callback(null);
                alert('serious error on saving content');
            }
        })
    }

}
/************************/


// creating class instance
var _Data_Army_Controller = new _Data_Army_ControllerClass();

// creating object
var _Data_Army_ControllerObject = {
    // important
    Name: '_Data_Army_',

    Controller: _Data_Army_Controller
}

// registering controller object
_core.addController(_Data_Army_ControllerObject);