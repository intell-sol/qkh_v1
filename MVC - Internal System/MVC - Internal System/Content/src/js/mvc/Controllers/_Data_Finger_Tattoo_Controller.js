﻿// _Data_Finger_Tattoo_ controller

/*****Main Function******/
var _Data_Finger_Tattoo_ControllerClass = function () {
    var _self = this;
    
    _self.addFingerTatooScarURL = '/_Data_Finger_Tattoo_/addFingerTatooScar';

    _self.getFingerRowsURL = '/_Popup_/Get_FingerRows';
    _self.getTatooRowsURL = '/_Popup_/Get_TatooRows';
    _self.getScarRowsURL = '/_Popup_/Get_ScarRows';

    _self.callback = null;
    _self.PrisonerID = null;

    _self.mode = "Add" // initial mode


    _self.init = function (Action) {
        console.log('_Data_Finger_Tattoo_ Controller Inited with action', Action);

        _self.mode = Action;
        // Js Bindings
        bindButtons();
        // bind file Events
        bindFilesEvents();
    }

    // add action
    _self.Add = function () {
        console.log('_Data_Finger_Tattoo_ add called');

        _self.init('Add');

    }

    // edit action
    _self.Edit = function (PrisonerID) {

        console.log('_Data_Finger_Tattoo_ edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;

    }

    _self.save = function (callback) {
        console.log('_Data_Finger_Tattoo_ data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }

    // js bindings
    function bindButtons() {

        var addFingerBtn = $('#addFingerBtn');
        var addTatooBtn = $('#addTatooBtn');
        var addScarBtn = $('#addScarBtn');
        addFingerBtn.unbind();
        addFingerBtn.bind('click', function () {
            debugger;
            
            var data = {};
            data.TypeID = 18;
            data.PrisonerID = _self.PrisonerID;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('FingerTatooScar').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
          
            
           
        });

        addTatooBtn.unbind();
        addTatooBtn.bind('click', function () {

            var data = {};
            data.TypeID = 20;
            data.PrisonerID = _self.PrisonerID;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('FingerTatooScar').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });

        addScarBtn.unbind();
        addScarBtn.bind('click', function () {

            var data = {};
            data.TypeID = 22;
            data.PrisonerID = _self.PrisonerID;
            
            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('FingerTatooScar').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });
    }

    // bind files events
    function bindFilesEvents() {
        var viewBtn=$('.viewFileButton');
        var removeBtn = $('.removeFileButton');
        var editBtn = $('.editFileButton');

        // remove file actin handle
        removeBtn.unbind().click(function (e) {
            e.preventDefault();

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('FingerTatooScar').Widget.Remove(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                if (res != null && typeof res.needApprove != "undefined" && res.needApprove) {
                    $('.mainMergeWarningClass').removeClass("hidden");
                    $('.fingerMergeWarningClass').removeClass("hidden");
                }
                else alert('error while removing file');
            })
        });
        viewBtn.unbind().click(function (e) {
            e.preventDefault();
            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');
            
            _core.getService('Loading').Service.enableBlur('loaderClass');

            // postService
            _core.getWidget('FingerTatooScar').Widget.View(data, function (res) {
             
            })
        });
    
        // edit file action handle
        editBtn.unbind().click(function (e) {
            e.preventDefault();

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');
            
            _core.getService('Loading').Service.enableBlur('loaderClass');

            // postService
            _core.getWidget('FingerTatooScar').Widget.Edit(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                if (res != null && typeof res.needApprove != "undefined" && res.needApprove) {
                    $('.mainMergeWarningClass').removeClass("hidden");
                    $('.fingerMergeWarningClass').removeClass("hidden");
                }
                else alert('error while removing file');
            })
        });
    }

    // Add file to database
    function addFile(data, type) {

        // appending prisoner id to formdata
        data.append('PrisonerID', _self.PrisonerID);
        data.append('TypeID', type);

        // log it
        for (var pair of data.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }

        var curName = '';
        if (parseInt(type) == 18) curName = 'fingerLoader';
        else if (parseInt(type) == 20) curName = 'tatooLoader';
        else if (parseInt(type) == 22) curName = 'scarLoader';

        _core.getService('Loading').Service.enableBlur(curName);

        // post service
        var postService = _core.getService('Post').Service;

        postService.postFormData(data, _self.addFingerTatooScarURL, function (res) {

            _core.getService('Loading').Service.disableBlur(curName);

            if (res.status) {
                getFingerList(res.type);
            }
            else alert('serious error on adding finger tatoo scar');
        })
    }

    // get image file list table rows
    function getFileList(type) {

        var url = null;

        if (type == 18) url = _self.getFingerRowsURL;
        else if (type == 20) url = _self.getTatooRowsURL;
        else if (type == 22) url = _self.getScarRowsURL;

        var curName = '';
        if (parseInt(type) == 18) curName = 'fingerLoader';
        else if (parseInt(type) == 20) curName = 'tatooLoader';
        else if (parseInt(type) == 22) curName = 'scarLoader';

        _core.getService('Loading').Service.enableBlur(curName);

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, url, function (curHtml) {

            var table = null;

            if (type == 18) table = $('#fingerprintbody');
            else if (type == 20) table = $('#tatooprintbody');
            else if (type == 22) table = $('#scarprintbody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur(curName);

            // bind file Events
            bindFilesEvents();
        });
    }

}
/************************/


// creating class instance
var _Data_Finger_Tattoo_Controller = new _Data_Finger_Tattoo_ControllerClass();

// creating object
var _Data_Finger_Tattoo_ControllerObject = {
    // important
    Name: '_Data_Finger_Tattoo_',

    Controller: _Data_Finger_Tattoo_Controller
}

// registering controller object
_core.addController(_Data_Finger_Tattoo_ControllerObject);