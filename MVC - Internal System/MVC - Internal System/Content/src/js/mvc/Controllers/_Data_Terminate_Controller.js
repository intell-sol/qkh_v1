﻿// terminate controller

/*****Main Function******/
var _Data_Terminate_ControllerClass = function () {
    var _self = this;

    _self.PrisonerID = null;

    _self.getFileTableRowURL = '/_Popup_/Get_AddFileTableRow';
    _self.getTerminateTableRowURL = '/_Popup_/Get_TerminateTableRows';


    _self.mode = "Add" // initial mode


    _self.init = function (Action) {
        console.log('_Data_Terminate_ Controller Inited with action', Action);

        _self.mode = Action;

        _self.callback = null;
        _self.PrisonerID = null;
        _self.PrisonerType = null;

        // Js Bindings
        bindButtons();

        // items table row events
        bindTerminateRowEvents();

        // bind file events
        bindFilesEvents();
    }

    // add action
    _self.Add = function () {
        console.log('_Data_Terminate_ add called');

        _self.init('Add');
    }

    // edit action
    _self.Edit = function (PrisonerID, PrisonerType) {

        console.log('_Data_Terminate_ edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;
        _self.PrisonerType = PrisonerType;
    }

    _self.save = function (callback) {
        console.log('_Data_Terminate_ data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }

    // js bindings
    function bindButtons() {

        var addFileBtn = $('#addfileBtn');

        addFileBtn.unbind();
        addFileBtn.bind('click', function () {

            var data = {};
            data.TypeID = 23;
            data.PrisonerID = _self.PrisonerID;

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (res) {

                // getting file list
                getFileList(data.TypeID);
            });
        });
    }

    // bind items table rows events
    function bindTerminateRowEvents() {
        var terminateRow = $('.terminateRow');

        // edit
        terminateRow.unbind();
        terminateRow.bind('click', function () {

            var data = {};
            data['Number'] = parseInt($(this).data('number'));
            data["PrisonerID"] = _self.PrisonerID;
            data["PrisonerType"] = _self.PrisonerType;
            data["DataType"] = $(this).data("datatype");
            data["SentenceID"] = $(this).data("sentenceid");
            data["ArrestID"] = $(this).data("arrestid");

            var number = parseInt($(this).data('number'));

            // call add file widget (image type)
            var addterminateWidget = _core.getWidget('CaseCloseTerminate').Widget;
            
            // run
            addterminateWidget.Terminate(data, function (res) {
                
                // get terminate list
                //getTerminateList();

                    // teminate events
                    terminateEvent();
            });
        });
    }

    // bind files events
    function bindFilesEvents() {

        var removeBtn = $('.removeFileButton');
        var editBtn = $('.editFileButton');

        // remove file actin handle
        removeBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Remove(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });

        // edit file action handle
        editBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Edit(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
    }

    // get file list table rows
    function getFileList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.TypeID = type;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getFileTableRowURL, function (curHtml) {

            var table = null;
            table = $('#addfileTableBody');

            table.empty();
            table.append(curHtml);

            // bind file events
            bindFilesEvents();
        });
    }

    // get terminateList
    function getTerminateList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getTerminateTableRowURL, function (curHtml) {

            var table = null;
            table = $('#terminatetablebody');

            table.empty();
            table.append(curHtml);

            bindTerminateRowEvents();
        });
    }

    // teminate event
    function terminateEvent() {

        // refresh page and it will redirect to Preview page
        location.reload();
    }

}
/************************/


// creating class instance
var _Data_Terminate_Controller = new _Data_Terminate_ControllerClass();

// creating object
var _Data_Terminate_ControllerObject = {
    // important
    Name: '_Data_Terminate_',

    Controller: _Data_Terminate_Controller
}

// registering controller object
_core.addController(_Data_Terminate_ControllerObject);