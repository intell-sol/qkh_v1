﻿// Education controller

/*****Main Function******/
var _Data_Education_ControllerClass = function () {
    var _self = this;

    _self.SaveContentURL = "/_Data_Education_/SaveContent";

    _self.HIGH_EDUCATON_ID = 202;

    _self.callback = null;
    _self.PrisonerID = null
    _self.getItems = null;
    _self.isContentReady = true;

    _self.finalData = {};

    _self.mode = "Add" // initial mode


    _self.init = function (Action) {
        console.log('_Data_Education_ Controller Inited with action', Action);

        _self.callback = null;
        _self.PrisonerID = null;
        _self.getItems = null;
        _self.isContentReady = true;
        _self.finalData = {};

        _self.mode = Action;
    }

    // add action
    _self.Add = function () {
        console.log('_Data_Education_ add called');

        _self.init('Add');

        // Js Bindings
        bindButtons();

    }

    // edit action
    _self.Edit = function (PrisonerID) {

        console.log('_Data_Education_ edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;

        // Js Bindings
        bindButtons();

    }

    // ending up subcontroller from main controller
    _self.save = function (callback) {
        console.log('_Data_Education_ data saved');

        var res = {};
        //res.status = _self.isContentReady;
        res.status = true;

        // callback
        callback(res);
    }

    // js bindings
    function bindButtons() {

        var acceptBtn = $('#acceptBtn');
        var checkItems = $('.checkValidate');
        var educationElem = $('');
        _self.getItems = $('.getItems');

        acceptBtn.unbind();
        acceptBtn.bind('click', function () {

            // collecting content
            var data = collectData();

            // saving content
            saveContent(data);
        });

        _core.getService("SelectMenu").Service.bindSelectEvents("Awards");

        // check validate and toggle accept button
        checkItems.unbind();
        checkItems.bind('change keyup', function () {

            var action = false;


            // value for education to check exception
            var checkHigh = false;
            var education = $('#education-status').val();
            if (parseInt(education) == _self.HIGH_EDUCATON_ID) checkHigh = true;

            // if it is education toggle wrap
            if (checkHigh) {
                $("#academic-wrap").removeClass("hidden");
            }
            else {
                if (!$("#academic-wrap").hasClass('hidden')) $("#academic-wrap").addClass("hidden");
            }

            checkItems.each(function () {
                
                if (!$(this).hasClass('getItemOptional') || checkHigh) {
                    if ($(this).val() == '' || $(this).val() == null) {
                        //action = true;
                    }
                }
            });

            _self.isContentReady = action;

            acceptBtn.attr("disabled", action);
        });
        // trigger
        checkItems.trigger('change');

        // selectize
        $(".selectize").each(function () {
            $(this).selectize({
                create: false
            });
        });

        $(".selectize-tags").each(function () {
            $(this).selectize({
                create: true
            });
        });

        $(".scrollTable").each(function () {
            $(this).tableScroll({
                flush: false,
                height: 136
            });
        });
    }

    // saving content
    function saveContent(data) {

        // appending prisonerID
        data['PrisonerID'] = _self.PrisonerID;
       
        _core.getService('Loading').Service.enableBlur('loaderClass');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.SaveContentURL, function (res) {

            _core.getService('Loading').Service.disableBlur('loaderClass');

            var acceptBtn = $('#acceptBtn');
            if (res.status) {
                _self.isContentReady = true; // if save ok then can continue to next subController
                //acceptBtn.attr('disabled', true);
            }
            if (res != null && typeof res.needApprove != "undefined" && res.needApprove) {
                $('.educationMergeWarningClass').removeClass("hidden");
                $('.mainMergeWarningClass').removeClass("hidden");
            }
        });
    }

    // collecting data to Object
    function collectData() {
        
        // value for education to check exception
        var checkHigh = false;
        var education = $('#education-status').val();
        if (parseInt(education) == _self.HIGH_EDUCATON_ID) checkHigh = true;

        // collect data
        _self.getItems.each(function () {
            debugger;
            // name of Field (same as in Entity)
            var name = $(this).attr('name');
           
            if (!$(this).hasClass('getItemOptional') || checkHigh) {

                // if has class objectItem then make object then bind
                if (!$(this).hasClass('objectItem')) {
                    
                    if (name != 'UniversityDegree') {

                        // appending to Object
                        _self.finalData[name] = $(this).val();
                    }
                    else { // if university degree, then fields are different

                        // object for Entity
                        var objectItem = {};
                        objectItem['AcademicDegreeLibItemID'] = $('#academic-degree').val();
                        objectItem['DegreeLibItemID'] = $('#science-academic-degree').val();
                       
                        objectItem['PrisonerID'] = _self.PrisonerID;

                        var awardList = _core.getService("SelectMenu").Service.collectData('Awards');
                        var awards = []; // array to bind to Awardes

                        _self.finalData[name] = objectItem;

                        // awards is list
                        for (var j in awardList) {
                            var tempObjectItem = {}; // new object
                            tempObjectItem['PrisonerID'] = _self.PrisonerID;
                            tempObjectItem['LibItemID'] = parseInt(awardList[j]);
                            awards.push(tempObjectItem); // add to array
                        }

                        // appending array to Object
                        _self.finalData[name]['Awardes'] = awards;
                    }
                }
                else {
                    
                    var curValue = $(this).val();

                    // array to append to Object
                    var arrayList = [];

                    for (var i in curValue) {

                        // object for Entity
                        var objectItem = {};
                        objectItem['LibItemID'] = parseInt(curValue[i]);
                        objectItem['PrisonerID'] = _self.PrisonerID;
                        arrayList.push(objectItem);
                    }

                    // appending array to Object
                    _self.finalData[name] = arrayList;
                }
            }

            if (name == "Awards") {
                var awardList = _core.getService("SelectMenu").Service.collectData('Awards');
                var awards = []; // array to bind to Awardes

                // awards is list
                for (var j in awardList) {
                    var tempObjectItem = {}; // new object
                    tempObjectItem['PrisonerID'] = _self.PrisonerID;
                    tempObjectItem['LibItemID'] = parseInt(awardList[j]);
                    awards.push(tempObjectItem); // add to array
                }

                // appending array to Object
                if (typeof _self.finalData['UniversityDegree'] == 'undefined') _self.finalData['UniversityDegree'] = {};
                _self.finalData['UniversityDegree']['Awardes'] = awards;
            }
        });

        return _self.finalData;
    }


}
/************************/


// creating class instance
var _Data_Education_Controller = new _Data_Education_ControllerClass();

// creating object
var _Data_Education_ControllerObject = {
    // important
    Name: '_Data_Education_',

    Controller: _Data_Education_Controller
}

// registering controller object
_core.addController(_Data_Education_ControllerObject);