﻿// PersonalVisits controller

/*****Main Function******/
var PersonalVisitsControllerClass = function () {
    var _self = this;

    _self.selectOrgUnitURL = '/Base/SelectOrgUnit';
    _self.PrisonerID = null;

    _self.searchPrisonerURL = "/Convicts/SearchPrisoner";

    _self.getFileTableRowURL = '/_Popup_/Get_AddFileTableRow';
    _self.getMainTableRowURL = '/_Popup_/Get_PersonalVisitTableRows';
    _self.filterURL = '/PersonalVisits/Draw_PersonalVisitTableRowByFilter';
    _self.resetURL = '/PersonalVisits/Draw_ResetPersonalVisitTableRow';
    _self.ReportSaveURL = "/PersonalVisits/PersonalVisitsReportListData";
    

    _self.mode = "Add" // initial mode

    _self.init = function (Action) {
        console.log('Controller Inited with action', Action);

        _self.mode = Action;

        _self.callback = null;
        _self.PrisonerID = null;

        // Js Bindings
        bindButtons();

        // items table row events
        bindMainTableRowEvents();
        bindPopStateEvent();
        // bind file events
        bindFilesEvents();
    }

    // index action
    _self.Index = function () {
        console.log('add called');

        _self.init('Index');

        // bind pagination
        bindPaginationEvent();

        // bind search events
        bindSearchEvents();

        // bind personal visit row events
        bindPersonalVisitTableRowEvents();
    }

    // add action
    _self.Add = function () {
        console.log('add called');

        _self.init('Add');
    }

    // edit action
    _self.Edit = function (PrisonerID) {

        console.log('edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;
    }

    _self.save = function (callback) {
        console.log('data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }
    _self.Complete = function (id, data, callback)
    {
        debugger;
        // initilize
        _self.init();

        console.log('Complete called');

        _self.id = id;

        // save callback
        _self.callback = callback;

        // remove
        CompleteData();
    };
    // js bindings
    function bindButtons() {

        var addMainBtn = $('#addMainBtn');
        var addFileBtn = $('#addfileBtn');

        addMainBtn.unbind();
        addMainBtn.bind('click', function () {

            var addMainWidget = _core.getWidget('PersonalVisits').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addMainWidget.Add(_self.PrisonerID, function (res) {
                if (res != null) {

                    // get penalty list
                    getMainTableList();
                }
            });
        });

        addFileBtn.unbind();
        addFileBtn.bind('click', function () {

            var data = {};
            data.TypeID = 31;
            data.PrisonerID = _self.PrisonerID;

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });
    }
    function bindPopStateEvent()
    {
        window.addEventListener('popstate', function (e)
        {
            var character = e.state;

            _core.getService('Loading').Service.enableBlur('loaderClass');
            var postService = _core.getService('Post').Service;
            var tempUrl = '/PersonalVisits/Draw_PersonalVisitTableRow';
            if (character == null)
            {
                postService.postPartial(null, tempUrl, function (res)
                {
                    _core.getService('Loading').Service.disableBlur('loaderClass');
                    if (res != null)
                    {
                        $('#filterList').empty().append(res)

                        // bind pagination buttons
                        bindPaginationEvent();


                    }
                })
            } else
            {
                postService.postPartial(null, character.url, function (res)
                {
                    _core.getService('Loading').Service.disableBlur('loaderClass');
                    if (res != null)
                    {
                        $('#filterList').empty().append(res)

                        // bind pagination buttons
                        bindPaginationEvent();


                    }
                })
            }

        });

    }
    // bind pagination events
    function bindPaginationEvent() {
        var pagBtn = $('.paginationBtn');
        var resetBtn = $('#reloadFilterBtn');
        pagBtn.unbind();
        pagBtn.click(function (e) {
            e.preventDefault();
            _core.getService('Loading').Service.enableBlur('loaderClass');

            var url = $(this).attr('href');

            // post service
            var postService = _core.getService('Post').Service;
            var element = $(this);
            postService.postPartial(null, url, function (res)
            {
                _core.getService('Loading').Service.disableBlur('loaderClass');

                if (res != null) {
                    $('#filterList').empty().append(res)
                    var tempUrl = '/PersonalVisits/Index/';

                    if (element.text().trim() != "" && !isNaN(element.text()))
                    {
                        window.history.pushState({ url: url }, "Page " + element.text(), tempUrl + element.text());

                    }
                    else
                    {
                        console.log(element);
                        var pageNumber = element.attr('data-pagenumber');
                        window.history.pushState({ url: url }, "Page " + pageNumber, tempUrl + pageNumber);

                    }
                    // bind pagination buttons
                    bindPaginationEvent();
                   
                    bindPersonalVisitTableRowEvents();

                }
            })
        })
        resetBtn.unbind().click(function (e)
        {
            _core.getService('Loading').Service.enableBlur('loaderClass');

            resetList(function (res)
            {
                if (res.status)
                    location.reload();
                _core.getService('Loading').Service.disableBlur('loaderClass');


            });

        });
    }
    function resetList(callback)
    {

        // postService
        var postService = _core.getService('Post').Service;

        postService.postJsonReturn(null, _self.resetURL, callback);
    }
   
    // bind search events
    function bindSearchEvents() {

        var acceptBtn = $('#acceptFilterBtn');
        var TypeLibItemID = $('#TypeLibItemID');
        var PurposeLibItemID = $('#PurposeLibItemID');
        var orgunitlist = $('#orgunitidlist');
        var PrisonerType = $('#PrisonerType');
        var dates = $('.FilterDates');

        acceptBtn.unbind().click(function (e) {
            e.preventDefault();
            _core.getService('Loading').Service.enableBlur('loaderClass');

            var finalData = {};
            $("#filterPersonForm").serializeArray().map(function (x) { finalData[x.name] = x.value; });
            finalData['TypeLibItemID'] = TypeLibItemID.val();
            finalData['PurposeLibItemID'] = PurposeLibItemID.val();
            finalData['OrgUnitIDList'] = orgunitlist.val();
            finalData['PrisonerType'] = PrisonerType.val();

            // filter list
            filterList(finalData);
        });

        $('.selectizeFilterSearch').selectize({
            valueField: 'ID',
            labelField: 'value',
            searchField: 'value',
            create: false,
            render: {
                item: function (item, escape) {
                    console.log(item);
                    return '<div>' +
                        item.Person.FirstName +
                    '</div>';
                },
                option: function (item, escape) {
                    console.log(item);
                    return '<div>' +
                        item.Person.FirstName +
                    '</div>';
                }
            },
            score: function (search) {
                var score = this.getScoreFunction(search);
                return function (item) {
                    return score(item) * (1 + Math.min(item.watchers / 100, 1));
                };
            },
            load: function (query, callback) {
                if (!query.length) return callback();

                var data = {};
                data.query = query;

                var url = _self.searchPrisonerURL;

                _core.getService('Post').Service.postJson(data, url, function (res) {
                    console.log(res);
                    if (res != null && res.status) {
                        callback(res.prisoners.slice(0, 10));
                    }
                })
            }
        });

        $(".selectizeFilter").each(function () {
            $(this).selectize({
                create: false
                //plugins: {
                //    'no-delete': {}
                //},
            });
        });

        $(".selectizeFilterArchive").each(function () {
            $(this).selectize({
                create: false,
                plugins: {
                    'no-delete': {}
                },
            });
        });

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

    }

    // bind penalty table rows events
    function bindMainTableRowEvents() {
        var removeRows = $('.removeMainRow');
        var editRows = $('.editMainRow');
        var viewRows = $('.viewMainRow');
        var completeRows = $('.completeMainRow');
        var declineRows = $('.declineMainRow');
        var approveRows = $('.approveMainRow');
        var printReportBtn = $('.printReport');

        printReportBtn.unbind().bind('click', function (e) {
            e.preventDefault();

            var id = $(this).data('id');
            var href = $(this).attr('href');

            printReport(id, href);
        });

        // remove row
        removeRows.unbind();
        removeRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('PersonalVisits').Widget;

                    // run
                    addMainWidget.Remove(id, function (res) {
                        if (res != null) {

                            // get penalty list
                            getMainTableList();
                        }
                    });
                }
            })
        });
        
        // edit row
        viewRows.unbind();
        viewRows.bind('click', function ()
        {
            var id = parseInt($(this).data('id'));

            var addMainWidget = _core.getWidget('PersonalVisits').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addMainWidget.View(id, function (res)
            {

            });
        });
        // edit row
        editRows.unbind();
        editRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            var addMainWidget = _core.getWidget('PersonalVisits').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addMainWidget.Edit(id, function (res) {

                if (res != null) {

                    // get penalty list
                    getMainTableList();
                }
            });
        });

        // complete row
        completeRows.unbind();
        completeRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            //_core.getWidget('YesOrNo').Widget.Ask(4, function (res)
            //{

            //    if (res) {

            //        var addMainWidget = _core.getWidget('PersonalVisits').Widget;

            //        // run
            //        addMainWidget.Complete(id, function (res) {
            //            if (res != null) {

            //                // get penalty list
            //                getMainTableList();
            //            }
            //        });
            //    }
            //})
            _core.getWidget('CompleteDate').Widget.Show(function (res)
            {

                if (res)
                {

                    var addMainWidget = _core.getWidget('PersonalVisits').Widget;

                    // run
                    addMainWidget.Complete(id, res['EndDate'], function (res)
                    {
                        if (res != null)
                        {

                            // get penalty list
                            getMainTableList();
                        }
                    });
                }
            })
        });

        // decline row
        declineRows.unbind();
        declineRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(3, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('PersonalVisits').Widget;

                    // run
                    addMainWidget.Decline(id, function (res) {
                        if (res != null) {

                            // get penalty list
                            getMainTableList();
                        }
                    });
                }
            })
        });

        // decline row
        approveRows.unbind();
        approveRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(2, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('PersonalVisits').Widget;

                    // run
                    addMainWidget.Approve(id, function (res) {
                        if (res != null) {

                            // get penalty list
                            getMainTableList();
                        }
                    });
                }
            })
        });
    }


    // open report
    function printReport(id, href) {
        if (typeof id != "undefined" && id != null && id != "") {
            var url = _self.ReportSaveURL;
            var data = {};
            data.ID = id;
            _core.getService("Post").Service.postJson(data, url, function (res) {
                if (res != null) {
                    var key = res.key;

                    if (typeof key != "undefined" && key != null) {
                        window.open(href + "?key=" + key, "_blank");
                    }
                }
            })
        }
    }

    // bind penalty table rows events
    function bindPersonalVisitTableRowEvents() {
        var approvePersonalVisitRow = $('.approvePersonalVisitRow');
        var declinePersonalVisitRow = $('.declinePersonalVisitRow');
        var completeRows = $('.completePersonalVisitsRow');
        var printReportBtn = $('.printReport');

        printReportBtn.unbind().bind('click', function (e) {
            e.preventDefault();

            var id = $(this).data('id');
            var href = $(this).attr('href');

            printReport(id, href);
        });

        // complete row
        completeRows.unbind();
        completeRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            //_core.getWidget('YesOrNo').Widget.Ask(4, function (res)
            //{

            //    if (res) {

            //        var addMainWidget = _core.getWidget('PersonalVisits').Widget;

            //        // run
            //        addMainWidget.Complete(id, function (res) {
            //            if (res != null) {

            //                // get penalty list
            //                getMainTableList();
            //            }
            //        });
            //    }
            //})
            _core.getWidget('CompleteDate').Widget.Show(function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('PersonalVisits').Widget;

                    // run
                    addMainWidget.Complete(id, res['EndDate'], function (res) {
                        if (res != null) {

                            // get penalty list
                            filterList({});
                        }
                    });
                }
            })
        });

        // decline row
        declinePersonalVisitRow.unbind();
        declinePersonalVisitRow.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(3, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('PersonalVisits').Widget;

                    // run
                    addMainWidget.Decline(id, function (res) {
                        if (res != null) {

                            // get penalty list                            
                            filterList({});

                        }
                    });
                }
            })
        });

        // decline row
        approvePersonalVisitRow.unbind();
        approvePersonalVisitRow.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(2, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('PersonalVisits').Widget;

                    // run
                    addMainWidget.Approve(id, function (res) {
                        if (res != null) {

                            // get penalty list
                            filterList({});
                        }
                    });
                }
            })
        });
    }

    // bind files events
    function bindFilesEvents() {

        var removeBtn = $('.removeFileButton');
        var editBtn = $('.editFileButton');
        var viewBtn = $('.viewFileButton');

        // remove file actin handle
        removeBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Remove(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
        
        // view file action handle
        viewBtn.unbind().click(function ()
        {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.View(data, function (res)
            {
               
            })
        });
        // edit file action handle
        editBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Edit(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
    }

    // filter list
    function filterList(data) {

        // postService
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.filterURL, function (res) {
            if (res != null) {
                $('#filterList').empty().append(res)

                // bind pagination buttons
                bindPaginationEvent();
		_core.getService('Loading').Service.disableBlur('loaderClass');

            }

            bindPersonalVisitTableRowEvents();
        })
    }

    // get file list table rows
    function getFileList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.TypeID = type;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getFileTableRowURL, function (curHtml) {

            var table = null;
            table = $('#addfileTableBody');

            table.empty();
            table.append(curHtml);

            // bind file events
            bindFilesEvents();
        });
    }
   
    // get personal visit list edit table rows
    function getMainTableList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        _core.getService('Loading').Service.enableBlur('maintablebody');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getMainTableRowURL, function (curHtml) {

            var table = null;
            table = $('#maintablebody');

            table.empty();
            table.append(curHtml);
            
            _core.getService('Loading').Service.disableBlur('maintablebody');

            bindMainTableRowEvents();
        });
    }

}
/************************/


// creating class instance
var PersonalVisitsController = new PersonalVisitsControllerClass();

// creating object
var PersonalVisitsControllerObject = {
    // important
    Name: "PersonalVisits",

    Controller: PersonalVisitsController
}

// registering controller object
_core.addController(PersonalVisitsControllerObject);