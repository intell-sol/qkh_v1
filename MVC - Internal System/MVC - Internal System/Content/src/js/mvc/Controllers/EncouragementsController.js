﻿// Encouragements controller

/*****Main Function******/
var EncouragementsControllerClass = function () {
    var _self = this;

    _self.selectOrgUnitURL = '/Base/SelectOrgUnit';
    _self.PrisonerID = null;

    _self.searchPrisonerURL = "/Convicts/SearchPrisoner";

    _self.getFileTableRowURL = '/_Popup_/Get_AddFileTableRow';
    _self.getEncTableRowURL = '/_Popup_/Get_EncouragementTableRows';
    _self.filterURL = '/Encouragements/Draw_EncouragementTableRowByFilter';
    _self.resetURL = '/Encouragements/Draw_ResetEncouragementTableRow';

    _self.mode = "Add" // initial mode

    _self.init = function (Action) {
        console.log('Encouragements Controller Inited with action', Action);

        _self.mode = Action;

        _self.callback = null;
        _self.PrisonerID = null;

        // Js Bindings
        bindButtons();

        // items table row events
        bindEncouragementsRowEvents();

        // bind file events
        bindFilesEvents();
    }

    // index action
    _self.Index = function () {
        console.log('Encouragements add called');

        _self.init('Index');

        // bind pagination
        bindPaginationEvent();
        bindPopStateEvent();
        // bind search events
        bindSearchEvents();
    }

    // add action
    _self.Add = function () {
        console.log('Encouragements add called');

        _self.init('Add');
    }

    // edit action
    _self.Edit = function (PrisonerID) {

        console.log('Encouragements edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;
    }

    _self.save = function (callback) {
        console.log('_Data_Cell_ data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }

    // js bindings
    function bindButtons() {

        var addEncBtn = $('#addEncBtn');
        var addFileBtn = $('#addfileBtn');

        addEncBtn.unbind();
        addEncBtn.bind('click', function () {

            // call add file widget (image type)
            var addEncWidget = _core.getWidget('AddEncouragement').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addEncWidget.Add(_self.PrisonerID, function (res) {
                if (res != null) {

                    // get encouragement list
                    getEncouragementList();
                }
            });
        });

        addFileBtn.unbind();
        addFileBtn.bind('click', function () {

            var data = {};
            data.TypeID = 26;
            data.PrisonerID = _self.PrisonerID;

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });
    }

    // bind search events
    function bindSearchEvents() {
        var resetBtn = $('#reloadFilterBtn');
        var acceptBtn = $('#acceptFilterBtn');
        var TypeLibItemListConvicts = $('#TypeLibItemListConvicts');
        var TypeLibItemListPrisoners = $('#TypeLibItemListPrisoners');
        var orgunitlist = $('#orgunitidlist');
        var dates = $('.FilterDates');
        var PrisonerType = $('#PrisonerType');

        acceptBtn.unbind().click(function (e) {
            e.preventDefault();
            _core.getService('Loading').Service.enableBlur('loaderClass');

            debugger;
            var finalData = {};
            $("#filterPersonForm").serializeArray().map(function (x) { finalData[x.name] = x.value; });
            finalData['OrgUnitIDList'] = orgunitlist.val();
            finalData['TypeLibItemList'] = $.merge(TypeLibItemListConvicts.val() || [], TypeLibItemListPrisoners.val() || []);

            // filter list
            filterList(finalData);
        });

        PrisonerType.unbind().bind('change', function () {
            if ($(this).val() == "3") {
                $('#convictTypeLibLIst').addClass("hidden");
                $('#prisonerTypeLibList').removeClass("hidden");
                $('#TypeLibItemListConvicts')[0].selectize.clear();
                $('#TypeLibItemListPrisoners')[0].selectize.clear();
            }
            else {
                $('#convictTypeLibLIst').removeClass("hidden");
                $('#prisonerTypeLibList').addClass("hidden");
                $('#TypeLibItemListConvicts')[0].selectize.clear();
                $('#TypeLibItemListPrisoners')[0].selectize.clear();
            }
        })

        resetBtn.unbind().click(function (e)
        {
            _core.getService('Loading').Service.enableBlur('loaderClass');

            resetList(function (res)
            {
                if (res.status)
                    location.reload();
                _core.getService('Loading').Service.disableBlur('loaderClass');

            });

        });
        $('.selectizeFilterEncouragementsSearch').selectize({
            valueField: 'ID',
            labelField: 'value',
            searchField: 'value',
            create: false,
            render: {
                item: function (item, escape) {
                    console.log(item);
                    return '<div>' +
                        item.Person.FirstName +
                    '</div>';
                },
                option: function (item, escape) {
                    console.log(item);
                    return '<div>' +
                        item.Person.FirstName +
                    '</div>';
                }
            },
            score: function (search) {
                var score = this.getScoreFunction(search);
                return function (item) {
                    return score(item) * (1 + Math.min(item.watchers / 100, 1));
                };
            },
            load: function (query, callback) {
                if (!query.length) return callback();

                var data = {};
                data.query = query;

                var url = _self.searchPrisonerURL;

                _core.getService('Post').Service.postJson(data, url, function (res) {
                    console.log(res);
                    if (res != null && res.status) {
                        callback(res.prisoners.slice(0, 10));
                    }
                })
            }
        });

        $(".selectizeFilterEncouragements").each(function () {
            $(this).selectize({
                create: false
                //plugins: {
                //    'no-delete': {}
                //},
            });
        });

        $(".selectizeFilterEncouragementsArchive").each(function () {
            $(this).selectize({
                create: false,
                plugins: {
                    'no-delete': {}
                },
            });
        });

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
        });

    }

    // bind encouragement table rows events
    function bindEncouragementsRowEvents() {
        var removeRows = $('.removeEncRow');
        var editRows = $('.editEncRow');
        var viewRows = $('.viewEncRow');

        // remove row
        removeRows.unbind();
        removeRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    var addEncWidget = _core.getWidget('AddEncouragement').Widget;

                    // run
                    addEncWidget.Remove(id, function (res) {
                        if (res != null) {

                            // get cell list
                            getEncouragementList();
                        }
                    });
                }
            })
        });
        
        // view row
        viewRows.unbind();
        viewRows.bind('click', function ()
        {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addEncWidget = _core.getWidget('AddEncouragement').Widget;

            // run
            addEncWidget.View(_self.PrisonerID, id, function (res)
            {

              
            });
        });
        // edit row
        editRows.unbind();
        editRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addEncWidget = _core.getWidget('AddEncouragement').Widget;

            // run
            addEncWidget.Edit(_self.PrisonerID, id, function (res) {
                
                if (res != null) {

                    // get cell list
                    getEncouragementList();
                }
            });
        });
    }
    function resetList(callback)
    {

        // postService
        var postService = _core.getService('Post').Service;

        postService.postJsonReturn(null, _self.resetURL, callback);
    }
    function bindPopStateEvent()
    {
        window.addEventListener('popstate', function (e)
        {
            var character = e.state;

            _core.getService('Loading').Service.enableBlur('loaderClass');
            var postService = _core.getService('Post').Service;
            var tempUrl = '/Encouragements/Draw_EncouragementTableRow';
            if (character == null)
            {
                postService.postPartial(null, tempUrl, function (res)
                {
                    _core.getService('Loading').Service.disableBlur('loaderClass');
                    if (res != null)
                    {
                        $('#filterList').empty().append(res)

                        // bind pagination buttons
                        bindPaginationEvent();


                    }
                })
            } else
            {
                postService.postPartial(null, character.url, function (res)
                {
                    _core.getService('Loading').Service.disableBlur('loaderClass');
                    if (res != null)
                    {
                        $('#filterList').empty().append(res)

                        // bind pagination buttons
                        bindPaginationEvent();


                    }
                })
            }

        });

    }
    // bind files events
    function bindFilesEvents() {

        var removeBtn = $('.removeFileButton');
        var editBtn = $('.editFileButton');
        var viewBtn = $('.viewFileButton');

        // remove file actin handle
        removeBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Remove(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
        viewBtn
        // view file action handle
        viewBtn.unbind().click(function ()
        {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.View(data, function (res)
            {
                
            })
        });
        // edit file action handle
        editBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Edit(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
    }

     // bind pagination events
    function bindPaginationEvent() {
        var pagBtn = $('.paginationBtn');

        pagBtn.unbind();
        pagBtn.click(function (e) {
            e.preventDefault();
            _core.getService('Loading').Service.enableBlur('loaderClass');

            var url = $(this).attr('href');

            // post service
            var postService = _core.getService('Post').Service;
            var element = $(this);

            postService.postPartial(null, url, function (res)
            {
                _core.getService('Loading').Service.enableBlur('loaderClass');

                if (res != null) {
                    $('#filterList').empty().append(res)
                    var tempUrl = '/Encouragements/Index/';
                    if (element.text().trim() != "" && !isNaN(element.text()))
                    {
                        window.history.pushState({ url: url }, "Page " + element.text(), tempUrl + element.text());

                    }
                    else
                    {
                        console.log(element);
                        var pageNumber = element.attr('data-pagenumber');
                        window.history.pushState({ url: url }, "Page " + pageNumber, tempUrl + pageNumber);

                    }
                    // bind pagination buttons
                    bindPaginationEvent();
                }
            })
        })
    }

    // filter list
    function filterList(data) {

        // postService
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.filterURL, function (res) {
            if (res != null) {
                $('#filterList').empty().append(res)

                // bind pagination buttons
                bindPaginationEvent();
                _core.getService('Loading').Service.disableBlur('loaderClass');

            }
        })
    }

    // get file list table rows
    function getFileList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.TypeID = type;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getFileTableRowURL, function (curHtml) {

            var table = null;
            table = $('#addfileTableBody');

            table.empty();
            table.append(curHtml);

            // bind file events
            bindFilesEvents();
        });
    }

    // get cell list table rows
    function getEncouragementList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        _core.getService('Loading').Service.enableBlur('enctablebody');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getEncTableRowURL, function (curHtml) {

            var table = null;
            table = $('#enctablebody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('enctablebody');

            bindEncouragementsRowEvents();
        });
    }

}
/************************/


// creating class instance
var EncouragementsController = new EncouragementsControllerClass();

// creating object
var EncouragementsControllerObject = {
    // important
    Name: "Encouragements",

    Controller: EncouragementsController
}

// registering controller object
_core.addController(EncouragementsControllerObject);