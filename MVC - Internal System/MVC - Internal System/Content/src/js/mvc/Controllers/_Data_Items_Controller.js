﻿// items controller

/*****Main Function******/
var _Data_Items_ControllerClass = function () {
    var _self = this;

    _self.PrisonerID = null;

    _self.getFileTableRowURL = '/_Popup_/Get_AddFileTableRow';
    _self.getItemTableRowURL = '/_Popup_/Get_ItemsObjectsTableRows';


    _self.mode = "Add" // initial mode


    _self.init = function (Action) {
        console.log('_Data_Items_ Controller Inited with action', Action);

        _self.mode = Action;

        _self.callback = null;
        _self.PrisonerID = null;

        // Js Bindings
        bindButtons();

        // items table row events
        bindItemRowEvents();

        // bind file events
        bindFilesEvents();
    }

    // add action
    _self.Add = function () {
        console.log('_Data_Items_ add called');

        _self.init('Add');
    }

    // edit action
    _self.Edit = function (PrisonerID) {

        console.log('_Data_Items_ edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;
    }

    _self.save = function (callback) {
        console.log('_Data_Items_ data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }

    // js bindings
    function bindButtons() {

        var addItemBtn = $('#additemobjectBtn');
        var addFileBtn = $('#addfileBtn');

        addItemBtn.unbind();
        addItemBtn.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var additemWidget = _core.getWidget('ItemsObjects').Widget;

            // run
            additemWidget.Add(_self.PrisonerID, function (res) {

                // get item list
                getitemList();
            });
        });

        addFileBtn.unbind();
        addFileBtn.bind('click', function () {

            var data = {};
            data.TypeID = 11;
            data.PrisonerID = _self.PrisonerID;

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });
    }

    // bind items table rows events
    function bindItemRowEvents() {
        var removeRows = $('.removeItemRow');
        var editRows = $('.editItemRow');
        var viewRows = $('.viewItemRow');

        // remove
        removeRows.unbind();
        removeRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var additemWidget = _core.getWidget('ItemsObjects').Widget;

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {
                if (res) {

                    // run
                    additemWidget.Remove(id, function (res) {

                        // get item list
                        getitemList();
                    });
                }
            })
        });
        // view
        viewRows.unbind();
        viewRows.bind('click', function ()
        {

            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var additemWidget = _core.getWidget('ItemsObjects').Widget;

            // run
            additemWidget.View(id, function (res)
            {

                // get item list
                getitemList();
            });
        });
        // edit
        editRows.unbind();
        editRows.bind('click', function () {

            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var additemWidget = _core.getWidget('ItemsObjects').Widget;
            
            // run
            additemWidget.Edit(id, function (res) {

                // get item list
                getitemList();
            });
        });
    }

    // bind files events
    function bindFilesEvents() {

        var removeBtn = $('.removeFileButton');
        var editBtn = $('.editFileButton');
        var viewBtn = $('.viewFileButton');

        // remove file actin handle
        removeBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Remove(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });

        // edit file action handle
        editBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Edit(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
        // view file action handle

        viewBtn.unbind().click(function ()
        {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.View(data, function (res)
            {
                
            })
        });
    }

    // get file list table rows
    function getFileList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.TypeID = type;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getFileTableRowURL, function (curHtml) {

            var table = null;
            table = $('#addfileTableBody');

            table.empty();
            table.append(curHtml);

            // bind file events
            bindFilesEvents();
        });
    }

    // get file list table rows
    function getitemList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('itemsobjectstablebody');

        postService.postPartial(data, _self.getItemTableRowURL, function (curHtml) {

            var table = null;
            table = $('#itemsobjectstablebody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('itemsobjectstablebody');

            bindItemRowEvents();
        });
    }

}
/************************/


// creating class instance
var _Data_Items_Controller = new _Data_Items_ControllerClass();

// creating object
var _Data_Items_ControllerObject = {
    // important
    Name: '_Data_Items_',

    Controller: _Data_Items_Controller
}

// registering controller object
_core.addController(_Data_Items_ControllerObject);