﻿// Family controller

/*****Main Function******/
var _Data_Family_ControllerClass = function () {
    var _self = this;

    _self.addFileURL = '/_Data_Main_/AddFile';
    _self.getFileTableRowURL = '/_Popup_/Get_AddFileTableRow';
    _self.SaveFamlityStatusURL = '/_Data_Family_/SaveFamiltyStatus';
    

    // add to databse
    _self.getFamilyURL = {};
    _self.getFamilyURL['spouse'] = '/_Popup_/Get_SpousesTableRow';
    _self.getFamilyURL['children'] = '/_Popup_/Get_ChildrensTableRow';
    _self.getFamilyURL['parent'] = '/_Popup_/Get_ParentsTableRow';
    _self.getFamilyURL['systerbrother'] = '/_Popup_/Get_SisterBrotherTableRow';
    _self.getFamilyURL['otherrelative'] = '/_Popup_/Get_OtherRelativeTableRow';

    // table bodies
    _self.FamilyTable = {};
    _self.FamilyTable['spouse'] = 'addspousetablebody';
    _self.FamilyTable['children'] = 'addchildrentablebody';
    _self.FamilyTable['parent'] = 'addparenttablebody';
    _self.FamilyTable['systerbrother'] = 'addsysterbrothertablebody';
    _self.FamilyTable['otherrelative'] = 'addotherrelativetablebody';

    _self.getClass = {};
    _self.getClass['spouse'] = 'spouseLoader';
    _self.getClass['children'] = 'childrenLoader';
    _self.getClass['parent'] = 'parentLoader';
    _self.getClass['systerbrother'] = 'systerLoader';
    _self.getClass['otherrelative'] = 'otherLoader';

    _self.curType = null;

    _self.mode = "Add" // initial mode


    _self.init = function (Action) {
        console.log('_Data_Family_ Controller Inited with action', Action);

        _self.mode = Action;

        _self.callback = null;
        _self.PrisonerID = null;
        _self.curType = null;

        // Js Bindings
        bindButtons();

        // bind has, dont have buttons
        bindSwitchEvents();

        // bind file events
        bindFilesEvents();

        // bindings
        bindRelativeTableRowEvents();
    }

    // add action
    _self.Add = function () {
        console.log('_Data_Family_ add called');

        _self.init('Add');
    }

    // edit action
    _self.Edit = function (PrisonerID) {

        console.log('_Data_Family_ edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;
    }

    _self.save = function (callback) {
        console.log('_Data_Family_ data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }

    // js bindings
    function bindButtons() {

        var addRelativeBtn = $('.addRelativeBtn');
        var addFileBtn = $('#addfileBtn');
        var acceptFamilyStatusButton = $('#acceptFamilyStatusButton');
        var FamilyStatusID = $('#FamilyStatusID');

        addRelativeBtn.unbind();
        addRelativeBtn.bind('click', function () {
            
            var type = $(this).data('type');

            // call add file widget (image type)
            var addRelativeWidget = _core.getWidget('FamiltyRelative').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addRelativeWidget.Add(type, _self.PrisonerID, function (res) {
                
                if (res != null && res.status) {
                    getList(type);
                }
            });
        });

        addFileBtn.unbind();
        addFileBtn.bind('click', function () {

            var data = {};
            data.TypeID = 6;
            data.PrisonerID = _self.PrisonerID;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });

        FamilyStatusID.unbind().bind('change keyup', function () {
            var status = false;
            var curValue = FamilyStatusID.val();
            if (curValue == null || curValue == "")
                status = true;
            acceptFamilyStatusButton.attr('disabled', status);
        });

        acceptFamilyStatusButton.unbind().bind('click', function () {

            var data = {};
            data.FamilyStatusID = FamilyStatusID.val();
            data.PrisonerID = _self.PrisonerID;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // post service
            var postService = _core.getService('Post').Service;

            postService.postJson(data, _self.SaveFamlityStatusURL, function (res) {

                if (typeof res.needApprove != "undefined" && res.needApprove) {
                    $('.familyMergeWarningClass').removeClass("hidden");
                    $('.mainMergeWarningClass').removeClass("hidden");
                }

                _core.getService('Loading').Service.disableBlur('loaderClass');

                acceptFamilyStatusButton.attr('disabled', true);
            });
        });

        // selectize
        $(".selectize").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // bind Switch buttons
    function bindSwitchEvents() {

        var familyChildrenStatus = $("#family-children-status");

        function familyChildrenStatusVals() {
            var familyChildrenStatusValue = familyChildrenStatus.val();
            if (familyChildrenStatusValue == "2") {
                $("#family-children-wrap").removeClass("hidden");
            }
            else {
                $("#family-children-wrap").addClass("hidden");
            }
        }

        familyChildrenStatus.change(familyChildrenStatusVals);
        familyChildrenStatusVals();

        var familySiblingsStatus = $("#family-siblings-status");

        function familySiblingsStatusVals() {
            var familySiblingsStatusValue = familySiblingsStatus.val();
            if (familySiblingsStatusValue == "2") {
                $("#family-siblings-wrap").removeClass("hidden");
            }
            else {
                $("#family-siblings-wrap").addClass("hidden");
            }
        }

        familySiblingsStatus.change(familySiblingsStatusVals);
        familySiblingsStatusVals();

        var familyOtherRelativesStatus = $("#family-other-relatives-status");

        function familyOtherRelativesStatusVals() {
            var familyOtherRelativesStatusValue = familyOtherRelativesStatus.val();
            if (familyOtherRelativesStatusValue == "2") {
                $("#family-other-relatives-wrap").removeClass("hidden");
            }
            else {
                $("#family-other-relatives-wrap").addClass("hidden");
            }
        }

        familyOtherRelativesStatus.change(familyOtherRelativesStatusVals);
        familyOtherRelativesStatusVals();
    }

    function bindFilesEvents() {

        var removeBtn = $('.removeFileButton');
        var editBtn = $('.editFileButton');
        var viewBtn = $('.viewFileButton');

        // remove file actin handle
        removeBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Remove(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
        
        // view file action handle
        viewBtn.unbind().click(function ()
        {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.View(data, function (res)
            {
               
            })
        });
        // edit file action handle
        editBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Edit(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
    }

    // Add file to database
    function addFile(data, type) {

        // appending prisoner id to formdata
        data.append('PrisonerID', _self.PrisonerID);
        data.append('Type', type);

        //// log it
        //for (var pair of data.entries()) {
        //    console.log(pair[0] + ', ' + pair[1]);
        //}

        // post service
        var postService = _core.getService('Post').Service;

        postService.postFormData(data, _self.addFileURL, function (res) {
            if (res.status) {
                getFileList(res.type);
            }
            else alert('serious eror on adding file');
        })
    }

    // get file list table rows
    function getFileList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.TypeID = type;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('fileLoader');

        postService.postPartial(data, _self.getFileTableRowURL, function (curHtml) {

            var table = null;
            table = $('#addfileTableBody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('fileLoader');

            // bind file Events
            bindFilesEvents();
        });
    }

    // get all list
    function getList(type) {
    
        var url = null;
        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur(_self.getClass[type]);

        postService.postPartial(data, _self.getFamilyURL[type], function (curHtml) {

            var table = null;
            table = $('#'+_self.FamilyTable[type]);

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur(_self.getClass[type]);

            // bindings
            bindRelativeTableRowEvents();
        });
    }

    // relative table row bindings
    function bindRelativeTableRowEvents() {
        var removeRows = $('.removeRelativeRow');
        var editRows = $('.editRelativeRow');
        var viewRows = $('.viewRelativeRow');

        // remove cell
        removeRows.unbind();
        removeRows.bind('click', function () {

            // data
            var id = parseInt($(this).data('id'));
            var type = $(this).data('type');

            // call add file widget (image type)
            var addRelativeWidget = _core.getWidget('FamiltyRelative').Widget;

            // run
            addRelativeWidget.Remove(id, type, function (data) {
                getList(type);
            });
        });
        
        // view cell
        viewRows.unbind().click(function ()
        {

            //data
            var id = parseInt($(this).data('id'));
            var type = $(this).data('type');

            // call add file widget (image type)
            var addRelativeWidget = _core.getWidget('FamiltyRelative').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addRelativeWidget.View(id, type, _self.PrisonerID, function (data)
            {
                getList(type);
            });
        });
        // edit cell
        editRows.unbind().click(function () {

            //data
            var id = parseInt($(this).data('id'));
            var type = $(this).data('type');

            // call add file widget (image type)
            var addRelativeWidget = _core.getWidget('FamiltyRelative').Widget;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // run
            addRelativeWidget.Edit(id, type, _self.PrisonerID, function (data) {
                getList(type);
            });
        });
    }


}
/************************/


// creating class instance
var _Data_Family_Controller = new _Data_Family_ControllerClass();

// creating object
var _Data_Family_ControllerObject = {
    // important
    Name: '_Data_Family_',

    Controller: _Data_Family_Controller
}

// registering controller object
_core.addController(_Data_Family_ControllerObject);