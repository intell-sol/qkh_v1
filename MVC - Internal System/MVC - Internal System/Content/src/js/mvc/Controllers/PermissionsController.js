﻿// Permissions controller

/*****Main Function******/
var PermissionsControllerClass = function () {
    var _self = this;

    _self.selectOrgUnitURL = '/Base/SelectOrgUnit';
    _self.PrisonerID = null;

    _self.searchPrisonerURL = "/Convicts/SearchPrisoner";

    _self.getFileTableRowURL = '/_Popup_/Get_AddFileTableRow';
    _self.getMainTableRowURL = '/_Popup_/Get_WorkLoadTableRows';
    _self.filterURL = '/Permissions/Draw_PermissionsTableRowsByFilter';
    _self.addPermissionURL = '/Permissions/Add';
    _self.remPermissionURL = '/Permissions/Remove';
    _self.getPermissionsTableRowURL = '/Permissions/Draw_PermissionsTableRows';

    _self.mode = "Add" // initial mode

    _self.init = function (Action) {
        console.log('Controller Inited with action', Action);

        _self.mode = Action;

        _self.callback = null;
        _self.PrisonerID = null;

        // Js Bindings
        bindButtons();

        // items table row events
        bindMainTableRowEvents();

        // bind file events
        bindFilesEvents();
    }

    // index action
    _self.Index = function () {
        console.log('add called');

        _self.init('Index');

        // bind pagination
        bindPaginationEvent();

        // bind search events
        //bindSearchEvents();
        bindPermissionAddEvents();
        bindPermissionTableRowEvents();
    }

    // add action
    _self.Add = function () {
        console.log('add called');

        _self.init('Add');
    }

    // edit action
    _self.Edit = function (PrisonerID) {

        console.log('edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;
    }

    _self.save = function (callback) {
        console.log('data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }

    // js bindings
    function bindButtons() {

        var addMainBtn = $('#addMainBtn');
        var addFileBtn = $('#addfileBtn');
        var addPermissionBtn = $('#addPermissionBtn')

        addMainBtn.unbind();
        addMainBtn.bind('click', function () {

            var addMainWidget = _core.getWidget('WorkLoads').Widget;

            // run
            addMainWidget.Add(_self.PrisonerID, function (res) {
                if (res != null) {

                    // get penalty list
                    getMainTableList();
                }
            });
        });

        addFileBtn.unbind();
        addFileBtn.bind('click', function () {

            var data = {};
            data.TypeID = 29;
            data.PrisonerID = _self.PrisonerID;

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });


        addPermissionBtn.unbind().bind('click', function (e) {
            e.preventDefault();

            _core.getService('Loading').Service.enableBlur('loaderClass');

            _core.getWidget('AddPermission').Widget.Add(function (res) {
                if (res != null) {
                    getPermissionList();
                }
            });
        });
    }

    // bind pagination events
    function bindPaginationEvent() {
        var pagBtn = $('.paginationBtn');

        pagBtn.unbind();
        pagBtn.click(function (e) {
            e.preventDefault();

            var url = $(this).attr('href');

            // post service
            var postService = _core.getService('Post').Service;

            postService.postPartial(null, url, function (res) {
                if (res != null) {
                    $('#filterList').empty().append(res)

                    // bind pagination buttons
                    bindPaginationEvent();
                }
            })
        })
    }

    // bind search events
    function bindSearchEvents() {

        //var JobTypeLibItemID = $('#JobTypeLibItemID');
        var PermissionID = $('#PermissionID');
        var dates = $('.FilterDates');


        $('.selectizeFilterSearch').selectize({
            valueField: 'ID',
            labelField: 'value',
            searchField: 'value',
            create: false,
            render: {
                item: function (item, escape) {
                    console.log(item);
                    return '<div>' +
                        item.Person.FirstName +
                    '</div>';
                },
                option: function (item, escape) {
                    console.log(item);
                    return '<div>' +
                        item.Person.FirstName +
                    '</div>';
                }
            },
            score: function (search) {
                var score = this.getScoreFunction(search);
                return function (item) {
                    return score(item) * (1 + Math.min(item.watchers / 100, 1));
                };
            },
            load: function (query, callback) {
                if (!query.length) return callback();

                var data = {};
                data.query = query;

                var url = _self.searchPrisonerURL;

                _core.getService('Post').Service.postJson(data, url, function (res) {
                    console.log(res);
                    if (res != null && res.status) {
                        callback(res.prisoners.slice(0, 10));
                    }
                })
            }
        });

        $(".selectizeFilter").each(function () {
            $(this).selectize({
                create: false
                //plugins: {
                //    'no-delete': {}
                //},
            });
        });

        $(".selectizeFilterArchive").each(function () {
            $(this).selectize({
                create: false,
                plugins: {
                    'no-delete': {}
                },
            });
        });

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
        });

    }

    // bind search events
    function bindPermissionAddEvents() {

        //var acceptBtn = $('#addPermissionBtn');
        //var JobTypeLibItemID = $('#JobTypeLibItemID');
        var PermissionID = $('#PermissionID');
        var dates = $('.FilterDates');

        //acceptBtn.unbind().click(function (e) {
        //    e.preventDefault();

        //    var finalData = {};
        //    $("#filterForm").serializeArray().map(function (x) { finalData[x.name] = x.value; });
        //    finalData['PermissionIDs'] = PermissionID.val();

        //    // filter list
        //    addPermistion(finalData);
        //});

        $('.selectizeFilterSearch').selectize({
            valueField: 'ID',
            labelField: 'value',
            searchField: 'value',
            create: false,
            render: {
                item: function (item, escape) {
                    console.log(item);
                    return '<div>' +
                        item.Person.FirstName +
                    '</div>';
                },
                option: function (item, escape) {
                    console.log(item);
                    return '<div>' +
                        item.Person.FirstName +
                    '</div>';
                }
            },
            score: function (search) {
                var score = this.getScoreFunction(search);
                return function (item) {
                    return score(item) * (1 + Math.min(item.watchers / 100, 1));
                };
            },
            load: function (query, callback) {
                if (!query.length) return callback();

                var data = {};
                data.query = query;

                var url = _self.searchPrisonerURL;

                _core.getService('Post').Service.postJson(data, url, function (res) {
                    console.log(res);
                    if (res != null && res.status) {
                        callback(res.prisoners.slice(0, 10));
                    }
                })
            }
        });

        $(".selectizeFilter").each(function () {
            $(this).selectize({
                create: false
                //plugins: {
                //    'no-delete': {}
                //},
            });
        });

        $(".selectizeFilterArchive").each(function () {
            $(this).selectize({
                create: false,
                plugins: {
                    'no-delete': {}
                },
            });
        });

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
        });

    }

    // bind penalty table rows events
    function bindMainTableRowEvents() {
        var removeRows = $('.removeMainRow');
        var editRows = $('.editMainRow');

        // remove row
        removeRows.unbind();
        removeRows.bind('click', function () {
            var id = parseInt($(this).data('id'));
            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('WorkLoads').Widget;

                    // run
                    addMainWidget.Remove(id, function (res) {
                        if (res != null) {

                            // get penalty list
                            getMainTableList();
                        }
                    });
                }
            })
        });

        // edit row
        editRows.unbind();
        editRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            var addMainWidget = _core.getWidget('WorkLoads').Widget;

            // run
            addMainWidget.Edit(id, function (res) {
                
                if (res != null) {

                    // get penalty list
                    getMainTableList();
                }
            });
        });
    }

    // bind penalty table rows events
    function bindPermissionTableRowEvents() {
        var closeRows = $('.closePermission');

        // remove row
        closeRows.unbind();
        closeRows.bind('click', function () {
            var id = parseInt($(this).data('id'));
            var data = {};
            data['ID'] = $(this).data('id');
            //data['PermToPosID'] = $(this).data('permtoposid');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                _core.getService('Loading').Service.disableBlur('loaderClass');

                if (res) {

                    // postService
                    var postService = _core.getService('Post').Service;

                    postService.postJson(data, _self.remPermissionURL, function (res) {
                        if (res != null) {

                            // get penalty list
                            getPermissionList();
                        }
                    })
                }
            })
        });
    }

    // bind files events
    function bindFilesEvents() {

        var removeBtn = $('.removeFileButton');
        var editBtn = $('.editFileButton');

        // remove file actin handle
        removeBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Remove(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });

        // edit file action handle
        editBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Edit(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
    }

    // filter list
    function addPermistion(data) {

        // postService
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.addPermissionURL, function (res) {
            if (res != null && res.status) {

                // get permission list
                getPermissionList();
            }
        })
    }

    // filter list
    function filterList(data) {

        // postService
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.filterURL, function (res) {
            if (res != null) {
                $('#filterList').empty().append(res)

                // bind pagination buttons
                bindPaginationEvent();
            }
        })
    }

    // get file list table rows
    function getFileList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.TypeID = type;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getFileTableRowURL, function (curHtml) {

            var table = null;
            table = $('#addfileTableBody');

            table.empty();
            table.append(curHtml);

            // bind file events
            bindFilesEvents();
        });
    }

    // get educational course list table rows
    function getMainTableList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getMainTableRowURL, function (curHtml) {

            var table = null;
            table = $('#maintablebody');

            table.empty();
            table.append(curHtml);

            bindMainTableRowEvents();
        });
    }

    // get educational course list table rows
    function getPermissionList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('permissionLoaderClass');

        postService.postPartial(data, _self.getPermissionsTableRowURL, function (curHtml) {

            var table = null;
            table = $('#permissiontablebody');

            _core.getService('Loading').Service.disableBlur('permissionLoaderClass');

            table.empty();
            table.append(curHtml);

            bindPermissionTableRowEvents();
        });
    }

}
/************************/


// creating class instance
var PermissionsController = new PermissionsControllerClass();

// creating object
var PermissionsControllerObject = {
    // important
    Name: "Permissions",

    Controller: PermissionsController
}

// registering controller object
_core.addController(PermissionsControllerObject);