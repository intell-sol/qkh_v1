﻿
/*****Main Function******/
var ConvictsControllerClass = function () {
    var _self = this;
    
    _self.subControllers = {
        //0: {
        //    name: "_data_initial_",
        //    status: false,
        //    primary: true,
        //    id: 'subcontrol0',
        //    lock: false
        //},
        //1: {
        //    name: "_data_primary_",
        //    status: false,
        //    primary: false,
        //    id: 'subcontrol1',
        //    lock: false
        //},
        //2: {
        //    name: "_data_physical_",
        //    status: false,
        //    primary: false,
        //    id: 'subcontrol2',
        //    lock: false
        //},
        //3: {
        //    name: "_data_finger_tattoo_",
        //    status: false,
        //    primary: false,
        //    id: 'subcontrol3',
        //    lock: false
        //},
        //4: {
        //    name: "_data_family_",
        //    status: false,
        //    primary: false,
        //    id: 'subcontrol4',
        //    lock: false
        //},
        //5: {
        //    name: "_data_education_",
        //    status: false,
        //    primary: false,
        //    id: 'subcontrol5',
        //    lock: false
        //},
        //6: {
        //    name: "_data_army_",
        //    status: false,
        //    primary: false,
        //    id: 'subcontrol6',
        //    lock: false
        //},
        //7: {
        //    name: "_data_enterance_",
        //    status: false,
        //    primary: false,
        //    id: 'subcontrol7',
        //    lock: false
        //},
        //8: {
        //    name: "_data_cell_",
        //    status: false,
        //    primary: false,
        //    id: 'subcontrol8',
        //    lock: false
        //},
        //9: {
        //    name: "_data_items_",
        //    status: false,
        //    primary: false,
        //    id: 'subcontrol9',
        //    lock: false
        //},
        //10: {
        //    name: "_data_ban_",
        //    status: false,
        //    primary: false,
        //    id: 'subcontrol10',
        //    lock: false
        //}
    }

    var controllerName = {};
    controllerName[2] = "Convicts";
    controllerName[3] = "Prisoners";

    _self.selectOrgUnitURL = '/Base/SelectOrgUnit';
    _self.getPreviewPartial = {};
    _self.getPreviewPartial[2] = '/Convicts/GetPreview';
    _self.getPreviewPartial[3] = '/Prisoners/GetPreview';
    _self.approvePrisonerURL = {};
    _self.approvePrisonerURL[2] = '/Convicts/ApprovePrisoner';
    _self.approvePrisonerURL[3] = '/Prisoners/ApprovePrisoner';
    _self.filterURL = {};
    _self.filterURL[2] = '/Convicts/Draw_ConvictsTableRowsByFilter';
    _self.filterURL[3] = '/Prisoners/Draw_ConvictsTableRowsByFilter';
    _self.resetURL = '/Convicts/Draw_ResetConvictsTableRows';
    _self.resetURLPrisoners = '/Prisoners/Draw_ResetConvictsTableRows';
    _self.checkMergeApproveURL = "/_Data_Main_/checkMergeApprove";
    _self.checkDepartMergeApproveURL = "/_Data_Main_/CheckMergeEIDs";

    _self.startControllerIndex = 0; // initial index
    _self.mode = 'Add'; // initial mode
    _self.action = null;
    _self.PrisonerID = null;
    _self.ArchiveData = null;
    _self.PrisonerType = null;

    // INIT function
    _self.init = function () {
        console.log('Convicts Controller Inited');

        _self.startControllerIndex = 0; // initial index
        _self.mode = 'Add'; // initial mode
        _self.action = null;
        _self.PrisonerID = null;
        _self.ArchiveData = null;
        _self.PrisonerType = null;
       
        // build subcontrollers
        buildSubControllers();

        // execute some desing code
        designCodes();

        // bind add form buttons
        bindFormButtons();
    }

    // index action CALLED FROM INIT
    _self.Index = function (PrisonerType) {
        console.log('Convicts index called');

        // init call'
        _self.init();

        _self.PrisonerType = PrisonerType;

        bindPopStateEvent();
        // bind pagination
        bindPaginationEvent();
        // bind search events
        bindSearchEvents();
    }

    // add action
    _self.Add = function (PrisonerType) {
        console.log('Convicts Add called');

        // calling init
        _self.init();

        // setting Add Mode
        _self.mode = 'Add';

        // real action (mode is beeing changed)
        _self.action = 'Add';

        _self.PrisonerType = PrisonerType;

        // start from first part
        _self.startControllerIndex = 0;

        // load subController
        loadSubController(_self.startControllerIndex);
    }

    // edit action
    _self.Edit = function (PrisonerType, PrisonerID) {
        console.log('Convicts Edit called');

        // calling init
        _self.init();

        // setting Add Mode
        _self.mode = 'Edit';

        // real action (mode is beeing changed)
        _self.action = 'Edit';

        // setting Prisoner ID
        _self.PrisonerID = PrisonerID != null ? parseInt(PrisonerID) : importantWarning();
        if (typeof $("#ArchiveData")[0] != "undefined") {
            _self.ArchiveData = $("#ArchiveData")[0].selectize.getValue() == "true";
        }

        _self.PrisonerType = PrisonerType;

        // start from first part
        _self.startControllerIndex = getStartIndex();

        // load subController
        loadSubController(_self.startControllerIndex);
    }

    // view action
    _self.View = function (PrisonerType, PrisonerID) {
        console.log('Convicts View called');

        // calling init
        _self.init();

        // setting Add Mode
        _self.mode = 'Edit';

        // real action (mode is beeing changed)
        _self.action = 'Preview';

        // setting Prisoner ID
        _self.PrisonerID = PrisonerID != null ? parseInt(PrisonerID) : importantWarning();
        if (typeof $("#ArchiveData")[0] != "undefined") {
            _self.ArchiveData = $("#ArchiveData")[0].selectize.getValue() == "true";
        }

        _self.PrisonerType = PrisonerType;

        // start from first part
        _self.startControllerIndex = getStartIndex();

        // bind open case Events
        bindOpenCaseEvents();

        // load subController
        loadSubController(_self.startControllerIndex);
    }

    // get start index
    function getStartIndex() {

        var isThereRedirect = window.location.hash != '';

        // get hash and navigate to it
        if (isThereRedirect) {
            var id = 0;

            for (var i in _self.subControllers) {
                if (_self.subControllers[i].Name == window.location.href.split('#')[1]) {
                    _self.subControllers[i].isOpen = true;
                    var id = parseInt(_self.subControllers[i].idValue);
                }
            }

            return id;
        }
        else {
            for (var i in _self.subControllers) {
                //_self.subControllers[i].Primary
                if (true) return parseInt(i);
            }
        }

    }

    // build subcontrollers
    function buildSubControllers() {
        
        var subControllerListElem = $('#subControllerList');

        _self.subControllers = {};

        subControllerListElem.find(".subControllerListLi").each(function () {

            var id = $(this).data('id');

            // create subcontroller object
            var curObject = {
                Name: $(this).data('controller'),
                Status: false,
                Primary: parseInt(id) == 0,
                ID: 'subControl'+id,
                idValue: id,
                Lock: false,
                isOpen: false
            };

            // bind subcontroller object to main subcontroller list
            _self.subControllers[parseInt(id)] = curObject;

        });

    }

    // bind form buttons (go forward, backward, skip, etc)   CALLED FROM INIT
    function bindFormButtons() {

        // form button main elements
        var prevButton = $('.btn-add-page-prev');
        var passButton = $('.btn-add-page-pass');
        var nextButton = $('.btn-add-page-next');
        var previewButton = $('.btn-add-page-preview');
        var menuButton = $('.subControllerListA');


        // unbinding any action
        prevButton.unbind();
        passButton.unbind();
        nextButton.unbind();
        previewButton.unbind();
        menuButton.unbind();

        // on previouse button press
        prevButton.bind('click', function ()
        {
         
            switchSubController('prev', null);
        });

        // on next button press
        nextButton.bind('click', function ()
        {
            switchSubController('next', null);
        });

        // on pass button press
        passButton.bind('click', function ()
        {
          
            switchSubController('pass', null);
        });

        // on pass button press
        previewButton.bind('click', function ()
        {
           

            // get preview modal
            getPreviewModal();
        });

        // on menu button press
        menuButton.bind('click', function (e)
        {
            e.preventDefault();
            var ID = parseInt($(this).data('id'));
            switchSubController('goto', ID);
        });

        // selectize archive status
        $(".selectizeArchiveData").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    function bindPopStateEvent()
    {
        window.addEventListener('popstate', function (e)
        {
            var character = e.state;

            _core.getService('Loading').Service.enableBlur('loaderClass');
            var postService = _core.getService('Post').Service;
            var tempUrl = '/' + controllerName[_self.PrisonerType] + '/Draw_ConvictsTableRows';
            if (character == null)
            {
                postService.postPartial(null, tempUrl, function (res)
                {
                    _core.getService('Loading').Service.disableBlur('loaderClass');
                    if (res != null)
                    {
                        $('#convictsList').empty().append(res)

                        // bind pagination buttons
                        bindPaginationEvent();


                    }
                })
            } else
            {
                postService.postPartial(null, character.url, function (res)
                {
                    _core.getService('Loading').Service.disableBlur('loaderClass');
                    if (res != null)
                    {
                        $('#convictsList').empty().append(res)

                        // bind pagination buttons
                        bindPaginationEvent();


                    }
                })
            }
            
        });

    }

    // bind pagination events
    function bindPaginationEvent() {
        var pagBtn = $('.paginationBtn');
       

        pagBtn.unbind();
        pagBtn.click(function (e) {
            e.preventDefault();
            _core.getService('Loading').Service.enableBlur('loaderClass');
            var url = $(this).attr('href');

            // post service
            var postService = _core.getService('Post').Service;
            var element = $(this);
            postService.postPartial(null, url, function (res)
            {
                _core.getService('Loading').Service.disableBlur('loaderClass');
                if (res != null) {
                    $('#convictsList').empty().append(res);

                    var tempUrl = '/' + controllerName[_self.PrisonerType] + '/Index/';
                    if (element.text().trim() != "" && !isNaN(element.text()))
                    {
                        window.history.pushState({ url: url }, "Page " + element.text(), tempUrl + element.text());

                    }
                    else
                    {
                        console.log(element);
                        var pageNumber = element.attr('data-pagenumber');
                        window.history.pushState({ url: url }, "Page " + pageNumber, tempUrl + pageNumber);

                    }
                    // bind pagination buttons
                    bindPaginationEvent();
                   

                }
            })
            
        })
    }

    

    // bind preview events
    function bindPreviewEvents() {
        
        var approvePrisonerBtn = $('#approvePrisonerBtn');

        approvePrisonerBtn.unbind().click(function (e) {
            e.preventDefault();

            // call approve prisoner function
            approvePrisoner();
        });

        disableAllEvents("previewWrapRow");
    }

    // bind search events
    function bindSearchEvents() {

        var acceptBtn = $('#acceptFilterPersonBtn');
        var resetBtn = $('#reloadFilterBtn')
        var citizenship = $('#convict-citizenship');
        var orgunitlist = $('#convict-qkc');

        acceptBtn.unbind().click(function (e) {
            e.preventDefault();
            _core.getService('Loading').Service.enableBlur('loaderClass');

            var finalData = {};
            $("#filterPersonForm").serializeArray().map(function (x) { finalData[x.name] = x.value; });
            finalData['CitizenshipLibItemIDList'] = citizenship.val();
            finalData['OrgUnitList'] = orgunitlist.val();
            finalData["Type"] = _self.PrisonerType;

            // filter person list
            filterPersonList(finalData);
        });
        resetBtn.unbind().click(function (e)
        {
            
            _core.getService('Loading').Service.enableBlur('loaderClass');

            resetList(function (res)
            {
                if (res.status)
                    location.reload();
                _core.getService('Loading').Service.disableBlur('loaderClass');

            });
            

        });
        $(".selectizeFilterPrisoner").each(function () {
            $(this).selectize({
                create: false,
            });
        });

        $(".selectizeFilterPrisonerArchive").each(function () {
            $(this).selectize({
                create: false,
                plugins: {
                    'no-delete': {}
                },
            });
        });

    }

    // bind open case events
    function bindOpenCaseEvents() {

        var openBtn = $('#openCaseBtn');

        openBtn.unbind().click(function () {

            _core.getWidget('CaseOpenTerminate').Widget.Show(_self.PrisonerID, function (res) {
                
                // action events
                openCaseActions();
            })

        });
    }

    // open case actions
    function openCaseActions() {

        var id = 0;

        for (var i in _self.subControllers) {
            if (_self.subControllers[i].Name == "_Data_Enterance_") {
                _self.subControllers[i].isOpen = true;
                var id = parseInt(_self.subControllers[i].idValue);
            }
        }


        // goto subcontroller
        switchSubController('goto', id);
    }

    // filter person list
    function filterPersonList(data) {

        // postService
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.filterURL[_self.PrisonerType], function (res) {
            if (res != null) {
                $('#convictsList').empty().append(res)

                // bind pagination buttons
                bindPaginationEvent();
                _core.getService('Loading').Service.disableBlur('loaderClass');

            }
        })
    }
    function resetList(callback)
    {
        // postService
        var postService = _core.getService('Post').Service;
        var url = null;
        if (_self.PrisonerType == 2)
        {
            url = _self.resetURL;
        } else
        {
            url = _self.resetURLPrisoners;
        }
        postService.postJsonReturn(null, url, callback)
        
    }
    // approve Prisoner Function
    function approvePrisoner() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // enable loading
        _core.getService('Loading').Service.enableBlur('previewLoadingPart');

        // postService
        var postService = _core.getService('Post').Service;

        postService.postJson(data, _self.approvePrisonerURL[_self.PrisonerType], function (res) {

            // enable loading
            _core.getService('Loading').Service.disableBlur('previewLoadingPart');

            if (res != null && res.status) {

                window.location.href = "/" + controllerName[_self.PrisonerType] + "/Edit/" + _self.PrisonerID;

            }
        });
    }

    // get preview modal
    function getPreviewModal() {

        $('#previewWrapRow').empty();

        // show modal
        $('.add-person-complete-modal').modal('show');

        // enable loading
        _core.getService('Loading').Service.enableBlur('previewLoadingPart');

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // postService
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getPreviewPartial[_self.PrisonerType], function (res) {

            $('#previewWrapRow').empty().append(res);

            // enable loading
            _core.getService('Loading').Service.disableBlur('previewLoadingPart');

            // bind preview buttons
            bindPreviewEvents();
        });
    }

    // toggle buttons
    function toggleFormButtons() {

        // control buttons
        var prevButton = $('.btn-add-page-prev');
        var passButton = $('.btn-add-page-pass');
        var nextButton = $('.btn-add-page-next');
        var previewButton = $('.btn-add-page-preview');

        var curIndex = _self.startControllerIndex; // current index
        var maxNum = Object.keys(_self.subControllers).length - 1; // max index

        // cases
        if (curIndex == 0) {
            prevButton.attr('disabled', true);
            passButton.attr('disabled', false);
            nextButton.attr('disabled', false);
            previewButton.attr('disabled', true);
        }
        else if (curIndex == maxNum) {
            prevButton.attr('disabled', false);
            passButton.attr('disabled', true);
            nextButton.attr('disabled', true);
            previewButton.attr('disabled', false);
        }
        else {
            prevButton.attr('disabled', false);
            passButton.attr('disabled', false);
            nextButton.attr('disabled', false);
            previewButton.attr('disabled', true);
        }
            
    }

    // view mode events
    function disableAllEvents(id) {

        var ElemID = "#" + id;

        // hide buttons
        $(ElemID).find("button").each(function () { $(this).addClass('hidden') });

        // disable and bind selectize
        $(ElemID).find(".selectize-sm").each(function () {
            $(this).attr('disabled', true);
            $(this).selectize({ create: false });
            $(this)[0].selectize.disable();
        });

        // disable and bind label add files
        $(ElemID).find(".add-file-label").each(function () {
            $(this).addClass('hidden');
        });

        // disable text areas
        $(ElemID).find("textarea").each(function () {
            $(this).attr('disabled', true);
        });

        // disable links to delete finger tatoos scars
        $(ElemID).find(".hideOnPreview").each(function () {
            $(this).addClass('hidden');
        });


        // bind custom select view event
        var currentName = "";
        $(".checkItemsCustom").each(function () {
            var newName = $(this).data("newname");
            var value = $(this).val();
            var dataName = $(this).data('name');

            var curName = dataName;

            if (typeof newName != "undefined" && newName != null && newName != "") curName = dataName;

            if ($(this).is(":checked")) {
                currentName += '<a data-name="' + curName + '" " class="item selectItem" href="javascript:;" >' + curName + '</a>';
            }
        });
        if (currentName == "") {
            currentName = '<a class="mainNode selectItems" href="javascript:;" >Ընտրել</a>';
        }
        $('.customSelectNamesItem').html(currentName);
        //=============================

    }

    // disable menus
    function disableMenus() {
        for (var i in _self.subControllers) {
            var curControl = _self.subControllers[i];

            // disable menu if it is not primary
            if (!curControl.Primary) {
                var curID = curControl.ID;
                $('#' + curID).attr('disabled', true);
                curControl.Lock = true;
            }
        }

        // disable controls
        var prevButton = $('.btn-add-page-prev');
        var passButton = $('.btn-add-page-pass');
        var nextButton = $('.btn-add-page-next');
        var previewButton = $('.btn-add-page-preview');

        prevButton.attr('disabled', true);
        passButton.attr('disabled', true);
        nextButton.attr('disabled', true);
        previewButton.attr('disabled', true);
    }

    // enable menus
    function enableMenus() {
        for (var i in _self.subControllers) {
            var curControl = _self.subControllers[i];

            // disable menu if it is not primary
            var curID = curControl.ID;
            $('#' + curID).attr('disabled', false);
            curControl.Lock = false;
        }
    }

    // switch sub Controller
    function switchSubController(Action, ID) {
        switch (Action) {
            case 'prev':
                if (_self.startControllerIndex > 0) {

                    var newIndex = _self.startControllerIndex - 1;
                    switchSubControllerWorker(newIndex, false);

                }
                break;
            case 'next':
                if (_self.startControllerIndex < Object.keys(_self.subControllers).length - 1) {

                    var newIndex = _self.startControllerIndex + 1;
                    switchSubControllerWorker(newIndex, false);

                }
                break;
            case 'pass':
                if (_self.startControllerIndex < Object.keys(_self.subControllers).length - 1) {

                    var newIndex = _self.startControllerIndex + 1;
                    switchSubControllerWorker(newIndex, false);

                }
                break;
            case 'goto':
                if (_self.startControllerIndex <= Object.keys(_self.subControllers).length - 1 && ID != null) {

                    var newIndex = ID;
                    switchSubControllerWorker(newIndex, true);

                }
                break;
        }
        //_core.getService('Loading').Service.disableBlur('loaderClass');

    }

    // helper method for switch sub controller
    function switchSubControllerWorker(newIndex, lockall) {

        // if it is not locked
        if (!_self.subControllers[newIndex].Lock || !lockall) {

            var currentController = _core.getController(_self.subControllers[_self.startControllerIndex].Name).Controller;

            // saving current working controller with callback
            currentController.save(function (res) {

                // if all data is saved
                if (res.status) {

                    if (typeof res.PrisonerID != 'undefined') {
                        _self.mode = 'Edit';
                        _self.PrisonerID = res.PrisonerID;
                        _self.ArchiveData = res.ArchiveData;
                    }

                    // call change tab status if in "Add" mode
                    if (_self.action == 'Add') {

                        // set tab status (must be called before index changed)
                        //changeTabStatus(_self.startControllerIndex, res.status);
                    }

                    // changing current action index
                    _self.startControllerIndex = newIndex;

                    // load subController
                    loadSubController(_self.startControllerIndex);
                }
                else {
                    // calling yes no widget
                    var YesOrNoWidget = _core.getWidget('YesOrNo').Widget;

                    // if it is not locked
                    if (!_self.subControllers[newIndex].Lock) {
                        // asking yes or no
                        YesOrNoWidget.Ask(function (res) {
                            if (res) {

                                // if skipped then always invalid
                                var status = false;

                                // set tab status
                                //changeTabStatus(_self.startControllerIndex, status);

                                // changing current action index
                                _self.startControllerIndex = newIndex;

                                // load subController
                                loadSubController(_self.startControllerIndex);
                            }
                            else {
                                changeActiveTab(_self.startControllerIndex);
                            }
                        });
                    }
                }
            })
        }
        else {
            changeActiveTab(_self.startControllerIndex);
        }
    }

    // load subController
    function loadSubController(index) {

        // check merge approve

        if (_self.mode != 'Add') {
            checkDepartmentApprove();
            enableMenus();
        }

        // change the url
        window.location.hash = _self.subControllers[_self.startControllerIndex].Name;

        // generating url
        var controllerName = _self.subControllers[index].Name;
        var url = '/' + controllerName + '/' + _self.mode;

        // loading content via post service
        var postService = _core.getService('Post').Service;

        // enable loader
        var partialViewHolder = $('#partialViewHolder');

        // empty content holder and adding loading
        partialViewHolder.empty();
        partialViewHolder.addClass('loading');

        // data to send to server
        var data = {};
        data.controllerName = controllerName;
        if (_self.mode == 'Add') data = null;
        else if (_self.mode != 'Add') {
            data.PrisonerID = _self.PrisonerID;
        }

        // changeing active tab
        changeActiveTab(_self.startControllerIndex);

        // loading content
        postService.postPartial(data, url, function (content) {
            if (content != null) {
                // appending content
                var partialViewHolder = $('#partialViewHolder');

                // empty content holder and write new one
                partialViewHolder.append(content);
                partialViewHolder.removeClass('loading');

                // getting controller and calling action
                var currentController = _core.getController(_self.subControllers[_self.startControllerIndex].Name).Controller;

                if (_self.mode == 'Add') {
                    
                    if (controllerName == "Transfers") {
                        currentController.Add(_self.ArchiveData);
                    }
                    else {
                        // callback is for going back and reiniting if person removed
                        if (_self.startControllerIndex == 0) currentController.Add(_self.PrisonerType);
                        else currentController.Add();
                    }

                    disableMenus();
                }
                else if (_self.mode != 'Add') {
                    if (data.controllerName == "Transfers") {
                        currentController.Edit(_self.PrisonerID, _self.ArchiveData);
                    }
                    else {
                        if (_self.startControllerIndex == 0) currentController.Edit(_self.PrisonerType, _self.PrisonerID);
                        else currentController.Edit(_self.PrisonerID, _self.PrisonerType);
                    }

                    // check if there is approve
                    checkMergeApprove();
                }

                // disable ation on view
                if (_self.action == 'Preview') {

                    // if is not open
                    if (!_self.subControllers[_self.startControllerIndex].isOpen) {

                        disableAllEvents("partialViewHolder");
                    }
                }
            }

        });

        // Toggle Control Buttons
        toggleFormButtons();
    }

    // changing active tab
    function changeActiveTab(newIndex) {
        var tabs = $('.subControllerListLi');
        var newTab = $('.subControllerListLi[data-id="' + newIndex + '"]');

        // remove all active tabs
        tabs.removeClass('active');

        // activate new tab
        newTab.addClass('active');
    }

    // hide or show merge approve exist div
    function checkMergeApprove() {
        var data = {};
        data.PrisonerID = _self.PrisonerID;
        var url = _self.checkMergeApproveURL;
        _core.getService('Post').Service.postJson(data, url, function (res) {
            if (typeof res.needApprove != "undefined" && !res.needApprove) {
                $('.mainMergeWarningClass').addClass("hidden");
            }
        })
    }

    // hide or show merge approve exist div
    function checkDepartmentApprove() {
        var data = {};
        data.PrisonerID = _self.PrisonerID;
        var url = _self.checkDepartMergeApproveURL;
        _core.getService('Post').Service.postJson(data, url, function (res) {
            if (res != null && res.EIds != null)
                markApproves(res.EIds)
        })
    }

    // mark approve
    function markApproves(list) {
        try {
        
            $('.subControllerListLi').removeClass('hasPending');

            for (var i in list) {
                var curItem = list[i];
                while (curItem.charAt(0) === '0')
                    curItem = curItem.substr(1);

                $(".pendingUnit" + curItem).addClass("hasPending");
            }
        }
        catch (e) {

        }
    }

    // changing tab status
    function changeTabStatus(newIndex, status) {
        var tabs = $('.subControllerListA');
        var curTab = $('.subControllerListA[data-id="' + newIndex + '"]');

        // clean current tab
        curTab.removeClass('valid');
        curTab.removeClass('invalid');

        // add valid or invalid class
        switch (status) {
            case true:
                curTab.addClass('valid');
                break;
            case false:
                curTab.addClass('invalid');
                break;
        }
    }

    // call partial view
    function getPartialView(index) {

    }

    // design codes
    function designCodes() {

        // Arsen's Code
        var formsWrapTabsLeftHeight = $(".formsWrapTabsLeft").height();
        if ($(".formsWrapTabsRight").length) {
            $(".formsWrapTabsRight").css("min-height", formsWrapTabsLeftHeight);
            $(".formsWrapTabsRight .tab-content").css("min-height", formsWrapTabsLeftHeight - 136);
        }


        var headerNames = $(".headerNames");
        var headerNamesDropWrap = $(".headerNamesDropRight");
        var headerNamesDrop = $("#headerNamesDrop");

        headerNamesDrop.click(function (event) {
            headerNamesDropWrap.toggleClass("open");
        });

        var openClass = $(".open");

        $("body").on("click", function (e) {
            if (!headerNames.is(e.target) && headerNames.has(e.target).length === 0 && openClass.has(e.target).length === 0) {
                headerNamesDropWrap.removeClass("open");
            }
        });

        var formsWrapTabsLeftHeight = $(".formsWrapTabsLeft").height();
        if ($(".formsWrapTabsRight").length) {
            $(".formsWrapTabsRight").css("min-height", formsWrapTabsLeftHeight);
            $(".formsWrapTabsRight .tab-content").css("min-height", formsWrapTabsLeftHeight - 136);
        }
    }

    // important waning function
    function importantWarning() {
        alert('Please Be carefull there is seiour error on your url');
        location.reload();
        return false;
    }

}
/************************/


// creating class instance
var ConvictsController = new ConvictsControllerClass();

// creating object
var ConvictsControllerObject = {
    // important
    Name: 'Convicts',

    Controller: ConvictsController
}

// registering controller object
_core.addController(ConvictsControllerObject);