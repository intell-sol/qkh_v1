﻿// Data_Initial controller

/*****Main Function******/
var _Data_Initial_ControllerClass = function () {
    var _self = this;
    _self.mode = null;
    _self.callback = null;
    _self.saveAddUrl = '/_Data_Initial_/AddData';
    _self.saveEditUrl = '/_Data_Initial_/EditData';
    _self.removeDocIdentityURL = '/_Data_Initial_/remDocIdentity';
    _self.addDocIdentityURL = '/_Data_Initial_/addDocIdentity';
    _self.idDocTableRowDrawURL = "/_Popup_/Draw_DocIdentity_Table_Row";
    _self.idDocTableRowGetAndDrawURL = "/_Popup_/Get_DocIdentity_Table_Row";
    _self.removePersonURL = "/_Data_Initial_/removePerson";
    _self.tempDocIdentities = new Array();
    _self.readyStatus = {};
    _self.readyStatus.policeDataReady = false;
    _self.readyStatus.docIdentityDataReady = false;
    _self.finalData = {};
    _self.finalData.isCustomPhoto = null;
    _self.finalData.DocNumber = null;
    _self.finalData.Type = false;
    _self.finalData.Person = null;
    _self.finalData.DocIdentities = new Array();
    _self.finalFormData = new FormData();
    _self.menualSearch = false;

    // for edit
    _self.PrisonerID = null;

    _self.init = function (Action) {
        console.log('_Data_Initial_ Controller Inited with action', Action);

        // clean params
        _self.tempDocIdentities = new Array();
        _self.readyStatus = {};
        _self.readyStatus.policeDataReady = false;
        _self.readyStatus.docIdentityDataReady = false;
        _self.finalData = {};
        _self.finalData.isCustomPhoto = null;
        _self.finalData.DocNumber = null;
        _self.finalData.Type = false;
        _self.finalData.Person = null;
        _self.finalData.DocIdentities = new Array();
        _self.finalFormData = new FormData();
        _self.menualSearch = false;

        // calling function

        _self.mode = Action;
    }

    // add action
    _self.Add = function(PrisonerType, callback) {

        console.log('_Data_Initial_ add called');

        _self.init('Add');

        _self.mode = 'Add';

        _self.PrisonerType = PrisonerType;

        // Js Bindings
        bindButtons();
    }

    _self.Edit = function (PrisonerType, PrisonerID) {

        console.log('_Data_Initial_ edit called');

        _self.init('Edit');

        _self.mode = 'Edit';

        _self.PrisonerID = PrisonerID;
        _self.PrisonerType = PrisonerType;

        _self.readyStatus.policeDataReady = true;
        _self.readyStatus.docIdentityDataReady = true;

        // Js Bindings
        bindButtons();
        bindIdDocTableRows();
    }

    // save work and return callback
    _self.save = function (callback) {
        console.log('_Data_Initial_ data saved');
        
        // save callback
        _self.callback = callback;

        // if all is ready then save to server
        if (_self.mode == 'Add') {
            if (checkValidate()) {

                // preparedata
                var data = prepareData();
                // logit
                //for (var pair of data.entries()) {
                //    console.log(pair[0] + ', ' + pair[1]);
                //}

                saveData(data, function (res) {
                    if (res.status) {
                        _self.PrisonerID = res.PrisonerID;

                        // after save events
                        savePrisonerEvents();
                    }

                    // callback
                    _self.callback(res);
                    _self.callback = null;
                });
            }
            else {
                // callback
                var res = {};
                res.status = false;
                _self.callback(res);
                _self.callback = null;
            }
        }
        else {
            var res = {};
            res.status = true;
            // callback
            _self.callback(res);
            _self.callback = null;
        }
    }

    // save prisoner events
    function savePrisonerEvents() {
        $("#ArchiveData")[0].selectize.disable();
    }

    // js bindings
    function bindButtons() {

        var findPersonBtn = $('#findpersoninpolice');
        var addDocIdentify = $('#adddocidentify');
        var addCustomPhoto = $('#add-custom-user-image');
        var removePersonBtn = $('#removeperson');

        findPersonBtn.unbind();
        findPersonBtn.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');
            
            // call find persion widget
            var findPersionWidget = _core.getWidget('FindPerson').Widget;

            //run
            findPersionWidget.Show(function (res) {
                matchPersonData(res.Person, res.menualSearch);
                _self.menualSearch = res.menualSearch;
            });
        });

        addDocIdentify.unbind();
        addDocIdentify.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call find persion widget
            var docIdentidyWidget = _core.getWidget('DocIdentity').Widget;

            // run
            docIdentidyWidget.Show(function (res) {
                if (res.status) {
                    _self.readyStatus.docIdentityDataReady = true;
                    // add now for edit
                    if (_self.mode == 'Edit') addDocidentity(res.data);
                    _self.tempDocIdentities.push(res.data);
                }
                if (_self.readyStatus.docIdentityDataReady && _self.mode == 'Add') drawPartialTable();
            });
        });

        addCustomPhoto.unbind();
        addCustomPhoto.bind('change', function () {

            // appending in finalFormData
            _self.finalFormData.delete('curstomImageFile');
            _self.finalFormData.append('curstomImageFile', $('#add-custom-user-image')[0].files[0], $('#add-custom-user-image').val());

            // display image before upload
            readURL(this);
        });

        // remove person part
        removePersonBtn.unbind().click(function (e) { e.preventDefault(); removeSelectedPerson()});

        // selectize
        $(".selectize").each(function () {
            $(this).selectize({
                create: false
            });
        });

        $(".selectize-tags").each(function () {
            $(this).selectize({
                create: true
            });
        });

        $(".scrollTable").each(function () {
            $(this).tableScroll({
                flush: false,
                height: 136
            });
        });
    }

    // bind identification document table
    function bindIdDocTableRows() {
        var tableRow = $('.idDocTableRow');
        var viewDocTableRow = $('.viewDocTableRow');

        tableRow.unbind();
        tableRow.bind('click', function (e) {
            e.preventDefault();

            // get id
            var id = $(this).data('id');

            // remove from array
            removeDocIdentity(id, null);

            // redraw table
            if (_self.mode == 'Add') drawPartialTable();
        });

        viewDocTableRow.unbind();
        viewDocTableRow.bind('click', function (e)
        {
            e.preventDefault();

            // get id
            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            _core.getWidget("DocIdentity").Widget.View(id);
        });
    }

    // preview & update an image before it is uploaded
    function readURL(input) {
        if (input.files && input.files[0]) {

            // custom photo
            _self.finalData.isCustomPhoto = true;

            var reader = new FileReader();

            reader.onload = function (e) {
                $('#person-photolink').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);

            if (_self.mode == 'Edit') updateAvatar();
        }
    }

    // toggle user image part
    function toggleUserImagePart(user) {

        // cant upload photo if there is photo
        if (user.photoExist) {
            _self.finalData.isCustomPhoto = false;
        }
        else {
            _self.finalData.isCustomPhoto = true;
        }
    }

    // match user params to fields
    function matchUserFields(user, menual) {
        var registerationData = user.Registration_address

        var livingData = user.Living_place;

        // gender part
        if (typeof user.Gender == 'undefined') {
            if (typeof user.SexLibItem_ID != 'undefined') {
                // SexLibItemIdCodes
                if (parseInt(user.SexLibItem_ID) == 154) user.Gender = 'M';
                else if (parseInt(user.SexLibItem_ID) == 153) user.Gender = 'F';
            }
            if (typeof user.Gender != 'undefined')
                $('#personal-sex')[0].selectize.setValue(user.Gender);
        }
        else
            $('#personal-sex')[0].selectize.setValue(user.Gender);

        // nationality
        if (typeof user.Nationality == 'undefined')
            $('#personal-nationality')[0].selectize.setValue(parseInt(user.NationalityID));
        else
            $('#personal-nationality')[0].selectize.setValue(user.Nationality);
        $('#personal-name-1').val(user.FirstName);
        $('#personal-name-2').val(user.LastName);
        $('#personal-name-3').val(user.MiddleName);
        // birthdate part
        if (typeof user.BirthDate == 'undefined')
            $('#personal-bd').val(user.Birthday);
        else
            $('#personal-bd').val(user.BirthDate);
        // social card part
        if (typeof user.SocialCardNumber == 'undefined')
            $('#personal-id').val(user.Personal_ID);
        else
            $('#personal-id').val(user.SocialCardNumber);

        //$('#user-person-nationality').val(user.Nationality);
        $('#personal-reg-address').val(registerationData);
        $('#personal-living-address').val(livingData);
        //$('#user-passport-number').val(user.PassportNumber);
        //$('#user-passport-date').val(user.PassportIssuanceDate);
        //$('#user-passport-issuer').val(user.PassportDepartment);
        $('#person-photolink').attr('src', user.PhotoLink);

    }

    // match full person data
    function matchPersonData(data, menual) {
        _self.readyStatus.policeDataReady = true;

        //if (menual) {
            _self.finalData.Person = data;
        //}
        //else {
        //    _self.finalData.DocNumber = data.SocialCardNumber;
        //    _self.finalData.Type = false;
        //}

        // match user data
        matchUserFields(data, menual);

        // toggle user image
        toggleUserImagePart(data);

        // enable next button
        personSelectEvents(true);

        // match docIdentities
        var docIdentities = data.IdentificationDocuments;
        if (docIdentities.length != 0) {
            removeDocIdentity(null, true);
        }

        // clean docIdentities
        _self.tempDocIdentities = [];
        
        for (var i in docIdentities) {
            _self.readyStatus.docIdentityDataReady = true;
            // add now for edit
            if (_self.mode == 'Edit') addDocidentity(docIdentities[i]);
            _self.tempDocIdentities.push(docIdentities[i]);
        }
        if (_self.readyStatus.docIdentityDataReady) drawPartialTable();
    }

    // get partial table row and drow
    function drawPartialTable() {

        var data = null;
        var url = null;

        // post service
        var postService = _core.getService('Post').Service

        if (_self.mode == 'Add') {
            data = _self.tempDocIdentities
            url = _self.idDocTableRowDrawURL;
        }
        else {
            data = {};
            data.PrisonerID = _self.PrisonerID;
            url = _self.idDocTableRowGetAndDrawURL;
        }

        _core.getService('Loading').Service.enableBlur('docIdentityLoader');

        postService.postPartial(data, url, function (curHtml) {
        
            // draw html
            var table = $('#docidentitytablebody');
            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('docIdentityLoader');

            // Bindings
            bindIdDocTableRows();
        });
    }

    // person is selected or not events
    function personSelectEvents(isSelected) {

        // toggle next button
        $('.btn-add-page-next').attr('disabled', !isSelected);
    }

    // remove from identification document
    function removeDocIdentity(id, type) {

        // remove by id
        if (id != null) {

            // remove now
            if (_self.mode == 'Edit') remDocidentity(id);

            for (var i in _self.tempDocIdentities) {
                if (parseInt(_self.tempDocIdentities[i].ID) == parseInt(id)) {
                    _self.tempDocIdentities.splice(i, 1);
                }
            }
        }
        // remove by type
        else if (type != null) {
            for (var i in _self.tempDocIdentities) {
                if (_self.tempDocIdentities[i].Type == type) {
                    _self.tempDocIdentities.splice(i, 1);
                }
            }
        }
    }

    // remove selected person
    function removeSelectedPerson() {
        var data = {};
        data.ID = _self.PrisonerID;

        _core.getService('Loading').Service.enableBlur('loaderClass');

        // yes or no widget
        _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {
            if (res) {

                // postService
                _core.getService('Post').Service.postJson(data, _self.removePersonURL, function (res) {
                    if (res != null && res.status) {

                        // action after removing person
                        removePersonAfterAction();
                    }
                })
            }
        })
    }

    // action after removing person
    function removePersonAfterAction() {
        var controllerName = 'Convicts';
        if (_self.PrisonerType == 3) controllerName = 'Prisoners' 
        window.location.href = '/' + controllerName + '/Add';
    }

    // prepare data for save
    function prepareData() {

        // clean DocIdenities
        _self.finalData.Person = _self.finalData.Person;

        // Prisoner Type
        _self.finalData.PrisonerType = _self.PrisonerType;

        //for (var i in _self.tempDocIdentities) {
        //    var tempDoc = _self.tempDocIdentities[i];
        //    _self.finalData.DocIdentities.push(tempDoc);
        //}

        // append fileData to formData
        for (var i in _self.finalData) {
            var tempData = _self.finalData[i];
            if ($.isArray(tempData) || i == "Person") {
                tempData = JSON.stringify(tempData);
            }
            _self.finalFormData.append(i, tempData);
        }

        // append archive status to data
        var ArchiveData = $("#ArchiveData")[0].selectize.getValue();
        ArchiveData = ArchiveData == "true";
        _self.finalFormData.append('ArchiveData', ArchiveData);
        
        return _self.finalFormData;
    }

    // save data to server
    function saveData(data, callback) {

        var url = _self.mode == 'Add' ? _self.saveAddUrl : _self.saveEditURL;

        var PostService = _core.getService('Post').Service;
        _core.getService('Loading').Service.enableBlur('loaderClass');
        PostService.postFormData(data, url, function (res) {
            _core.getService('Loading').Service.disableBlur('loaderClass');
            callback(res);
        });
    }

    // add doc identity now for edit
    function addDocidentity(data) {

        // adding prisoner id
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('docIdentityLoader');

        postService.postJson(data, _self.addDocIdentityURL, function (res) {
            _core.getService('Loading').Service.disableBlur('docIdentityLoader');
                if (res == null || !res.status) alert('error occured on removing document');
                else drawPartialTable();
        });
    }

    // rem doc identity now for edit
    function remDocidentity(id) {

        var data = {};
        data.ID = id;

        _core.getService('Loading').Service.enableBlur('loaderClass');

        // yes or no widget
        _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {
            if (res) {

                // post service
                var postService = _core.getService('Post').Service;

                postService.postJson(data, _self.removeDocIdentityURL, function (res) {
                    if (!res.status) alert('error occured on removing document');
                    else drawPartialTable();
                });
            }
        });
    }

    // check if all is ready
    function checkValidate() {
        return status = (_self.readyStatus.docIdentityDataReady && _self.readyStatus.policeDataReady);
        //return status = (_self.readyStatus.policeDataReady);
    }
}
/************************/


// creating class instance
var _Data_Initial_Controller = new _Data_Initial_ControllerClass();

// creating object
var _Data_Initial_ControllerObject = {
    // important
    Name: '_Data_Initial_',

    Controller: _Data_Initial_Controller
}

// registering controller object
_core.addController(_Data_Initial_ControllerObject);