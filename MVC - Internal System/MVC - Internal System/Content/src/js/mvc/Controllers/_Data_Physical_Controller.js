﻿// _Data_Physical_ controller

/*****Main Function******/
var _Data_Physical_ControllerClass = function () {
    var _self = this;

    _self.addInvalidsURL = '/_Data_Physical_/AddInvalid';
    _self.getInvalidsTableRowURL = '/_Popup_/Get_InvalidTableRows';
    _self.getFileTableRowURL = '/_Popup_/Get_AddFileTableRow';
    _self.getMainDataURL = '/_Data_Physical_/GetData';
    _self.addLeaningURL = "/_Data_Physical_/AddLeaning";
    _self.SaveHeightWeightURL = "/_Data_Physical_/SaveHeightWeight";
    _self.getHeightWeightTableRowURL = "/_Popup_/Get_HeightWeightTableRows";
    _self.getLeaningsTableRowURL = "/_Popup_/Get_LeaningTableRows";
    _self.saveContentURL = "/_Data_Physical_/SaveContent";
    _self.remInvalidURL = "/_Data_Physical_/RemInvalid";
    _self.remLeaningURL = "/_Data_Physical_/RemLeaning";
    _self.RemHWURL = "/_Data_Physical_/remHeightWeight";
    _self.finalFormData = new FormData();

    _self.callback = null;
    _self.PrisonerID = null;
    _self.PhysicalDataID = null;
    _self.getItems = null;
    _self.checkItems = null;
    _self.isReady = true;

    _self.mode = "Add" // initial mode


    _self.init = function (Action) {
        console.log('_Data_Physical_ Controller Inited with action', Action);

        _self.finalFormData = new FormData();

        _self.mode = Action;

        // Js Bindings
        bindButtons();

        // binds invalids
        bindInvalidsEvents();

        // binds leanings
        bindLeaningsEvents();

        // bind file events
        bindFilesEvents();
    }

    // add action
    _self.Add = function () {
        console.log('_Data_Physical_ add called');

        _self.init('Add');

    }

    // edit action
    _self.Edit = function (PrisonerID) {

        console.log('_Data_Physical_ edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;

        _self.PhysicalDataID = $('#physicaldataid').data('physicalid');

    }

    _self.save = function (callback) {
        console.log('_Data_Primary_ data saved');

        var res = {};

        //// check validate
        //if (checkValidate()) {

        //    //save content
        //    saveContent(function (res) {

        //        // make ready if saved for later not saving in vain
        //        if (res != null) _self.isReady = res.status;

        //        // return callback
        //        callback(res);
        //    });
        //}
        //else {
            res.status = true;

            // return false
            callback(res);
        //}
    }

    // js bindings
    function bindButtons() {

        _self.getItems = $('.getItems');
        _self.checkItems = $('.checkValidate');
        var addHeightWeight = $('#addheightweight');
        var addFileBtn = $('#addfileMain');
        var addInvalidsBtn = $('#invalidsmodalbtn');
        var addLeaningBtn = $('#addleaningbtn');
        var savePhysicalBtn = $('#savePhysicalDataButton');
        
        // save physical data
        savePhysicalBtn.unbind().click(function (e) {
            e.preventDefault();

            // check validate
            if (checkValidate()) {

                //save content
                saveContent(function (res) {

                    // make ready if saved for later not saving in vain
                    if (res != null) {
                        _self.isReady = res.status;
                        if (typeof res.needApprove != "undefined" && res.needApprove) {
                            $('.physicalMergeWarningClass').removeClass("hidden");
                            $('.mainMergeWarningClass').removeClass("hidden");
                        }
                    }
                    // disable button
                    savePhysicalBtn.attr('disabled', res.status);
                });
            }
        });

        addFileBtn.unbind();
        addFileBtn.bind('click', function () {

            var data = {};
            data.TypeID = 4;
            data.PrisonerID = _self.PrisonerID;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });

        addInvalidsBtn.unbind();
        addInvalidsBtn.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('Invalids').Widget;

            // run
            addFileWidget.Show('Add', function (data) {
                AddInvalid(data);
            });
        })

        addLeaningBtn.unbind();
        addLeaningBtn.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('Leanings').Widget;

            // run
            addFileWidget.Show('Add', function (data) {
                AddLeaning(data);
            });
        });

        addHeightWeight.unbind();
        addHeightWeight.bind('click', function () {

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call add file widget (image type)
            var addHeightWeightWidget = _core.getWidget('HeightWeight').Widget;

            // run
            addHeightWeightWidget.Show('Add', _self.PrisonerID, function (data) {
                if (data != null) {

                    var saveData = {};
                    saveData['NewList'] = data;

                    saveData['PrisonerID'] = _self.PrisonerID;

                    // add HeightWeightEntity to Database
                    SaveHeightWeaight(saveData);
                }
                else {
                    console.error('wtf happened');
                }
            });
        });

        // check validate on keypress and change
        _self.checkItems.unbind().bind('change keyup', function () {

            // check validate
            checkValidate();
        })

        // selectize
        $(".selectizeMain").each(function () {
            $(this).selectize({
                create: false
            });
        });

    }

    // saveing content
    function saveContent(callback) {

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            // clean FormData
            _self.finalFormData.delete(name);

            // appending to FormData
            _self.finalFormData.append(name, $(this).val());
        });

        // appending PrisonerID
        _self.finalFormData.append('PrisonerID', _self.PrisonerID);

        _core.getService('Loading').Service.enableBlur('loaderClass');

        // post service
        var postService = _core.getService('Post').Service

        // saving content
        postService.postFormData(_self.finalFormData, _self.saveContentURL, function (res) {

            _core.getService('Loading').Service.disableBlur('loaderClass');

            if (res == null) {
                console.error('serious error on saveing physical data content');
                var resp = {};
                resp.status = false;
                callback(resp);
            }
            else {
                callback(res);
            }
        })
    }

    // check content
    function checkValidate() {

        var status = true;

        // if null return false
        if (_self.checkItems == null) return false;

        _self.checkItems.each(function () {
            if ($(this).val() == '' || $(this).val() == null) {
                var curName = $(this).attr('name');
                if (curName == 'Description' || curName == 'Note' || curName == 'ExternalDesc') $(this).val(' ');
                else if (curName != 'BloodTypeLibItem_ID') status = false;
            }
        });

        // disabled button
        $('#savePhysicalDataButton').attr('disabled', !status);

        return status;
    }

    // bind files events
    function bindFilesEvents() {

        var removeBtn = $('.removeFileButton');
        var editBtn = $('.editFileButton');
        var viewBtn = $('.viewFileButton');

        // remove file actin handle
        removeBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Remove(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            });
        });
        viewBtn.unbind().click(function ()
        {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.View(data, function (res)
            {
                
            })
        });
        // edit file action handle
        editBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Edit(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
    }

    // invalids row bindings
    function bindInvalidsEvents() {

        var tableRows = $('.removeinvalidsrow');
        var editRows = $('.editinvalidsrow');
        var viewBtn = $('.viewinvalidsrow');
        // remove
        tableRows.unbind();
        tableRows.bind('click', function () {

            var id = $(this).data('id');

            var data = {};
            data['ID'] = id;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {
                if (res) {

                    // postService
                    var postService = _core.getService('Post').Service;

                    _core.getService('Loading').Service.enableBlur('invalidLoader');

                    postService.postJson(data, _self.remInvalidURL, function (res) {

                    _core.getService('Loading').Service.disableBlur('invalidLoader');

                        if (res != null && res.status) {
                            getInvalidList();
                        }
                        else alert('serious error on remivng invalid row');
                    });
                }
                _core.getService('Loading').Service.disableBlur('loaderClass');

            })
        });
        //View
        viewBtn.unbind().click(function ()
        {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('Invalids').Widget;

            // run
            addArrsetWidget.View(id, _self.PrisonerID, function (res)
            {


            });
        });
        // edit
        editRows.unbind().click(function () {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('Invalids').Widget;

            // run
            addArrsetWidget.Edit(id, _self.PrisonerID, function (res) {

                if (res != null && res.status) {
                    // get arrest list
                    getInvalidList();
                }
                _core.getService('Loading').Service.disableBlur('loaderClass');

            });
        });
    }
    
    // leanings row bindings
    function bindLeaningsEvents() {

        var tableRows = $('.removeleaningrow');
        var editRows = $('.editleaningrow')
        var viewBtn = $('.viewleaningrow')
        // remove
        tableRows.unbind();
        tableRows.bind('click', function () {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            var data = {};
            data['ID'] = id;

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {
                if (res) {

                    // postService
                    var postService = _core.getService('Post').Service;

                    _core.getService('Loading').Service.enableBlur('leaningLoader');

                    postService.postJson(data, _self.remLeaningURL, function (res) {

                        _core.getService('Loading').Service.disableBlur('leaningLoader');

                        if (res != null && res.status) {
                            getLeaningList();
                        }
                        else alert('serious error on remivng leaning row');

                        _core.getService('Loading').Service.disableBlur('loaderClass');

                    });
                }
            });
        });

        viewBtn.unbind().click(function ()
        {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('Leanings').Widget;

            // run
            addArrsetWidget.View(id, function (res)
            {


            });
        });
        // edit
        editRows.unbind().click(function () {

            var id = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // call arrest data widged
            var addArrsetWidget = _core.getWidget('Leanings').Widget;

            // run
            addArrsetWidget.Edit(id, function (res) {

                if (res != null && res.status) {
                    // get arrest list
                    getLeaningList();
                }

                _core.getService('Loading').Service.disableBlur('loaderClass');

            });
        });
    }

    // Add invalid to database
    function AddInvalid(data) {

        // appending prisoner id to formdata
        data.delete('PrisoneID');
        data.append('PrisonerID', _self.PrisonerID);

        //// log it
        //for (var pair of data.entries()) {
        //    console.log(pair[0] + ', ' + pair[1]);
        //}

        // post service

        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('invalidLoader');

        postService.postFormData(data, _self.addInvalidsURL, function (res) {
            _core.getService('Loading').Service.disableBlur('invalidLoader');
            if (res.status) {
                getInvalidList();
            }
            else alert('serious eror on adding invalid');
        })
    }

    // Add lenaing to database
    function AddLeaning(data) {

        // appending prisoner id to formdata
        data.delete('PrisoneID');
        data.append('PrisonerID', _self.PrisonerID);

        //// log it
        //for (var pair of data.entries()) {
        //    console.log(pair[0] + ', ' + pair[1]);
        //}

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('leaningLoader');

        postService.postFormData(data, _self.addLeaningURL, function (res) {
            _core.getService('Loading').Service.disableBlur('leaningLoader');
            if (res.status) {
                getLeaningList();
            }
            else alert('serious eror on adding leaning');
        })
    }

    // Save height weight
    function SaveHeightWeaight(sendData) {

        var lastHeight = '';
        var lastWeight = '';

        var data = [];
        var List = sendData['NewList'];
        sendData['List'] = [];
        for (var i in List) {
            var tempData = {};
            tempData['ID'] = List[i]['ID'];
            tempData['Height'] = List[i]['Height'];
            tempData['Weight'] = List[i]['Weight'];
            tempData['PhysicalDataID'] = _self.PhysicalDataID;
            tempData['PrisonerID'] = _self.PrisonerID;

            lastHeight = List[i]['Height'];
            lastWeight = List[i]['Weight'];

            sendData['List'].push(tempData);
        }

        // post service
        var postService = _core.getService('Post').Service;

        postService.postJson(sendData, _self.SaveHeightWeightURL, function (res) {
            if (res != null && res.status) {
                if (res.needApprove) {
                    $('.physicalMergeWarningClass').removeClass("hidden");
                    $('.mainMergeWarningClass').removeClass("hidden");
                }
                $('#physical-height').val(lastHeight);
                $('#physical-weight').val(lastWeight);
            }
            else alert('serious eror on saving height weight');
        })
    }

    // get image file list table rows
    function getFileList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.TypeID = type;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('fileLoader');

        postService.postPartial(data, _self.getFileTableRowURL, function (curHtml) {

            var table = null;

            table = $('#addPysicalFilesTableRow');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('fileLoader');

            // bind file Events
            bindFilesEvents();
        });
    }

    // get image file list table rows
    function getInvalidList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('invalidLoader');

        postService.postPartial(data, _self.getInvalidsTableRowURL, function (curHtml) {

            var table = null;
            table = $('#invalideTableBody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('invalidLoader');

            // binds
            bindInvalidsEvents();
        });
    }

    // get image file list table rows
    function getLeaningList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService('Loading').Service.enableBlur('leaningLoader');

        postService.postPartial(data, _self.getLeaningsTableRowURL, function (curHtml) {

            var table = null;
            table = $('#leaningTableBody');

            table.empty();
            table.append(curHtml);

            _core.getService('Loading').Service.disableBlur('leaningLoader');

            // bind leaning events
            bindLeaningsEvents();
        });
    }

    // bind table row buttons
    function bindTableRow() {

        var remHW = $('.remHW');

        remHW.unbind();
        remHW.bind('click', function () {

            // id
            var id = parseInt($(this).data('id'));

            remHeightWeight(id);
        });
    }


}
/************************/

// creating class instance
var _Data_Physical_Controller = new _Data_Physical_ControllerClass();

// creating object
var _Data_Physical_ControllerObject = {
    // important
    Name: '_Data_Physical_',

    Controller: _Data_Physical_Controller
}

// registering controller object
_core.addController(_Data_Physical_ControllerObject);