﻿// Dashboard controller

/*****Main Function******/
var DashboardControllerClass = function () {
    var _self = this;

    _self.selectOrgUnitURL = '/Base/SelectOrgUnit';
    _self.PrisonerID = null;

    _self.searchPrisonerURL = "/Convicts/SearchPrisoner";

    _self.getFileTableRowURL = '/_Popup_/Get_AddFileTableRow';
    _self.getMainTableRowURL = '/_Popup_/Get_PackageTableRows'; 
    _self.getDepartureTableRowURL = '/Dashboard/Get_DeparturesTableRow';
    _self.getPackageTableRowURL = '/Dashboard/Get_PackageTableRow';
    _self.getTransferTableRowURL = '/Dashboard/Get_TransferTableRow';
    _self.filterURL = '/Dashboard/Get_PackageTableRow';

    _self.mode = "Add" // initial mode

    _self.init = function (Action) {
        console.log('Controller Inited with action', Action);

        _self.mode = Action;

        _self.callback = null;
        _self.PrisonerID = null;

        // Js Bindings
        bindButtons();

        // items table row events
        bindMainTableRowEvents();
        bindDepartureTableRowEvents();
        bindPackageTableRowEvents();
        bindTransferTableRowEvents();

        // bind file events
        bindFilesEvents();
    }

    // index action
    _self.Index = function () {
        console.log('add called');

        _self.init('Index');

        // bind search events
        bindSearchEvents();
    }

    // add action
    _self.Add = function () {
        console.log('add called');

        _self.init('Add');
    }

    // edit action
    _self.Edit = function (PrisonerID) {

        console.log('edit called');

        _self.init('Edit');

        // prisoner id
        _self.PrisonerID = PrisonerID;
    }

    _self.save = function (callback) {
        console.log('data saved');

        var status = true;
        var res = {};
        res.status = status;
        // callback
        callback(res);
    }

    // js bindings
    function bindButtons() {

        var addMainBtn = $('#addMainBtn');
        var addFileBtn = $('#addfileBtn');

        addMainBtn.unbind();
        addMainBtn.bind('click', function () {

            var addMainWidget = _core.getWidget('Dashboard').Widget;

            // run
            addMainWidget.Add(_self.PrisonerID, function (res) {
                if (res != null) {

                    // get penalty list
                    getMainTableList();
                }
            });
        });

        addFileBtn.unbind();
        addFileBtn.bind('click', function () {

            var data = {};
            data.TypeID = 29;
            data.PrisonerID = _self.PrisonerID;

            // call add file widget (image type)
            var addFileWidget = _core.getWidget('AddFile').Widget;

            // run
            addFileWidget.Add(data, function (res) {
                getFileList(data.TypeID);
            });
        });
    }

    // bind pagination events
   


    // bind search events
    function bindSearchEvents() {

        var acceptBtn = $('#acceptFilterBtn');
        var TypeLibItemID = $('#TypeLibItemID');
        var PackageLibItemID = $('#PackageLibItemID');
        var orgunitlist = $('#orgunitidlist');
        var dates = $('.FilterDates');

        acceptBtn.unbind().click(function (e) {
            e.preventDefault();

            var finalData = {};
            $("#filterPersonForm").serializeArray().map(function (x) { finalData[x.name] = x.value; });
            finalData['TypeLibItemID'] = TypeLibItemID.val();
            finalData['PackageLibItemID'] = PackageLibItemID.val();

            // filter list
            filterList(finalData);
        });

        $('.selectizeFilterSearch').selectize({
            valueField: 'ID',
            labelField: 'value',
            searchField: 'value',
            create: false,
            render: {
                item: function (item, escape) {
                    console.log(item);
                    return '<div>' +
                        item.Person.FirstName +
                    '</div>';
                },
                option: function (item, escape) {
                    console.log(item);
                    return '<div>' +
                        item.Person.FirstName +
                    '</div>';
                }
            },
            score: function (search) {
                var score = this.getScoreFunction(search);
                return function (item) {
                    return score(item) * (1 + Math.min(item.watchers / 100, 1));
                };
            },
            load: function (query, callback) {
                if (!query.length) return callback();

                var data = {};
                data.query = query;

                var url = _self.searchPrisonerURL;

                _core.getService('Post').Service.postJson(data, url, function (res) {
                    console.log(res);
                    if (res != null && res.status) {
                        callback(res.prisoners.slice(0, 10));
                    }
                })
            }
        });

        $(".selectizeFilter").each(function () {
            $(this).selectize({
                create: false
                //plugins: {
                //    'no-delete': {}
                //},
            });
        });

        $(".selectizeFilterArchive").each(function () {
            $(this).selectize({
                create: false,
                plugins: {
                    'no-delete': {}
                },
            });
        });

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
        });

    }

    // bind penalty table rows events
    function bindMainTableRowEvents() {
        var removeRows = $('.removeMainRow');
        var editRows = $('.editMainRow');
        var completeRows = $('.completeMainRow');

        // remove row      
    


        // complete row
        completeRows.unbind();
        completeRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('Packages').Widget;

                    // run
                    addMainWidget.Complete(id, function (res) {
                        if (res != null) {

                            // get penalty list
                            getMainTableList();
                        }
                    });
                }
            })
        });
    }
    function bindPackageTableRowEvents() {

        var removeRows = $('.removePackageRow');
        var editRows = $('.editPackageRow');
        var completeRows = $('.completePackageRow');
        var declineRows = $('.declinePackageRow');
        var approveRows = $('.approvePackageRow');


        // remove row
        removeRows.unbind();
        removeRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('Packages').Widget;

                    // run
                    addMainWidget.Remove(id, function (res) {
                        if (res != null) {

                            // get penalty list
                            getPackageTableList();
                        }
                    });
                }
            })
        });

        // edit row
        editRows.unbind();
        editRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            var addMainWidget = _core.getWidget('Packages').Widget;

            // run
            addMainWidget.Edit(id, function (res) {

                if (res != null) {

                    // get penalty list
                    getPackageTableList();
                }
            });
        });

        // decline row
        declineRows.unbind();
        declineRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(3, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('Packages').Widget;

                    // run
                    addMainWidget.Decline(id, function (res) {
                        if (res != null) {

                            // get penalty list
                            getPackageTableList();
                        }
                    });
                }
            })
        });

        // decline row
        approveRows.unbind();
        approveRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(2, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('Packages').Widget;

                    // run
                    addMainWidget.Approve(id, function (res) {
                        if (res != null) {

                            // get penalty list
                            getPackageTableList();
                        }
                    });
                }
            })
        });
    }

    function bindTransferTableRowEvents() {

        var completeRows = $('.completeTransferRow');
        var removeRows = $('.removeTransferRow');
        var editRows = $('.editTransferRow');
        var declineRows = $('.declineTransferRow');
        var approveRows = $('.approveTransferRow');

        // remove row
        removeRows.unbind();
        removeRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('Transfers').Widget;

                    // run
                    addMainWidget.Remove(id, function (res) {
                        if (res != null) {

                            // get penalty list
                            getTransferTableList();
                        }
                    });
                }
            })
        });

        // edit row
        editRows.unbind();
        editRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            var addMainWidget = _core.getWidget('Transfers').Widget;

            //run
            addMainWidget.Edit(id, function (res) {

                if (res != null) {

                    //get main table list
                    getTransferTableList();
                }
            });
        });
        // remove row      
        // complete row
        completeRows.unbind();
        completeRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');
            console.log(id);

            // yes or no widget
            _core.getWidget('CompleteDate').Widget.Show(function (res) {

                if (res) {
                    console.log(id);

                    var addMainWidget = _core.getWidget('Transfers').Widget;

                    // run
                    addMainWidget.Complete(id, res['EndDate'], function (res) {
                        if (res != null) {

                            // get penalty list
                            getTransferTableList();
                        }
                    });
                }
            })
        });
        // decline row
        declineRows.unbind();
        declineRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(3, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('Transfers').Widget;

                    // run
                    addMainWidget.Decline(id, function (res) {
                        if (res != null) {

                            // get penalty list
                            getTransferTableList();
                        }
                    });
                }
            })
        });

        // decline row
        approveRows.unbind();
        approveRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(2, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('Transfers').Widget;

                    // run
                    addMainWidget.Approve(id, function (res) {
                        if (res != null) {

                            // get penalty list
                            getTransferTableList();
                        }
                    });
                }
            })
        });
    }
    function bindDepartureTableRowEvents() {

        var approveMainRow = $('.approveDepartureRow');
        var declineMainRow = $('.declineDepartureRow');
        var completeRows = $('.completeDepartureRow');

        // remove row      

        // complete row
        completeRows.unbind();
        completeRows.bind('click', function () {
            var id = parseInt($(this).data('id'));

            // yes or no widget
            _core.getWidget('CompleteDate').Widget.Show(function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('Departures').Widget;

                    // run
                    addMainWidget.Complete(id, res['EndDate'], function (res) {
                        if (res != null) {

                            // get penalty list
                            getDepartureTableList();
                        }
                    });
                }
            })
        });

        // decline row
        declineMainRow.unbind();
        declineMainRow.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(3, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('Departures').Widget;

                    // run
                    addMainWidget.Decline(id, function (res) {
                        if (res != null) {

                            // get penalty list
                            getDepartureTableList();
                        }
                    });
                }
            })
        });

        // decline row
        approveMainRow.unbind();
        approveMainRow.bind('click', function () {
            var id = parseInt($(this).data('id'));

            _core.getService('Loading').Service.enableBlur('loaderClass');

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(2, function (res) {

                if (res) {

                    var addMainWidget = _core.getWidget('Departures').Widget;

                    // run
                    addMainWidget.Approve(id, function (res) {
                        if (res != null) {

                            // get penalty list
                            getDepartureTableList();
                        }
                    });
                }
            })
        });
    }
    // bind files events
    function bindFilesEvents() {

        var removeBtn = $('.removeFileButton');
        var editBtn = $('.editFileButton');

        // remove file actin handle
        removeBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Remove(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });

        // edit file action handle
        editBtn.unbind().click(function () {

            var data = {};
            data.ID = $(this).data('id');
            data.TypeID = $(this).data('type');

            // postService
            _core.getWidget('AddFile').Widget.Edit(data, function (res) {
                if (res != null && res.status) {

                    // get file lists
                    getFileList(data.TypeID);
                }
                else alert('error while removing file');
            })
        });
    }

    // filter list
    function filterList(data) {

        // postService
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.filterURL, function (res) {
            if (res != null) {
                $('#filterList').empty().append(res)

                // bind pagination buttons
               
            }
        })
    }

    // get file list table rows
    function getFileList(type) {

        var data = {};
        data.PrisonerID = _self.PrisonerID;
        data.TypeID = type;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getFileTableRowURL, function (curHtml) {

            var table = null;
            table = $('#addfileTableBody');

            table.empty();
            table.append(curHtml);

            // bind file events
            bindFilesEvents();
        });
    }

    // get educational course list table rows
    function getMainTableList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getMainTableRowURL, function (curHtml) {

            var table = null;
            table = $('#maintablebody');

            table.empty();
            table.append(curHtml);

            bindMainTableRowEvents();
        });
    }
    // get educational course list table rows
    function getDepartureTableList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getDepartureTableRowURL, function (curHtml) {

            var table = null;
            table = $('#departureTableBody');

            table.empty();
            table.append(curHtml);
            bindDepartureTableRowEvents();
        });

    }
    function getPackageTableList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID;

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getPackageTableRowURL, function (curHtml) {

            var table = null;
            table = $('#packagesTableBody');

            table.empty();
            table.append(curHtml);
            bindPackageTableRowEvents();
        });
    }

    // get educational course list table rows
    function getTransferTableList() {

        var data = {};
        data.PrisonerID = _self.PrisonerID; getTransferTableList

        _core.getService('Loading').Service.enableBlur('trasnfersTableBody');

        // post service
        var postService = _core.getService('Post').Service;

        postService.postPartial(data, _self.getTransferTableRowURL, function (curHtml) {

            var table = null;
            table = $('#trasnfersTableBody');

            _core.getService('Loading').Service.disableBlur('trasnfersTableBody');

            table.empty();
            table.append(curHtml);
            bindMainTableRowEvents();
            bindTransferTableRowEvents();
        });
    }

}
/************************/


// creating class instance
var DashboardController = new DashboardControllerClass();

// creating object
var DashboardControllerObject = {
    // important
    Name: "Dashboard",

    Controller: DashboardController
}

// registering controller object
_core.addController(DashboardControllerObject);