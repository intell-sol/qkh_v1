﻿// Date service

/*****Main Function******/
var CalendarServiceClass = function () {
    var _self = this;

    _self.getDateTimeLocal = function(time) {
        return {
            "format": (time ? "DD/MM/YYYY HH:mm:ss" : "DD/MM/YYYY"),
            "separator": " - ",
            "applyLabel": "Հաստատել",
            "cancelLabel": "Մաքրել",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "daysOfWeek": [
                "Կրկ",
                "Երկ",
                "Երք",
                "Չրք",
                "Հնգ",
                "Ուր",
                "Շբթ"
            ],
            "monthNames": [
                "Հունվար",
                "Փետրվար",
                "Մարտ",
                "Ապրիլ",
                "Մայիս",
                "Հունիս",
                "Հուլիս",
                "Օգոստոս",
                "Սեպտեմբեր",
                "Հոկտեմբեր",
                "Նոյեմբեր",
                "Դեկտեմբեր"
            ],
            "firstDay": 1
        };
    }
}
/************************/


// creating class instance
var CalendarService = new CalendarServiceClass();

// creating object
var CalendarServiceObject = {
    // important
    Name: 'Calendar',

    Service: CalendarService
}

// registering controller object
_core.addService(CalendarServiceObject);