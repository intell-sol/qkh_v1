﻿// Validation service

/*****Main Function******/
var ValidationServiceClass = function () {
    var self = this;

    self.validate = function(action) {
        if (action == null || action) {
            alert('Խնդրում ենք լրացնել անհրաժաշտ բոլոր դաշտերը');
        }
    }
}
/************************/


// creating class instance
var ValidationService = new ValidationServiceClass();

// creating object
var ValidationServiceObject = {
    // important
    Name: 'Validation',

    Service: ValidationService
}

// registering controller object
_core.addService(ValidationServiceObject);