﻿// Date service

/*****Main Function******/
var DateServiceClass = function () {
    var _self = this;

    function init() {
        _self.today = new Date();
        _self.dd = _self.today.getDate();
        _self.mm = _self.today.getMonth() + 1;
        _self.yyyy = _self.today.getFullYear();
    }

    _self.CurrentDate = function () {
        init();
        return _self.dd + '/' + _self.mm + '/' + _self.yyyy;
    }

    _self.Bind = function (dates, time) {
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            timePicker: (time ? true : false),
            timePicker24Hour: (time ? true : false),
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(time ? true : false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format((time ? "DD/MM/YYYY HH:mm:ss" : "DD/MM/YYYY")));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });
        dates.each(function () {
            if ($(this).val() == "")
                $(this).data('daterangepicker').setStartDate(new Date($.now()));
            else {
                var dateString = $(this).val();
                dateString = dateString2Date(dateString);
                $(this).data('daterangepicker').setStartDate(dateString);
            }
        })
        function dateString2Date(dateString) {
            var dt = dateString.split(/\/|\s/);
            return new Date(dt.slice(0, 3).reverse().join('/') + ' ' + dt[3]);
        }
    }

}
/************************/


// creating class instance
var DateService = new DateServiceClass();

// creating object
var DateServiceObject = {
    // important
    Name: 'Date',

    Service: DateService
}

// registering controller object
_core.addService(DateServiceObject);