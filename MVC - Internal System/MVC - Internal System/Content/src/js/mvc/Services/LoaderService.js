﻿// loading service

/*****Main Function******/
var LoadingServiceClass = function () {
    var self = this;

    // enable loader
    self.enable = function (loaderDivClass, hideAndShowDivClass) {
        var elem = $('.' + loaderDivClass);
        var elemNew = $('.' + hideAndShowDivClass).addClass('hidden');
        elem.addClass('loading');
    }
    
    // disable loader
    self.disable = function (loaderDivClass, hideAndShowDivClass, hideAll) {
        var elem = $('.' + loaderDivClass);
        if (hideAll) $('.workingforms').addClass('hidden');
        var elemNew = $('.' + hideAndShowDivClass).removeClass('hidden');
        elem.removeClass('loading');
    }

    // disable all loader
    self.disableAll = function (loaderDivClass) {
        var elem = $('.' + loaderDivClass);
        elem.removeClass('loading');
    }

    // enable blur loader
    self.enableBlur = function (loaderDivClass) {
        var elemNew = $('.' + loaderDivClass);
        elemNew.addClass('blur');
        elemNew.addClass('loading');
    }

    // disable blur loader
    self.disableBlur = function (loaderDivClass) {
        var elemNew = $('.' + loaderDivClass);
        elemNew.removeClass('blur');
        elemNew.removeClass('loading');
    }

    // enable clear loader
    self.enableClear = function (loaderDivClass) {
        var elemNew = $('.' + loaderDivClass);
        elemNew.addClass('loading');
    }

    // disable clear loader
    self.disableClear = function (loaderDivClass) {
        var elemNew = $('.' + loaderDivClass);
        elemNew.removeClass('loading');
    }

    // enable clear loader
    self.enableClearByID = function (loaderDivID) {
        var elemNew = $('#' + loaderDivID);
        elemNew.addClass('loading');
    }

    // disable clear loader
    self.disableClearByID = function (loaderDivID) {
        var elemNew = $('#' + loaderDivID);
        elemNew.removeClass('loading');
    }
}
/************************/


// creating class instance
var LoadingService = new LoadingServiceClass();

// creating object
var LoadingServiceObject = {
    // important
    Name: 'Loading',

    Service: LoadingService
}

// registering controller object
_core.addService(LoadingServiceObject);