﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVC___Internal_System
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Merge",
                url: "Merge/{action}/{id}/{page}",
                defaults: new { controller = "Merge", action = "Index", id = UrlParameter.Optional, page = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Еncouragements",
                url: "Еncouragements/{action}/{id}",
                defaults: new { controller = "Еncouragements", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "_Data_Primary_",
                url: "_Data_Primary_/{action}/{id}",
                defaults: new { controller = "_Data_Primary_", action = "Add", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Convicts",
                url: "Convicts/{action}/{id}",
                defaults: new { controller = "Convicts", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Amnesty",
                url: "Amnesty/{action}/{id}/{page}",
                defaults: new { controller = "Amnesty", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}", 
                defaults: new { controller = "Login", action = "Login", id = UrlParameter.Optional }
            );
        }
    }
}
