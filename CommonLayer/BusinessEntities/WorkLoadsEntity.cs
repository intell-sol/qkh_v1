﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class WorkLoadsEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { get; set; }
        public int? JobTypeLibItemID { get; set; }
        public string JobTypeLibItemLabel { get; set; }
        public int? WorkTitleLibItemID { get; set; }
        public string WorkTitleLibItemLabel { get; set; }
        public int? EmployerLibItemID { get; set; }
        public string EmployerLibItemLabel { get; set; }
        public DateTime? EndDate { get; set; }
        public string Note { get; set; }
        public string ContractNumber { get; set; }
        public DateTime? EngagementDate { get; set; }
        public int? EngagementBasisLibItemID { get; set; }
        public string EngagementBasisLibItemLabel { get; set; }
        public int? Salary { get; set; }
        public bool? EngagementState { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public int? ReleaseBasisLibItemID { get; set; }
        public string ReleaseBasisLibItemLabel { get; set; }
        public string ReleaseNote { get; set; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
        public WorkLoadsEntity()
        {
        }
        public WorkLoadsEntity(int? ID = null, int? PrisonerID = null, int? JobTypeLibItemID = null,
            string JobTypeLibItemLabel = null, int? WorkTitleLibItemID = null, string WorkTitleLibItemLabel = null,
            int? EmployerLibItemID = null, string EmployerLibItemLabel = null, DateTime? EndDate = null,
            string ContractNumber = null, DateTime? EngagementDate = null,
            int? EngagementBasisLibItemID = null, string EngagementBasisLibItemLabel = null, int? Salary = null,
            bool? EngagementState = null, DateTime? ReleaseDate = null,
            int? ReleaseBasisLibItemID = null, string ReleaseBasisLibItemLabel = null,
            string ReleaseNote = null, string Note = null, bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.JobTypeLibItemID = JobTypeLibItemID;
            this.JobTypeLibItemLabel = JobTypeLibItemLabel;
            this.WorkTitleLibItemID = WorkTitleLibItemID;
            this.WorkTitleLibItemLabel = WorkTitleLibItemLabel;
            this.EmployerLibItemID = EmployerLibItemID;
            this.EmployerLibItemLabel = EmployerLibItemLabel;
            this.EndDate = EndDate;
            this.ContractNumber = ContractNumber;
            this.EngagementBasisLibItemID = EngagementBasisLibItemID;
            this.Salary = Salary;
            this.ReleaseDate = ReleaseDate;
            this.EngagementDate = EngagementDate;
            this.EngagementBasisLibItemLabel = EngagementBasisLibItemLabel;
            this.EngagementState = EngagementState;
            this.ReleaseBasisLibItemID = ReleaseBasisLibItemID;
            this.ReleaseBasisLibItemLabel = ReleaseBasisLibItemLabel;
            this.ReleaseNote = ReleaseNote;
            this.Note = Note;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }

        public override bool Equals(object obj)
        {
            WorkLoadsEntity old = (WorkLoadsEntity)obj;
            if (!PrisonerID.Equals(old.PrisonerID))
                return false;
            if (!JobTypeLibItemID.Equals(old.JobTypeLibItemID))
                return false;
            if (!WorkTitleLibItemID.Equals(old.WorkTitleLibItemID))
                return false;
            if (!WorkTitleLibItemLabel.Equals(old.WorkTitleLibItemLabel))
                return false;
            if (!EmployerLibItemID.Equals(old.EmployerLibItemID))
                return false;
            if (!EndDate.Equals(old.EndDate))
                return false;
            if (!ContractNumber.Equals(old.ContractNumber))
                return false;
            if (!EngagementBasisLibItemID.Equals(old.EngagementBasisLibItemID))
                return false;
            if (!EngagementState.Equals(old.EngagementState))
                return false;
            if (!ReleaseBasisLibItemID.Equals(old.ReleaseBasisLibItemID))
                return false;
            if (!Note.Equals(old.Note))
                return false;
            if (!ReleaseNote.Equals(old.ReleaseNote))
                return false;
            if (!EngagementDate.Equals(old.EngagementDate))
                return false;
            if (!JobTypeLibItemID.Equals(old.JobTypeLibItemID))
                return false;
            return true;
        }
        
    }
}
