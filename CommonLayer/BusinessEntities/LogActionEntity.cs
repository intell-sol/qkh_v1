﻿using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class LogActionEntity
    {
        public int? ID { set; get; }
        public int? EmployeeID { set; get; }

        public string EmployeeFirstName { get; set; }

        public string EmployeeMiddleName { get; set; }

        public string EmployeeLastName { get; set; }

        public int? PositionID { set; get; }

        public string PositionName { set; get; }

        public string ActionName { set; get; }
        public string RequestData { set; get; }
        public DateTime? CreationDate { get; set; }

        public LogActionEntity()
        {
            ID = null;
            EmployeeID = null;
            PositionID = null;
            ActionName = null;
            RequestData = null;
            CreationDate = null;
        }
        public LogActionEntity(int? ID = null, int? EmployeeID = null, int? PositionID = null, string ActionName = null, string RequestData = null, DateTime? CreationDate = null)
        {
            this.ID = ID;
            this.EmployeeID = EmployeeID;
            this.PositionID = PositionID;
            this.ActionName = ActionName;
            this.RequestData = RequestData;
            this.CreationDate = CreationDate;
        }
    }
}
