﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MedicalPrimaryEntity
    {
        public MedicalPrimaryEntity()
        {

        }
        public MedicalPrimaryEntity(int? ID = null, int? PrisonerID = null,
            int? HealthLibItemID = null,string PrimaryLibItemLabel = null,
            bool? Status = null, DateTime? ModifyDate = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.HealthLibItemID = HealthLibItemID;
            this.PrimaryLibItemLabel = PrimaryLibItemLabel;
            this.Status = Status;
            this.ModifyDate = ModifyDate;
            this.MergeStatus = MergeStatus;
        }
        public int? ID { get; set; }
        public int? PrisonerID { get; set; }
        public int? HealthLibItemID { get; set; }
        public string PrimaryLibItemLabel { get; set; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
