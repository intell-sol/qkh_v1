﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class TransferEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { get; set; }
        public int? PersonID { get; set; }
        public string PersonName { get; set; }
        public int? PurposeLibItemID { get; set; }
        public string PurposeLibItemLabel { get; set; }
        public int? TransferLibItemID { get; set; }
        public string TransferLibItemLabel { get; set; }
        public int? HospitalLibItemID { get; set; }
        public int? CourtLibItemID { get; set; }
        public int? StateLibItemID { get; set; }
        public string StateLibItemLabel { get; set; }
        public int? OrgUnitID { get; set; }
        public int? EndLibItemID { get; set; }
        public string EndLibItemLabel { get; set; }
        public string Description { get; set; }
        public int? EmergencyLibItemID { get; set; }
        public int? HogebujaranLibItemID { get; set; }
        public int? TransferTypeLibItemID { get; set; }        
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? ApproverEmployeeID { get; set; }
        public string ApproverEmployeeName { get; set; }
        public bool? Status { get; set; }
        public string Personal_ID { get; set; }
        public int? QnnchakanLibItemID { get; set; }
        public int? CurrentOrgUnit { get; set; }
        public bool? ApproveStatus { get; set; }
        public DateTime? ApproveDate { get; set; }
        public bool? MergeStatus { get; set; }
        public PersonEntity Person { get; set; }

        public TransferEntity()
        {
        }

        public TransferEntity(int? ID = null, int? PersonID = null, string PersonName=null,
            int? PrisonerID = null, int? PurposeLibItemID = null, string PurposeLibItemLabel=null,
            int? TransferLibItemID = null,
            string TransferLibItemLabel=null, int? HospitalLibItemID = null, int? CourtLibItemID = null,
            int? StateLibItemID = null,string StateLibItemLabel=null,
             int? OrgUnitID = null, int? EndLibItemID = null,string EndLibItemLabel=null,
             string Description=null, int? EmergencyLibItemID = null, int? TransferTypeLibItemID=null,
             DateTime? StartDate = null,DateTime? EndDate=null, DateTime? CreationDate = null,
             DateTime ? ModifyDate = null,int? ApproverEmployeeID=null,
             string ApproverEmployeeName=null,bool?Status=null, int? HogebujaranLibItemID = null,
             int? QnnchakanLibItemID = null,
             int? CurrentOrgUnit = null, bool? ApproveStatus = null, DateTime? ApproveDate = null, bool? MergeStatus = null)
        {
            this.ID = ID;            
            this.PersonID = PersonID;
            this.CurrentOrgUnit = CurrentOrgUnit;
            this.ApproveStatus = ApproveStatus;
            this.ApproveDate = ApproveDate;
            this.PersonName = PersonName;
            this.PrisonerID = PrisonerID;
            this.PurposeLibItemID = PurposeLibItemID;
            this.PurposeLibItemLabel = PurposeLibItemLabel;
            this.TransferLibItemID = TransferLibItemID;
            this.TransferLibItemLabel = TransferLibItemLabel;
            this.HospitalLibItemID = HospitalLibItemID;
            this.CourtLibItemID = CourtLibItemID;
            this.StateLibItemID = StateLibItemID;
            this.StateLibItemLabel = StateLibItemLabel;
            this.OrgUnitID = OrgUnitID;
            this.EndLibItemID = EndLibItemID;
            this.EndLibItemLabel = EndLibItemLabel;
            this.Description = Description;
            this.EmergencyLibItemID = EmergencyLibItemID;
            this.TransferTypeLibItemID = TransferTypeLibItemID;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.CreationDate = CreationDate;
            this.ModifyDate = ModifyDate;
            this.ApproverEmployeeID = ApproverEmployeeID;
            this.ApproverEmployeeName = ApproverEmployeeName;
            this.HogebujaranLibItemID = HogebujaranLibItemID;
            this.QnnchakanLibItemID = QnnchakanLibItemID;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }

    }
}
