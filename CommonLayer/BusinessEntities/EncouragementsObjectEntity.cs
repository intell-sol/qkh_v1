﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class EncouragementsObjectEntity
    {
        public int? PrisonerID { get; set; }
        public List<EncouragementsEntity> Encouragements { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public EncouragementsObjectEntity()
        {
            this.PrisonerID = null;
            this.Encouragements = new List<EncouragementsEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }
        public EncouragementsObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.Encouragements = new List<EncouragementsEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }

    }
}
