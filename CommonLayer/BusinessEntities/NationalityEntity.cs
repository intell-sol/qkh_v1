﻿
using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class NationalityEntity
    {
        public int? ID { set; get; }
        public int? Code { set; get; }
        public string Label { set; get; }
        public NationalityEntity()
        {
            ID = 0;
            Code = 0;
            Label = "";
        }
        public NationalityEntity(int ID, int Code, string Label)
        {
            this.ID = ID;
            this.Code = Code;
            this.Label = Label;
        }
    }
}
