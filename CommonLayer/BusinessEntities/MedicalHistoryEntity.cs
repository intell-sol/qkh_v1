﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MedicalHistoryEntity
    {
        public MedicalHistoryEntity()
        {

        }
        public MedicalHistoryEntity(int? ID = null, int? PrisonerID = null, int? Type = null,
            int? ResultLibItemID = null, string ResultNotes = null,
            string Name = null, DateTime? Date = null, bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.Type = Type;
            this.Name = Name;
            this.Date = Date;
            this.ResultLibItemID = ResultLibItemID;
            this.ResultNotes = ResultNotes;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
        public int? ID { get; set; }
        public int? PrisonerID { get; set; }
        public int? Type { get; set; }
        public string Name { get; set; }
        public int? ResultLibItemID { get; set; }
        public string ResultNotes { get; set; }
        public DateTime? Date { get; set; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
    }
}
