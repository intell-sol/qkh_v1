﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class VisitorEntity
    {
        public int? ID { set; get; }
        public string PersonalID { get; set; }
        public PersonEntity Person { get; set; }
        public int? PositionLibItemID { get; set; }
        public VisitorEntity()
        {
        }

    }
}