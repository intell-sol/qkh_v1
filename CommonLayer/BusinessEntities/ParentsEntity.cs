﻿using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class ParentsEntity
    {
        public int? ID { set; get; }
        public int? PersonID { set; get; }
        public int? PrisonerID { set; get; }
        public int? RelatedPersonID { set; get; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
        public PersonEntity Person { get; set; }
        public PersonEntity RelatedPerson { get; set; }

        public ParentsEntity()
        {
        }
        public ParentsEntity(int? ID = null, int? PersonID = null, int? PrisonerID = null, 
            int? RelatedPersonID = null, 
            bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PersonID = PersonID;
            this.PrisonerID = PrisonerID;
            this.RelatedPersonID = RelatedPersonID;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
    }
    
}
