﻿using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class AddressEntity
    {
        public AddressItemEntity Region { set; get; }
        public AddressItemEntity Community { set; get; }
        public AddressItemEntity Street { set; get; }
        public AddressItemEntity BuildingType { set; get; }
        public AddressItemEntity Building { set; get; }
        public AddressItemEntity Apartment { set; get; }
        public AddressEntity()
        {

        }
        public AddressEntity(AddressItemEntity Region = null, AddressItemEntity Community = null,
            AddressItemEntity Street = null, AddressItemEntity BuildingType = null, AddressItemEntity Building = null,
            AddressItemEntity Apartment = null)
        {
            this.Region = Region;
            this.Community = Community;
            this.Street = Street;
            this.BuildingType = BuildingType;
            this.Building = Building;
            this.Apartment = Apartment;
        }
    }
}
