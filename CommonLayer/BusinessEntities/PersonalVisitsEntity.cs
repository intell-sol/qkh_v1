﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PersonalVisitsEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { get; set; }
        public int? TypeLibItemID { get; set; }
        public string TypeLibItemLabel { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? PersonID { get; set; }
        public string PersonName { get; set; }
        public bool? VisitStatus { get; set; }
        public int? PurposeLibItemID { get; set; }
        public int? ApproverEmployeeID { get; set; }
        public string ApproverEmployeeName { get; set; }
        public string Description { get; set; }
        public string Group { get; set; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
        public PersonEntity Person { get; set; }
        public List<VisitorEntity> VisitorList { get; set; }

        public PersonalVisitsEntity()
        {
            
        }

        public PersonalVisitsEntity(int? ID = null, int? PrisonerID = null, int? TypeLibItemID = null,
            string TypeLibItemLabel = null, DateTime? StartDate = null, DateTime? EndDate = null,
            int? PersonID = null, string PersonName = null, bool? VisitStatus = null, 
            int? ApproverEmployeeID = null, string ApproverEmployeeName = null,
            bool? Status = null, bool? MergeStatus = null, string Description = null, string Group = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.TypeLibItemID = TypeLibItemID;
            this.TypeLibItemLabel = TypeLibItemLabel;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.PersonID = PersonID;
            this.PersonName = PersonName;
            this.VisitStatus = VisitStatus;
            this.ApproverEmployeeID = ApproverEmployeeID;
            this.ApproverEmployeeName = ApproverEmployeeName;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
            this.Description = Description;
            this.Group = Group;
        }

    }
}
