﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PersonalVisitsDashboardEntity : PersonalVisitsEntity
    {
        public string PrisonerName { get; set; }
        public string Personal_ID { get; set; }
        public int? PrisonerType { get; set; }
        public bool? ArchiveStatus { get; set; }
        public PersonalVisitsDashboardEntity()
        {
        }
        public PersonalVisitsDashboardEntity(int? ID = null, int? PrisonerID = null, int? TypeLibItemID = null,
            string TypeLibItemLabel = null, DateTime? StartDate = null, DateTime? EndDate = null,
            int? PersonID = null, string PersonName = null, bool? VisitStatus = null,
            int? ApproverEmployeeID = null, string ApproverEmployeeName = null,
            bool? Status = null,
            string PrisonerName = null, string Personal_ID = null, bool? ArchiveStatus = null, int? PrisonerType = null) 
            :base(ID , PrisonerID , TypeLibItemID ,
            TypeLibItemLabel , StartDate , EndDate ,
            PersonID , PersonName , VisitStatus ,
            ApproverEmployeeID , ApproverEmployeeName
              , Status )
        {
            
            this.PrisonerName = PrisonerName;
            this.Personal_ID = Personal_ID;
            this.ArchiveStatus = ArchiveStatus;
            this.PrisonerType = PrisonerType;
        }
    }
}