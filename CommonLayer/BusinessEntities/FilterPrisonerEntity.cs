﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class FilterPrisonerEntity
    {
        public int? Type { set; get; }
        public string Personal_ID { set; get; }
        public int OrgUnitID { set; get; }
        public string PersonalCaseNumber { set; get; }
        public string FirstName { set; get; }
        public string MiddleName { set; get; }
        public string LastName { set; get; }
        public int? CitizenshipLibItemID { set; get; }
        public List<int> CitizenshipLibItemIDList { set; get; }
        public List<int> OrgUnitList { set; get; }
        public int? SexLibItem_ID { set; get; }
        public int? NationalityID { set; get; }
        public bool? State { set; get; }
        public bool? ArchiveStatus { get; set; }
        public Paging paging { get; set; }
        public FilterPrisonerEntity()
        {
            this.ArchiveStatus = true;
        }
        public FilterPrisonerEntity(int? Type = null, string Personal_ID = null, string PersonalCaseNumber = null, 
            string FirstName = null, string MiddleName = null, string LastName = null, int? CitizenshipLibItemID = null,
            int? SexLibItem_ID = null, int? NationalityID = null, bool? ArchiveStatus = true)
        {
            this.Type = Type;
            this.Personal_ID = Personal_ID;
            this.PersonalCaseNumber = PersonalCaseNumber;
            this.FirstName = FirstName;
            this.MiddleName = MiddleName;
            this.LastName = LastName;
            this.CitizenshipLibItemID = CitizenshipLibItemID;
            this.SexLibItem_ID = SexLibItem_ID;
            this.NationalityID = NationalityID;
            this.ArchiveStatus = ArchiveStatus;
            CitizenshipLibItemIDList = new List<int>();
            OrgUnitList = new List<int>();
            paging = new Paging();
        }

        [Serializable]
        public class Paging
        {
            public int CurrentPage { set; get; }
            public int TotalPage { set; get; }
            public int ViewCount { set; get; }
            public int TotalCount { set; get; }

            public Paging(int CurrentPage = 1, int TotalPage = 1, int ViewCount = 10, int TotalCount = 0)
            {
                this.CurrentPage = CurrentPage;
                this.TotalPage = TotalPage;
                this.ViewCount = ViewCount;
                this.TotalCount = TotalCount;
            }
        }
    }
}
