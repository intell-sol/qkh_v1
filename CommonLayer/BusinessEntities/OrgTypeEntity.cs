﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class OrgTypeEntity
    {
        /// <summary>
        /// For Storing Organization Type
        /// </summary>
        public OrgTypeEntity()
        {
        }

        public OrgTypeEntity(string Name)
        {
            this.Name = Name;
        }

        public OrgTypeEntity(int ID, string Name)
        {
            this.ID = ID;
            this.Name = Name;
        }

        public int ID { get; set; }

        public string Name { get; set; }
 
    }
}
