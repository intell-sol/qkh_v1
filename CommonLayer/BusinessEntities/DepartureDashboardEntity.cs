﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class DepartureDashboardEntity : DepartureEntity
    {
        public string PrisonerName { get; set; }
        public string Personal_ID { get; set; }
        public int? PrisonerType { get; set; }
        public bool? ArchiveStatus { get; set; }
        public DepartureDashboardEntity()
        {
        }
        public DepartureDashboardEntity(int? ID = null, int? PrisonerID = null,string Place = null, int? TypeLibItemID = null,
            int? MedLibItemID = null,int? CaseLibItemID = null, int? MoveLibItemID = null,int? InvestigateLibItemID=null,
            int? OrgUnitID = null, int? CourtLibItemID = null, int? HospitalLibItemID = null,
            string TypeLibItemLabel = null, DateTime? StartDate = null, DateTime? EndDate = null,
            int? Duration = null,int? DuartionByHour = null, DateTime? CreationDate = null, DateTime? ModifyDate = null,
            int? ApproverEmployeeID = null, string ApproverEmployeeName = null,
            int? PurposeLibItemID = null, string PurposeLibItemLabel = null, bool? Status = null, bool? State = null,
            string PrisonerName = null, string Personal_ID = null, int? PrisonerType = null, bool? ArchiveStatus = null) 
            :base(ID,  PrisonerID , Place ,TypeLibItemID, MedLibItemID,
             CaseLibItemID,  MoveLibItemID ,InvestigateLibItemID,OrgUnitID,CourtLibItemID,HospitalLibItemID,
             TypeLibItemLabel,  StartDate, EndDate,
            Duration,DuartionByHour, CreationDate, ModifyDate,
            ApproverEmployeeID,ApproverEmployeeName,
            PurposeLibItemID ,PurposeLibItemLabel , Status, State)
        {
            
            this.PrisonerName = PrisonerName;
            this.Personal_ID = Personal_ID;
            this.ArchiveStatus = ArchiveStatus;
            this.PrisonerType = PrisonerType;
        }
    }
}