﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class UniversityDegreeEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? EducationProfessionsID { set; get; }
        public int? AcademicDegreeLibItemID { set; get; }
        public int? DegreeLibItemID { set; get; }
        public List<EducationAwardsEntity> Awardes { set; get; }
        public bool? Status { get; set; }

        public UniversityDegreeEntity()
        {
            ID = null;
            PrisonerID = null;
            EducationProfessionsID = null;
            AcademicDegreeLibItemID = null;
            DegreeLibItemID = null;
            Awardes = new List<EducationAwardsEntity>();
            Status = null;
        }
        public UniversityDegreeEntity(int? ID = null, int? PrisonerID = null, int? EducationProfessionsID = null, int? AcademicDegreeLibItemID = null, 
            int? DegreeLibItemID = null, List<EducationAwardsEntity> Awardes = null, bool? Status = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.EducationProfessionsID = EducationProfessionsID;
            this.AcademicDegreeLibItemID = AcademicDegreeLibItemID;
            this.DegreeLibItemID = DegreeLibItemID;
            this.Awardes = Awardes;
            this.Status = Status;
        }
    }
}
