﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class EncouragementsEntity
    {

        public int? ID { set; get; }
        public int? PrisonerID { get; set; }
        public int? TypeLibItemID { get; set; }
        public string TypeLibItemLabel { get; set; }
        public DateTime? StartDate { get; set; }
        public int? EmployeerID { get; set; }
        public string EmployeerName { get; set; }
        public int? BaseLibItemID { get; set; }
        public string BaseLibItemLabel { get; set; }
        public string Note { get; set; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
        public EncouragementsEntity()
        {
        }
        public EncouragementsEntity(int? ID = null, int? PrisonerID = null, int? TypeLibItemID = null,
            string TypeLibItemLabel = null, DateTime? StartDate = null, int? EmployeerID = null, string EmployeerName = null,
            int? BaseLibItemID = null, string BaseLibItemLabel = null, string Note = null, bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.TypeLibItemID = TypeLibItemID;
            this.TypeLibItemLabel = TypeLibItemLabel;
            this.StartDate = StartDate;
            this.EmployeerID = EmployeerID;
            this.EmployeerName = EmployeerName;
            this.BaseLibItemID = BaseLibItemID;
            this.BaseLibItemLabel = BaseLibItemLabel;
            this.Note = Note;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }

    }
}
