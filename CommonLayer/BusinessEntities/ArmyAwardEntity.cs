﻿using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class ArmyAwardEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? LibItemID { set; get; }
        public bool? Status { set; get; }
        public ArmyAwardEntity()
        {

        }
        public ArmyAwardEntity(int? ID = null, int? PrisonerID = null, int? LibItemID = null, bool? Status = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.LibItemID = LibItemID;
            this.Status = Status;
        }
    }
}
