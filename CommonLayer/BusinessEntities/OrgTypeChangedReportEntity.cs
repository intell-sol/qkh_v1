﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class OrgTypeChangedReportEntity
    {
        public DateTime? FirstDate { get; set; }
        public DateTime? SecondDate { get; set; }
        public int? OpenToHalfOpenOrgHigh { get; set; }
        public int? OpenToHalfCloseOrgHigh { get; set; }
        public int? OpenToCloseOrgHigh { get; set; }
        public int? HalfOpenToHalfCloseOrgHigh { get; set; }
        public int? HalfOpenToCloseOrgHigh { get; set; }
        public int? HalfCloseToCloseOrgHigh { get; set; }

        public int? CloseToHalfCloseOrgLow { get; set; }
        public int? HalfCloseToHalfOpenOrgLow { get; set; }
        public int? HalfOpenToOpenOrgLow { get; set; }

        public int? OpenToHalfOpenOwnHigh { get; set; }
        public int? OpenToHalfCloseOwnHigh { get; set; }
        public int? OpenToCloseOwnHigh { get; set; }
        public int? HalfOpenToHalfCloseOwnHigh { get; set; }
        public int? HalfOpenToCloseOwnHigh { get; set; }
        public int? HalfCloseToCloseOwnHigh { get; set; }
        public int? CloseToHalfCloseOwnPrev { get; set; }
        public int? CloseToHalfOpenOwnPrev { get; set; }
        public int? CloseToOpenOwnPrev { get; set; }
        public int? HalfCloseToHalfOpenOwnPrev { get; set; }
        public int? HalfCloseToOpenOwnPrev { get; set; }
        public int? HalfOpenToOpenOwnPrev { get; set; }
        public OrgTypeChangedReportEntity()
        {

        }
        public OrgTypeChangedReportEntity(DateTime? FirstDate = null,
                                          DateTime? SecondDate = null,
                                          int? OpenToHalfOpenOrgHigh = null,
                                          int? OpenToHalfCloseOrgHigh = null,
                                          int? OpenToCloseOrgHigh = null,
                                          int? HalfOpenToHalfCloseOrgHigh = null,
                                          int? HalfOpenToCloseOrgHigh = null,
                                          int? HalfCloseToCloseOrgHigh = null,
                                          int? CloseToHalfCloseOrgLow = null,
                                          int? HalfCloseToHalfOpenOrgLow = null,
                                          int? HalfOpenToOpenOrgLow = null,
                                          int? OpenToHalfOpenOwnHigh = null,
                                          int? OpenToHalfCloseOwnHigh = null,
                                          int? OpenToCloseOwnHigh = null,
                                          int? HalfOpenToHalfCloseOwnHigh = null,
                                          int? HalfOpenToCloseOwnHigh = null,
                                          int? HalfCloseToCloseOwnHigh = null,
                                          int? CloseToHalfCloseOwnPrev = null,
                                          int? CloseToHalfOpenOwnPrev = null,
                                          int? CloseToOpenOwnPrev = null,
                                          int? HalfCloseToHalfOpenOwnPrev = null,
                                          int? HalfCloseToOpenOwnPrev = null,
                                          int? HalfOpenToOpenOwnPrev = null)
        {
            this.FirstDate = FirstDate;
            this.SecondDate = SecondDate;
            this.OpenToHalfOpenOrgHigh = OpenToHalfOpenOrgHigh;
            this.OpenToHalfCloseOrgHigh = OpenToHalfCloseOrgHigh;
            this.OpenToCloseOrgHigh = OpenToCloseOrgHigh;
            this.HalfOpenToHalfCloseOrgHigh = HalfOpenToHalfCloseOrgHigh;
            this.HalfOpenToCloseOrgHigh = HalfOpenToCloseOrgHigh;
            this.HalfCloseToCloseOrgHigh = HalfCloseToCloseOrgHigh;
            this.CloseToHalfCloseOrgLow = CloseToHalfCloseOrgLow;
            this.HalfCloseToHalfOpenOrgLow = HalfCloseToHalfOpenOrgLow;
            this.HalfOpenToOpenOrgLow = HalfOpenToOpenOrgLow;
            this.OpenToHalfOpenOwnHigh = OpenToHalfOpenOwnHigh;
            this.OpenToHalfCloseOwnHigh = OpenToHalfCloseOwnHigh;
            this.OpenToCloseOwnHigh = OpenToCloseOwnHigh;
            this.HalfOpenToHalfCloseOwnHigh = HalfOpenToHalfCloseOwnHigh;
            this.HalfOpenToCloseOwnHigh = HalfOpenToCloseOwnHigh;
            this.HalfCloseToCloseOwnHigh = HalfCloseToCloseOwnHigh;
            this.CloseToHalfCloseOwnPrev = CloseToHalfCloseOwnPrev;
            this.CloseToHalfOpenOwnPrev = CloseToHalfOpenOwnPrev;
            this.CloseToOpenOwnPrev = CloseToOpenOwnPrev;
            this.HalfCloseToHalfOpenOwnPrev = HalfCloseToHalfOpenOwnPrev;
            this.HalfCloseToOpenOwnPrev = HalfCloseToOpenOwnPrev;
            this.HalfOpenToOpenOwnPrev = HalfOpenToOpenOwnPrev;
        }
    }
}
