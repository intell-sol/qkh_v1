﻿using System;

namespace CommonLayer.BusinessEntities
{
        [Serializable]
    public class ArchiveEntity
    {
        public int? ID { set; get; }
        public int? PersonalCaseNumber { set; get; }
        public DateTime? StartDate { set; get; }
        public DateTime? EndDate { set; get; }
        public string OrgUnitLabel { set; get; }

        public ArchiveEntity()
        {

        }
        public ArchiveEntity(int? ID = null, int? PersonalCaseNumber = null, string OrgUnitLabel = null, DateTime? StartDate = null, DateTime? EndDate = null)
        {
            this.ID = ID;
            this.PersonalCaseNumber = PersonalCaseNumber;
            this.OrgUnitLabel = OrgUnitLabel;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
        }
    }
}
