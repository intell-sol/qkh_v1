﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class SentencingDataArticleLibsEntity : IEqualityComparer<SentencingDataArticleLibsEntity>
    {
        public int? ID { set; get; }
        public int? SentencingDataID { set; get; }
        public int? ArrestDataID { set; get; }
        public int? PrevConvictionDataID { set; get; }
        public int? LibItem_ID { set; get; }
        public bool? Status { get; set; }

        public SentencingDataArticleLibsEntity()
        {
            ID = null;
            SentencingDataID = null;
            LibItem_ID = null;
            Status = null;
        }
        public SentencingDataArticleLibsEntity(int? ID = null, int? SentencingDataID = null, 
            int? PrevConvictionDataID = null, int? LibItem_ID = null, bool? Status = null, int? ArrestDataID = null)
        {
            this.ID = ID;
            this.PrevConvictionDataID = PrevConvictionDataID;
            this.SentencingDataID = SentencingDataID;
            this.LibItem_ID = LibItem_ID;
            this.ArrestDataID = ArrestDataID;
            this.Status = Status;
        }

        public bool Equals(SentencingDataArticleLibsEntity x, SentencingDataArticleLibsEntity y)
        {
            if (x.SentencingDataID == y.SentencingDataID && x.LibItem_ID == y.LibItem_ID)
                return true;
            return false;
        }

        public int GetHashCode(SentencingDataArticleLibsEntity obj)
        {
            throw new NotImplementedException();
        }
    }
}
