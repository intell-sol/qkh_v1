﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MergeEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { get; set; }
        public int? EmployeeID { get; set; }
        public int? ApproverID { get; set; }
        public string Json { get; set; }
        public int? Type { get; set; }
        public DateTime? Date { get; set; }
        public bool? Approved { get; set; }
        public int? EntityID{ get; set; }
        public DateTime? CreationDate { get; set; }
        public int? EID { get; set; }
        public bool? Status { get; set; }
        public int? OrgUnitID { get; set; }

        public MergeEntity()
        {
        }

        public MergeEntity(int? ID = null, int? PrisonerID = null, int? EmployeeID = null,
            int? ApproverID = null, string Json = null, int? Type = null,
            DateTime? Date = null, bool? Approved = null, int? EntityID = null,            
            int? OrgUnitID = null, DateTime? CreationDate = null, 
            int? EID = null, bool ? Status = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.EmployeeID = EmployeeID;
            this.ApproverID = ApproverID;
            this.Json = Json;
            this.Type = Type;
            this.Date = Date;
            this.Approved = Approved;
            this.EntityID = EntityID;            
            this.OrgUnitID = OrgUnitID;
            this.CreationDate = CreationDate;
            this.EID = EID;
            this.Status = Status;
        }

        public enum TableIDS
        {
            // Table ID format is 1xxyyzz, where xx is 'Բաժին', yy is 'Մաս', id yy == 00, then we don't have 'Մաս', zz is table order number
            INITIAL_DOC_IDENTITY = PermissionsHardCodedIds.MainData * 10000 + 1000100,                          //1010001,
            PRIMARY_PREVENTION = PermissionsHardCodedIds.MainData * 10000 + 1000200,                            //1020001,
            PRIMARY_IMAGES = PermissionsHardCodedIds.MainData * 10000 + 1000300,                                //1020002,
            PRIMARY_FIELS = PermissionsHardCodedIds.MainData * 10000 + 1000400,                                 //1020003,
            PHYSICAL_HEIGHT_WEIGHT = PermissionsHardCodedIds.PhysicalData * 10000 + 1000100,                    //1030001,
            PHYSICAL_CONTENT = PermissionsHardCodedIds.PhysicalData * 10000 + 1000200,                          //1030002,
            PHYSICAL_INVALIDS = PermissionsHardCodedIds.PhysicalData * 10000 + 1000300,                         //1030003,
            PHYSICAL_LEANINGS = PermissionsHardCodedIds.PhysicalData * 10000 + 1000400,                         //1030004,
            PHYSICAL_FILES = PermissionsHardCodedIds.PhysicalData * 10000 + 1000500,                            //1030005,
            MEDICAL_PRELIMINARY_CONTENT = PermissionsHardCodedIds.Medicals * 10000 + 1000101,                   //1040101,
            MEDICAL_PRELIMINARY_COMPLATIN = PermissionsHardCodedIds.Medicals * 10000 + 1000102,                 //1040102,
            MEDICAL_PRELIMINARY_EXTERNAL_EXAMINATION = PermissionsHardCodedIds.Medicals * 10000 + 1000103,      //1040103,
            MEDICAL_PRELIMINARY_DIAGNOISIS = PermissionsHardCodedIds.Medicals * 10000 + 1000104,                //1040104,
            MEDICAL_FILES = PermissionsHardCodedIds.Medicals * 10000 + 1000200,                                 //1040002,
            MEDICAL_AMBULATOR_HISTORY = PermissionsHardCodedIds.Medicals * 10000 + 1000300,                     //1040003,
            MEDICAL_AMBULATOR_HISTORY_COMPLATIN = PermissionsHardCodedIds.Medicals * 10000 + 1000301,           //1040301,
            MEDICAL_AMBULATOR_HISTORY_RESEARCH = PermissionsHardCodedIds.Medicals * 10000 + 1000302,            //1040302,
            MEDICAL_AMBULATOR_HISTORY_DIAGNOSIS = PermissionsHardCodedIds.Medicals * 10000 + 1000303,           //1040303,
            MEDICAL_STATIONARY_HISTORY = PermissionsHardCodedIds.Medicals * 10000 + 1000400,                    //1040004,
            MEDICAL_STATIONARY_HISTORY_COMPLATIN = PermissionsHardCodedIds.Medicals * 10000 + 1000401,          //1040401,
            MEDICAL_STATIONARY_HISTORY_RESEARCH = PermissionsHardCodedIds.Medicals * 10000 + 1000402,           //1040402,
            MEDICAL_STATIONARY_HISTORY_DIAGNOSIS = PermissionsHardCodedIds.Medicals * 10000 + 1000403,          //1040403,
            FINGERTATOOSCARS_FINGERS = PermissionsHardCodedIds.FingerprintsTattoosScars * 10000 + 1000100,      //1050001,
            FINGERTATOOSCARS_TATOOS = PermissionsHardCodedIds.FingerprintsTattoosScars * 10000 + 1000200,       //1050002,
            FINGERTATOOSCARS_SCARS = PermissionsHardCodedIds.FingerprintsTattoosScars * 10000 + 1000300,        //1050003,
            FAMILY_CONTENT = PermissionsHardCodedIds.FamilyRelative * 10000 + 1000100,                          //1060001,
            FAMILY_SPOUSES = PermissionsHardCodedIds.FamilyRelative * 10000 + 1000200,                          //1060002,
            FAMILY_CHILDRENS = PermissionsHardCodedIds.FamilyRelative * 10000 + 1000300,                        //1060003,
            FAMILY_PARENTS = PermissionsHardCodedIds.FamilyRelative * 10000 + 1000400,                          //1060004,
            FAMILY_SYSTER_BROTHER = PermissionsHardCodedIds.FamilyRelative * 10000 + 1000500,                   //1060005,   
            FAMILY_OTHERS = PermissionsHardCodedIds.FamilyRelative * 10000 + 1000600,                           //1060006,
            FAMILY_FILES = PermissionsHardCodedIds.FamilyRelative * 10000 + 1000700,                            //1060007,
            EDUCATION = PermissionsHardCodedIds.EducationsProfessions * 10000 + 1000100,                        //1070001,
            ARMY_CONTENT = PermissionsHardCodedIds.MilitaryService * 10000 + 1000100,                           //1080001,
            ARMY_MAIN = PermissionsHardCodedIds.MilitaryService * 10000 + 1000200,                              //1080002,
            ARMY_FILES = PermissionsHardCodedIds.MilitaryService * 10000 + 1000300,                             //1080003,
            ENTERANCE_ARREST_DATA = PermissionsHardCodedIds.AdoptionInformation * 10000 + 1000100,              //1090001,
            ENTERANCE_SENTENCE = PermissionsHardCodedIds.AdoptionInformation * 10000 + 1000200,                 //1090002,
            ENTERANCE_PROTOCOL = PermissionsHardCodedIds.AdoptionInformation * 10000 + 1000300,                 //1090003,
            ENTERANCE_FILES_ARREST = PermissionsHardCodedIds.AdoptionInformation * 10000 + 1000400,             //1090004,
            ENTERANCE_FILES = PermissionsHardCodedIds.AdoptionInformation * 10000 + 1000500,                    //1090005,
            ENTERANCE_PrisonerTypeStatus = PermissionsHardCodedIds.AdoptionInformation * 10000 + 1000600,       //1090006,
            ENTERANCE_PRISONERPUNISHMENTTYPE = PermissionsHardCodedIds.AdoptionInformation * 10000 + 1000700,   //1090007,
            CELL_MAIN = PermissionsHardCodedIds.CameraCard * 10000 + 1000100,                                   //1100001,
            CELL_FILES = PermissionsHardCodedIds.CameraCard * 10000 + 1000200,                                  //1100002,
            ITEMS_MAIN = PermissionsHardCodedIds.ItemsObjects * 10000 + 1000100,                                //1110001,
            ITEMS_FILES = PermissionsHardCodedIds.ItemsObjects * 10000 + 1000200,                               //1110002,
            BAN_MAIN = PermissionsHardCodedIds.DataBans * 10000 + 1000100,                                      //1120001,
            BAN_FILES = PermissionsHardCodedIds.DataBans * 100000 + 1000100,                                    //1120002,
            ENCOURAGEMENTS_MAIN = PermissionsHardCodedIds.Encouragements * 10000 + 1000100,                     //1130001,
            ENCOURAGEMENTS_FILES = PermissionsHardCodedIds.Encouragements * 10000 + 1000200,                    //1130002,
            PENALTY_MAIN = PermissionsHardCodedIds.Penalties * 10000 + 1000100,                                 //1140001,
            PENALTY_FILES = PermissionsHardCodedIds.Penalties * 10000 + 1000200,                                //1140002,
            EDUCATIONAL_COURSES_MAIN = PermissionsHardCodedIds.EducationalCourses * 10000 + 1000100,            //1150001,
            EDUCATIONAL_COURSES_FILES = PermissionsHardCodedIds.EducationalCourses * 10000 + 1000200,           //1150002,
            WORKLOADS_MAIN = PermissionsHardCodedIds.WorkLoads * 10000 + 1000100,                               //1160001,
            WORKLOADS_FILES = PermissionsHardCodedIds.WorkLoads * 10000 + 1000200,                              //1160002,
            OFFICIAL_VISITS_MAIN = PermissionsHardCodedIds.OfficialVisits * 10000 + 1000100,                    //1170001,
            OFFICIAL_VISITS_FILES = PermissionsHardCodedIds.OfficialVisits * 10000 + 1000200,                   //1170002,
            PERSONAL_VISITS_MAIN = PermissionsHardCodedIds.PersonalVisits * 10000 + 1000100,                    //1180001,
            PERSONAL_VISITS_FILES = PermissionsHardCodedIds.PersonalVisits * 10000 + 1000200,                   //1180002,
            PACKAGES_MAIN = PermissionsHardCodedIds.Packages * 10000 + 1000100,                                 //1190001,
            PACKAGES_FILES = PermissionsHardCodedIds.Packages * 10000 + 1000200,                                //1190002,
            DEPARTURES_MAIN = PermissionsHardCodedIds.Departures * 10000 + 1000100,                             //1200001,
            DEPARTURES_FILES = PermissionsHardCodedIds.Departures * 10000 + 1000200,                            //1200002,
            TRANSFERS_MAIN = PermissionsHardCodedIds.Transfers * 10000 + 1000100,                               //1210001,
            TRANSFERS_FILES = PermissionsHardCodedIds.Transfers * 10000 + 1000200,                              //1210002,
            CONFLICTS_MAIN = PermissionsHardCodedIds.ConflictPersons * 10000 + 1000100,                         //1220001,
            CONFLICTS_FILES = PermissionsHardCodedIds.ConflictPersons * 10000 + 1000200,                        //1220002,
            DATA_TERMINATE_MAIN = PermissionsHardCodedIds.DataTerminate * 10000 + 1000100                       //1230001, // TODO: not in _Popup_Controller

        }

        public class TableEntity
        {
            public int? Name;
            //public int? 
        }
    }
}
