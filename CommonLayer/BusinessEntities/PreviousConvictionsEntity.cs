﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PreviousConvictionsEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? CodeLibItemID { set; get; }
        public string CodeLibItemName { set; get; }
        public int? PenaltyLibItemID { set; get; }
        public string PenaltyLibItemName { set; get; }
        public int? PenaltyDay { set; get; }
        public int? PenaltyMonth { set; get; }
        public int? PenaltyYear { set; get; }
        public DateTime? StartDate { set; get; }
        public DateTime? EndDate { set; get; }
        public string Notes { set; get; }
        public bool? Status { get; set; }
        public bool Archive { get; set; }
        public bool? MergeStatus { get; set; }
        public List<SentencingDataArticleLibsEntity> SentencingDataArticles { get; set; }
        public List<LibsEntity> SentencingArticles { get; set; }

        public PreviousConvictionsEntity()
        {
            this.Archive = false;
        }
        public PreviousConvictionsEntity (int? ID = null, int? PrisonerID = null, int? CodeLibItemID = null,
            string CodeLibItemName= null, int? PenaltyDay = null, int? PenaltyMonth = null, int? PenaltyYear = null,
            string SentenceDuration = null, int ? PenaltyLibItemID = null, string PenaltyLibItemName = null,
            DateTime? StartDate = null, DateTime? EndDate = null, string Notes = null, bool? Status = null,
            bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.CodeLibItemID = CodeLibItemID;
            this.PenaltyDay = PenaltyDay;
            this.PenaltyMonth = PenaltyMonth;
            this.PenaltyYear = PenaltyYear;
            this.CodeLibItemName = CodeLibItemName;
            this.PenaltyLibItemName = PenaltyLibItemName;
            this.PenaltyLibItemID = PenaltyLibItemID;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.Notes = Notes;
            this.Status = Status;
            this.Archive = false;
            this.MergeStatus = MergeStatus;
        }
    }
}
