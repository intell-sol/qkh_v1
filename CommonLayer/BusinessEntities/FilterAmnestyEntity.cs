﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class FilterAmnestyEntity
    {      
        public string Number { set; get; }
        public int? TypeLibItemID { set; get; }
        public string Source { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public int? RecipientLibItemID { set; get; }
        public int? SignatoriesLibItemID { set; get; }
        public bool? State { set; get; }
        public DateTime? StartDate { set; get; }
        public DateTime? SignDate { set; get; }
        public DateTime? EndDate { set; get; }
        public DateTime? ModifyDate { set; get; }
        public DateTime? AcceptDate { set; get; }
        public DateTime? CreationDate { set; get; }
        public DateTime? EnterDate { set; get; }
        public List<AmnestyPointsEntity> AmnestyPoints { get; set; }
        public bool? Status { set; get; }
        public int? CurrentPage { get; set; }
        public int? ViewCount { get; set; }
        public int? PrisonerType { get; set; }
        public Paging paging { get; set; }
        public FilterAmnestyEntity()
        {
            this.Status = true;
        }
        public FilterAmnestyEntity(string Number=null, int? TypeLibItemID = null,DateTime?StrartDate= null, string Source = null,string Name = null,
            string Description = null, int? RecipientLibItemID=null, bool? State= null, DateTime? SignDate = null,
            DateTime? EndDate = null, DateTime? ModifyDate=null, DateTime? AcceptDate = null, DateTime? CreationDate = null,
            DateTime? EnterDate = null, List<AmnestyPointsEntity> AmnestyPoints = null,
            int? CurrentPage = null, int? ViewCount = null)
        {
            this.Number = Number;
            this.TypeLibItemID = TypeLibItemID;
            this.Source = Source;
            this.Name = Name;
            this.StartDate = StrartDate;
            this.Description=Description;
            this.RecipientLibItemID = RecipientLibItemID;
            this.State = State;
            this.SignDate = SignDate;
            this.EndDate = EndDate;
            this.ModifyDate = ModifyDate;
            this.AcceptDate = AcceptDate;
            this.CreationDate = CreationDate;
            this.EnterDate = EnterDate;
            this.AmnestyPoints = AmnestyPoints;
            this.CurrentPage = CurrentPage;
            this.ViewCount = ViewCount;
        }
        [Serializable]
        public class Paging
        {
            public int CurrentPage { set; get; }
            public int TotalPage { set; get; }
            public int ViewCount { set; get; }
            public int TotalCount { set; get; }

            public Paging(int CurrentPage = 1, int TotalPage = 1, int ViewCount = 10, int TotalCount = 0)
            {
                this.CurrentPage = CurrentPage;
                this.TotalPage = TotalPage;
                this.ViewCount = ViewCount;
                this.TotalCount = TotalCount;
            }
        }
    }
}
