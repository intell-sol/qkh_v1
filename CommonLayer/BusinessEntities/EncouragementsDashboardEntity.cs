﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class EncouragementsDashboardEntity : EncouragementsEntity
    {
        public string PrisonerName { get; set; }
        public string Personal_ID { get; set; }
        public int? PrisonerType { get; set; }
        public bool? ArchiveStatus { get; set; }
        public EncouragementsDashboardEntity()
        {
        }
        public EncouragementsDashboardEntity(int? ID = null, int? PrisonerID = null, int? TypeLibItemID = null,
            string TypeLibItemLabel = null, DateTime? StartDate = null, int? EmployeerID = null, string EmployeerName = null,
            int? BaseLibItemID = null, string BaseLibItemLabel = null, string Note = null, bool? Status = null, 
            string PrisonerName = null, string Personal_ID = null, bool? ArchiveStatus = null, int? PrisonerType = null) 
            :base(ID, PrisonerID,TypeLibItemID, TypeLibItemLabel,StartDate, EmployeerID,
                 EmployeerName, BaseLibItemID, BaseLibItemLabel, Note, Status)
        {
            
            this.PrisonerName = PrisonerName;
            this.Personal_ID = Personal_ID;
            this.ArchiveStatus = ArchiveStatus;
            this.PrisonerType = PrisonerType;
        }

    }
}