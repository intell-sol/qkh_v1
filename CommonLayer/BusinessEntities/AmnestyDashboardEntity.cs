﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class AmnestyDashboardEntity: AmnestyEntity
    {
       
        public AmnestyDashboardEntity()
        {
        }
        public AmnestyDashboardEntity(int?ID=null,string Number=null,int? TypeLibItemID=null,string Source=null,
            string Name=null,string Description=null,
            int? RecipientLibItemID=null,int? SignatoriesLibItemID=null,bool? State = null,DateTime? 
            SignDate=null, DateTime? EndDate = null, DateTime? ModifyDate = null, 
            DateTime? AcceptDate = null, DateTime? CreationDate = null, DateTime? EnterDate = null,bool?Status=null) 
            :base(ID,Number,TypeLibItemID,Source,Name,Description,RecipientLibItemID,SignatoriesLibItemID,State,
                 SignDate,EndDate,ModifyDate,AcceptDate,CreationDate,EnterDate,Status)
        {
        }
    }
}