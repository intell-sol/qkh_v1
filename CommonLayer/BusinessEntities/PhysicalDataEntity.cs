﻿
using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PhysicalDataEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? SkinColorLibItem_ID { set; get; }
        public int? EyesColorLibItem_ID { set; get; }
        public int? HairsColorLibItem_ID { set; get; }
        public int? BloodTypeLibItem_ID { set; get; }
        public string ExternalDesc { set; get; }
        public bool? MergeStatus { get; set; }
        public List<HeightWeightEntity> HeightWeight { set; get; }
        public List<InvalidEntity> Invalides { set; get; }
        public List<LeaningEntity> Leanings { set; get; }
        public List<AdditionalFileEntity> Files { set; get; }
        public PhysicalDataEntity ()
        {
            ID = 0;
            PrisonerID = 0;
            SkinColorLibItem_ID = 0;
            EyesColorLibItem_ID = 0;
            HairsColorLibItem_ID = 0;
            BloodTypeLibItem_ID = 0;
            ExternalDesc = null;
            HeightWeight = new List<HeightWeightEntity>();
            Invalides = new List<InvalidEntity>();
            Leanings = new List<LeaningEntity>();
            Files = new List<AdditionalFileEntity>();
        }
        public PhysicalDataEntity(int? ID = null, int? PrisonerID = null, int? SkinColorLibItem_ID = null,
            int? EyesColorLibItem_ID = null, int? HairsColorLibItem_ID = null,
            int? BloodTypeLibItem_ID = null, string ExternalDesc = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.SkinColorLibItem_ID = SkinColorLibItem_ID;
            this.EyesColorLibItem_ID = EyesColorLibItem_ID;
            this.HairsColorLibItem_ID = HairsColorLibItem_ID;
            this.BloodTypeLibItem_ID = BloodTypeLibItem_ID;
            this.ExternalDesc = ExternalDesc;
            this.MergeStatus = MergeStatus;
            HeightWeight = new List<HeightWeightEntity>();
            Invalides = new List<InvalidEntity>();
            Leanings = new List<LeaningEntity>();
            Files = new List<AdditionalFileEntity>();
        }
    }
}
