﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class OpenTypeReportEntity
    {
        public List<int> OrgUnitList { set; get; }
        public DateTime? Date { get; set; }
        public int? TypeID { get; set; }
        public string  TypeName { get; set; }
        public int? Number { get; set; }
        public string PrisonerName { get; set; }
        public string Decision { get; set; }
        public DateTime? SentencingStartDate { get; set; }
        public DateTime? SentencingEndDate { get; set; }
        public DateTime? EarlyDate { get; set; }
        public string OrgUnits { get; set; }
        public DateTime? FilterDate { get; set; }
        public DateTime? PrisonAccessDate { get; set; }
        public OpenTypeReportEntity()
        {

        }
        public OpenTypeReportEntity(
                                 DateTime? Date = null,
                                 string TypeName=null,
                                 int? TypeID=null,
                                 int? Number = null,
                                 string PrisonerName = null,
                                 string Decision = null,
                                 DateTime? SentencingStartDate = null,
                                 DateTime? SentencingEndDate = null,
                                 DateTime? EarlyDate = null,
                                 string OrgUnits = null,
                                 DateTime? FilterDate = null,
                                 DateTime? PrisonAccessDate = null,
                                 List<int> OrgUnitList = null)
        {
            this.Date = Date;
            this.TypeName = TypeName;
            this.TypeID = TypeID;
            this.Number = Number;
            this.PrisonerName = PrisonerName;
            this.Decision = Decision;
            this.SentencingStartDate = SentencingStartDate;
            this.SentencingEndDate = SentencingEndDate;
            this.FilterDate = FilterDate;
            this.PrisonAccessDate = PrisonAccessDate;
            this.OrgUnits = OrgUnits;
            this.EarlyDate = EarlyDate;
            OrgUnitList = new List<int>();
        }
    }
}
