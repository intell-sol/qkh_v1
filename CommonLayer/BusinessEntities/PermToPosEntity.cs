﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PermToPosEntity
    {
        public PermToPosEntity()
        {
            Approve = new List<int>();
        }

        public PermToPosEntity(int ID, int PermID, int OrgUnitID, int Type)
        {
            this.ID = ID;
            this.PermID = PermID;
            this.OrgUnitID = OrgUnitID;
            this.Type = Type;
        }

        public int PermID { get; set; }

        public int OrgUnitID { get; set; }

        public int Type { get; set; }

        public int ID { get; set; }
        

        public List<int> Approve { get; set; }
        
    }
}
