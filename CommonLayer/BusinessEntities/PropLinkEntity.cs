﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PropLinkEntity
    {
        public PropLinkEntity()
        {

        }
        public PropLinkEntity(Double id, PropsEntity prop, PropValuesEntity value)
        {
            this.id = id;
            this.property = prop;
            this.value = value;
        }
        public Double id { get; set; }
        public PropsEntity property { get; set; }
        public PropValuesEntity value { get; set; }
    }
}
