﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MergeDashboardEntity : MergeEntity
    {
        public string PrisonerName { get; set; }
        public string Personal_ID { get; set; }
        public int? PrisonerType { get; set; }
        public bool? ArchiveStatus { get; set; }
        public int? ModificationCount { get; set; }
        public DateTime? LastModificationDate { get; set; } 

        public MergeDashboardEntity()
        {

        }

        public MergeDashboardEntity(int? ID = null, int? PrisonerID = null, int? EmployeeID = null,
            int? ApproverID = null, string Json = null, int? Type = null,
            DateTime? Date = null, bool? Approved = null, int? EntityID = null,            
            int? OrgUnitID = null, DateTime? CreationDate = null, int? EID = null, bool? Status = null,
            string PrisonerName = null, string Personal_ID = null, bool? ArchiveStatus = null, int? PrisonerType = null, 
            int? ModificationCount = null, DateTime? LastModificationDate = null)
            :base(ID,PrisonerID, EmployeeID ,
            ApproverID, Json, Type, Date, Approved,
            EntityID, OrgUnitID, CreationDate, EID, Status)
        {

            this.PrisonerName = PrisonerName;
            this.Personal_ID = Personal_ID;
            this.ArchiveStatus = ArchiveStatus;
            this.PrisonerType = PrisonerType;
            this.ModificationCount = ModificationCount;
            this.LastModificationDate = LastModificationDate;
        }
    }
}
