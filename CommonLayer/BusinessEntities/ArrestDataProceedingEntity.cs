﻿using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class ArrestDataProceedingEntity
    {
        public int? ID { set; get; }
        public int? ArrestDataID { set; get; }
        public int? ProceedLibItemID { set; get; }
        public string ProceedLibItemLabel { set; get; }
        public DateTime? Date { set; get; }
        public bool? Status { get; set; }
        public ArrestDataProceedingEntity()
        {

        }
        public ArrestDataProceedingEntity(int? ID = null, int? ArrestDataID = null, int? ProceedLibItemID = null,
           string ProceedLibItemLabel = null, DateTime? Date = null, bool? Status = null)
        {
            this.ID = ID;
            this.ArrestDataID = ArrestDataID;
            this.ProceedLibItemID = ProceedLibItemID;
            this.ProceedLibItemLabel = ProceedLibItemLabel;
            this.Date = Date;
            this.Status = Status;
        }
    }
}
