﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PrisonerConvictSemesterReportEntity
    {
        public DateTime? FirstDate { get; set; }
        public DateTime? SecondDate { get; set; }
        public List<int?> OrgUnitList { get; set; }
        public string OrgUnitName { get; set; }
        public int? AllPrisoners { get; set; }
        public int? Prisoner { get; set; }
        public int? WaithingCurt { get; set; }
        public int? Convict { get; set; }
        public int? UnderInvestigation { get; set; }
        public int? PreviousConvictions { get; set; }
        public int? DontHavePrevConv { get; set; }  
        public int? SubToTransfer { get; set; }
        public int? CloseType { get; set; }
        public int? HalfCloseType { get; set; }
        public int? HalfOpenType { get; set; }
        public int? OpenType { get; set; }
        public int? NotBig { get; set; }
        public int? Mid { get; set; }
        public int? Heavy { get; set; }
        public int? Grave { get; set; }
        public int? OtherNation { get; set; }
        public int? ALLFilterPrisoners { get; set; }
        public int? DuringFilterPrisoner { get; set; }
        public int? DuringFilterConvict { get; set; }
        public int? FPreviousConvictions { get; set; }
        public int? FDontHavePrevConv { get; set; }
        public int? TransferOtherOrg { get; set; }
        public int? MentalHospital { get; set; }
        public int? ALLGetRid { get; set; }
        public int? FullPenalty { get; set; }
        public int? EarlyClose { get; set; }
        public int? Amnesty { get; set; }
        public int? LetOff { get; set; }
        public int? Sick { get; set; }
        public int? Curt { get; set; }
        public int? CurtOther { get; set; }
        public int? CurtPenalty { get; set; }
        public int? CurtWork { get; set; }
        public int? CurtProbation { get; set; }
        public int? CurtAmnesty { get; set; }
        public int? ChangedORClosed { get; set; }
        public int? ChiefDecision { get; set; }
        public int? OtherCaseCose { get; set; }
        public int? Escape { get; set; }
        public int? Died { get; set; }
        public int? Under1 { get; set; }
        public int? Under1To3 { get; set; }
        public int? Under3To5 { get; set; }
        public int? Under5To10 { get; set; }
        public int? Under10To15 { get; set; }
        public int? MoreThen15 { get; set; }
        public int? Underage { get; set; }
        public int? Age18To25 { get; set; }
        public int? Age25To35 { get; set; }
        public int? Age35To45 { get; set; }
        public int? Age45To60 { get; set; }
        public int? AgeMore60 { get; set; }
        public PrisonerConvictSemesterReportEntity()
        {

        }
        public PrisonerConvictSemesterReportEntity(DateTime? FirstDate = null,
                                                   DateTime? SecondDate = null,
                                                   List<int?> OrgUnitList = null,
                                                   string OrgUnitName = null,
                                                   int? AllPrisoners = null,
                                                   int? Prisoner = null,
                                                   int? WaithingCurt = null,
                                                   int? Convict = null,
                                                   int? UnderInvestigation = null,
                                                   int? PreviousConvictions = null,
                                                   int? DontHavePrevConv = null,
                                                   int? SubToTransfer = null,
                                                   int? CloseType = null,
                                                   int? HalfCloseType = null,
                                                   int? HalfOpenType = null,
                                                   int? OpenType = null,
                                                   int? NotBig = null,
                                                   int? Mid = null,
                                                   int? Heavy = null,
                                                   int? Grave = null,
                                                   int? OtherNation = null,
                                                   int? ALLFilterPrisoners = null,
                                                   int? DuringFilterPrisoner = null,
                                                   int? DuringFilterConvict = null,
                                                   int? FPreviousConvictions = null,
                                                   int? FDontHavePrevConv = null,
                                                   int? TransferOtherOrg = null,
                                                   int? MentalHospital = null,
                                                   int? ALLGetRid = null,
                                                   int? FullPenalty = null,
                                                   int? EarlyClose = null,
                                                   int? Amnesty = null,
                                                   int? LetOff = null,
                                                   int? Sick = null,
                                                   int? Curt = null,
                                                   int? CurtOther = null,
                                                   int? CurtPenalty = null,
                                                   int? CurtWork = null,
                                                   int? CurtProbation = null,
                                                   int? CurtAmnesty = null,
                                                   int? ChangedORClosed = null,
                                                   int? ChiefDecision = null,
                                                   int? OtherCaseCose = null,
                                                   int? Escape = null,
                                                   int? Died = null,
                                                   int? Under1 = null,
                                                   int? Under1To3 = null, 
                                                   int? Under3To5 = null,
                                                   int? Under5To10 = null,
                                                   int? Under10To15 = null,
                                                   int? MoreThen15 = null,
                                                   int? Underage =null,
                                                   int? Age18To25 = null,
                                                   int? Age25To35 = null,
                                                   int? Age35To45 = null,  
                                                   int? Age45To60 =null, 
                                                   int? AgeMore60 = null )
        {
            this.FirstDate = FirstDate;
            this.SecondDate = SecondDate;
            OrgUnitList = new List<int?>();
            this.OrgUnitName = OrgUnitName;
            this.AllPrisoners = AllPrisoners;
            this.Prisoner = Prisoner;
            this.WaithingCurt = WaithingCurt;
            this.Convict = Convict;
            this.UnderInvestigation = UnderInvestigation;
            this.PreviousConvictions = PreviousConvictions;
            this.DontHavePrevConv = DontHavePrevConv;
            this.SubToTransfer = SubToTransfer;
            this.CloseType = CloseType;
            this.HalfCloseType = HalfCloseType;
            this.HalfOpenType = HalfOpenType;
            this.OpenType = OpenType;
            this.NotBig = NotBig;
            this.Mid = Mid;
            this.Heavy = Heavy;
            this.Grave = Grave;
            this.OtherNation = OtherNation;
            this.ALLFilterPrisoners = ALLFilterPrisoners;
            this.DuringFilterPrisoner = DuringFilterPrisoner;
            this.DuringFilterConvict = DuringFilterConvict;
            this.FPreviousConvictions = FPreviousConvictions;
            this.FDontHavePrevConv = FDontHavePrevConv;
            this.TransferOtherOrg = TransferOtherOrg;
            this.MentalHospital = MentalHospital;
            this.ALLGetRid = ALLGetRid;
            this.FullPenalty = FullPenalty;
            this.EarlyClose = EarlyClose;
            this.Amnesty = Amnesty;
            this.LetOff = LetOff;
            this.Sick = Sick;
            this.Curt = Curt;
            this.CurtPenalty = CurtPenalty;
            this.CurtWork = CurtWork;
            this.CurtProbation = CurtProbation;
            this.CurtAmnesty = CurtAmnesty;
            this.ChangedORClosed = ChangedORClosed;
            this.ChiefDecision = ChiefDecision;
            this.OtherCaseCose = OtherCaseCose;
            this.Escape = Escape;
            this.Died = Died;
            this.Under1 = Under1;
            this.Under1To3 = Under1To3;
            this.Under3To5 = Under3To5;
            this.Under5To10 = Under5To10;
            this.Under10To15 = Under10To15;
            this.MoreThen15 = MoreThen15;
            this.Underage = Underage;
            this.Age18To25 = Age18To25;
            this.Age25To35 = Age25To35;
            this.Age35To45 = Age35To45;
            this.Age45To60 = Age45To60;
            this.AgeMore60 = AgeMore60;
        }
    }
}
