﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class WorkLoadsDashboardEntity : WorkLoadsEntity
    {
        public string PrisonerName { get; set; }
        public string Personal_ID { get; set; }
        public int? PrisonerType { get; set; }
        public bool? ArchiveStatus { get; set; }
        public WorkLoadsDashboardEntity()
        {
        }
        public WorkLoadsDashboardEntity(int? ID = null, int? PrisonerID = null, int? JobTypeLibItemID = null,
            string JobTypeLibItemLabel = null, int? WorkTitleLibItemID = null, string WorkTitleLibItemLabel = null,
            int? EmployerLibItemID = null, string EmployerLibItemLabel = null, DateTime? EndDate = null,
            string ContractNumber = null, DateTime? EngagementDate = null,
            int? EngagementBasisLibItemID = null, string EngagementBasisLibItemLabel = null,
            int? Salary = null,
            bool? EngagementState = null, DateTime? ReleaseDate = null,
            int? ReleaseBasisLibItemID = null, string ReleaseBasisLibItemLabel = null,
            string ReleaseNote = null, string Note = null, bool? Status = null, 
            string PrisonerName = null, string Personal_ID = null, bool? ArchiveStatus = null, int? PrisonerType = null) 
            :base(ID , PrisonerID , JobTypeLibItemID ,
            JobTypeLibItemLabel , WorkTitleLibItemID , WorkTitleLibItemLabel ,
            EmployerLibItemID , EmployerLibItemLabel , EndDate ,
            ContractNumber , EngagementDate ,
            EngagementBasisLibItemID , EngagementBasisLibItemLabel , Salary ,
            EngagementState , ReleaseDate ,
            ReleaseBasisLibItemID , ReleaseBasisLibItemLabel,
            ReleaseNote , Note , Status )
        {
            
            this.PrisonerName = PrisonerName;
            this.Personal_ID = Personal_ID;
            this.ArchiveStatus = ArchiveStatus;
            this.PrisonerType = PrisonerType;
        }

    }
}