﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PenaltiesProtestEntity
    {
        public int? ID { get; set; }
	    public int? PenaltyID { get; set; }
	    public int? AppealsContentLibItemID { get; set; }
	    public int? AppealsBody { get; set; }
	    public DateTime? AppealsDate { get; set; }
	    public string AppealsNote { get; set; }
	    public int? AppealResultLibItemID { get; set; }
	    public string AppealResultNote { get; set; }
	    public DateTime? AppealResultDate { get; set; }
        public bool? Status { get; set; }
        public PenaltiesProtestEntity()
        {
        }
        public PenaltiesProtestEntity(int? ID = null, int? PenaltyID = null, int? AppealsContentLibItemID = null,
            int? AppealsBody = null, DateTime? AppealsDate = null, string AppealsNote = null,
            int? AppealResultLibItemID = null, string AppealResultNote = null, DateTime? AppealResultDate = null, bool? Status = null)
        {
            this.ID = ID;
            this.PenaltyID = PenaltyID;
            this.AppealsContentLibItemID = AppealsContentLibItemID;
            this.AppealsBody = AppealsBody;
            this.AppealsDate = AppealsDate;
            this.AppealsNote = AppealsNote;
            this.AppealResultLibItemID = AppealResultLibItemID;
            this.AppealResultNote = AppealResultNote;
            this.AppealResultDate = AppealResultDate;
            this.Status = Status;
        }

    }
}
