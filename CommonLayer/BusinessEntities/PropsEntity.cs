﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PropsEntity
    {
        public PropsEntity()
        {

        }
        public PropsEntity(int? ID = null, string Name = null, Boolean? Fixed = null)
        {
            this.ID = ID;
            this.Name = Name;
            this.Fixed = Fixed;
        }
        public int? ID { get; set; }
        public string Name { get; set; }
        public Boolean? Fixed { get; set; }
        public List<PropValuesEntity> Values { get; set; }
        public bool NewItem { get; set; }

        public enum PropToLibID
        {
            IDENTIFICATION_CODE = 4
        }
    }
}
