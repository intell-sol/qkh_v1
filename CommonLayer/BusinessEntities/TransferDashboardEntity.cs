﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class TransferDashboardEntity : TransferEntity
    {
        public string PrisonerName { get; set; }
        public int? PrisonerType { get; set; }
        public bool? ArchiveStatus { get; set; }
        public TransferDashboardEntity()
        {
        }
        public TransferDashboardEntity(int? ID = null, int? PersonID = null, string PersonName = null,
            int? PrisonerID = null, int? PurposeLibItemID = null, string PurposeLibItemLabel = null,
            int? TransferLibItemID = null,
            string TransferLibItemLabel = null, int? HospitalLibItemID = null, int? CourtLibItemID = null,
            int? StateLibItemID = null, string StateLibItemLabel = null,
             int? OrgUnitID = null, int? EndLibItemID = null, string EndLibItemLabel = null,
             string Description = null, int? EmergencyLibItemID = null, int? TransferTypeLibItemID = null,
             DateTime? StartDate = null, DateTime? EndDate = null, DateTime? CreationDate = null,
             DateTime? ModifyDate = null, int? ApproverEmployeeID = null,
             string ApproverEmployeeName = null, bool? Status = null, int? HogebujaranLibItemID = null,
             int? QnnchakanLibItemID = null,
             int? CurrentOrgUnit = null, bool? ApproveStatus = null, DateTime? ApproveDate = null,
             string PrisonerName = null, string Personal_ID = null, int? PrisonerType = null, bool? ArchiveStatus = null)
            : base(ID, PersonID,PersonName,PrisonerID , PurposeLibItemID , PurposeLibItemLabel ,
            TransferLibItemID , TransferLibItemLabel , HospitalLibItemID , CourtLibItemID ,StateLibItemID ,
            StateLibItemLabel ,OrgUnitID , EndLibItemID , EndLibItemLabel ,Description , EmergencyLibItemID ,
            TransferTypeLibItemID ,StartDate , EndDate , CreationDate ,ModifyDate , ApproverEmployeeID ,
             ApproverEmployeeName , Status , HogebujaranLibItemID ,QnnchakanLibItemID ,
             CurrentOrgUnit , ApproveStatus , ApproveDate )
        {
            this.PrisonerName = PrisonerName;
            this.Personal_ID = Personal_ID;
            this.ArchiveStatus = ArchiveStatus;
            this.PrisonerType = PrisonerType;
        }
    }
}