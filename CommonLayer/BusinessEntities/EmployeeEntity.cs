﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class EmployeeEntity : ISerializable
    {
        public EmployeeEntity()
        {

        }

        public EmployeeEntity(int OrgUnitID, string PSN, string FirstName, string LastName)
        {
            this.OrgUnitID = OrgUnitID;
            this.PSN = PSN;
            this.FirstName = FirstName;
            this.LastName = LastName;
        }

        public EmployeeEntity(int ID, int OrgUnitID, string PSN, string FirstName, string LastName)
        {
            this.ID = ID;
            this.OrgUnitID = OrgUnitID;
            this.PSN = PSN;
            this.FirstName = FirstName;
            this.LastName = LastName;
        }

        public int ID { get; set; }

        public int OrgUnitID { get; set; }

        public string PSN { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public DateTime BirthDay { get; set; }

        public string Passport { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public bool ChangePassword { get; set; }

        public bool Editable { get; set; }

        public OrderEntity Order { get; set; }
        
        public String Phone { get; set; }
        public String Email { get; set; }
        public String RegistrationAddress { get; set; }
        public String LivingAddress { get; set; }
        public String Nationality { get; set; }
        public String Fax { get; set; }
        public DateTime PassportDate { get; set; }
        public String PassportBy { get; set; }
        public int? Photo { get; set; }
        public string PhotoLink { get; set; }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("ID", this.ID);
            info.AddValue("OrgUnitID", this.OrgUnitID);
            info.AddValue("SocialCardNumber", this.PSN);
            info.AddValue("FirstName", this.FirstName);
            info.AddValue("MiddleName", this.MiddleName);
            info.AddValue("LastName", this.LastName);
            info.AddValue("BirthDate", this.BirthDay.ToString("dd/MM/yyyy"));
            info.AddValue("Login", this.Login);
            //info.AddValue("Password", this.Password);
            info.AddValue("ChangePassword", this.ChangePassword);
            info.AddValue("Editable", this.Editable);

            info.AddValue("Phone", this.Phone);
            info.AddValue("Email", this.Email);
            info.AddValue("RegistrationAddress", this.RegistrationAddress);
            info.AddValue("LivingAddress", this.LivingAddress);
            info.AddValue("Nationality", this.Nationality);
            info.AddValue("Fax", this.Fax);
            info.AddValue("Passport", this.Passport);
            info.AddValue("PassportDate", this.PassportDate);
            info.AddValue("PassportBy", this.PassportBy);
            info.AddValue("Photo", this.Photo);
            info.AddValue("PhotoLink", this.PhotoLink);

            if (this.Order != null && this.Order.OrderTypeID != null)
            {
                info.AddValue("Order", this.Order);
            }
        }
    }
}
