﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class ItemObjectEntity
    {
        public int? ID { set; get; }
        public List<ItemEntity> Items { set; get; }
        public List<AdditionalFileEntity> Files { set; get; }
    }
}
