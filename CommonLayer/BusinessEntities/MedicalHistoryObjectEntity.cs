﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MedicalHistoryObjectEntity
    {
        public int? PrisonerID { get; set; }
        public MedicalHistoryEntity currentMedicalHistory { get; set; }
        public List<MedicalComplaintsEntity> MedicalComplaints { get; set; }
        public List<MedicalResearchEntity> MedicalResearch { get; set; }
        public List<MedicalHealingEntity> MedicalHealing { get; set; }
        public List<MedicalExternalExaminationEntity> MedicalExternalExaminations { get; set; }
        public List<MedicalDiagnosisEntity> MedicalDiagnosis { get; set; }
        public List<MedicalHistoryEntity> MedicalHistories { get; set; }
        public MedicalHistoryObjectEntity()
        {
        }
        public MedicalHistoryObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
        }

    }
}
