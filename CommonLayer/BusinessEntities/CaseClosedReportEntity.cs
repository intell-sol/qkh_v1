﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class CaseClosedReportEntity
    {
        public int? OrgUnitID { get; set; }
        public List<int> OrgUnitList { set; get; }
        public DateTime? Date { get; set; }
        public int? Number { get; set; }
        public string PrisonerName { get; set; }
        public string Sentencing { get; set; }
        public DateTime? SentencingStartDate { get; set; }
        public string CloseInfo { get; set; }
        public string OrgUnitNames { get; set; }
        public string CaseCloseType { get; set; }
        public string Description { get; set; }
        public DateTime? CurDate { get; set; }

        public CaseClosedReportEntity()
        {

        }
        public CaseClosedReportEntity(int? OrgUnitID = null,
                                 DateTime? Date = null,
                                 int? Number = null,
                                 string PrisonerName = null,
                                 string Sentencing = null,
                                 DateTime? SentencingStartDate = null,
                                 DateTime? CurDate = null,
                                 string CloseInfo = null,
                                 string OrgUnitNames = null,
                                 string CaseCloseType = null,
                                 string Description = null,
                                 List<int> OrgUnitList = null)
        {
            this.OrgUnitID = OrgUnitID;
            this.Date = Date;
            this.Number = Number;
            this.PrisonerName = PrisonerName;
            this.Sentencing = Sentencing;
            this.SentencingStartDate = SentencingStartDate;
            this.CloseInfo = CloseInfo;
            this.OrgUnitNames = OrgUnitNames;
            this.CaseCloseType = CaseCloseType;
            this.Description = Description;
            this.CurDate = CurDate;
            OrgUnitList = new List<int>();
        }
    }
}
