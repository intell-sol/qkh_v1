﻿using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PenaltiesDashboardEntity : PenaltiesEntity
    {
        public string PrisonerName { get; set; }
        public string Personal_ID { get; set; }
        public bool? ArchiveStatus { get; set; }
        public int? PrisonerType { get; set; }
        public PenaltiesDashboardEntity()
        {
        }
        public PenaltiesDashboardEntity(int? ID = null, int? PrisonerID = null, int? TypeListItemID = null, string TypeListItemName = null, DateTime? PenaltiesMaturity = null,
            DateTime? ViolationDate = null, DateTime? PenaltyDate = null, int? StateLibItemID = null, DateTime? PenaltyEndDate = null,
             int? EmployeerID = null, string EmployeerName = null, int? StatusChangeBasisLibItemID = null,
            string StatusChangeBasisLibItemName = null, DateTime? StatusChangeBasis = null, string Notes = null, bool? Status = null, string StateLibItemLabel = null,
            string PrisonerName = null, string Personal_ID = null, bool? ArchiveStatus = null, int? PrisonerType = null) 
            :base(ID: ID, PrisonerID: PrisonerID, TypeListItemID: TypeListItemID, TypeListItemName: TypeListItemName, PenaltiesMaturity: PenaltiesMaturity,
                 EmployeerID: EmployeerID, EmployeerName: EmployeerName,
                 ViolationDate: ViolationDate, PenaltyDate: PenaltyDate, StateLibItemID: StateLibItemID,
                 PenaltyEndDate: PenaltyEndDate, StatusChangeBasisLibItemID: StatusChangeBasisLibItemID,
                 StatusChangeBasisLibItemName: StatusChangeBasisLibItemName, StatusChangeBasis: StatusChangeBasis,
                 Notes: Notes, Status: Status, StateLibItemLabel: StateLibItemLabel)
        {
            
            this.PrisonerName = PrisonerName;
            this.Personal_ID = Personal_ID;
            this.ArchiveStatus = ArchiveStatus;
            this.PrisonerType = PrisonerType;
        }
    }
}