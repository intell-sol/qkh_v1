﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PenaltiesEntity
    {
        public int? ID { get; set; }
	    public int? PrisonerID { get; set; }
	    public int? TypeListItemID { get; set; }
	    public string TypeListItemName { get; set; }
        public DateTime? ViolationDate { get; set; }
        public DateTime? PenaltyDate { get; set; }
        public int? StateLibItemID { get; set; }
        public DateTime? PenaltyEndDate { get; set; }
        public DateTime? PenaltiesMaturity { get; set; }
        public int? EmployeerID { get; set; }
        public string EmployeerName { get; set; }
        public int? StatusChangeBasisLibItemID { get; set; }
        public string StateLibItemLabel { get; set; }
        public string StatusChangeBasisLibItemName { get; set; }
        public DateTime? StatusChangeBasis { get; set; }
        public string Notes { get; set; }
        public List<PenaltiesViolationEntity> ViolationList { get; set; }
        public List<PenaltiesProtestEntity> ProtestList { get; set; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
        public PenaltiesEntity()
        {
        }
        public PenaltiesEntity(int? ID = null, int? PrisonerID = null, int? TypeListItemID = null, string TypeListItemName = null,
            DateTime? ViolationDate = null, DateTime? PenaltyDate = null, int? StateLibItemID = null, DateTime? PenaltyEndDate = null,
            DateTime? PenaltiesMaturity = null, int? EmployeerID = null, string EmployeerName = null, int? StatusChangeBasisLibItemID = null,
            string StatusChangeBasisLibItemName = null, DateTime? StatusChangeBasis = null, string Notes = null, bool? Status = null, bool? MergeStatus = null, string StateLibItemLabel = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.TypeListItemID = TypeListItemID;
            this.TypeListItemName = TypeListItemName;
            this.ViolationDate = ViolationDate;
            this.PenaltyDate = PenaltyDate;
            this.StateLibItemID = StateLibItemID;
            this.StateLibItemLabel = StateLibItemLabel;
            this.PenaltyEndDate = PenaltyEndDate;
            this.PenaltiesMaturity = PenaltiesMaturity;
            this.EmployeerID = EmployeerID;
            this.EmployeerName = EmployeerName;
            this.StatusChangeBasisLibItemID = StatusChangeBasisLibItemID;
            this.StatusChangeBasisLibItemName = StatusChangeBasisLibItemName;
            this.StatusChangeBasis = StatusChangeBasis;
            this.Notes = Notes;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }

    }
}
