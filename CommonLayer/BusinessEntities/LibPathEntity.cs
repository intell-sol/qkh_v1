﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    /// <summary>
    /// For storing Lib paths tree
    /// </summary>
    [Serializable]
    public class LibPathEntity
    {
        public LibPathEntity()
        {

        }
        public LibPathEntity(int ID, int Count)
        {
            this.ID = ID;
            this.LibsCount = Count;
        }
        public LibPathEntity(string Name, int ParentID)
        {
            this.Name = Name;
            this.ParentID = ParentID;
            LibsCount = 0;
        }
        public LibPathEntity(int ID, string Name, int ParentID)
        {
            this.ID = ID;
            this.Name = Name;
            this.ParentID = ParentID;
            LibsCount = 0;
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public int ParentID { get; set; }
        public int LibsCount { get; set; }

        public List<LibPathEntity> subitems = new List<LibPathEntity>();
    }
}
