﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PackageContentEntity
    {
        public int? ID { get; set; }
        public int? PackageID { get; set; }
        public int? TypeLibItemID { get; set; }
        public string TypeLibItemLabel { get; set; }
        public int? GoodsLibItemID { get; set; }
        public string GoodsLibItemLabel { get; set; }
        public int? MeasureLibItemID { get; set; }
        public string MeasureLibItemLabel { get; set; }
        public int? WeightCount { get; set; }
        public bool? Status { get; set; }
        public PackageContentEntity()
        {
        }
        public PackageContentEntity(int? ID = null, int? PackageID = null, int? TypeLibItemID = null,
            string TypeLibItemLabel = null,
         int? GoodsLibItemID = null, string GoodsLibItemLabel = null, int?
            MeasureLibItemID = null, string MeasureLibItemLabel = null, int? WightCount = null, bool? Status = null)
        {
            this.ID = ID;
            this.PackageID = PackageID;
            this.TypeLibItemID = TypeLibItemID;
            this.TypeLibItemLabel = TypeLibItemLabel;
            this.GoodsLibItemID = GoodsLibItemID;
            this.GoodsLibItemLabel = GoodsLibItemLabel;
            this.MeasureLibItemID = MeasureLibItemID;
            this.MeasureLibItemLabel = MeasureLibItemLabel;
            this.WeightCount = WightCount;
            this.Status = Status;
        }
    }
}
