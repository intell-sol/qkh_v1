﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MedicalHealingEntity
    {
        public MedicalHealingEntity()
        {

        }
        public MedicalHealingEntity(int? ID = null, int? PrisonerID = null, int? MedicalHistoryID = null,
           string NameLibItemLabel = null, string Description = null, bool? State = null, int? NameLibItemID = null, bool? Status = null, DateTime? ModifyDate = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.MedicalHistoryID = MedicalHistoryID;
            this.NameLibItemLabel = NameLibItemLabel;
            this.Description = Description;
            this.State = State;
            this.NameLibItemID = NameLibItemID;
            this.Status = Status;
            this.ModifyDate = ModifyDate;
        }
        public int? ID { get; set; }
        public int? PrisonerID { get; set; }
        public int? MedicalHistoryID { get; set; }
        public int? NameLibItemID { get; set; }
        public string NameLibItemLabel { get; set; }
        public string Description { get; set; }
        public bool? State { get; set; }
        public bool? Status { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
