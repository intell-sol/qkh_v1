﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PropValuesEntity
    {
        public PropValuesEntity()
        {

        }
        public PropValuesEntity(string Value)
        {
            this.Value = Value;
        }
        public PropValuesEntity(int ID, string Value)
        {
            this.ID = ID;
            this.Value = Value;
        }
        public PropValuesEntity(int ID, string Value, int PropID)
        {
            this.ID = ID;
            this.Value = Value;
            this.PropID = PropID;
        }
        public PropValuesEntity(int? ID = null, string Value = null, int? PropID = null, bool? Status = null)
        {
            this.ID = ID;
            this.Value = Value;
            this.PropID = PropID;
            this.Status = Status;
        }
        public int? ID { get; set; }
        public string Value { get; set; }
        public int? PropID { get; set; }
        public bool? Status { get; set; }
    }
}
