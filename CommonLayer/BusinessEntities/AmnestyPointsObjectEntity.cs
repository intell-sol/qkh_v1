﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class AmnestyPointsObjectEntity
    {
        public int? PrisonerID { get; set; }
        public List<AmnestyObjectEntity> AmnestyPoints { get; set; }
        public AmnestyPointsObjectEntity()
        {
            this.PrisonerID = null;
            this.AmnestyPoints = new List<AmnestyObjectEntity>();
        }
        public AmnestyPointsObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.AmnestyPoints = new List<AmnestyObjectEntity>();
        }

    }
}
