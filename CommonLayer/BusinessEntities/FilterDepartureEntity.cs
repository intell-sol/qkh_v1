﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class FilterDepartureEntity
    {
        public int? TypeLibItemID { get; set; }
        public int? PurposeLibItemID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Duration { get; set; }
        public int? DuartionByHour { get; set; }
        public int? PrisonerID { get; set; }
        public string FirstName { set; get; }
        public string MiddleName { set; get; }
        public string LastName { set; get; }
        public bool ArchiveStatus { get; set; }
        public bool? State { get; set; }
        public List<int> OrgUnitIDList { set; get; }
        public int? OrgUnitID { get; set; }
        public int? CurrentPage { get; set; }
        public int? ViewCount { get; set; }
        public int? PrisonerType { get; set; }
        public Paging paging { get; set; }
        public FilterDepartureEntity()
        {
            this.ArchiveStatus = true;
        }
        public FilterDepartureEntity(int? TypeLibItemID = null, int? PurposeLibItemID = null,int? Duration=null, int? DuartionByHour=null, DateTime? StartDate = null, DateTime? EndDate = null,
            int? PrisonerID = null, bool ArchiveStatus = true, List<int> OrgUnitIDList = null,
            string FirstName = null, string MiddleName = null, string LastName = null,
            int? CurrentPage = null, int? ViewCount = null, int? OrgUnitID = null, int? PrisonerType = null, bool? State = null)
        {
            this.TypeLibItemID = TypeLibItemID;
            this.PurposeLibItemID = PurposeLibItemID;
            this.StartDate = StartDate;
            this.Duration = Duration;
            this.DuartionByHour = DuartionByHour;
            this.EndDate = EndDate;
            this.FirstName = FirstName;
            this.MiddleName = MiddleName;
            this.LastName = LastName;
            this.PrisonerID = PrisonerID;
            this.ArchiveStatus = ArchiveStatus;
            this.OrgUnitIDList = OrgUnitIDList;
            this.PrisonerType = PrisonerType;
            this.CurrentPage = CurrentPage;
            this.ViewCount = ViewCount;
            this.OrgUnitID = OrgUnitID;
            this.State = State;
        }
        [Serializable]
        public class Paging
        {
            public int CurrentPage { set; get; }
            public int TotalPage { set; get; }
            public int ViewCount { set; get; }
            public int TotalCount { set; get; }

            public Paging(int CurrentPage = 1, int TotalPage = 1, int ViewCount = 10, int TotalCount = 0)
            {
                this.CurrentPage = CurrentPage;
                this.TotalPage = TotalPage;
                this.ViewCount = ViewCount;
                this.TotalCount = TotalCount;
            }
        }
    }
}
