﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class ArmyEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { set; get; }
        public int? TypeLibItemID { set; get; }
        public int? ProffessionLibItemID { set; get; }
        public int? RankLibItemID { set; get; }
        public int? PositionLibItemID { set; get; }
        public bool? Status { set; get; }
        public bool? MergeStatus { set; get; }
        public ArmyEntity() {
        }
        public ArmyEntity(int? ID = null, int? PrisonerID = null, DateTime? StartDate = null, DateTime? EndDate = null, int? TypeLibItemID = null, 
            int? ProffessionLibItemID = null, int? RankLibItemID = null, int? PositionLibItemID = null, bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.TypeLibItemID = TypeLibItemID;
            this.ProffessionLibItemID = ProffessionLibItemID;
            this.RankLibItemID = RankLibItemID;
            this.PositionLibItemID = PositionLibItemID;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }

    }
}
