﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class LanguageEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? EducationProfessionsID { set; get; }
        public int? LibItemID { set; get; }
        public bool? Status { get; set; }
        
        public LanguageEntity()
        {

        }

        public LanguageEntity(int? ID = null, int? PrisonerID = null, int? EducationProfessionsID = null, int? LibItemID = null, bool? Status = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.EducationProfessionsID = EducationProfessionsID;
            this.LibItemID = LibItemID;
            this.Status = Status;
        }
    }
}
