﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class WorkLoadsObjectEntity
    {
        public int? PrisonerID { get; set; }
        public List<WorkLoadsEntity> WorkLoads { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public WorkLoadsObjectEntity()
        {
            this.PrisonerID = null;
            this.WorkLoads= new List<WorkLoadsEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }
        public WorkLoadsObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.WorkLoads = new List<WorkLoadsEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }

    }
}
