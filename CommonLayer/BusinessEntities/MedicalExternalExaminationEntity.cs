﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MedicalExternalExaminationEntity
    {
        public MedicalExternalExaminationEntity()
        {

        }
        public MedicalExternalExaminationEntity(int? ID = null, int? PrisonerID = null,
            int? MedicalHistoryID = null,
           string MedicalHistoryName = null, string Description = null,
           bool? State = null, DateTime? Date = null, bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.MedicalHistoryID = MedicalHistoryID;
            this.MedicalHistoryName = MedicalHistoryName;
            this.Description = Description;
            this.State = State;
            this.Date = Date;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
        public int? ID { get; set; }
        public int? PrisonerID { get; set; }
        public int? MedicalHistoryID { get; set; }
        public string MedicalHistoryName { get; set; }
        public string Description { get; set; }
        public bool? State { get; set; }
        public DateTime? Date { get; set; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
    }
}
