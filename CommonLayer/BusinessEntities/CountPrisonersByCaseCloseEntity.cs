﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class CountPrisonersByCaseCloseEntity
    {
        public DateTime? CurrentDate { get; set; }
        public DateTime? FirstDate { get; set; }
        public int? GetRid { get; set; }
        public int? Full { get; set; }
        public int? Early { get; set; }
        public int? Amnesty { get; set; }
        public int? ԼetՕff { get; set; }
        public int? Sick { get; set; }
        public int? Died { get; set; }
        public int? EndPrisonerDate { get; set; }
        public int? Curt { get; set; }
        public int? Changed { get; set; }
        public int? ChiefDecision { get; set; }
        public CountPrisonersByCaseCloseEntity()
        {

        }
        public CountPrisonersByCaseCloseEntity(DateTime? CurrentDate = null,
                                         DateTime? FirstDate = null,
                                         int? GetRid = null,
                                         int? Full = null,
                                         int? Early = null,
                                         int? Amnesty = null,
                                         int? ԼetՕff = null,
                                         int? Sick = null,
                                         int? Died = null,
                                         int? EndPrisonerDate = null,
                                         int? Curt = null,
                                         int? Changed = null,
                                         int? ChiefDecision = null)
        {
            this.CurrentDate = CurrentDate;
            this.FirstDate = FirstDate;
            this.GetRid = GetRid;
            this.Full = Full;
            this.Early = Early;
            this.Amnesty = Amnesty;
            this.ԼetՕff = ԼetՕff;
            this.Sick = Sick;
            this.Died = Died;
            this.EndPrisonerDate = EndPrisonerDate;
            this.Curt = Curt;
            this.Changed = Changed;
            this.ChiefDecision = ChiefDecision;
        }
    }
}
