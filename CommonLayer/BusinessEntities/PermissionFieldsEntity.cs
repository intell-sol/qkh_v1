﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PermissionFieldsEntity
    {
        public string EditJson { set; get; }
        public int? EditType { set; get; }
        public PermissionFieldsEntity() {
        }
    }
}
