﻿using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class AdditionalFileEntity : FileEntity
    {
        private FileType TypeID1;

        public int? ID { set; get; }
        public int? PersonID { set; get; }
        public int? PrisonerID { get; set; }
        public FileType? TypeID { set; get; }
        public string Description { set; get; }
        public string Path { set; get; }
        public bool? Status { set; get; }
        public bool? State { set; get; }
        public DateTime ModifyDate { set; get; }
        public bool? MergeStatus { get; set; }
        public AdditionalFileEntity()
        {

        }
        public AdditionalFileEntity(long ContentLength) : base(ContentLength)
        {
            ID = null;
            Description = null;
            Path = null;
            Status = null;
            State = true;
        }

        public AdditionalFileEntity(FileType TypeID)
        {
            TypeID1 = TypeID;
        }

        public string ServerFileFullPath
        {
            get
            {
                string localPath = HelperClasses.ConfigurationHelper.GetFileStoragePath();
                localPath += @"/";
                localPath += Path;
                return localPath;
            }
        }
        
    }
    public enum FileType
    {
        MAIN_DATA_AVATAR_IMAGES = 15,
        MAIN_DATA_ATTACHED_IMAGES = 1,
        MAIN_DATA_ATTACHED_FILES = 2,
        PHYSICAL_DATA_INVALID = 3,
        PHYSICAL_DATA_ATTACHED_FILES = 4,
        FAMILY_ATTACHED_FILES = 6,
        ARMY_ATTACHED_FILES = 7,
        ADOPTION_INFORMATION_ARREST_RECORD = 8,
        ADOPTION_INFORMATION_ATTACHED_FILES = 9,
        CAMERA_CARD_ATTACHED_FILES = 10,
        ITEMS_ATTACHED_FILES = 11,
        INJUNCTIONS_ATTACHED_FILES = 13,
        EDUCATION_PROFESSIONS_FILES = 14,
        FINGERTATOOSCARS_FINGER = 18,
        FINGERTATOOSCARS_TATOO = 20,
        FINGERTATOOSCARS_SCAR = 22,
        TERMINATE = 23,
        ENCOURAGEMENTS = 26,
        PENALTIES = 27,
        EDUCATIONAL_COURSES = 28,
        WORKLOADS = 29,
        OFFICIAL_VISITS = 30,
        PERSONAL_VISITS = 31,
        PACKAGES = 32,
        DEPARTURES = 33,
        MEDICAL_PRELIMINARY = 34,
        MEDICAL_AMBULATOR = 35,
        MEDICAL_STATIONARY = 36,
        TRANSFERS = 37,
        CONFLICTS = 38,
        PERSON_ADD_PHOTO = 39
    }
}
