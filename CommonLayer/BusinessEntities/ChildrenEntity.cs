﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class ChildrenEntity
    {
        public int? ID { set; get; }
        public int? PersonID { set; get; }
        public int? PrisonerID { set; get; }
        public ChildrenType? ChildrenType { set; get; }
        public string Certificate { set; get; }
        public bool? Status { get; set; }
        public PersonEntity Person { get; set; }
        public string Note { get; set; }
        public bool? MergeStatus { get; set; }

        public ChildrenEntity()
        {
        }
        public ChildrenEntity(int? ID = null, int? PersonID = null,
            int? PrisonerID = null, ChildrenType? ChildrenType = null,
            string Certificate = null, 
            bool? State = null, bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PersonID = PersonID;
            this.PrisonerID = PrisonerID;
            this.ChildrenType = ChildrenType;
            this.Certificate = Certificate;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
    }

    public enum ChildrenType
    {
        OWN             = 1,
        ADOPTIVE        = 2,
        UNDER_THE_CARE  = 3
    }
}
