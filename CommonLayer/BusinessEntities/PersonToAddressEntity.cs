﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PersonToAddressEntity
    {
        public int? ID { get; set; }
	    public int? RegionID { get; set; }
	    public int? ComunityID { get; set; }       
        public int? StreetID { get; set; }            
        public int? BuildingTypeID { get; set; }
        public int? BuildingID { get; set; }      
        public int? ApartmentID { get; set; }
        public string RegionName { get; set; }
        public string ComunityName { get; set; }
        public string StreetName { get; set; }        
        public string BuildingTypeName { get; set; }
        public string BuildingName { get; set; }
        public string ApartmentName { get; set; }
        public DateTime? CreationDate { get; set; }
        public bool? Status { get; set; }
        public PersonToAddressEntity()
        {
        }
        public PersonToAddressEntity(int? ID=null, int? RegionID=null, int? ComunityID=null, int? StreetID=null,
            int? BuildingTypeID=null, int? BuildingID=null, int? ApartmentID = null, string RegionName = null,
            string ComunityName = null, string StreetName = null, string BuildingTypeName = null, string BuildingName = null,
            string ApartmentName = null, DateTime? CreationDate=null, bool? Status=null)
        {
            this.ID = ID;
            this.RegionID = RegionID;
            this.ComunityID = ComunityID;
            this.StreetID = StreetID;
            this.BuildingTypeID = BuildingTypeID;
            this.BuildingID = BuildingID;
            this.ApartmentID = ApartmentID;
            this.RegionName = RegionName;
            this.ComunityName = ComunityName;
            this.StreetName = StreetName;
            this.BuildingTypeName = BuildingTypeName;
            this.BuildingName = BuildingName;
            this.ApartmentName = ApartmentName;
            this.CreationDate = CreationDate;
            this.Status = Status;
        }

    }
}
