﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PenaltiesObjectEntity
    {
        public int? PrisonerID { get; set; }
        public List<PenaltiesEntity> Penalties { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public PenaltiesObjectEntity()
        {
            this.PrisonerID = null;
            this.Penalties = new List<PenaltiesEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }
        public PenaltiesObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.Penalties = new List<PenaltiesEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }

    }
}
