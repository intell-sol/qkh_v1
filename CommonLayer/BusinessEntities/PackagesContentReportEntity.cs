﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PackagesContentReportEntity
    {
        public int? ID { get; set; }
        public string TypeName { get; set; }
        public string GoodsName { get; set; }
        public string MeasureName { get; set; }
        public int? WightCount { get; set; }
        public PackagesContentReportEntity()
        {

        }
        public PackagesContentReportEntity(int? ID = null,
                                    string TypeName = null,
                                    string GoodsName = null,
                                    string MeasureName = null,
                                    int? WightCount = null)
        {
            this.ID = ID;
            this.TypeName = TypeName;
            this.GoodsName = GoodsName;
            this.MeasureName = MeasureName;
            this.WightCount = WightCount;
        }
    }
}
