﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class InjunctionsEntity
    {
        public int? PrisonerID;
        public List<InjunctionItemEntity> Items { set; get; }
        public List<AdditionalFileEntity> Files { set; get; }

        public InjunctionsEntity()
        {

        }
    }
}
