﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    /// <summary>
    /// For storing Lib paths tree
    /// </summary>
    [Serializable]
    public class LibsEntity
    {
        public LibsEntity()
        {

        }
        public LibsEntity(string Name)
        {
            this.Name = Name;
        }
        public LibsEntity(string Name, int ParentID)
        {
            this.Name = Name;
            this.ParentID = ParentID;
        }
        public LibsEntity(int ID, string Name, int ParentID)
        {
            this.ID = ID;
            this.Name = Name;
            this.ParentID = ParentID;
        }
        public LibsEntity(int ID, string Name, int ParentID, Boolean Editable, int Depth)
        {
            this.ID = ID;
            this.Name = Name;
            this.ParentID = ParentID;
            this.Editable = Editable;
            this.Depth = Depth;
        }
        public LibsEntity(int ID, string Name, int ParentID, Boolean Status, DateTime CreationDate)
        {
            this.ID = ID;
            this.Name = Name;
            this.ParentID = ParentID;
            this.Status = Status;
            this.CreationDate = CreationDate;
        }
        public LibsEntity(int ID, string Name, int ParentID, Boolean Status, DateTime CreationDate, Boolean Editable)
        {
            this.ID = ID;
            this.Name = Name;
            this.ParentID = ParentID;
            this.Status = Status;
            this.CreationDate = CreationDate;
            this.Editable = Editable;
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public Boolean Status { get; set; }
        public int ParentID { get; set; }
        public Boolean Editable { get; set; }

        public int Depth { get; set; }

        public DateTime CreationDate { get; set; }

        public List<LibsEntity> subitems = new List<LibsEntity>();

        public List<PropsEntity> propList = new List<PropsEntity>();

        public enum LIBS
        {
            GERNER = 31,
            CITIZENSHIP = 33,
            DOCIDENTITY = 34,
            INVALID_DEGREE = 39,
            LEANING_TYPE = 40,
            SKIN_COLOR = 76,
            EYES_COLOR = 77,
            HAIR_COLOR = 78,
            Blood_Type = 149,
            EDUCATION = 49,
            PROFESSION = 79,
            PREFERANCE = 80,
            ABILITIES = 81,
            LAGNUAGES = 82,
            ACADEMIC_DEGREE = 83,
            SCIENCE_DEGREE = 84,
            OFFICIAL_GIFTS = 85,
            NO_SERVE = 87,
            MILITARY_AWARDS = 88,
            MILITARY_WARS = 89,
            ARMY_TYPE = 90,
            ARMY_PROFESSION = 91,
            ARMY_RANK = 92,
            ARMY_POSTITION = 93,
            ITEMS_OBJECTS = 96,
            BAN_OBJECT = 99,
            BAN_PERSON = 100,
            SENTENCE_PERFORM_PERSON = 102,
            SENTENCE_CODES = 103,
            SENTENCE_PUNISHMENT_TYPE = 104,
            SENTENCE_PROPERTY_CONFISCATION = 163,
            PRISONER_PUNISHMENT_TYPE = 174,
            PRISONER_PUNISHMENT_BASE = 175,
            DETECTIVE_RANK = 105,
            QKH_TYPE = 106,
            VARUYT_MARMIN = 107,
            KALANAVORMAN_MARMIN = 108,
            SYSTER_BROTHER_TYPE = 48,
            RELATIVE_TYPE = 109,
            TERMINATION_CASE_CONVICT = 111,
            TERMINATION_CASE_PRISONER = 162,
            TERMINATION_BY_WHOOM = 112,
            ENCOURAGEMENT_TYPE_CONVICT = 114,
            ENCOURAGEMENT_TYPE_PRISONER = 173,
            ENCOURAGEMENT_BASE = 115,
            PENALTY_TYPE_CONVICT = 53,
            PENALTY_TYPE_PRISONER = 170,
            PENALTY_VIOLATION = 54,
            PENALTY_STATE = 60,
            PENALTY_STATE_BASE = 59,
            PENALTY_PROTEST_RESULT = 58,
            PENALTY_PROTESTER = 57,
            PENLATY_PROTEST_BASE = 56,
            EDUCATIONAL_COURSES_TYPE = 67,
            EDUCATIONAL_COURSES_ENG = 68,
            EDUCATIONAL_COURSES_REL = 70,
            WORKLOADS_TYPE = 62,
            WORKLOADS_TITLE = 117,
            WORKLOADS_ENG = 64,
            WORKLOADS_REL = 66,
            WORKLOADS_EMP = 63,
            OFFICIAL_VISITS_TYPE = 119,
            OFFICIAL_VISITS_POSITION = 120,
            PERSONAL_VISITS_TYPE = 122,
            PERSONAL_VISITS_PURPOSE = 123,
            PACHACKE_TYPE = 125,
            PACKAGE_CONTENT_TYPE = 126,
            PACKAGE_CONTENT_GOODS = 127,
            PACKAGE_CONTENT_MEASURES = 128,
            DEPARTURES_TYPE = 130,
            DEPARTURES_PURPOSE = 131,
            DEPARTURES_MED = 132,
            DEPARTURES_CASE = 134,
            DEPARTURES_MOVE = 135,
            DEPARTURES_HOSPITAL = 136,
            DEPARTURES_INVESTIGATE = 137,
            DEPARTURES_COURT = 138,
            TRANSFER = 139,
            TRANSFER_TYPE = 146,
            TRANSFER_PURPOSE = 147,
            TRANSFER_HOSPITAL = 140,
            TRANSFER_COURT = 141,
            TRANSFER_STATE = 142,
            TRANSFER_HOGEBUJARAN = 164,
            TRANSFER_END = 143,
            TRANSFER_EMERGENCY = 144,
            TRANSFER_QNNCHAKAN = 168,
            TRANSFER_QKHHOSPITAL = 167,
            TRANSFER_BJSHKAKANTEX = 166,
            TRANSFER_VARUYTTEX = 165,
            HEALTH_LIB = 151,
            MEDICAL_COMPLAINT = 152,
            MEDICAL_DIAGNOSIS = 153,
            MEDICAL_RESEARCH = 154,
            MEDICAL_HISTORY_RESULT = 155,
            AMNESTY_TYPE = 157,
            AMNESTY_RECIPIENT = 158,
            AMNESTY_SIGNATURE = 159
        }
    }
}
