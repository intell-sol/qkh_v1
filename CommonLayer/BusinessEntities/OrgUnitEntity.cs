﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    /// <summary>
    /// For Storing Organization Unit tree
    /// </summary>
    [Serializable]
    public class OrgUnitEntity
    {
        public OrgUnitEntity()
        {
            this.subItems = new List<OrgUnitEntity>();
            //Permissions = new List<PermToPosEntity>();
            this.VisibleOrgUnits = new List<int>();
        }
        public OrgUnitEntity ShallowCopy()
        {
            return (OrgUnitEntity) this.MemberwiseClone();
        }

        public OrgUnitEntity(int ParentID, int TypeID, bool Single, string Label, string Address)
        {
            this.ParentID = ParentID;
            this.TypeID = TypeID;
            this.Single = Single;
            this.Label = Label;
            this.Address = Address;

            subItems = new List<OrgUnitEntity>();
        }

        public OrgUnitEntity(int ID, int ParentID, int TypeID, bool Single, string Label, string Address, int? StateID, string Phone, string Fax, string Email, bool Editable, int? BindPositionID = null)
        {
            this.ID = ID;
            this.ParentID = ParentID;
            this.TypeID = TypeID;
            this.Single = Single;
            this.Label = Label;
            this.Address = Address;
            this.StateID = StateID;
            this.Phone = Phone;
            this.Fax = Fax;
            this.Email = Email;
            this.BindPositionID = BindPositionID;
            this.Editable = Editable;

            subItems = new List<OrgUnitEntity>();
        }

        public int ID { get; set; }

        public int ParentID { get; set; }
        public int? BindPositionID { get; set; }

        public int TypeID { get; set; }
        
        public bool Single { get; set; }

        public string Label { get; set; }
        public string Address { get; set; }
        public int? StateID { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }

        public bool Editable { get; set; }
        public int? SortLevel { get; set; }
        public List<OrgUnitEntity> subItems;

        public List<int> VisibleOrgUnits { get; set; }
        public List<PermEntity> Permissions { get; set; }
    }
}
