﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PackagesObjectEntity
    {
        public int? PrisonerID { get; set; }
        public List<PackagesEntity> Packages { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public PackagesObjectEntity()
        {
            this.PrisonerID = null;
            this.Packages = new List<PackagesEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }
        public PackagesObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.Packages = new List<PackagesEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }

    }
}
