﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class FilterPersonalVisitsEntity
    {
        public int? TypeLibItemID { get; set; }
        public int? ApproverEmployeeID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? PrisonerID { get; set; }
        public string FirstName { set; get; }
        public string MiddleName { set; get; }
        public string LastName { set; get; }
        public bool ArchiveStatus { get; set; }
        public bool? VisitStatus { get; set; }
        public List<int> OrgUnitIDList { set; get; }
        public int? OrgUnitID { get; set; }
        public int? CurrentPage { get; set; }
        public int? ViewCount { get; set; }
        public int? PrisonerType { get; set; }
        public Paging paging { get; set; }
        public FilterPersonalVisitsEntity()
        {
            this.ArchiveStatus = true;
        }
        public FilterPersonalVisitsEntity(int? TypeLibItemID = null,
            int? ApproverEmployeeID = null, DateTime? StartDate = null, DateTime? EndDate = null,
            int? PrisonerID = null, string FirstName = null, string MiddleName = null, string LastName = null,
            bool ArchiveStatus = true, bool? VisitStatus = null, List<int> OrgUnitIDList = null,
            int? CurrentPage = null, int? ViewCount = null, int? OrgUnitID = null, int? PrisonerType = null)
        {
            this.TypeLibItemID = TypeLibItemID;
            this.ApproverEmployeeID = ApproverEmployeeID;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.PrisonerID = PrisonerID;
            this.FirstName = FirstName;
            this.MiddleName = MiddleName;
            this.LastName = LastName;
            this.ArchiveStatus = ArchiveStatus;
            this.VisitStatus = VisitStatus;
            this.OrgUnitIDList = OrgUnitIDList;
            this.PrisonerType = PrisonerType;
            this.CurrentPage = CurrentPage;
            this.ViewCount = ViewCount;
            this.OrgUnitID = OrgUnitID;
        }

        [Serializable]
        public class Paging
        {
            public int CurrentPage { set; get; }
            public int TotalPage { set; get; }
            public int ViewCount { set; get; }
            public int TotalCount { set; get; }

            public Paging(int CurrentPage = 1, int TotalPage = 1, int ViewCount = 10, int TotalCount = 0)
            {
                this.CurrentPage = CurrentPage;
                this.TotalPage = TotalPage;
                this.ViewCount = ViewCount;
                this.TotalCount = TotalCount;
            }
        }
    }
}
