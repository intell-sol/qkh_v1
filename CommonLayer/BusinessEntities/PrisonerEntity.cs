﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PrisonerEntity 
    {
        public int? ID { set; get; }
        public int? PersonID { set; get; }
        public int? PersonalCaseNumber { set; get; }
        public int? Type { set; get; }
        public int? OrgUnitID { set; get; }
        public int? EmployeeID { set; get; }
        public bool? Status { set; get; }
        public bool? ArchiveStatus { get; set; }
        public bool? ArchiveData { get; set; }
        public bool? State { set; get; }
        public PersonEntity Person { set; get; }
        public MainDataEntity MainData { set; get; }
        public PhysicalDataEntity PhysicalData { set; get; }
        public FingerprintsTattoosScarsEntity FingerprintsTattoosScars { set; get; }
        public EducationsProfessionsEntity EducationsProfessions { set; get; }
        public MilitaryServiceEntity MilitaryService { set; get; }
        public AdoptionInformationEntity AdoptionInformation { set; get; }
        public CameraCardEntity CameraCard { set; get; }
        public ItemObjectEntity ItemsObjects { set; get; }
        public InjunctionsEntity Injunctions { set; get; }
        public FamilyRelativeEntity FamiltyRelative { set; get; }
        public PrisonerEntity()
        {
        }

        public PrisonerEntity(int? ID, int? PersonID, int? PersonalCaseNumber,  int? Type, bool Status = true, bool? State = null, bool? ArchiveStatus = null, bool ? ArchiveData = null)
        {
            this.ID = ID;
            this.PersonID = PersonID;
            this.PersonalCaseNumber = PersonalCaseNumber;
            this.Type = Type;
            this.Status = Status;
            this.State = State;
            this.ArchiveStatus = ArchiveStatus;
            this.ArchiveData = ArchiveData;
            Person = new PersonEntity();
            FamiltyRelative = new FamilyRelativeEntity();
        }

        public PrisonerEntity(int ID, bool State, bool? Status = null)
        {
            this.ID = ID;
            this.State = State;
            this.Status = Status;
        }

        public PrisonerEntity(int? ID, int? Type)
        {
            this.ID = ID;
            this.Type = Type;
        }
    }
}
