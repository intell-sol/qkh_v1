﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class EducationAwardsEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? LibItemID { set; get; }
        public bool? Status { set; get; }
        
        public EducationAwardsEntity()
        {
            ID = null;
            PrisonerID = null;
            LibItemID = null;
            Status = null;
        }
        public EducationAwardsEntity(int? ID, int? PrisonerID, int? LibItemID, bool? Status = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.LibItemID = LibItemID;
            this.Status = Status;
        }
    }
}
