﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class HeightWeightEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? PhysicalDataID { set; get; }
        public int? Height { set; get; }
        public int? Weight { set; get; }
        public DateTime? ModifyDate { set; get; }
        public bool? Status { set; get; }
        public bool? MergeStatus { get; set; }
        public HeightWeightEntity()
        {
        }
        public HeightWeightEntity(int? ID = null,int? PrisonerID = null,
            int? PhysicalDataID = null, int? Height = null, int? Weight = null,
            DateTime? ModifyDate = null, bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.PhysicalDataID = PhysicalDataID;
            this.Height = Height;
            this.Weight = Weight;
            this.ModifyDate = ModifyDate;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
    }
}
