﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class ConflictsEntity
    {

        public int? ID { set; get; }
        public int? PrisonerID { get; set; }
        public int? PersonID { get; set; }
        public string Description { get; set; }
        public bool? Status { get; set; }
        public string PersonalID { get; set; }
        public PersonEntity Person { get; set; }
        public bool? MergeStatus { get; set; }
        public ConflictsEntity()
        {
        }
        public ConflictsEntity(int? ID = null, int? PrisonerID = null, int? PersonID = null,
            string Description = null, bool? Status = null,bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.PersonID = PersonID;
            this.Description = Description;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }

    }
}
