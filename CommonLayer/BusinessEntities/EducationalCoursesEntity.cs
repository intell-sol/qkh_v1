﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class EducationalCoursesEntity
    {

        public int? ID { set; get; }
        public int? PrisonerID { get; set; }
        public int? TypeLibItemID { get; set; }
        public string TypeLibItemLabel { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? EngagementDate { get; set; }
        public int? EngagementBasisLibItemID { get; set; }
        public string EngagementBasisLibItemLabel { get; set; }
        public bool? EngagementState { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public int? ReleaseBasisLibItemID { get; set; }
        public string ReleaseBasisLibItemLabel { get; set; }
        public string ReleaseNote { get; set; }
        public string Note { get; set; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
        public EducationalCoursesEntity()
        {
        }
        public EducationalCoursesEntity(int? ID = null, int? PrisonerID = null, int? TypeLibItemID = null,
            string TypeLibItemLabel = null, DateTime? EndDate = null, DateTime? EngagementDate = null,
            int? EngagementBasisLibItemID = null, string EngagementBasisLibItemLabel = null,
            bool? EngagementState = null, DateTime? ReleaseDate = null,
            int? ReleaseBasisLibItemID = null, string ReleaseBasisLibItemLabel = null,
            string ReleaseNote = null, string Note = null, bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.TypeLibItemID = TypeLibItemID;
            this.TypeLibItemLabel = TypeLibItemLabel;
            this.EndDate = EndDate;
            this.EngagementBasisLibItemID = EngagementBasisLibItemID;
            this.ReleaseDate = ReleaseDate;
            this.EngagementDate = EngagementDate;
            this.EngagementBasisLibItemLabel = EngagementBasisLibItemLabel;
            this.EngagementState = EngagementState;
            this.ReleaseBasisLibItemID = ReleaseBasisLibItemID;
            this.ReleaseBasisLibItemLabel = ReleaseBasisLibItemLabel;
            this.ReleaseNote = ReleaseNote;
            this.Note = Note;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }

    }
}
