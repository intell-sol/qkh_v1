﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PermissionsGurdEntity //: ISerializable
    {
        public int? ID { get; set; }
        public int? PositionID { get; set; }
        public string PositionName { get; set; }
        public int? OrgUnitID { get; set; }
        public int? PermissionIDs { get; set; }
        public bool? Status { get; set; }
        public List<EmployeeEntity> Employees { get; set; }
        public PermissionsGurdEntity()
        {
            this.Employees = new List<EmployeeEntity>();
        }

        public PermissionsGurdEntity(int? ID = null,
                                     int? PositionID = null,
                                     int? OrgUnitID = null,
                                     int? PermissionIDs = null,
                                     string PositionName = null,
                                     bool? Status = null)
        {
            this.ID = ID;
            this.PositionID = PositionID;
            this.OrgUnitID = OrgUnitID;
            this.PositionName = PositionName;
            this.PermissionIDs = PermissionIDs;
            this.Status = Status;

            this.Employees = new List<EmployeeEntity>();

        }


        //public void GetObjectData(SerializationInfo info, StreamingContext context)
        //{
        //    info.AddValue("Employees", this.Employees);
        //}
    }
}
