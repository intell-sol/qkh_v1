﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class SentencingDataEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? OrgUnitID { set; get; }
        public int? SentencingDecisionLibItem_ID { set; get; }
        public string SentencingDecisionLibItem_Label { get; set; }
        public string SentencNumber { set; get; }
        public string Judge { set; get; }
        public DateTime? SentencingDate { set; get; }
        public DateTime? SentencingStartDate { set; get; }
        public DateTime? SentencingVerdictDate { set; get; }
        public int? CodeLibItem_ID { set; get; }
        public string CodeLibItem_Label { set; get; }
        public int? PenaltyDay { set; get; }
        public int? CloseCaseID { set; get; }
        public int? OpenCaseID { set; get; }
        public int? PenaltyMonth { set; get; }
        public int? PenaltyYear { set; get; }
        public DateTime? SentencingEndDate { set; get; }
        public DateTime? EarlyDate { get; set; }
        public int? EarlyType { get; set; }
        public int? SentenceArticleType { get; set; }
        public string Description { get; set; }
        public bool? State { get; set; }
        public bool? LowSuit { get; set; }
        public bool? LowSuitState { get; set; }
        public string LowSuitAmount { get; set; }
        public string LowSuitPayAmount { get; set; }
        public bool? FineHas { get; set; }
        public int? PropertyConfiscationLibItemID { get; set; }
        public string PropertyConfiscationLibItemLabel { get; set; }
        public string PropertyConfiscationValue { get; set; }
        public string FinePay { get; set; }
        public string Fine { get; set; }
        public bool? FineState { get; set; }
        public int? PenaltyLibItemID { get; set; }
        public int? PunishmentLibItemID { get; set; }
        public string PenaltyLibItemLabel { get; set; }
        public string PunishmentLibItemLabel { get; set; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
        public List<SentencingDataArticleLibsEntity> SentencingDataArticles { set; get; }
        public SentencingDataEntity()
        {
        }
        public SentencingDataEntity(int? ID = null, int? PrisonerID = null, int? OrgUnitID = null,
            int? SentencingDecisionLibItem_ID = null, string SentencNumber = null, 
            string Judge  = null, DateTime? SentencingDate = null, DateTime? SentencingStartDate = null,
            int? CodeLibItem_ID = null, bool? LowSuit = null, int? PropertyConfiscationLibItemID = null,
            string PropertyConfiscationLibItemLabel = null, string PropertyConfiscationValue = null,
            string Fine = null,
            int? PenaltyDay = null, int? PenaltyMonth = null,
            int? PenaltyYear = null, DateTime? SentencingVerdictDate = null, string LowSuitAmount = null,
            DateTime? SentencingEndDate = null, DateTime? EarlyDate = null, bool? LowSuitState = null,
            int? EarlyType = null, int? SentenceArticleType = null, bool? Status = null,
            string SentencingDecisionLibItem_Label = null, string CodeLibItem_Label = null,
            string Description = null, bool? State = null, bool? MergeStatus = null,
            int? CloseCaseID = null, int? OpenCaseID = null,
            int? PenaltyLibItemID = null, string PenaltyLibItemLabel = null, int?PunishmentLibItemID=null, string PunishmentLibItemLabel=null,
            string LowSuitPayAmount = null, bool? FineHas = null, string FinePay = null, bool? FineState = null)
        {
            this.ID = ID;
            this.OrgUnitID = OrgUnitID;
            this.PrisonerID = PrisonerID;
            this.LowSuit = LowSuit;
            this.PenaltyLibItemID = PenaltyLibItemID;
            this.LowSuitState = LowSuitState;
            this.PenaltyLibItemLabel = PenaltyLibItemLabel;
            this.PunishmentLibItemID = PunishmentLibItemID;
            this.PunishmentLibItemLabel = PunishmentLibItemLabel;
            this.LowSuitAmount = LowSuitAmount;
            this.PropertyConfiscationLibItemID = PropertyConfiscationLibItemID;
            this.PropertyConfiscationLibItemLabel = PropertyConfiscationLibItemLabel;
            this.SentencingDecisionLibItem_ID = SentencingDecisionLibItem_ID;
            this.SentencingDecisionLibItem_Label = SentencingDecisionLibItem_Label;
            this.CodeLibItem_Label = CodeLibItem_Label;
            this.Fine = Fine;
            this.SentencNumber = SentencNumber;
            this.PropertyConfiscationValue = PropertyConfiscationValue;
            this.Judge = Judge;
            this.FineState = FineState;
            this.FineHas = FineHas;
            this.SentencingDate = SentencingDate;
            this.SentencingVerdictDate = SentencingVerdictDate;
            this.SentencingStartDate = SentencingStartDate;
            this.CodeLibItem_ID = CodeLibItem_ID;
            this.FinePay = FinePay;
            this.PenaltyDay = PenaltyDay;
            this.CloseCaseID = CloseCaseID;
            this.OpenCaseID = OpenCaseID;
            this.State = State;
            this.MergeStatus = MergeStatus;
            this.PenaltyMonth = PenaltyMonth;
            this.PenaltyYear = PenaltyYear;
            this.SentencingEndDate = SentencingEndDate;
            this.LowSuitPayAmount = LowSuitPayAmount;
            this.EarlyDate = EarlyDate;
            this.EarlyType = EarlyType;
            this.SentenceArticleType = SentenceArticleType;
            this.Description = Description;
            this.Status = Status;
            SentencingDataArticles = new List<SentencingDataArticleLibsEntity>();
        }
    }
}
