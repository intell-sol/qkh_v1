﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class ArticlePercentReportEntity
    {
        public DateTime? Date { get; set; }
        public int? LibItemID { get; set; }
        public int? PrisonersCount { get; set; }
        public decimal? Percent { get; set; }
        public ArticlePercentReportEntity()
        {

        }
        public ArticlePercentReportEntity(DateTime? Date = null,
                                          int? LibItemID = null,
                                          int? PrisonersCount = null,
                                          decimal? Percent = null)
        {
            this.Date = Date;
            this.LibItemID = LibItemID;
            this.PrisonersCount = PrisonersCount;
            this.Percent = Percent;
        }
    }   
}
