﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class AmnestyEntity
    {
        public int? ID { set; get; }
        public string Number { set; get; }
        public int? TypeLibItemID { set; get; }
        public string Source { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public int? RecipientLibItemID { set; get; }
        public int? SignatoriesLibItemID { set; get; }
        public bool? State { set; get; }       
        public DateTime? SignDate { set; get; }
        public DateTime? EndDate { set; get; }
        public DateTime? ModifyDate { set; get; }
        public DateTime? AcceptDate { set; get; }
        public DateTime? CreationDate { set; get; }        
        public DateTime? EnterDate { set; get; }
        public List<AmnestyPointsEntity> AmnestyPoints { get; set; }
        public bool? Status { set; get; }
        public AmnestyEntity() {
        }
        public AmnestyEntity(int? ID = null, string Number = null,int? TypeLibItemID=null,string Source=null,string Name=null,
            string Description=null,int? RecipientLibItemID=null,int? SignatoriesLibItemID=null,bool? State=null,DateTime?
            SignDate=null,DateTime? EndDate = null, DateTime? ModifyDate = null,DateTime? AcceptDate=null, DateTime? CreationDate=null,DateTime? EnterDate=null,
            bool? Status = null)
        {
            this.ID = ID;
            this.Number = Number;
            this.TypeLibItemID = TypeLibItemID;
            this.Source = Source;
            this.Name = Name;
            this.Description = Description;
            this.RecipientLibItemID = RecipientLibItemID;
            this.SignatoriesLibItemID = SignatoriesLibItemID;
            this.State = State;
            this.SignDate = SignDate;
            this.EndDate = EndDate;
            this.AcceptDate = AcceptDate;
            this.ModifyDate = ModifyDate;
            this.CreationDate = CreationDate;
            this.EnterDate = EnterDate;
            this.Status = Status;
            AmnestyPoints = new List<AmnestyPointsEntity>();
        }

    }
}
