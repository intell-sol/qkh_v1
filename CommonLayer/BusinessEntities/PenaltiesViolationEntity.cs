﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PenaltiesViolationEntity
    {
        public int? ID { get; set; }
	    public int? PrisonerID { get; set; }
	    public int? PenaltyID { get; set; }
	    public int? LibItemID { get; set; }
        public bool? Status { get; set; }
        public PenaltiesViolationEntity()
        {
        }
        public PenaltiesViolationEntity(int? ID = null, int? PrisonerID = null, int? PenaltyID = null, int? LibItemID = null, bool? Status = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.PenaltyID = PenaltyID;
            this.LibItemID = LibItemID;
            this.Status = Status;
        }

    }
}
