﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class OfficialVisitReportEntity
    {
        public int? ID { get; set; }
        public int? ArrestCodeLibItemID { get; set; }
        public int? SentencCodeLibItemID { get; set; }
        public string OrgUnitLabel { get; set; }
        public string PrisonerName { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime? StartDate { get; set; }
        public string ArrestCodeLibItemLabel { get; set; }
        public int? ArrestArticle { get; set; }
        public string ArrestArticleName { get; set; }
        public string SentCodeLibItemLabel { get; set; }
        public int? SentArticle { get; set; }
        public int? ArrestDataID { get; set; }
        public int? SentencingDataID { get; set; }
        public string SentArticleName { get; set; }
        public string Sentence { get; set; }
        public DateTime? DetentionStart { get; set; }
        public string VisitPerson { get; set; }
        public string LivingAddress { get; set; }
        public OfficialVisitReportEntity()
        {

        }
        public OfficialVisitReportEntity(int? ID = null,
                                       int? ArrestCodeLibItemID = null,
                                       int? SentencCodeLibItemID = null,
                                    string OrgUnitLabel = null,
                                    string PrisonerName = null,
                                    DateTime? Birthday = null,
                                    DateTime? StartDate = null,
                                    string ArrestCodeLibItemLabel = null,
                                    int? ArrestArticle = null,
                                    string ArrestArticleName = null,
                                    string SentCodeLibItemLabel = null,
                                    int? SentArticle = null,
                                    int? ArrestDataID = null,
                                    int? SentencingDataID = null,
                                    string SentArticleName = null,
                                    string Sentence = null,
                                    DateTime? DetentionStart = null,
                                    string VisitPerson = null,
                                    string LivingAddress = null)
        {
            this.ID = ID;
            this.OrgUnitLabel = OrgUnitLabel;
            this.PrisonerName = PrisonerName;
            this.Birthday = Birthday;
            this.StartDate = StartDate;
            this.ArrestCodeLibItemLabel = ArrestCodeLibItemLabel;
            this.ArrestArticle = ArrestArticle;
            this.ArrestArticleName = ArrestArticleName;
            this.SentCodeLibItemLabel = SentCodeLibItemLabel;
            this.SentArticle = SentArticle;
            this.SentArticleName = SentArticleName;
            this.Sentence = Sentence;
            this.ArrestDataID = ArrestDataID;
            this.SentencingDataID = SentencingDataID;
            this.DetentionStart = DetentionStart;
            this.VisitPerson = VisitPerson;
            this.LivingAddress = LivingAddress;
            this.ArrestCodeLibItemID = ArrestCodeLibItemID;
            this.SentencCodeLibItemID = SentencCodeLibItemID;
        }
    }
}
