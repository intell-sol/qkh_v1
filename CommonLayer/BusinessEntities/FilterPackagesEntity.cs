﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class FilterPackagesEntity
    {
        public int? TypeLibItemID { get; set; }
        public int? TypeContentLibItemID { get; set; }
        public int? ApproveEmployeeID { get; set; }
        public int? PersonID { get; set; }
        public int? GoodsLibItemID { get; set; }
        public int? MeasureLibItemID { get; set; }
        public int? EmployeeID { get; set; }
        public string FirstName { set; get; }
        public string MiddleName { set; get; }
        public string LastName { set; get; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? PrisonerID { get; set; }
        public bool? ApproveStatus { get; set; }
        public bool? ApproveStatusAll { get; set; }
        public bool ArchiveStatus { get; set; }
        public List<int> OrgUnitIDList { set; get; }
        public int? OrgUnitID { get; set; }
        public int? CurrentPage { get; set; }
        public int? ViewCount { get; set; }
        public int? PrisonerType { get; set; }
        public Paging paging { get; set; }
        public FilterPackagesEntity()
        {
            this.ArchiveStatus = true;
        }
        public FilterPackagesEntity(int? TypeLibItemID = null, int? TypeContentLibItemID = null,
            int? ApproveEmployeeID = null, int? PersonID = null, int? GoodsLibItemID = null, int? MeasureLibItemID = null, DateTime? StartDate = null,
            int? EmployeeID = null, DateTime? EndDate = null,
            string FirstName = null, string MiddleName = null, string LastName = null,
            int? PrisonerID = null, bool ArchiveStatus = true, List<int> OrgUnitIDList = null,
            int? CurrentPage = null, int? ViewCount = null, int? OrgUnitID = null, int? PrisonerType = null, bool? ApproveStatus = null, bool ApproveStatusAll = true)
        {
            this.TypeLibItemID = TypeLibItemID;
            this.ApproveEmployeeID = ApproveEmployeeID;
            this.PersonID = PersonID;
            this.TypeContentLibItemID = TypeContentLibItemID;
            this.GoodsLibItemID = GoodsLibItemID;
            this.MeasureLibItemID = MeasureLibItemID;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.ApproveStatus = ApproveStatus;
            this.ApproveStatusAll = ApproveStatusAll;
            this.PrisonerID = PrisonerID;
            this.FirstName = FirstName;
            this.MiddleName = MiddleName;
            this.LastName = LastName;
            this.ArchiveStatus = ArchiveStatus;
            this.OrgUnitIDList = OrgUnitIDList;
            this.PrisonerType = PrisonerType;
            this.CurrentPage = CurrentPage;
            this.ViewCount = ViewCount;
            this.OrgUnitID = OrgUnitID;
        }

        [Serializable]
        public class Paging
        {
            public int CurrentPage { set; get; }
            public int TotalPage { set; get; }
            public int ViewCount { set; get; }
            public int TotalCount { set; get; }

            public Paging(int CurrentPage = 1, int TotalPage = 1, int ViewCount = 10, int TotalCount = 0)
            {
                this.CurrentPage = CurrentPage;
                this.TotalPage = TotalPage;
                this.ViewCount = ViewCount;
                this.TotalCount = TotalCount;
            }
        }
    }
}
