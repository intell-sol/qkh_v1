﻿using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PrisonAccessProtocolEntity
    {
        public int? ID { set; get; }
        public int? OrgUnitID { set; get; }
        public int? PrisonerID { set; get; }
        public int? RankLib_ID { set; get; }
        public int? EmployeeID { set; get; }
        public int? PrisonInstitutionTypeLib_ID { set; get; }
        public DateTime? PrisonAccessDate { set; get; }
        public string AdoptionBasis { set; get; }
        public string PhysicalExamnationResult { set; get; }
        public string PersonalSearchResults { set; get; }
        public string PPIC { set; get; }
        public string SPC { set; get; }
        public string Description { set; get; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }

        public PrisonAccessProtocolEntity()
        {
        }
        public PrisonAccessProtocolEntity(int? ID = null, int? PrisonerID = null,int? OrgUnitID = null, int? RankLib_ID = null, int? EmployeeID = null, 
            int? PrisonInstitutionTypeLib_ID = null, DateTime? PrisonAccessDate = null, string AdoptionBasis = null, string PhysicalExamnationResult = null, 
            string PersonalSearchResults = null, string PPIC = null, string SPC = null, string Description = null, bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.OrgUnitID = OrgUnitID;
            this.PrisonerID = PrisonerID;
            this.RankLib_ID = RankLib_ID;
            this.EmployeeID = EmployeeID;
            this.PrisonInstitutionTypeLib_ID = PrisonInstitutionTypeLib_ID;
            this.PrisonAccessDate = PrisonAccessDate;
            this.AdoptionBasis = AdoptionBasis;
            this.PhysicalExamnationResult = PhysicalExamnationResult;
            this.PersonalSearchResults = PersonalSearchResults;
            this.PPIC = PPIC;
            this.SPC = SPC;
            this.Description = Description;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
    }
}
