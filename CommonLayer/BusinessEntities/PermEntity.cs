﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;


namespace CommonLayer.BusinessEntities
{
    /// <summary>
    /// For Storing Permissions tree
    /// </summary>

    [Serializable]
    public class PermEntity
    {
        public PermEntity()
        {
            this.Approvers = new List<int>();
        }

        public PermEntity(int? ID, int? ParentID, string Name, string Title, bool Type, bool Status)
        {
            this.ID = ID;
            this.Name = Name;
            this.ParentID = ParentID;
            this.Title = Title;
            this.Type = Type;
            this.Status = Status;
            this.Approvers = new List<int>();
        }

        public int? ID { get; set; }

        public int? ParentID { get; set; }

        public string Name { get; set; }

        public string Title { get; set; }

        public bool Type { get; set; }

        public int PPType { get; set; }

        public bool Status { get; set; }

        public List<PermEntity> subItems { get; set; }
        public List<int> Approvers { get; set; }
    }

    public class PermEntityComparer : IEqualityComparer<PermEntity>
    {
        public bool Equals(PermEntity e1, PermEntity e2)
        {
            ////Check whether the compared objects reference the same data.
            //if (Object.ReferenceEquals(e1, e2)) return true;

            ////Check whether any of the compared objects is null.
            //if (Object.ReferenceEquals(e1, null) || Object.ReferenceEquals(e2, null))
            //    return false;

            return e1.ID.HasValue && e2.ID.HasValue && e1.ID.Value == e2.ID.Value;
        }

        public int GetHashCode(PermEntity pe)
        {
            return pe.ID.GetHashCode();
        }
    }

    public class PermEntityStrictComparer : IEqualityComparer<PermEntity>
    {
        public bool Equals(PermEntity e1, PermEntity e2)
        {
            e1.Approvers.Sort();
            e2.Approvers.Sort();
            return e1.ID.HasValue && e2.ID.HasValue && e1.ID.Value == e2.ID.Value && e1.PPType == e2.PPType && e1.Approvers.SequenceEqual(e2.Approvers);
        }

        public int GetHashCode(PermEntity pe)
        {
            return pe.ID.GetHashCode() ^ pe.PPType.GetHashCode();
        }
    }

    public enum PermissionsHardCodedIds
    {
        MainData = 2,
        PhysicalData = 3,
        FingerprintsTattoosScars = 4,
        FamilyRelative = 5,
        EducationsProfessions = 6,
        MilitaryService = 7,
        AdoptionInformation = 8,
        CameraCard = 9,
        ConflictPersons = 10,
        ItemsObjects = 11,
        Convicts = 13,
        Prisoners = 14,
        Encouragements=15,
        Penalties=16,
        EducationalCourses=17,
        Transfers = 28,
        Medicals=29,
        Medias=30,
        Dashboard=31,
        DataTerminate=33,
        DataBans=34,
        DataArchive=35,
        Amnesties=25,
        Reports=27,
        OfficialVisits = 19,
        PersonalVisits = 20,
        Packages = 21,
        Departures = 22,
        ViewLogs = 23,
        Permissions = 24,
        WorkLoads=18,
        Merge = 36,
        ArchivePrisoner = 37
    }
}
