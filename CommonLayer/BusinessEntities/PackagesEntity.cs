﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PackagesEntity
    {
        public int? ID { get; set; }
	    public int? PrisonerID { get; set; }
	    public int? TypeLibItemID { get; set; }
        public string TypeLibItemLabel { get; set; }
        public int? ApproveEmployeeID { get; set; }
        public string ApproveEmployeeName { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? AcceptDate { get; set; }
        public int? Weight { get; set; }
        public int? EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public int? PersonID { get; set; }
        public string PersonalID { get; set; }
        public string PersonName { get; set; }
        public bool? Status { get; set; }
        public bool? ApproveStatus { get; set; }
        public bool? MergeStatus { get; set; }
        public List<PackageContentEntity> ContentList { get; set; }
        public PersonEntity Person { get; set; }
        public PackagesEntity()
        {
        }
        public PackagesEntity(int? ID = null, int? PrisonerID = null,
            int? TypeLibItemID = null, string TypeLibItemLabel = null,
            int? ApproveEmployeeID = null, string ApproveEmployeeName = null, DateTime? Date = null, DateTime? AcceptDate = null,
          int? Weight=null, int? EmployeeID=null, string EmployeeName = null,
          int? PersonID = null,string PersonName = null, bool? Status=null, bool? ApproveStatus = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.TypeLibItemID = TypeLibItemID;
            this.TypeLibItemLabel = TypeLibItemLabel;
            this.ApproveEmployeeID = ApproveEmployeeID;
            this.ApproveEmployeeName = ApproveEmployeeName;
            this.Weight = Weight;
            this.EmployeeID = EmployeeID;
            this.EmployeeName = EmployeeName;
            this.ApproveStatus = ApproveStatus;
            this.PersonID = PersonID;
            this.PersonName = PersonName;
            this.Date = Date;
            this.AcceptDate = AcceptDate;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }

    }
}
