﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MedicalCountReportEntity
    {
        public DateTime? FilterDate { get; set; }
        public DateTime? FirstDate { get; set; }
        public DateTime? SecondDate { get; set; }
        public int? TotalInvalids { get; set; }
        public int? FirstClass { get; set; }
        public int? SecondClass { get; set; }
        public int? ThirdClass { get; set; }
        public int? Died { get; set; }
        public int? Disease { get; set; }
        public int? Ambulance { get; set; }
        public int? Static { get; set; }
        public int? Hospital { get; set; }
        public int? Hurry { get; set; }
        public int? DiedInHospital { get; set; }
        public int? Lab { get; set; }
        public MedicalCountReportEntity()
        {

        }
        public MedicalCountReportEntity(DateTime? FilterDate = null,
                                        DateTime? FirstDate = null,
                                        DateTime? SecondDate = null,
                                         int? TotalInvalids = null,
                                         int? FirstClass = null,
                                         int? SecondClass = null,
                                         int? ThirdClass =null,
                                         int? Died = null,
                                         int? Disease = null,
                                         int? Ambulance =null,
                                         int? Static = null,
                                         int? Hospital = null,
                                         int? Hurry = null,
                                         int? DiedInHospital = null,
                                         int? Lab = null)
        {
            this.FilterDate = FilterDate;
            this.FirstDate = FirstDate;
            this.SecondDate = SecondDate;
            this.TotalInvalids = TotalInvalids;
            this.FirstClass = FirstClass;
            this.SecondClass = SecondClass;
            this.ThirdClass = ThirdClass;
            this.Died = Died;
            this.Disease = Disease;
            this.Ambulance = Ambulance;
            this.Static = Static;
            this.Hospital = Hospital;
            this.Hurry = Hurry;
            this.DiedInHospital = DiedInHospital;
            this.Lab = Lab;
        }
    }
}
