﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PackagesDashboardEntity : PackagesEntity
    {
        public string PrisonerName { get; set; }
        public string TypeContentLibItemLabel { get; set; }
        public string Personal_ID { get; set; }
        public bool? ArchiveStatus { get; set; }
        public int? PrisonerType { get; set; }
        public PackagesDashboardEntity()
        {
        }
        public PackagesDashboardEntity(int? ID = null, int? PrisonerID = null, int? TypeLibItemID = null,
            string TypeLibItemLabel = null, int? ApproveEmployeeID = null, string ApproveEmployeeName = null,
            DateTime? Date = null,
            DateTime? AcceptDate = null,
          int? Weight = null, int? EmployeeID = null, string EmployeeName = null,
          int? PersonID = null, string PersonName = null, bool? Status = null, bool? ApproveStatus = null,
            string TypeContentLibItemLabel = null, string PrisonerName = null, string Personal_ID = null, bool? ArchiveStatus = null, int? PrisonerType = null) 
            :base(ID , PrisonerID , TypeLibItemID , TypeLibItemLabel , ApproveEmployeeID, ApproveEmployeeName, Date, AcceptDate,
          Weight , EmployeeID, EmployeeName, PersonID, PersonName, Status, ApproveStatus)
        {
            
            this.PrisonerName = PrisonerName;
            this.TypeContentLibItemLabel = TypeContentLibItemLabel;
            this.Personal_ID = Personal_ID;
            this.ArchiveStatus = ArchiveStatus;
            this.PrisonerType = PrisonerType;
        }
    }
}