﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class FilterOfficialVisitsEntity
    {
        public int? TypeLibItemID { get; set; }
        public int? PositionLibItemID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? PrisonerID { get; set; }
        public string FirstName { set; get; }
        public string MiddleName { set; get; }
        public string LastName { set; get; }
        public string VisitorFirstName { set; get; }
        public string VisitorMiddleName { set; get; }
        public string VisitorLastName { set; get; }
        public bool ArchiveStatus { get; set; }
        public List<int> OrgUnitIDList { set; get; }
        public int? OrgUnitID { get; set; }
        public int? CurrentPage { get; set; }
        public int? ViewCount { get; set; }
        public int? PrisonerType { get; set; }
        public Paging paging { get; set; }
        public FilterOfficialVisitsEntity()
        {
            this.ArchiveStatus = true;
        }
        public FilterOfficialVisitsEntity(int? TypeLibItemID = null, int? PositionLibItemID = null, DateTime? StartDate = null, DateTime? EndDate = null,
            int? PrisonerID = null, string FirstName = null, string MiddleName = null, string LastName = null, string VisitorFirstName = null, string VisitorMiddleName = null, string VisitorLastName = null,
            bool ArchiveStatus = true, List<int> OrgUnitIDList = null,
            int? CurrentPage = null, int? ViewCount = null, int? OrgUnitID = null, int? PrisonerType = null)
        {
            this.TypeLibItemID = TypeLibItemID;
            this.PositionLibItemID = PositionLibItemID;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.PrisonerID = PrisonerID;
            this.FirstName = FirstName;
            this.MiddleName = MiddleName;
            this.LastName = LastName;
            this.VisitorFirstName = VisitorFirstName;
            this.VisitorLastName = VisitorLastName;
            this.VisitorMiddleName = VisitorMiddleName;
            this.ArchiveStatus = ArchiveStatus;
            this.OrgUnitIDList = OrgUnitIDList;
            this.PrisonerType = PrisonerType;
            this.CurrentPage = CurrentPage;
            this.ViewCount = ViewCount;
            this.OrgUnitID = OrgUnitID;
        }

        [Serializable]
        public class Paging
        {
            public int CurrentPage { set; get; }
            public int TotalPage { set; get; }
            public int ViewCount { set; get; }
            public int TotalCount { set; get; }

            public Paging(int CurrentPage = 1, int TotalPage = 1, int ViewCount = 10, int TotalCount = 0)
            {
                this.CurrentPage = CurrentPage;
                this.TotalPage = TotalPage;
                this.ViewCount = ViewCount;
                this.TotalCount = TotalCount;
            }
        }
    }
}
