﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class ConflictsObjectEntity
    {
        public int? PrisonerID { get; set; }
        public List<ConflictsEntity> Conflicts { get; set; }
        public List<ConflictsEntity> ConflictsMain { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public ConflictsObjectEntity()
        {
            this.PrisonerID = null;
            this.Conflicts = new List<ConflictsEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }
        public ConflictsObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.Conflicts = new List<ConflictsEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }

    }
}
