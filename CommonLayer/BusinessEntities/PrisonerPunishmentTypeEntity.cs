﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PrisonerPunishmentTypeEntity
    {
        public int? ID { get; set; }
        public int? PrisonerID { get; set; }
        public int? PrisonerType { get; set; }
        public int? OrgUnitID { get; set; }
        public int? BaseLibItemID { get; set; }
        public string BaseLibItemLabel { get; set; }
        public int? TypeLibItemID { get; set; }
        public string TypeLibItemLabel { get; set; }
        public string Description { get; set; }
        public DateTime? AccessDate { get; set; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
        public PrisonerPunishmentTypeEntity()
        {

        }
        public PrisonerPunishmentTypeEntity(int? ID = null,
                                          int? PrisonerID = null,
                                          int? PrisonerType = null,
                                          int? OrgUnitID = null,
                                          int? BaseLibItemID = null,
                                          string BaseLibItemLabel = null,
                                          int? TypeLibItemID = null,
                                          string TypeLibItemLabel = null,
                                          string Description = null,
                                          DateTime? AccessDate = null,
                                          bool? Status = null,
                                          bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.PrisonerType = PrisonerType;
            this.OrgUnitID = OrgUnitID;
            this.BaseLibItemID = BaseLibItemID;
            this.BaseLibItemLabel = BaseLibItemLabel;
            this.TypeLibItemID = TypeLibItemID;
            this.TypeLibItemLabel = TypeLibItemLabel;
            this.Description = Description;
            this.AccessDate = AccessDate;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
    }
}
