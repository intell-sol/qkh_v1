﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MedicalDiagnosisEntity
    {
        public MedicalDiagnosisEntity()
        {

        }
        public MedicalDiagnosisEntity(int? ID = null, int? PrisonerID = null,
            int? MedicalHistoryID = null,
           int? NameLibItemID = null, string Description = null,
           string NameLibItemLabel = null, bool? State = null,
           bool? Status = null, DateTime? ModifyDate = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.MedicalHistoryID = MedicalHistoryID;
            this.NameLibItemID = NameLibItemID;
            this.Description = Description;
            this.NameLibItemLabel = NameLibItemLabel;
            this.State = State;
            this.Status = Status;
            this.ModifyDate = ModifyDate;
            this.MergeStatus = MergeStatus;
        }
        public int? ID { get; set; }
        public int? PrisonerID { get; set; }
        public int? MedicalHistoryID { get; set; }
        public int? NameLibItemID { get; set; }
        public string Description { get; set; }
        public string NameLibItemLabel { get; set; }
        public bool? State { get; set; }
        public bool? Status { get; set; }
        public DateTime? ModifyDate { get; set; }
        public bool? MergeStatus { get; set; }
    }
}
