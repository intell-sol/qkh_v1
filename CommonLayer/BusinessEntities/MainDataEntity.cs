﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MainDataEntity
    {
        public int? PrisonerID { set; get; }
        public List<PreviousConvictionsEntity> PreviousConvictions { set; get; }
        public List<AdditionalFileEntity> Images { set; get; }
        public List<AdditionalFileEntity> Files { set; get; }

        public MainDataEntity (int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            PreviousConvictions = new List<PreviousConvictionsEntity>();
            Images = new List<AdditionalFileEntity>();
            Files = new List<AdditionalFileEntity>();
        }
    }
}
