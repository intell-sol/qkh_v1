﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PackageReportEntity
    {
        public int? ID { get; set; }
        public int? ArrestCodeLibItemID { get; set; }
        public int? SentencCodeLibItemID { get; set; }
        public string OrgUnitLabel { get; set; }
        public string PrisonerName { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime? StartDate { get; set; }
        public string ArrestCodeLibItemLabel { get; set; }
        public int? ArrestArticle { get; set; }
        public string ArrestArticleName { get; set; }
        public string SentCodeLibItemLabel { get; set; }
        public int? SentArticle { get; set; }
        public string SentArticleName { get; set; }
        public string Sentence { get; set; }
        public int? ArrestDataID { get; set; }
        public int? SentencingDataID { get; set; }
        public DateTime? DetentionStart { get; set; }
        public DateTime? DeliveryDay { get; set; }
        public string PackagePerson { get; set; }
        public string PackagesType { get; set; }
        public int? Weight { get; set; }
        public string ApproveEmployee { get; set; }
        public string EmployeeName { get; set; }
        public PackageReportEntity()
        {

        }
        public PackageReportEntity( int? ID = null,
                                       int? ArrestCodeLibItemID = null,
                                       int? SentencCodeLibItemID = null,
                                    string OrgUnitLabel = null,
                                    string PrisonerName = null,
                                    DateTime? Birthday = null,
                                    DateTime? StartDate = null,
                                    string ArrestCodeLibItemLabel = null,
                                    int? ArrestArticle = null,
                                    string ArrestArticleName = null,
                                    string SentCodeLibItemLabel = null,
                                    int? SentArticle = null,
                                    string SentArticleName = null,
                                    string Sentence = null,
                                    int? ArrestDataID = null,
                                    int? SentencingDataID = null,
                                    DateTime? DetentionStart = null,
                                    DateTime? DeliveryDay = null,
                                    string PackagePerson = null,
                                    string PackagesType = null,
                                    int? Weight = null,
                                    string EmployeeName = null,
                                    string ApproveEmployee = null)
        {
            this.ID = ID;
            this.OrgUnitLabel = OrgUnitLabel;
            this.PrisonerName = PrisonerName;
            this.Birthday = Birthday;
            this.StartDate = StartDate;
            this.ArrestCodeLibItemLabel = ArrestCodeLibItemLabel;
            this.ArrestArticle = ArrestArticle;
            this.ArrestArticleName = ArrestArticleName;
            this.SentCodeLibItemLabel = SentCodeLibItemLabel;
            this.SentArticle = SentArticle;
            this.SentArticleName = SentArticleName;
            this.Sentence = Sentence;
            this.ArrestDataID = ArrestDataID;
            this.SentencingDataID = SentencingDataID;
            this.DetentionStart = DetentionStart;
            this.DeliveryDay = DeliveryDay;
            this.PackagePerson = PackagePerson;
            this.PackagesType = PackagesType;
            this.Weight = Weight;
            this.ApproveEmployee = ApproveEmployee;
            this.ArrestCodeLibItemID = ArrestCodeLibItemID;
            this.SentencCodeLibItemID = SentencCodeLibItemID;
            this.EmployeeName = EmployeeName;
        }

    }
}
