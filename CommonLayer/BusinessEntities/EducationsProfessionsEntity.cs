﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class EducationsProfessionsEntity
    { 
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? LibItemID { set; get; }
        public string Note { set; get; }
        public bool? Status { set; get; }
        public bool? MergeStatus { get; set; }
        public List<ProfessionEntity> Professions { set; get; }
        public List<InterestEntity> Interests { set; get; }
        public List<LanguageEntity> Languages { set; get; }
        public List<AbilitiesSkillsEntity> AbilitiesSkills { set; get; }
        public UniversityDegreeEntity UniversityDegree { set; get; }
        public List<AdditionalFileEntity> Files { set; get; }
        public EducationsProfessionsEntity()
        {
            this.ID = null;
            this.PrisonerID = null;
            this.Note = null;
            this.LibItemID = null;
            this.Status = null;
            Professions = new List<ProfessionEntity>();
            Interests = new List<InterestEntity>();
            AbilitiesSkills = new List<AbilitiesSkillsEntity>();
            Languages = new List<LanguageEntity>();
            UniversityDegree = new UniversityDegreeEntity();
            Files = new List<AdditionalFileEntity>();
        }
        public EducationsProfessionsEntity(int? ID = null, int? PrisonerID = null,
            int? LibItemID = null, string Note = null,
            bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.LibItemID = LibItemID;
            this.Note = Note;
            this.Status = Status;
            Professions = new List<ProfessionEntity>();
            Interests = new List<InterestEntity>();
            AbilitiesSkills = new List<AbilitiesSkillsEntity>();
            Languages = new List<LanguageEntity>();
            UniversityDegree = new UniversityDegreeEntity();
            Files = new List<AdditionalFileEntity>();
            this.MergeStatus = MergeStatus;
        }
    }
}
