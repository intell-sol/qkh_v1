﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class OfficialVisitsObjectEntity
    {
        public int? PrisonerID { get; set; }
        public List<OfficialVisitsEntity> Visits { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public OfficialVisitsObjectEntity()
        {
            this.PrisonerID = null;
            this.Visits = new List<OfficialVisitsEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }
        public OfficialVisitsObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.Visits = new List<OfficialVisitsEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }

    }
}
