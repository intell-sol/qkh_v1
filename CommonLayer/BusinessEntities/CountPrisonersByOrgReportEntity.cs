﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class CountPrisonersByOrgReportEntity
    {
        public DateTime? CurrentDate { get; set; }
        public DateTime? FirstDate { get; set; }
        public DateTime? SecondDate { get; set; }
        public string OrgName { get; set; }
        public int ? AllPrisoners { get; set; }
        public int ? FirstAllPrisoners { get; set; }
        public int ? SecondAllPrisoners { get; set; }
        public int ? CountPrisoners { get; set; }
        public int ? FirstCountPrisoners { get; set; }
        public int ? SecondCountPrisoners { get; set; }
        public CountPrisonersByOrgReportEntity()
        {

        }
        public CountPrisonersByOrgReportEntity( DateTime? CurrentDate = null,
                                         DateTime? FirstDate = null,
                                         DateTime? SecondDate = null,
                                         string OrgName = null,
                                         int ? AllPrisoners = null,
                                         int ? FirstAllPrisoners = null,
                                         int ? SecondAllPrisoners = null,
                                         int ? CountPrisoners = null,
                                         int ? FirstCountPrisoners = null,
                                         int ? SecondCountPrisoners = null)
        {
            this.CurrentDate = CurrentDate;
            this.FirstDate = FirstDate;
            this.SecondDate = SecondDate;
            this.OrgName = OrgName;
            this.AllPrisoners = AllPrisoners;
            this.CountPrisoners = CountPrisoners;
            this.FirstAllPrisoners = FirstAllPrisoners;
            this.FirstCountPrisoners = FirstCountPrisoners;
            this.SecondAllPrisoners = SecondAllPrisoners;
            this.SecondCountPrisoners = SecondCountPrisoners;
        }
    }
}
