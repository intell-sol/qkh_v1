﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MergeObjectEntity
    {
        public int? PrisonerID { get; set; }
        public MergeEntity currentModification { get; set; }
        public List<MergeEntity> Merge { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public MergeObjectEntity()
        {
            this.PrisonerID = null;
            this.Merge= new List<MergeEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }
        public MergeObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.Merge = new List<MergeEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }

    }
}
