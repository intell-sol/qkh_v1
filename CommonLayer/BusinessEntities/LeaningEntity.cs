﻿using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class LeaningEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? TypeLibItem_ID { set; get; }
        public int? SubTypeLibItem_ID { set; get; }
        public DateTime? Date { set; get; }
        public string Description { set; get; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
        public LeaningEntity() {

        }
        public LeaningEntity(int? ID = null, int? PrisonerID = null,
            int? TypeLibItem_ID = null, int? SubTypeLibItem_ID = null,
            DateTime? Date = null, string Description = null, 
            bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.TypeLibItem_ID = TypeLibItem_ID;
            this.SubTypeLibItem_ID = SubTypeLibItem_ID;
            this.Date = Date;
            this.Description = Description;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
    }
}
