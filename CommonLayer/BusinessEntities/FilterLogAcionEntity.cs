﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class FilterLogAcionEntity
    {
        public int? PositionID { get; set; }
        public int? EmployeeID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public int? CurrentPage { get; set; }
        public int? ViewCount { get; set; }

        public Paging paging { get; set; }

        public FilterLogAcionEntity()
        {
        }

        public FilterLogAcionEntity(int? PositionID = null, int? EmployeeID = null, 
            DateTime? StartDate = null, DateTime? EndDate = null,
            int? CurrentPage = null, int? ViewCount = null)
        {
            this.PositionID = PositionID;
            this.EmployeeID = EmployeeID;
            this.StartDate = StartDate;
            this.EndDate = EndDate;

            this.CurrentPage = CurrentPage;
            this.ViewCount = ViewCount;
        }

        [Serializable]
        public class Paging
        {
            public int CurrentPage { set; get; }
            public int TotalPage { set; get; }
            public int ViewCount { set; get; }
            public int TotalCount { set; get; }

            public Paging(int CurrentPage = 1, int TotalPage = 1, int ViewCount = 10, int TotalCount = 0)
            {
                this.CurrentPage = CurrentPage;
                this.TotalPage = TotalPage;
                this.ViewCount = ViewCount;
                this.TotalCount = TotalCount;
            }
        }
    }
}
