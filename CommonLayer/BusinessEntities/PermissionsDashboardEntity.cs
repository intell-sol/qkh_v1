﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PermissionsDashboardEntity
    {
        public PermissionsDashboardEntity()
        {
            this.Employees = new List<EmployeeEntity>();
        }

        public OrgUnitEntity Position { get; set; }

        public List<EmployeeEntity> Employees { get; set; }        

        public int? PermApproveID { get; set;}

        public int? PermissionID { get; set; }

        public int? PermToPosID { get; set; }
    }
}
