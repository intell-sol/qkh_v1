﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class AmnestyObjectEntity
    {
        public int? PrisonerID { get; set; }
        public List<AmnestyEntity> Amnesties { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public AmnestyObjectEntity()
        {
            this.PrisonerID = null;
            this.Amnesties = new List<AmnestyEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }
        public AmnestyObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.Amnesties = new List<AmnestyEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }

    }
}
