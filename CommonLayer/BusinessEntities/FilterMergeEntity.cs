﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class FilterMergeEntity
    {
        public int? TypeLibItemID { get; set; }
        public int? EngagementBasisLibItemID { get; set; }
        public int? ReleaseBasisLibItemID { get; set; }
        public int? EmployerLibItemID { get; set; }
        public int? JobTypeLibItemID { get; set; }
        public int? WorkTitleLibItemID { get; set; }
        public bool? EngagementState { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? EngagementDate { get; set; }
        public int? PrisonerID { get; set; }
        public bool ArchiveStatus { get; set; }
        public List<int> OrgUnitIDList { set; get; }
        public int? OrgUnitID { get; set; }
        public int? CurrentPage { get; set; }
        public int? ViewCount { get; set; }
        public int? PrisonerType { get; set; }
        public Paging paging { get; set; }
        public FilterMergeEntity()
        {
            this.ArchiveStatus = true;
        }
        public FilterMergeEntity(int? TypeLibItemID = null, int? EngagementBasisLibItemID = null,
            int? JobTypeLibItemID = null, DateTime? StartDate = null, DateTime? EngagementDate = null, 
            int? EmployerLibItemID = null, int? ReleaseBasisLibItemID = null, int? WorkTitleLibItemID = null,
            DateTime? EndDate = null, bool? EngagementState = null,
            int? PrisonerID = null, int? EmployeerID = null, bool ArchiveStatus = true, List<int> OrgUnitIDList = null,
            int? CurrentPage = null, int? ViewCount = null, int? OrgUnitID = null, int? PrisonerType = null)
        {
            this.TypeLibItemID = TypeLibItemID;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.EngagementDate = EngagementDate;
            this.PrisonerID = PrisonerID;
            this.ArchiveStatus = ArchiveStatus;
            this.OrgUnitIDList = OrgUnitIDList;
            this.JobTypeLibItemID = JobTypeLibItemID;
            this.EmployerLibItemID = EmployerLibItemID;
            this.EngagementState = EngagementState;
            this.WorkTitleLibItemID = WorkTitleLibItemID;
            this.ReleaseBasisLibItemID = ReleaseBasisLibItemID;
            this.EngagementBasisLibItemID = EngagementBasisLibItemID;
            this.PrisonerType = PrisonerType;
            this.CurrentPage = CurrentPage;
            this.ViewCount = ViewCount;
            this.OrgUnitID = OrgUnitID;
        }

        [Serializable]
        public class Paging
        {
            public int CurrentPage { set; get; }
            public int TotalPage { set; get; }
            public int ViewCount { set; get; }
            public int TotalCount { set; get; }

            public Paging(int CurrentPage = 1, int TotalPage = 1, int ViewCount = 10, int TotalCount = 0)
            {
                this.CurrentPage = CurrentPage;
                this.TotalPage = TotalPage;
                this.ViewCount = ViewCount;
                this.TotalCount = TotalCount;
            }
        }
    }
}