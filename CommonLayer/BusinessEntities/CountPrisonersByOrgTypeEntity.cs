﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class CountPrisonersByOrgTypeEntity
    { 
     public DateTime? CurrentDate { get; set; }
    public DateTime? FirstDate { get; set; }
    public int? CloseType { get; set; }
    public int? HalfCloseType { get; set; }
    public int? HalfOpenType { get; set; }
    public int? OpenType { get; set; }
    public int? Female { get; set; }
    public int? Nationality { get; set; }
    public int? Underage { get; set; }
    public int? NotBig { get; set; }
    public int? Mid { get; set; }
    public int? Heavy { get; set; }
    public int? Grave { get; set; }
        public CountPrisonersByOrgTypeEntity()
    {

    }
    public CountPrisonersByOrgTypeEntity(DateTime? CurrentDate = null,
                                     DateTime? FirstDate = null,
                                     int? CloseType = null,
                                     int? HalfCloseType = null,
                                     int? HalfOpenType = null,
                                     int? OpenType = null,
                                     int? Female = null,
                                     int? Underage = null,
                                     int? NotBig = null,
                                     int? Mid = null,
                                     int? Heavy = null,
                                     int? Grave = null,
                                     int? Nationality = null)
        {
        this.CurrentDate = CurrentDate;
        this.FirstDate = FirstDate;
        this.CloseType = CloseType;
        this.HalfCloseType = HalfCloseType;
        this.HalfOpenType = HalfOpenType;
        this.OpenType = OpenType;
        this.Female = Female;
        this.Nationality = Nationality;
        this.Underage = Underage;
        this.NotBig = NotBig;
        this.Mid = Mid;
        this.Heavy = Heavy;
        this.Grave = Grave;
        }
    }
}
