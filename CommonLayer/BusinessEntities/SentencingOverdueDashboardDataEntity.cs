﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class SentencingOverdueDashboardDataEntity
    {
        public int? PrisonerID { get; set; }
        public string Personal_ID { get; set; }
        public DateTime? SentencingStartDate { get; set; }
        public DateTime? SentencingEndDate { get; set; }
        public string PrisonerName { get; set; }
        public PersonEntity Persons { get; set; }
        public PrisonerEntity Prisoners { get; set; }

        public SentencingOverdueDashboardDataEntity()
        {
        }
        public SentencingOverdueDashboardDataEntity(int? PrisonerID = null, string Personal_ID = null, DateTime? SentencingStartDate = null, DateTime? SentencingEndDate = null, string PrisonerName = null)
        {
            this.PrisonerID = PrisonerID;
            this.Personal_ID = Personal_ID;
            this.SentencingStartDate = SentencingStartDate;
            this.SentencingEndDate = SentencingEndDate;
            this.PrisonerName = PrisonerName;
            Persons = new PersonEntity();
            Prisoners = new PrisonerEntity();
        }
    }
}
