﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class DepartureObjectEntity
    {
        public int? PrisonerID { get; set; }
        public List<DepartureEntity> Departures { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public DepartureObjectEntity()
        {
            this.PrisonerID = null;
            this.Departures = new List<DepartureEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }
        public DepartureObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.Departures = new List<DepartureEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }

    }
}
