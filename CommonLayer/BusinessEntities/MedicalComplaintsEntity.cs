﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MedicalComplaintsEntity
    {
        public MedicalComplaintsEntity()
        {

        }
        public MedicalComplaintsEntity(int? ID = null, int? PrisonerID = null, int? MedicalHistoryID = null,
           int? ComplaintsLibItemID = null, string ComplaintsLibItemLabel = null, string Description = null,
           DateTime? ModifyDate = null, bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.MedicalHistoryID = MedicalHistoryID;
            this.ComplaintsLibItemID = ComplaintsLibItemID;
            this.Description = Description;
            this.Status = Status;
            this.ComplaintsLibItemLabel = ComplaintsLibItemLabel;
            this.ModifyDate = ModifyDate;
            this.MergeStatus = MergeStatus;
        }
        public int? ID { get; set; }
        public int? PrisonerID { get; set; }
        public int? MedicalHistoryID { get; set; }
        public int? ComplaintsLibItemID { get; set; }
        public string ComplaintsLibItemLabel { get; set; }
        public string Description { get; set; }
        public bool? Status { get; set; }
        public DateTime? ModifyDate { get; set; }
        public bool? MergeStatus { get; set; }
    }
}
