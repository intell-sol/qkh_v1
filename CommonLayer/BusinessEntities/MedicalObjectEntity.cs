﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MedicalObjectEntity
    {
        public int? PrisonerID { get; set; }
        public MedicalPrimaryEntity primaryEntity { get; set; }
        public List<MedicalComplaintsEntity> MedicalComplaints { get; set; }
        public List<MedicalExternalExaminationEntity> MedicalExternalExaminations { get; set; }
        public List<MedicalDiagnosisEntity> MedicalDiagnosis { get; set; }
        public List<MedicalHistoryEntity> MedicalHistories { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public MedicalObjectEntity()
        {
            this.PrisonerID = null;
            this.Files = new List<AdditionalFileEntity>();
        }
        public MedicalObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.Files = new List<AdditionalFileEntity>();
        }

    }
}
