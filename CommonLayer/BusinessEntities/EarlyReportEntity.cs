﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class EarlyReportEntity
    {
        public int? OrgUnitID { get; set; }
        public List<int> OrgUnitList { set; get; }
        public DateTime? Date { get; set; }
        public int? Number { get; set; }
        public string PrisonerName { get; set; }
        public DateTime? BirthDay { get; set; }
        public string PreviousConviction { get; set; }
        public string PresentConviction { get; set; }
        public DateTime? SentencingDate { get; set; }
        public DateTime? StartDate { get; set; }
        public string Encouragements { get; set; }
        public string PenaltyName { get; set; }
        public DateTime? CloseDate { get; set; }
        public string OrgUnits { get; set; }
        public DateTime? FilterDate { get; set; }
        public EarlyReportEntity()
        {

        }
        public EarlyReportEntity(int? OrgUnitID = null,
                                 DateTime? Date = null, 
                                 int? Number=null,
                                 string PrisonerName=null,
                                 DateTime? BirthDay=null,
                                 string PreviousConviction=null,
                                 string PresentConviction=null,
                                 DateTime? SentencingDate=null,
                                 DateTime? StartDate = null,
                                 string Encouragements=null,
                                 string PenaltyName=null,
                                 bool column_visible = true,
                                 DateTime? CloseDate = null,
                                 string OrgUnits = null,
                                 DateTime? FilterDate = null,
                                 List<int> OrgUnitList = null)
        {
            this.OrgUnitID = OrgUnitID;
            this.Date = Date;
            this.Number = Number;
            this.PrisonerName = PrisonerName;
            this.BirthDay = BirthDay;
            this.PreviousConviction = PreviousConviction;
            this.PresentConviction = PresentConviction;
            this.SentencingDate = SentencingDate;
            this.Encouragements = Encouragements;
            this.PenaltyName = PenaltyName;
            this.StartDate = StartDate;
            this.FilterDate = FilterDate;
            this.OrgUnits = OrgUnits;
            this.CloseDate = CloseDate;
            OrgUnitList = new List<int>();
        }
    }
}
