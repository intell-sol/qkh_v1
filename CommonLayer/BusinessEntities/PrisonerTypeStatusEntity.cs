﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PrisonerTypeStatusEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get;} 
        public int? OrgUnitID { set; get; }
        public int? PrisonerType { set; get; }
        public DateTime? Date { set; get; }
        public string Description { set; get; }
        public bool? Status { get;set; }
        public bool? MergeStatus { get;set; }
        public PrisonerTypeStatusEntity()
        {
        }
        public PrisonerTypeStatusEntity(int? ID = null, int? PrisonerID = null, int? OrgUnitID = null,
            int? PrisonerType = null, DateTime? Date = null, string Description = null, bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.OrgUnitID = OrgUnitID;
            this.PrisonerID= PrisonerID;
            this.OrgUnitID = OrgUnitID;
            this.PrisonerType = PrisonerType;
            this.Date = Date;
            this.Description = Description;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
    }
}
