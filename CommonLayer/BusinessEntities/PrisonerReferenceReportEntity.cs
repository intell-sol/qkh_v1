﻿using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable()]
    public class PrisonerReferenceReportEntity
    {
        public int? PrisonerID { get; set; }
        public string OrgUnitName { get; set; }
        public string PrisonerName { get; set; }
        public int? FileID { get; set; }
        public Byte[] FilePath { get; set; }
        public DateTime? BirthDay { get; set; }
        public string RegAddress { get; set; }
        public string Nationality { get; set; }
        public string Citizenship { get; set; }
        public string EducationName { get; set; }
        public string FamilyStatus { get; set; }
        public string LivingAddress { get; set; }
        public string PreviousConviction { get; set; }
        public string PresentSentencingName { get; set; }
        public DateTime? SentencingVerdictDate { get; set; }
        public DateTime? SentencingStartDate { get; set; }
        public string Description { get; set; }
        public DateTime? EarlyDate { get; set; }
        public string EarlyType { get; set; }
        public string PenaltyName { get; set; }
        public string EncouragementsName { get; set; }
        public string Passports { get; set; }
        public string Spouses { get; set; }
        public string Parents { get; set; }
        public string Children { get; set; }
        public string SisterBrother { get; set; }
        public string Transfers { get; set; }
        public PrisonerReferenceReportEntity()
        {

        }
        public PrisonerReferenceReportEntity(
                                             int? PrisonerID = null,
                                             string OrgUnitName = null,
                                             int? FileID = null,
                                             Byte[] FilePath = null,
                                             string PrisonerName = null,
                                             DateTime? BirthDay = null,
                                             string RegAddress = null,
                                             string Nationality = null,
                                             string Citizenship = null,
                                             string EducationName = null,
                                             string FamilyStatus = null,
                                             string LivingAddress = null,
                                             string PreviousConviction = null,
                                             string PresentSentencingName = null,
                                             DateTime? SentencingVerdictDate = null,
                                             DateTime? SentencingStartDate = null,
                                             string Description = null,
                                             DateTime? EarlyDate = null,
                                             string EarlyType = null,
                                             string PenaltyName = null,
                                             string EncouragementsName = null,
                                             string Passports = null,
                                             string Spouses = null,
                                             string Parents = null,
                                             string Children = null,
                                             string SisterBrother = null,
                                             string Transfers = null)
        {
            this.PrisonerID = PrisonerID;
            this.OrgUnitName = OrgUnitName;
            this.FileID = FileID;
            this.FilePath = FilePath;
            this.PrisonerName = PrisonerName;
            this.BirthDay = BirthDay;
            this.RegAddress = RegAddress;
            this.Citizenship = Citizenship;
            this.Nationality = Nationality;
            this.EducationName = EducationName;
            this.FamilyStatus = FamilyStatus;
            this.LivingAddress = LivingAddress;
            this.PreviousConviction = PreviousConviction;
            this.PresentSentencingName = PresentSentencingName;
            this.SentencingVerdictDate = SentencingVerdictDate;
            this.SentencingStartDate = SentencingStartDate;
            this.Description = Description;
            this.EarlyDate = EarlyDate;
            this.EarlyType = EarlyType;
            this.PenaltyName = PenaltyName;
            this.EncouragementsName = EncouragementsName;
            this.Passports = Passports;
            this.Spouses = Spouses;
            this.Parents = Parents;
            this.Children = Children;
            this.SisterBrother = SisterBrother;
            this.Transfers = Transfers;
            this.Nationality = Nationality;
        }
    }
}
