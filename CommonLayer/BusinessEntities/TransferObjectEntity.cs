﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class TransferObjectEntity
    {
        public int? PrisonerID { get; set; }
        public int? PersonID { get; set; }
        public List<TransferEntity> Transfers { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public TransferObjectEntity()
        {
            this.PrisonerID = null;
            this.Transfers = new List<TransferEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }
        public TransferObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.PersonID = PersonID;
            this.Transfers = new List<TransferEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }

    }
}
