﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class WarInvolvedEntity
    {
        public WarInvolvedEntity()
        {
            ID = null;
            PrisonerID = null;
            LibItemID = null;
            Status = null;
        }

        public WarInvolvedEntity(int? ID = null, int? PrisonerID = null, int? LibItemID = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.LibItemID = LibItemID;
        }

        public int? ID { get; set; }

        public int? PrisonerID { get; set; }

        public int? LibItemID { get; set; }

        public bool? Status { get; set; }
    }
}
