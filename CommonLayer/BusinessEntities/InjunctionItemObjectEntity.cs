﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class InjunctionItemObjectEntity
    {
        public int? ID { set; get; }
        public int? InjunctionID { set; get; }
        public int? LibItem_ID { set; get; }
        public string LibItem_Label { set; get; }
        public bool? Status { set; get; }
        public InjunctionItemObjectEntity() {
        }
        public InjunctionItemObjectEntity(int? ID = null, int? InjunctionID = null, int? LibItem_ID = null, bool? Status = null,
            string LibItem_Label = null)
        {
            this.ID = ID;
            this.InjunctionID = InjunctionID;
            this.LibItem_ID = LibItem_ID;
            this.LibItem_Label = LibItem_Label;
            this.Status = Status;
        }
    }
}
