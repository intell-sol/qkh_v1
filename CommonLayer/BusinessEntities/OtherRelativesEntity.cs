﻿
using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class OtherRelativesEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? PersonID { set; get; }
        public int? RelationLibItemID { set; get; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
        public PersonEntity Person { get; set; }

        public OtherRelativesEntity()
        {
        }
        public OtherRelativesEntity(int? ID = null, int? PersonID = null,
            int? PrisonerID = null, int? RelationLibItemID = null,
            bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PersonID = PersonID;
            this.PrisonerID = PrisonerID;
            this.RelationLibItemID = RelationLibItemID;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
    }
    
}
