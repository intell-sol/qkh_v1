﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MedicalResearchEntity
    {
        public MedicalResearchEntity()
        {

        }
        public MedicalResearchEntity(int? ID = null, int? PrisonerID = null, int? MedicalHistoryID = null,
           string ResearchLibItemLabel = null, string MedicalHistoryName = null,
           string Description = null, int? NameLibItemID = null,
           bool? Status = null, DateTime? ModifyDate = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.MedicalHistoryID = MedicalHistoryID;
            this.ResearchLibItemLabel = ResearchLibItemLabel;
            this.MedicalHistoryName = MedicalHistoryName;
            this.Description = Description;
            this.NameLibItemID = NameLibItemID;
            this.Status = Status;
            this.ModifyDate = ModifyDate;
            this.MergeStatus = MergeStatus;
        }
        public int? ID { get; set; }
        public int? PrisonerID { get; set; }
        public int? MedicalHistoryID { get; set; }
        public int? NameLibItemID { get; set; }
        public string ResearchLibItemLabel { get; set; }
        public string MedicalHistoryName { get; set; }
        public string Description { get; set; }
        public bool? Status { get; set; }
        public DateTime? ModifyDate { get; set; }
        public bool? MergeStatus { get; set; }
    }
}
