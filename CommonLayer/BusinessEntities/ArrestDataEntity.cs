﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class ArrestDataEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? OrgUnitID { set; get; }
        public int? ArrestLib_ID { set; get; }
        public string VerdictNumber { set; get; }
        public string Judge { set; get; }
        public DateTime? VerdictDate { set; get; }
        public DateTime? DetentionStart { set; get; }
        public DateTime? DetentionEnd { set; get; }
        public string Description { get; set; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
        public int? CloseCaseID { get; set; }
        public int? OpenCaseID { get; set; }
        public int? CodeLibItem_ID { get; set; }
        public string CodeNumber { get; set; }
        public List<ArrestDataProceedingEntity> ArrestDataProceedings { get; set; }
        public List<SentencingDataArticleLibsEntity> SentencingDataArticles { set; get; }

        public ArrestDataEntity()
        {
            ArrestDataProceedings = new List<ArrestDataProceedingEntity>();
        }
        public ArrestDataEntity(int? ID = null, int? PrisonerID = null, int? OrgUnitID = null, int? ArrestLib_ID = null, 
            string VerdictNumber = null, string Judge = null, DateTime? VerdictDate = null, string Description = null,
            DateTime? DetentionStart = null, DateTime? DetentionEnd = null, bool? Status = null, bool? MergeStatus = null,
            int? CloseCaseID = null, int? OpenCaseID = null, int? CodeLibItem_ID = null, string CodeNumber = null)
        {
            this.ID = ID;
            this.OrgUnitID = OrgUnitID;
            this.CloseCaseID = CloseCaseID;
            this.OpenCaseID = OpenCaseID;
            this.PrisonerID = PrisonerID;
            this.ArrestLib_ID = ArrestLib_ID;
            this.VerdictNumber = VerdictNumber;
            this.Judge = Judge;
            this.VerdictDate = VerdictDate;
            this.DetentionStart = DetentionStart;
            this.CodeLibItem_ID = CodeLibItem_ID;
            this.CodeNumber = CodeNumber;
            this.DetentionEnd = DetentionEnd;
            this.Description = Description;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
    }
}
