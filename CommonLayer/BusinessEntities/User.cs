﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class BEUser
    {
        public BEUser()
        {

        }
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMail { get; set; }
        public string Phone { get; set; }
        public string UserIcon { get; set; }
        public string Position { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int Status { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? LastLoginDate { get; set; }

        public int PositionOrgUnitID { get; set; }
        public int SelectedOrgUnitID { get; set; }
        public Dictionary<int, string> VisibleOrgUnits { get; set; }
        public List<PermEntity> Permissions { get; set; }
        public List<PermissionsGurdEntity> PermissionGuards { get; set; }
    }
}
