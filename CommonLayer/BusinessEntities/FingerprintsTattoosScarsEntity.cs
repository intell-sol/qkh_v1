﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class FingerprintsTattoosScarsEntity
    {
        public List<FingerprintsTattoosScarsItemEntity> Fingerprints { set; get; }
        public List<FingerprintsTattoosScarsItemEntity> Tattoos { set; get; }
        public List<FingerprintsTattoosScarsItemEntity> Scars { set; get; }
    }
    public class FingerprintsTattoosScarsItemEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public int? FileID { set; get; }
        public bool? State { set; get; }
        public bool? Status { set; get; }
        public int? TypeID { set; get; }
        public DateTime? ModifyDate { set; get; }
        public bool? MergeStatus { get; set; }
        public FingerprintsTattoosScarsItemEntity() {
        }
        public FingerprintsTattoosScarsItemEntity(int? ID = null, int? PrisonerID = null,
            string Name = null, string Description = null,
            int? TypeID = null,int? FileID = null, DateTime? ModifyDate = null, 
            bool? State = null, bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.Name = Name;
            this.Description = Description;
            this.FileID = FileID;
            this.TypeID = TypeID;
            this.ModifyDate = ModifyDate;
            this.State = State;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
    }


}
