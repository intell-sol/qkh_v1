﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class AdoptionInformationEntity
    {
        public List<ArrestDataEntity> ArrestData { set; get; }
        public List<PrisonAccessProtocolEntity> PrisonAccessProtocol { set; get; }
        public List<PrisonerTypeStatusEntity> PrisonerTypeList{ set; get; }
        public List<PrisonerPunishmentTypeEntity> PrisonerPunishmentTypeList { set; get; }
        public List<SentencingDataEntity> SentencingData { set; get; }
        public List<AdditionalFileEntity> ArrestFiles { set; get; }
        public List<AdditionalFileEntity> Files { set; get; }

        public AdoptionInformationEntity()
        {
            ArrestData = new List<ArrestDataEntity>();
            PrisonAccessProtocol = new List<PrisonAccessProtocolEntity>();
            SentencingData = new List<SentencingDataEntity>();
        }
    }
}
