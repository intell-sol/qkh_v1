﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PersonalDescriptionReportEntity
    {
        public int? PrisonerID { get; set; }
        public string PrisonerName { get; set; }
        public DateTime? BirthDay { get; set; }
        public string RegAddress { get; set; }
        public string Nationality { get; set; }
        public string Citizenship { get; set; }
        public string LivingAddress { get; set; }
        public string FamilyStatus { get; set; }
        public string EducationName { get; set; }
        public string ProfessionsName { get; set; }
        public string WorkName { get; set; }
        public string ArmyName { get; set; }
        public string HealthType { get; set; }
        public string PreviousConviction { get; set; }
        public string PresentConviction { get; set; }
        public string PresentConvDuration { get; set; }
        public string Fine { get; set; }
        public DateTime? SentencingStartDate { get; set; }
        public DateTime? SentencingEndDate { get; set; }
        public DateTime? EarlyDate { get; set; }
        public string EarlyType { get; set; }
        public string EarlyCurentType { get; set; }
        public DateTime? PrisonAccessDate { get; set; }
        public string OrgType { get; set; }
        public string LowSuit { get; set; }
        public string LowSuitState { get; set; }
        public string LowSuitAmount { get; set; }
        public string PenaltyName { get; set; }
        public string EncouragementsName { get; set; }
        public DateTime? DateTimeForReport { get; set; }
        public string OrgUnitName { get; set; }
        public PersonalDescriptionReportEntity()
        {

        }
        public PersonalDescriptionReportEntity(
                                             int? PrisonerID = null,
                                             string PrisonerName = null,
                                             DateTime? BirthDay = null,
                                             string RegAddress = null,
                                             string Nationality = null,
                                             string Citizenship = null,
                                             string LivingAddress = null,
                                             string FamilyStatus = null,
                                             string EducationName = null,
                                             string ProfessionsName = null,
                                             string WorkName = null,
                                             string ArmyName = null,
                                             string HealthType = null,
                                             string PreviousConviction = null,
                                             string PresentConviction = null,
                                             string PresentConvDuration = null,
                                             string Fine = null,
                                             DateTime? SentencingStartDate = null,
                                             DateTime? SentencingEndDate = null,
                                             DateTime? EarlyDate = null,
                                             string EarlyType = null,
                                             string EarlyCurentType = null,
                                             DateTime? PrisonAccessDate = null,
                                             string OrgType = null,
                                             string LowSuit = null,
                                             string LowSuitState = null,
                                             string LowSuitAmount = null,
                                             string PenaltyName = null,
                                             string OrgUnitName = null,
                                             DateTime? DateTimeForReport = null,
                                             string EncouragementsName = null)
        {
            this.PrisonerID = PrisonerID;
            this.PrisonerName = PrisonerName;
            this.BirthDay = BirthDay;
            this.RegAddress = RegAddress;
            this.PrisonerName = PrisonerName;
            this.Nationality = Nationality;
            this.Citizenship = Citizenship;
            this.LivingAddress = LivingAddress;
            this.FamilyStatus = FamilyStatus;
            this.EducationName = EducationName;
            this.ProfessionsName = ProfessionsName;
            this.WorkName  = WorkName;
            this.ArmyName = ArmyName;
            this.HealthType = HealthType;
            this.PreviousConviction = PreviousConviction;
            this.PresentConviction = PresentConviction;
            this.PresentConvDuration = PresentConvDuration;
            this.Fine = Fine;
            this.SentencingStartDate = SentencingStartDate;
            this.SentencingEndDate = SentencingEndDate;
            this.EarlyDate = EarlyDate;
            this.EarlyType = EarlyType;
            this.EarlyCurentType = EarlyCurentType;
            this.PrisonAccessDate = PrisonAccessDate;
            this.OrgType = OrgType;
            this.LowSuit = LowSuit;
            this.LowSuitState = LowSuitState;
            this.LowSuitAmount = LowSuitAmount;
            this.PenaltyName = PenaltyName;
            this.EncouragementsName = EncouragementsName;
            this.DateTimeForReport = DateTimeForReport;
            this.OrgUnitName = OrgUnitName;
        }
    }
}
