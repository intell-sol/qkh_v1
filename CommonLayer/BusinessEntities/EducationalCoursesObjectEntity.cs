﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class EducationalCoursesObjectEntity
    {
        public int? PrisonerID { get; set; }
        public List<EducationalCoursesEntity> EducationCourses { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public EducationalCoursesObjectEntity()
        {
            this.PrisonerID = null;
            this.EducationCourses= new List<EducationalCoursesEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }
        public EducationalCoursesObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.EducationCourses = new List<EducationalCoursesEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }

    }
}
