﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class OfficialVisitsEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { get; set; }
        public int? TypeLibItemID { get; set; }
        public string TypeLibItemLabel { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? PersonID { get; set; }
        public PersonEntity Person { get; set; }
        public string PersonName { get; set; }
        public int? PositionLibItemID { get; set; }
        public string PositionLibItemLabel { get; set; }
        public List<VisitorEntity> VisitorList { get; set; }
        public string Note { get; set; }
        public string Description { get; set; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
        public OfficialVisitsEntity()
        {
        }

        public OfficialVisitsEntity(int? ID = null, int? PrisonerID = null, int? TypeLibItemID = null,
            string TypeLibItemLabel = null, DateTime? StartDate = null, DateTime? EndDate = null, int? PersonID = null,
            string PositionLibItemLabel = null,
            string PersonName = null, int? PositionLibItemID = null,
            string Note = null, bool? Status = null, bool? MergeStatus = null, string Description = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.TypeLibItemID = TypeLibItemID;
            this.TypeLibItemLabel = TypeLibItemLabel;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.PersonID = PersonID;
            this.PersonName = PersonName;
            this.PositionLibItemID = PositionLibItemID;
            this.PositionLibItemLabel = PositionLibItemLabel;
            this.Note = Note;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
            this.Description = Description;
        }
    }
}