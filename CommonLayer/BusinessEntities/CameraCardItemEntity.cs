﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class CameraCardItemEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? OrgUnitID { set; get; }
        public int? Number { set; get; }
        public DateTime? InputDate { set; get; }
        public DateTime? OutputDate { set; get; }
        public int? EmployeeID { set; get; }
        public string Note { set; get; }
        public string OrgUnitName { get; set; }
        public bool? Conflicted { set; get; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }
        public CameraCardItemEntity() {
        }
        public CameraCardItemEntity(int? ID = null,int? PrisonerID = null, int? OrgUnitID = null, int? Number = null, 
            DateTime? InputDate = null, DateTime? OutputDate = null, int? EmployeeID = null, string Note = null, bool? Status = null, bool? MergeStatus = null, string OrgUnitName = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.OrgUnitID = OrgUnitID;
            this.Number = Number;
            this.InputDate = InputDate;
            this.OutputDate = OutputDate;
            this.EmployeeID = EmployeeID;
            this.OrgUnitName = OrgUnitName;
            this.Note = Note;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
    }
}
