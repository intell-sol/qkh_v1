﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class ItemEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? LibItemID { set; get; }
        public DateTime? Date { set; get; }
        public int? Count { set; get; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }

        public ItemEntity()
        {

        }
        public ItemEntity(int? ID = null, int? PrisonerID = null, int? LibItemID = null,  DateTime? Date=null, int? Count = null, bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.LibItemID = LibItemID;
            this.Date = Date;
            this.Count = Count;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }

    }
}
