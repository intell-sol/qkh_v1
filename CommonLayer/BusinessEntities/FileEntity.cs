﻿
using System;
using System.IO;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class FileEntity
    {
        public string PNum { get; set; }
        public bool isPrisoner { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public string ContentType { get; set; }
        public byte[] FileContent { get; set; }
        public long ContentLength { get; set; }
        private string genFileName { get; set; }
        public FileEntity()
        {

        }
        public FileEntity(long ContentLength)
        {
            FileName = null;
            FileExtension = null;
            ContentType = null;
            FileContent = new byte[ContentLength];
            PNum = null;
            isPrisoner = true;
            this.ContentLength = ContentLength;
            genFileName = System.DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }
        public string ServerDirectoryPath
        {
            get
            {
                string localPath = CommonLayer.HelperClasses.ConfigurationHelper.GetFileStoragePath();
                localPath += @"/";
                localPath += ((isPrisoner) ? "Prisoners" : "Employees");
                localPath += @"/";
                localPath += System.DateTime.Now.Year.ToString();
                localPath += @"/";
                localPath += PNum;
                return localPath;
            }
        }
        
        public string LocalFilePath
        {
            get
            {
                string localPath = ((isPrisoner) ? "Prisoners" : "Employees");
                localPath += @"/";
                localPath += System.DateTime.Now.Year.ToString();
                localPath += @"/";
                localPath += PNum;
                localPath += @"/";
                localPath += genFileName;
                localPath += FileExtension;
                return localPath;
            }
        }
        public string ServerFullPath
        {
            get
            {
                string localPath = CommonLayer.HelperClasses.ConfigurationHelper.GetFileStoragePath();
                localPath += @"/";
                localPath += LocalFilePath;
                return localPath;
            }
        }
    }
}
