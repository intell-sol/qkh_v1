﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class AmnestyPointsEntity
    {
        public int? ID { set; get; }
        public int? PointID { set; get; }
        public string TempID { set; get; }
        public bool? New { set; get; }
        public string ParentID { set; get; }
        public int? RealParentID { set; get; }
        public int? AmnestyID { set; get; }
        public string Name { set; get; }
        public string Order { set; get; }
        public DateTime? ModifyDate { set; get; }
        public DateTime? CreationDate { set; get; } 
        public bool? Status { set; get; }
        public List<AmnestyPointsEntity> subItems { get; set; }
        public AmnestyPointsEntity() {
            subItems = new List<AmnestyPointsEntity>();
        }
        public AmnestyPointsEntity(int? ID = null, int? PointID = null, int? RealParentID = null, int? AmnestyID=null, string Name=null,
            string Order = null, DateTime? ModifyDate = null, DateTime? CreationDate=null,bool? Status = null)
        {
            this.ID = ID;
            this.PointID = PointID;
            this.RealParentID = RealParentID;
            this.AmnestyID = AmnestyID;
            this.Name = Name;
            this.Order = Order;
            this.ModifyDate = ModifyDate;
            this.CreationDate = CreationDate;
            this.Status = Status;
            subItems = new List<AmnestyPointsEntity>();
        }

    }
}
