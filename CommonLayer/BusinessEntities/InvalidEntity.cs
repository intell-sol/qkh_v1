﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class InvalidEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public DateTime? Date { set; get; }
        public string Description { get; set; }
        public int? ClassLibItem_ID { set; get; }
        public string CertificateNumber { set; get; }
        public DateTime? CertificateDate { set; get; }
        public string CertificateFrom { set; get; }
        public int? CertificateFileID { set; get; }
        public DateTime? ModifyDate { set; get; }
        public bool? Status { set; get; }
        public bool? MergeStatus { get; set; }
        public string FileSrc { get; set; }
        public InvalidEntity() {
        }

        public InvalidEntity(int? ID = null, int? PrisonerID = null,
            DateTime? Date = null,string Description= null,
            int? ClassLibItem_ID = null, string CertificateNumber = null,
            DateTime? CertificateDate = null, string CertificateFrom = null,
            int? CertificateFileID = null, DateTime? ModifyDate = null,
            bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.Date = Date;
            this.Description = Description;
            this.ClassLibItem_ID = ClassLibItem_ID;
            this.CertificateNumber = CertificateNumber;
            this.CertificateDate = CertificateDate;
            this.CertificateFrom = CertificateFrom;
            this.CertificateFileID = CertificateFileID;
            this.ModifyDate = ModifyDate;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }

    }
}
