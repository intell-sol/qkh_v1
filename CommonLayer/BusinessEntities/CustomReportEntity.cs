﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class CustomReportEntity
    {
        public DateTime FirstDate { get; set; }
        public DateTime SecondDate { get; set; }
        public bool? chPersonalNumber { get; set; }
        public bool? chPersonalCaseNumber { get; set; }
        public bool? chPrisonerName { get; set; }
        public bool? chPrisonerType { get; set; }
        public bool? chBirthDay { get; set; }
        public bool? chSexType { get; set; }
        public bool? chNationality { get; set; }
        public bool? chCitizenship { get; set; }
        public bool? chArrestStartDate { get; set; }
        public bool? chSentencingStartDate { get; set; }
        public bool? chPrisonAccessDate { get; set; }
        public bool? chPenaltyName { get; set; }
        public bool? chEncouragementsName { get; set; }
        public bool? chInjunctionsName { get; set; }
        public bool? chCaseCloseType { get; set; }
        public bool? chTransfersCount { get; set; }
        public bool? chPunishmentType { get; set; }
        public bool? chSentenceArticleType { get; set; }
        public bool? chPresentArticle { get; set; }
        public bool? chConflictsPersons { get; set; }
        public bool? chCameraCardNumber { get; set; }
        public bool? chParents { get; set; }
        public bool? chChildren { get; set; }
        public bool? chSisterBrother { get; set; }
        public bool? chSpouses { get; set; }
        public bool? chEducationName { get; set; }
        public bool? chProfessionName { get; set; }
        public bool? chWorkName { get; set; }
        public bool? chPreviousConviction { get; set; }
        public bool? chBloodType { get; set; }
        public bool? chInvalidClass { get; set; }
        public bool? chLeaningNames { get; set; }
        public bool? chOfficialVisitsCount { get; set; }
        public bool? chPersonalVisitsCount { get; set; }
        public bool? chPackagesCount { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public int? SexType { get; set; }
        public string Nationality { get; set; }
        public string Citizenship { get; set; }
        public string PersonalID { get; set; }
        public int? PersonalCaseNumber { get; set; }
        public string OrgUnit { get; set; }
        public int? Type { get; set; }
        public string PunishmentType { get; set; }
        public string SentenceArticleType { get; set; }
        public string PenaltyType { get; set; }
        public string Leaning { get; set; }
        public string Invalid { get; set; }
        public int? Age { get; set; }
        public string PrisonerType { get; set; }
        public DateTime? BirthDay { get; set; }
        public string SexTypeName { get; set; }
        public string ArrestStartDate { get; set; }
        public string SentencingStartDate { get; set; }
        public string PrisonAccessDate { get; set; }
        public string PenaltyName { get; set; }
        public string EncouragementsName { get; set; }
        public string InjunctionsName { get; set; }
        public string CaseCloseType { get; set; }
        public string TransfersCount { get; set; }
        public string PresentArticle { get; set; }
        public string ConflictsPersons { get; set; }
        public int? CameraCardNumber { get; set; }
        public string Parents { get; set; }
        public string Children { get; set; }
        public string SisterBrother { get; set; }
        public string Spouses { get; set; }
        public string EducationName { get; set; }
        public string ProfessionName { get; set; }
        public string WorkName { get; set; }
        public string PreviousConviction { get; set; }
        public string BloodType { get; set; }
        public string InvalidClass { get; set; }
        public string LeaningNames { get; set; }
        public int? OfficialVisitsCount { get; set; }
        public int? PersonalVisitsCount { get; set; }
        public int? PackagesCount { get; set; }
        public  CustomReportEntity()
        {

        }
        public CustomReportEntity(string FirstName = null,
                                  string LastName = null,
                                  string MiddleName = null,
                                  int? SexType = null,
                                  string Nationality = null,
                                  string Citizenship = null,
                                  string PersonalID = null,
                                  int? PersonalCaseNumber = null,
                                  string OrgUnit = null,
                                  int? Type = null,
                                  string PunishmentType = null,
                                  string SentenceArticleType = null,
                                  string PenaltyType = null,
                                  string Leaning = null,
                                  string Invalid = null,
                                  int? Age = null,
                                  string PrisonerType = null,
                                  DateTime? BirthDay = null,
                                  string SexTypeName = null,
                                  string ArrestStartDate = null,
                                  string SentencingStartDate = null,
                                  string PrisonAccessDate = null,
                                  string PenaltyName = null,
                                  string EncouragementsName = null,
                                  string InjunctionsName = null,
                                  string CaseCloseType = null,
                                  string TransfersCount = null,
                                  string PresentArticle = null,
                                  string ConflictsPersons = null,
                                  int? CameraCardNumber = null,
                                  string Parents = null,
                                  string Children = null,
                                  string SisterBrother = null,
                                  string Spouses = null,
                                  string EducationName = null,
                                  string ProfessionName = null,
                                  string WorkName = null,
                                  string PreviousConviction = null,
                                  string BloodType = null,
                                  string InvalidClass = null,
                                  string LeaningNames = null,
                                  int? OfficialVisitsCount = null,
                                  int? PersonalVisitsCount = null,
                                  int? PackagesCount = null)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.MiddleName = MiddleName;
            this.SexType = SexType;
            this.Nationality = Nationality;
            this.Citizenship = Citizenship;
            this.PersonalID = PersonalID;
            this.PersonalCaseNumber = PersonalCaseNumber;
            this.OrgUnit = OrgUnit;
            this.Type = Type;
            this.PunishmentType = PunishmentType;
            this.SentenceArticleType = SentenceArticleType;
            this.PenaltyType = PenaltyType;
            this.Leaning = Leaning;
            this.Invalid = Invalid;
            this.Age = Age;
            this.PrisonerType = PrisonerType;
            this.BirthDay = BirthDay;
            this.SexTypeName = SexTypeName;
            this.ArrestStartDate = ArrestStartDate;
            this.SentencingStartDate = SentencingStartDate;
            this.PrisonAccessDate = PrisonAccessDate;
            this.PenaltyName = PenaltyName;
            this.EncouragementsName = EncouragementsName;
            this.InjunctionsName = InjunctionsName;
            this.CaseCloseType = CaseCloseType;
            this.TransfersCount = TransfersCount;
            this.PresentArticle = PresentArticle;
            this.ConflictsPersons = ConflictsPersons;
            this.CameraCardNumber = CameraCardNumber;
            this.Parents = Parents;
            this.Children = Children;
            this.SisterBrother = SisterBrother;
            this.Spouses = Spouses;
            this.EducationName = EducationName;
            this.ProfessionName = ProfessionName;
            this.WorkName = WorkName;
            this.PreviousConviction = PreviousConviction;
            this.BloodType = BloodType;
            this.InvalidClass = InvalidClass;
            this.LeaningNames = LeaningNames;
            this.OfficialVisitsCount = OfficialVisitsCount;
            this.PersonalVisitsCount = PersonalVisitsCount;
            this.PackagesCount = PackagesCount;
        }
    }
}
