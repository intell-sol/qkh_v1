﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class InjunctionItemEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? ObjectLibItem_ID { set; get; }
        public int? VoroshoxLibItem_ID { set; get; }
        public DateTime? Date { set; get; }
        public string Voroshman_Hamar { set; get; }
        public string TerminationNumber { set; get; }
        public int? TerminationVoroshoxLibItem_ID { set; get; }
        public string TerminationVoroshoxLibItem_Label { get; set; }
        public DateTime? TerminationDate { set; get; }
        public bool? State { set; get; }
        public bool? MergeStatus { set; get; }
        public bool? Status { set; get; }
        public List<InjunctionItemObjectEntity> Objects { get; set; }
        public InjunctionItemEntity() {
        }
        public InjunctionItemEntity(int? ID = null, int? PrisonerID = null, int? ObjectLibItem_ID = null,
            int? VoroshoxLibItem_ID = null, DateTime? Date = null, string Voroshman_Hamar = null,
            string TerminationNumber = null, int? TerminationVoroshoxLibItem_ID = null,
            DateTime?  TerminationDate = null, bool? State = null, bool? Status = null, bool? MergeStatus = null,
            string TerminationVoroshoxLibItem_Label = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.ObjectLibItem_ID = ObjectLibItem_ID;
            this.VoroshoxLibItem_ID = VoroshoxLibItem_ID;
            this.Date = Date;
            this.Voroshman_Hamar = Voroshman_Hamar;
            this.TerminationNumber = TerminationNumber;
            this.TerminationVoroshoxLibItem_ID = TerminationVoroshoxLibItem_ID;
            this.TerminationVoroshoxLibItem_Label = TerminationVoroshoxLibItem_Label;
            this.TerminationDate = TerminationDate;
            this.State = State;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
    }
}
