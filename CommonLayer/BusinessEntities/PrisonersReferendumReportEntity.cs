﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PrisonersReferendumReportEntity
    {
        public List<int> OrgUnitList { set; get; }
        public DateTime? Date { get; set; }
        public string PrisonerName { get; set; }
        public DateTime? BirthDay { get; set; }
        public string Address { get; set; }
        public string IdentificationDocument { get; set; }
        public PrisonersReferendumReportEntity()
        {

        }
        public PrisonersReferendumReportEntity(DateTime? Date = null,
                                                List<int> OrgUnitList = null,
                                                string PrisonerName = null,
                                                DateTime? BirthDay = null,
                                                string Address = null,
                                                string IdentificationDocument = null)
        {
            this.Date = Date;
            this.PrisonerName = PrisonerName;
            this.BirthDay = BirthDay;
            this.Address = Address;
            this.IdentificationDocument = IdentificationDocument;
            OrgUnitList = new List<int>();
        }
    }
}
