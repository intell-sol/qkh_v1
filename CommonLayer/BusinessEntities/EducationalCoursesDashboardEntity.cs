﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class EducationalCoursesDashboardEntity : EducationalCoursesEntity
    {
        public string PrisonerName { get; set; }
        public string Personal_ID { get; set; }
        public int? PrisonerType { get; set; }
        public bool? ArchiveStatus { get; set; }
        public EducationalCoursesDashboardEntity()
        {
        }
        public EducationalCoursesDashboardEntity(int? ID = null, int? PrisonerID = null, int? TypeLibItemID = null,
            string TypeLibItemLabel = null, DateTime? EndDate = null, DateTime? EngagementDate = null,
            int? EngagementBasisLibItemID = null, string EngagementBasisLibItemLabel = null,
            bool? EngagementState = null, DateTime? ReleaseDate = null,
            int? ReleaseBasisLibItemID = null, string ReleaseBasisLibItemLabel = null,
            string ReleaseNote = null, string Note = null, bool? Status = null, 
            string PrisonerName = null, string Personal_ID = null, bool? ArchiveStatus = null, int? PrisonerType = null) 
            :base(ID, PrisonerID , TypeLibItemID , TypeLibItemLabel , EndDate, EngagementDate ,
            EngagementBasisLibItemID , EngagementBasisLibItemLabel , EngagementState, ReleaseDate ,
            ReleaseBasisLibItemID , ReleaseBasisLibItemLabel ,
            ReleaseNote , Note , Status )
        {
            
            this.PrisonerName = PrisonerName;
            this.Personal_ID = Personal_ID;
            this.ArchiveStatus = ArchiveStatus;
            this.PrisonerType = PrisonerType;
        }

    }
}