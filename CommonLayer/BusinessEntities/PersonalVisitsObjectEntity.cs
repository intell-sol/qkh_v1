﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PersonalVisitsObjectEntity
    {
        public int? PrisonerID { get; set; }
        public List<PersonalVisitsEntity> Visits { get; set; }
        public List<AdditionalFileEntity> Files { get; set; }
        public PersonalVisitsObjectEntity()
        {
            this.PrisonerID = null;
            this.Visits = new List<PersonalVisitsEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }
        public PersonalVisitsObjectEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.Visits = new List<PersonalVisitsEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }

    }
}
