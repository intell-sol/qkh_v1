﻿

using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class MilitaryServiceEntity
    {
        public int? PrisonerID { get; set; }
        public List<ArmyEntity> Army { set; get; }
        public List<ArmyAwardEntity> ArmyAwards { set; get; }
        public NotServedEntity NotServed { set; get; }
        public List<WarInvolvedEntity> WarInvolved { set; get; }
        public List<AdditionalFileEntity> Files { set; get; }

        public MilitaryServiceEntity()
        {
            ArmyAwards = new List<ArmyAwardEntity>();
            WarInvolved = new List<WarInvolvedEntity>();
        }
    }
}
