﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class DepartureEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { get; set; }
        public string Place { get; set; }
        public int? TypeLibItemID { get; set; }
        public int? CaseLibItemID { get; set; }
        public int? MoveLibItemID { get; set; }
        public int? MedLibItemID { get; set; }
        public int? InvestigateLibItemID { get; set; }
        public int? OrgUnitID { get; set; }
        public int? CourtLibItemID { get; set; }
        public int? HospitalLibItemID { get; set; }
        public string TypeLibItemLabel { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? State { get; set; }
        public int? Duration { get; set; }
        public int? DuartionByHour { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? PurposeLibItemID { get; set; }
        public string PurposeLibItemLabel { get; set; }
        public int? ApproverEmployeeID { get; set; }
        public string ApproverEmployeeName { get; set; }
        public bool? Status { get; set; }
        public bool? MergeStatus { get; set; }

        public DepartureEntity()
        {
        }

        public DepartureEntity(int? ID = null, int? PrisonerID = null,string Place = null, int? TypeLibItemID = null, int? MedLibItemID = null,
             int? CaseLibItemID = null, int? MoveLibItemID = null,int? InvestigateLibItemID=null,
             int? OrgUnitID = null, int? CourtLibItemID = null, int? HospitalLibItemID = null,
            string TypeLibItemLabel = null, DateTime? StartDate = null, DateTime? EndDate = null,
            int? Duration=null,int? DuartionByHour= null, DateTime? CreationDate=null,DateTime? ModifyDate=null,
            int? ApproverEmployeeID = null, string ApproverEmployeeName = null,
            int? PurposeLibItemID = null, string PurposeLibItemLabel = null, bool? Status = null, bool? State = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.Place = Place;
            this.MoveLibItemID = MoveLibItemID;
            this.CaseLibItemID = CaseLibItemID;
            this.State = State;
            this.MedLibItemID = MedLibItemID;
            this.TypeLibItemID = TypeLibItemID;
            this.InvestigateLibItemID = InvestigateLibItemID;
            this.OrgUnitID = OrgUnitID;
            this.CourtLibItemID = CourtLibItemID;
            this.HospitalLibItemID = HospitalLibItemID;
            this.TypeLibItemLabel = TypeLibItemLabel;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.Duration = Duration;
            this.DuartionByHour = DuartionByHour;
            this.CreationDate = CreationDate;
            this.ModifyDate = ModifyDate;
            this.ApproverEmployeeID = ApproverEmployeeID;
            this.ApproverEmployeeName = ApproverEmployeeName;
            this.PurposeLibItemID = PurposeLibItemID;
            this.PurposeLibItemLabel = PurposeLibItemLabel;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }

    }
}
