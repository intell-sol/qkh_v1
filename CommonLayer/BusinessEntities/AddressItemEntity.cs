﻿using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class AddressItemEntity
    {
        public int? ID { set; get; }
        public string Name { set; get; }
        public bool? Status { set; get; }
        public AddressItemEntity()
        {

        }
        public AddressItemEntity(int? ID = null, string Name = null, bool? Status = null)
        {
            this.ID = ID;
            this.Name = Name;
            this.Status = Status;
        }
    }
}
