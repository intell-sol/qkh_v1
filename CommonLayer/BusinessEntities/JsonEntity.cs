﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    /// <summary>
    /// Json object for working with and serializing
    /// </summary>
    [Serializable]
    public class JsonEntity
    {
        Boolean Error { get; set; }
        Boolean Status { get; set; }
        String Message { get; set; }
        public JsonEntity()
        {
            this.Error = false;
        }
        public JsonEntity(Boolean Error)
        {
            this.Error = Error;
        }
        public JsonEntity(Boolean Error, Boolean Status)
        {
            this.Error= Error;
            this.Status = Status;
        }
        public JsonEntity(Boolean Error, Boolean Status, string Message)
        {
            this.Error = Error;
            this.Status = Status;
            this.Message = Message;
        }
    }
}
