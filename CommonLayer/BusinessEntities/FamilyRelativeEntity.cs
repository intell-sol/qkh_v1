﻿

using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    public class FamilyRelativeEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public FamilyStatus? FamilyStatusID { set; get; }
        public bool? Status { set; get; }
        public bool? MergeStatus { get; set; }
        public List<SpousesEntity> Spouses { set; get; }
        public List<ChildrenEntity> Childrens { set; get; }
        public List<ParentsEntity> Parents { set; get; }
        public List<SisterBrotherEntity> SisterBrother { set; get; }
        public List<OtherRelativesEntity> OtherRelatives { set; get; }
        public List<AdditionalFileEntity> Files { set; get; }
        public FamilyRelativeEntity()
        {
            ID = null;
            PrisonerID = null;
            FamilyStatusID = null;
            Status = null;
            Spouses = new List<SpousesEntity>();
            Childrens = new List<ChildrenEntity>();
            Parents = new List<ParentsEntity>();
            SisterBrother = new List<SisterBrotherEntity>();
            OtherRelatives = new List<OtherRelativesEntity>();
        }
        public FamilyRelativeEntity(int? ID = null,int? PrisonerID = null,
            FamilyStatus? FamilyStatusID = null,
            bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.FamilyStatusID = FamilyStatusID;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
            Spouses = new List<SpousesEntity>();
            Childrens = new List<ChildrenEntity>();
            Parents = new List<ParentsEntity>();
            SisterBrother = new List<SisterBrotherEntity>();
            OtherRelatives = new List<OtherRelativesEntity>();
        }
    }
    
    public enum FamilyStatus
    {
        NOT_MARRIED = 1,
        MARRIED = 2,
        DIVORCED = 3
    }
}
