﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class PersonEntity
    {
        public int? ID { set; get; }
        public string Personal_ID { set; get; }
        public string FirstName { set; get; }
        public string MiddleName { set; get; }
        public string LastName { set; get; }
        public DateTime? Birthday { set; get; }
        public int? SexLibItem_ID { set; get; }
        public int? NationalityID { set; get; }
        public string NationalityLabel { set; get; }
        public int? Registration_ID { set; get; }
        public int? Living_ID { set; get; }
        public string Registration_address { set; get; }
        public string Living_place { set; get; }
        public string PhotoLink { set; get; }
        public bool? Status { set; get; }
        public RegistrationAddress Registration_Entity { get; set; }
        public RegistrationAddress Living_Entity { get; set; }
        public string Photo_ID { get; set; }
        public string RelativeLabel { get; set; }
        public bool? isEditable { get; set; }
        public bool? isCustom { get; set; }
        public List<IdentificationDocument> IdentificationDocuments { set; get; }
        
        public PersonEntity()
        {
            isEditable = true;
            isCustom = false;
        }

        public PersonEntity(int? ID = null, string Personal_ID = null, string FirstName = null,
            string MiddleName = null, string LastName = null, DateTime? Birthday = null, int? SexLibItem_ID = null,
            int? NationalityID = null, string Registration_address = null, string Living_place = null, bool Status = true,
            int? Registration_ID = null, int? Living_ID = null, bool? isEditable = true, bool? isCustom = false)
        {
            this.ID = ID;
            this.Personal_ID = Personal_ID;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.MiddleName = MiddleName;
            this.Birthday = Birthday;
            this.SexLibItem_ID = SexLibItem_ID;
            this.Registration_ID = Registration_ID;
            this.Living_ID = Living_ID;
            this.NationalityID = NationalityID;
            this.Registration_address = Registration_address;
            this.Living_place = Living_place;
            this.Status = Status;
            this.NationalityLabel = null;
            this.isEditable = isEditable;
            this.isCustom = isCustom;
        }
    }
}
