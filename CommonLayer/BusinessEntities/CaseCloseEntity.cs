﻿using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class CaseCloseEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? PCN { set; get; }
        public int? OrgUnitID { set; get; }
        public string OrgUnitLabel { set; get; }
        public DateTime? StartDate { set; get; }
        public DateTime? EndDate { set; get; }
        public int? CloseTypeLibItemID { set; get; }
        public DateTime? CloseDate { set; get; }
        public string CommandNumber { set; get; }
        public int? CloseFromWhomID { set; get; }
        public string Notes { set; get; }
        public bool? CourtDesicion { get; set; }
        public bool? Suicide { get; set; }
        public bool? DataType { get; set; }
        public int? SentenceID { get; set; }
        public int? ArrestID { get; set; }
        public bool? Status { get; set; }
        public bool? ArchiveCase { get; set; }

        public CaseCloseEntity()
        {

        }
        public CaseCloseEntity(int? ID = null, int? PrisonerID = null, int? PCN = null,
            int? OrgUnitID = null, string OrgUnitLabel = null, bool? DataType = null,
            DateTime? StartDate = null, DateTime? EndDate = null, 
            int? CloseTypeLibItemID = null, DateTime? CloseDate = null,
            string CommandNumber = null, int? CloseFromWhomID = null, string Notes = null,
            bool? Status = null, bool? CourtDesicion = null,bool? Suicide = null,
            int? SentenceID = null, int? ArrestID = null, bool? ArchiveCase = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.PCN = PCN;
            this.OrgUnitID = OrgUnitID;
            this.OrgUnitLabel = OrgUnitLabel;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.CloseTypeLibItemID = CloseTypeLibItemID;
            this.CloseDate = CloseDate;
            this.CommandNumber = CommandNumber;
            this.CloseFromWhomID = CloseFromWhomID;
            this.CourtDesicion = CourtDesicion;
            this.Suicide = Suicide;
            this.Notes = Notes;
            this.DataType = DataType;
            this.SentenceID = SentenceID;
            this.ArrestID = ArrestID;
            this.ArchiveCase = ArchiveCase;
            this.Status = Status;
        }
    }
}
