﻿using System;


namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class IdentificationDocument
    {
        public int? ID { set; get; }
        public int? PersonID { set; get; }
        public int? PrisonerID { set; get; }
        public int? CitizenshipLibItem_ID { set; get; }
        public string CitizenshipLibItem_Name { get; set; }
        public int? TypeLibItem_ID { set; get; }
        public string TypeLibItem_Name { set; get; }
        public string Number { set; get; }
        public bool Type { set; get; }
        public DateTime? Date { set; get; }
        public string FromWhom { set; get; }
        public bool? Status { set; get; }
        public bool? MergeStatus { get; set; }

        public IdentificationDocument() {
        }
        public IdentificationDocument(int? ID = null, int? PersonID = null, string CitizenshipLibItem_Name = null,
            string TypeLibItem_Name = null,
            int? CitizenshipLibItem_ID = null,  int? TypeLibItem_ID = null,
            string Number = null, DateTime? Date = null, string FromWhom = null,
            bool? Status = null, int? PrisonerID = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PersonID = PersonID;
            this.CitizenshipLibItem_ID = CitizenshipLibItem_ID;
            this.PrisonerID = PrisonerID;
            this.TypeLibItem_ID = TypeLibItem_ID;
            this.CitizenshipLibItem_Name = CitizenshipLibItem_Name;
            this.TypeLibItem_Name = TypeLibItem_Name;
            this.Number = Number;
            this.Date = Date;
            this.FromWhom = FromWhom;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
    }
}
