﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class CameraCardEntity
    {
        public int? PrisonerID { set; get; }
        public List<CameraCardItemEntity> CameraCardItem { get; set; }
        public List<AdditionalFileEntity> Files { set; get; }
        public CameraCardEntity(int? PrisonerID = null)
        {
            this.PrisonerID = PrisonerID;
            this.CameraCardItem = new List<CameraCardItemEntity>();
            this.Files = new List<AdditionalFileEntity>();
        }
    }
}
