﻿using System;
using System.Collections.Generic;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class SpousesEntity
    {
        public int? ID { set; get; }
        public int? PersonID { set; get; }
        public int? PrisonerID { set; get; }
        public bool? MarriageRegistered { set; get; }
        public string Certificate { set; get; }
        public bool? State { set; get; }
        public bool? Status { get; set; }
        public PersonEntity Person { get; set; }
        public bool? MergeStatus { get; set; }

        public SpousesEntity()
        {
        }
        public SpousesEntity(int? ID = null, int? PersonID = null, int? PrisonerID = null, bool? MarriageRegistered = null, string Certificate = null, 
            bool? State = null, bool? Status = null, bool? MergeStatus = null)
        {
            this.ID = ID;
            this.PersonID = PersonID;
            this.PrisonerID = PrisonerID;
            this.MarriageRegistered = MarriageRegistered;
            this.Certificate = Certificate;
            this.State = State;
            this.Status = Status;
            this.MergeStatus = MergeStatus;
        }
    }
}
