﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CommonLayer.BusinessEntities
{
    [Serializable]

    [XmlRoot(ElementName = "RegistrationAddress", Namespace = HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class RegistrationAddress
    {
        [XmlElement(ElementName = "Registration_Region", Namespace = HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Registration_Region { get; set; }
        [XmlElement(ElementName = "Registration_Community", Namespace = HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Registration_Community { get; set; }
        [XmlElement(ElementName = "Registration_Street", Namespace = HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Registration_Street { get; set; }
        [XmlElement(ElementName = "Registration_Building", Namespace = HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Registration_Building { get; set; }
        [XmlElement(ElementName = "Registration_Building_Type", Namespace = HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Registration_Building_Type { get; set; }
        [XmlElement(ElementName = "Registration_Apartment", Namespace = HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Registration_Apartment { get; set; }
        public RegistrationAddress() {

        }
        public RegistrationAddress(PersonToAddressEntity item)
        {
            this.Registration_Apartment = item.ApartmentName;
            this.Registration_Region = item.RegionName;
            this.Registration_Community = item.ComunityName;
            this.Registration_Street = item.StreetName;
            this.Registration_Building = item.BuildingName;
            this.Registration_Building_Type = item.BuildingTypeName;
        }
    }
}
