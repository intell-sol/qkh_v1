﻿using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class InterestEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? EducationProfessionsID { set; get; }
        public int? LibItemID { set; get; }
        public bool? Status { get; set; }
        public InterestEntity()
        {
            ID = null;
            PrisonerID = null;
            EducationProfessionsID = null;
            LibItemID = null;
            Status = null;
        }
        public InterestEntity(int? ID = null, int? PrisonerID = null, int? EducationProfessionsID = null, int? LibItemID = null, bool? Status = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.EducationProfessionsID = EducationProfessionsID;
            this.LibItemID = LibItemID;
            this.Status = Status;
        }
    }
}
