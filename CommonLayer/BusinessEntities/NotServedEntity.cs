﻿
using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class NotServedEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? ReasonLibItemID { set; get; }
        public string Description { set; get; }
        public bool? Status { set; get; }
        public NotServedEntity() {
        }
        public NotServedEntity(int? ID = null, int? PrisonerID = null, int? ReasonLibItemID = null, string Description = null, bool? Status = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.ReasonLibItemID = ReasonLibItemID;
            this.Description = Description;
            this.Status = Status;
        }
    }
}
