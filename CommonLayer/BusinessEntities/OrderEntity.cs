﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;


namespace CommonLayer.BusinessEntities
{
    /// <summary>
    /// For Storing Permissions tree
    /// </summary>

    [Serializable]
    public class OrderEntity
    {
        public OrderEntity()
        {

        }
        public OrderEntity(int? EmployeeID, int? OrderTypeID, string OrderNumber, string OrderBy, DateTime OrderDate, int FileID)
        {
            this.EmployeeID = EmployeeID;
            this.OrderTypeID = OrderTypeID;
            this.OrderNumber = OrderNumber;
            this.OrderBy = OrderBy;
            this.OrderDate = OrderDate;
            this.FileID = FileID;
        }

        public OrderEntity(int? ID, int? EmployeeID, int? OrderTypeID, string OrderNumber, string OrderBy, DateTime OrderDate, int FileID, DateTime CreationDate, DateTime ModifyDate, Boolean Status)
        {
            this.ID = ID;
            this.EmployeeID = EmployeeID;
            this.OrderTypeID = OrderTypeID;
            this.OrderNumber = OrderNumber;
            this.OrderBy = OrderBy;
            this.OrderDate = OrderDate;
            this.FileID = FileID;
            this.CreationDate = CreationDate;
            this.ModifyDate = ModifyDate;
            this.Status = Status;
        }

        public int? ID { get; set; }

        public int? EmployeeID { get; set; }
        public int? OrderTypeID { get; set; }
        public string OrderNumber { get; set; }
        public string OrderBy { get; set; }
        public DateTime OrderDate { get; set; }
        public int? FileID { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Boolean Status { get; set; }
        
    }
}
