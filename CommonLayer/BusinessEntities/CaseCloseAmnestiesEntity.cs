﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLayer.BusinessEntities
{
    public class CaseCloseAmnestiesEntity
    {
        public int? ID { get; set; }
        public int? AmnestyPointID { get; set; }
        public string PointName { get; set; }
        public int? CaseCloseID { get; set; }
        public bool? Status { get; set; }
        public CaseCloseAmnestiesEntity()
        {

        }
        public CaseCloseAmnestiesEntity(int? ID = null,
                                        int? AmnestyPointID = null,
                                        string PointName = null,
                                        int? CaseCloseID = null,
                                        bool? Status = null)
        {
            this.ID = ID;
            this.AmnestyPointID = AmnestyPointID;
            this.PointName = PointName;
            this.CaseCloseID = CaseCloseID;
            this.Status = Status;
        }
    }
}
