﻿using System;

namespace CommonLayer.BusinessEntities
{
    [Serializable]
    public class CaseOpenEntity
    {
        public int? ID { set; get; }
        public int? PrisonerID { set; get; }
        public int? PCN { set; get; }
        public int? OrgUnitID { set; get; }
        public string OrgUnitLabel { set; get; }
        public DateTime? StartDate { set; get; }
        public DateTime? EndDate { set; get; }
        public DateTime? OpenDate { set; get; }
        public string CommandNumber { set; get; }
        public int? FromWhomID { set; get; }
        public string Notes { set; get; }
        public bool? Status { get; set; }

        public CaseOpenEntity()
        {

        }
        public CaseOpenEntity(int? ID = null, int? PrisonerID = null, int? PCN = null, int? OrgUnitID = null, string OrgUnitLabel = null, DateTime? StartDate = null, DateTime? EndDate = null, 
            DateTime? OpenDate = null, string CommandNumber = null, int? FromWhomID = null, string Notes = null, bool? Status = null)
        {
            this.ID = ID;
            this.PrisonerID = PrisonerID;
            this.PCN = PCN;
            this.OrgUnitID = OrgUnitID;
            this.OrgUnitLabel = OrgUnitLabel;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.OpenDate = OpenDate;
            this.CommandNumber = CommandNumber;
            this.FromWhomID = FromWhomID;
            this.Notes = Notes;
            this.Status = Status;
        }
    }
}
