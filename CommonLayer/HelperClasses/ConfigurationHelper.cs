﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CommonLayer.HelperClasses
{
    public static class ConfigurationHelper
    {
        //public const string XMLNamespace = @"http://ErMMGI.org/ag/AG_Objects.xsd";
        public const string XMLNamespace = @"http://ermmgi.org/ct/qkag";
        public static string GetFileStoragePath()
        {
            try
            {
                return ConfigurationManager.AppSettings["FilesStoragePath"];
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log error
                throw;
            }
        }
        public static string GetDataBasePath()
        {
            try
            {
                GetMessages();
                return ConfigurationManager.AppSettings["DatabasePath"];
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log error
                throw;
            }
        }
        public static int GetPrisonerDashboardListViewCount()
        {
            try
            {
                return Int32.Parse(ConfigurationManager.AppSettings["PrisonerDashboardViewCount"]);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log error
                throw;
            }
        }
        public static Dictionary<string,string> GetMessages()
        {
            try
            {
                Dictionary<string, string> conf = new Dictionary<string, string>();
                conf.Add("record.status.draft", (string)ConfigurationManager.AppSettings["record.status.draft"]);
                conf.Add("record.status.final", (string)ConfigurationManager.AppSettings["record.status.final"]);
                return conf;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log error
                throw;
            }
        }

        public static string GetServiceAVVURL()
        {
            try
            {
                return ConfigurationManager.AppSettings["Service.AVV.URL"];

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log error
                throw;
            }
        }

        public static string GetServiceAVVXMLNS()
        {
            try
            {
                return ConfigurationManager.AppSettings["Service.AVV.XMLNS"];

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log error
                throw;
            }
        }

    }
}
