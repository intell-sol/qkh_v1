﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_OtherRelativesService : DataComm
    {
        public int? SP_Add_OtherRelatives(OtherRelativesEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PersonID");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("RelationLibItemID");

            ArrayValues.Add(entity.PersonID);
            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.RelationLibItemID);


            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_OtherRelatives", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<OtherRelativesEntity> SP_GetList_OtherRelatives(OtherRelativesEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_OtherRelatives", ArrayValues, ArrayParams);

                List<OtherRelativesEntity> list = new List<OtherRelativesEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        OtherRelativesEntity item = new OtherRelativesEntity(
                                ID:(int)row["ID"],
                                PersonID:(int)row["PersonID"],
                                PrisonerID:(int)row["PrisonerID"],
                                RelationLibItemID:(int)row["RelationLibItemID"],
                                MergeStatus:(row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }



                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<OtherRelativesEntity>();
            }
        }
        public bool SP_Update_OtherRelatives(OtherRelativesEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("RelationLibItemID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.RelationLibItemID);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_OtherRelatives", ArrayValues, ArrayParams);
                
                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
