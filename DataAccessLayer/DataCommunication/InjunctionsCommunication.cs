﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_InjunctionsService : DataComm
    {
        #region Injunction
        public int? SP_Add_Injunctions(InjunctionItemEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("VoroshoxLibItem_ID");
            ArrayParams.Add("Date");
            ArrayParams.Add("Voroshman_Hamar");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.VoroshoxLibItem_ID);
            ArrayValues.Add(entity.Date);
            ArrayValues.Add(entity.Voroshman_Hamar);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Injunctions", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<InjunctionItemEntity> SP_GetList_Injunctions(InjunctionItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Injunctions", ArrayValues, ArrayParams);

                List<InjunctionItemEntity> list = new List<InjunctionItemEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        InjunctionItemEntity item = new InjunctionItemEntity(
                                ID:(int)row["ID"],
                                PrisonerID:(int)row["PrisonerID"],
                                VoroshoxLibItem_ID:(int)row["VoroshoxLibItem_ID"],
                                Date:(row["Date"] == DBNull.Value) ? null : (DateTime?)row["Date"],
                                Voroshman_Hamar:(row["Voroshman_Hamar"] == DBNull.Value) ? null : (string)row["Voroshman_Hamar"],
                                TerminationNumber:(row["TerminationNumber"] == DBNull.Value) ? null : (string)row["TerminationNumber"],
                                TerminationVoroshoxLibItem_ID:(row["TerminationVoroshoxLibItem_ID"] == DBNull.Value) ? null : (int?)row["TerminationVoroshoxLibItem_ID"],
                                TerminationVoroshoxLibItem_Label: (row["TerminationVoroshoxLibItem_Label"] == DBNull.Value) ? null : (string)row["TerminationVoroshoxLibItem_Label"],
                                TerminationDate:(row["TerminationDate"] == DBNull.Value) ? null : (DateTime?)row["TerminationDate"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"],
                                State:(bool)row["State"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<InjunctionItemEntity>();
            }
        }
        public bool SP_Update_Injunctions(InjunctionItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("VoroshoxLibItem_ID");
                ArrayParams.Add("Date");
                ArrayParams.Add("Voroshman_Hamar");
                ArrayParams.Add("TerminationNumber");
                ArrayParams.Add("TerminationVoroshoxLibItem_ID");
                ArrayParams.Add("TerminationDate");
                ArrayParams.Add("State");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.VoroshoxLibItem_ID);
                ArrayValues.Add(entity.Date);
                ArrayValues.Add(entity.Voroshman_Hamar);
                ArrayValues.Add(entity.TerminationNumber);
                ArrayValues.Add(entity.TerminationVoroshoxLibItem_ID);
                ArrayValues.Add(entity.TerminationDate);
                ArrayValues.Add(entity.State);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Injunctions", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion

        #region InjunctionObjects
        public int? SP_Add_InjunctionObject(InjunctionItemObjectEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("InjunctionID");
            ArrayParams.Add("LibItem_ID");

            ArrayValues.Add(entity.InjunctionID);
            ArrayValues.Add(entity.LibItem_ID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_InjunctionObjects", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<InjunctionItemObjectEntity> SP_GetList_InjunctionObjects(InjunctionItemObjectEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("InjunctionID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.InjunctionID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_InjunctionObjects", ArrayValues, ArrayParams);

                List<InjunctionItemObjectEntity> list = new List<InjunctionItemObjectEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        InjunctionItemObjectEntity item = new InjunctionItemObjectEntity(
                                ID: (int)row["ID"],
                                InjunctionID: (int)row["InjunctionID"],
                                LibItem_ID: (int)row["LibItem_ID"],
                                LibItem_Label: (row["LibItem_Label"] == DBNull.Value) ? null : (string)row["LibItem_Label"],
                                Status: (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<InjunctionItemObjectEntity>();
            }
        }
        public bool SP_Update_InjunctionObject(InjunctionItemObjectEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_InjunctionObjects", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
