﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_ConflictsService : DataComm
    {
        public int? SP_Add_Conflict(ConflictsEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("PersonID");
            ArrayParams.Add("Description");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.PersonID);
            ArrayValues.Add(entity.Description);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Conflicts", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<ConflictsEntity> SP_GetList_Conflicts(ConflictsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Conflicts", ArrayValues, ArrayParams);

                List<ConflictsEntity> list = new List<ConflictsEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        ConflictsEntity item = new ConflictsEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                PersonID: (int)row["PersonID"],
                                Description: (row["Description"] == DBNull.Value) ? null : (string)row["Description"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<ConflictsEntity>();
            }
        }

        public List<ConflictsEntity> SP_GetList_ConflictsMain(ConflictsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_ConflictsMain", ArrayValues, ArrayParams);

                List<ConflictsEntity> list = new List<ConflictsEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        ConflictsEntity item = new ConflictsEntity(
                                PersonID: (int)row["ID"]
                            );
                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<ConflictsEntity>();
            }
        }
        public bool SP_Update_Conflict(ConflictsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();


                ArrayParams.Add("ID");
                ArrayParams.Add("PersonID");
                ArrayParams.Add("Description");
                ArrayParams.Add("Status");


                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PersonID);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.Status);


                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Conflicts", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}