﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_InvalidsService:DataComm
    {
        public int? SP_Add_Invalids(InvalidEntity Invalid)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("Date");
            ArrayParams.Add("Description");
            ArrayParams.Add("ClassLibItem_ID");
            ArrayParams.Add("CertificateNumber");
            ArrayParams.Add("CertificateDate");
            ArrayParams.Add("CertificateFrom");
            ArrayParams.Add("CertificateFileID");

            ArrayValues.Add(Invalid.PrisonerID);
            ArrayValues.Add(Invalid.Date);
            ArrayValues.Add(Invalid.Description);
            ArrayValues.Add(Invalid.ClassLibItem_ID);
            ArrayValues.Add(Invalid.CertificateNumber);
            ArrayValues.Add(Invalid.CertificateDate);
            ArrayValues.Add(Invalid.CertificateFrom);
            ArrayValues.Add(Invalid.CertificateFileID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Invalids", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public List<InvalidEntity> SP_GetList_Invalids(InvalidEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Invalids", ArrayValues, ArrayParams);

                List<InvalidEntity> list = new List<InvalidEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        InvalidEntity item = new InvalidEntity(
                                ID:(int)row["ID"],
                                PrisonerID:(int)row["PrisonerID"],
                                Date:(row["Date"] == DBNull.Value) ? null : (DateTime?)row["Date"],
                                Description:(row["Description"] == DBNull.Value) ? null : (string)row["Description"],
                                ClassLibItem_ID:(int)row["ClassLibItem_ID"],
                                CertificateNumber:(string)row["CertificateNumber"],
                                CertificateDate:(row["CertificateDate"] == DBNull.Value) ? null : (DateTime?)row["CertificateDate"],
                                CertificateFrom:(string)row["CertificateFrom"],
                                CertificateFileID:(row["CertificateFileID"] == DBNull.Value) ? null : (int?)row["CertificateFileID"],
                                ModifyDate:(DateTime?)row["ModifyDate"],
                                Status:(bool)row["Status"],
                                MergeStatus:(row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<InvalidEntity>();
            }
        }
        public bool SP_Update_Invalids(InvalidEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Date");
                ArrayParams.Add("ClassLibItem_ID");
                ArrayParams.Add("CertificateNumber");
                ArrayParams.Add("CertificateDate");
                ArrayParams.Add("CertificateFrom");
                ArrayParams.Add("CertificateFileID");
                ArrayParams.Add("Description");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Date);
                ArrayValues.Add(entity.ClassLibItem_ID);
                ArrayValues.Add(entity.CertificateNumber);
                ArrayValues.Add(entity.CertificateDate);
                ArrayValues.Add(entity.CertificateFrom);
                ArrayValues.Add(entity.CertificateFileID);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Invalids", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
