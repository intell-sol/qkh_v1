﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLayer.BusinessEntities;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_OrganizationServices : DataComm
    {
        public int? SP_Add_OrganizationUnit(int ParentID, int TypeID, bool Single, string Label, string Address, string Phone, string Fax, string Email, int? StateID = null, int? BindPositionID = null)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ParentID");
                ArrayValues.Add(ParentID);
                ArrayParams.Add("TypeID");
                ArrayValues.Add(TypeID);
                ArrayParams.Add("Single");
                ArrayValues.Add(Single);
                ArrayParams.Add("Label");
                ArrayValues.Add(Label);
                ArrayParams.Add("Address");
                ArrayValues.Add(Address);
                ArrayParams.Add("StateID");
                ArrayValues.Add(StateID);
                ArrayParams.Add("Phone");
                ArrayValues.Add(Phone);
                ArrayParams.Add("Fax");
                ArrayValues.Add(Fax);
                ArrayParams.Add("Email");
                ArrayValues.Add(Email);
                ArrayParams.Add("BindPositionID");
                ArrayValues.Add(BindPositionID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_OrganizationUnit", ArrayValues, ArrayParams);

                if (dt.Rows.Count == 0)
                    return -1;

                return Decimal.ToInt32((decimal)dt.Rows[0][0]);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public int? SP_Update_OrganizationUnit(int ID, string Label = null, string Address = null, string Phone = null, string Fax = null, string Email = null, bool? Status = null, int? StateID = null, int? ParentID = null, int? TypeID = null, bool? Single = null, int? BindPositionID = null)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(ID);
                ArrayParams.Add("TypeID");
                ArrayValues.Add(TypeID);
                ArrayParams.Add("Single");
                ArrayValues.Add(Single);
                ArrayParams.Add("Label");
                ArrayValues.Add(Label);
                ArrayParams.Add("Address");
                ArrayValues.Add(Address);
                ArrayParams.Add("StateID");
                ArrayValues.Add(StateID);
                ArrayParams.Add("Phone");
                ArrayValues.Add(Phone);
                ArrayParams.Add("Status");
                ArrayValues.Add(Status);
                ArrayParams.Add("Fax");
                ArrayValues.Add(Fax);
                ArrayParams.Add("Email");
                ArrayValues.Add(Email); ;
                ArrayParams.Add("BindPositionID");
                ArrayValues.Add(BindPositionID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_OrganizationUnit", ArrayValues, ArrayParams);

                if (dt == null)
                    return -1;

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public bool SP_Update_OrganizationUnitLevel(OrgUnitEntity item)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("SortLevel");
                ArrayValues.Add(item.ID);
                ArrayValues.Add(item.SortLevel);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_OrganizationUnitLevel", ArrayValues, ArrayParams);

                if (dt == null)
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        

        public int? SP_Add_PositionToOrganisation(int PositionID, int OrgUnitID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("PositionID");
                ArrayValues.Add(PositionID);
                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PositionToOrganisation", ArrayValues, ArrayParams);

                if (dt.Rows.Count == 0)
                    return -1;

                return Decimal.ToInt32((decimal)dt.Rows[0][0]);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public int? SP_Update_PositionToOrganization(int? ID = null, int? PositionID = null, bool Status = true)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(ID);
                ArrayParams.Add("PositionID");
                ArrayValues.Add(PositionID);
                ArrayParams.Add("Status");
                ArrayValues.Add(Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_PositionToOrganization", ArrayValues, ArrayParams);

                if (dt.Rows.Count == 0)
                    return -1;

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<OrgUnitEntity> SP_GetList_OrganizationUnit(int? ID = null, int? ParentID = null, int? TypeID = null)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(ID);
                ArrayParams.Add("ParentID");
                ArrayValues.Add(ParentID);
                ArrayParams.Add("TypeID");
                ArrayValues.Add(TypeID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_OrganizationUnit", ArrayValues, ArrayParams);

                List<OrgUnitEntity> list = new List<OrgUnitEntity>();
                foreach(DataRow row in dt.Rows)
                {
                    OrgUnitEntity item = new OrgUnitEntity(
                            ID:(int)row["ID"],
                            ParentID:(int)row["ParentID"],
                            TypeID:(short)row["TypeID"],
                            Single:(bool)row["Single"],
                            Label:(string)row["Label"],
                            Address:row.IsNull("Address")?"": (string)row["Address"],
                            StateID:row.IsNull("StateID") ? null : (int?)row["StateID"],
                            Phone:row.IsNull("Phone") ? "" : (string)row["Phone"],
                            Fax:row.IsNull("Fax") ? "" : (string)row["Fax"],
                            Email:row.IsNull("Email") ? "" : (string)row["Email"],
                            BindPositionID:row.IsNull("BindPositionID") ? null : (int?)row["BindPositionID"],
                            Editable:row.IsNull("Editable") ? false : (bool)row["Editable"]
                        );
                    item.SortLevel = (int)row["SortLevel"];
                    string IDList = row["IDList"] == DBNull.Value ? "" : (string)row["IDList"];
                    item.VisibleOrgUnits = IDList != String.Empty ? IDList.Split(',').Select(Int32.Parse).ToList() : new List<int>();

                    list.Add(item);
                }

                return list;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<OrgUnitEntity> SP_GetList_AvailableOrganizationUnits()
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_AvailableOrgUnits", ArrayValues, ArrayParams);

                List<OrgUnitEntity> list = new List<OrgUnitEntity>();
                foreach (DataRow row in dt.Rows)
                {
                    OrgUnitEntity item = new OrgUnitEntity(
                            (int)row["ID"],
                            (int)row["ParentID"],
                            (short)row["TypeID"],
                            (bool)row["Single"],
                            (string)row["Label"],
                            row.IsNull("Address") ? "" : (string)row["Address"],
                            row.IsNull("StateID") ? null : (int?)row["StateID"],
                            row.IsNull("Phone") ? "" : (string)row["Phone"],
                            row.IsNull("Fax") ? "" : (string)row["Fax"],
                            row.IsNull("Email") ? "" : (string)row["Email"],
                            row.IsNull("Editable") ? false : (bool)row["Editable"]
                        );
                    list.Add(item);
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        
        public List<Tuple<int, String>> SP_GetList_OrganizationStates()
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_OrganizationStates", ArrayValues, ArrayParams);

                List<Tuple<int, String>> list = new List<Tuple<int, String>>();
                foreach (DataRow row in dt.Rows)
                {
                    Tuple<int,string> state = new Tuple<int, string>(
                            (int)row["ID"],
                            (string)row["Name"]
                        );

                    list.Add(state);
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<OrgUnitEntity> SP_GetList_OrganizationUnitByTypeID(int TypeID = 3)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("TypeID");
                ArrayValues.Add(TypeID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_OrganizationUnitByTypeID", ArrayValues, ArrayParams);

                List<OrgUnitEntity> list = new List<OrgUnitEntity>();
                foreach (DataRow row in dt.Rows)
                {
                    OrgUnitEntity item = new OrgUnitEntity(
                            (int)row["ID"],
                            (int)row["ParentID"],
                            (short)row["TypeID"],
                            (bool)row["Single"],
                            (string)row["Label"],
                            row.IsNull("Address") ? "" : (string)row["Address"],
                            row.IsNull("StateID") ? null : (int?)row["StateID"],
                            row.IsNull("Phone") ? "" : (string)row["Phone"],
                            row.IsNull("Fax") ? "" : (string)row["Fax"],
                            row.IsNull("Email") ? "" : (string)row["Email"],
                            row.IsNull("Editable") ? false : (bool)row["Editable"]
                        );

                    list.Add(item);
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<OrgUnitEntity> SP_GetList_OrganizationBindPositions(int OrgUnitID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_OrganizationBindPositions", ArrayValues, ArrayParams);

                List<OrgUnitEntity> list = new List<OrgUnitEntity>();
                foreach (DataRow row in dt.Rows)
                {
                    OrgUnitEntity item = new OrgUnitEntity(
                            ID:(int)row["ID"],
                            ParentID: (int)row["ParentID"],
                            TypeID: (short)row["TypeID"],
                            Single: (bool)row["Single"],
                            Label:(string)row["Label"],
                            Address: row.IsNull("Address") ? "" : (string)row["Address"],
                            StateID: row.IsNull("StateID") ? null : (int?)row["StateID"],
                            Phone: row.IsNull("Phone") ? "" : (string)row["Phone"],
                            Fax: row.IsNull("Fax") ? "" : (string)row["Fax"],
                            Email: row.IsNull("Email") ? "" : (string)row["Email"],
                            Editable: row.IsNull("Editable") ? false : (bool)row["Editable"],
                            BindPositionID: row.IsNull("BindPositionID") ? null : (int?)row["BindPositionID"]
                        );

                    list.Add(item);
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<OrgUnitEntity> SP_GetList_OrganizationUnitByID(int ID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(ID);
                
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_OrganizationUnitByID", ArrayValues, ArrayParams);

                List<OrgUnitEntity> list = new List<OrgUnitEntity>();
                foreach (DataRow row in dt.Rows)
                {
                    OrgUnitEntity item = new OrgUnitEntity(
                            ID: (int)row["ID"],
                            ParentID:(int)row["ParentID"],
                            TypeID:(short)row["TypeID"],
                            Single:(bool)row["Single"],
                            Label:(string)row["Label"],
                            Address:row.IsNull("Address") ? "" : (string)row["Address"],
                            StateID:row.IsNull("StateID") ? null : (int?)row["StateID"],
                            Phone:row.IsNull("Phone") ? "" : (string)row["Phone"],
                            Fax:row.IsNull("Fax") ? "" : (string)row["Fax"],
                            Email:row.IsNull("Email") ? "" : (string)row["Email"],
                            Editable:row.IsNull("Editable") ? false : (bool)row["Editable"],
                            BindPositionID:row.IsNull("BindPositionID") ? null : (int?)row["BindPositionID"]
                        );
                    string IDList = row["IDList"] == DBNull.Value ? "" : (string)row["IDList"];
                    item.VisibleOrgUnits = IDList != String.Empty ? IDList.Split(',').Select(Int32.Parse).ToList() : new List<int>();

                    list.Add(item);
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }


        public List<OrgUnitEntity> SP_GetList_OrganizationUnit(int ID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(ID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_OrganizationUnit", ArrayValues, ArrayParams);

                List<OrgUnitEntity> list = new List<OrgUnitEntity>();
                foreach (DataRow row in dt.Rows)
                {
                    OrgUnitEntity item = new OrgUnitEntity(
                            (int)row["ID"],
                            (int)row["ParentID"],
                            (short)row["TypeID"],
                            (bool)row["Single"],
                            (string)row["Label"],
                            row.IsNull("Address") ? "" : (string)row["Address"],
                            row.IsNull("StateID") ? null : (int?)row["StateID"],
                            row.IsNull("Phone") ? "" : (string)row["Phone"],
                            row.IsNull("Fax") ? "" : (string)row["Fax"],
                            row.IsNull("Email") ? "" : (string)row["Email"],
                            row.IsNull("Editable") ? false : (bool)row["Editable"]
                        );
                    string IDList = row["IDList"] == DBNull.Value ? "" : (string)row["IDList"];
                    item.VisibleOrgUnits = IDList != String.Empty ? IDList.Split(',').Select(Int32.Parse).ToList() : new List<int>();

                    list.Add(item);
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<OrgUnitEntity> SP_GetList_ApproveOrganizationUnitByID(int OrgUnitID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_ApproveOrganizationUnitByOrgUnitID", ArrayValues, ArrayParams);

                List<OrgUnitEntity> list = new List<OrgUnitEntity>();
                foreach (DataRow row in dt.Rows)
                {
                    OrgUnitEntity item = new OrgUnitEntity(
                            (int)row["ID"],
                            (int)row["ParentID"],
                            (short)row["TypeID"],
                            (bool)row["Single"],
                            (string)row["Label"],
                            row.IsNull("Address") ? "" : (string)row["Address"],
                            row.IsNull("StateID") ? null : (int?)row["StateID"],
                            row.IsNull("Phone") ? "" : (string)row["Phone"],
                            row.IsNull("Fax") ? "" : (string)row["Fax"],
                            row.IsNull("Email") ? "" : (string)row["Email"],
                            row.IsNull("Editable") ? false : (bool)row["Editable"]
                        );

                    list.Add(item);
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<OrgTypeEntity> SP_GetList_OrganizationTypes(string Types = null)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("id");
                ArrayValues.Add(Types);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_OrganizationTypes", ArrayValues, ArrayParams);

                List<OrgTypeEntity> list = new List<OrgTypeEntity>();
                foreach (DataRow row in dt.Rows)
                {
                    OrgTypeEntity item = new OrgTypeEntity(
                            (short)row["ID"],
                            (string)row["Name"]
                        );

                    list.Add(item);
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<OrgUnitEntity> SP_GetList_AvailablePositions(int OrgUnitID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_AvailablePositions", ArrayValues, ArrayParams);

                List<OrgUnitEntity> list = new List<OrgUnitEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        OrgUnitEntity item = new OrgUnitEntity(
                                (int)row["ID"],
                                (int)row["ParentID"],
                                (short)row["TypeID"],
                                (bool)row["Single"],
                                (string)row["Label"],
                                row.IsNull("Address") ? "" : (string)row["Address"],
                                row.IsNull("StateID") ? null : (int?)row["StateID"],
                                row.IsNull("Phone") ? "" : (string)row["Phone"],
                                row.IsNull("Fax") ? "" : (string)row["Fax"],
                                row.IsNull("Email") ? "" : (string)row["Email"],
                                row.IsNull("Editable") ? false : (bool)row["Editable"]
                            );
                        string IDList = row["IDList"] == DBNull.Value ? "" : (string)row["IDList"];
                        item.VisibleOrgUnits = IDList != String.Empty ? IDList.Split(',').Select(Int32.Parse).ToList() : new List<int>();

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<OrgUnitEntity> SP_GetList_PositionsByOrgUnitID(int OrgUnitID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PositionsByOrgUnitID", ArrayValues, ArrayParams);

                List<OrgUnitEntity> list = new List<OrgUnitEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        OrgUnitEntity item = new OrgUnitEntity(
                                (int)row["ID"],
                                (int)row["ParentID"],
                                (short)row["TypeID"],
                                (bool)row["Single"],
                                (string)row["Label"],
                                row.IsNull("Address") ? "" : (string)row["Address"],
                                row.IsNull("StateID") ? null : (int?)row["StateID"],
                                row.IsNull("Phone") ? "" : (string)row["Phone"],
                                row.IsNull("Fax") ? "" : (string)row["Fax"],
                                row.IsNull("Email") ? "" : (string)row["Email"],
                                row.IsNull("Editable") ? false : (bool)row["Editable"]
                            );
                        string IDList = row["IDList"] == DBNull.Value ? "" : (string)row["IDList"];
                        item.VisibleOrgUnits = IDList != String.Empty ? IDList.Split(',').Select(Int32.Parse).ToList() : new List<int>();

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public int? SP_Add_Employees(int OrgUnitID, string PSN, string FirstName, string MiddleName, string LastName, DateTime BirthDay, string Passport, string Login, string Password, String Phone, String Fax, String Email, String RegistrationAddress, String LivingAddress, String Nationality, DateTime PassportDate, String PassportBy, int? fileID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();


                ArrayParams.Add("Phone");
                ArrayValues.Add(Phone);
                ArrayParams.Add("Fax");
                ArrayValues.Add(Fax);
                ArrayParams.Add("Email");
                ArrayValues.Add(Email);
                ArrayParams.Add("RegistrationAddress");
                ArrayValues.Add(RegistrationAddress);
                ArrayParams.Add("LivingAddress");
                ArrayValues.Add(LivingAddress);
                ArrayParams.Add("Nationality");
                ArrayValues.Add(Nationality);
                ArrayParams.Add("PassportDate");
                ArrayValues.Add(PassportDate);
                ArrayParams.Add("PassportBy");
                ArrayValues.Add(PassportBy);
                ArrayParams.Add("Photo");
                ArrayValues.Add(fileID);

                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);
                ArrayParams.Add("PSN");
                ArrayValues.Add(PSN);
                ArrayParams.Add("FirstName");
                ArrayValues.Add(FirstName);
                ArrayParams.Add("MiddleName");
                ArrayValues.Add(MiddleName);
                ArrayParams.Add("LastName");
                ArrayValues.Add(LastName);
                ArrayParams.Add("BirthDay");
                ArrayValues.Add(BirthDay);
                ArrayParams.Add("Passport");
                ArrayValues.Add(Passport);
                ArrayParams.Add("Login");
                ArrayValues.Add(Login);
                ArrayParams.Add("Password");
                ArrayValues.Add(Password);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Employees", ArrayValues, ArrayParams);

                if (dt == null || dt.Rows.Count == 0)
                    return 0;

                return Decimal.ToInt32((decimal)dt.Rows[0][0]);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<EmployeeEntity> SP_GetList_Employees(int? ID = null, string PSN = null, string FirstName = null, string LastName = null, int? OrgUnitID = null, bool? Status = null)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(ID);
                ArrayParams.Add("PSN");
                ArrayValues.Add(PSN);
                //ArrayParams.Add("FirstName");
                //ArrayValues.Add(FirstName);
                //ArrayParams.Add("LastName");
                //ArrayValues.Add(LastName);
                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);
                ArrayParams.Add("Status");
                ArrayValues.Add(Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Employees", ArrayValues, ArrayParams);

                List<EmployeeEntity> list = new List<EmployeeEntity>();
                if(dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        EmployeeEntity item = new EmployeeEntity(
                                (int)row["ID"],
                                (int)row["OrgUnitID"],
                                (string)row["PSN"],
                                row["FirstName"] == DBNull.Value ? null : (string)row["FirstName"],
                                row["LastName"] == DBNull.Value ? null : (string)row["LastName"]
                            );
                        item.MiddleName = row["MiddleName"] == DBNull.Value ? null : (string)row["MiddleName"];
                        item.BirthDay = row["BirthDay"] == DBNull.Value ? DateTime.MinValue : (DateTime)row["BirthDay"];
                        item.Passport = row["Passport"] == DBNull.Value ? null : (string)row["Passport"];
                        item.Login = row["Login"] == DBNull.Value ? null : (string)row["Login"];
                        item.Password = row["Password"] == DBNull.Value ? null : (string)row["Password"];
                        item.ChangePassword = (bool)row["ChangePassword"];
                        item.Editable = (bool)row["Editable"];
                        item.Phone = row["Phone"] == DBNull.Value ? null : (string)row["Phone"];
                        item.Email = row["Email"] == DBNull.Value ? null : (string)row["Email"];
                        item.RegistrationAddress = row["RegistrationAddress"] == DBNull.Value ? null : (string)row["RegistrationAddress"];
                        item.LivingAddress = row["LivingAddress"] == DBNull.Value ? null : (string)row["LivingAddress"];
                        item.Nationality = row["Nationality"] == DBNull.Value ? null : (string)row["Nationality"];
                        item.Fax = row["Fax"] == DBNull.Value ? null : (string)row["Fax"];

                        if (row["PassportDate"] != DBNull.Value) {
                            item.PassportDate = (DateTime)row["PassportDate"];
                        }

                        item.Passport = row["Passport"] == DBNull.Value ? null : (string)row["Passport"];
                        item.PassportBy = row["PassportBy"] == DBNull.Value ? null : (string)row["PassportBy"];
                        if (row["Photo"] != DBNull.Value)
                        {
                            item.Photo = (int)row["Photo"];
                        }
                        else
                        {
                            item.Photo = null;
                        }

                        OrderEntity orderEntity = new OrderEntity();
                        orderEntity.OrderTypeID = row["OrderTypeID"] == DBNull.Value ? null : (short?)row["OrderTypeID"];
                        orderEntity.OrderNumber = row["OrderNumber"] == DBNull.Value ? null : (string)row["OrderNumber"];
                        orderEntity.OrderBy = row["OrderBy"] == DBNull.Value ? null : (string)row["OrderBy"];
                        orderEntity.OrderDate = row["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)row["OrderDate"];
                        orderEntity.FileID = row["FileID"] == DBNull.Value ? null : (int?)row["FileID"];
                        item.Order = orderEntity;

                        list.Add(item);
                    }
                }
                

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        
        public List<EmployeeEntity> SP_GetList_OrganizationEmployees(int OrgUnitID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_OrganizationEmployees", ArrayValues, ArrayParams);

                List<EmployeeEntity> list = new List<EmployeeEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        EmployeeEntity item = new EmployeeEntity(
                                (int)row["ID"],
                                (int)row["OrgUnitID"],
                                (string)row["PSN"],
                                row["FirstName"] == DBNull.Value ? null : (string)row["FirstName"],
                                row["LastName"] == DBNull.Value ? null : (string)row["LastName"]
                            );
                        item.MiddleName = row["MiddleName"] == DBNull.Value ? null : (string)row["MiddleName"];
                        item.BirthDay = row["BirthDay"] == DBNull.Value ? DateTime.MinValue : (DateTime)row["BirthDay"];
                        item.Passport = row["Passport"] == DBNull.Value ? null : (string)row["Passport"];
                        item.Login = row["Login"] == DBNull.Value ? null : (string)row["Login"];
                        item.Password = row["Password"] == DBNull.Value ? null : (string)row["Password"];
                        item.ChangePassword = (bool)row["ChangePassword"];
                        item.Editable = (bool)row["Editable"];
                        item.Phone = row["Phone"] == DBNull.Value ? null : (string)row["Phone"];
                        item.Email = row["Email"] == DBNull.Value ? null : (string)row["Email"];
                        item.RegistrationAddress = row["RegistrationAddress"] == DBNull.Value ? null : (string)row["RegistrationAddress"];
                        item.LivingAddress = row["LivingAddress"] == DBNull.Value ? null : (string)row["LivingAddress"];
                        item.Nationality = row["Nationality"] == DBNull.Value ? null : (string)row["Nationality"];
                        item.Fax = row["Fax"] == DBNull.Value ? null : (string)row["Fax"];

                        if (row["PassportDate"] != DBNull.Value)
                        {
                            item.PassportDate = (DateTime)row["PassportDate"];
                        }

                        item.Passport = row["Passport"] == DBNull.Value ? null : (string)row["Passport"];
                        item.PassportBy = row["PassportBy"] == DBNull.Value ? null : (string)row["PassportBy"];
                        if (row["Photo"] != DBNull.Value)
                        {
                            item.Photo = (int)row["Photo"];
                        }
                        else
                        {
                            item.Photo = null;
                        }

                        //OrderEntity orderEntity = new OrderEntity();
                        //orderEntity.OrderTypeID = row["OrderTypeID"] == DBNull.Value ? null : (short?)row["OrderTypeID"];
                        //orderEntity.OrderNumber = row["OrderNumber"] == DBNull.Value ? null : (string)row["OrderNumber"];
                        //orderEntity.OrderBy = row["OrderBy"] == DBNull.Value ? null : (string)row["OrderBy"];
                        //orderEntity.OrderDate = row["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)row["OrderDate"];
                        //orderEntity.FileID = row["FileID"] == DBNull.Value ? null : (int?)row["FileID"];
                        //item.Order = orderEntity;

                        list.Add(item);
                    }
                }


                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public int? SP_Update_Employees(int? ID = null, string PSN = null, int? OrgUnitID = null, string FirstName = null, string MiddleName = null, string LastName = null, DateTime? BirthDay = null, string Passport = null, string Login = null, string Password = null, String Phone = null, String Fax = null, String Email = null, bool? ChangePassword = null, bool ? Status = null, String RegistrationAddress = null, String LivingAddress = null, String Nationality = null, DateTime? PassportDate = null, String PassportBy = null, int? fileID= null)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("Phone");
                ArrayValues.Add(Phone);
                ArrayParams.Add("Fax");
                ArrayValues.Add(Fax);
                ArrayParams.Add("Email");
                ArrayValues.Add(Email);
                ArrayParams.Add("RegistrationAddress");
                ArrayValues.Add(RegistrationAddress);
                ArrayParams.Add("LivingAddress");
                ArrayValues.Add(LivingAddress);
                ArrayParams.Add("Nationality");
                ArrayValues.Add(Nationality);
                ArrayParams.Add("PassportDate");
                ArrayValues.Add(PassportDate);
                ArrayParams.Add("PassportBy");
                ArrayValues.Add(PassportBy);
                ArrayParams.Add("Photo");
                ArrayValues.Add(fileID);

                ArrayParams.Add("ID");
                ArrayValues.Add(ID);
                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);
                ArrayParams.Add("PSN");
                ArrayValues.Add(PSN);
                ArrayParams.Add("FirstName");
                ArrayValues.Add(FirstName);
                ArrayParams.Add("MiddleName");
                ArrayValues.Add(MiddleName);
                ArrayParams.Add("LastName");
                ArrayValues.Add(LastName);
                ArrayParams.Add("BirthDay");
                ArrayValues.Add(BirthDay);
                ArrayParams.Add("Passport");
                ArrayValues.Add(Passport);
                ArrayParams.Add("Login");
                ArrayValues.Add(Login);
                ArrayParams.Add("Password");
                ArrayValues.Add(Password);
                ArrayParams.Add("ChangePassword");
                ArrayValues.Add(ChangePassword);
                ArrayParams.Add("Status");
                ArrayValues.Add(Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Employees", ArrayValues, ArrayParams);

                if (dt == null)
                    return -1;

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        public EmployeeEntity SP_Check_Employee(string PSN)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("PSN");
                ArrayValues.Add(PSN);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Check_Employee", ArrayValues, ArrayParams);

                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        EmployeeEntity item = new EmployeeEntity(
                                (int)row["ID"],
                                (int)row["OrgUnitID"],
                                (string)row["PSN"],
                                row["FirstName"] == DBNull.Value ? null : (string)row["FirstName"],
                                row["LastName"] == DBNull.Value ? null : (string)row["LastName"]
                            );
                        item.MiddleName = row["MiddleName"] == DBNull.Value ? null : (string)row["MiddleName"];
                        item.BirthDay = row["BirthDay"] == DBNull.Value ? DateTime.MinValue : (DateTime)row["BirthDay"];
                        item.Passport = row["Passport"] == DBNull.Value ? null : (string)row["Passport"];
                        item.Login = row["Login"] == DBNull.Value ? null : (string)row["Login"];
                        item.Password = row["Password"] == DBNull.Value ? null : (string)row["Password"];
                        item.ChangePassword = (bool)row["ChangePassword"];
                        item.Editable = (bool)row["Editable"];
                        item.Phone = row["Phone"] == DBNull.Value ? null : (string)row["Phone"];
                        item.Email = row["Email"] == DBNull.Value ? null : (string)row["Email"];
                        item.RegistrationAddress = row["RegistrationAddress"] == DBNull.Value ? null : (string)row["RegistrationAddress"];
                        item.LivingAddress = row["LivingAddress"] == DBNull.Value ? null : (string)row["LivingAddress"];
                        item.Nationality = row["Nationality"] == DBNull.Value ? null : (string)row["Nationality"];
                        item.Fax = row["Fax"] == DBNull.Value ? null : (string)row["Fax"];

                        if (row["PassportDate"] != DBNull.Value)
                        {
                            item.PassportDate = (DateTime)row["PassportDate"];
                        }

                        item.Passport = row["Passport"] == DBNull.Value ? null : (string)row["Passport"];
                        item.PassportBy = row["PassportBy"] == DBNull.Value ? null : (string)row["PassportBy"];
                        if (row["Photo"] != DBNull.Value)
                        {
                            item.Photo = (int)row["Photo"];
                        }
                        else
                        {
                            item.Photo = null;
                        }

                        OrderEntity orderEntity = new OrderEntity();
                        orderEntity.OrderTypeID = row["OrderTypeID"] == DBNull.Value ? null : (short?)row["OrderTypeID"];
                        orderEntity.OrderNumber = row["OrderNumber"] == DBNull.Value ? null : (string)row["OrderNumber"];
                        orderEntity.OrderBy = row["OrderBy"] == DBNull.Value ? null : (string)row["OrderBy"];
                        orderEntity.OrderDate = row["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)row["OrderDate"];
                        orderEntity.FileID = row["FileID"] == DBNull.Value ? null : (int?)row["FileID"];
                        item.Order = orderEntity;

                        return item;
                    }
                    return null;
                }
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public Boolean SP_Check_UserName(string Login, int? UserID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("Login");
                ArrayValues.Add(Login);
                ArrayParams.Add("UserID");
                ArrayValues.Add(UserID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Check_EmployeesUserName", ArrayValues, ArrayParams);

                if (dt.Rows.Count == 0)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return true;
            }
        }
    }
}
