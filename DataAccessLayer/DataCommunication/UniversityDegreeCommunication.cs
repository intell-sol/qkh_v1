﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_UniversityDegreeService:DataComm
    {
        public int? SP_Add_EducationAwards(EducationAwardsEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("LibItemID");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.LibItemID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_EducationAwards", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public int? SP_Add_UniversityDegree(UniversityDegreeEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("EducationProfessionsID");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("AcademicDegreeLibItemID");
            ArrayParams.Add("DegreeLibItemID");

            ArrayValues.Add(entity.EducationProfessionsID);
            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.AcademicDegreeLibItemID);
            ArrayValues.Add(entity.DegreeLibItemID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_UniversityDegree", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public UniversityDegreeEntity SP_GetList_UniversityDegree(UniversityDegreeEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("EducationProfessionsID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.EducationProfessionsID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_UniversityDegree", ArrayValues, ArrayParams);

                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        UniversityDegreeEntity item = new UniversityDegreeEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                EducationProfessionsID: (int)row["EducationProfessionsID"],
                                AcademicDegreeLibItemID: (row["AcademicDegreeLibItemID"] == DBNull.Value) ? null : (int?)row["AcademicDegreeLibItemID"],
                                DegreeLibItemID: (row["DegreeLibItemID"] == DBNull.Value) ? null : (int?)row["DegreeLibItemID"]
                            );

                        return item;
                    }
                }

                return new UniversityDegreeEntity();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new UniversityDegreeEntity();
            }
        }

        public List<EducationAwardsEntity> SP_GetList_EducationAwards(int? ID = null, int? PrisonerID = null, int? LibItemID = null)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayParams.Add("LibItemID");

                ArrayValues.Add(ID);
                ArrayValues.Add(PrisonerID);
                ArrayValues.Add(LibItemID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_EducationAwards", ArrayValues, ArrayParams);


                List<EducationAwardsEntity> list = new List<EducationAwardsEntity>();

                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        EducationAwardsEntity item = new EducationAwardsEntity(
                                (int)row["ID"],
                                (int)row["PrisonerID"],
                                (int)row["LibItemID"],
                                (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<EducationAwardsEntity>();
            }
        }
        public bool SP_Update_EducationAwards(EducationAwardsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("LibItemID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.LibItemID);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_EducationAwards", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public bool SP_Update_UniversityDegree(UniversityDegreeEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("AcademicDegreeLibItemID");
                ArrayParams.Add("DegreeLibItemID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.AcademicDegreeLibItemID);
                ArrayValues.Add(entity.DegreeLibItemID);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_UniversityDegree", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
