﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_WarInvolvedService:DataComm
    {
        public int? SP_Add_WarInvolved(WarInvolvedEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("LibItemID");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.LibItemID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_WarInvolved", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public List<WarInvolvedEntity> SP_GetList_WarInvolved(WarInvolvedEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_WarInvolved", ArrayValues, ArrayParams);

                List<WarInvolvedEntity> list = new List<WarInvolvedEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        WarInvolvedEntity item = new WarInvolvedEntity(
                                (int)row["ID"],
                                (int)row["PrisonerID"],
                                (int)row["LibItemID"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<WarInvolvedEntity>();
            }
        }

        public bool SP_Update_WarInvolved(WarInvolvedEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("LibItemID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.LibItemID);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_WarInvolved", ArrayValues, ArrayParams);


                if (dt != null && dt.Rows.Count > 0)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
