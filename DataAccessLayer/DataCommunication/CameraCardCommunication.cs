﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_CameraCardService:DataComm
    {
        public int? SP_Add_CameraCard(CameraCardItemEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("Number");
            ArrayParams.Add("InputDate");
            ArrayParams.Add("OutputDate");
            ArrayParams.Add("EmployeeID");
            ArrayParams.Add("Note");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.OrgUnitID);
            ArrayValues.Add(entity.Number);
            ArrayValues.Add(entity.InputDate);
            ArrayValues.Add(entity.OutputDate);
            ArrayValues.Add(entity.EmployeeID);
            ArrayValues.Add(entity.Note);


            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_CameraCard", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<CameraCardItemEntity> SP_GetList_CameraCard(CameraCardItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_CameraCard", ArrayValues, ArrayParams);

                List<CameraCardItemEntity> list = new List<CameraCardItemEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        CameraCardItemEntity item = new CameraCardItemEntity(
                                ID: (int)row["ID"],
                                PrisonerID:(int)row["PrisonerID"],
                                OrgUnitID:(int)row["OrgUnitID"],
                                Number:(int)row["Number"],
                                OrgUnitName: (string)row["OrgUnitName"],
                                InputDate:(row["InputDate"] == DBNull.Value) ? null : (DateTime?)row["InputDate"],
                                OutputDate:(row["OutputDate"] == DBNull.Value) ? null : (DateTime?)row["OutputDate"],
                                EmployeeID:(int)row["EmployeeID"],
                                Note:(row["Note"] == DBNull.Value) ? null : (string)row["Note"],
                                MergeStatus:(row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<CameraCardItemEntity>();
            }
        }

        public bool SP_Update_CameraCard(CameraCardItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Number");
                ArrayParams.Add("InputDate");
                ArrayParams.Add("OutputDate");
                ArrayParams.Add("EmployeeID");
                ArrayParams.Add("Note");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Number);
                ArrayValues.Add(entity.InputDate);
                ArrayValues.Add(entity.OutputDate);
                ArrayValues.Add(entity.EmployeeID);
                ArrayValues.Add(entity.Note);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_CameraCard", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
