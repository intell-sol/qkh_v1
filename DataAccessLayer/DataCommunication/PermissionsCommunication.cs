﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PermissionsService:DataComm
    {
        public int? SP_Add_Permissions(int ParentID, string Name, string Title, int Type)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("ParentID");
            ArrayParams.Add("Name");
            ArrayParams.Add("Title");
            ArrayParams.Add("Type");

            ArrayValues.Add(ParentID);
            ArrayValues.Add(Name);
            ArrayValues.Add(Title);
            ArrayValues.Add(Type);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Permissions", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
    }
}
