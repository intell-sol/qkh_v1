﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PersonsService:DataComm
    {
        public int? SP_Add_Persons(PersonEntity person)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("Personal_ID");
            ArrayParams.Add("FirstName");
            ArrayParams.Add("MiddleName");
            ArrayParams.Add("LastName");
            ArrayParams.Add("Birthday");
            ArrayParams.Add("SexLibItem_ID");
            ArrayParams.Add("NationalityID");
            ArrayParams.Add("isCustom");
            ArrayParams.Add("Registration_ID");
            ArrayParams.Add("Living_ID");

            ArrayValues.Add(person.Personal_ID);
            ArrayValues.Add(person.FirstName);
            ArrayValues.Add(person.MiddleName);
            ArrayValues.Add(person.LastName);
            ArrayValues.Add(person.Birthday);
            ArrayValues.Add(person.SexLibItem_ID);
            ArrayValues.Add(person.NationalityID);
            ArrayValues.Add(person.isCustom);
            ArrayValues.Add(person.Registration_ID);
            ArrayValues.Add(person.Living_ID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Persons", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public List<PersonEntity> SP_GetList_Persons(PersonEntity person)
        {
            
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Personal_ID");

                ArrayValues.Add(person.ID);
                ArrayValues.Add(person.Personal_ID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Persons", ArrayValues, ArrayParams);

                List<PersonEntity> list = new List<PersonEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PersonEntity item = new PersonEntity(
                                ID:(int)row["ID"],
                                Personal_ID: (string)row["Personal_ID"],
                                FirstName: (string)row["FirstName"],
                                MiddleName: (row["MiddleName"] == DBNull.Value) ? null : (string)row["MiddleName"],
                                LastName: (row["LastName"] == DBNull.Value) ? null : (string)row["LastName"],
                                Birthday: (row["Birthday"] == DBNull.Value) ? null : (DateTime?)row["Birthday"],
                                SexLibItem_ID: (row["SexLibItem_ID"] == DBNull.Value) ? null : (int?)row["SexLibItem_ID"],
                                NationalityID: (row["NationalityID"] == DBNull.Value) ? null : (int?)row["NationalityID"],
                                Registration_ID: (row["Registration_ID"] == DBNull.Value) ? null : (int?)row["Registration_ID"],
                                Living_ID: (row["Living_ID"] == DBNull.Value) ? null : (int?)row["Living_ID"],
                                Registration_address: (row["Registration_address"] == DBNull.Value) ? null : (string)row["Registration_address"],
                                Living_place: (row["Living_place"] == DBNull.Value) ? null : (string)row["Living_place"],
                                isCustom: (row["isCustom"] == DBNull.Value) ? false : (bool?)row["isCustom"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PersonEntity>();
            }
        }

        public List<PersonEntity> SP_GetList_PersonsByPrisonerID(int PrisonerID)
        {

            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PersonsByPrisonerID", ArrayValues, ArrayParams);

                List<PersonEntity> list = new List<PersonEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PersonEntity item = new PersonEntity(
                                ID:(int)row["ID"],
                                Personal_ID:(string)row["Personal_ID"],
                                FirstName:(row["FirstName"] == DBNull.Value) ? null : (string)row["FirstName"],
                                MiddleName: (row["MiddleName"] == DBNull.Value) ? null : (string)row["MiddleName"],
                                LastName: (row["LastName"] == DBNull.Value) ? null : (string)row["LastName"],
                                Birthday: (row["Birthday"] == DBNull.Value) ? null : (DateTime?)row["Birthday"],
                                SexLibItem_ID: (row["SexLibItem_ID"] == DBNull.Value) ? null : (int?)row["SexLibItem_ID"],
                                NationalityID: (row["NationalityID"] == DBNull.Value) ? null : (int?)row["NationalityID"],
                                Registration_address: (row["Registration_address"] == DBNull.Value) ? null : (string)row["Registration_address"],
                                Living_place: (row["Living_place"] == DBNull.Value) ? null : (string)row["Living_place"],
                                isCustom: (row["isCustom"] == DBNull.Value) ? false : (bool?)row["isCustom"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PersonEntity>();
            }
        }

        public bool SP_Update_Persons(PersonEntity person)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();
                                
                ArrayParams.Add("ID");
                ArrayParams.Add("Personal_ID");
                ArrayParams.Add("FirstName");
                ArrayParams.Add("MiddleName");
                ArrayParams.Add("LastName");
                ArrayParams.Add("Birthday");
                ArrayParams.Add("SexLibItem_ID");
                ArrayParams.Add("NationalityID");
                ArrayParams.Add("Registration_ID");
                ArrayParams.Add("Living_ID");
                ArrayParams.Add("Status");

                ArrayValues.Add(person.ID);
                ArrayValues.Add(person.Personal_ID);
                ArrayValues.Add(person.FirstName);
                ArrayValues.Add(person.MiddleName);
                ArrayValues.Add(person.LastName);
                ArrayValues.Add(person.Birthday);
                ArrayValues.Add(person.SexLibItem_ID);
                ArrayValues.Add(person.NationalityID);
                ArrayValues.Add(person.Registration_ID);
                ArrayValues.Add(person.Living_ID);
                ArrayValues.Add(person.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Persons", ArrayValues, ArrayParams);

                
                if (dt != null )
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
