﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PrisonersToLibsService:DataComm
    {
        public int? SP_Add_PrisonersToLibs(int PrisonerID, int ListLib_ID, int ListItemLib_ID)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("ListLib_ID");
            ArrayParams.Add("ListItemLib_ID");

            ArrayValues.Add(PrisonerID);
            ArrayValues.Add(ListLib_ID);
            ArrayValues.Add(ListItemLib_ID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PrisonersToLibs", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
    }
}
