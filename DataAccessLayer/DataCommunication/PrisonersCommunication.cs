﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PrisonersService:DataComm
    {
        public List<PrisonerEntity> SP_GetList_PrisonersForDashboard(FilterPrisonerEntity PrisonerData) {

            ArrayParams.Clear();
            ArrayValues.Clear();

            int TotalCount = 0;

            ArrayParams.Add("Type");
            ArrayParams.Add("Personal_ID");
            ArrayParams.Add("PersonalCaseNumber");
            ArrayParams.Add("FirstName");
            ArrayParams.Add("MiddleName");
            ArrayParams.Add("LastName");
            ArrayParams.Add("CitizenshipLibItemIDList");
            ArrayParams.Add("SexLibItem_ID");
            ArrayParams.Add("NationalityID");
            ArrayParams.Add("State");
            ArrayParams.Add("ArchiveStatus");
            ArrayParams.Add("OrgUnitIDList");
            ArrayParams.Add("CurrentPage");
            ArrayParams.Add("ViewCount");

            ArrayValues.Add(PrisonerData.Type);
            ArrayValues.Add(PrisonerData.Personal_ID);
            ArrayValues.Add(PrisonerData.PersonalCaseNumber);
            ArrayValues.Add(PrisonerData.FirstName);
            ArrayValues.Add(PrisonerData.MiddleName);
            ArrayValues.Add(PrisonerData.LastName);
            ArrayValues.Add((PrisonerData.CitizenshipLibItemIDList != null && PrisonerData.CitizenshipLibItemIDList.Any())?string.Join(",",PrisonerData.CitizenshipLibItemIDList):null);
            ArrayValues.Add(PrisonerData.SexLibItem_ID);
            ArrayValues.Add(PrisonerData.NationalityID);
            ArrayValues.Add(PrisonerData.State);
            ArrayValues.Add(PrisonerData.ArchiveStatus);
            ArrayValues.Add((PrisonerData.OrgUnitList != null && PrisonerData.OrgUnitList.Any())? string.Join(",", PrisonerData.OrgUnitList):PrisonerData.OrgUnitID.ToString());
            ArrayValues.Add(PrisonerData.paging.CurrentPage);
            ArrayValues.Add(PrisonerData.paging.ViewCount);

            ArrayList OutParams = new ArrayList();
            Dictionary<string,object> OutValues = new Dictionary<string, object>();

            OutParams.Add("TotalCount");
            OutValues.Add("TotalCount",TotalCount);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Prisoners_ForDashboard", ArrayValues, ArrayParams, ref OutValues, OutParams);

                List<PrisonerEntity> list = new List<PrisonerEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PrisonerEntity item = new PrisonerEntity(
                                (int)row["ID"],
                                (int)row["PersonID"],
                                (int)row["PersonalCaseNumber"],
                                (int)row["Type"],
                                State: (bool)row["State"],
                                ArchiveStatus: (bool)row["ArchiveStatus"]
                            );
                        item.Person = new PersonEntity(ID:(int)row["PersonID"],
                            Personal_ID:(string)row["Personal_ID"],
                            FirstName: (row["FirstName"] == DBNull.Value) ? null : (string)row["FirstName"],
                            MiddleName:(row["MiddleName"] == DBNull.Value) ? null : (string)row["MiddleName"],
                            LastName:(row["LastName"] == DBNull.Value) ? null : (string)row["LastName"],
                            Birthday:(row["Birthday"] == DBNull.Value) ? null : (DateTime?)row["Birthday"],
                            SexLibItem_ID:(row["SexLibItem_ID"] == DBNull.Value) ? null : (int?)row["SexLibItem_ID"],
                            NationalityID:(row["NationalityID"] == DBNull.Value) ? null : (int?)row["NationalityID"], 
                            Registration_address: (row["Registration_address"] == DBNull.Value) ? null :(string)row["Registration_address"], Living_place: (row["Living_place"] == DBNull.Value) ? null : (string)row["Living_place"]);
                        item.Person.NationalityLabel = (string)row["NationalityLabel"];
                        item.Person.IdentificationDocuments = new List<IdentificationDocument>();
                        IdentificationDocument idoc = new IdentificationDocument();
                        idoc.CitizenshipLibItem_Name = (string)row["CitizenshipName"];
                        item.Person.IdentificationDocuments.Add(idoc);

                        list.Add(item);
                    }
                }

                PrisonerData.paging.TotalCount = (int)OutValues["TotalCount"];
                PrisonerData.paging.TotalPage = (int)Math.Ceiling(Convert.ToDouble(OutValues["TotalCount"]) / PrisonerData.paging.ViewCount);
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                PrisonerData.paging.TotalPage = 0;
                return new List<PrisonerEntity>();
            }
        }
        public List<PrisonerEntity> SP_GetList_PrisonersBySearch (string Query)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("Query");
            ArrayValues.Add(Query);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PrisonersBySearch", ArrayValues, ArrayParams);

                List<PrisonerEntity> list = new List<PrisonerEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PrisonerEntity item = new PrisonerEntity(
                                (int)row["ID"],
                                (int)row["PersonID"],
                                (int)row["PersonalCaseNumber"],
                                (int)row["Type"],
                                State: (bool)row["State"],
                                ArchiveStatus: (bool)row["ArchiveStatus"]
                            );
                        item.Person = new PersonEntity((int)row["PersonID"], (string)row["Personal_ID"], (string)row["FirstName"], (string)row["MiddleName"], (string)row["LastName"],
                            (DateTime)row["Birthday"], (int)row["SexLibItem_ID"], (int)row["NationalityID"], Registration_address: (row["Registration_address"] == DBNull.Value) ? null : (string)row["Registration_address"], Living_place: (row["Living_place"] == DBNull.Value) ? null : (string)row["Living_place"]);

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PrisonerEntity>();
            }
        }
        public int? SP_Check_ConflictPersonsTogether(int PrisonerID, int? Number = null)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayValues.Add(PrisonerID);
            ArrayParams.Add("Number");
            ArrayValues.Add(Number);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Check_ConflictPersonsTogether", ArrayValues, ArrayParams);

                if (dt != null && dt.Rows.Count != 0)
                    return (dt.Rows[0]["RESULT"] == DBNull.Value) ? null : (int?)dt.Rows[0]["RESULT"];

                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public int? SP_Add_Prisoners(PrisonerEntity PrisonerData)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PersonID");
            ArrayParams.Add("PersonalCaseNumber");
            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("EmployeeID");
            ArrayParams.Add("Type");
            ArrayParams.Add("State");
            ArrayParams.Add("ArchiveData");

            ArrayValues.Add(PrisonerData.PersonID);
            ArrayValues.Add(PrisonerData.PersonalCaseNumber);
            ArrayValues.Add(PrisonerData.OrgUnitID);
            ArrayValues.Add(PrisonerData.EmployeeID);
            ArrayValues.Add(PrisonerData.Type);
            ArrayValues.Add(PrisonerData.State);
            ArrayValues.Add(PrisonerData.ArchiveData);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Prisoners", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<PrisonerEntity> SP_GetList_Prisoners(int? ID = null, int? Personal_ID = null)
        {
            
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(ID);
                ArrayParams.Add("PersonID");
                ArrayValues.Add(Personal_ID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Prisoners", ArrayValues, ArrayParams);

                List<PrisonerEntity> list = new List<PrisonerEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PrisonerEntity item = new PrisonerEntity(
                                ID: (int)row["ID"],
                                PersonID: (int)row["PersonID"],
                                PersonalCaseNumber: (int)row["PersonalCaseNumber"],
                                Type: (int)row["Type"],
                                State: (bool)row["State"],
                                ArchiveStatus: (bool)row["ArchiveStatus"],
                                ArchiveData: (row["ArchiveData"] == DBNull.Value) ? null : (bool?)row["ArchiveData"]
                            );
                        item.OrgUnitID = (int)row["OrgUnitID"];

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PrisonerEntity>();
            }
        }
        public bool SP_Update_Prisoners(PrisonerEntity entity)
        {

            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();
                
                ArrayParams.Add("ID");
                ArrayParams.Add("PersonalCaseNumber");
                ArrayParams.Add("OrgUnitID");
                ArrayParams.Add("Type");
                ArrayParams.Add("Status");
                ArrayParams.Add("State");


                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PersonalCaseNumber);
                ArrayValues.Add(entity.OrgUnitID);
                ArrayValues.Add(entity.Type);
                ArrayValues.Add(entity.Status);
                ArrayValues.Add(entity.State);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Prisoners", ArrayValues, ArrayParams);

                
                if (dt != null && dt.Rows.Count>0)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
