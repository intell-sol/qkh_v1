﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_LibsServices:DataComm
    {

        public int? SP_Add_LibPath(string Name, int ParentID)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("Name");
            ArrayParams.Add("ParentID");
            ArrayValues.Add(Name);
            ArrayValues.Add(ParentID);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_LibPath", ArrayValues, ArrayParams);
                
                if (dt.Rows.Count == 0)
                    return -1;


                return 0;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public int? SP_Add_Libs(string Name, int? ParentID = 0)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("Name");
            ArrayParams.Add("ParentID");
            ArrayValues.Add(Name);
            ArrayValues.Add(ParentID);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Libs", ArrayValues, ArrayParams);

                Decimal decId = (Decimal) dt.Rows[0][0];

                int newId = Decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public int? SP_Add_LibPathToLibs(int? PathID = null, int? LibID = null)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("PathID");
            ArrayParams.Add("LibID");
            ArrayValues.Add(PathID);
            ArrayValues.Add(LibID);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_LibPathToLibs", ArrayValues, ArrayParams);

                Decimal decId = (Decimal)dt.Rows[0][0];

                int newId = Decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public List<LibPathEntity> SP_GetList_LibPathWay(int? ParentID = null)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("ParentID");
            ArrayValues.Add(ParentID);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_LibPathWay", ArrayValues, ArrayParams);

                List<LibPathEntity> returnValue = new List<LibPathEntity>();

                foreach (DataRow dr in dt.Rows)
                {
                    LibPathEntity currentLib = new LibPathEntity((int)dr["ID"], (string)dr["Name"], (int)dr["ParentID"]);
                    returnValue.Add(currentLib);
                }
                return returnValue;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public List<LibPathEntity> SP_Get_LibPathsWithCountByParentID(int? ParentID = null)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("ParentID");
            ArrayValues.Add(ParentID);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Get_LibPathsWithCountByParentID", ArrayValues, ArrayParams);

                List<LibPathEntity> returnValue = new List<LibPathEntity>();

                foreach (DataRow dr in dt.Rows)
                {
                    LibPathEntity currentLib = new LibPathEntity((int)dr["ID"], (string)dr["Name"], (int)dr["ParentID"]);
                    currentLib.LibsCount = (int)dr["ChildCount"];
                    returnValue.Add(currentLib);
                }
                return returnValue;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public LibPathEntity SP_GetList_LibPathByID(int ID)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("ID");
            ArrayValues.Add(ID);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_LibPathByID", ArrayValues, ArrayParams);

                foreach (DataRow dr in dt.Rows)
                {
                    LibPathEntity currentLib = new LibPathEntity((int)dr["ID"], (string)dr["Name"], (int)dr["ParentID"]);
                    return currentLib;
                }
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<LibPathEntity> SP_Get_LibPath(int? ParentID = null)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("ParentID");
            ArrayValues.Add(ParentID);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_LibPath", ArrayValues, ArrayParams);

                List<LibPathEntity> returnValue = new List<LibPathEntity>();

                foreach (DataRow dr in dt.Rows)
                {
                    LibPathEntity currentLib = new LibPathEntity((int)dr["ID"], (string)dr["Name"], (int)dr["ParentID"]);
                    returnValue.Add(currentLib);
                }
                return returnValue;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public LibsEntity SP_Get_Lib(int? ID = null)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("ID");
            ArrayValues.Add(ID);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Libs", ArrayValues, ArrayParams);
                LibsEntity returnValue = new LibsEntity();
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        returnValue = new LibsEntity((int)dr["ID"], (string)dr["Name"], (int)dr["ParentID"]);
                    }
                }
                return returnValue;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public List<LibsEntity> SP_GetList_LibsByLibPathID(int LibPathID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("LibPathID");
                ArrayValues.Add(LibPathID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_LibsByLibPathID", ArrayValues, ArrayParams);
                
                List<LibsEntity> returnValue = new List<LibsEntity>();
                if(dt != null)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        LibsEntity currentLib = new LibsEntity((int)dr["ID"], (string)dr["Name"], (int)dr["ParentID"], (Boolean)dr["Editable"], (byte)dr["Depth"]);
                        returnValue.Add(currentLib);
                    }
                }
                return returnValue;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        public List<LibsEntity> SP_GetList_LibsLocalTreeByLibPathID(int LibPathID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("LibPathID");
                ArrayValues.Add(LibPathID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_LibsLocalTreeByLibPathID", ArrayValues, ArrayParams);

                List<LibsEntity> returnValue = new List<LibsEntity>();
                if (dt != null)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        LibsEntity currentLib = new LibsEntity((int)dr["ID"], (string)dr["Name"], (int)dr["ParentID"], (Boolean)dr["Editable"], (byte)dr["Depth"]);
                        returnValue.Add(currentLib);
                    }
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        public List<LibsEntity> SP_GetList_LibsLocalTreeWithPropertiesByLibParentID(int ParentID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ParentID");
                ArrayValues.Add(ParentID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_LibsLocalTreeWithPropertiesByLibParentID", ArrayValues, ArrayParams);

                List<LibsEntity> returnValue = new List<LibsEntity>();
                if (dt != null)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        LibsEntity currentLib = new LibsEntity((int)dr["ID"], (string)dr["Name"], (int)dr["ParentID"], (Boolean)dr["Editable"], (byte)dr["Depth"]);
                        if (dr["PropertyID"] != DBNull.Value)
                        {
                            PropsEntity currentProperty = new PropsEntity(ID: (int)dr["PropertyID"], Name: (string)dr["PropertyName"]);
                            PropValuesEntity currentPropertyValue = new PropValuesEntity(ID: (int)dr["PropertyValueID"], Value: (string)dr["PropertyValue"]);
                            currentProperty.Values.Add(currentPropertyValue);
                        }
                        if (!returnValue.FindAll(r => r.ID == (int)dr["ID"]).Any())
                        {
                            returnValue.Add(currentLib);
                        }
                    }
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        public List<LibsEntity> SP_GetList_LibsLocalTreeWithPropertiesByLibPathID(int LibPathID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("LibPathID");
                ArrayValues.Add(LibPathID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_LibsLocalTreeWithPropertiesByLibPathID", ArrayValues, ArrayParams);

                List<LibsEntity> returnValue = new List<LibsEntity>();
                if (dt != null)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        LibsEntity currentLib = new LibsEntity((int)dr["ID"], (string)dr["Name"], (int)dr["ParentID"], (Boolean)dr["Editable"], (byte)dr["Depth"]);
                        if (dr["PropertyID"] != DBNull.Value)
                        {
                            PropsEntity currentProperty = new PropsEntity(ID: (int)dr["PropertyID"], Name: (string)dr["PropertyName"]);
                            PropValuesEntity currentPropertyValue = new PropValuesEntity(ID: (int)dr["PropertyValueID"], Value: (string)dr["PropertyValue"]);
                            currentProperty.Values.Add(currentPropertyValue);
                        }
                        if (!returnValue.FindAll(r => r.ID == (int)dr["ID"]).Any())
                        {
                            returnValue.Add(currentLib);
                        }
                    }
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public Dictionary<int,List<LibsEntity>>  SP_GetList_LibsByLibPathIDList(string LibPathIDList)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("LibPathIDList");
                ArrayValues.Add(LibPathIDList);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_LibsByLibPathIDList", ArrayValues, ArrayParams);


                Dictionary<int, List<LibsEntity>> returnValue = new Dictionary<int, List<LibsEntity>>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        int PathID = (int)row["PathID"];
                        List<LibsEntity> list;
                        if (!returnValue.ContainsKey(PathID))
                        {
                            list = new List<LibsEntity>();
                            returnValue.Add(PathID, list);
                        }
                        else
                        {
                            list = returnValue[PathID];
                        }
                        LibsEntity currentLib = new LibsEntity((int)row["ID"], (string)row["Name"], (int)row["ParentID"], (Boolean)row["Editable"], (byte)row["Depth"]);
                        list.Add(currentLib);
                    }
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<LibsEntity> SP_GetList_LibsByLibParentID(int? ParentID = null)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ParentID");
                ArrayValues.Add(ParentID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_LibsByLibParentID", ArrayValues, ArrayParams);

                List<LibsEntity> returnValue = new List<LibsEntity>();
                if (dt != null)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        LibsEntity currentLib = new LibsEntity((int)dr["ID"], (string)dr["Name"], (int)dr["ParentID"], (bool)dr["Editable"], (byte)dr["Depth"]);
                        returnValue.Add(currentLib);
                    }
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public LibsEntity SP_Get_LibByID(int LibID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("LibID");
                ArrayValues.Add(LibID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Get_LibByLibID", ArrayValues, ArrayParams);

                LibsEntity currentLib = new LibsEntity((int)dt.Rows[0]["ID"], (string)dt.Rows[0]["Name"], (int)dt.Rows[0]["ParentID"], (Boolean)dt.Rows[0]["Editable"], (byte)dt.Rows[0]["Depth"]);
             
                return currentLib;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
 
        public Boolean SP_Update_LibByID(int LibID, string Name = null)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(LibID);
                ArrayParams.Add("Name");
                ArrayValues.Add(Name);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Libs", ArrayValues, ArrayParams);

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public Boolean SP_Delete_LibByID(int LibID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(LibID);
                ArrayParams.Add("Status");
                ArrayValues.Add(0);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Libs", ArrayValues, ArrayParams);

                ArrayParams.Clear();
                ArrayValues.Clear();
                ArrayParams.Add("LibID");
                ArrayValues.Add(LibID);
                ArrayParams.Add("Status");
                ArrayValues.Add(0);
                dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_LibPathToLibs", ArrayValues, ArrayParams);
                

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public int SP_Get_LibCountByLibPathID(int LibPathID)
        {
            try
            {
                int count = 0;
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("LibPathID");
                ArrayValues.Add(LibPathID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Get_LibCountByLibPathID", ArrayValues, ArrayParams);

                Dictionary<int, Dictionary<int, int>> returnValue = new Dictionary<int, Dictionary<int, int>>();
                if (dt != null)
                {
                    count = (int)dt.Rows[0]["ChildCount"];
                    
                }
                return count;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }

        public int? SP_Get_LibPathLibID(int? PathID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("PathID");
                ArrayValues.Add(PathID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Get_LibPathLibID", ArrayValues, ArrayParams);

                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        int LibID = (int)row["LibID"];
                        return LibID;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

        }

        public List<LibsEntity> SP_GetList_CitizenshipLibsByCode(string Code, int CitizenshipLibPathID = 3, int IdentCodePropID = 4)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("Code");
                ArrayValues.Add(Code);
                ArrayParams.Add("CitizenshipLibPathID");
                ArrayValues.Add(CitizenshipLibPathID);
                ArrayParams.Add("IdentCodePropID");
                ArrayValues.Add(IdentCodePropID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_CitizenshipLibsByCode", ArrayValues, ArrayParams);

                List<LibsEntity> returnValue = new List<LibsEntity>();
                if (dt != null)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        LibsEntity currentLib = new LibsEntity((int)dr["ID"], (string)dr["Name"], (int)dr["ParentID"], (Boolean)dr["Editable"], (byte)dr["Depth"]);
                        returnValue.Add(currentLib);
                    }
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine(String.Format("SP_GetList_CitizenshipLibsByCode {0}", ex.Message));
                return null;
            }
        }
    }
}
