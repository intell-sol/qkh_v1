﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_LogActionService : DataComm
    {

        public int? SP_Add_LogAction(LogActionEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("EmployeeID");
            ArrayParams.Add("PositionID");
            ArrayParams.Add("ActionName");
            ArrayParams.Add("RequestData");


            ArrayValues.Add(entity.EmployeeID);
            ArrayValues.Add(entity.PositionID);
            ArrayValues.Add(entity.ActionName);
            ArrayValues.Add(entity.RequestData);


            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_LogAction", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        
        public List<LogActionEntity> SP_GetList_LogAction(FilterLogAcionEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("EmployeeID");
                ArrayValues.Add(entity.EmployeeID);
                ArrayParams.Add("PositionID");
                ArrayValues.Add(entity.PositionID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_LogAction", ArrayValues, ArrayParams);

                List<LogActionEntity> returnValue = new List<LogActionEntity>();
                if (dt != null)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        LogActionEntity log = new LogActionEntity(
                            (int)dr["ID"], 
                            (int)dr["EmployeeID"], 
                            (int)dr["PositionID"], 
                            (string)dr["ActionName"],
                            dr["RequestData"] == DBNull.Value ? null : (string)dr["RequestData"],
                            (DateTime)dr["CreationDate"]);
                        returnValue.Add(log);
                    }
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<LogActionEntity> SP_GetList_LogAction_ForDashboard(FilterLogAcionEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            int TotalCount = 0;

            ArrayParams.Add("EmployeeID");
            ArrayValues.Add(entity.EmployeeID);
            ArrayParams.Add("PositionID");
            ArrayValues.Add(entity.PositionID);
            ArrayParams.Add("StartDate");
            ArrayValues.Add(entity.StartDate);
            ArrayParams.Add("EndDate");
            ArrayValues.Add(entity.EndDate);

            ArrayParams.Add("CurrentPage");
            ArrayParams.Add("ViewCount");
            ArrayValues.Add(entity.paging.CurrentPage);
            ArrayValues.Add(entity.paging.ViewCount);

            ArrayList OutParams = new ArrayList();
            Dictionary<string, object> OutValues = new Dictionary<string, object>();

            OutParams.Add("TotalCount");
            OutValues.Add("TotalCount", TotalCount);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_LogAction_ForDashboard", ArrayValues, ArrayParams, ref OutValues, OutParams);

                List<LogActionEntity> returnValue = new List<LogActionEntity>();
                if (dt != null)
                {

                    foreach (DataRow dr in dt.Rows)
                    {
                        LogActionEntity log = new LogActionEntity(
                            (int)dr["ID"],
                            (int)dr["EmployeeID"],
                            (int)dr["PositionID"],
                            (string)dr["ActionName"],
                            dr["RequestData"] == DBNull.Value? null: (string)dr["RequestData"],
                            (DateTime)dr["CreationDate"]);
                        log.EmployeeFirstName = dr["FirstName"] == DBNull.Value ? null : (string)dr["FirstName"];
                        log.EmployeeMiddleName = dr["MiddleName"] == DBNull.Value ? null : (string)dr["MiddleName"];
                        log.EmployeeLastName = dr["LastName"] == DBNull.Value ? null : (string)dr["LastName"];
                        log.PositionName = dr["PositionName"] == DBNull.Value ? null : (string)dr["PositionName"];
                        returnValue.Add(log);
                    }                        
                }

                entity.paging.TotalCount = (int)OutValues["TotalCount"];
                entity.paging.TotalPage = (int)Math.Ceiling(Convert.ToDouble(OutValues["TotalCount"]) / entity.paging.ViewCount);
                return returnValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                entity.paging.TotalPage = 0;
                return new List<LogActionEntity>();
            }
        }
    }
}
