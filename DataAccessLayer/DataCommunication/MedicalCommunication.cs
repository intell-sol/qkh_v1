﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_MedicalService: DataComm
    {
        #region Add_Medical
        public int? SP_Add_MedicalComplaints(MedicalComplaintsEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("MedicalHistoryID");
            ArrayParams.Add("ComplaintsLibItemID");
            ArrayParams.Add("Description");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.MedicalHistoryID);
            ArrayValues.Add(entity.ComplaintsLibItemID);
            ArrayValues.Add(entity.Description);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_MedicalComplaints", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public int? SP_Add_MedicalDiagnosis(MedicalDiagnosisEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("MedicalHistoryID");
            ArrayParams.Add("NameLibItemID");
            ArrayParams.Add("State");
            ArrayParams.Add("Description");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.MedicalHistoryID);
            ArrayValues.Add(entity.NameLibItemID);
            ArrayValues.Add(entity.State);
            ArrayValues.Add(entity.Description);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_MedicalDiagnosis", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public int? SP_Add_MedicalExternalExamination(MedicalExternalExaminationEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("MedicalHistoryID");
            ArrayParams.Add("State");
            ArrayParams.Add("Date");
            ArrayParams.Add("Description");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.MedicalHistoryID);
            ArrayValues.Add(entity.State);
            ArrayValues.Add(entity.Date);
            ArrayValues.Add(entity.Description);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_MedicalExternalExamination", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public int? SP_Add_MedicalHistory(MedicalHistoryEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("Type");
            ArrayParams.Add("Name");
            ArrayParams.Add("Date");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.Type);
            ArrayValues.Add(entity.Name);
            ArrayValues.Add(entity.Date);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_MedicalHistory", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public int? SP_Add_MedicalHealing(MedicalHealingEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("MedicalHistoryID");
            ArrayParams.Add("NameLibItemID");
            ArrayParams.Add("State");
            ArrayParams.Add("Description");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.MedicalHistoryID);
            ArrayValues.Add(entity.NameLibItemID);
            ArrayValues.Add(entity.State);
            ArrayValues.Add(entity.Description);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_MedicalHealing", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public int? SP_Add_MedicalResearch(MedicalResearchEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("MedicalHistoryID");
            ArrayParams.Add("NameLibItemID");
            ArrayParams.Add("Description");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.MedicalHistoryID);
            ArrayValues.Add(entity.NameLibItemID);
            ArrayValues.Add(entity.Description);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_MedicalResearch", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public int? SP_Add_MedicalPrimary(MedicalPrimaryEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("HealthLibItemID");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.HealthLibItemID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_MedicalPrimary", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        #endregion

        #region Update_Medical
        public bool SP_Update_MedicalComplaints(MedicalComplaintsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("ComplaintsLibItemID");
                ArrayParams.Add("Description");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.ComplaintsLibItemID);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_MedicalComplaints", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool SP_Update_MedicalDiagnosis(MedicalDiagnosisEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear(); 

                ArrayParams.Add("ID");
                ArrayParams.Add("NameLibItemID");
                ArrayParams.Add("State");
                ArrayParams.Add("Description");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.NameLibItemID);
                ArrayValues.Add(entity.State);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_MedicalDiagnosis", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool SP_Update_MedicalExternalExamination(MedicalExternalExaminationEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("State");
                ArrayParams.Add("Date");
                ArrayParams.Add("Description");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.State);
                ArrayValues.Add(entity.Date);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_MedicalExternalExamination", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool SP_Update_MedicalHealing(MedicalHealingEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("NameLibItemID");
                ArrayParams.Add("State");
                ArrayParams.Add("Description");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.NameLibItemID);
                ArrayValues.Add(entity.State);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_MedicalHealing", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool SP_Update_MedicalHistory(MedicalHistoryEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Date");
                ArrayParams.Add("Name");
                ArrayParams.Add("ResultLibItemID");
                ArrayParams.Add("ResultNotes");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Date);
                ArrayValues.Add(entity.Name);
                ArrayValues.Add(entity.ResultLibItemID);
                ArrayValues.Add(entity.ResultNotes);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_MedicalHistory", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool SP_Update_MedicalPrimary(MedicalPrimaryEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayParams.Add("HealthLibItemID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);
                ArrayValues.Add(entity.HealthLibItemID);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_MedicalPrimary", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool SP_Update_MedicalResearch(MedicalResearchEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("NameLibItemID");
                ArrayParams.Add("Description");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.NameLibItemID);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_MedicalResearch", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion

        #region GetList_Medical

        public List<MedicalComplaintsEntity> SP_GetList_MedicalComplaints(MedicalComplaintsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(entity.ID);
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.PrisonerID);
                ArrayParams.Add("MedicalHistoryID");
                ArrayValues.Add(entity.MedicalHistoryID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_MedicalComplaints", ArrayValues, ArrayParams);

                List<MedicalComplaintsEntity> list = new List<MedicalComplaintsEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        MedicalComplaintsEntity item = new MedicalComplaintsEntity(
                                ID: (int)row["ID"],
                                MedicalHistoryID: (int)row["MedicalHistoryID"],
                                PrisonerID: (int)row["PrisonerID"],
                                ComplaintsLibItemID: (int)row["ComplaintsLibItemID"],
                                ComplaintsLibItemLabel: (string)row["ComplaintsLibItemLabel"],
                                Description: (row["Description"] == DBNull.Value) ? null : (string)row["Description"],
                                ModifyDate:(DateTime)row["ModifyDate"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null :(bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<MedicalComplaintsEntity>();
            }
        }

        public List<MedicalDiagnosisEntity> SP_GetList_MedicalDiagnosis(MedicalDiagnosisEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(entity.ID);
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.PrisonerID);
                ArrayParams.Add("MedicalHistoryID");
                ArrayValues.Add(entity.MedicalHistoryID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_MedicalDiagnosis", ArrayValues, ArrayParams);

                List<MedicalDiagnosisEntity> list = new List<MedicalDiagnosisEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        MedicalDiagnosisEntity item = new MedicalDiagnosisEntity(
                                ID: (int)row["ID"],
                                MedicalHistoryID:(int)row["MedicalHistoryID"],
                                PrisonerID:(int)row["PrisonerID"],
                                NameLibItemID: (int)row["NameLibItemID"],
                                NameLibItemLabel: (string)row["NameLibItemLabel"],
                                State: (bool)row["State"],
                                Description: (row["Description"] == DBNull.Value) ? null : (string)row["Description"],
                                ModifyDate: (DateTime)row["ModifyDate"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<MedicalDiagnosisEntity>();
            }
        }

        public List<MedicalExternalExaminationEntity> SP_GetList_MedicalExternalExamination(MedicalExternalExaminationEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(entity.ID);
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.PrisonerID);
                ArrayParams.Add("MedicalHistoryID");
                ArrayValues.Add(entity.MedicalHistoryID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_MedicalExternalExamination", ArrayValues, ArrayParams);

                List<MedicalExternalExaminationEntity> list = new List<MedicalExternalExaminationEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        MedicalExternalExaminationEntity item = new MedicalExternalExaminationEntity(
                                ID: (int)row["ID"],
                                MedicalHistoryID: (int)row["MedicalHistoryID"],
                                PrisonerID: (int)row["PrisonerID"],
                                MedicalHistoryName: (string)row["MedicalHistoryName"],
                                Description: (string)row["Description"],
                                Date: (DateTime)row["Date"],
                                State: (bool)row["State"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<MedicalExternalExaminationEntity>();
            }
        }

        public List<MedicalHealingEntity> SP_GetList_MedicalHealing(MedicalHealingEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(entity.ID);
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.PrisonerID);
                ArrayParams.Add("MedicalHistoryID");
                ArrayValues.Add(entity.MedicalHistoryID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_MedicalHealing", ArrayValues, ArrayParams);

                List<MedicalHealingEntity> list = new List<MedicalHealingEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        MedicalHealingEntity item = new MedicalHealingEntity(
                                ID: (int)row["ID"],
                                MedicalHistoryID:(int)row["MedicalHistoryID"],
                                NameLibItemID: (int)row["NameLibItemID"],
                                NameLibItemLabel: (string)row["LibItemLabel"],
                                Description: (string)row["Description"],
                                ModifyDate: (DateTime)row["[ModifyDate]"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<MedicalHealingEntity>();
            }
        }

        public List<MedicalHistoryEntity> SP_GetList_MedicalHistory(MedicalHistoryEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayParams.Add("Type");
                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);
                ArrayValues.Add(entity.Type);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_MedicalHistory", ArrayValues, ArrayParams);

                List<MedicalHistoryEntity> list = new List<MedicalHistoryEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        MedicalHistoryEntity item = new MedicalHistoryEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                Type: (int)row["Type"],
                                Name: (string)row["Name"],
                                ResultLibItemID: (row["ResultLibItemID"] == DBNull.Value) ? null :(int?)row["ResultLibItemID"],
                                ResultNotes: (row["ResultNotes"] == DBNull.Value) ? null :(string)row["ResultNotes"],
                                Date: (DateTime)row["Date"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null :(bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<MedicalHistoryEntity>();
            }
        }

        public List<MedicalPrimaryEntity> SP_GetList_MedicalPrimary(MedicalPrimaryEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(entity.ID);
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_MedicalPrimary", ArrayValues, ArrayParams);

                List<MedicalPrimaryEntity> list = new List<MedicalPrimaryEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        MedicalPrimaryEntity item = new MedicalPrimaryEntity(
                                ID: (int)row["ID"],
                                HealthLibItemID: (int)row["HealthLibItemID"],
                                PrisonerID: (int)row["PrisonerID"],
                                PrimaryLibItemLabel: (string)row["PrimaryLibItemLabel"],
                                ModifyDate: (DateTime)row["ModifyDate"],
                                MergeStatus:(row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<MedicalPrimaryEntity>();
            }
        }
        public List<MedicalResearchEntity> SP_GetList_MedicalResearch(MedicalResearchEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(entity.ID);
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.PrisonerID);
                ArrayParams.Add("MedicalHistoryID");
                ArrayValues.Add(entity.MedicalHistoryID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_MedicalResearch", ArrayValues, ArrayParams);

                List<MedicalResearchEntity> list = new List<MedicalResearchEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        MedicalResearchEntity item = new MedicalResearchEntity(
                                ID: (int)row["ID"],
                                PrisonerID:(int)row["PrisonerID"],
                                MedicalHistoryID:(int)row["MedicalHistoryID"],
                                NameLibItemID: (int)row["NameLibItemID"],
                                ResearchLibItemLabel: (string)row["ResearchLibItemLabel"],
                                MedicalHistoryName: (row["MedicalHistoryName"] == DBNull.Value) ? null : (string)row["MedicalHistoryName"],
                                Description: (row["Description"] == DBNull.Value) ? null : (string)row["Description"],
                                ModifyDate: (DateTime)row["ModifyDate"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<MedicalResearchEntity>();
            }
        }

        #endregion

    }
}
