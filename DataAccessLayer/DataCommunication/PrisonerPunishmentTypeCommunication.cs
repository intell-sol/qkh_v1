﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PrisonerPunishmentTypeService : DataComm
    {
        #region PrisonerPunishmentType
        public int? SP_Add_PrisonerPunishmentType(PrisonerPunishmentTypeEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("PrisonerType");
            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("BaseLibItemID");
            ArrayParams.Add("TypeLibItemID");
            ArrayParams.Add("AccessDate");
            ArrayParams.Add("Description");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.PrisonerType);
            ArrayValues.Add(entity.OrgUnitID);
            ArrayValues.Add(entity.BaseLibItemID);
            ArrayValues.Add(entity.TypeLibItemID);
            ArrayValues.Add(entity.AccessDate);
            ArrayValues.Add(entity.Description);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PrisonerPunishmentType", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<PrisonerPunishmentTypeEntity> SP_GetList_PrisonerPunishmentType(PrisonerPunishmentTypeEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PrisonerPunishmentType", ArrayValues, ArrayParams);

                List<PrisonerPunishmentTypeEntity> list = new List<PrisonerPunishmentTypeEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PrisonerPunishmentTypeEntity item = new PrisonerPunishmentTypeEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                PrisonerType: (int)row["PrisonerType"],
                                OrgUnitID: (int)row["OrgUnitID"],
                                BaseLibItemID: (int)row["BaseLibItemID"],
                                BaseLibItemLabel: (string)row["BaseLibItemLabel"],
                                TypeLibItemID: (int)row["TypeLibItemID"],
                                TypeLibItemLabel: (string)row["TypeLibItemLabel"],
                                Description: (row["Description"] == DBNull.Value) ? null : (string)row["Description"],
                                AccessDate: (row["AccessDate"] == DBNull.Value) ? null : (DateTime?)row["AccessDate"],
                                Status: (row["Status"] == DBNull.Value) ? null : (bool?)row["Status"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                          );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PrisonerPunishmentTypeEntity>();
            }
        }
        public bool SP_Update_PrisonerPunishmentType(PrisonerPunishmentTypeEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("BaseLibItemID");
                ArrayParams.Add("PrisonerType");
                ArrayParams.Add("TypeLibItemID");
                ArrayParams.Add("Description");
                ArrayParams.Add("AccessDate");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.BaseLibItemID);
                ArrayValues.Add(entity.PrisonerType);
                ArrayValues.Add(entity.TypeLibItemID);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.AccessDate);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_PrisonerPunishmentType", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
