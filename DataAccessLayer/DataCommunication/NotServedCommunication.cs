﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_NotServedService:DataComm
    {
        public int? SP_Add_NotServed(NotServedEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("ReasonLibItemID");
            ArrayParams.Add("Description");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.ReasonLibItemID);
            ArrayValues.Add(entity.Description);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_NotServed", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<NotServedEntity> SP_GetList_NotServed(NotServedEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_NotServed", ArrayValues, ArrayParams);

                List<NotServedEntity> list = new List<NotServedEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        NotServedEntity item = new NotServedEntity(
                                (int)row["ID"],
                                (int)row["PrisonerID"],
                                (int)row["ReasonLibItemID"],
                                (string)row["Description"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<NotServedEntity>();
            }
        }
        public bool SP_Update_NotServed(NotServedEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("ReasonLibItemID");
                ArrayParams.Add("Description");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.ReasonLibItemID);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_NotServed", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
