﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PrisonerTypeStatusService : DataComm
    {
        #region PrisonerTypeStatus
        public int? SP_Add_PrisonerTypeStatus(PrisonerTypeStatusEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("PrisonerType");
            ArrayParams.Add("Date");
            ArrayParams.Add("Description");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.OrgUnitID);
            ArrayValues.Add(entity.PrisonerType);
            ArrayValues.Add(entity.Date);
            ArrayValues.Add(entity.Description);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PrisonerTypeStatus", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<PrisonerTypeStatusEntity> SP_GetList_PrisonerTypeStatus(PrisonerTypeStatusEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PrisonerTypeStatus", ArrayValues, ArrayParams);

                List<PrisonerTypeStatusEntity> list = new List<PrisonerTypeStatusEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PrisonerTypeStatusEntity item = new PrisonerTypeStatusEntity(
                                ID:(int)row["ID"],
                                OrgUnitID:(int)row["OrgUnitID"],
                                PrisonerID:(int)row["PrisonerID"],
                                PrisonerType:(row["PrisonerType"] == DBNull.Value) ? null : (int?)row["PrisonerType"],
                                Description: (row["Description"] == DBNull.Value) ? null :(string)row["Description"],
                                Status: (row["Status"] == DBNull.Value) ? null :(bool?)row["Status"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null :(bool?)row["MergeStatus"],
                                Date: (row["Date"] == DBNull.Value) ? null :(DateTime?)row["Date"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PrisonerTypeStatusEntity>();
            }
        }
        public bool SP_Update_PrisonerTypeStatus(PrisonerTypeStatusEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerType");
                ArrayParams.Add("Date");
                ArrayParams.Add("Description");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerType);
                ArrayValues.Add(entity.Date);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_PrisonerTypeStatus", ArrayValues, ArrayParams);

                
                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
