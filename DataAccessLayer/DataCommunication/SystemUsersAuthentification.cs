﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_SystemUsersAuthentification : DataComm
    {
        public BESystemUser SP_Login(string UserName, string Password)
        {
            BESystemUser SysUser = new BESystemUser();
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("Name");
            ArrayParams.Add("Password");
            ArrayValues.Add(UserName);
            ArrayValues.Add(Password);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Check_SystemUser", ArrayValues, ArrayParams);

                if (dt.Rows.Count == 0)
                    return null;

                DataRow dr = dt.Rows[0];
                SysUser.ID = (int)dr["id"];
                SysUser.FirstName = (string)dr["name"];
                //SysUser.LastName = UserName;

                return SysUser;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public bool SP_LogOut(BESystemUser SysUser)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("SystemUserID");
            ArrayValues.Add(SysUser.ID.ToString());
            try
            {
                //DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_LogOut", ArrayValues, ArrayParams);
                //
                //if (dt.Rows.Count == 0)
                //    return false;
                //DataRow dr = dt.Rows[0];
                //int id = (int)dr["SystemUserID"];

                return true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return false;
            }
        }
    }
}
