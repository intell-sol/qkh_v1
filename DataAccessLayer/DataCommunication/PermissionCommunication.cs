﻿
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PermissionServices : DataComm
    {
        public int? SP_Add_PermissionsApprove(int OrgUnitID, int PermToPosID, bool Permanent = true)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("PermToPosID");
            ArrayParams.Add("Permanent");
            ArrayValues.Add(OrgUnitID);
            ArrayValues.Add(PermToPosID);
            ArrayValues.Add(Permanent);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PermissionsApprove", ArrayValues, ArrayParams);

                if (dt.Rows.Count == 0)
                    return -1;

                return 0;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public int? SP_Update_PermissionsApprove(int? ID = null, int? PermToPosID = null, bool? Status = true)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(ID);
                ArrayParams.Add("PermToPosID");
                ArrayValues.Add(PermToPosID);
                ArrayParams.Add("Status");
                ArrayValues.Add(Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_PermissionsApprove", ArrayValues, ArrayParams);

                if (dt.Rows.Count == 0)
                    return -1;

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public int? SP_Add_PermissionsToPosition(int OrgUnitID, int PermissionID, int Type)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("PermissionID");
            ArrayParams.Add("Type");
            ArrayValues.Add(OrgUnitID);
            ArrayValues.Add(PermissionID);
            ArrayValues.Add(Type);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PermissionsToPosition", ArrayValues, ArrayParams);

                if (dt.Rows.Count == 0)
                    return -1;

                return Decimal.ToInt32((decimal)dt.Rows[0][0]);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public int? SP_Update_PermissionsToPosition(int? ID, int? OrgUnitID = null, int? PermissionID = null, int? Type = null, bool? Status = null, bool Recursive = false)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(ID);
                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);
                ArrayParams.Add("PermissionID");
                ArrayValues.Add(PermissionID);
                ArrayParams.Add("Type");
                ArrayValues.Add(Type);
                ArrayParams.Add("Status");
                ArrayValues.Add(Status);
                ArrayParams.Add("Recursive");
                ArrayValues.Add(Recursive);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_PermissionsToPosition", ArrayValues, ArrayParams);

                if (dt.Rows.Count == 0)
                    return -1;

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public int? SP_Delete_PermissionsToPosition(int? PermissionID, int? OrgUnitID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("PermissionID");
                ArrayValues.Add(PermissionID);
                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Delete_PermissionsToPosition", ArrayValues, ArrayParams);

                if (dt.Rows.Count == 0)
                    return -1;

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        
        public List<PermToPosEntity> SP_GetList_PermissionsToPosition(int? ID = null, int? PermissionID = null, int? OrgUnitID = null, int? Type = null)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(ID);
                ArrayParams.Add("PermissionID");
                ArrayValues.Add(PermissionID);
                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);
                ArrayParams.Add("Type");
                ArrayValues.Add(Type);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PermissionsToPosition", ArrayValues, ArrayParams);

                List<PermToPosEntity> list = new List<PermToPosEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PermToPosEntity item = new PermToPosEntity(
                                (int)row["ID"],
                                (int)row["PermissionID"],
                                (int)row["OrgUnitID"],
                                (short)row["Type"]
                            );
                        string IDList = row["IDList"] == DBNull.Value ? "" : (string)row["IDList"]; // Atttention!! IDList is a permanent = fasle aprrove list;
                        System.Diagnostics.Debug.WriteLine("Atttention!! IDList is a 'permanent = fasle' approve list;");
                        item.Approve = IDList != String.Empty ? IDList.Split(',').Select(Int32.Parse).ToList() : new List<int>();

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        
        public List<PermEntity> SP_GetList_Permissions(int? ID = null, int? ParentID = null)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(ID);
                ArrayParams.Add("ParentID");
                ArrayValues.Add(ParentID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Permissions", ArrayValues, ArrayParams);

                List<PermEntity> list = new List<PermEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PermEntity item = new PermEntity(
                                (int)row["ID"],
                                (int)row["ParentID"],
                                (string)row["Name"],
                                (string)row["Title"],
                                (bool)row["Type"],
                                (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<PermEntity> SP_GetList_PermissionsByOrgUnitID(int OrgUnitID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PermissionsByOrgUnitID", ArrayValues, ArrayParams);

                List<PermEntity> list = new List<PermEntity>();
                if(dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PermEntity item = new PermEntity(
                                (int)row["ID"],
                                (int)row["ParentID"],
                                (string)row["Name"],
                                (string)row["Title"],
                                (bool)row["Type"],
                                (bool)row["Status"]
                            );
                        item.PPType = (short)row["PPType"];
                        string IDList = row["IDList"] == DBNull.Value ? "" : (string)row["IDList"];
                        string[] Approvers = IDList != String.Empty ? IDList.Split(',') : new string[0];
                        for (int i=0; i<Approvers.Length; i++)
                        {
                            item.Approvers.Add(Int32.Parse(Approvers[i]));
                        }
                        list.Add(item);
                    }
                }                

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<int> SP_GetList_PermissionApproveIDs(int OrgUnitID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PermissionsApproveIDsByOrgUnitID", ArrayValues, ArrayParams);

                List<int> list = new List<int>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add((int)row["ID"]);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<int>();
            }
        }

        public List<PermissionsDashboardEntity> SP_GetList_PermissionedPositions(int PermissionID, int QKHOrgUnitID, bool Permanent)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("PermissionID");
                ArrayValues.Add(PermissionID);
                ArrayParams.Add("QKHOrgUnitID");
                ArrayValues.Add(QKHOrgUnitID);
                ArrayParams.Add("Permanent");
                ArrayValues.Add(Permanent);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PermissionedPositions", ArrayValues, ArrayParams);

                List<PermissionsDashboardEntity> list = new List<PermissionsDashboardEntity>();
                foreach (DataRow row in dt.Rows)
                {
                    PermissionsDashboardEntity item = new PermissionsDashboardEntity();
                    item.Position = new OrgUnitEntity(
                            (int)row["ID"],
                            (int)row["ParentID"],
                            (short)row["TypeID"],
                            (bool)row["Single"],
                            (string)row["Label"],
                            row.IsNull("Address") ? "" : (string)row["Address"],
                            row.IsNull("StateID") ? null : (int?)row["StateID"],
                            row.IsNull("Phone") ? "" : (string)row["Phone"],
                            row.IsNull("Fax") ? "" : (string)row["Fax"],
                            row.IsNull("Email") ? "" : (string)row["Email"],
                            row.IsNull("Editable") ? false : (bool)row["Editable"]
                        );
                    item.PermApproveID = (int)row["PermApproveID"];
                    item.PermToPosID = (int)row["PermToPosID"];
                    item.PermissionID = PermissionID;
                    DAL_OrganizationServices service = new DAL_OrganizationServices();
                    item.Employees = service.SP_GetList_Employees(OrgUnitID: item.Position.ID);

                    list.Add(item);
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<PermissionsDashboardEntity> SP_GetList_PermissionedPositionsForDashboard(int PermissionID, int QKHOrgUnitID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("PermissionID");
                ArrayValues.Add(PermissionID);
                ArrayParams.Add("QKHOrgUnitID");
                ArrayValues.Add(QKHOrgUnitID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PermissionedPositionsForDashboard", ArrayValues, ArrayParams);

                List<PermissionsDashboardEntity> list = new List<PermissionsDashboardEntity>();
                foreach (DataRow row in dt.Rows)
                {
                    PermissionsDashboardEntity item = new PermissionsDashboardEntity();
                    item.Position = new OrgUnitEntity(
                            (int)row["ID"],
                            (int)row["ParentID"],
                            (short)row["TypeID"],
                            (bool)row["Single"],
                            (string)row["Label"],
                            row.IsNull("Address") ? "" : (string)row["Address"],
                            row.IsNull("StateID") ? null : (int?)row["StateID"],
                            row.IsNull("Phone") ? "" : (string)row["Phone"],
                            row.IsNull("Fax") ? "" : (string)row["Fax"],
                            row.IsNull("Email") ? "" : (string)row["Email"],
                            row.IsNull("Editable") ? false : (bool)row["Editable"]
                        );
                    item.PermApproveID = (int)row["PermApproveID"];
                    item.PermToPosID = (int)row["PermToPosID"];
                    item.PermissionID = PermissionID;
                    DAL_OrganizationServices service = new DAL_OrganizationServices();
                    item.Employees = service.SP_GetList_Employees(OrgUnitID: item.Position.ID, Status: true);

                    list.Add(item);
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        #region PermissionsGuard
        public int? SP_Add_PermissionsGuard(PermissionsGurdEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PositionID");
            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("PermissionIDs");

            ArrayValues.Add(entity.PositionID);
            ArrayValues.Add(entity.OrgUnitID);
            ArrayValues.Add(entity.PermissionIDs);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PermissionsGuard", ArrayValues, ArrayParams);

                if (dt.Rows.Count == 0)
                    return -1;

                return Decimal.ToInt32((decimal)dt.Rows[0][0]);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public int? SP_Update_PermissionsGuard(PermissionsGurdEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PositionID");
                ArrayParams.Add("OrgUnitID");
                ArrayParams.Add("PermissionIDs");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PositionID);
                ArrayValues.Add(entity.OrgUnitID);
                ArrayValues.Add(entity.PermissionIDs);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_PermissionsGuard", ArrayValues, ArrayParams);

                if (dt.Rows.Count == 0)
                    return -1;

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<PermissionsGurdEntity> SP_GetList_PermissionsGuard(PermissionsGurdEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PositionID");
                ArrayParams.Add("OrgUnitID");
                ArrayParams.Add("PermissionIDs");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PositionID);
                ArrayValues.Add(entity.OrgUnitID);
                ArrayValues.Add(entity.PermissionIDs);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PermissionsGuard", ArrayValues, ArrayParams);

                List<PermissionsGurdEntity> list = new List<PermissionsGurdEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PermissionsGurdEntity item = new PermissionsGurdEntity(
                                ID: (int)row["ID"],
                                PositionID: (int)row["PositionID"],
                                OrgUnitID: (int)row["OrgUnitID"],
                                PermissionIDs:(int)row["PermissionIDs"],
                                PositionName: (string)row["PositionName"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        #endregion
    }
}