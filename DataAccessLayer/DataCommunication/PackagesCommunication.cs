﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PackageService : DataComm
    {
        // penalties part
        public int? SP_Add_Package(PackagesEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("TypeLibItemID");
            ArrayParams.Add("ApproveEmployeeID");
            ArrayParams.Add("Date");
            ArrayParams.Add("AcceptDate");
            ArrayParams.Add("Weight");
            ArrayParams.Add("EmployeeID");
            ArrayParams.Add("PersonID");
            ArrayParams.Add("ApproveStatus");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.TypeLibItemID);
            ArrayValues.Add(entity.ApproveEmployeeID);
            ArrayValues.Add(entity.Date);
            ArrayValues.Add(entity.AcceptDate);
            ArrayValues.Add(entity.Weight);
            ArrayValues.Add(entity.EmployeeID);
            ArrayValues.Add(entity.PersonID);
            ArrayValues.Add(entity.ApproveStatus);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Packages", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<PackagesEntity> SP_GetList_Packages(PackagesEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Packages", ArrayValues, ArrayParams);

                List<PackagesEntity> list = new List<PackagesEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PackagesEntity item = new PackagesEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                TypeLibItemID: (int)row["TypeLibItemID"],
                                TypeLibItemLabel: (string)row["TypeLibItemLabel"],
                                Weight: (int)row["Weight"],
                                EmployeeID: (int)row["EmployeeID"],
                                EmployeeName: (string)row["EmployeeName"],
                                ApproveEmployeeID: (row["ApproveEmployeeID"] == DBNull.Value) ? null : (int?)row["ApproveEmployeeID"],
                                ApproveStatus: (row["ApproveStatus"] == DBNull.Value) ? null : (bool?)row["ApproveStatus"],
                                ApproveEmployeeName: (row["ApproveEmployeeName"] == DBNull.Value) ? null : (string)row["ApproveEmployeeName"],
                                PersonID: (int)row["PersonID"],
                                PersonName: (string)row["PersonName"],
                                Date: (row["Date"] == DBNull.Value) ? null : (DateTime?)row["Date"],
                                AcceptDate: (row["AcceptDate"] == DBNull.Value) ? null : (DateTime?)row["AcceptDate"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"],
                                Status: (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PackagesEntity>();
            }
        }
        public bool SP_Update_Package(PackagesEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayParams.Add("TypeLibItemID");
                ArrayParams.Add("ApproveEmployeeID");
                ArrayParams.Add("Date");
                ArrayParams.Add("AcceptDate");
                ArrayParams.Add("Weight");
                ArrayParams.Add("EmployeeID");
                ArrayParams.Add("PersonID");
                ArrayParams.Add("Status");
                ArrayParams.Add("ApproveStatus");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);
                ArrayValues.Add(entity.TypeLibItemID);
                ArrayValues.Add(entity.ApproveEmployeeID);
                ArrayValues.Add(entity.Date);
                ArrayValues.Add(entity.AcceptDate);
                ArrayValues.Add(entity.Weight);
                ArrayValues.Add(entity.EmployeeID);
                ArrayValues.Add(entity.PersonID);
                ArrayValues.Add(entity.Status);
                ArrayValues.Add(entity.ApproveStatus);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Packages", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public List<PackagesDashboardEntity> SP_GetList_PackagesForDashboard(FilterPackagesEntity FilterData)
        {
            // UNDONE: to this first and move it to EncouragementsCommunicatin
            ArrayParams.Clear();
            ArrayValues.Clear();

            int TotalCount = 0;

            ArrayParams.Add("FirstName");
            ArrayParams.Add("MiddleName");
            ArrayParams.Add("LastName");
            ArrayParams.Add("TypeLibItemID");
            ArrayParams.Add("TypeContentLibItemID");
            ArrayParams.Add("ApproveEmployee");
            ArrayParams.Add("PersonID");
            ArrayParams.Add("ApproveStatus");
            ArrayParams.Add("ApproveStatusAll");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("EmployeeID");
            ArrayParams.Add("PrisonerType");
            ArrayParams.Add("ArchiveStatus");
            ArrayParams.Add("OrgUnitIDList");
            ArrayParams.Add("CurrentPage");
            ArrayParams.Add("ViewCount");


            ArrayValues.Add(FilterData.FirstName);
            ArrayValues.Add(FilterData.MiddleName);
            ArrayValues.Add(FilterData.LastName);
            ArrayValues.Add(FilterData.TypeLibItemID);
            ArrayValues.Add(FilterData.TypeContentLibItemID);
            ArrayValues.Add(FilterData.ApproveEmployeeID);
            ArrayValues.Add(FilterData.PersonID);
            ArrayValues.Add(FilterData.ApproveStatus);
            ArrayValues.Add(FilterData.ApproveStatusAll);
            ArrayValues.Add(FilterData.StartDate);
            ArrayValues.Add(FilterData.EndDate);
            ArrayValues.Add(FilterData.PrisonerID);
            ArrayValues.Add(FilterData.EmployeeID);
            ArrayValues.Add(FilterData.PrisonerType);
            ArrayValues.Add(FilterData.ArchiveStatus);
            ArrayValues.Add((FilterData.OrgUnitIDList != null && FilterData.OrgUnitIDList.Any()) ? string.Join(",", FilterData.OrgUnitIDList) : FilterData.OrgUnitID.ToString());
            ArrayValues.Add(FilterData.paging.CurrentPage);
            ArrayValues.Add(FilterData.paging.ViewCount);


            ArrayList OutParams = new ArrayList();
            Dictionary<string, object> OutValues = new Dictionary<string, object>();

            OutParams.Add("TotalCount");
            OutValues.Add("TotalCount", TotalCount);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PackagesForDashboard", ArrayValues, ArrayParams, ref OutValues, OutParams);

                List<PackagesDashboardEntity> list = new List<PackagesDashboardEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PackagesDashboardEntity item = new PackagesDashboardEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                TypeLibItemID: (int)row["TypeLibItemID"],
                                Weight:(int)row["Weight"],
                                AcceptDate: (row["AcceptDate"] == DBNull.Value) ? null : (DateTime?)row["AcceptDate"],
                                EmployeeID: (int)row["EmployeeID"],
                                Status: (bool)row["Status"],
                                ApproveStatus: (row["ApproveStatus"] == DBNull.Value) ? null : (bool?)row["ApproveStatus"],
                                TypeLibItemLabel: (row["TypeLibItemLabel"] == DBNull.Value) ? null : (string)row["TypeLibItemLabel"],
                                TypeContentLibItemLabel: (row["TypeContentLibItemLabel"] == DBNull.Value) ? null : (string)row["TypeContentLibItemLabel"],
                                EmployeeName: (row["EmployeeName"] == DBNull.Value) ? null  : (string)row["EmployeeName"],
                                ApproveEmployeeName: (row["ApproveEmployeeName"] == DBNull.Value) ? null : (string)row["ApproveEmployeeName"],
                                Personal_ID: (string)row["Personal_ID"],
                                PersonName: (string)row["PersonName"],
                                PrisonerName: (string)row["PrisonerName"],
                                ArchiveStatus: (bool)row["ArchiveStatus"]
                            );

                        list.Add(item);
                    }
                }

                FilterData.paging.TotalCount = (int)OutValues["TotalCount"];
                FilterData.paging.TotalPage = (int)Math.Ceiling(Convert.ToDouble(OutValues["TotalCount"]) / FilterData.paging.ViewCount);
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                FilterData.paging.TotalPage = 0;
                return new List<PackagesDashboardEntity>();
            }
        }

        // violation to penalties part
        public int? SP_Add_PackageContent(PackageContentEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PackageID");
            ArrayParams.Add("TypeLibItemID");
            ArrayParams.Add("GoodsLibItemID");
            ArrayParams.Add("MeasureLibItemID");
            ArrayParams.Add("WightCount");

            ArrayValues.Add(entity.PackageID);
            ArrayValues.Add(entity.TypeLibItemID);
            ArrayValues.Add(entity.GoodsLibItemID);
            ArrayValues.Add(entity.MeasureLibItemID);
            ArrayValues.Add(entity.WeightCount);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PackageContent", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<PackageContentEntity> SP_GetList_PackageContent(PackageContentEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PackageID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PackageID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PackageContent", ArrayValues, ArrayParams);

                List<PackageContentEntity> list = new List<PackageContentEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PackageContentEntity item = new PackageContentEntity(
                                ID: (int)row["ID"],
                                PackageID: (int)row["PackageID"],
                                TypeLibItemID: (row["TypeLibItemID"] == DBNull.Value)? null : (int?)row["TypeLibItemID"],
                                TypeLibItemLabel: (row["TypeLibItemLabel"] == DBNull.Value) ? null : (string)row["TypeLibItemLabel"],
                                GoodsLibItemID: (row["GoodsLibItemID"] == DBNull.Value) ? null : (int?)row["GoodsLibItemID"],
                                GoodsLibItemLabel: (row["GoodsLibItemLabel"] == DBNull.Value) ? null : (string)row["GoodsLibItemLabel"],
                                MeasureLibItemID: (row["MeasureLibItemID"] == DBNull.Value) ? null : (int?)row["MeasureLibItemID"],
                                MeasureLibItemLabel: (row["MeasureLibItemLabel"] == DBNull.Value) ? null : (string)row["MeasureLibItemLabel"],
                                WightCount: (row["WightCount"] == DBNull.Value) ? null : (int?)row["WightCount"],
                                Status: (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PackageContentEntity>();
            }
        }
        public bool SP_Update_PackageContent(PackageContentEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PackageID");
                ArrayParams.Add("TypeLibItemID");
                ArrayParams.Add("GoodsLibItemID");
                ArrayParams.Add("MeasureLibItemID");
                ArrayParams.Add("WightCount");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PackageID);
                ArrayValues.Add(entity.TypeLibItemID);
                ArrayValues.Add(entity.GoodsLibItemID);
                ArrayValues.Add(entity.MeasureLibItemID);
                ArrayValues.Add(entity.WeightCount);
                ArrayValues.Add(entity.Status);


                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_PackageContent", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

    }
}