﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PhysicalDataToLibsService:DataComm
    {
        public int? SP_Add_PhysicalDataToLibs(int PhysicalDataID, int ListLib_ID, int ListItemLib_ID)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PhysicalDataID");
            ArrayParams.Add("ListLib_ID");
            ArrayParams.Add("ListItemLib_ID");

            ArrayValues.Add(PhysicalDataID);
            ArrayValues.Add(ListLib_ID);
            ArrayValues.Add(ListItemLib_ID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PhysicalDataToLibs", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
    }
}
