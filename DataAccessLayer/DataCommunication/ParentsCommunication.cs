﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_ParentsService:DataComm
    {
        public int? SP_Add_Parents(ParentsEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PersonID");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("RelatedPersonID");

            ArrayValues.Add(entity.PersonID);
            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.RelatedPersonID);


            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Parents", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<ParentsEntity> SP_GetList_Parents(ParentsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Parents", ArrayValues, ArrayParams);

                List<ParentsEntity> list = new List<ParentsEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        ParentsEntity item = new ParentsEntity(
                                ID:(int)row["ID"],
                                PersonID:(int)row["PersonID"],
                                PrisonerID:(int)row["PrisonerID"],
                                RelatedPersonID:(row["RelatedPersonID"] == DBNull.Value) ? null : (int?)row["RelatedPersonID"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }



                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<ParentsEntity>();
            }
        }
        public bool SP_Update_Parents(ParentsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("RelatedPersonID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.RelatedPersonID);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Parents", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public List<PersonEntity> SP_GetList_ParentRelatedPersons(int PrisonerID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_ParentRelatedPersons", ArrayValues, ArrayParams);

                List<PersonEntity> list = new List<PersonEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PersonEntity item = new PersonEntity(
                                (int)row["ID"],
                                (string)row["Personal_ID"],
                                (string)row["FirstName"],
                                (string)row["MiddleName"],
                                (string)row["LastName"],
                                (DateTime)row["Birthday"],
                                (int)row["SexLibItem_ID"],
                                (int)row["NationalityID"],
                                Registration_address: (row["Registration_address"] == DBNull.Value) ? null :(string)row["Registration_address"],
                                Living_place: (row["Living_place"] == DBNull.Value) ? null : (string)row["Living_place"]
                            );

                        list.Add(item);
                    }
                }



                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PersonEntity>();
            }
        }
    }
}
