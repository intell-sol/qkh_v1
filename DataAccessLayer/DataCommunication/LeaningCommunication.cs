﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_LeaningService:DataComm
    {
        public int? SP_Add_Leaning(LeaningEntity leaning)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("TypeLibItem_ID");
            ArrayParams.Add("SubTypeLibItem_ID");
            ArrayParams.Add("Date");
            ArrayParams.Add("Description");

            ArrayValues.Add(leaning.PrisonerID);
            ArrayValues.Add(leaning.TypeLibItem_ID);
            ArrayValues.Add(leaning.SubTypeLibItem_ID);
            ArrayValues.Add(leaning.Date);
            ArrayValues.Add(leaning.Description);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Leaning", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }


        public List<LeaningEntity> SP_GetList_Leaning(LeaningEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Leaning", ArrayValues, ArrayParams);

                List<LeaningEntity> list = new List<LeaningEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        LeaningEntity item = new LeaningEntity(
                                ID: (int)row["ID"],
                                PrisonerID:(int)row["PrisonerID"],
                                TypeLibItem_ID:(int)row["TypeLibItem_ID"],
                                SubTypeLibItem_ID:(row["SubTypeLibItem_ID"] == DBNull.Value) ? null : (int?)row["SubTypeLibItem_ID"],
                                Date:(DateTime)row["Date"],
                                Description:(row["Description"] == DBNull.Value) ? null : (string)row["Description"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<LeaningEntity>();
            }
        }
        public bool SP_Update_Leaning(LeaningEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("TypeLibItem_ID");
                ArrayParams.Add("SubTypeLibItem_ID");
                ArrayParams.Add("Date");
                ArrayParams.Add("Description");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.TypeLibItem_ID);
                ArrayValues.Add(entity.SubTypeLibItem_ID);
                ArrayValues.Add(entity.Date);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Leaning", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
