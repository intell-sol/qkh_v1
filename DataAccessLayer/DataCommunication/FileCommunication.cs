﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_FileService:DataComm
    {
        public int? SP_Add_Files(AdditionalFileEntity File)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PersonID");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("CategoryID");
            ArrayParams.Add("Name");
            ArrayParams.Add("Description");
            ArrayParams.Add("State");
            ArrayParams.Add("Size");
            ArrayParams.Add("Path");

            ArrayValues.Add(File.PersonID);
            ArrayValues.Add(File.PrisonerID);
            ArrayValues.Add((int)File.TypeID);
            ArrayValues.Add(File.FileName);
            ArrayValues.Add(File.Description);
            ArrayValues.Add(File.State);
            ArrayValues.Add(File.ContentLength);
            ArrayValues.Add(File.LocalFilePath);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Files", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public int? SP_Add_EmployeeFiles(AdditionalFileEntity file)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("EmployeeID");
            ArrayParams.Add("Name");
            ArrayParams.Add("Description");
            ArrayParams.Add("Size");
            ArrayParams.Add("Path");

            ArrayValues.Add(file.PrisonerID);
            ArrayValues.Add(file.FileName);
            ArrayValues.Add(file.Description);
            ArrayValues.Add(file.ContentLength);
            ArrayValues.Add(file.LocalFilePath);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_EmployeeFiles", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<AdditionalFileEntity> SP_GetList_Files(AdditionalFileEntity curEntity)
        {
            try
            {
                int? ViewType = null;
                switch ((int?)curEntity.TypeID)
                {
                    case (int)FileType.PHYSICAL_DATA_ATTACHED_FILES:
                        ViewType = (int)MergeEntity.TableIDS.PHYSICAL_FILES;
                        break;
                    case (int)FileType.MAIN_DATA_ATTACHED_IMAGES:
                        ViewType = (int)MergeEntity.TableIDS.PRIMARY_IMAGES;
                        break;
                    case (int)FileType.MAIN_DATA_ATTACHED_FILES:
                        ViewType = (int)MergeEntity.TableIDS.PRIMARY_FIELS;
                        break;
                    case (int)FileType.MEDICAL_AMBULATOR:
                    case (int)FileType.MEDICAL_PRELIMINARY:
                    case (int)FileType.MEDICAL_STATIONARY:
                        ViewType = (int)MergeEntity.TableIDS.MEDICAL_FILES;
                        break;
                    case (int)FileType.FAMILY_ATTACHED_FILES:
                        ViewType = (int)MergeEntity.TableIDS.FAMILY_FILES;
                        break;
                    case (int)FileType.ARMY_ATTACHED_FILES:
                        ViewType = (int)MergeEntity.TableIDS.ARMY_FILES;
                        break;
                    case (int)FileType.ADOPTION_INFORMATION_ARREST_RECORD:
                        ViewType = (int)MergeEntity.TableIDS.ENTERANCE_FILES_ARREST;
                        break;
                    case (int)FileType.ADOPTION_INFORMATION_ATTACHED_FILES:
                        ViewType = (int)MergeEntity.TableIDS.ENTERANCE_FILES;
                        break;
                    case (int)FileType.CAMERA_CARD_ATTACHED_FILES:
                        ViewType = (int)MergeEntity.TableIDS.CELL_FILES;
                        break;
                    case (int)FileType.ITEMS_ATTACHED_FILES:
                        ViewType = (int)MergeEntity.TableIDS.ITEMS_FILES;
                        break;
                    case (int)FileType.INJUNCTIONS_ATTACHED_FILES:
                        ViewType = (int)MergeEntity.TableIDS.BAN_FILES;
                        break;
                    case (int)FileType.ENCOURAGEMENTS:
                        ViewType = (int)MergeEntity.TableIDS.ENCOURAGEMENTS_FILES;
                        break;
                    case (int)FileType.PENALTIES:
                        ViewType = (int)MergeEntity.TableIDS.PENALTY_FILES;
                        break;
                    case (int)FileType.EDUCATIONAL_COURSES:
                        ViewType = (int)MergeEntity.TableIDS.EDUCATIONAL_COURSES_FILES;
                        break;
                    case (int)FileType.WORKLOADS:
                        ViewType = (int)MergeEntity.TableIDS.WORKLOADS_FILES;
                        break;
                    case (int)FileType.OFFICIAL_VISITS:
                        ViewType = (int)MergeEntity.TableIDS.OFFICIAL_VISITS_FILES;
                        break;
                    case (int)FileType.PERSONAL_VISITS:
                        ViewType = (int)MergeEntity.TableIDS.PERSONAL_VISITS_FILES;
                        break;
                    case (int)FileType.PACKAGES:
                        ViewType = (int)MergeEntity.TableIDS.PACKAGES_FILES;
                        break;
                    case (int)FileType.DEPARTURES:
                        ViewType = (int)MergeEntity.TableIDS.DEPARTURES_FILES;
                        break;
                    case (int)FileType.TRANSFERS:
                        ViewType = (int)MergeEntity.TableIDS.TRANSFERS_FILES;
                        break;
                    case (int)FileType.CONFLICTS:
                        ViewType = (int)MergeEntity.TableIDS.CONFLICTS_FILES;
                        break;
                }

                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("FileID");
                ArrayParams.Add("PersonID"); 
                ArrayParams.Add("PrisonerID");
                ArrayParams.Add("CategoryID"); 
                ArrayParams.Add("EID");
                ArrayValues.Add(curEntity.ID);
                ArrayValues.Add(curEntity.PersonID);
                ArrayValues.Add(curEntity.PrisonerID);
                ArrayValues.Add((int?)curEntity.TypeID);
                ArrayValues.Add(ViewType);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Files", ArrayValues, ArrayParams);

                List<AdditionalFileEntity> list = new List<AdditionalFileEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AdditionalFileEntity item = new AdditionalFileEntity((long)row["Size"]);
                        item.ID = (int)row["ID"];
                        item.PersonID = (int)row["PersonID"];
                        item.PrisonerID = row["PrisonerID"] == DBNull.Value ? null : (int?)row["PrisonerID"];
                        item.TypeID = (FileType)((int)row["CategoryID"]);
                        item.FileName = row["Name"] == DBNull.Value ? null : (string)row["Name"];
                        item.Description = row["Description"] == DBNull.Value ? null : (string)row["Description"];
                        item.Path = row["Path"] == DBNull.Value ? null : (string)row["Path"];
                        item.ContentLength = (long)row["Size"];
                        item.State = (bool)row["State"];
                        item.ModifyDate = (DateTime)row["ModifyDate"];
                        item.MergeStatus = (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"];

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<AdditionalFileEntity>();
            }
        }

        public AdditionalFileEntity SP_Get_EmployeeFile(int? fileID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(fileID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Get_EmployeeFile", ArrayValues, ArrayParams);

                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AdditionalFileEntity item = new AdditionalFileEntity((long)row["Size"]);
                        item.isPrisoner = false;
                        item.ID = (int)row["ID"];
                        item.PersonID = (int)row["EmployeeID"];
                        item.FileName = (string)row["Name"];
                        item.Description = row["Description"] == DBNull.Value ? "" : (string)row["Description"];
                        item.Path = (string)row["Path"];
                        item.ContentLength = (long)row["Size"];
                        item.ModifyDate = (DateTime)row["ModifyDate"];

                        return item;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

        }

        public bool SP_Update_Files(AdditionalFileEntity File)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("ID");
            ArrayParams.Add("Description");
            ArrayParams.Add("CategoryID");
            ArrayParams.Add("State");
            ArrayParams.Add("Status");

            ArrayValues.Add(File.ID);
            ArrayValues.Add(File.Description);
            ArrayValues.Add((File.TypeID != null) ? (int?)File.TypeID : null);
            ArrayValues.Add(File.State);
            ArrayValues.Add(File.Status);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Files", ArrayValues, ArrayParams);

                if(dt != null)
                {
                    return true;
                }

                return false;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }
}
