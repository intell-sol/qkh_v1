﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_SystemUserCommunication:DataComm
    {
        public int? SP_Add_SystemUser(string Name, string Password)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("Name");
            ArrayParams.Add("Password");

            ArrayValues.Add(Name);
            ArrayValues.Add(Password);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_SystemUser", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
    }
}
