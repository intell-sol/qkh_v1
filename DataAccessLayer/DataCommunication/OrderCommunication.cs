﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_OrderServices : DataComm
    {
        public Boolean SP_Add_EmployeeOrder(OrderEntity currentEntity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("EmployeeID");
            ArrayParams.Add("OrderTypeID");
            ArrayParams.Add("OrderNumber");
            ArrayParams.Add("OrderBy");
            ArrayParams.Add("OrderDate");
            ArrayParams.Add("FileID");
            ArrayValues.Add(currentEntity.EmployeeID);
            ArrayValues.Add(currentEntity.OrderTypeID);
            ArrayValues.Add(currentEntity.OrderNumber);
            ArrayValues.Add(currentEntity.OrderBy);
            ArrayValues.Add(currentEntity.OrderDate);
            ArrayValues.Add(currentEntity.FileID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_EmployeeOrders", ArrayValues, ArrayParams);

                if (dt != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="EmployeeID"></param>
        /// <param name="OrderTypeID"></param>
        /// <returns></returns>
        public List<OrderEntity> SP_GetList_EmployeeOrder(int? ID = null, int? EmployeeID = null, int? OrderTypeID = null)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("ID");
            ArrayParams.Add("EmployeeID");
            ArrayParams.Add("OrderTypeID");
            ArrayValues.Add(ID);
            ArrayValues.Add(EmployeeID);
            ArrayValues.Add(OrderTypeID);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_EmployeeOrders", ArrayValues, ArrayParams);

                List<OrderEntity> result = new List<OrderEntity>();
                if (dt != null)
                {
                    foreach(DataRow row in dt.Rows)
                    {
                        OrderEntity entity = new OrderEntity(
                                (int)row["ID"],
                                (int)row["OrderTypeID"],
                                (string)row["OrderNumber"],
                                (string)row["OrderBy"],
                                (DateTime)row["OrderDate"],
                                (int)row["FileID"]
                            );
                        result.Add(entity);
                    }
                }
                return result;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
    }
}