﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_UsersAuthentification : DataComm
    {
        public BEUser SP_Login(string UserName, string Password)
        {
            BEUser User = new BEUser();
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("Name");
            ArrayParams.Add("Password");
            ArrayValues.Add(UserName);
            ArrayValues.Add(Password);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Check_User", ArrayValues, ArrayParams);

                if (dt.Rows.Count == 0)
                    return null;

                DataRow dr = dt.Rows[0];
                User.ID = (int)dr["ID"];
                User.FirstName = (string)dr["FirstName"];
                User.LastName = (string)dr["LastName"];
                User.Position = (string)dr["Position"];
                User.LastLoginDate = (dr["LastLogin"] == DBNull.Value)?null:(DateTime?)dr["LastLogin"];
                User.UserIcon = dr.IsNull("Photo") ? String.Empty : dr["Photo"].ToString();
                //SysUser.LastName = UserName;

                return User;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public bool SP_LogOut(BESystemUser SysUser)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("SystemUserID");
            ArrayValues.Add(SysUser.ID.ToString());
            try
            {
                //DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_LogOut", ArrayValues, ArrayParams);
                //
                //if (dt.Rows.Count == 0)
                //    return false;
                //DataRow dr = dt.Rows[0];
                //int id = (int)dr["SystemUserID"];

                return true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return false;
            }
        }
    }
}
