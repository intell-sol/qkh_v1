﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_AddressService : DataComm
    {
        public int? SP_Add_AddressRegion(AddressItemEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("Name");

            ArrayValues.Add(entity.Name);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_AddressRegion", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public int? SP_Add_AddressStreet(AddressItemEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("Name");

            ArrayValues.Add(entity.Name);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_AddressStreet", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public int? SP_Add_AddressCommunity(AddressItemEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("Name");

            ArrayValues.Add(entity.Name);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_AddressCommunity", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public int? SP_Add_AddressBuildingType(AddressItemEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("Name");

            ArrayValues.Add(entity.Name);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_AddressBuildingType", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public int? SP_Add_AddressBuilding(AddressItemEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("Name");

            ArrayValues.Add(entity.Name);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_AddressBuilding", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public int? SP_Add_AddressApartment(AddressItemEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("Name");

            ArrayValues.Add(entity.Name);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_AddressApartment", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public int? SP_Add_PersonToAddress(PersonToAddressEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("RegionID");
            ArrayParams.Add("ComunityID");
            ArrayParams.Add("StreetID");
            ArrayParams.Add("BuildingTypeID");
            ArrayParams.Add("BuildingID");
            ArrayParams.Add("ApartmentID");
            ArrayValues.Add(entity.RegionID);
            ArrayValues.Add(entity.ComunityID);
            ArrayValues.Add(entity.StreetID);
            ArrayValues.Add(entity.BuildingTypeID);
            ArrayValues.Add(entity.BuildingID);
            ArrayValues.Add(entity.ApartmentID);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PersonToAddress", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public List<AddressItemEntity> SP_GetList_AddressRegion(AddressItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Name");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Name);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_AddressRegion", ArrayValues, ArrayParams);

                List<AddressItemEntity> list = new List<AddressItemEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AddressItemEntity item = new AddressItemEntity(
                                (int)row["ID"],
                                (string)row["Name"],
                                (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<AddressItemEntity>();
            }
        }
        public List<AddressItemEntity> SP_GetList_AddressStreet(AddressItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Name");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Name);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_AddressStreet", ArrayValues, ArrayParams);

                List<AddressItemEntity> list = new List<AddressItemEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AddressItemEntity item = new AddressItemEntity(
                                (int)row["ID"],
                                (string)row["Name"],
                                (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<AddressItemEntity>();
            }
        }
        public List<AddressItemEntity> SP_GetList_AddressCommunity(AddressItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Name");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Name);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_AddressCommunity", ArrayValues, ArrayParams);

                List<AddressItemEntity> list = new List<AddressItemEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AddressItemEntity item = new AddressItemEntity(
                                (int)row["ID"],
                                (string)row["Name"],
                                (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<AddressItemEntity>();
            }
        }
        public List<AddressItemEntity> SP_GetList_AddressBuildingType(AddressItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Name");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Name);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_AddressBuildingType", ArrayValues, ArrayParams);

                List<AddressItemEntity> list = new List<AddressItemEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AddressItemEntity item = new AddressItemEntity(
                                (int)row["ID"],
                                (string)row["Name"],
                                (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<AddressItemEntity>();
            }
        }
        public List<AddressItemEntity> SP_GetList_AddressBuilding(AddressItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Name");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Name);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_AddressBuilding", ArrayValues, ArrayParams);

                List<AddressItemEntity> list = new List<AddressItemEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AddressItemEntity item = new AddressItemEntity(
                                (int)row["ID"],
                                (string)row["Name"],
                                (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<AddressItemEntity>();
            }
        }
        public List<AddressItemEntity> SP_GetList_AddressApartment(AddressItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Name");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Name);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_AddressApartment", ArrayValues, ArrayParams);

                List<AddressItemEntity> list = new List<AddressItemEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AddressItemEntity item = new AddressItemEntity(
                                (int)row["ID"],
                                (string)row["Name"],
                                (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<AddressItemEntity>();
            }
        }
        public List<PersonToAddressEntity> SP_GetList_PersonToAddress(PersonToAddressEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("RegionID");
                ArrayParams.Add("ComunityID");
                ArrayParams.Add("StreetID");
                ArrayParams.Add("BuildingTypeID");
                ArrayParams.Add("BuildingID");
                ArrayParams.Add("ApartmentID");
                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.RegionID);
                ArrayValues.Add(entity.ComunityID);
                ArrayValues.Add(entity.StreetID);
                ArrayValues.Add(entity.BuildingTypeID);
                ArrayValues.Add(entity.BuildingID);
                ArrayValues.Add(entity.ApartmentID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PersonToAddress", ArrayValues, ArrayParams);

                List<PersonToAddressEntity> list = new List<PersonToAddressEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PersonToAddressEntity item = new PersonToAddressEntity(
                            ID: (int)row["ID"],
                            RegionID: (row["RegionID"] == DBNull.Value) ? null : (int?)row["RegionID"],
                            ComunityID: (row["ComunityID"] == DBNull.Value) ? null : (int?)row["ComunityID"],
                            StreetID: (row["StreetID"] == DBNull.Value) ? null : (int?)row["StreetID"],
                            BuildingTypeID: (row["BuildingTypeID"] == DBNull.Value) ? null : (int?)row["BuildingTypeID"],
                            BuildingID: (row["BuildingID"] == DBNull.Value) ? null : (int?)row["BuildingID"],
                            ApartmentID: (row["ApartmentID"] == DBNull.Value) ? null : (int?)row["ApartmentID"],
                            RegionName: (row["RegionName"] == DBNull.Value) ? null : (string)row["RegionName"],
                            ComunityName: (row["ComunityName"] == DBNull.Value) ? null : (string)row["ComunityName"],
                            StreetName: (row["StreetName"] == DBNull.Value) ? null : (string)row["StreetName"],
                            BuildingTypeName: (row["BuildingTypeName"] == DBNull.Value) ? null : (string)row["BuildingTypeName"],
                            BuildingName: (row["BuildingName"] == DBNull.Value) ? null : (string)row["BuildingName"],
                            ApartmentName: (row["ApartmentName"] == DBNull.Value) ? null : (string)row["ApartmentName"]
                        );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PersonToAddressEntity>();
            }
        }

        public bool SP_Update_AddressApartment(AddressItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Name");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Name);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_AddressApartment", ArrayValues, ArrayParams);

                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public bool SP_Update_AddressBuilding(AddressItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Name");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Name);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_AddressBuilding", ArrayValues, ArrayParams);

                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public bool SP_Update_AddressBuildingType(AddressItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Name");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Name);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_AddressBuildingType", ArrayValues, ArrayParams);

                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public bool SP_Update_AddressCommunity(AddressItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Name");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Name);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_AddressCommunity", ArrayValues, ArrayParams);

                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public bool SP_Update_AddressRegion(AddressItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Name");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Name);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_AddressRegion", ArrayValues, ArrayParams);

                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public bool SP_Update_AddressStreet(AddressItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Name");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Name);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_AddressStreet", ArrayValues, ArrayParams);

                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
