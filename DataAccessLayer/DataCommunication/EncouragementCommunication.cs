﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_EncouragementService : DataComm
    {
        public int? SP_Add_Encouragement(EncouragementsEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("TypeLibItemID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EmployeerID");
            ArrayParams.Add("Note");
            ArrayParams.Add("BaseLibItemID");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.TypeLibItemID);
            ArrayValues.Add(entity.StartDate);
            ArrayValues.Add(entity.EmployeerID);
            ArrayValues.Add(entity.Note);
            ArrayValues.Add(entity.BaseLibItemID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Encouragements", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<EncouragementsEntity> SP_GetList_Encouragements(EncouragementsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Encouragements", ArrayValues, ArrayParams);

                List<EncouragementsEntity> list = new List<EncouragementsEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        EncouragementsEntity item = new EncouragementsEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                TypeLibItemID: (int)row["TypeLibItemID"],
                                StartDate: (DateTime)row["StartDate"],
                                EmployeerID: (int)row["EmployeerID"],
                                EmployeerName: (string)row["EmployeerName"],
                                BaseLibItemID: (int)row["BaseLibItemID"],
                                Note: (row["Note"] == DBNull.Value) ? null : (string)row["Note"],
                                Status: (bool)row["Status"],
                                BaseLibItemLabel: (row["BaseLibItemLabel"] != DBNull.Value) ? (string)row["BaseLibItemLabel"] : null,
                                TypeLibItemLabel: (row["TypeLibItemLabel"] != DBNull.Value) ? (string)row["TypeLibItemLabel"] : null,
                                MergeStatus: (row["MergeStatus"] != DBNull.Value) ? (bool?)row["MergeStatus"] : null
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<EncouragementsEntity>();
            }
        }
        public bool SP_Update_Encouragement(EncouragementsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayParams.Add("TypeLibItemID");
                ArrayParams.Add("StartDate");
                ArrayParams.Add("EmployeerID");
                ArrayParams.Add("Note");
                ArrayParams.Add("BaseLibItemID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);
                ArrayValues.Add(entity.TypeLibItemID);
                ArrayValues.Add(entity.StartDate);
                ArrayValues.Add(entity.EmployeerID);
                ArrayValues.Add(entity.Note);
                ArrayValues.Add(entity.BaseLibItemID);
                ArrayValues.Add(entity.Status);


                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Encouragements", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public List<EncouragementsDashboardEntity> SP_GetList_EncouragementsForDashboard(FilterEncouragementsEntity EncouragementData)
        {
            // UNDONE: to this first and move it to EncouragementsCommunicatin
            ArrayParams.Clear();
            ArrayValues.Clear();

            int TotalCount = 0;
            ArrayParams.Add("TypeLibItemList");
            ArrayParams.Add("FirstName");
            ArrayParams.Add("MiddleName");
            ArrayParams.Add("LastName");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("EmployeerID");
            ArrayParams.Add("ArchiveStatus");
            ArrayParams.Add("PrisonerType");
            ArrayParams.Add("OrgUnitIDList");
            ArrayParams.Add("CurrentPage");
            ArrayParams.Add("ViewCount");

            ArrayValues.Add((EncouragementData.TypeLibItemList != null && EncouragementData.TypeLibItemList.Any()) ? string.Join(",", EncouragementData.TypeLibItemList) :null);
            ArrayValues.Add(EncouragementData.FirstName);
            ArrayValues.Add(EncouragementData.MiddleName);
            ArrayValues.Add(EncouragementData.LastName);
            ArrayValues.Add(EncouragementData.StartDate);
            ArrayValues.Add(EncouragementData.EndDate);
            ArrayValues.Add(EncouragementData.PrisonerID);
            ArrayValues.Add(EncouragementData.EmployeerID);
            ArrayValues.Add(EncouragementData.ArchiveStatus);
            ArrayValues.Add(EncouragementData.PrisonerType);
            ArrayValues.Add((EncouragementData.OrgUnitIDList != null && EncouragementData.OrgUnitIDList.Any()) ? string.Join(",", EncouragementData.OrgUnitIDList) : EncouragementData.OrgUnitID.ToString());
            ArrayValues.Add(EncouragementData.paging.CurrentPage);
            ArrayValues.Add(EncouragementData.paging.ViewCount);


            ArrayList OutParams = new ArrayList();
            Dictionary<string, object> OutValues = new Dictionary<string, object>();

            OutParams.Add("TotalCount");
            OutValues.Add("TotalCount", TotalCount);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_EncouragementsForDashboard", ArrayValues, ArrayParams, ref OutValues, OutParams);

                List<EncouragementsDashboardEntity> list = new List<EncouragementsDashboardEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        EncouragementsDashboardEntity item = new EncouragementsDashboardEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                TypeLibItemID: (row["TypeLibItemID"] == DBNull.Value) ? null : (int?)row["TypeLibItemID"],
                                StartDate: (row["StartDate"] == DBNull.Value) ? null : (DateTime?)row["StartDate"],
                                EmployeerID: (row["EmployeerID"] == DBNull.Value) ? null : (int?)row["EmployeerID"],
                                BaseLibItemID: (row["BaseLibItemID"] == DBNull.Value) ? null : (int?)row["BaseLibItemID"],
                                Note: (row["Note"] == DBNull.Value) ? null :(string)row["Note"],
                                Status: (bool)row["Status"],
                                TypeLibItemLabel: (row["TypeLibItemLabel"] == DBNull.Value) ? null :(string)row["TypeLibItemLabel"],
                                BaseLibItemLabel: (row["BaseLibItemLabel"] == DBNull.Value) ? null :(string)row["BaseLibItemLabel"],
                                EmployeerName: (row["EmployeerName"] == DBNull.Value) ? null :(string)row["EmployeerName"],
                                Personal_ID: (row["Personal_ID"] == DBNull.Value) ? null : (string)row["Personal_ID"],
                                PrisonerName: (row["PrisonerName"] == DBNull.Value) ? null : (string)row["PrisonerName"],
                                ArchiveStatus: (bool)row["ArchiveStatus"],
                                PrisonerType: (int)row["PrisonerType"]
                            );

                        list.Add(item);
                    }
                }

                EncouragementData.paging.TotalCount = (int)OutValues["TotalCount"];
                EncouragementData.paging.TotalPage = (int)Math.Ceiling(Convert.ToDouble(OutValues["TotalCount"]) / EncouragementData.paging.ViewCount);
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EncouragementData.paging.TotalPage = 0;
                return new List<EncouragementsDashboardEntity>();
            }
        }
    }
}