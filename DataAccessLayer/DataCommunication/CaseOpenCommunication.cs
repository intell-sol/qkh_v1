﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_CaseOpenService : DataComm
    {
        public int? SP_Add_CaseOpen(CaseOpenEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("PCN");
            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("OpenDate");
            ArrayParams.Add("CommandNumber");
            ArrayParams.Add("FromWhomID");
            ArrayParams.Add("Notes");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.PCN);
            ArrayValues.Add(entity.OrgUnitID);
            ArrayValues.Add(entity.StartDate);
            ArrayValues.Add(entity.EndDate);
            ArrayValues.Add(entity.OpenDate);
            ArrayValues.Add(entity.CommandNumber);
            ArrayValues.Add(entity.FromWhomID);
            ArrayValues.Add(entity.Notes);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_CaseOpen", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<CaseOpenEntity> SP_GetList_CaseOpen(CaseOpenEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_CaseOpen", ArrayValues, ArrayParams);

                List<CaseOpenEntity> list = new List<CaseOpenEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        CaseOpenEntity item = new CaseOpenEntity(
                                (int)row["ID"],
                                (int)row["PrisonerID"],
                                (int)row["PCN"],
                                (int)row["OrgUnitID"],
                                (string)row["Label"],
                                (row["StartDate"] != DBNull.Value) ? (DateTime?)row["StartDate"] : null,
                                (row["EndDate"] != DBNull.Value) ? (DateTime?)row["EndDate"] : null,
                                (row["CloseDate"] != DBNull.Value) ? (DateTime?)row["CloseDate"] : null,
                                (row["CommandNumber"] != DBNull.Value) ? (string)row["CommandNumber"] : null,
                                (row["FromWhomID"] != DBNull.Value) ? (int?)row["FromWhomID"] : null,
                                (row["Notes"] != DBNull.Value) ? (string)row["Notes"] : null,
                                false
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<CaseOpenEntity>();
            }
        }
        
    }
}
