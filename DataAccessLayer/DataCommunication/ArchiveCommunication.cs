﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_ArchiveService : DataComm
    {
        
        public List<ArchiveEntity> SP_GetList_Archive(int PrisonerID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();
                
                ArrayParams.Add("PrisonerID");
                
                ArrayValues.Add(PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Archive", ArrayValues, ArrayParams);

                List<ArchiveEntity> list = new List<ArchiveEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        ArchiveEntity item = new ArchiveEntity(
                                ID:(int)row["ID"],
                                PersonalCaseNumber:(int)row["PersonalCaseNumber"],
                                OrgUnitLabel:(string)row["OrgUnitLabel"],
                                StartDate:(row["StartDate"] != DBNull.Value) ? (DateTime?)row["StartDate"] : null,
                                EndDate:(row["EndDate"] != DBNull.Value) ? (DateTime?)row["EndDate"] : null
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<ArchiveEntity>();
            }
        }
    }
}
