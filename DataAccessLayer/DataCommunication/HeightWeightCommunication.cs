﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_HeightWeightService:DataComm
    {
        public int? SP_Add_HeightWeight(HeightWeightEntity HeightWeight)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            //ArrayParams.Add("PhysicalDataID");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("Height");
            ArrayParams.Add("Weight");

            //ArrayValues.Add(HeightWeight.PhysicalDataID);
            ArrayValues.Add(HeightWeight.PrisonerID);
            ArrayValues.Add(HeightWeight.Height);
            ArrayValues.Add(HeightWeight.Weight);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_HeightWeight", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<HeightWeightEntity> SP_GetList_HeightWeight(HeightWeightEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                //ArrayParams.Add("PhysicalDataID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);
                //ArrayValues.Add(entity.PhysicalDataID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_HeightWeight", ArrayValues, ArrayParams);

                List<HeightWeightEntity> list = new List<HeightWeightEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        HeightWeightEntity item = new HeightWeightEntity(
                                ID:(int?)row["ID"],
                                PrisonerID:(int?)row["PrisonerID"],
                                //(int?)row["PhysicalDataID"],
                                Height:(short)row["Height"],
                                Weight:(short)row["Weight"],
                                ModifyDate:(DateTime?)row["ModifyDate"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<HeightWeightEntity>();
            }
        }
        public bool SP_Update_HeightWeight(HeightWeightEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_HeightWeight", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
