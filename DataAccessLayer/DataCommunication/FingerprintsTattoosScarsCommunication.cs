﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_FingerprintsTattoosScarsService:DataComm
    {
        public int? SP_Add_FingerprintsTattoosScars(FingerprintsTattoosScarsItemEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("Name");
            ArrayParams.Add("Description");
            ArrayParams.Add("TypeID");
            ArrayParams.Add("FileID");
            ArrayParams.Add("State");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.Name);
            ArrayValues.Add(entity.Description);
            ArrayValues.Add((int)entity.TypeID);
            ArrayValues.Add(entity.FileID);
            ArrayValues.Add(entity.State);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_FingerprintsTattoosScars", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }


        public List<FingerprintsTattoosScarsItemEntity> SP_GetList_FingerprintsTattoosScars(int? TypeID, int? ID = null, int? PrisonerID = null)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayParams.Add("TypeID");

                ArrayValues.Add(ID);
                ArrayValues.Add(PrisonerID);
                ArrayValues.Add(TypeID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_FingerprintsTattoosScars", ArrayValues, ArrayParams);

                List<FingerprintsTattoosScarsItemEntity> list = new List<FingerprintsTattoosScarsItemEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        FingerprintsTattoosScarsItemEntity item = new FingerprintsTattoosScarsItemEntity(
                                ID:(int)row["ID"],
                                PrisonerID:(int)row["PrisonerID"],
                                Name:(string)row["Name"],
                                Description:(row["Description"] == DBNull.Value)?"":(string)row["Description"],
                                TypeID:(int)row["TypeID"],
                                FileID:(row["FileID"] == DBNull.Value) ? null : (int?)row["FileID"],
                                ModifyDate:(DateTime)row["ModifyDate"],
                                State:(bool)row["State"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value)?null:(bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<FingerprintsTattoosScarsItemEntity>();
            }
        }
        public bool SP_Update_FingerprintsTattoosScars(FingerprintsTattoosScarsItemEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Name");
                ArrayParams.Add("Description");
                ArrayParams.Add("State");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Name);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.State);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_FingerprintsTattoosScars", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
