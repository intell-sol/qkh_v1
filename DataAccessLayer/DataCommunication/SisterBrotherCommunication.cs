﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_SisterBrotherService:DataComm
    {
        public int? SP_Add_SisterBrother(SisterBrotherEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PersonID");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("RelationLibItemID");

            ArrayValues.Add(entity.PersonID);
            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.RelationLibItemID);


            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_SisterBrother", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<SisterBrotherEntity> SP_GetList_SisterBrother(SisterBrotherEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_SisterBrother", ArrayValues, ArrayParams);

                List<SisterBrotherEntity> list = new List<SisterBrotherEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SisterBrotherEntity item = new SisterBrotherEntity(
                                ID:(int)row["ID"],
                                PersonID:(int)row["PersonID"],
                                PrisonerID:(int)row["PrisonerID"],
                                RelationLibItemID:(row["RelationLibItemID"] == DBNull.Value) ? null : (int?)row["RelationLibItemID"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            ); 

                        list.Add(item);
                    }
                }



                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<SisterBrotherEntity>();
            }
        }
        public bool SP_Update_SisterBrother(SisterBrotherEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("RelationLibItemID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.RelationLibItemID);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_SisterBrother", ArrayValues, ArrayParams);
                
                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
