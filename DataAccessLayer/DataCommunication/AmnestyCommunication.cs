﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_AmnestyService : DataComm
    {
       
        public int? SP_Add_Amnesty(AmnestyEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("Number");
            ArrayParams.Add("TypeLibItemID");
            ArrayParams.Add("Source");
            ArrayParams.Add("Name");
            ArrayParams.Add("Description");
            ArrayParams.Add("RecipientLibItemID");
            ArrayParams.Add("SignatoriesLibItemID");
            ArrayParams.Add("State");
            ArrayParams.Add("AcceptDate");
            ArrayParams.Add("SignDate");
            ArrayParams.Add("EnterDate");
            ArrayParams.Add("EndDate");

            ArrayValues.Add(entity.Number);
            ArrayValues.Add(entity.TypeLibItemID);
            ArrayValues.Add(entity.Source);
            ArrayValues.Add(entity.Name);
            ArrayValues.Add(entity.Description);
            ArrayValues.Add(entity.RecipientLibItemID);
            ArrayValues.Add(entity.SignatoriesLibItemID);
            ArrayValues.Add(entity.State);
            ArrayValues.Add(entity.AcceptDate);
            ArrayValues.Add(entity.SignDate);
            ArrayValues.Add(entity.EnterDate);
            ArrayValues.Add(entity.EndDate);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Amnesty", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<AmnestyEntity> SP_GetList_Amnesty(AmnestyEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");

                ArrayValues.Add(entity.ID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Amnesty", ArrayValues, ArrayParams);

                List<AmnestyEntity> list = new List<AmnestyEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AmnestyEntity item = new AmnestyEntity(
                                ID: (int)row["ID"],
                                Number: (string)row["Number"],
                                TypeLibItemID: (int)(row["TypeLibItemID"]),
                                Source: (string)(row["Source"]),
                                Name: (string)(row["Name"]),
                                Description: ((row["Description"]) == DBNull.Value) ? null : (string)(row["Description"]),
                                RecipientLibItemID: (int)(row["RecipientLibItemID"]),
                                SignatoriesLibItemID: (int)(row["SignatoriesLibItemID"]),
                                State: (bool)(row["State"]),
                                AcceptDate: (row["AcceptDate"] == DBNull.Value) ? null : (DateTime?)row["AcceptDate"],
                                SignDate: (row["SignDate"] == DBNull.Value) ? null : (DateTime?)row["SignDate"],
                                EnterDate: (row["EnterDate"] == DBNull.Value) ? null : (DateTime?)row["EnterDate"],
                                EndDate: (row["EndDate"] == DBNull.Value) ? null : (DateTime?)row["EndDate"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<AmnestyEntity>();
            }
        }
        public List<AmnestyEntity>SP_GetList_AmnestyByNumber(AmnestyEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("Number");

                ArrayValues.Add(entity.Number);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_AmnestyByNumber", ArrayValues, ArrayParams);

                List<AmnestyEntity> list = new List<AmnestyEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AmnestyEntity item = new AmnestyEntity(
                                ID: (int)row["ID"],
                                Number: (string)row["Number"],
                                TypeLibItemID: (int)(row["TypeLibItemID"]),
                                Source: (string)(row["Source"]),
                                Name: (string)(row["Name"]),
                                Description: ((row["Description"]) == DBNull.Value) ? null : (string)(row["Description"]),
                                RecipientLibItemID: (int)(row["RecipientLibItemID"]),
                                SignatoriesLibItemID: (int)(row["SignatoriesLibItemID"]),
                                State: (bool)(row["State"]),
                                AcceptDate: (row["AcceptDate"] == DBNull.Value) ? null : (DateTime?)row["AcceptDate"],
                                SignDate: (row["SignDate"] == DBNull.Value) ? null : (DateTime?)row["SignDate"],
                                EnterDate: (row["EnterDate"] == DBNull.Value) ? null : (DateTime?)row["EnterDate"],
                                EndDate: (row["EndDate"] == DBNull.Value) ? null : (DateTime?)row["EndDate"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<AmnestyEntity>();
            }
        }
        public List<AmnestyDashboardEntity> SP_GetList_AmnestyForDashboard(FilterAmnestyEntity AmnestyData)
        {
            // UNDONE: to this first and move it to EncouragementsCommunicatin
            ArrayParams.Clear();
            ArrayValues.Clear();

            int TotalCount = 0;

            ArrayParams.Add("TypeLibItemID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("CurrentPage");
            ArrayParams.Add("ViewCount");

            ArrayValues.Add(AmnestyData.TypeLibItemID);
            ArrayValues.Add(AmnestyData.StartDate);
            ArrayValues.Add(AmnestyData.EndDate);
            ArrayValues.Add(AmnestyData.paging.CurrentPage);
            ArrayValues.Add(AmnestyData.paging.ViewCount);


            ArrayList OutParams = new ArrayList();
            Dictionary<string, object> OutValues = new Dictionary<string, object>();

            OutParams.Add("TotalCount");
            OutValues.Add("TotalCount", TotalCount);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_AmnestyForDashboard", ArrayValues, ArrayParams, ref OutValues, OutParams);

                List<AmnestyDashboardEntity> list = new List<AmnestyDashboardEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AmnestyDashboardEntity item = new AmnestyDashboardEntity(
                                ID: (int)row["ID"],
                                Number: (string)row["Number"],
                                TypeLibItemID: (int)row["TypeLibItemID"],
                                Source: (string)row["Source"],
                                Name: (string)row["Name"],
                                Description: (string)row["Description"],
                                RecipientLibItemID: (int)row["RecipientLibItemID"],
                                SignatoriesLibItemID: (int)row["SignatoriesLibItemID"],
                                State: (bool)row["State"],
                                SignDate: (row["SignDate"] == DBNull.Value) ? null : (DateTime?)row["SignDate"],
                                EndDate: (row["EndDate"] == DBNull.Value) ? null : (DateTime?)row["EndDate"],
                                ModifyDate: (row["ModifyDate"] == DBNull.Value) ? null : (DateTime?)row["ModifyDate"],
                                AcceptDate: (row["AcceptDate"] == DBNull.Value) ? null : (DateTime?)row["AcceptDate"],
                                CreationDate: (row["CreationDate"] == DBNull.Value) ? null : (DateTime?)row["CreationDate"],
                                EnterDate: (row["EnterDate"] == DBNull.Value) ? null : (DateTime?)row["EnterDate"],
                                Status: (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                AmnestyData.paging.TotalCount = (int)OutValues["TotalCount"];
                AmnestyData.paging.TotalPage = (int)Math.Ceiling(Convert.ToDouble(OutValues["TotalCount"]) / AmnestyData.paging.ViewCount);
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                AmnestyData.paging.TotalPage = 0;
                return new List<AmnestyDashboardEntity>();
            }
        }
        public bool Sp_Update_Amnesty(AmnestyEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("Number");
                ArrayParams.Add("TypeLibItemID");
                ArrayParams.Add("Source");
                ArrayParams.Add("Name");
                ArrayParams.Add("Description");
                ArrayParams.Add("RecipientLibItemID");
                ArrayParams.Add("SignatoriesLibItemID");
                ArrayParams.Add("State");
                ArrayParams.Add("AcceptDate ");
                ArrayParams.Add("SignDate");
                ArrayParams.Add("EnterDate");
                ArrayParams.Add("EndDate");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.Number);
                ArrayValues.Add(entity.TypeLibItemID);
                ArrayValues.Add(entity.Source);
                ArrayValues.Add(entity.Name);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.RecipientLibItemID);
                ArrayValues.Add(entity.SignatoriesLibItemID);
                ArrayValues.Add(entity.State);
                ArrayValues.Add(entity.AcceptDate);
                ArrayValues.Add(entity.SignDate);
                ArrayValues.Add(entity.EnterDate);
                ArrayValues.Add(entity.EndDate);


                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("Sp_Update_Amnesty", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public int? SP_Add_AmnestyPoints(AmnestyPointsEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("ParentID");
            ArrayParams.Add("AmnestyID");
            ArrayParams.Add("Name");
            ArrayParams.Add("Order");

            ArrayValues.Add(entity.RealParentID);
            ArrayValues.Add(entity.AmnestyID);
            ArrayValues.Add(entity.Name);
            ArrayValues.Add(entity.Order);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_AmnestyPoints", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<AmnestyPointsEntity> SP_GetList_AmnestyPoints(AmnestyPointsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("AmnestyID");
                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.AmnestyID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_AmnestyPoints", ArrayValues, ArrayParams);

                List<AmnestyPointsEntity> list = new List<AmnestyPointsEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        AmnestyPointsEntity item = new AmnestyPointsEntity(
                                ID: (int)row["ID"],
                                AmnestyID: (int)row["AmnestyID"],
                                RealParentID: (int)(row["ParentID"]),
                                Name: (string)(row["Name"]),
                                Order: (row["Order"] == DBNull.Value) ? null : (string)(row["Order"])
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<AmnestyPointsEntity>();
            }
        }
        public bool Sp_Update_AmnestyPoints(AmnestyPointsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("ParentID");
                ArrayParams.Add("Name");
                ArrayParams.Add("Order");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.ParentID);
                ArrayValues.Add(entity.Name);
                ArrayValues.Add(entity.Order);


                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("Sp_Update_AmnestyPoints", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
