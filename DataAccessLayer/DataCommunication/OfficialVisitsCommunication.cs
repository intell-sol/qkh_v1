﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_OfficialVisitsService : DataComm
    {
        public int? SP_Add_OfficialVisit(OfficialVisitsEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("TypeLibItemID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("PersonID");
            ArrayParams.Add("PositionLibItemID");
            ArrayParams.Add("Description");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.TypeLibItemID);
            ArrayValues.Add(entity.StartDate);
            ArrayValues.Add(entity.EndDate);
            ArrayValues.Add(entity.PersonID);
            ArrayValues.Add(entity.PositionLibItemID);
            ArrayValues.Add(entity.Description);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_OfficialVisits", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<OfficialVisitsEntity> SP_GetList_OfficialVisits(OfficialVisitsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_OfficialVisits", ArrayValues, ArrayParams);

                List<OfficialVisitsEntity> list = new List<OfficialVisitsEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        OfficialVisitsEntity item = new OfficialVisitsEntity(
                                ID: (int)row["ID"],
                                PersonID: (int)row["PersonID"],
                                PrisonerID: (int)row["PrisonerID"],
                                TypeLibItemID: (int)row["TypeLibItemID"],
                                TypeLibItemLabel: (string)row["TypeLibItemLabel"],
                                PersonName: (string)row["PersonName"],
                                StartDate: (DateTime)row["StartDate"],
                                EndDate: (row["EndDate"] != DBNull.Value) ? (DateTime?)row["EndDate"] : null,
                                PositionLibItemID: (int)row["PositionLibItemID"],
                                PositionLibItemLabel: (string)row["PositionLibItemLabel"],
                                Description: (row["Description"] == DBNull.Value) ? null : (string)row["Description"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"],
                                Status: (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<OfficialVisitsEntity>();
            }
        }
        public bool SP_Update_OfficialVisit(OfficialVisitsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("TypeLibItemID");
                ArrayParams.Add("StartDate");
                ArrayParams.Add("EndDate");
                ArrayParams.Add("PersonID");
                ArrayParams.Add("PositionLibItemID");
                ArrayParams.Add("Status");
                ArrayParams.Add("Description");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.TypeLibItemID);
                ArrayValues.Add(entity.StartDate);
                ArrayValues.Add(entity.EndDate);
                ArrayValues.Add(entity.PersonID);
                ArrayValues.Add(entity.PositionLibItemID);
                ArrayValues.Add(entity.Status);
                ArrayValues.Add(entity.Description);


                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_OfficialVisits", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public List<OfficialVisitsDashboardEntity> SP_GetList_OfficialVisitsForDashboard(FilterOfficialVisitsEntity EncouragementData)
        {
            // UNDONE: to this first and move it to EncouragementsCommunicatin
            ArrayParams.Clear();
            ArrayValues.Clear();

            int TotalCount = 0;

            ArrayParams.Add("FirstName");
            ArrayParams.Add("MiddleName");
            ArrayParams.Add("LastName");
            ArrayParams.Add("VisitorFirstName");
            ArrayParams.Add("VisitorMiddleName");
            ArrayParams.Add("VisitorLastName");
            ArrayParams.Add("TypeLibItemID");
            ArrayParams.Add("PositionLibItemID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("ArchiveStatus");
            ArrayParams.Add("PrisonerType");
            ArrayParams.Add("OrgUnitIDList");
            ArrayParams.Add("CurrentPage");
            ArrayParams.Add("ViewCount");

            ArrayValues.Add(EncouragementData.FirstName);
            ArrayValues.Add(EncouragementData.MiddleName);
            ArrayValues.Add(EncouragementData.LastName);
            ArrayValues.Add(EncouragementData.VisitorFirstName);
            ArrayValues.Add(EncouragementData.VisitorMiddleName);
            ArrayValues.Add(EncouragementData.VisitorLastName);
            ArrayValues.Add(EncouragementData.TypeLibItemID);
            ArrayValues.Add(EncouragementData.PositionLibItemID);
            ArrayValues.Add(EncouragementData.StartDate);
            ArrayValues.Add(EncouragementData.EndDate);
            ArrayValues.Add(EncouragementData.PrisonerID);
            ArrayValues.Add(EncouragementData.ArchiveStatus);
            ArrayValues.Add(EncouragementData.PrisonerType);
            ArrayValues.Add((EncouragementData.OrgUnitIDList != null && EncouragementData.OrgUnitIDList.Any()) ? string.Join(",", EncouragementData.OrgUnitIDList) : EncouragementData.OrgUnitID.ToString());
            ArrayValues.Add(EncouragementData.paging.CurrentPage);
            ArrayValues.Add(EncouragementData.paging.ViewCount);


            ArrayList OutParams = new ArrayList();
            Dictionary<string, object> OutValues = new Dictionary<string, object>();

            OutParams.Add("TotalCount");
            OutValues.Add("TotalCount", TotalCount);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_OfficialVisitsForDashboard", ArrayValues, ArrayParams, ref OutValues, OutParams);

                List<OfficialVisitsDashboardEntity> list = new List<OfficialVisitsDashboardEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        OfficialVisitsDashboardEntity item = new OfficialVisitsDashboardEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                TypeLibItemID: (int)row["TypeLibItemID"],
                                TypeLibItemLabel: (string)row["TypeLibItemLabel"],
                                StartDate: (row["StartDate"] == DBNull.Value) ? null : (DateTime?)row["StartDate"],
                                Status: (bool)row["Status"],
                                PositionLibItemID: (int)row["PositionLibItemID"],
                                PositionLibItemLabel: (string)row["PositionLibItemLabel"],
                                PersonName: (string)row["PersonName"],
                                Personal_ID: (string)row["Personal_ID"],
                                PrisonerName: (string)row["PrisonerName"],
                                ArchiveStatus: (bool)row["ArchiveStatus"],
                                PrisonerType: (int)row["PrisonerType"]
                            );

                        list.Add(item);
                    }
                }

                EncouragementData.paging.TotalCount = (int)OutValues["TotalCount"];
                EncouragementData.paging.TotalPage = (int)Math.Ceiling(Convert.ToDouble(OutValues["TotalCount"]) / EncouragementData.paging.ViewCount);
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EncouragementData.paging.TotalPage = 0;
                return new List<OfficialVisitsDashboardEntity>();
            }
        }
    }
}