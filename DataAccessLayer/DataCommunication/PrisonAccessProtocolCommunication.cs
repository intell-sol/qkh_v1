﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PrisonAccessProtocolService : DataComm
    {
        public int? SP_Add_PrisonAccessProtocol(PrisonAccessProtocolEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("RankLib_ID");
            ArrayParams.Add("EmployeeID");
            ArrayParams.Add("PrisonInstitutionTypeLib_ID");
            ArrayParams.Add("PrisonAccessDate");
            ArrayParams.Add("AdoptionBasis");
            ArrayParams.Add("PhysicalExamnationResult");
            ArrayParams.Add("PersonalSearchResults");
            ArrayParams.Add("PPIC");
            ArrayParams.Add("SPC");
            ArrayParams.Add("Description");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.OrgUnitID);
            ArrayValues.Add(entity.RankLib_ID);
            ArrayValues.Add(entity.EmployeeID);
            ArrayValues.Add(entity.PrisonInstitutionTypeLib_ID);
            ArrayValues.Add(entity.PrisonAccessDate);
            ArrayValues.Add(entity.AdoptionBasis);
            ArrayValues.Add(entity.PhysicalExamnationResult);
            ArrayValues.Add(entity.PersonalSearchResults);
            ArrayValues.Add(entity.PPIC);
            ArrayValues.Add(entity.SPC);
            ArrayValues.Add(entity.Description);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PrisonAccessProtocol", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<PrisonAccessProtocolEntity> SP_GetList_PrisonAccessProtocol(PrisonAccessProtocolEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PrisonAccessProtocol", ArrayValues, ArrayParams);

                List<PrisonAccessProtocolEntity> list = new List<PrisonAccessProtocolEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PrisonAccessProtocolEntity item = new PrisonAccessProtocolEntity(
                                ID:(int)row["ID"],
                                PrisonerID:(int)row["PrisonerID"],
                                OrgUnitID:(int)row["OrgUnitID"],
                                RankLib_ID:(int)row["RankLib_ID"],
                                EmployeeID:(int)row["EmployeeID"],
                                PrisonInstitutionTypeLib_ID:(int)row["PrisonInstitutionTypeLib_ID"],
                                PrisonAccessDate:(DateTime)row["PrisonAccessDate"],
                                AdoptionBasis: (row["AdoptionBasis"] == DBNull.Value) ? null : (string)row["AdoptionBasis"],
                                PhysicalExamnationResult: (string)row["PhysicalExamnationResult"],
                                PersonalSearchResults:(string)row["PersonalSearchResults"],
                                PPIC:(string)row["PPIC"],
                                SPC: (row["SPC"] == DBNull.Value) ? null : (string)row["SPC"],
                                Description:(row["Description"] == DBNull.Value) ? null : (string)row["Description"],
                                MergeStatus:(row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }


                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PrisonAccessProtocolEntity>();
            }
        }
        public bool SP_Update_PrisonAccessProtocol(PrisonAccessProtocolEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("RankLib_ID");
                ArrayParams.Add("EmployeeID");
                ArrayParams.Add("PrisonInstitutionTypeLib_ID");
                ArrayParams.Add("PrisonAccessDate");
                ArrayParams.Add("AdoptionBasis");
                ArrayParams.Add("PhysicalExamnationResult");
                ArrayParams.Add("PersonalSearchResults");
                ArrayParams.Add("PPIC");
                ArrayParams.Add("SPC");
                ArrayParams.Add("Description");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.RankLib_ID);
                ArrayValues.Add(entity.EmployeeID);
                ArrayValues.Add(entity.PrisonInstitutionTypeLib_ID);
                ArrayValues.Add(entity.PrisonAccessDate);
                ArrayValues.Add(entity.AdoptionBasis);
                ArrayValues.Add(entity.PhysicalExamnationResult);
                ArrayValues.Add(entity.PersonalSearchResults);
                ArrayValues.Add(entity.PPIC);
                ArrayValues.Add(entity.SPC);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_PrisonAccessProtocol", ArrayValues, ArrayParams);

                
                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
