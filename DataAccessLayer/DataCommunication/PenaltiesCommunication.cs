﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PenaltiesService : DataComm
    {
        // penalties part
        public int? SP_Add_Penalty(PenaltiesEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("TypeListItemID");
            ArrayParams.Add("ViolationDate");
            ArrayParams.Add("PenaltyDate");
            ArrayParams.Add("StateLibItemID");
            ArrayParams.Add("PenaltyEndDate");
            ArrayParams.Add("PenaltiesMaturity");
            ArrayParams.Add("EmployeerID");
            ArrayParams.Add("StatusChangeBasisLibItemID");
            ArrayParams.Add("StatusChangeBasis");
            ArrayParams.Add("Notes");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.TypeListItemID);
            ArrayValues.Add(entity.ViolationDate);
            ArrayValues.Add(entity.PenaltyDate);
            ArrayValues.Add(entity.StateLibItemID);
            ArrayValues.Add(entity.PenaltyEndDate);
            ArrayValues.Add(entity.PenaltiesMaturity);
            ArrayValues.Add(entity.EmployeerID);
            ArrayValues.Add(entity.StatusChangeBasisLibItemID);
            ArrayValues.Add(entity.StatusChangeBasis);
            ArrayValues.Add(entity.Notes);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Penalties", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<PenaltiesEntity> SP_GetList_Penalties(PenaltiesEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Penalties", ArrayValues, ArrayParams);

                List<PenaltiesEntity> list = new List<PenaltiesEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PenaltiesEntity item = new PenaltiesEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                TypeListItemID: (int)row["TypeListItemID"],
                                ViolationDate: (row["ViolationDate"] == DBNull.Value) ? null : (DateTime?)row["ViolationDate"],
                                PenaltyDate: (row["PenaltyDate"] == DBNull.Value) ? null : (DateTime?)row["PenaltyDate"],
                                StateLibItemID: (int)row["StateLibItemID"],
                                PenaltyEndDate: (row["PenaltyEndDate"] == DBNull.Value) ? null : (DateTime?)row["PenaltyEndDate"],
                                PenaltiesMaturity: (row["PenaltiesMaturity"] == DBNull.Value) ? null : (DateTime?)row["PenaltiesMaturity"],
                                EmployeerID: (int)row["EmployeerID"],
                                StatusChangeBasisLibItemID: (row["StatusChangeBasisLibItemID"] == DBNull.Value) ? null : (int?)row["StatusChangeBasisLibItemID"],
                                StatusChangeBasis: (row["StatusChangeBasis"] == DBNull.Value) ? null : (DateTime?)row["StatusChangeBasis"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"],
                                Notes: (row["Notes"] == DBNull.Value) ? null : (string)row["Notes"],
                                Status: (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PenaltiesEntity>();
            }
        }
        public bool SP_Update_Penalty(PenaltiesEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayParams.Add("TypeListItemID");
                ArrayParams.Add("ViolationDate");
                ArrayParams.Add("PenaltyDate");
                ArrayParams.Add("StateLibItemID");
                ArrayParams.Add("PenaltyEndDate");
                ArrayParams.Add("PenaltiesMaturity");
                ArrayParams.Add("EmployeerID");
                ArrayParams.Add("StatusChangeBasisLibItemID");
                ArrayParams.Add("StatusChangeBasis");
                ArrayParams.Add("Notes");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);
                ArrayValues.Add(entity.TypeListItemID);
                ArrayValues.Add(entity.ViolationDate);
                ArrayValues.Add(entity.PenaltyDate);
                ArrayValues.Add(entity.StateLibItemID);
                ArrayValues.Add(entity.PenaltyEndDate);
                ArrayValues.Add(entity.PenaltiesMaturity);
                ArrayValues.Add(entity.EmployeerID);
                ArrayValues.Add(entity.StatusChangeBasisLibItemID);
                ArrayValues.Add(entity.StatusChangeBasis);
                ArrayValues.Add(entity.Notes);
                ArrayValues.Add(entity.Status);


                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Penalties", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        // violation to penalties part
        public int? SP_Add_ViolationLibItemToPenalty(PenaltiesViolationEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("PenaltyID");
            ArrayParams.Add("LibItemID");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.PenaltyID);
            ArrayValues.Add(entity.LibItemID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_ViolationLibItemToPenalty", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<PenaltiesViolationEntity> SP_GetList_ViolationLibItemToPenalty(PenaltiesViolationEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayParams.Add("PenaltyID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);
                ArrayValues.Add(entity.PenaltyID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_ViolationLibItemToPenalty", ArrayValues, ArrayParams);

                List<PenaltiesViolationEntity> list = new List<PenaltiesViolationEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PenaltiesViolationEntity item = new PenaltiesViolationEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                PenaltyID: (int)row["PenaltyID"],
                                LibItemID: (int)row["LibItemID"],
                                Status: (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PenaltiesViolationEntity>();
            }
        }
        public bool SP_Update_ViolationLibItemToPenalty(PenaltiesViolationEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayParams.Add("PenaltyID");
                ArrayParams.Add("LibItemID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);
                ArrayValues.Add(entity.PenaltyID);
                ArrayValues.Add(entity.LibItemID);
                ArrayValues.Add(entity.Status);


                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_ViolationLibItemToPenalty", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        // penalties protest part
        public int? SP_Add_PenaltyProtest(PenaltiesProtestEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PenaltyID");
            ArrayParams.Add("AppealsContentLibItemID");
            ArrayParams.Add("AppealsBody");
            ArrayParams.Add("AppealsDate");
            ArrayParams.Add("AppealsNote");

            ArrayValues.Add(entity.PenaltyID);
            ArrayValues.Add(entity.AppealsContentLibItemID);
            ArrayValues.Add(entity.AppealsBody);
            ArrayValues.Add(entity.AppealsDate);
            ArrayValues.Add(entity.AppealsNote);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PenaltyProtest", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<PenaltiesProtestEntity> SP_GetList_PenaltyProtest(PenaltiesProtestEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PenaltyID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PenaltyID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PenaltyProtest", ArrayValues, ArrayParams);

                List<PenaltiesProtestEntity> list = new List<PenaltiesProtestEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PenaltiesProtestEntity item = new PenaltiesProtestEntity(
                                ID: (int)row["ID"],
                                PenaltyID: (int)row["PenaltyID"],
                                AppealsContentLibItemID: (row["AppealsContentLibItemID"] == DBNull.Value) ? null : (int?)row["AppealsContentLibItemID"],
                                AppealsBody: (row["AppealsBody"] == DBNull.Value) ? null : (int?)row["AppealsBody"],
                                AppealsDate: (row["AppealsDate"] == DBNull.Value) ? null : (DateTime?)row["AppealsDate"],
                                AppealsNote: (row["AppealsNote"] == DBNull.Value) ? null : (string)row["AppealsNote"],
                                AppealResultLibItemID: (row["AppealResultLibItemID"] == DBNull.Value) ? null : (int?)row["AppealResultLibItemID"],
                                AppealResultNote: (row["AppealResultNote"] == DBNull.Value) ? null : (string)row["AppealResultNote"],
                                AppealResultDate: (row["AppealResultDate"] == DBNull.Value) ? null : (DateTime?)row["AppealResultDate"],
                                Status: (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PenaltiesProtestEntity>();
            }
        }
        public bool SP_Update_PenaltyProtest(PenaltiesProtestEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PenaltyID");
                ArrayParams.Add("AppealsContentLibItemID");
                ArrayParams.Add("AppealsBody");
                ArrayParams.Add("AppealsDate");
                ArrayParams.Add("AppealsNote");
                ArrayParams.Add("AppealResultLibItemID");
                ArrayParams.Add("AppealResultNote");
                ArrayParams.Add("AppealResultDate");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PenaltyID);
                ArrayValues.Add(entity.AppealsContentLibItemID);
                ArrayValues.Add(entity.AppealsBody);
                ArrayValues.Add(entity.AppealsDate);
                ArrayValues.Add(entity.AppealsNote);
                ArrayValues.Add(entity.AppealResultLibItemID);
                ArrayValues.Add(entity.AppealResultNote);
                ArrayValues.Add(entity.AppealResultDate);
                ArrayValues.Add(entity.Status);


                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_PenaltyProtest", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }


        public List<PenaltiesDashboardEntity> SP_GetList_PenaltiesForDashboard(FilterPenaltiesEntity PenaltiiesData)
        {
            // UNDONE: to this first and move it to EncouragementsCommunicatin
            ArrayParams.Clear();
            ArrayValues.Clear();

            int TotalCount = 0;

            ArrayParams.Add("FirstName");
            ArrayParams.Add("MiddleName");
            ArrayParams.Add("LastName");
            ArrayParams.Add("TypeLibItemList");
            ArrayParams.Add("StateLibItemID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("EmployeerID");
            ArrayParams.Add("ArchiveStatus");
            ArrayParams.Add("PrisonerType");
            ArrayParams.Add("OrgUnitIDList");
            ArrayParams.Add("CurrentPage");
            ArrayParams.Add("ViewCount");

            ArrayValues.Add(PenaltiiesData.FirstName);
            ArrayValues.Add(PenaltiiesData.MiddleName);
            ArrayValues.Add(PenaltiiesData.LastName);
            ArrayValues.Add((PenaltiiesData.OrgUnitIDList != null && PenaltiiesData.TypeLibItemList.Any()) ? string.Join(",", PenaltiiesData.TypeLibItemList) : null);

            ArrayValues.Add(PenaltiiesData.StateLibItemID);
            ArrayValues.Add(PenaltiiesData.StartDate);
            ArrayValues.Add(PenaltiiesData.EndDate);
            ArrayValues.Add(PenaltiiesData.PrisonerID);
            ArrayValues.Add(PenaltiiesData.EmployeerID);
            ArrayValues.Add(PenaltiiesData.ArchiveStatus);
            ArrayValues.Add(PenaltiiesData.PrisonerType);
            ArrayValues.Add((PenaltiiesData.OrgUnitIDList != null && PenaltiiesData.OrgUnitIDList.Any()) ? string.Join(",", PenaltiiesData.OrgUnitIDList) : PenaltiiesData.OrgUnitID.ToString());
            ArrayValues.Add(PenaltiiesData.paging.CurrentPage);
            ArrayValues.Add(PenaltiiesData.paging.ViewCount);


            ArrayList OutParams = new ArrayList();
            Dictionary<string, object> OutValues = new Dictionary<string, object>();

            OutParams.Add("TotalCount");
            OutValues.Add("TotalCount", TotalCount);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PenaltiesForDashboard", ArrayValues, ArrayParams, ref OutValues, OutParams);

                List<PenaltiesDashboardEntity> list = new List<PenaltiesDashboardEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PenaltiesDashboardEntity item = new PenaltiesDashboardEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                TypeListItemID: (int)row["TypeListItemID"],
                                PenaltyDate: (row["PenaltyDate"] == DBNull.Value) ? null : (DateTime?)row["PenaltyDate"],
                                PenaltyEndDate: (row["PenaltyEndDate"] == DBNull.Value) ? null : (DateTime?)row["PenaltyEndDate"],
                                PenaltiesMaturity: (row["PenaltiesMaturity"] == DBNull.Value) ? null : (DateTime?)row["PenaltiesMaturity"],
                                EmployeerID: (int)row["EmployeerID"],
                                //StatusChangeBasisLibItemID: (int)row["StatusChangeBasisLibItemID"],
                                StatusChangeBasisLibItemID: (row["StatusChangeBasisLibItemID"]==DBNull.Value) ? null : (int?)row["StatusChangeBasisLibItemID"],
                                Notes: (row["Notes"] == DBNull.Value) ? null : (string)row["Notes"],
                                Status: (bool)row["Status"],
                                TypeListItemName: (string)row["TypeListItemName"],
                                StateLibItemLabel: (row["StateLibItemLabel"] == DBNull.Value) ? null : (string)row["StateLibItemLabel"],
                                StatusChangeBasisLibItemName: (row["StatusChangeBasisLibItemName"] == DBNull.Value)?null:(string)row["StatusChangeBasisLibItemName"],
                                EmployeerName: (string)row["EmployeerName"],
                                Personal_ID: (string)row["Personal_ID"],
                                PrisonerName: (string)row["PrisonerName"],
                                ArchiveStatus: (bool)row["ArchiveStatus"],
                                PrisonerType: (int)row["PrisonerType"]
                            );

                        list.Add(item);
                    }
                }

                PenaltiiesData.paging.TotalCount = (int)OutValues["TotalCount"];
                PenaltiiesData.paging.TotalPage = (int)Math.Ceiling(Convert.ToDouble(OutValues["TotalCount"]) / PenaltiiesData.paging.ViewCount);
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                PenaltiiesData.paging.TotalPage = 0;
                return new List<PenaltiesDashboardEntity>();
            }
        }
    }
}