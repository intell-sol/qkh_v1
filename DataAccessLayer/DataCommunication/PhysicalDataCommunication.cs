﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PhysicalDataService:DataComm
    {
        public int? SP_Add_PhysicalData(PhysicalDataEntity PhysicalData)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("SkinColorLibItem_ID");
            ArrayParams.Add("EyesColorLibItem_ID");
            ArrayParams.Add("HairsColorLibItem_ID");
            ArrayParams.Add("BloodTypeLibItem_ID");
            ArrayParams.Add("ExternalDesc");

            ArrayValues.Add(PhysicalData.PrisonerID);
            ArrayValues.Add(PhysicalData.SkinColorLibItem_ID);
            ArrayValues.Add(PhysicalData.EyesColorLibItem_ID);
            ArrayValues.Add(PhysicalData.HairsColorLibItem_ID);
            ArrayValues.Add(PhysicalData.BloodTypeLibItem_ID);
            ArrayValues.Add(PhysicalData.ExternalDesc);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PhysicalData", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public bool SP_Update_PhysicalData(PhysicalDataEntity PhysicalData) {

            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("SkinColorLibItem_ID");
            ArrayParams.Add("EyesColorLibItem_ID");
            ArrayParams.Add("HairsColorLibItem_ID");
            ArrayParams.Add("BloodTypeLibItem_ID");
            ArrayParams.Add("ExternalDesc");

            ArrayValues.Add(PhysicalData.PrisonerID);
            ArrayValues.Add(PhysicalData.SkinColorLibItem_ID);
            ArrayValues.Add(PhysicalData.EyesColorLibItem_ID);
            ArrayValues.Add(PhysicalData.HairsColorLibItem_ID);
            ArrayValues.Add(PhysicalData.BloodTypeLibItem_ID);
            ArrayValues.Add(PhysicalData.ExternalDesc);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_PhysicalData", ArrayValues, ArrayParams);

                if (dt != null) return true;

                return false;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return false;
            }
        }

        public List<PhysicalDataEntity> SP_GetList_PhysicalData(int? ID = null, int? PrisonerID = null)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(ID);
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PhysicalData", ArrayValues, ArrayParams);

                List<PhysicalDataEntity> list = new List<PhysicalDataEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PhysicalDataEntity item = new PhysicalDataEntity(
                                ID:(int)row["ID"],
                                PrisonerID:(int)row["PrisonerID"],
                                SkinColorLibItem_ID: (row["SkinColorLibItem_ID"] == DBNull.Value) ? null : (int?)row["SkinColorLibItem_ID"],
                                EyesColorLibItem_ID: (row["EyesColorLibItem_ID"] == DBNull.Value) ? null : (int?)row["EyesColorLibItem_ID"],
                                HairsColorLibItem_ID: (row["HairsColorLibItem_ID"] == DBNull.Value) ? null : (int?)row["HairsColorLibItem_ID"],
                                BloodTypeLibItem_ID: (row["BloodTypeLibItem_ID"] == DBNull.Value) ? null : (int?)row["BloodTypeLibItem_ID"],
                                ExternalDesc: (row["ExternalDesc"] == DBNull.Value) ? null : (string)row["ExternalDesc"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PhysicalDataEntity>();
            }
        }
    }
}
