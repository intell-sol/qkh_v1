﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_IdentificationDocumentsService : DataComm
    {
        public int? SP_Add_IdentificationDocuments(IdentificationDocument doc)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PersonID");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("TypeLibItem_ID");
            ArrayParams.Add("Number");
            ArrayParams.Add("Date");
            ArrayParams.Add("FromWhom");
            ArrayParams.Add("CitizenshipLibItem_ID");

            ArrayValues.Add(doc.PersonID);
            ArrayValues.Add(doc.PrisonerID);
            ArrayValues.Add(doc.TypeLibItem_ID);
            ArrayValues.Add(doc.Number);
            ArrayValues.Add(doc.Date);
            ArrayValues.Add(doc.FromWhom);
            ArrayValues.Add(doc.CitizenshipLibItem_ID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_IdentificationDocuments", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<IdentificationDocument> SP_GetList_IdentificationDocuments(IdentificationDocument entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PersonID");
                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PersonID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_IdentificationDocuments", ArrayValues, ArrayParams);

                List<IdentificationDocument> list = new List<IdentificationDocument>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        IdentificationDocument item = new IdentificationDocument(
                                ID: (int)row["ID"],
                                PersonID: (int)row["PersonID"],
                                PrisonerID: (row["PrisonerID"] == DBNull.Value) ? null : (int?)row["PrisonerID"],
                                CitizenshipLibItem_ID: (int)row["CitizenshipLibItem_ID"],
                                CitizenshipLibItem_Name: (row["CitizenshipLibItem_Name"] == DBNull.Value) ? null : (string)row["CitizenshipLibItem_Name"],
                                TypeLibItem_Name: (row["TypeLibItem_Name"] == DBNull.Value) ? null : (string)row["TypeLibItem_Name"],
                                TypeLibItem_ID: (int)row["TypeLibItem_ID"],
                                Number: (string)row["Number"],
                                Date: (DateTime)row["Date"],
                                FromWhom: (string)row["FromWhom"],
                                MergeStatus: (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<IdentificationDocument>();
            }
        }
        public bool SP_Update_IdentificationDocuments(IdentificationDocument doc)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("TypeLibItem_ID");
                ArrayParams.Add("Number");
                ArrayParams.Add("Date");
                ArrayParams.Add("FromWhom");
                ArrayParams.Add("CitizenshipLibItem_ID");
                ArrayParams.Add("Status");

                ArrayValues.Add(doc.ID);
                ArrayValues.Add(doc.TypeLibItem_ID);
                ArrayValues.Add(doc.Number);
                ArrayValues.Add(doc.Date);
                ArrayValues.Add(doc.FromWhom);
                ArrayValues.Add(doc.CitizenshipLibItem_ID);
                ArrayValues.Add(doc.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_IdentificationDocuments", ArrayValues, ArrayParams);

                
                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
