﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_ProfessionsService:DataComm
    {
        public int? SP_Add_Professions(ProfessionEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("EducationProfessionsID");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("LibItemID");

            ArrayValues.Add(entity.EducationProfessionsID);
            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.LibItemID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Professions", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<ProfessionEntity> SP_GetList_Professions(ProfessionEntity entity )
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("EducationProfessionsID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.EducationProfessionsID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Professions", ArrayValues, ArrayParams);

                List<ProfessionEntity> list = new List<ProfessionEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        ProfessionEntity item = new ProfessionEntity(
                                (int)row["ID"],
                                (int)row["PrisonerID"],
                                (int)row["EducationProfessionsID"],
                                (int)row["LibItemID"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<ProfessionEntity>();
            }
        }
        public bool SP_Update_Professions(ProfessionEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("LibItemID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.LibItemID);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Professions", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
