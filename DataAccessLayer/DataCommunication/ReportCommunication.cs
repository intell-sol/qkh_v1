﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_ReportService : DataComm
    {
        
        public List<EarlyReportEntity> SP_GetReportList_Early(EarlyReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("Date");
            ArrayValues.Add(String.Join(",", entity.OrgUnitList));
            ArrayValues.Add(entity.Date);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_Early", ArrayValues, ArrayParams);

            List<EarlyReportEntity> list = new List<EarlyReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    EarlyReportEntity item = new EarlyReportEntity(
                            PrisonerName: (string)row["PrisonerName"],
                            BirthDay: (row["BirthDay"] == DBNull.Value) ? null : (DateTime?)row["BirthDay"],
                            PreviousConviction: (row["PreviousConviction"] != DBNull.Value) ? (string)row["PreviousConviction"] : null,
                            PresentConviction: (row["PresentConviction"] != DBNull.Value) ? (string)row["PresentConviction"] : null,
                            SentencingDate: (row["SentencingDate"] == DBNull.Value) ? null : (DateTime?)row["SentencingDate"],
                            StartDate: (row["StartDate"] == DBNull.Value) ? null : (DateTime?)row["StartDate"],
                            Encouragements: (row["Encouragements"] != DBNull.Value) ? (string)row["Encouragements"] : null,
                            PenaltyName: (row["PenaltyName"] != DBNull.Value) ? (string)row["PenaltyName"] : null,
                            CloseDate: (DateTime)row["CloseDate"],
                            OrgUnits: String.Join(",", entity.OrgUnitList),
                            FilterDate: entity.Date
                        );

                    list.Add(item);
                }
            }

            return list;
        }
        public List<CustomReportEntity> SP_GetReportList_Custom(CustomReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("FirstDate");
            ArrayParams.Add("SecondDate");
            ArrayParams.Add("FPersonalNumber");
            ArrayParams.Add("FPersonalCaseNumber");
            ArrayParams.Add("FPrisonerName");
            ArrayParams.Add("FPrisonerType");
            ArrayParams.Add("FBirthDay");
            ArrayParams.Add("FSexType");
            ArrayParams.Add("FNationality");
            ArrayParams.Add("FCitizenship");
            ArrayParams.Add("FArrestStartDate");
            ArrayParams.Add("FSentencingStartDate");
            ArrayParams.Add("FPrisonAccessDate");
            ArrayParams.Add("FPenaltyName");
            ArrayParams.Add("FEncouragementsName");
            ArrayParams.Add("FInjunctionsName");
            ArrayParams.Add("FCaseCloseType");
            ArrayParams.Add("FTransfersCount");
            ArrayParams.Add("FPunishmentType");
            ArrayParams.Add("FSentenceArticleType");
            ArrayParams.Add("FPresentArticle");
            ArrayParams.Add("FConflictsPersons");
            ArrayParams.Add("FCameraCardNumber");
            ArrayParams.Add("FParents");
            ArrayParams.Add("FChildren");
            ArrayParams.Add("FSisterBrother");
            ArrayParams.Add("FSpouses");
            ArrayParams.Add("FEducationName");
            ArrayParams.Add("FProfessionName");
            ArrayParams.Add("FWorkName");
            ArrayParams.Add("FPreviousConviction");
            ArrayParams.Add("FBloodType");
            ArrayParams.Add("FInvalidClass");
            ArrayParams.Add("FLeaningNames");
            ArrayParams.Add("FOfficialVisitsCount");
            ArrayParams.Add("FPersonalVisitsCount");
            ArrayParams.Add("FPackagesCount");
            ArrayParams.Add("FirstName");
            ArrayParams.Add("LastName");
            ArrayParams.Add("MiddleName");
            ArrayParams.Add("SexType");
            ArrayParams.Add("Nationality");
            ArrayParams.Add("Citizenship");
            ArrayParams.Add("PersonalID");
            ArrayParams.Add("PersonalCaseNumber");
            ArrayParams.Add("OrgUnit");
            ArrayParams.Add("Type"); 
            ArrayParams.Add("PunishmentType"); 
            ArrayParams.Add("SentenceArticleType"); 
            ArrayParams.Add("PenaltyType"); 
            ArrayParams.Add("Leaning"); 
            ArrayParams.Add("Invalid"); 
            ArrayParams.Add("Age"); 

            ArrayValues.Add(entity.FirstDate);
            ArrayValues.Add(entity.SecondDate);
            ArrayValues.Add(entity.chPersonalNumber);
            ArrayValues.Add(entity.chPersonalCaseNumber);
            ArrayValues.Add(entity.chPrisonerName);
            ArrayValues.Add(entity.chPrisonerType);
            ArrayValues.Add(entity.chBirthDay);
            ArrayValues.Add(entity.chSexType);
            ArrayValues.Add(entity.chNationality);
            ArrayValues.Add(entity.chCitizenship);
            ArrayValues.Add(entity.chArrestStartDate);
            ArrayValues.Add(entity.chSentencingStartDate);
            ArrayValues.Add(entity.chPrisonAccessDate);
            ArrayValues.Add(entity.chPenaltyName);
            ArrayValues.Add(entity.chEncouragementsName);
            ArrayValues.Add(entity.chInjunctionsName);
            ArrayValues.Add(entity.chCaseCloseType);
            ArrayValues.Add(entity.chTransfersCount);
            ArrayValues.Add(entity.chPunishmentType);
            ArrayValues.Add(entity.chSentenceArticleType);
            ArrayValues.Add(entity.chPresentArticle);
            ArrayValues.Add(entity.chConflictsPersons);
            ArrayValues.Add(entity.chCameraCardNumber);
            ArrayValues.Add(entity.chParents);
            ArrayValues.Add(entity.chChildren);
            ArrayValues.Add(entity.chSisterBrother);
            ArrayValues.Add(entity.chSpouses);
            ArrayValues.Add(entity.chEducationName);
            ArrayValues.Add(entity.chProfessionName);
            ArrayValues.Add(entity.chWorkName);
            ArrayValues.Add(entity.chPreviousConviction);
            ArrayValues.Add(entity.chBloodType);
            ArrayValues.Add(entity.chInvalidClass);
            ArrayValues.Add(entity.chLeaningNames);
            ArrayValues.Add(entity.chOfficialVisitsCount);
            ArrayValues.Add(entity.chPersonalVisitsCount);
            ArrayValues.Add(entity.chPackagesCount);
            ArrayValues.Add(entity.FirstName);
            ArrayValues.Add(entity.LastName);
            ArrayValues.Add(entity.MiddleName);
            ArrayValues.Add(entity.SexType);
            ArrayValues.Add(entity.Nationality);
            ArrayValues.Add(entity.Citizenship);
            ArrayValues.Add(entity.PersonalID);
            ArrayValues.Add(entity.PersonalCaseNumber);
            ArrayValues.Add(entity.OrgUnit);
            ArrayValues.Add(entity.Type);
            ArrayValues.Add(entity.PunishmentType);
            ArrayValues.Add(entity.SentenceArticleType);
            ArrayValues.Add(entity.PenaltyType);
            ArrayValues.Add(entity.Leaning);
            ArrayValues.Add(entity.Invalid);
            ArrayValues.Add(entity.Age);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_Custom", ArrayValues, ArrayParams);

            List<CustomReportEntity> list = new List<CustomReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                        
                {
                    CustomReportEntity item = new CustomReportEntity(
                            PersonalID: (row["Personal_ID"] == DBNull.Value) ? null : (string)row["Personal_ID"],
                            PersonalCaseNumber: (row["PersonalCaseNumber"] == DBNull.Value) ? null : (int?)row["PersonalCaseNumber"],
                            FirstName: (row["PrisonerName"] == DBNull.Value) ? null : (string)row["PrisonerName"],
                            PrisonerType: (row["PrisonerType"] != DBNull.Value) ? (string)row["PrisonerType"] : null,
                            BirthDay: (row["BirthDay"] == DBNull.Value) ? null : (DateTime?)row["BirthDay"],
                            SexTypeName: (row["SexType"] == DBNull.Value) ? null : (string)row["SexType"],
                            Nationality: (row["Nationality"] != DBNull.Value) ? (string)row["Nationality"] : null,
                            Citizenship: (row["Citizenship"] != DBNull.Value) ? (string)row["Citizenship"] : null,
                            ArrestStartDate: (row["ArrestStartDate"] != DBNull.Value) ? (string)row["ArrestStartDate"] : null,
                            SentencingStartDate: (row["SentencingStartDate"] != DBNull.Value) ? (string)row["SentencingStartDate"] : null,
                            PrisonAccessDate: (row["PrisonAccessDate"] != DBNull.Value) ? (string)row["PrisonAccessDate"] : null,
                            PenaltyName: (row["PenaltyName"] != DBNull.Value) ? (string)row["PenaltyName"] : null,
                            EncouragementsName: (row["EncouragementsName"] != DBNull.Value) ? (string)row["EncouragementsName"] : null,
                            InjunctionsName: (row["InjunctionsName"] != DBNull.Value) ? (string)row["InjunctionsName"] : null,
                            CaseCloseType: (row["CaseCloseType"] != DBNull.Value) ? (string)row["CaseCloseType"] : null,
                            TransfersCount: (row["TransfersCount"] == DBNull.Value) ? null : (string)row["TransfersCount"],
                            PunishmentType: (row["PunishmentType"] != DBNull.Value) ? (string)row["PunishmentType"] : null,
                            SentenceArticleType: (row["SentenceArticleType"] != DBNull.Value) ? (string)row["SentenceArticleType"] : null,
                            PresentArticle: (row["PresentArticle"] != DBNull.Value) ? (string)row["PresentArticle"] : null,
                            ConflictsPersons: (row["ConflictsPersons"] != DBNull.Value) ? (string)row["ConflictsPersons"] : null,
                            CameraCardNumber: (row["CameraCardNumber"] == DBNull.Value) ? null : (int?)row["CameraCardNumber"],
                            Parents: (row["Parents"] != DBNull.Value) ? (string)row["Parents"] : null,
                            Children: (row["Children"] != DBNull.Value) ? (string)row["Children"] : null,
                            SisterBrother: (row["SisterBrother"] != DBNull.Value) ? (string)row["SisterBrother"] : null,
                            Spouses: (row["Spouses"] != DBNull.Value) ? (string)row["Spouses"] : null,
                            EducationName: (row["EducationName"] != DBNull.Value) ? (string)row["EducationName"] : null,
                            ProfessionName: (row["ProfessionName"] != DBNull.Value) ? (string)row["ProfessionName"] : null,
                            WorkName: (row["WorkName"] != DBNull.Value) ? (string)row["WorkName"] : null,
                            PreviousConviction: (row["PreviousConviction"] != DBNull.Value) ? (string)row["PreviousConviction"] : null,
                            BloodType: (row["BloodType"] != DBNull.Value) ? (string)row["BloodType"] : null,
                            InvalidClass: (row["InvalidClass"] != DBNull.Value) ? (string)row["InvalidClass"] : null,
                            LeaningNames: (row["LeaningNames"] != DBNull.Value) ? (string)row["LeaningNames"] : null,
                            OfficialVisitsCount: (row["OfficialVisitsCount"] == DBNull.Value) ? null : (int?)row["OfficialVisitsCount"],
                            PersonalVisitsCount: (row["PersonalVisitsCount"] == DBNull.Value) ? null : (int?)row["PersonalVisitsCount"],
                            PackagesCount: (row["PackagesCount"] == DBNull.Value) ? null : (int?)row["PackagesCount"]
                        );

                    list.Add(item);
                }
            }

            return list;
        }
        public List<OpenTypeReportEntity> SP_GetReportList_OpenType(OpenTypeReportEntity entity)
        {
             ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("TypeID");
            ArrayParams.Add("Date");

            ArrayValues.Add(String.Join(",", entity.OrgUnitList));
            ArrayValues.Add(entity.TypeID);
            ArrayValues.Add(entity.Date);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_OpenType", ArrayValues, ArrayParams);

            List<OpenTypeReportEntity> list = new List<OpenTypeReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    OpenTypeReportEntity item = new OpenTypeReportEntity(
                            Number: Convert.ToInt32(row["Number"]),
                            PrisonerName: (row["PrisonerName"] == DBNull.Value) ? null : (string)row["PrisonerName"],
                            TypeName: (row["Name"] == DBNull.Value) ? null : (string)row["Name"],
                            Decision: (row["Decision"] == DBNull.Value) ? null : (string)row["Decision"],
                            SentencingStartDate: (row["SentencingStartDate"] == DBNull.Value) ? null : (DateTime?)row["SentencingStartDate"],
                            SentencingEndDate: (row["SentencingEndDate"] == DBNull.Value) ? null : (DateTime?)row["SentencingEndDate"],
                            EarlyDate: (row["EarlyDate"] == DBNull.Value) ? null : (DateTime?)row["EarlyDate"],
                            PrisonAccessDate: (row["PrisonAccessDate"] == DBNull.Value) ? null : (DateTime?)row["PrisonAccessDate"],
                            OrgUnits: (row["OrgUnits"] == DBNull.Value) ? null : (string)row["OrgUnits"],
                            FilterDate: (row["FilterDate"] == DBNull.Value) ? null : (DateTime?)row["FilterDate"]
                        );

                    list.Add(item);
                }
            }

            return list;
        }

        public List<PersonalDescriptionReportEntity> SP_GetReportList_PersonalDescription(PersonalDescriptionReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            
            ArrayValues.Add(entity.PrisonerID);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_PersonalDescription", ArrayValues, ArrayParams);

            List<PersonalDescriptionReportEntity> list = new List<PersonalDescriptionReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    PersonalDescriptionReportEntity item = new PersonalDescriptionReportEntity(

                            PrisonerID: (int)row["PrisonerID"],
                            PrisonerName: (row["PrisonerName"] == DBNull.Value) ? null : (string)row["PrisonerName"],
                            BirthDay: (row["BirthDay"] == DBNull.Value) ? null : (DateTime?)row["BirthDay"],
                            RegAddress: (row["RegAddress"] == DBNull.Value) ? null : (string)row["RegAddress"],
                            Nationality: (row["Nationality"] == DBNull.Value) ? null : (string)row["Nationality"],
                            Citizenship: (row["Citizenship"] == DBNull.Value) ? null : (string)row["Citizenship"],
                            LivingAddress: (row["LivingAddress"] == DBNull.Value) ? null : (string)row["LivingAddress"],
                            FamilyStatus: (row["FamilyStatus"] == DBNull.Value) ? null : (string)row["FamilyStatus"],
                            EducationName: (row["EducationName"] == DBNull.Value) ? null : (string)row["EducationName"],
                            ProfessionsName: (row["ProfessionsName"] == DBNull.Value) ? null : (string)row["ProfessionsName"],
                            WorkName: (row["WorkName"] == DBNull.Value) ? null : (string)row["WorkName"],
                            ArmyName: (row["ArmyName"] == DBNull.Value) ? null : (string)row["ArmyName"],
                            HealthType: (row["HealthType"] == DBNull.Value) ? null : (string)row["HealthType"],
                            PreviousConviction: (row["PrevConv"] == DBNull.Value) ? null : (string)row["PrevConv"],
                            PresentConviction: (row["PresentConviction"] == DBNull.Value) ? null : (string)row["PresentConviction"],
                            PresentConvDuration: (row["PresentConvDuration"] == DBNull.Value) ? null : (string)row["PresentConvDuration"],
                            Fine: (row["Fine"] == DBNull.Value) ? null : (string)row["Fine"],
                            SentencingStartDate: (row["SentencingStartDate"] == DBNull.Value) ? null : (DateTime?)row["SentencingStartDate"],
                            SentencingEndDate: (row["SentencingEndDate"] == DBNull.Value) ? null : (DateTime?)row["SentencingEndDate"],
                            EarlyDate: (row["EarlyDate"] == DBNull.Value) ? null : (DateTime?)row["EarlyDate"],
                            EarlyType: (row["EarlyType"] == DBNull.Value) ? null : (string)row["EarlyType"],
                            PrisonAccessDate: (row["PrisonAccessDate"] == DBNull.Value) ? null : (DateTime?)row["PrisonAccessDate"],
                            OrgType: (row["OrgType"] == DBNull.Value) ? null : (string)row["OrgType"],
                            LowSuit: (row["LowSuit"] == DBNull.Value) ? null : (string)row["LowSuit"],
                            LowSuitState: (row["LowSuitState"] == DBNull.Value) ? null : (string)row["LowSuitState"],
                            LowSuitAmount: (row["LowSuitAmount"] == DBNull.Value) ? null : (string)row["LowSuitAmount"],
                            PenaltyName: (row["PenaltyName"] == DBNull.Value) ? null : (string)row["PenaltyName"],
                            EncouragementsName: (row["EncouragementsName"] == DBNull.Value) ? null : (string)row["EncouragementsName"],
                            DateTimeForReport : DateTime.Now.Date,
                            OrgUnitName: (row["OrgUnitName"] == DBNull.Value) ? null : (string)row["OrgUnitName"]
                       );
                    list.Add(item);
                }
            }

            return list;
        }

        public List<CaseClosedReportEntity> SP_GetReportList_CaseClosed(CaseClosedReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("Date");

            ArrayValues.Add(String.Join(",", entity.OrgUnitList));
            ArrayValues.Add(entity.Date);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_CaseClosed", ArrayValues, ArrayParams);

            List<CaseClosedReportEntity> list = new List<CaseClosedReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    CaseClosedReportEntity item = new CaseClosedReportEntity(
                            Date: (row["FilterDate"] == DBNull.Value) ? null : (DateTime?)row["FilterDate"],
                            PrisonerName: (row["PrisonerName"] == DBNull.Value) ? null : (string)row["PrisonerName"],
                            Sentencing: (row["Sentencing"] == DBNull.Value) ? null : (string)row["Sentencing"],
                            SentencingStartDate: (row["SentencingStartDate"] == DBNull.Value) ? null : (DateTime?)row["SentencingStartDate"],
                            CloseInfo: (row["CloseInfo"] == DBNull.Value) ? null : (string)row["CloseInfo"],
                            OrgUnitNames: (row["OrgUnitNames"] == DBNull.Value) ? null : (string)row["OrgUnitNames"],
                            CaseCloseType: (row["CaseCLoseType"] == DBNull.Value) ? null : (string)row["CaseCLoseType"],
                            Description: (row["Description"] == DBNull.Value) ? null : (string)row["Description"],
                            CurDate: DateTime.Now.Date
                       );
                    list.Add(item);
                }
            }

            return list;
        }

        public List<PrisonerReferenceReportEntity> SP_GetReportList_Reference(PrisonerReferenceReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");

            ArrayValues.Add(entity.PrisonerID);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_Reference", ArrayValues, ArrayParams);

            List<PrisonerReferenceReportEntity> list = new List<PrisonerReferenceReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    PrisonerReferenceReportEntity item = new PrisonerReferenceReportEntity(
                            PrisonerID: (int)row["PrisonerID"],
                            OrgUnitName: (row["OrgUnitName"] == DBNull.Value) ? null : (string)row["OrgUnitName"],
                            FileID: (row["FileID"] == DBNull.Value) ? null : (int?)row["FileID"],
                            PrisonerName: (row["PrisonerName"] == DBNull.Value) ? null : (string)row["PrisonerName"],
                            BirthDay: (row["BirthDay"] == DBNull.Value) ? null : (DateTime?)row["BirthDay"],
                            RegAddress: (row["RegAddress"] == DBNull.Value) ? null : (string)row["RegAddress"],
                            Citizenship: (row["Citizenship"] == DBNull.Value) ? null : (string)row["Citizenship"],
                            Nationality: (row["Nationality"] == DBNull.Value) ? null : (string)row["Nationality"],
                            EducationName: (row["EducationName"] == DBNull.Value) ? null : (string)row["EducationName"],
                            FamilyStatus: (row["FamilyStatus"] == DBNull.Value) ? null : (string)row["FamilyStatus"],
                            LivingAddress: (row["LivingAddress"] == DBNull.Value) ? null : (string)row["LivingAddress"],
                            PreviousConviction: (row["PreviousConviction"] == DBNull.Value) ? null : (string)row["PreviousConviction"],
                            PresentSentencingName: (row["PresentSentencingName"] == DBNull.Value) ? null : (string)row["PresentSentencingName"],
                            SentencingVerdictDate: (row["SentencingVerdictDate"] == DBNull.Value) ? null : (DateTime?)row["SentencingVerdictDate"],
                            SentencingStartDate: (row["SentencingStartDate"] == DBNull.Value) ? null : (DateTime?)row["SentencingStartDate"],
                            Description: (row["Description"] == DBNull.Value) ? null : (string)row["Description"],
                            EarlyDate: (row["EarlyDate"] == DBNull.Value) ? null : (DateTime?)row["EarlyDate"],
                            EarlyType: (row["EarlyType"] == DBNull.Value) ? null : (string)row["EarlyType"],
                            PenaltyName: (row["PenaltyName"] == DBNull.Value) ? null : (string)row["PenaltyName"],
                            EncouragementsName: (row["EncouragementsName"] == DBNull.Value) ? null : (string)row["EncouragementsName"],
                            Passports: (row["Passports"] == DBNull.Value) ? null : (string)row["Passports"],
                            Spouses: (row["Spouses"] == DBNull.Value) ? null : (string)row["Spouses"],
                            Parents: (row["Parents"] == DBNull.Value) ? null : (string)row["Parents"],
                            Children: (row["Children"] == DBNull.Value) ? null : (string)row["Children"],
                            SisterBrother: (row["SisterBrother"] == DBNull.Value) ? null : (string)row["SisterBrother"],
                            Transfers: (row["TransfersName"] == DBNull.Value) ? null : (string)row["TransfersName"]
                       );
                    list.Add(item);
                }
            }

            return list;
        }

        public List<MedicalCountReportEntity> SP_GetReportList_MedicalCount(MedicalCountReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("FirstDate");
            ArrayParams.Add("SecondDate");

            ArrayValues.Add(entity.FirstDate);
            ArrayValues.Add(entity.SecondDate);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_MedicalCount", ArrayValues, ArrayParams);


            List<MedicalCountReportEntity> list = new List<MedicalCountReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    MedicalCountReportEntity item = new MedicalCountReportEntity(
                            FilterDate: (row["FilterDate"] == DBNull.Value) ? null : (DateTime?)row["FilterDate"],
                            TotalInvalids: (row["TotalInvalids"] == DBNull.Value) ? null : (int?)row["TotalInvalids"],
                            FirstClass: (row["FirstClass"] == DBNull.Value) ? null : (int?)row["FirstClass"],
                            SecondClass: (row["SecondClass"] == DBNull.Value) ? null : (int?)row["SecondClass"],
                            ThirdClass: (row["ThirdClass"] == DBNull.Value) ? null : (int?)row["ThirdClass"],
                            Died: (row["Died"] == DBNull.Value) ? null : (int?)row["Died"],
                            Disease: (row["Disease"] == DBNull.Value) ? null : (int?)row["Disease"],
                            Ambulance: (row["Ambulance"] == DBNull.Value) ? null : (int?)row["Ambulance"],
                            Static: (row["Static"] == DBNull.Value) ? null : (int?)row["Static"],
                            Hospital: (row["Hospital"] == DBNull.Value) ? null : (int?)row["Hospital"],
                            Hurry: (row["Hurry"] == DBNull.Value) ? null : (int?)row["Hurry"],
                            DiedInHospital: (row["DiedInHospital"] == DBNull.Value) ? null : (int?)row["DiedInHospital"],
                            Lab: (row["Lab"] == DBNull.Value) ? null : (int?)row["Lab"]
                       );
                    list.Add(item);
                }
            }

            return list;
        }

        public List<CountPrisonersByOrgReportEntity> SP_GetReportList_PrisonersCountByOrgUnit(CountPrisonersByOrgReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("FirstDate");
            ArrayParams.Add("SecondDate");
            ArrayParams.Add("CurrentDate");

            ArrayValues.Add(entity.FirstDate);
            ArrayValues.Add(entity.SecondDate);
            ArrayValues.Add(entity.CurrentDate);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_PrisonersCountByOrgUnit", ArrayValues, ArrayParams);


            List<CountPrisonersByOrgReportEntity> list = new List<CountPrisonersByOrgReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    CountPrisonersByOrgReportEntity item = new CountPrisonersByOrgReportEntity(
                            OrgName: (row["OrgName"] == DBNull.Value) ? null : (string)row["OrgName"],
                            AllPrisoners: (row["AllPrisoners"] == DBNull.Value) ? null : (int?)row["AllPrisoners"],
                            CountPrisoners: (row["CountPrisoners"] == DBNull.Value) ? null : (int?)row["CountPrisoners"],
                            FirstAllPrisoners: (row["FirstAllPrisoners"] == DBNull.Value) ? null : (int?)row["FirstAllPrisoners"],
                            FirstCountPrisoners: (row["FirstCountPrisoners"] == DBNull.Value) ? null : (int?)row["FirstCountPrisoners"],
                            SecondAllPrisoners: (row["SecondAllPrisoners"] == DBNull.Value) ? null : (int?)row["SecondAllPrisoners"],
                            SecondCountPrisoners: (row["SecondCountPrisoners"] == DBNull.Value) ? null : (int?)row["SecondCountPrisoners"],
                            CurrentDate: entity.CurrentDate,
                            FirstDate: entity.FirstDate,
                            SecondDate: entity.SecondDate
                       );
                    list.Add(item);
                }
            }

            return list;
        }

        public List<CountPrisonersByOrgTypeEntity> SP_GetReportList_PrisonersCountByType(CountPrisonersByOrgTypeEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("FilterDate");
            ArrayParams.Add("FirstDate");

            ArrayValues.Add(entity.CurrentDate);
            ArrayValues.Add(entity.FirstDate);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_PrisonersCountByType", ArrayValues, ArrayParams);


            List<CountPrisonersByOrgTypeEntity> list = new List<CountPrisonersByOrgTypeEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    CountPrisonersByOrgTypeEntity item = new CountPrisonersByOrgTypeEntity(
                            CloseType: (row["CloseType"] == DBNull.Value) ? null : (int?)row["CloseType"],
                            HalfCloseType: (row["HalfCloseType"] == DBNull.Value) ? null : (int?)row["HalfCloseType"],
                            HalfOpenType: (row["HalfOpenType"] == DBNull.Value) ? null : (int?)row["HalfOpenType"],
                            OpenType: (row["OpenType"] == DBNull.Value) ? null : (int?)row["OpenType"],
                            Female: (row["Female"] == DBNull.Value) ? null : (int?)row["Female"],
                            Nationality: (row["Nationality"] == DBNull.Value) ? null : (int?)row["Nationality"],
                            Underage: (row["Underage"] == DBNull.Value) ? null : (int?)row["Underage"],
                            NotBig: (row["NotBig"] == DBNull.Value) ? null : (int?)row["NotBig"],
                            Mid: (row["Mid"] == DBNull.Value) ? null : (int?)row["Mid"],
                            Heavy: (row["Heavy"] == DBNull.Value) ? null : (int?)row["Heavy"],
                            Grave: (row["Grave"] == DBNull.Value) ? null : (int?)row["Grave"],
                            CurrentDate: entity.CurrentDate,
                            FirstDate: entity.FirstDate
                       );
                    list.Add(item);
                }
            }

            return list;
        }

        public List<CountPrisonersByCaseCloseEntity> SP_GetReportList_CloseCountPrisoners(CountPrisonersByCaseCloseEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("FilterDate");
            ArrayParams.Add("FirstDate");

            ArrayValues.Add(entity.CurrentDate);
            ArrayValues.Add(entity.FirstDate);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_CloseCountPrisoners", ArrayValues, ArrayParams);


            List<CountPrisonersByCaseCloseEntity> list = new List<CountPrisonersByCaseCloseEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    CountPrisonersByCaseCloseEntity item = new CountPrisonersByCaseCloseEntity(
                            GetRid: (row["GetRid"] == DBNull.Value) ? null : (int?)row["GetRid"],
                            Full: (row["Full"] == DBNull.Value) ? null : (int?)row["Full"],
                            Early: (row["Early"] == DBNull.Value) ? null : (int?)row["Early"],
                            Amnesty: (row["Amnesty"] == DBNull.Value) ? null : (int?)row["Amnesty"],
                            ԼetՕff: (row["ԼetՕff"] == DBNull.Value) ? null : (int?)row["ԼetՕff"],
                            Sick: (row["Sick"] == DBNull.Value) ? null : (int?)row["Sick"],
                            Died: (row["Died"] == DBNull.Value) ? null : (int?)row["Died"],
                            EndPrisonerDate: (row["EndPrisonerDate"] == DBNull.Value) ? null : (int?)row["EndPrisonerDate"],
                            Curt: (row["Curt"] == DBNull.Value) ? null : (int?)row["Curt"],
                            Changed: (row["Changed"] == DBNull.Value) ? null : (int?)row["Changed"],
                            ChiefDecision: (row["ChiefDecision"] == DBNull.Value) ? null : (int?)row["ChiefDecision"],
                            CurrentDate: entity.CurrentDate,
                            FirstDate: entity.FirstDate
                       );
                    list.Add(item);
                }
            }

            return list;
        }

        public List<PrisonerConvictSemesterReportEntity> SP_GetReportList_CountList(PrisonerConvictSemesterReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("FirstDate");
            ArrayParams.Add("SecondDate");
            ArrayParams.Add("OrgUnitID");

            ArrayValues.Add(entity.FirstDate);
            ArrayValues.Add(entity.SecondDate);
            ArrayValues.Add(75);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_CountList", ArrayValues, ArrayParams);


            List<PrisonerConvictSemesterReportEntity> list = new List<PrisonerConvictSemesterReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    PrisonerConvictSemesterReportEntity item = new PrisonerConvictSemesterReportEntity(
                            FirstDate: DateTime.Today,
                            SecondDate: DateTime.Now,
                            OrgUnitName: (row["OrgUnitName"] == DBNull.Value) ? null : (string)row["OrgUnitName"],
                            AllPrisoners: (row["AllPrisoners"] == DBNull.Value) ? null : (int?)row["AllPrisoners"],
                            Prisoner: (row["Prisoner"] == DBNull.Value) ? null : (int?)row["Prisoner"],
                            WaithingCurt: (row["WaithingCurt"] == DBNull.Value) ? null : (int?)row["WaithingCurt"],
                            Convict: (row["Convict"] == DBNull.Value) ? null : (int?)row["Convict"],
                            UnderInvestigation: (row["UnderInvestigation"] == DBNull.Value) ? null : (int?)row["UnderInvestigation"],
                            PreviousConvictions: (row["PreviousConviction"] == DBNull.Value) ? null : (int?)row["PreviousConviction"],
                            DontHavePrevConv: (row["DontHavePrevConv"] == DBNull.Value) ? null : (int?)row["DontHavePrevConv"],
                            SubToTransfer: (row["SubToTransfer"] == DBNull.Value) ? null : (int?)row["SubToTransfer"],
                            CloseType: (row["CloseType"] == DBNull.Value) ? null : (int?)row["CloseType"],
                            HalfCloseType: (row["HalfCloseType"] == DBNull.Value) ? null : (int?)row["HalfCloseType"],
                            HalfOpenType: (row["HalfOpenType"] == DBNull.Value) ? null : (int?)row["HalfOpenType"],
                            OpenType: (row["OpenType"] == DBNull.Value) ? null : (int?)row["OpenType"],
                            NotBig: (row["NotBig"] == DBNull.Value) ? null : (int?)row["NotBig"],
                            Mid: (row["Mid"] == DBNull.Value) ? null : (int?)row["Mid"],
                            Heavy: (row["Heavy"] == DBNull.Value) ? null : (int?)row["Heavy"],
                            Grave: (row["Grave"] == DBNull.Value) ? null : (int?)row["Grave"],
                            OtherNation: (row["OtherNation"] == DBNull.Value) ? null : (int?)row["OtherNation"],
                            ALLFilterPrisoners: (row["ALLFilterPrisoners"] == DBNull.Value) ? null : (int?)row["ALLFilterPrisoners"],
                            DuringFilterPrisoner: (row["DuringFilterPrisoner"] == DBNull.Value) ? null : (int?)row["DuringFilterPrisoner"],
                            DuringFilterConvict: (row["DuringFilterConvict"] == DBNull.Value) ? null : (int?)row["DuringFilterConvict"],
                            FPreviousConvictions: (row["FPreviousConvictions"] == DBNull.Value) ? null : (int?)row["FPreviousConvictions"],
                            FDontHavePrevConv: (row["FDontHavePrevConv"] == DBNull.Value) ? null : (int?)row["FDontHavePrevConv"],
                            TransferOtherOrg: (row["TransferOtherOrg"] == DBNull.Value) ? null : (int?)row["TransferOtherOrg"],
                            MentalHospital: (row["MentalHospital"] == DBNull.Value) ? null : (int?)row["MentalHospital"],
                            ALLGetRid: (row["GetRidOf"] == DBNull.Value) ? null : (int?)row["GetRidOf"],
                            FullPenalty: (row["FullPenalty"] == DBNull.Value) ? null : (int?)row["FullPenalty"],
                            EarlyClose: (row["EarlyClose"] == DBNull.Value) ? null : (int?)row["EarlyClose"],
                            Amnesty: (row["Amnesty"] == DBNull.Value) ? null : (int?)row["Amnesty"],
                            LetOff: (row["LetOff"] == DBNull.Value) ? null : (int?)row["LetOff"],
                            Sick: (row["Sick"] == DBNull.Value) ? null : (int?)row["Sick"],
                            Curt: (row["Curt"] == DBNull.Value) ? null : (int?)row["Curt"],
                            CurtOther: (row["CurtOther"] == DBNull.Value) ? null : (int?)row["CurtOther"],
                            CurtPenalty: (row["CurtPenalty"] == DBNull.Value) ? null : (int?)row["CurtPenalty"],
                            CurtWork: (row["CurtWork"] == DBNull.Value) ? null : (int?)row["CurtWork"],
                            CurtProbation: (row["CurtProbation"] == DBNull.Value) ? null : (int?)row["CurtProbation"],
                            CurtAmnesty: (row["CurtAmnesty"] == DBNull.Value) ? null : (int?)row["CurtAmnesty"],
                            ChangedORClosed: (row["ChangedORClosed"] == DBNull.Value) ? null : (int?)row["ChangedORClosed"],
                            ChiefDecision: (row["ChiefDecision"] == DBNull.Value) ? null : (int?)row["ChiefDecision"],
                            OtherCaseCose: (row["OtherCaseCose"] == DBNull.Value) ? null : (int?)row["OtherCaseCose"],
                            Escape: (row["Escape"] == DBNull.Value) ? null : (int?)row["Escape"],
                            Died: (row["Died"] == DBNull.Value) ? null : (int?)row["Died"],
                            Under1: (row["Under1"] == DBNull.Value) ? null : (int?)row["Under1"],
                            Under1To3: (row["Under1To3"] == DBNull.Value) ? null : (int?)row["Under1To3"],
                            Under3To5: (row["Under3To5"] == DBNull.Value) ? null : (int?)row["Under3To5"],
                            Under5To10: (row["Under5To10"] == DBNull.Value) ? null : (int?)row["Under5To10"],
                            Under10To15: (row["Under10To15"] == DBNull.Value) ? null : (int?)row["Under10To15"],
                            MoreThen15: (row["MoreThen15"] == DBNull.Value) ? null : (int?)row["MoreThen15"],
                            Underage: (row["Underage"] == DBNull.Value) ? null : (int?)row["Underage"],
                            Age18To25: (row["Age18To25"] == DBNull.Value) ? null : (int?)row["Age18To25"],
                            Age25To35: (row["Age25To35"] == DBNull.Value) ? null : (int?)row["Age25To35"],
                            Age35To45: (row["Age35To45"] == DBNull.Value) ? null : (int?)row["Age35To45"],
                            Age45To60: (row["Age45To60"] == DBNull.Value) ? null : (int?)row["Age45To60"],
                            AgeMore60: (row["AgeMore60"] == DBNull.Value) ? null : (int?)row["AgeMore60"]
                       );
                    list.Add(item);
                }
            }

            return list;
        }

        public List<ArticlePercentReportEntity> SP_GetReportList_ArticlePercent(ArticlePercentReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            
            ArrayParams.Add("Date");
            ArrayValues.Add(entity.Date);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_ArticlePercent", ArrayValues, ArrayParams);

            List<ArticlePercentReportEntity> list = new List<ArticlePercentReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    ArticlePercentReportEntity item = new ArticlePercentReportEntity(
                            Date: entity.Date,
                            LibItemID: (row["LibItem_ID"] == DBNull.Value) ? null : (int?)row["LibItem_ID"],
                            PrisonersCount: (row["PrisonersCount"] == DBNull.Value) ? null : (int?)row["PrisonersCount"],
                            Percent: (row["Percent"] == DBNull.Value) ? null : (decimal?)row["Percent"]
                        );

                    list.Add(item);
                }
            }

            return list;
        }
        public List<PrisonersReferendumReportEntity> SP_GetReportList_PrisonersReferendum(PrisonersReferendumReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("Date");
            ArrayParams.Add("OrgUnitList");

            ArrayValues.Add(entity.Date);
            ArrayValues.Add(String.Join(",", entity.OrgUnitList));

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_PrisonersReferendum", ArrayValues, ArrayParams);

            List<PrisonersReferendumReportEntity> list = new List<PrisonersReferendumReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    PrisonersReferendumReportEntity item = new PrisonersReferendumReportEntity(
                            Date: entity.Date,
                            PrisonerName: (row["PrisonerName"] == DBNull.Value) ? null : (string)row["PrisonerName"],
                            BirthDay: (row["BirthDay"] == DBNull.Value) ? null : (DateTime?)row["BirthDay"],
                            Address: (row["Address"] == DBNull.Value) ? null : (string)row["Address"],
                            IdentificationDocument: (row["identificationDocument"] == DBNull.Value) ? null : (string)row["IdentificationDocument"]
                        );

                    list.Add(item);
                }
            }

            return list;
        }

        public List<OrgTypeChangedReportEntity> SP_GetReportList_OrgTypeChanged(OrgTypeChangedReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("FirstDate");
            ArrayParams.Add("SecondDate");

            ArrayValues.Add(entity.FirstDate);
            ArrayValues.Add(entity.SecondDate);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_OrgTypeChanged", ArrayValues, ArrayParams);

            List<OrgTypeChangedReportEntity> list = new List<OrgTypeChangedReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    OrgTypeChangedReportEntity item = new OrgTypeChangedReportEntity(
                            FirstDate: entity.FirstDate,
                            SecondDate: entity.SecondDate,
                            OpenToHalfOpenOrgHigh: (row["OpenToHalfOpenOrgHigh"] == DBNull.Value) ? null : (int?)row["OpenToHalfOpenOrgHigh"],
                            OpenToHalfCloseOrgHigh: (row["OpenToHalfCloseOrgHigh"] == DBNull.Value) ? null : (int?)row["OpenToHalfCloseOrgHigh"],
                            OpenToCloseOrgHigh: (row["OpenToCloseOrgHigh"] == DBNull.Value) ? null : (int?)row["OpenToCloseOrgHigh"],
                            HalfOpenToHalfCloseOrgHigh: (row["HalfOpenToHalfCloseOrgHigh"] == DBNull.Value) ? null : (int?)row["HalfOpenToHalfCloseOrgHigh"],
                            HalfOpenToCloseOrgHigh: (row["HalfOpenToCloseOrgHigh"] == DBNull.Value) ? null : (int?)row["HalfOpenToCloseOrgHigh"],
                            HalfCloseToCloseOrgHigh: (row["HalfOpenToHalfCloseOrgHigh"] == DBNull.Value) ? null : (int?)row["HalfOpenToHalfCloseOrgHigh"],
                            CloseToHalfCloseOrgLow: (row["CloseToHalfCloseOrgLow"] == DBNull.Value) ? null : (int?)row["CloseToHalfCloseOrgLow"],
                            HalfCloseToHalfOpenOrgLow: (row["HalfCloseToHalfOpenOrgLow"] == DBNull.Value) ? null : (int?)row["HalfCloseToHalfOpenOrgLow"],
                            HalfOpenToOpenOrgLow: (row["HalfOpenToOpenOrgLow"] == DBNull.Value) ? null : (int?)row["HalfOpenToOpenOrgLow"],
                            OpenToHalfOpenOwnHigh: (row["OpenToHalfOpenOwnHigh"] == DBNull.Value) ? null : (int?)row["OpenToHalfOpenOwnHigh"],
                            OpenToHalfCloseOwnHigh: (row["OpenToHalfCloseOwnHigh"] == DBNull.Value) ? null : (int?)row["OpenToHalfCloseOwnHigh"],
                            OpenToCloseOwnHigh: (row["OpenToCloseOwnHigh"] == DBNull.Value) ? null : (int?)row["OpenToCloseOwnHigh"],
                            HalfOpenToHalfCloseOwnHigh: (row["HalfOpenToHalfCloseOwnHigh"] == DBNull.Value) ? null : (int?)row["HalfOpenToHalfCloseOwnHigh"],
                            HalfOpenToCloseOwnHigh: (row["HalfOpenToCloseOwnHigh"] == DBNull.Value) ? null : (int?)row["HalfOpenToCloseOwnHigh"],
                            HalfCloseToCloseOwnHigh: (row["HalfCloseToCloseOwnHigh"] == DBNull.Value) ? null : (int?)row["HalfCloseToCloseOwnHigh"],
                            CloseToHalfCloseOwnPrev: (row["CloseToHalfCloseOwnPrev"] == DBNull.Value) ? null : (int?)row["CloseToHalfCloseOwnPrev"],
                            CloseToHalfOpenOwnPrev: (row["CloseToHalfOpenOwnPrev"] == DBNull.Value) ? null : (int?)row["CloseToHalfOpenOwnPrev"],
                            CloseToOpenOwnPrev: (row["CloseToOpenOwnPrev"] == DBNull.Value) ? null : (int?)row["CloseToOpenOwnPrev"],
                            HalfCloseToHalfOpenOwnPrev: (row["HalfCloseToHalfOpenOwnPrev"] == DBNull.Value) ? null : (int?)row["HalfCloseToHalfOpenOwnPrev"],
                            HalfCloseToOpenOwnPrev: (row["HalfCloseToOpenOwnPrev"] == DBNull.Value) ? null : (int?)row["HalfCloseToOpenOwnPrev"],
                            HalfOpenToOpenOwnPrev: (row["HalfOpenToOpenOwnPrev"] == DBNull.Value) ? null : (int?)row["HalfOpenToOpenOwnPrev"]
                        );

                    list.Add(item);
                }
            }

            return list;
        }

        public List<PackageReportEntity> SP_GetReportList_Package(PackageReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("ID");

            ArrayValues.Add(entity.ID);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_Package", ArrayValues, ArrayParams);

            List<PackageReportEntity> list = new List<PackageReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    PackageReportEntity item = new PackageReportEntity(

                            ID: (row["ID"] == DBNull.Value) ? null : (int?)row["ID"],
                            OrgUnitLabel: (row["OrgUnitLabel"] == DBNull.Value) ? null : (string)row["OrgUnitLabel"],
                            PrisonerName: (row["PrisonerName"] == DBNull.Value) ? null : (string)row["PrisonerName"],
                            Birthday: (row["Birthday"] == DBNull.Value) ? null : (DateTime?)row["Birthday"],
                            StartDate: (row["StartDate"] == DBNull.Value) ? null : (DateTime?)row["StartDate"],
                            ArrestCodeLibItemLabel: (row["ArrestCodeLibItemLabel"] == DBNull.Value) ? null : (string)row["ArrestCodeLibItemLabel"],
                            ArrestDataID: (row["ArrestDataID"] == DBNull.Value) ? null : (int?)row["ArrestDataID"],
                            SentCodeLibItemLabel: (row["SentCodeLibItemLabel"] == DBNull.Value) ? null : (string)row["SentCodeLibItemLabel"],
                            SentencingDataID: (row["SentencingDataID"] == DBNull.Value) ? null : (int?)row["SentencingDataID"],
                            Sentence: (row["Sentence"] == DBNull.Value) ? null : (string)row["Sentence"],
                            DetentionStart: (row["DetentionStart"] == DBNull.Value) ? null : (DateTime?)row["DetentionStart"],
                            DeliveryDay: (row["DeliveryDay"] == DBNull.Value) ? null : (DateTime?)row["DeliveryDay"],
                            PackagePerson: (row["PackagePerson"] == DBNull.Value) ? null : (string)row["PackagePerson"],
                            PackagesType: (row["PackagesType"] == DBNull.Value) ? null : (string)row["PackagesType"],
                            Weight: (row["Weight"] == DBNull.Value) ? null : (int?)row["Weight"],
                            ArrestCodeLibItemID: (row["ArrestCodeLibItemID"] == DBNull.Value) ? null : (int?)row["ArrestCodeLibItemID"],
                            SentencCodeLibItemID: (row["SentencCodeLibItemID"] == DBNull.Value) ? null : (int?)row["SentencCodeLibItemID"],
                            ApproveEmployee: (row["ApproveEmployee"] == DBNull.Value) ? null : (string)row["ApproveEmployee"],
                            EmployeeName: (row["EmployeeName"] == DBNull.Value) ? null : (string)row["EmployeeName"]
                       );
                    list.Add(item);
                }
            }

            return list;
        }

        public List<PackagesContentReportEntity> SP_GetReportList_PackageContent(PackagesContentReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("ID");

            ArrayValues.Add(entity.ID);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_PackageContent", ArrayValues, ArrayParams);

            List<PackagesContentReportEntity> list = new List<PackagesContentReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    PackagesContentReportEntity item = new PackagesContentReportEntity(
                        
                            TypeName: (row["TypeName"] == DBNull.Value) ? null : (string)row["TypeName"],
                            GoodsName: (row["GoodsName"] == DBNull.Value) ? null : (string)row["GoodsName"],
                            MeasureName: (row["MEasureName"] == DBNull.Value) ? null : (string)row["MEasureName"],
                            WightCount: (row["WightCount"] == DBNull.Value) ? null : (int?)row["WightCount"]
                       );
                    list.Add(item);
                }
            }

            return list;
        }

        public List<PersonalVisitsReportEntity> SP_GetReportList_PersonalVisits(PersonalVisitsReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("ID");

            ArrayValues.Add(entity.ID);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_PersonalVisits", ArrayValues, ArrayParams);

            List<PersonalVisitsReportEntity> list = new List<PersonalVisitsReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    PersonalVisitsReportEntity item = new PersonalVisitsReportEntity(

                            ID: (row["ID"] == DBNull.Value) ? null : (int?)row["ID"],
                            OrgUnitLabel: (row["OrgUnitLabel"] == DBNull.Value) ? null : (string)row["OrgUnitLabel"],
                            PrisonerName: (row["PrisonerName"] == DBNull.Value) ? null : (string)row["PrisonerName"],
                            Birthday: (row["Birthday"] == DBNull.Value) ? null : (DateTime?)row["Birthday"],
                            StartDate: (row["StartDate"] == DBNull.Value) ? null : (DateTime?)row["StartDate"],
                            ArrestCodeLibItemLabel: (row["ArrestCodeLibItemLabel"] == DBNull.Value) ? null : (string)row["ArrestCodeLibItemLabel"],
                            ArrestDataID: (row["ArrestDataID"] == DBNull.Value) ? null : (int?)row["ArrestDataID"],
                            SentCodeLibItemLabel: (row["SentCodeLibItemLabel"] == DBNull.Value) ? null : (string)row["SentCodeLibItemLabel"],
                            SentencingDataID: (row["SentencingDataID"] == DBNull.Value) ? null : (int?)row["SentencingDataID"],
                            Sentence: (row["Sentence"] == DBNull.Value) ? null : (string)row["Sentence"],
                            DetentionStart: (row["DetentionStart"] == DBNull.Value) ? null : (DateTime?)row["DetentionStart"],
                            VisitStartDate: (row["VisitStartDate"] == DBNull.Value) ? null : (DateTime?)row["VisitStartDate"],
                            VisitPerson: (row["VisitPerson"] == DBNull.Value) ? null : (string)row["VisitPerson"],
                            VisitType: (row["VisitType"] == DBNull.Value) ? null : (string)row["VisitType"],
                            ArrestCodeLibItemID: (row["ArrestCodeLibItemID"] == DBNull.Value) ? null : (int?)row["ArrestCodeLibItemID"],
                            SentencCodeLibItemID: (row["SentencCodeLibItemID"] == DBNull.Value) ? null : (int?)row["SentencCodeLibItemID"],
                            ApproverEmployee: (row["ApproverEmployee"] == DBNull.Value) ? null : (string)row["ApproverEmployee"]
                       );
                    list.Add(item);
                }
            }

            return list;
        }
        public List<OfficialVisitReportEntity> SP_GetReportList_OfficialVisit(OfficialVisitReportEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("ID");

            ArrayValues.Add(entity.ID);

            DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetReportList_OfficialVisit", ArrayValues, ArrayParams);

            List<OfficialVisitReportEntity> list = new List<OfficialVisitReportEntity>();
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    OfficialVisitReportEntity item = new OfficialVisitReportEntity(

                            ID: (row["ID"] == DBNull.Value) ? null : (int?)row["ID"],
                            OrgUnitLabel: (row["OrgUnitLabel"] == DBNull.Value) ? null : (string)row["OrgUnitLabel"],
                            PrisonerName: (row["PrisonerName"] == DBNull.Value) ? null : (string)row["PrisonerName"],
                            Birthday: (row["Birthday"] == DBNull.Value) ? null : (DateTime?)row["Birthday"],
                            StartDate: (row["StartDate"] == DBNull.Value) ? null : (DateTime?)row["StartDate"],
                            ArrestCodeLibItemLabel: (row["ArrestCodeLibItemLabel"] == DBNull.Value) ? null : (string)row["ArrestCodeLibItemLabel"],
                            ArrestDataID: (row["ArrestDataID"] == DBNull.Value) ? null : (int?)row["ArrestDataID"],
                            SentCodeLibItemLabel: (row["SentCodeLibItemLabel"] == DBNull.Value) ? null : (string)row["SentCodeLibItemLabel"],
                            SentencingDataID: (row["SentencingDataID"] == DBNull.Value) ? null : (int?)row["SentencingDataID"],
                            Sentence: (row["Sentence"] == DBNull.Value) ? null : (string)row["Sentence"],
                            DetentionStart: (row["DetentionStart"] == DBNull.Value) ? null : (DateTime?)row["DetentionStart"],
                            VisitPerson: (row["VisitPerson"] == DBNull.Value) ? null : (string)row["VisitPerson"],
                            ArrestCodeLibItemID: (row["ArrestCodeLibItemID"] == DBNull.Value) ? null : (int?)row["ArrestCodeLibItemID"],
                            SentencCodeLibItemID: (row["SentencCodeLibItemID"] == DBNull.Value) ? null : (int?)row["SentencCodeLibItemID"],
                            LivingAddress: (row["LivingAddress"] == DBNull.Value) ? null : (string)row["LivingAddress"]
                       );
                    list.Add(item);
                }
            }

            return list;
        }
    }

}