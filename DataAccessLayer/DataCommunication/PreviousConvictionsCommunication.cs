﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PreviousConvictionsService:DataComm
    {
        public int? SP_Add_PreviousConvictions(PreviousConvictionsEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("CodeLibItemID");
            ArrayParams.Add("PenaltyLibItemID");
            ArrayParams.Add("PenaltyDay");
            ArrayParams.Add("PenaltyMonth");
            ArrayParams.Add("PenaltyYear");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("Notes");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.CodeLibItemID);
            ArrayValues.Add(entity.PenaltyLibItemID);
            ArrayValues.Add(entity.PenaltyDay);
            ArrayValues.Add(entity.PenaltyMonth);
            ArrayValues.Add(entity.PenaltyYear);
            ArrayValues.Add(entity.StartDate);
            ArrayValues.Add(entity.EndDate);
            ArrayValues.Add(entity.Notes);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PreviousConvictions", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public bool? SP_Update_PreviousConvictions(PreviousConvictionsEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("ID");
            ArrayParams.Add("CodeLibItemID");
            ArrayParams.Add("PenaltyLibItemID");
            ArrayParams.Add("PenaltyDay");
            ArrayParams.Add("PenaltyMonth");
            ArrayParams.Add("PenaltyYear");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("Notes");
            ArrayParams.Add("Status");

            ArrayValues.Add(entity.ID);
            ArrayValues.Add(entity.CodeLibItemID);
            ArrayValues.Add(entity.PenaltyLibItemID);
            ArrayValues.Add(entity.PenaltyDay);
            ArrayValues.Add(entity.PenaltyMonth);
            ArrayValues.Add(entity.PenaltyYear);
            ArrayValues.Add(entity.StartDate);
            ArrayValues.Add(entity.EndDate);
            ArrayValues.Add(entity.Notes);
            ArrayValues.Add(entity.Status);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_PreviousConvictions", ArrayValues, ArrayParams);

                if (dt == null) return false;

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public List<PreviousConvictionsEntity> SP_GetList_PreviousConvictions(PreviousConvictionsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();
                
                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PreviousConvictions", ArrayValues, ArrayParams);

                List<PreviousConvictionsEntity> list = new List<PreviousConvictionsEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PreviousConvictionsEntity item = new PreviousConvictionsEntity(
                                ID: (int)row["ID"],
                                PrisonerID:(int)row["PrisonerID"],
                                CodeLibItemID: (int)row["CodeLibItemID"],
                                CodeLibItemName: (string)row["CodeLibItemName"],
                                PenaltyLibItemID:(row["PenaltyLibItemID"] == DBNull.Value) ? null : (int?)row["PenaltyLibItemID"],
                                PenaltyDay:(row["PenaltyDay"] == DBNull.Value) ? null : (int?)row["PenaltyDay"],
                                PenaltyMonth: (row["PenaltyMonth"] == DBNull.Value) ? null : (int?)row["PenaltyMonth"],
                                PenaltyYear: (row["PenaltyYear"] == DBNull.Value) ? null : (int?)row["PenaltyYear"],
                                PenaltyLibItemName:(row["PenaltyLibItemName"] == DBNull.Value) ? null : (string)row["PenaltyLibItemName"],
                                StartDate: (row["StartDate"] == DBNull.Value) ? null : (DateTime?)row["StartDate"],
                                EndDate: (row["EndDate"] == DBNull.Value) ? null : (DateTime?)row["EndDate"],
                                Notes: (row["Notes"] == DBNull.Value) ? null :(string)row["Notes"],
                                MergeStatus: (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PreviousConvictionsEntity>();
            }
        }
    }
}
