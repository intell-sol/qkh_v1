﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_ChildrenService:DataComm
    {

        public int? SP_Add_Children(ChildrenEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PersonID");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("TypeID");
            ArrayParams.Add("Certificate");


            ArrayValues.Add(entity.PersonID);
            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add((int)entity.ChildrenType);
            ArrayValues.Add(entity.Certificate);


            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Children", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<ChildrenEntity> SP_GetList_Children(ChildrenEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Children", ArrayValues, ArrayParams);

                List<ChildrenEntity> list = new List<ChildrenEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        ChildrenEntity item = new ChildrenEntity(
                                ID:(int)row["ID"],
                                PersonID:(int)row["PersonID"],
                                PrisonerID:(int)row["PrisonerID"],
                                ChildrenType: (ChildrenType)((int)row["TypeID"]),
                                Certificate:(row["Certificate"] == DBNull.Value) ? null : (string)row["Certificate"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }



                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<ChildrenEntity>();
            }
        }
        public bool SP_Update_Children(ChildrenEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("TypeID");
                ArrayParams.Add("Certificate");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add((int?)entity.ChildrenType);
                ArrayValues.Add(entity.Certificate);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Children", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }


    }
}
