﻿using CommonLayer.HelperClasses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public abstract class DataComm
    {
        //public static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        protected DataUtility dtDataUtility = new DataUtility(CommonLayer.HelperClasses.ConfigurationHelper.GetDataBasePath());
        protected ArrayList ArrayParams = new ArrayList();
        protected ArrayList ArrayValues = new ArrayList();
    }
}