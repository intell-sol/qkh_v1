﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_ArrestDataService : DataComm
    {
        #region ArrestData
        public int? SP_Add_ArrestData(ArrestDataEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("OrgUnitID");
            //ArrayParams.Add("ProceedingLib_ID");
            ArrayParams.Add("ArrestLib_ID");
            ArrayParams.Add("VerdictNumber");
            ArrayParams.Add("Judge");
            ArrayParams.Add("VerdictDate");
            ArrayParams.Add("DetentionStart");
            ArrayParams.Add("DetentionEnd");
            ArrayParams.Add("CloseCaseID");
            ArrayParams.Add("OpenCaseID");
            ArrayParams.Add("CodeLibItem_ID");
            ArrayParams.Add("CodeNumber");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.OrgUnitID);
            //ArrayValues.Add(entity.ProceedingLib_ID);
            ArrayValues.Add(entity.ArrestLib_ID);
            ArrayValues.Add(entity.VerdictNumber);
            ArrayValues.Add(entity.Judge);
            ArrayValues.Add(entity.VerdictDate);
            ArrayValues.Add(entity.DetentionStart);
            ArrayValues.Add(entity.DetentionEnd);
            ArrayValues.Add(entity.CloseCaseID);
            ArrayValues.Add(entity.OpenCaseID);
            ArrayValues.Add(entity.CodeLibItem_ID);
            ArrayValues.Add(entity.CodeNumber);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_ArrestData", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<ArrestDataEntity> SP_GetList_ArrestData(ArrestDataEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_ArrestData", ArrayValues, ArrayParams);

                List<ArrestDataEntity> list = new List<ArrestDataEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        ArrestDataEntity item = new ArrestDataEntity(
                                ID:(int)row["ID"],
                                OrgUnitID:(int)row["OrgUnitID"],
                                PrisonerID:(int)row["PrisonerID"],
                                //ProceedingLib_ID:(int)row["ProceedingLib_ID"],
                                ArrestLib_ID:(int)row["ArrestLib_ID"],
                                VerdictNumber:(string)row["VerdictNumber"],
                                Judge:(row["Judge"] == DBNull.Value)?null:(string)row["Judge"],
                                VerdictDate:(DateTime)row["VerdictDate"],
                                DetentionStart:(DateTime)row["DetentionStart"],
                                DetentionEnd:(row["DetentionEnd"] == DBNull.Value) ? null : (DateTime?)row["DetentionEnd"],
                                CodeLibItem_ID:(row["CodeLibItem_ID"] == DBNull.Value) ? null : (int?)row["CodeLibItem_ID"],
                                Description: (row["Description"] == DBNull.Value) ? null :(string)row["Description"],
                                CloseCaseID: (row["CloseCaseID"] == DBNull.Value) ? null :(int?)row["CloseCaseID"],
                                OpenCaseID: (row["OpenCaseID"] == DBNull.Value) ? null :(int?)row["OpenCaseID"],
                                CodeNumber: (row["CodeNumber"] == DBNull.Value) ? null :(string)row["CodeNumber"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null :(bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<ArrestDataEntity>();
            }
        }
        public bool SP_Update_ArrestData(ArrestDataEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                //ArrayParams.Add("ProceedingLib_ID");
                ArrayParams.Add("ArrestLib_ID");
                ArrayParams.Add("VerdictNumber");
                ArrayParams.Add("Judge");
                ArrayParams.Add("VerdictDate");
                ArrayParams.Add("DetentionStart");
                ArrayParams.Add("DetentionEnd");
                ArrayParams.Add("Status");
                ArrayParams.Add("Description");
                ArrayParams.Add("CloseCaseID");
                ArrayParams.Add("OpenCaseID");
                ArrayParams.Add("CodeLibItem_ID");
                ArrayParams.Add("CodeNumber");

                ArrayValues.Add(entity.ID);
                //ArrayValues.Add(entity.ProceedingLib_ID);
                ArrayValues.Add(entity.ArrestLib_ID);
                ArrayValues.Add(entity.VerdictNumber);
                ArrayValues.Add(entity.Judge);
                ArrayValues.Add(entity.VerdictDate);
                ArrayValues.Add(entity.DetentionStart);
                ArrayValues.Add(entity.DetentionEnd);
                ArrayValues.Add(entity.Status);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.CloseCaseID);
                ArrayValues.Add(entity.OpenCaseID);
                ArrayValues.Add(entity.CodeLibItem_ID);
                ArrayValues.Add(entity.CodeNumber);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_ArrestData", ArrayValues, ArrayParams);

                
                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion

        #region ArrestDataProceedings
        public int? SP_Add_ArrestDataProceeding(ArrestDataProceedingEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("ArrestDataID");
            ArrayParams.Add("ProceedLibItemID");
            ArrayParams.Add("Date");

            ArrayValues.Add(entity.ArrestDataID);
            ArrayValues.Add(entity.ProceedLibItemID);
            ArrayValues.Add(entity.Date);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_ArrestDataProceedings", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<ArrestDataProceedingEntity> SP_GetList_ArrestDataProceedings(ArrestDataProceedingEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("ArrestDataID");
                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.ArrestDataID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_ArrestDataProceedings", ArrayValues, ArrayParams);

                List<ArrestDataProceedingEntity> list = new List<ArrestDataProceedingEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        ArrestDataProceedingEntity item = new ArrestDataProceedingEntity(
                                ID: (int)row["ID"],
                                ArrestDataID: (int)row["ArrestDataID"],
                                ProceedLibItemID: (int)row["ProceedLibItemID"],
                                ProceedLibItemLabel: (string)row["ProceedLibItemLabel"],
                                Date: (DateTime)row["Date"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<ArrestDataProceedingEntity>();
            }
        }
        public bool SP_Update_ArrestDataProceedings(ArrestDataProceedingEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("ArrestDataID");
                ArrayParams.Add("ProceedLibItemID");
                ArrayParams.Add("Date");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.ArrestDataID);
                ArrayValues.Add(entity.ProceedLibItemID);
                ArrayValues.Add(entity.Date);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_ArrestDataProceedings", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
