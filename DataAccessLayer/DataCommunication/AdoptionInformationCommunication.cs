﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_AdoptionService:DataComm
    {
        public int? SP_Add_AdoptionInformation(int PrisonerID, int ProceedingUnitLib_ID, int ArrestUnitLib_ID, int VerdictUnitLib_ID, string VerdictNumber, string Judge, DateTime Date, string Description, string ArrestRecordDescription, int CodeLib_ID, int CodeLibItem_ID, int CodeArticleLibID, int DetectiveRankLibID, string DetectiveFirstName, string DetectiveSurName, string DetectiveMiddleName, int PenitentiaryTypeLib_ID, DateTime ArrestDate, DateTime PrisonAdoptionDate, DateTime SentencingDate, string Basis, DateTime SentenceStartDate, int SentenceTypeLibID, string SentenceDuration, string PhysicalExamination, string SearchResult, string ItemsProtocol, string SocialPsychologicalCondition)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("ProceedingUnitLib_ID");
            ArrayParams.Add("ArrestUnitLib_ID");
            ArrayParams.Add("VerdictUnitLib_ID");
            ArrayParams.Add("VerdictNumber");
            ArrayParams.Add("Judge");
            ArrayParams.Add("Date");
            ArrayParams.Add("Description");
            ArrayParams.Add("ArrestRecordDescription");
            ArrayParams.Add("CodeLib_ID");
            ArrayParams.Add("CodeLibItem_ID");
            ArrayParams.Add("CodeArticleLibID");
            ArrayParams.Add("DetectiveRankLibID");
            ArrayParams.Add("DetectiveFirstName");
            ArrayParams.Add("DetectiveSurName");
            ArrayParams.Add("DetectiveMiddleName");
            ArrayParams.Add("PenitentiaryTypeLib_ID");
            ArrayParams.Add("ArrestDate");
            ArrayParams.Add("PrisonAdoptionDate");
            ArrayParams.Add("SentencingDate");
            ArrayParams.Add("Basis");
            ArrayParams.Add("SentenceStartDate");
            ArrayParams.Add("SentenceTypeLibID");
            ArrayParams.Add("SentenceDuration");
            ArrayParams.Add("PhysicalExamination");
            ArrayParams.Add("SearchResult");
            ArrayParams.Add("ItemsProtocol");
            ArrayParams.Add("SocialPsychologicalCondition");

            ArrayValues.Add(PrisonerID);
            ArrayValues.Add(ProceedingUnitLib_ID);
            ArrayValues.Add(ArrestUnitLib_ID);
            ArrayValues.Add(VerdictUnitLib_ID);
            ArrayValues.Add(VerdictNumber);
            ArrayValues.Add(Judge);
            ArrayValues.Add(Date);
            ArrayValues.Add(Description);
            ArrayValues.Add(ArrestRecordDescription);
            ArrayValues.Add(CodeLib_ID);
            ArrayValues.Add(CodeLibItem_ID);
            ArrayValues.Add(CodeArticleLibID);
            ArrayValues.Add(DetectiveRankLibID);
            ArrayValues.Add(DetectiveFirstName);
            ArrayValues.Add(DetectiveSurName);
            ArrayValues.Add(DetectiveMiddleName);
            ArrayValues.Add(PenitentiaryTypeLib_ID);
            ArrayValues.Add(ArrestDate);
            ArrayValues.Add(PrisonAdoptionDate);
            ArrayValues.Add(SentencingDate);
            ArrayValues.Add(Basis);
            ArrayValues.Add(SentenceStartDate);
            ArrayValues.Add(SentenceTypeLibID);
            ArrayValues.Add(SentenceDuration);
            ArrayValues.Add(PhysicalExamination);
            ArrayValues.Add(SearchResult);
            ArrayValues.Add(ItemsProtocol);
            ArrayValues.Add(SocialPsychologicalCondition);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_AdoptionInformation", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        
    }
}
