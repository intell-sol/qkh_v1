﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_WorkLoadsService : DataComm
    {
        public int? SP_Add_WorkLoad(WorkLoadsEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("JobTypeLibItemID");
            ArrayParams.Add("WorkTitleLibItemID");
            ArrayParams.Add("EmployerLibItemID");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("Note");
            ArrayParams.Add("ContractNumber");
            ArrayParams.Add("EngagementDate");
            ArrayParams.Add("EngagementBasisLibItemID");
            ArrayParams.Add("EngagementState");
            ArrayParams.Add("Salary");
            ArrayParams.Add("ReleaseBasisLibItemID");
            ArrayParams.Add("ReleaseDate");
            ArrayParams.Add("ReleaseNote");


            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.JobTypeLibItemID);
            ArrayValues.Add(entity.WorkTitleLibItemID);
            ArrayValues.Add(entity.EmployerLibItemID);
            ArrayValues.Add(entity.EndDate);
            ArrayValues.Add(entity.Note);
            ArrayValues.Add(entity.ContractNumber);
            ArrayValues.Add(entity.EngagementDate);
            ArrayValues.Add(entity.EngagementBasisLibItemID);
            ArrayValues.Add(entity.EngagementState);
            ArrayValues.Add(entity.Salary);
            ArrayValues.Add(entity.ReleaseBasisLibItemID);
            ArrayValues.Add(entity.ReleaseDate);
            ArrayValues.Add(entity.ReleaseNote);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_WorkLoads", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<WorkLoadsEntity> SP_GetList_WorkLoads(WorkLoadsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_WorkLoads", ArrayValues, ArrayParams);

                List<WorkLoadsEntity> list = new List<WorkLoadsEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        WorkLoadsEntity item = new WorkLoadsEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                WorkTitleLibItemID: (int)row["WorkTitleLibItemID"],
                                WorkTitleLibItemLabel: (string)row["WorkTitleLibItemLabel"],
                                JobTypeLibItemID: (int)row["JobTypeLibItemID"],
                                JobTypeLibItemLabel: (string)row["JobTypeLibItemLabel"],
                                EmployerLibItemID: (int)row["EmployerLibItemID"],
                                EmployerLibItemLabel: (string)row["EmployerLibItemLabel"],
                                EndDate: (DateTime)row["EndDate"],
                                Note: (row["Note"] != DBNull.Value) ? (string)row["Note"] : null,
                                Salary: (row["Salary"] != DBNull.Value) ? (int?)row["Salary"] : null,
                                ContractNumber: (row["ContractNumber"] != DBNull.Value) ? (string)row["ContractNumber"] : null,
                                EngagementDate: (row["EngagementDate"] != DBNull.Value) ? (DateTime?)row["EngagementDate"] : null,
                                EngagementBasisLibItemID: (row["EngagementBasisLibItemID"] != DBNull.Value) ? (int?)row["EngagementBasisLibItemID"] : null,
                                EngagementBasisLibItemLabel: (row["EngagementBasisLibItemLabel"] != DBNull.Value) ? (string)row["EngagementBasisLibItemLabel"] : null,
                                EngagementState: (row["EngagementState"] != DBNull.Value) ? (bool?)row["EngagementState"] : null,
                                ReleaseDate: (row["ReleaseDate"] != DBNull.Value) ? (DateTime?)row["ReleaseDate"] : null,
                                ReleaseBasisLibItemID: (row["ReleaseBasisLibItemID"] != DBNull.Value) ? (int?)row["ReleaseBasisLibItemID"] : null,
                                ReleaseBasisLibItemLabel: (row["ReleaseBasisLibItemLabel"] != DBNull.Value) ? (string)row["ReleaseBasisLibItemLabel"] : null,
                                ReleaseNote: (row["ReleaseNote"] != DBNull.Value) ? (string)row["ReleaseNote"] : null,
                                MergeStatus: (row["MergeStatus"] != DBNull.Value) ? (bool?)row["MergeStatus"] : null,
                                Status: (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<WorkLoadsEntity>();
            }
        }
        public bool SP_Update_WorkLoad(WorkLoadsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();


                ArrayParams.Add("ID");
                ArrayParams.Add("JobTypeLibItemID");
                ArrayParams.Add("WorkTitleLibItemID");
                ArrayParams.Add("EmployerLibItemID");
                ArrayParams.Add("EndDate");
                ArrayParams.Add("Note");
                ArrayParams.Add("ContractNumber");
                ArrayParams.Add("EngagementDate");
                ArrayParams.Add("EngagementBasisLibItemID");
                ArrayParams.Add("EngagementState");
                ArrayParams.Add("Salary");
                ArrayParams.Add("ReleaseBasisLibItemID");
                ArrayParams.Add("ReleaseDate");
                ArrayParams.Add("ReleaseNote");
                ArrayParams.Add("Status");


                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.JobTypeLibItemID);
                ArrayValues.Add(entity.WorkTitleLibItemID);
                ArrayValues.Add(entity.EmployerLibItemID);
                ArrayValues.Add(entity.EndDate);
                ArrayValues.Add(entity.Note);
                ArrayValues.Add(entity.ContractNumber);
                ArrayValues.Add(entity.EngagementDate);
                ArrayValues.Add(entity.EngagementBasisLibItemID);
                ArrayValues.Add(entity.EngagementState);
                ArrayValues.Add(entity.Salary);
                ArrayValues.Add(entity.ReleaseBasisLibItemID);
                ArrayValues.Add(entity.ReleaseDate);
                ArrayValues.Add(entity.ReleaseNote);
                ArrayValues.Add(entity.Status);


                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_WorkLoads", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public List<WorkLoadsDashboardEntity> SP_GetList_WorkLoadsForDashboard(FilterWorkLoadsEntity FilterData)
        {
            // UNDONE: to this first and move it to EncouragementsCommunicatin
            ArrayParams.Clear();
            ArrayValues.Clear();

            int TotalCount = 0;

            ArrayParams.Add("FirstName");
            ArrayParams.Add("MiddleName");
            ArrayParams.Add("LastName");
            ArrayParams.Add("JobTypeLibItemID");
            ArrayParams.Add("WorkTitleLibItemID");
            ArrayParams.Add("EmployerLibItemID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("EngagementBasisLibItemID");
            ArrayParams.Add("EngagementState");
            ArrayParams.Add("ReleaseBasisLibItemID");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("ArchiveStatus");
            ArrayParams.Add("PrisonerType");
            ArrayParams.Add("OrgUnitIDList");
            ArrayParams.Add("CurrentPage");
            ArrayParams.Add("ViewCount");

            ArrayValues.Add(FilterData.FirstName);
            ArrayValues.Add(FilterData.MiddleName);
            ArrayValues.Add(FilterData.LastName);
            ArrayValues.Add(FilterData.JobTypeLibItemID);
            ArrayValues.Add(FilterData.WorkTitleLibItemID);
            ArrayValues.Add(FilterData.EmployerLibItemID);
            ArrayValues.Add(FilterData.StartDate);
            ArrayValues.Add(FilterData.EndDate);
            ArrayValues.Add(FilterData.EngagementBasisLibItemID);
            ArrayValues.Add(FilterData.EngagementState);
            ArrayValues.Add(FilterData.ReleaseBasisLibItemID);
            ArrayValues.Add(FilterData.PrisonerID);
            ArrayValues.Add(FilterData.ArchiveStatus);
            ArrayValues.Add(FilterData.PrisonerType);
            ArrayValues.Add((FilterData.OrgUnitIDList != null && FilterData.OrgUnitIDList.Any()) ? string.Join(",", FilterData.OrgUnitIDList) : FilterData.OrgUnitID.ToString());
            ArrayValues.Add(FilterData.paging.CurrentPage);
            ArrayValues.Add(FilterData.paging.ViewCount);


            ArrayList OutParams = new ArrayList();
            Dictionary<string, object> OutValues = new Dictionary<string, object>();

            OutParams.Add("TotalCount");
            OutValues.Add("TotalCount", TotalCount);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_WorkLoadsForDashboard", ArrayValues, ArrayParams, ref OutValues, OutParams);

                List<WorkLoadsDashboardEntity> list = new List<WorkLoadsDashboardEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        WorkLoadsDashboardEntity item = new WorkLoadsDashboardEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                JobTypeLibItemLabel: (string)row["JobTypeLibItemLabel"],
                                WorkTitleLibItemLabel: (string)row["WorkTitleLibItemLabel"],
                                EmployerLibItemLabel: (string)row["EmployerLibItemLabel"],
                                EndDate: (row["EndDate"] == DBNull.Value) ? null : (DateTime?)row["EndDate"],
                                EngagementDate: (row["EngagementDate"] == DBNull.Value) ? null : (DateTime?)row["EngagementDate"],
                                Note: (string)row["Note"],
                                Status: (bool)row["Status"],
                                EngagementState: (row["EngagementState"] == DBNull.Value) ? null : (bool?)row["EngagementState"],
                                EngagementBasisLibItemLabel: (string)row["EngagementBasisLibItemLabel"],
                                ReleaseBasisLibItemLabel: (row["ReleaseBasisLibItemLabel"] == DBNull.Value) ? null : (string)row["ReleaseBasisLibItemLabel"],
                                ReleaseDate: (row["ReleaseDate"] == DBNull.Value) ? null : (DateTime?)row["ReleaseDate"],
                                Personal_ID: (string)row["Personal_ID"],
                                PrisonerName: (string)row["PrisonerName"],
                                ArchiveStatus: (bool)row["ArchiveStatus"],
                                PrisonerType: (int)row["PrisonerType"]
                            );

                        list.Add(item);
                    }
                }

                FilterData.paging.TotalCount = (int)OutValues["TotalCount"];
                FilterData.paging.TotalPage = (int)Math.Ceiling(Convert.ToDouble(OutValues["TotalCount"]) / FilterData.paging.ViewCount);
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                FilterData.paging.TotalPage = 0;
                return new List<WorkLoadsDashboardEntity>();
            }
        }
    }
}