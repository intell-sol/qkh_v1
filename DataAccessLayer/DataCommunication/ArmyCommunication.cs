﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_ArmyService:DataComm
    {
        public int? SP_Add_Army(ArmyEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("TypeLibItemID");
            ArrayParams.Add("ProffessionLibItemID");
            ArrayParams.Add("RankLibItemID");
            ArrayParams.Add("PositionLibItemID");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.StartDate);
            ArrayValues.Add(entity.EndDate);
            ArrayValues.Add(entity.TypeLibItemID);
            ArrayValues.Add(entity.ProffessionLibItemID);
            ArrayValues.Add(entity.RankLibItemID);
            ArrayValues.Add(entity.PositionLibItemID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Army", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public List<ArmyEntity> SP_GetList_Army(ArmyEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Army", ArrayValues, ArrayParams);

                List<ArmyEntity> list = new List<ArmyEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        ArmyEntity item = new ArmyEntity(
                        ID:(int)row["ID"],
                        PrisonerID:(int)row["PrisonerID"],
                        StartDate:(row["StartDate"] == DBNull.Value) ? null : (DateTime?)row["StartDate"],
                        EndDate:(row["EndDate"] == DBNull.Value) ? null : (DateTime?)row["EndDate"],
                        TypeLibItemID:(row["TypeLibItemID"] == DBNull.Value) ? null : (int?)row["TypeLibItemID"],
                        ProffessionLibItemID:(int)row["ProffessionLibItemID"],
                        RankLibItemID:(int)row["RankLibItemID"],
                        MergeStatus:(row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"],
                        PositionLibItemID: (row["PositionLibItemID"] == DBNull.Value) ? null : (int?)row["PositionLibItemID"]
                        
                        );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<ArmyEntity>();
            }
        }
        public bool SP_Update_Army(ArmyEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("StartDate");
                ArrayParams.Add("EndDate");
                ArrayParams.Add("TypeLibItemID");
                ArrayParams.Add("ProffessionLibItemID");
                ArrayParams.Add("RankLibItemID");
                ArrayParams.Add("PositionLibItemID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.StartDate);
                ArrayValues.Add(entity.EndDate);
                ArrayValues.Add(entity.TypeLibItemID);
                ArrayValues.Add(entity.ProffessionLibItemID);
                ArrayValues.Add(entity.RankLibItemID);
                ArrayValues.Add(entity.PositionLibItemID);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Army", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
