﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_NationalityService:DataComm
    {
        public List<NationalityEntity> SP_GetList_Nationality(int? ID = null, int? Code = null)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("ID");
            ArrayParams.Add("Code");

            ArrayValues.Add(ID);
            ArrayValues.Add(Code);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Nationality", ArrayValues, ArrayParams);

                List<NationalityEntity> returnValue = new List<NationalityEntity>();

                foreach (DataRow dr in dt.Rows)
                {
                    NationalityEntity currentNationality = new NationalityEntity((int)dr["ID"], (int)dr["Code"], (string)dr["Label"]);
                    returnValue.Add(currentNationality);
                }
                return returnValue;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
    }
}
