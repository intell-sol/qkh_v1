﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_CloseCaseService : DataComm
    {
        public int? SP_Add_CaseClose(CaseCloseEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("PCN");
            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("CloseTypeLibItemID");
            ArrayParams.Add("CloseDate");
            ArrayParams.Add("CommandNumber");
            ArrayParams.Add("CloseFromWhomID");
            ArrayParams.Add("Notes");
            ArrayParams.Add("CourtDesicion");
            ArrayParams.Add("Suicide");
            
            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.PCN);
            ArrayValues.Add(entity.OrgUnitID);
            ArrayValues.Add(entity.StartDate);
            ArrayValues.Add(entity.EndDate);
            ArrayValues.Add(entity.CloseTypeLibItemID);
            ArrayValues.Add(entity.CloseDate);
            ArrayValues.Add(entity.CommandNumber);
            ArrayValues.Add(entity.CloseFromWhomID);
            ArrayValues.Add(entity.Notes);
            ArrayValues.Add(entity.CourtDesicion);
            ArrayValues.Add(entity.Suicide);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_CaseClose", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<CaseCloseEntity> SP_GetList_CaseClose(CaseCloseEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_CaseClose", ArrayValues, ArrayParams);

                List<CaseCloseEntity> list = new List<CaseCloseEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        CaseCloseEntity item = new CaseCloseEntity(
                                ID:(int)row["ID"],
                                PrisonerID:(int)row["PrisonerID"],
                                PCN:(int)row["PCN"],
                                OrgUnitID:(int)row["OrgUnitID"],
                                OrgUnitLabel: (string)row["Label"],
                                StartDate:(row["StartDate"] != DBNull.Value) ? (DateTime?)row["StartDate"] : null,
                                EndDate:(row["EndDate"] != DBNull.Value) ? (DateTime?)row["EndDate"] : null,
                                CloseTypeLibItemID:(row["CloseTypeLibItemID"] != DBNull.Value) ? (int?)row["CloseTypeLibItemID"] : null,
                                CloseDate:(row["CloseDate"] != DBNull.Value) ? (DateTime?)row["CloseDate"] : null,
                                CommandNumber:(row["CommandNumber"] != DBNull.Value) ? (string)row["CommandNumber"] : null,
                                CloseFromWhomID:(row["CloseFromWhomID"] != DBNull.Value) ? (int?)row["CloseFromWhomID"] : null,
                                Notes:(row["Notes"] != DBNull.Value) ? (string)row["Notes"] : null,
                                Status: (row["Status"] != DBNull.Value) ? (bool?)row["Status"] : null,
                                CourtDesicion: (row["CourtDesicion"] != DBNull.Value) ? (bool?)row["CourtDesicion"] : null,
                                Suicide: (row["Suicide"] !=DBNull.Value) ? (bool?)row["Suicide"] : null,
                                ArchiveCase: true
                            );
                        
                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<CaseCloseEntity>();
            }
        }
        public List<CaseCloseEntity> SP_GetList_ActiveCase(CaseCloseEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();
                
                ArrayParams.Add("PrisonerID");
                
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_ActiveCase", ArrayValues, ArrayParams);

                List<CaseCloseEntity> list = new List<CaseCloseEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        CaseCloseEntity item = new CaseCloseEntity(
                                
                                PrisonerID: entity.PrisonerID,
                                PCN: (row["PersonalCaseNumber"] != DBNull.Value) ? (int?)row["PersonalCaseNumber"] : null,
                                OrgUnitID: (int)row["OrgUnitID"],
                                OrgUnitLabel: (string)row["Label"],
                                DataType: (bool?)row["DataType"],
                                StartDate: (row["StartDate"] != DBNull.Value) ? (DateTime?)row["StartDate"] : null,
                                EndDate:(row["EndDate"] != DBNull.Value) ? (DateTime?)row["EndDate"] : null,
                                Status: true,
                                ArchiveCase: false
                            );
                        bool ArrestType = (bool)row["DataType"];
                        if (ArrestType)
                            item.SentenceID = (int)row["CurrentID"];
                        else item.ArrestID = (int)row["CurrentID"];

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<CaseCloseEntity>();
            }
        }

        #region CaseCloseAmnesties
        public int? SP_Add_CaseCloseAmnesties(CaseCloseAmnestiesEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("AmnestyPointID");
            ArrayParams.Add("CaseCloseID");

            ArrayValues.Add(entity.AmnestyPointID);
            ArrayValues.Add(entity.CaseCloseID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_CaseCloseAmnesties", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public List<CaseCloseAmnestiesEntity> SP_GetList_CaseCloseAmnesties(CaseCloseAmnestiesEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("AmnestyPointID");
                ArrayParams.Add("CaseCloseID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.AmnestyPointID);
                ArrayValues.Add(entity.CaseCloseID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_CaseCloseAmnesties", ArrayValues, ArrayParams);

                List<CaseCloseAmnestiesEntity> list = new List<CaseCloseAmnestiesEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        CaseCloseAmnestiesEntity item = new CaseCloseAmnestiesEntity(
                                ID: (int)row["ID"],
                                AmnestyPointID: (int)row["PrisonerID"],
                                PointName: (string)row["PointName"],
                                CaseCloseID: (int)row["OrgUnitID"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<CaseCloseAmnestiesEntity>();
            }
        }

        public bool Sp_Update_CaseCloseAmnesties(CaseCloseAmnestiesEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("AmnestyPointID");
                ArrayParams.Add("CaseCloseID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.AmnestyPointID);
                ArrayValues.Add(entity.CaseCloseID);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("Sp_Update_CaseCloseAmnesties", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion
    }
}
