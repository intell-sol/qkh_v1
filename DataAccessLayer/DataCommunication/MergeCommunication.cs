﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_MergeService : DataComm
    {
        public int? SP_Add_Modification(MergeEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("EmployeeID");
            ArrayParams.Add("EID");
            ArrayParams.Add("Type");
            ArrayParams.Add("Json");
            ArrayParams.Add("EntityID");


            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.EmployeeID);
            ArrayValues.Add(entity.EID);
            ArrayValues.Add(entity.Type);
            ArrayValues.Add(entity.Json);
            ArrayValues.Add(entity.EntityID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_ModificationInfo", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public int? SP_Update_Modification(MergeEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(entity.ID);
                ArrayParams.Add("Type");
                ArrayValues.Add(entity.Type);
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.PrisonerID);
                ArrayParams.Add("ApproverID");
                ArrayValues.Add(entity.ApproverID);
                ArrayParams.Add("EmployeeID");
                ArrayValues.Add(entity.EmployeeID);
                ArrayParams.Add("Approved");
                ArrayValues.Add(entity.Approved);
                ArrayParams.Add("EntityID");
                ArrayValues.Add(entity.EntityID);
                ArrayParams.Add("Status");
                ArrayValues.Add(entity.Status);
                ArrayParams.Add("Json");
                ArrayValues.Add(entity.Json);
                ArrayParams.Add("Date");
                ArrayValues.Add(entity.Date);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_ModificationInfo", ArrayValues, ArrayParams);

                if (dt == null)
                    return -1;

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<int> SP_GetList_PermissionApproveIDs(MergeEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("OrgUnitID");

                ArrayValues.Add(entity.OrgUnitID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PermissionsApproveIDsByOrgUnitID", ArrayValues, ArrayParams);

                List<int> list = new List<int>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add((int)row["ID"]);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<int>();
            }
        }

        public bool SP_Update_WorkLoad(WorkLoadsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();


                ArrayParams.Add("ID");
                ArrayParams.Add("JobTypeLibItemID");
                ArrayParams.Add("WorkTitleLibItemID");
                ArrayParams.Add("EmployerLibItemID");
                ArrayParams.Add("EndDate");
                ArrayParams.Add("Note");
                ArrayParams.Add("ContractNumber");
                ArrayParams.Add("EngagementDate");
                ArrayParams.Add("EngagementBasisLibItemID");
                ArrayParams.Add("EngagementState");
                ArrayParams.Add("Salary");
                ArrayParams.Add("ReleaseBasisLibItemID");
                ArrayParams.Add("ReleaseDate");
                ArrayParams.Add("ReleaseNote");
                ArrayParams.Add("Status");


                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.JobTypeLibItemID);
                ArrayValues.Add(entity.WorkTitleLibItemID);
                ArrayValues.Add(entity.EmployerLibItemID);
                ArrayValues.Add(entity.EndDate);
                ArrayValues.Add(entity.Note);
                ArrayValues.Add(entity.ContractNumber);
                ArrayValues.Add(entity.EngagementDate);
                ArrayValues.Add(entity.EngagementBasisLibItemID);
                ArrayValues.Add(entity.EngagementState);
                ArrayValues.Add(entity.Salary);
                ArrayValues.Add(entity.ReleaseBasisLibItemID);
                ArrayValues.Add(entity.ReleaseDate);
                ArrayValues.Add(entity.ReleaseNote);
                ArrayValues.Add(entity.Status);


                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_WorkLoads", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public List<MergeDashboardEntity> SP_GetList_ModificationsForDashboard(FilterMergeEntity FilterData)
        {
            // UNDONE: to this first and move it to EncouragementsCommunicatin
            ArrayParams.Clear();
            ArrayValues.Clear();

            int TotalCount = 0;

            //ArrayParams.Add("StartDate");
            //ArrayParams.Add("EndDate");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("ArchiveStatus");
            ArrayParams.Add("PrisonerType");
            ArrayParams.Add("OrgUnitIDList");
            ArrayParams.Add("CurrentPage");
            ArrayParams.Add("ViewCount");

            //ArrayValues.Add(FilterData.StartDate);
            //ArrayValues.Add(FilterData.EndDate);
            ArrayValues.Add(FilterData.PrisonerID);
            ArrayValues.Add(FilterData.ArchiveStatus);
            ArrayValues.Add(FilterData.PrisonerType);
            ArrayValues.Add((FilterData.OrgUnitIDList != null && FilterData.OrgUnitIDList.Any()) ? string.Join(",", FilterData.OrgUnitIDList) : FilterData.OrgUnitID.ToString());
            ArrayValues.Add(FilterData.paging.CurrentPage);
            ArrayValues.Add(FilterData.paging.ViewCount);


            ArrayList OutParams = new ArrayList();
            Dictionary<string, object> OutValues = new Dictionary<string, object>();

            OutParams.Add("TotalCount");
            OutValues.Add("TotalCount", TotalCount);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_ModificationsForDashboard", ArrayValues, ArrayParams, ref OutValues, OutParams);

                List<MergeDashboardEntity> list = new List<MergeDashboardEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        MergeDashboardEntity item = new MergeDashboardEntity(
                            Personal_ID: (string)row["Personal_ID"],
                            PrisonerName: (string)row["PrisonerName"],
                            ArchiveStatus: (bool)row["ArchiveStatus"],
                            PrisonerType: (int)row["PrisonerType"],
                            ID: (int)row["ID"],
                            PrisonerID: (int)row["PrisonerID"],
                            EmployeeID: (int)row["EmployeeID"],
                            ApproverID: (row["ApproverID"] == DBNull.Value) ? null : (int?)row["ApproverID"],
                            Json: (row["Json"] == DBNull.Value) ? null : (string)row["Json"],
                            Type: (row["Type"] == DBNull.Value) ? null : (int?)row["Type"],
                            Date: (row["Date"] == DBNull.Value) ? null : (DateTime?)row["Date"],
                            Approved: (row["Approved"] == DBNull.Value) ? null : (bool?)row["Approved"],
                            EntityID: (row["EntityID"] == DBNull.Value) ? null : (int?)row["EntityID"],
                            EID: (row["EID"] == DBNull.Value) ? null : (int?)row["EID"],
                            Status: (row["Status"] == DBNull.Value) ? null : (bool?)row["Status"],
                            CreationDate: (row["CreationDate"] == DBNull.Value) ? null : (DateTime?)row["CreationDate"]
                        );

                        list.Add(item);
                    }
                }

                FilterData.paging.TotalCount = (int)OutValues["TotalCount"];
                FilterData.paging.TotalPage = (int)Math.Ceiling(Convert.ToDouble(OutValues["TotalCount"]) / FilterData.paging.ViewCount);
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                FilterData.paging.TotalPage = 0;
                return new List<MergeDashboardEntity>();
            }
        }

        public List<MergeDashboardEntity> SP_GetList_GroupedModificationsForDashboard(FilterMergeEntity FilterData)
        {
            // UNDONE: to this first and move it to EncouragementsCommunicatin
            ArrayParams.Clear();
            ArrayValues.Clear();

            int TotalCount = 0;

            //ArrayParams.Add("StartDate");
            //ArrayParams.Add("EndDate");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("ArchiveStatus");
            ArrayParams.Add("PrisonerType");
            ArrayParams.Add("OrgUnitIDList");
            ArrayParams.Add("CurrentPage");
            ArrayParams.Add("ViewCount");

            //ArrayValues.Add(FilterData.StartDate);
            //ArrayValues.Add(FilterData.EndDate);
            ArrayValues.Add(FilterData.PrisonerID);
            ArrayValues.Add(FilterData.ArchiveStatus);
            ArrayValues.Add(FilterData.PrisonerType);
            ArrayValues.Add((FilterData.OrgUnitIDList != null && FilterData.OrgUnitIDList.Any()) ? string.Join(",", FilterData.OrgUnitIDList) : FilterData.OrgUnitID.ToString());
            ArrayValues.Add(FilterData.paging.CurrentPage);
            ArrayValues.Add(FilterData.paging.ViewCount);
            
            ArrayList OutParams = new ArrayList();
            Dictionary<string, object> OutValues = new Dictionary<string, object>();

            OutParams.Add("TotalCount");
            OutValues.Add("TotalCount", TotalCount);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_GroupedModificationsForDashboard", ArrayValues, ArrayParams, ref OutValues, OutParams);

                List<MergeDashboardEntity> list = new List<MergeDashboardEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        MergeDashboardEntity item = new MergeDashboardEntity(
                            Personal_ID: (string)row["Personal_ID"],
                            PrisonerName: (string)row["PrisonerName"],
                            ArchiveStatus: (bool)row["ArchiveStatus"],
                            PrisonerType: (int)row["PrisonerType"],
                            ID: (int)row["ID"],
                            PrisonerID: (int)row["PrisonerID"],
                            ModificationCount: (int)row["ModificationCount"],
                            LastModificationDate: row.IsNull("LastModificationDate")? null: (DateTime?)row["LastModificationDate"]
                        );

                        list.Add(item);
                    }
                }

                FilterData.paging.TotalCount = (int)OutValues["TotalCount"];
                FilterData.paging.TotalPage = (int)Math.Ceiling(Convert.ToDouble(OutValues["TotalCount"]) / FilterData.paging.ViewCount);
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                FilterData.paging.TotalPage = 0;
                return new List<MergeDashboardEntity>();
            }
        }

        public List<MergeObjectEntity> SP_GetList_ModificationInfoByEIDList(string EIDList, int QKHOrgUnitID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("EIDList");
                ArrayValues.Add(EIDList);
                ArrayParams.Add("QKHOrgUnitID");
                ArrayValues.Add(QKHOrgUnitID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_ModificationInfoByEIDList", ArrayValues, ArrayParams);

                List<MergeObjectEntity> list = new List<MergeObjectEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        MergeObjectEntity Item = new MergeObjectEntity();
                        MergeEntity entity = new MergeEntity(
                            ID: (int)row["ID"],
                            PrisonerID: (int)row["PrisonerID"],
                            EmployeeID: (int)row["EmployeeID"],
                            ApproverID: (row["ApproverID"] == DBNull.Value) ? null : (int?)row["ApproverID"],
                            Json: (row["Json"] == DBNull.Value) ? null : (string)row["Json"],
                            Type: (row["Type"] == DBNull.Value) ? null : (int?)row["Type"],
                            Date: (row["Date"] == DBNull.Value) ? null : (DateTime?)row["Date"],
                            Approved: (row["Approved"] == DBNull.Value) ? null : (bool?)row["Approved"],
                            EntityID: (row["EntityID"] == DBNull.Value) ? null : (int?)row["EntityID"],
                            EID: (row["EID"] == DBNull.Value) ? null : (int?)row["EID"],
                            Status: (row["Status"] == DBNull.Value) ? null : (bool?)row["Status"],
                            CreationDate: (row["CreationDate"] == DBNull.Value) ? null : (DateTime?)row["CreationDate"]
                            );
                        Item.currentModification = entity;

                        list.Add(Item);
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<MergeObjectEntity>();
            }
        }

        public MergeObjectEntity SP_GetList_PrisonersActiveModifications(int PrisonerID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PrisonersActiveModifications", ArrayValues, ArrayParams);

                MergeObjectEntity MOEntity = new MergeObjectEntity(PrisonerID: PrisonerID);
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        MergeEntity entity = new MergeEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                EmployeeID: (int)row["EmployeeID"],
                                ApproverID: (row["ApproverID"] == DBNull.Value) ? null : (int?)row["ApproverID"],
                                Json: (row["Json"] == DBNull.Value) ? null : (string)row["Json"],
                                Type: (row["Type"] == DBNull.Value) ? null : (int?)row["Type"],
                                Date: (row["Date"] == DBNull.Value) ? null : (DateTime?)row["Date"],
                                Approved: (row["Approved"] == DBNull.Value) ? null : (bool?)row["Approved"],
                                EntityID: (row["EntityID"] == DBNull.Value) ? null : (int?)row["EntityID"],
                                EID: (row["EID"] == DBNull.Value) ? null : (int?)row["EID"],
                                Status: (row["Status"] == DBNull.Value) ? null : (bool?)row["Status"],
                                CreationDate: (row["CreationDate"] == DBNull.Value) ? null : (DateTime?)row["CreationDate"]
                            );
                        MOEntity.currentModification = entity;
                        MOEntity.Merge.Add(entity);
                    }
                }

                return MOEntity;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<MergeEntity> SP_GetList_ModificationInfo(MergeEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayValues.Add(entity.ID);
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.PrisonerID);
                ArrayParams.Add("EmployeeID");
                ArrayValues.Add(entity.EmployeeID);
                ArrayParams.Add("ApproverID");
                ArrayValues.Add(entity.ApproverID);
                ArrayParams.Add("Type");
                ArrayValues.Add(entity.Type);
                ArrayParams.Add("Approved");
                ArrayValues.Add(entity.Approved);
                ArrayParams.Add("EntityID");
                ArrayValues.Add(entity.EntityID);
                ArrayParams.Add("EID");
                ArrayValues.Add(entity.EID);
                ArrayParams.Add("Status");
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_ModificationInfo", ArrayValues, ArrayParams);

                List<MergeEntity> MergeList = new List<MergeEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        MergeEntity mEntity = new MergeEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                EmployeeID: (int)row["EmployeeID"],
                                ApproverID: (row["ApproverID"] == DBNull.Value) ? null : (int?)row["ApproverID"],
                                Json: (row["Json"] == DBNull.Value) ? null : (string)row["Json"],
                                Type: (row["Type"] == DBNull.Value) ? null : (int?)row["Type"],
                                Date: (row["Date"] == DBNull.Value) ? null : (DateTime?)row["Date"],
                                Approved: (row["Approved"] == DBNull.Value) ? null : (bool?)row["Approved"],
                                EntityID: (row["EntityID"] == DBNull.Value) ? null : (int?)row["EntityID"],
                                EID: (row["EID"] == DBNull.Value) ? null : (int?)row["EID"],
                                Status: (row["Status"] == DBNull.Value) ? null : (bool?)row["Status"],
                                CreationDate: (row["CreationDate"] == DBNull.Value) ? null : (DateTime?)row["CreationDate"]
                            );
                        MergeList.Add(mEntity);
                    }
                }

                return MergeList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<MergeEntity>();
            }
        }
        public List<string> SP_GetList_EID_WaitAppove(int PrisonerID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_EID_WaitAppove", ArrayValues, ArrayParams);

                List<string> list = new List<string>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add((string)row["EID"]);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<string>();
            }
        }
    }
}