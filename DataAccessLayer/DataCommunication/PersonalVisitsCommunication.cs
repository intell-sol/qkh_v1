﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PersonalVisitsCommunication : DataComm
    {
        public int? SP_Add_PersonalVisits(PersonalVisitsEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("TypeLibItemID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("PersonID");
            ArrayParams.Add("VisitStatus");
            ArrayParams.Add("ApproverEmployeeID");
            ArrayParams.Add("Description"); 
            ArrayParams.Add("Group");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.TypeLibItemID);
            ArrayValues.Add(entity.StartDate);
            ArrayValues.Add(entity.EndDate);
            ArrayValues.Add(entity.PersonID);
            ArrayValues.Add(entity.VisitStatus);
            ArrayValues.Add(entity.ApproverEmployeeID);
            ArrayValues.Add(entity.Description);
            ArrayValues.Add(entity.Group);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PersonalVisits", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public List<PersonalVisitsEntity> SP_GetList_PersonalVisits(PersonalVisitsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PersonalVisits", ArrayValues, ArrayParams);

                List<PersonalVisitsEntity> list = new List<PersonalVisitsEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PersonalVisitsEntity item = new PersonalVisitsEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                TypeLibItemID: (int)row["TypeLibItemID"],
                                StartDate: (DateTime)row["StartDate"],
                                EndDate: (row["EndDate"] == DBNull.Value) ? null : (DateTime?)row["EndDate"],
                                PersonID: (int)row["PersonID"],
                                PersonName: (string)row["PersonName"],
                                VisitStatus: (row["VisitStatus"] == DBNull.Value) ? null : (bool?)row["VisitStatus"],
                                ApproverEmployeeID: (row["ApproverEmployeeID"] == DBNull.Value) ? null : (int?)row["ApproverEmployeeID"],
                                Status: (bool)row["Status"],
                                TypeLibItemLabel: (row["TypeLibItemLabel"] != DBNull.Value) ? (string)row["TypeLibItemLabel"] : null,
                                ApproverEmployeeName: (row["ApproverEmployeeName"] != DBNull.Value) ? (string)row["ApproverEmployeeName"] : null,
                                Description: (row["Description"] != DBNull.Value) ? (string)row["Description"] : null,
                                Group: (row["Group"] != DBNull.Value) ? (string)row["Group"] : null,
                                MergeStatus: (row["MergeStatus"] != DBNull.Value) ? (bool?)row["MergeStatus"] : null
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<PersonalVisitsEntity>();
            }
        }

        public bool SP_Update_PersonalVisits(PersonalVisitsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayParams.Add("TypeLibItemID");
                ArrayParams.Add("StartDate");
                ArrayParams.Add("EndDate");
                ArrayParams.Add("PersonID");
                ArrayParams.Add("VisitStatus");
                ArrayParams.Add("ApproverEmployeeID");
                ArrayParams.Add("Status");
                ArrayParams.Add("Description");
                ArrayParams.Add("Group");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);
                ArrayValues.Add(entity.TypeLibItemID);
                ArrayValues.Add(entity.StartDate);
                ArrayValues.Add(entity.EndDate);
                ArrayValues.Add(entity.PersonID);
                ArrayValues.Add(entity.VisitStatus);
                ArrayValues.Add(entity.ApproverEmployeeID);
                ArrayValues.Add(entity.Status);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.Group);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_PersonalVisits", ArrayValues, ArrayParams);

                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public List<PersonalVisitsDashboardEntity> SP_GetList_PersonalVisitsForDashboard(FilterPersonalVisitsEntity FilterData)
        {
            // UNDONE: to this first and move it to EncouragementsCommunicatin
            ArrayParams.Clear();
            ArrayValues.Clear();

            int TotalCount = 0;

            ArrayParams.Add("FirstName");
            ArrayParams.Add("MiddleName");
            ArrayParams.Add("LastName");
            ArrayParams.Add("TypeLibItemID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("ArchiveStatus");
            ArrayParams.Add("VisitStatus");
            ArrayParams.Add("PrisonerType");
            ArrayParams.Add("OrgUnitIDList");
            ArrayParams.Add("CurrentPage");
            ArrayParams.Add("ViewCount");

            ArrayValues.Add(FilterData.FirstName);
            ArrayValues.Add(FilterData.MiddleName);
            ArrayValues.Add(FilterData.LastName);
            ArrayValues.Add(FilterData.TypeLibItemID);
            ArrayValues.Add(FilterData.StartDate);
            ArrayValues.Add(FilterData.EndDate);
            ArrayValues.Add(FilterData.PrisonerID);
            ArrayValues.Add(FilterData.ArchiveStatus);
            ArrayValues.Add(FilterData.VisitStatus);
            ArrayValues.Add(FilterData.PrisonerType);
            ArrayValues.Add((FilterData.OrgUnitIDList != null && FilterData.OrgUnitIDList.Any()) ? string.Join(",", FilterData.OrgUnitIDList) : FilterData.OrgUnitID.ToString());
            ArrayValues.Add(FilterData.paging.CurrentPage);
            ArrayValues.Add(FilterData.paging.ViewCount);


            ArrayList OutParams = new ArrayList();
            Dictionary<string, object> OutValues = new Dictionary<string, object>();

            OutParams.Add("TotalCount");
            OutValues.Add("TotalCount", TotalCount);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PersonalVisitsForDashboard", ArrayValues, ArrayParams, ref OutValues, OutParams);

                List<PersonalVisitsDashboardEntity> list = new List<PersonalVisitsDashboardEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PersonalVisitsDashboardEntity item = new PersonalVisitsDashboardEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                TypeLibItemID: (row["TypeLibItemID"] == DBNull.Value) ? null : (int?)row["TypeLibItemID"],
                                ApproverEmployeeID: (row["ApproverEmployeeID"] == DBNull.Value) ? null : (int?)row["ApproverEmployeeID"],
                                TypeLibItemLabel: (row["TypeLibItemLabel"] == DBNull.Value) ? null : (string)row["TypeLibItemLabel"],
                                StartDate: (row["StartDate"] == DBNull.Value) ? null : (DateTime?)row["StartDate"],
                                EndDate: (row["EndDate"] == DBNull.Value) ? null : (DateTime?)row["EndDate"],
                                Status: (bool)row["Status"],
                                PersonName: (row["PersonName"] == DBNull.Value) ? null : (string)row["PersonName"],
                                Personal_ID: (row["Personal_ID"] == DBNull.Value)?null:(string)row["Personal_ID"],
                                PrisonerName: (row["PrisonerName"] == DBNull.Value) ? null : (string)row["PrisonerName"],
                                ArchiveStatus: (row["ArchiveStatus"] == DBNull.Value) ? null : (bool?)row["ArchiveStatus"],
                                VisitStatus: (row["VisitStatus"] == DBNull.Value) ? null : (bool?)row["VisitStatus"],
                                PrisonerType: (row["PrisonerType"] == DBNull.Value)? null:(int?)row["PrisonerType"]
                            );

                        list.Add(item);
                    }
                }

                FilterData.paging.TotalCount = (int)OutValues["TotalCount"];
                FilterData.paging.TotalPage = (int)Math.Ceiling(Convert.ToDouble(OutValues["TotalCount"]) / FilterData.paging.ViewCount);
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                FilterData.paging.TotalPage = 0;
                return new List<PersonalVisitsDashboardEntity>();
            }
        }

    }
}
