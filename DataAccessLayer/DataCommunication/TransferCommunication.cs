﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_TransferService : DataComm
    {
        public int? SP_Add_Transfer(TransferEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("PersonID");
            ArrayParams.Add("PurposeLibItemID");
            ArrayParams.Add("TransferLibItemID");
            ArrayParams.Add("HospitalLibItemID");
            ArrayParams.Add("CourtLibItemID");
            ArrayParams.Add("StateLibItemID");
            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("Description");
            ArrayParams.Add("EndLibItemID");
            ArrayParams.Add("EmergencyLibItemID");
            ArrayParams.Add("TransferTypeLibItemID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("ApproverEmployeeID");

            ArrayParams.Add("HogebujaranLibItemID");
            ArrayParams.Add("QnnchakanLibItemID");

            ArrayParams.Add("CurrentOrgUnit");
            ArrayParams.Add("ApproveStatus");
            ArrayParams.Add("ApproveDate");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.PersonID);
            ArrayValues.Add(entity.PurposeLibItemID);
            ArrayValues.Add(entity.TransferLibItemID);
            ArrayValues.Add(entity.HospitalLibItemID);
            ArrayValues.Add(entity.CourtLibItemID);
            ArrayValues.Add(entity.StateLibItemID);
            ArrayValues.Add(entity.OrgUnitID);
            ArrayValues.Add(entity.Description);
            ArrayValues.Add(entity.EndLibItemID);
            ArrayValues.Add(entity.EmergencyLibItemID);
            ArrayValues.Add(entity.TransferTypeLibItemID);
            ArrayValues.Add(entity.StartDate);
            ArrayValues.Add(entity.EndDate);
            ArrayValues.Add(entity.ApproverEmployeeID);

            ArrayValues.Add(entity.HogebujaranLibItemID);
            ArrayValues.Add(entity.QnnchakanLibItemID);

            ArrayValues.Add(entity.CurrentOrgUnit);
            ArrayValues.Add(entity.ApproveStatus);
            ArrayValues.Add(entity.ApproveDate);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Transfer", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<TransferEntity> SP_GetList_Trasnfer(TransferEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayParams.Add("OrgUnitID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);
                ArrayValues.Add(entity.OrgUnitID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Transfers", ArrayValues, ArrayParams);

                List<TransferEntity> list = new List<TransferEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        TransferEntity item = new TransferEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                PersonID: (int)row["PersonID"],
                                PersonName:(string)row["PersonName"],
                                PurposeLibItemLabel: (row["PurposeLibItemLabel"] == DBNull.Value) ? null : (string)row["PurposeLibItemLabel"],
                                ApproverEmployeeName: (row["ApproverEmployeeName"] == DBNull.Value) ? null : (string)row["ApproverEmployeeName"],      
                                PurposeLibItemID: (int)row["PurposeLibItemID"],
                                TransferLibItemID: (row["TransferLibItemID"] == DBNull.Value) ? null : (int?)row["TransferLibItemID"],
                                TransferLibItemLabel: (row["TransferLibItemLabel"] == DBNull.Value) ? null : (string)row["TransferLibItemLabel"],
                                HospitalLibItemID: (row["HospitalLibItemID"] == DBNull.Value) ? null : (int?)row["HospitalLibItemID"],
                                CourtLibItemID: (row["CourtLibItemID"] == DBNull.Value) ? null : (int?)row["CourtLibItemID"],
                                StateLibItemID: (row["StateLibItemID"] == DBNull.Value) ? null : (int?)row["StateLibItemID"],
                                HogebujaranLibItemID: (row["HogebujaranLibItemID"] == DBNull.Value) ? null : (int?)row["HogebujaranLibItemID"],
                                QnnchakanLibItemID: (row["QnnchakanLibItemID"] == DBNull.Value) ? null : (int?)row["QnnchakanLibItemID"],
                                StateLibItemLabel: (row["StateLibItemLabel"] == DBNull.Value) ? null : (string)row["StateLibItemLabel"],
                                OrgUnitID: (row["OrgUnitID"] == DBNull.Value) ? null : (int?)row["OrgUnitID"],
                                Description: (row["Description"] == DBNull.Value) ? null : (string)row["Description"],
                                EndLibItemID: (row["EndLibItemID"] == DBNull.Value) ? null : (int?)row["EndLibItemID"],
                                EndLibItemLabel: (row["EndLibItemLabel"] == DBNull.Value) ? null : (string)row["EndLibItemLabel"],
                                EmergencyLibItemID: (row["EmergencyLibItemID"] == DBNull.Value) ? null : (int?)row["EmergencyLibItemID"],
                                TransferTypeLibItemID: (row["TransferTypeLibItemID"] == DBNull.Value) ? null : (int?)row["TransferTypeLibItemID"],
                                StartDate: (DateTime)row["StartDate"],
                                ApproveDate: (row["ApproveDate"] == DBNull.Value) ? null : (DateTime?)row["ApproveDate"],
                                CurrentOrgUnit: (row["CurrentOrgUnit"] == DBNull.Value) ? null : (int?)row["CurrentOrgUnit"],
                                ApproveStatus: (row["ApproveStatus"] == DBNull.Value) ? null : (bool?)row["ApproveStatus"],
                                EndDate: (row["EndDate"] != DBNull.Value) ? (DateTime?)row["EndDate"] : null,
                                MergeStatus: (row["MergeStatus"] != DBNull.Value) ? (bool?)row["MergeStatus"] : null,
                                Status: (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<TransferEntity>();
            }
        }
        public bool SP_Update_Transfer(TransferEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PersonID");
                ArrayParams.Add("PurposeLibItemID");
                ArrayParams.Add("TransferLibItemID");
                ArrayParams.Add("HospitalLibItemID");
                ArrayParams.Add("CourtLibItemID");
                ArrayParams.Add("StateLibItemID");
                ArrayParams.Add("OrgUnitID");
                ArrayParams.Add("Description");
                ArrayParams.Add("EndLibItemID");
                ArrayParams.Add("EmergencyLibItemID");
                ArrayParams.Add("TransferTypeLibItemID");                
                ArrayParams.Add("StartDate");
                ArrayParams.Add("EndDate");
                ArrayParams.Add("ApproverEmployeeID");
                ArrayParams.Add("QnnchakanLibItemID");
                ArrayParams.Add("HogebujaranLibItemID");
                ArrayParams.Add("Status");

                ArrayParams.Add("CurrentOrgUnit");
                ArrayParams.Add("ApproveStatus");
                ArrayParams.Add("ApproveDate");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PersonID);
                ArrayValues.Add(entity.PurposeLibItemID);
                ArrayValues.Add(entity.TransferLibItemID);
                ArrayValues.Add(entity.HospitalLibItemID);
                ArrayValues.Add(entity.CourtLibItemID);
                ArrayValues.Add(entity.StateLibItemID);
                ArrayValues.Add(entity.OrgUnitID);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.EndLibItemID);
                ArrayValues.Add(entity.EmergencyLibItemID);
                ArrayValues.Add(entity.TransferTypeLibItemID);
                ArrayValues.Add(entity.StartDate);
                ArrayValues.Add(entity.EndDate);
                ArrayValues.Add(entity.ApproverEmployeeID);
                ArrayValues.Add(entity.QnnchakanLibItemID);
                ArrayValues.Add(entity.HogebujaranLibItemID);
                ArrayValues.Add(entity.Status);

                ArrayValues.Add(entity.CurrentOrgUnit);
                ArrayValues.Add(entity.ApproveStatus);
                ArrayValues.Add(entity.ApproveDate);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Transfer", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public List<TransferDashboardEntity> SP_GetList_TransfersForDashboard(FilterTransferEntity TransferData)
        {
            // UNDONE: to this first and move it to EncouragementsCommunicatin
            ArrayParams.Clear();
            ArrayValues.Clear();

            int TotalCount = 0;

            ArrayParams.Add("FirstName");
            ArrayParams.Add("MiddleName");
            ArrayParams.Add("LastName");
            ArrayParams.Add("TransferLibItemID");
            ArrayParams.Add("PurposeLibItemID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("PrisonerType");
            ArrayParams.Add("ArchiveStatus");
            ArrayParams.Add("OrgUnitIDList");
            ArrayParams.Add("CurrentPage");
            ArrayParams.Add("ViewCount");

            ArrayValues.Add(TransferData.FirstName);
            ArrayValues.Add(TransferData.MiddleName);
            ArrayValues.Add(TransferData.LastName);
            ArrayValues.Add(TransferData.TransferLibItemID);
            ArrayValues.Add(TransferData.PurposeLibItemID);
            ArrayValues.Add(TransferData.StartDate);
            ArrayValues.Add(TransferData.EndDate);
            ArrayValues.Add(TransferData.PrisonerID);
            ArrayValues.Add(TransferData.PrisonerType);
            ArrayValues.Add(TransferData.ArchiveStatus);
            ArrayValues.Add((TransferData.OrgUnitIDList != null && TransferData.OrgUnitIDList.Any()) ? string.Join(",", TransferData.OrgUnitIDList) : TransferData.OrgUnitID.ToString());
            ArrayValues.Add(TransferData.paging.CurrentPage);
            ArrayValues.Add(TransferData.paging.ViewCount);


            ArrayList OutParams = new ArrayList();
            Dictionary<string, object> OutValues = new Dictionary<string, object>();

            OutParams.Add("TotalCount");
            OutValues.Add("TotalCount", TotalCount);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_TransfersForDashboard", ArrayValues, ArrayParams, ref OutValues, OutParams);

                List<TransferDashboardEntity> list = new List<TransferDashboardEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        TransferDashboardEntity item = new TransferDashboardEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                TransferLibItemID: (int)row["TransferLibItemID"],
                                TransferLibItemLabel: (string)row["TransferLibItemLabel"],
                                StartDate: (row["StartDate"] == DBNull.Value) ? null : (DateTime?)row["StartDate"],
                                ApproverEmployeeName: (row["ApproverEmployeeName"] == DBNull.Value) ? null : (string)row["ApproverEmployeeName"],
                                ApproveStatus: (row["ApproveStatus"] == DBNull.Value) ? null : (bool?)row["ApproveStatus"],
                                ApproveDate: (row["ApproveDate"] == DBNull.Value) ? null : (DateTime?)row["ApproveDate"],
                                ApproverEmployeeID: (row["ApproveEmployeeID"] == DBNull.Value) ? null : (int?)row["ApproveEmployeeID"],
                                OrgUnitID: (row["OrgUnitID"] == DBNull.Value) ? null : (int?)row["OrgUnitID"],
                                PersonName: (row["PersonName"] == DBNull.Value) ? null : (string)row["PersonName"],
                                EndDate: (row["EndDate"] == DBNull.Value) ? null : (DateTime?)row["EndDate"],
                                Status: (bool)row["Status"],
                                ArchiveStatus: (bool)row["ArchiveStatus"],
                                PurposeLibItemID: (row["PurposeLibItemID"] == DBNull.Value) ? null : (int?)row["PurposeLibItemID"],
                                StateLibItemID: (row["StateLibItemID"] == DBNull.Value) ? null : (int?)row["StateLibItemID"],
                                StateLibItemLabel: (row["StateLibItemLabel"] == DBNull.Value) ? null : (string)row["StateLibItemLabel"],
                                PurposeLibItemLabel: (row["PurposeLibItemLabel"] == DBNull.Value) ? null : (string)row["PurposeLibItemLabel"],
                                Personal_ID: (string)row["Personal_ID"],
                                PrisonerName: (string)row["PrisonerName"],
                                PrisonerType: (int)row["PrisonerType"]
                            );

                        list.Add(item);
                    }
                }

                TransferData.paging.TotalCount = (int)OutValues["TotalCount"];
                TransferData.paging.TotalPage = (int)Math.Ceiling(Convert.ToDouble(OutValues["TotalCount"]) / TransferData.paging.ViewCount);
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                TransferData.paging.TotalPage = 0;
                return new List<TransferDashboardEntity>();
            }
        }

    }
}