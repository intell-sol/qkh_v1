﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_PropsServices : DataComm
    {
        public List<PropsEntity> SP_GetList_Properties(int? LibID = null)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();
                ArrayParams.Add("LibID");
                ArrayValues.Add(LibID);
                
                DataTable dtProps = new DataTable();

                dtProps = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PropertiesByLibID", ArrayValues, ArrayParams);

                List<PropsEntity> list = new List<PropsEntity>();
                if (dtProps != null)
                {
                    foreach (DataRow dr in dtProps.Rows)
                    {
                        PropsEntity currentProp = new PropsEntity((int)dr["ID"], (string)dr["Name"], (Boolean)dr["Fixed"]);
                        currentProp.NewItem = false;
                        list.Add(currentProp);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }


        public PropsEntity SP_GetList_Property(int PropID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();
                ArrayParams.Add("ID");
                ArrayValues.Add(PropID);

                DataTable dtProps = new DataTable();

                dtProps = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Properties", ArrayValues, ArrayParams);

                PropsEntity returnProp = null;
                if (dtProps != null)
                {
                    foreach (DataRow dr in dtProps.Rows)
                    {
                        returnProp = new PropsEntity((int)dr["ID"], (string)dr["Name"], (Boolean)dr["Fixed"]);
                    }
                }

                return returnProp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        public LibsEntity SP_Get_CitizenshipLibIDByCode(int PathID, string Value)
        {
            try
            {
                DataTable dtPropValues = new DataTable();

                ArrayParams.Clear();
                ArrayValues.Clear();
                ArrayParams.Add("PathID");
                ArrayParams.Add("Value");
                ArrayValues.Add(PathID);
                ArrayValues.Add(Value);

                dtPropValues = dtDataUtility.Exec_SP_Into_DataTable("SP_Get_CitizenshipLibIDByCode", ArrayValues, ArrayParams);

                LibsEntity Lib = new LibsEntity();

                foreach (DataRow drp in dtPropValues.Rows)
                {
                    Lib.ID = (int)drp["ID"];
                    Lib.Name = (string)drp["Name"];
                    Lib.ParentID = (int)drp["ParentID"];
                    Lib.Status = (bool)drp["Status"];
                    break;
                }

                return Lib;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        public List<PropValuesEntity> SP_GetList_PropertiesValue(int PropID, int? LibID = null)
        {
            try
            {
                DataTable dtPropValues = new DataTable();

                ArrayParams.Clear();
                ArrayValues.Clear();
                ArrayParams.Add("PropID");
                ArrayParams.Add("LibID");
                ArrayValues.Add(PropID);
                ArrayValues.Add(LibID);

                dtPropValues = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_PropertiesValues", ArrayValues, ArrayParams);

                List<PropValuesEntity> list = new List<PropValuesEntity>();

                foreach (DataRow drp in dtPropValues.Rows)
                {
                    PropValuesEntity currentPropValue = new PropValuesEntity((int)drp["ID"], (string)drp["Value"], (int)drp["PropID"]);
                    list.Add(currentPropValue);
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public int? SP_Add_PropertyToList(int? LibID, int? PropID, int? PropValueID)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("LibID");
            ArrayParams.Add("PropID");
            ArrayParams.Add("PropValueID");
            ArrayValues.Add(LibID);
            ArrayValues.Add(PropID);
            ArrayValues.Add(PropValueID);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PropertiesToPropertyValue", ArrayValues, ArrayParams);

                Decimal decId = (Decimal)dt.Rows[0][0];

                int newId = Decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public Boolean SP_Update_PropertyToList(int LibID)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("LibID");
            ArrayValues.Add(LibID);
            ArrayParams.Add("Status");
            ArrayValues.Add(0);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_PropertiesToPropertyValueByListID", ArrayValues, ArrayParams);

                return true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return false;
            }
        }


        public Boolean SP_Update_PropertiesToPropertyValue(int LibID, int PropID, bool Status)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("ID");
            ArrayParams.Add("LibID");
            ArrayParams.Add("PropID");
            ArrayParams.Add("PropValueID");
            ArrayParams.Add("Status");

            ArrayValues.Add(null);
            ArrayValues.Add(LibID);
            ArrayValues.Add(PropID);
            ArrayValues.Add(null);
            ArrayValues.Add(Status);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_PropertiesToPropertyValue", ArrayValues, ArrayParams);

                if (dt != null)
                    return true;

                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return false;
            }
        }
        public Boolean SP_Update_PropertyValue(string idList)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("IDList");
            ArrayValues.Add(idList);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Delete_PropertyValueByIDList", ArrayValues, ArrayParams);

                return true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return false;
            }
        }

        public int? SP_Add_PropertyValue(int? PropID, String Value, Boolean Fixed)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            ArrayParams.Add("PropID");
            ArrayParams.Add("Value");
            //ArrayParams.Add("CustomID");
            ArrayValues.Add(PropID);
            ArrayValues.Add(Value);
            //ArrayValues.Add(CustomID);
            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_PropertyValue", ArrayValues, ArrayParams);

                Decimal decId = (Decimal)dt.Rows[0][0];

                int newId = Decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
    }
}