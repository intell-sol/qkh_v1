﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_SentencingDataService : DataComm
    {
        #region SentencingData
        public int? SP_Add_SentencingData(SentencingDataEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("SentencingDecisionLibItem_ID");
            ArrayParams.Add("SentencNumber");
            ArrayParams.Add("Judge");
            ArrayParams.Add("SentencingDate");
            ArrayParams.Add("SentencingVerdictDate");
            ArrayParams.Add("CodeLibItem_ID");
            ArrayParams.Add("PunishmentLibItemID");
            ArrayParams.Add("PenaltyDay");
            ArrayParams.Add("PenaltyMonth");
            ArrayParams.Add("PenaltyYear");
            ArrayParams.Add("SentencingStartDate");
            ArrayParams.Add("SentencingEndDate");
            ArrayParams.Add("EarlyDate");
            ArrayParams.Add("EarlyType");
            ArrayParams.Add("SentenceArticleType");
            ArrayParams.Add("Description");
            ArrayParams.Add("PenaltyLibItemID");

            ArrayParams.Add("LowSuit");
            ArrayParams.Add("LowSuitState");
            ArrayParams.Add("LowSuitAmount");
            ArrayParams.Add("PropertyConfiscationLibItemID");
            ArrayParams.Add("PropertyConfiscationValue");
            ArrayParams.Add("Fine");

            ArrayParams.Add("LowSuitPayAmount");
            ArrayParams.Add("FineHas");
            ArrayParams.Add("FinePay");
            ArrayParams.Add("FineState");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.OrgUnitID);
            ArrayValues.Add(entity.SentencingDecisionLibItem_ID);
            ArrayValues.Add(entity.SentencNumber);
            ArrayValues.Add(entity.Judge);
            ArrayValues.Add(entity.SentencingDate);
            ArrayValues.Add(entity.SentencingVerdictDate);
            ArrayValues.Add(entity.CodeLibItem_ID);
            ArrayValues.Add(entity.PunishmentLibItemID);
            ArrayValues.Add(entity.PenaltyDay);
            ArrayValues.Add(entity.PenaltyMonth);
            ArrayValues.Add(entity.PenaltyYear);
            ArrayValues.Add(entity.SentencingStartDate);
            ArrayValues.Add(entity.SentencingEndDate);
            ArrayValues.Add(entity.EarlyDate);
            ArrayValues.Add(entity.EarlyType);
            ArrayValues.Add(entity.SentenceArticleType);
            ArrayValues.Add(entity.Description);
            ArrayValues.Add(entity.PenaltyLibItemID);

            ArrayValues.Add(entity.LowSuit);
            ArrayValues.Add(entity.LowSuitState);
            ArrayValues.Add(entity.LowSuitAmount);
            ArrayValues.Add(entity.PropertyConfiscationLibItemID);
            ArrayValues.Add(entity.PropertyConfiscationValue);
            ArrayValues.Add(entity.Fine);

            ArrayValues.Add(entity.LowSuitPayAmount);
            ArrayValues.Add(entity.FineHas);
            ArrayValues.Add(entity.FinePay);
            ArrayValues.Add(entity.FineState);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_SentencingData", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<SentencingDataEntity> SP_GetList_SentencingData(SentencingDataEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_SentencingData", ArrayValues, ArrayParams);

                List<SentencingDataEntity> list = new List<SentencingDataEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SentencingDataEntity item = new SentencingDataEntity(
                                ID:(int)row["ID"],
                                PrisonerID:(int)row["PrisonerID"],
                                OrgUnitID:(int)row["OrgUnitID"],
                                SentencingDecisionLibItem_ID:(int)row["SentencingDecisionLibItem_ID"],
                                SentencingDecisionLibItem_Label:(string)row["SentencingDecisionLibItem_Label"],
                                SentencNumber:(string)row["SentencNumber"],
                                Judge: (row["Judge"] == DBNull.Value) ? null : (string)row["Judge"],
                                SentencingDate:(DateTime)row["SentencingDate"],
                                SentencingStartDate:(row["SentencingStartDate"] == DBNull.Value) ? null : (DateTime?)row["SentencingStartDate"],
                                SentencingVerdictDate: (row["SentencingVerdictDate"] == DBNull.Value) ? null : (DateTime?)row["SentencingVerdictDate"],
                                CodeLibItem_ID:(int)row["CodeLibItem_ID"],
                                CodeLibItem_Label:(string)row["CodeLibItem_Label"],
                                PenaltyDay:(row["PenaltyDay"] == DBNull.Value) ? null : (int?)row["PenaltyDay"],
                                CloseCaseID: (row["CloseCaseID"] == DBNull.Value) ? null : (int?)row["CloseCaseID"],
                                OpenCaseID: (row["OpenCaseID"] == DBNull.Value) ? null : (int?)row["OpenCaseID"],
                                PenaltyMonth:(row["PenaltyMonth"] == DBNull.Value) ? null : (int?)row["PenaltyMonth"],
                                PenaltyYear:(row["PenaltyYear"] == DBNull.Value) ? null : (int?)row["PenaltyYear"],
                                SentencingEndDate:(DateTime)row["SentencingEndDate"],
                                EarlyDate:(row["EarlyDate"] == DBNull.Value) ? null : (DateTime?)row["EarlyDate"],
                                SentenceArticleType: (row["SentenceArticleType"] == DBNull.Value) ? null : (int?)row["SentenceArticleType"],
                                Description: (row["Description"] == DBNull.Value) ? null : (string)row["Description"],
                                State: (row["State"] == DBNull.Value) ? null : (bool?)row["State"],
                                EarlyType:(row["EarlyType"] == DBNull.Value) ? null : (int?)row["EarlyType"],
                                PenaltyLibItemID:(row["PenaltyLibItemID"] == DBNull.Value) ? null : (int?)row["PenaltyLibItemID"],
                                PunishmentLibItemID: (row["PunishmentLibItemID"] == DBNull.Value) ? null : (int?)row["PunishmentLibItemID"],
                                PenaltyLibItemLabel:(row["PenaltyLibItemLabel"] == DBNull.Value) ? null : (string)row["PenaltyLibItemLabel"],
                                PunishmentLibItemLabel: (row["PunishmentLibItemLabel"] == DBNull.Value) ? null : (string)row["PunishmentLibItemLabel"],

                                LowSuit: (row["LowSuit"] == DBNull.Value) ? null : (bool?)row["LowSuit"],
                                LowSuitState: (row["LowSuitState"] == DBNull.Value) ? null : (bool?)row["LowSuitState"],
                                LowSuitAmount: (row["LowSuitAmount"] == DBNull.Value) ? null : (string)row["LowSuitAmount"],
                                PropertyConfiscationLibItemID: (row["PropertyConfiscationLibItemID"] == DBNull.Value) ? null : (int?)row["PropertyConfiscationLibItemID"],
                                PropertyConfiscationValue: (row["PropertyConfiscationValue"] == DBNull.Value) ? null : (string)row["PropertyConfiscationValue"],
                                Fine: (row["Fine"] == DBNull.Value) ? null : (string)row["Fine"],
                                PropertyConfiscationLibItemLabel: (row["PropertyConfiscationLibItemLabel"] == DBNull.Value) ? null : (string)row["PropertyConfiscationLibItemLabel"],

                                LowSuitPayAmount: (row["LowSuitPayAmount"] == DBNull.Value) ? null : (string)row["LowSuitPayAmount"],
                                FineHas: (row["FineHas"] == DBNull.Value) ? null : (bool?)row["FineHas"],
                                FinePay: (row["FinePay"] == DBNull.Value) ? null : (string)row["FinePay"],
                                FineState: (row["FineState"] == DBNull.Value) ? null : (bool?)row["FineState"],

                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );
                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<SentencingDataEntity>();
            }
        }
        public bool SP_Update_SentencingData(SentencingDataEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("SentencingDecisionLibItem_ID");
                ArrayParams.Add("SentencNumber");
                ArrayParams.Add("Judge");
                ArrayParams.Add("SentencingDate");
                ArrayParams.Add("SentencingStartDate");
                ArrayParams.Add("SentencingVerdictDate");
                ArrayParams.Add("CodeLibItem_ID");
                ArrayParams.Add("PenaltyDay");
                ArrayParams.Add("PenaltyMonth");
                ArrayParams.Add("PenaltyYear");
                ArrayParams.Add("SentencingEndDate");
                ArrayParams.Add("EarlyDate");
                ArrayParams.Add("EarlyType");
                ArrayParams.Add("SentenceArticleType");
                ArrayParams.Add("Description");
                ArrayParams.Add("CloseCaseID");
                ArrayParams.Add("OpenCaseID");
                ArrayParams.Add("Status");
                ArrayParams.Add("LowSuit");
                ArrayParams.Add("LowSuitState");
                ArrayParams.Add("LowSuitAmount");
                ArrayParams.Add("PropertyConfiscationLibItemID");
                ArrayParams.Add("PropertyConfiscationValue");
                ArrayParams.Add("Fine");
                ArrayParams.Add("PunishmentLibItemID");

                ArrayParams.Add("LowSuitPayAmount");
                ArrayParams.Add("FineHas");
                ArrayParams.Add("FinePay");
                ArrayParams.Add("FineState");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.SentencingDecisionLibItem_ID);
                ArrayValues.Add(entity.SentencNumber);
                ArrayValues.Add(entity.Judge);
                ArrayValues.Add(entity.SentencingDate);
                ArrayValues.Add(entity.SentencingStartDate);
                ArrayValues.Add(entity.SentencingVerdictDate);
                ArrayValues.Add(entity.CodeLibItem_ID);
                ArrayValues.Add(entity.PenaltyDay);
                ArrayValues.Add(entity.PenaltyMonth);
                ArrayValues.Add(entity.PenaltyYear);
                ArrayValues.Add(entity.SentencingEndDate);
                ArrayValues.Add(entity.EarlyDate);
                ArrayValues.Add(entity.EarlyType);
                ArrayValues.Add(entity.SentenceArticleType);
                ArrayValues.Add(entity.Description);
                ArrayValues.Add(entity.CloseCaseID);
                ArrayValues.Add(entity.OpenCaseID);
                ArrayValues.Add(entity.Status);
                ArrayValues.Add(entity.LowSuit);
                ArrayValues.Add(entity.LowSuitState);
                ArrayValues.Add(entity.LowSuitAmount);
                ArrayValues.Add(entity.PropertyConfiscationLibItemID);
                ArrayValues.Add(entity.PropertyConfiscationValue);
                ArrayValues.Add(entity.Fine);
                ArrayValues.Add(entity.PunishmentLibItemID);

                ArrayValues.Add(entity.LowSuitPayAmount);
                ArrayValues.Add(entity.FineHas);
                ArrayValues.Add(entity.FinePay);
                ArrayValues.Add(entity.FineState);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_SentencingData", ArrayValues, ArrayParams);

                
                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion

        #region SentencingDataArticleLibs
        public int? SP_Add_SentencingDataArticleLibs(SentencingDataArticleLibsEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrevConvictionDataID");
            ArrayParams.Add("SentencingDataID");
            ArrayParams.Add("ArrestDataID");
            ArrayParams.Add("LibItem_ID");

            ArrayValues.Add(entity.PrevConvictionDataID);
            ArrayValues.Add(entity.SentencingDataID);
            ArrayValues.Add(entity.ArrestDataID);
            ArrayValues.Add(entity.LibItem_ID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_SentencingDataArticleLibs", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<SentencingDataArticleLibsEntity> SP_GetList_SentencingDataArticleLibs(SentencingDataArticleLibsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID"); 
                ArrayParams.Add("SentencingDataID"); 
                ArrayParams.Add("ArrestDataID");
                ArrayParams.Add("PrevConvictionDataID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.SentencingDataID);
                ArrayValues.Add(entity.ArrestDataID);
                ArrayValues.Add(entity.PrevConvictionDataID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_SentencingDataArticleLibs", ArrayValues, ArrayParams);

                List<SentencingDataArticleLibsEntity> list = new List<SentencingDataArticleLibsEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SentencingDataArticleLibsEntity item = new SentencingDataArticleLibsEntity(
                                ID:(int)row["ID"],
                                SentencingDataID: (row["SentencingDataID"] == DBNull.Value) ? null : (int?)row["SentencingDataID"],
                                ArrestDataID: (row["ArrestDataID"] == DBNull.Value) ? null : (int?)row["ArrestDataID"],
                                PrevConvictionDataID: (row["PrevConvictionDataID"] == DBNull.Value) ? null : (int?)row["PrevConvictionDataID"],
                                LibItem_ID: (int)row["LibItem_ID"]
                            );

                        list.Add(item);
                    }
                }

                


                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<SentencingDataArticleLibsEntity>();
            }
        }
        public bool SP_Update_SentencingDataArticleLibs(SentencingDataArticleLibsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("SentencingDataID");
                ArrayParams.Add("ArrestDataID");
                ArrayParams.Add("PrevConvictionDataID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.SentencingDataID);
                ArrayValues.Add(entity.ArrestDataID);
                ArrayValues.Add(entity.PrevConvictionDataID);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_SentencingDataArticleLibs", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion

        #region SentencingDataOverdue
        public List<SentencingOverdueDashboardDataEntity> SP_GetList_OverdueForDashboard(int OrgUnitID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_OverdueForDashboard", ArrayValues, ArrayParams);

                List<SentencingOverdueDashboardDataEntity> list = new List<SentencingOverdueDashboardDataEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SentencingOverdueDashboardDataEntity item = new SentencingOverdueDashboardDataEntity(
                                (int)row["PrisonerID"],
                                (string)row["Personal_ID"],
                                (DateTime)row["SentencingStartDate"],
                                (DateTime)row["SentencingEndDate"],
                                (string)row["PrisonerName"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<SentencingOverdueDashboardDataEntity>();
            }
        }
        #endregion

        #region SentencingSevenDay
        public List<SentencingOverdueDashboardDataEntity> SP_GetList_SevenDayForDashboard(int OrgUnitID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_SevenDayForDashboard", ArrayValues, ArrayParams);

                List<SentencingOverdueDashboardDataEntity> list = new List<SentencingOverdueDashboardDataEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SentencingOverdueDashboardDataEntity item = new SentencingOverdueDashboardDataEntity(
                                (int)row["PrisonerID"],
                                (string)row["Personal_ID"],
                                (DateTime)row["SentencingStartDate"],
                                (DateTime)row["SentencingEndDate"],
                                (string)row["PrisonerName"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<SentencingOverdueDashboardDataEntity>();
            }
        }
        #endregion

        #region SentencingEarlyDate
        public List<SentencingOverdueDashboardDataEntity> SP_GetList_EarlyDate(int OrgUnitID)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("OrgUnitID");
                ArrayValues.Add(OrgUnitID);
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_EarlyDate", ArrayValues, ArrayParams);

                List<SentencingOverdueDashboardDataEntity> list = new List<SentencingOverdueDashboardDataEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        SentencingOverdueDashboardDataEntity item = new SentencingOverdueDashboardDataEntity(
                                (int)row["PrisonerID"],
                                (string)row["Personal_ID"],
                                (DateTime)row["SentencingStartDate"],
                                (DateTime)row["SentencingEndDate"],
                                (string)row["PrisonerName"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<SentencingOverdueDashboardDataEntity>();
            }
        }
        #endregion
    }
}
