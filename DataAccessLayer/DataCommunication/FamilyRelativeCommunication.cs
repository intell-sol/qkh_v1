﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_FamilyRelativesService:DataComm
    {
        private static DAL_FamilyRelativesService instance { set; get; }
        public static DAL_FamilyRelativesService getInstance()
        {
            if(instance == null)
            {
                instance = new DAL_FamilyRelativesService();
            }
            return instance;
        }
        public int? SP_Add_FamilyRelatives(FamilyRelativeEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("FamilyStatusID");
            

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add((int)entity.FamilyStatusID);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_FamilyRelatives", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<FamilyRelativeEntity> SP_GetList_FamilyRelatives(FamilyRelativeEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");
                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_FamilyRelatives", ArrayValues, ArrayParams);

                List<FamilyRelativeEntity> list = new List<FamilyRelativeEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        FamilyRelativeEntity item = new FamilyRelativeEntity(
                                ID:(int)row["ID"],
                                PrisonerID:(int)row["PrisonerID"],
                                FamilyStatusID:(FamilyStatus)row["FamilyStatusID"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<FamilyRelativeEntity>();
            }
        }
        public bool SP_Update_FamilyRelatives(FamilyRelativeEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("FamilyStatusID");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add((int)entity.FamilyStatusID);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_FamilyRelatives", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

    }
}
