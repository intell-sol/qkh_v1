﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_EducationsProfessionsService:DataComm
    {
        public int? SP_Add_EducationsProfessions(EducationsProfessionsEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();

            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("LibItemID");
            ArrayParams.Add("Note");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.LibItemID);
            ArrayValues.Add(entity.Note);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_EducationsProfessions", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }

        public List<EducationsProfessionsEntity> SP_GetList_EducationsProfessions(EducationsProfessionsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_EducationsProfessions", ArrayValues, ArrayParams);

                List<EducationsProfessionsEntity> list = new List<EducationsProfessionsEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        EducationsProfessionsEntity item = new EducationsProfessionsEntity(
                                ID:(int)row["ID"],
                                PrisonerID:(int)row["PrisonerID"],
                                LibItemID:(int)row["LibItemID"],
                                Note:(row["Note"] == DBNull.Value) ? null : (string)row["Note"],
                                MergeStatus:(row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<EducationsProfessionsEntity>();
            }
        }
        public bool SP_Update_EducationsProfessions(EducationsProfessionsEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("LibItemID");
                ArrayParams.Add("Note");
                ArrayParams.Add("Status");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.LibItemID);
                ArrayValues.Add(entity.Note);
                ArrayValues.Add(entity.Status);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_EducationsProfessions", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
