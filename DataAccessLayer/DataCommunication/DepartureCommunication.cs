﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataAccessLayer.DataCommunication
{
    public class DAL_DepartureService : DataComm
    {
        public int? SP_Add_Departure(DepartureEntity entity)
        {
            ArrayParams.Clear();
            ArrayValues.Clear();
            
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("TypeLibItemID");
            ArrayParams.Add("Place");
            ArrayParams.Add("MoveLibItemID");
            ArrayParams.Add("MedLibItemID");
            ArrayParams.Add("CaseLibItemID");
            ArrayParams.Add("InvestigateLibItemID");
            ArrayParams.Add("OrgUnitID");
            ArrayParams.Add("CourtLibItemID");
            ArrayParams.Add("HospitalLibItemID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("Duration");
            ArrayParams.Add("DuartionByHour");
            ArrayParams.Add("PurposeLibItemID");
            ArrayParams.Add("ApproverEmployeeID");
            ArrayParams.Add("State");

            ArrayValues.Add(entity.PrisonerID);
            ArrayValues.Add(entity.TypeLibItemID);
            ArrayValues.Add(entity.Place);
            ArrayValues.Add(entity.MoveLibItemID);
            ArrayValues.Add(entity.MedLibItemID);
            ArrayValues.Add(entity.CaseLibItemID);
            ArrayValues.Add(entity.InvestigateLibItemID);
            ArrayValues.Add(entity.OrgUnitID);
            ArrayValues.Add(entity.CourtLibItemID);
            ArrayValues.Add(entity.HospitalLibItemID);
            ArrayValues.Add(entity.StartDate);
            ArrayValues.Add(entity.EndDate);
            ArrayValues.Add(entity.Duration);
            ArrayValues.Add(entity.DuartionByHour);
            ArrayValues.Add(entity.PurposeLibItemID);
            ArrayValues.Add(entity.ApproverEmployeeID);
            ArrayValues.Add(entity.State);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Add_Departure", ArrayValues, ArrayParams);

                decimal decId = (decimal)dt.Rows[0][0];

                int newId = decimal.ToInt32(decId);

                return newId;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log.Error("", e);
                return null;
            }
        }
        public List<DepartureEntity> SP_GetList_Departure(DepartureEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("PrisonerID");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.PrisonerID);

                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_Departures", ArrayValues, ArrayParams);

                List<DepartureEntity> list = new List<DepartureEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        DepartureEntity item = new DepartureEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                TypeLibItemID: (int)row["TypeLibItemID"],
                                MoveLibItemID: (row["MoveLibItemID"] == DBNull.Value) ? null : (int?)row["MoveLibItemID"],
                                Place: (row["Place"] == DBNull.Value) ? null : (string)row["Place"],
                                CaseLibItemID: (row["CaseLibItemID"] == DBNull.Value) ? null : (int?)row["CaseLibItemID"],
                                MedLibItemID: (row["MedLibItemID"] == DBNull.Value) ? null : (int?)row["MedLibItemID"],
                                InvestigateLibItemID: (row["InvestigateLibItemID"] == DBNull.Value) ? null : (int?)row["InvestigateLibItemID"],
                                OrgUnitID: (row["OrgUnitID"] == DBNull.Value) ? null : (int?)row["OrgUnitID"],
                                ApproverEmployeeID: (row["ApproverEmployeeID"] == DBNull.Value) ? null : (int?)row["ApproverEmployeeID"],
                                CourtLibItemID: (row["CourtLibItemID"] == DBNull.Value) ? null : (int?)row["CourtLibItemID"],
                                HospitalLibItemID: (row["HospitalLibItemID"] == DBNull.Value) ? null : (int?)row["HospitalLibItemID"],
                                TypeLibItemLabel: (string)row["TypeLibItemLabel"],
                                Duration: (int)row["Duration"],
                                DuartionByHour: (int)row["DuartionByHour"],
                                StartDate: (DateTime)row["StartDate"],
                                EndDate: (row["EndDate"] != DBNull.Value) ? (DateTime?)row["EndDate"] : null,
                                PurposeLibItemID: (int)row["PurposeLibItemID"],
                                PurposeLibItemLabel: (string)row["PurposeLibItemLabel"],
                                MergeStatus: (row["MergeStatus"] == DBNull.Value) ? null : (bool?)row["MergeStatus"], 
                                State: (row["State"] == DBNull.Value) ? null : (bool?)row["State"], 
                                Status: (bool)row["Status"]
                            );

                        list.Add(item);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<DepartureEntity>();
            }
        }
        public bool SP_Update_Departure(DepartureEntity entity)
        {
            try
            {
                ArrayParams.Clear();
                ArrayValues.Clear();

                ArrayParams.Add("ID");
                ArrayParams.Add("TypeLibItemID");
                ArrayParams.Add("Place");
                ArrayParams.Add("MoveLibItemID");
                ArrayParams.Add("MedLibItemID");
                ArrayParams.Add("CaseLibItemID");
                ArrayParams.Add("InvestigateLibItemID");
                ArrayParams.Add("OrgUnitID");
                ArrayParams.Add("CourtLibItemID");
                ArrayParams.Add("HospitalLibItemID");
                ArrayParams.Add("StartDate");
                ArrayParams.Add("EndDate");
                ArrayParams.Add("Duration");
                ArrayParams.Add("DuartionByHour");
                ArrayParams.Add("PurposeLibItemID");
                ArrayParams.Add("ApproverEmployeeID");
                ArrayParams.Add("Status");
                ArrayParams.Add("State");

                ArrayValues.Add(entity.ID);
                ArrayValues.Add(entity.TypeLibItemID);
                ArrayValues.Add(entity.Place);
                ArrayValues.Add(entity.MoveLibItemID);
                ArrayValues.Add(entity.MedLibItemID);
                ArrayValues.Add(entity.CaseLibItemID);
                ArrayValues.Add(entity.InvestigateLibItemID);
                ArrayValues.Add(entity.OrgUnitID);
                ArrayValues.Add(entity.CourtLibItemID);
                ArrayValues.Add(entity.HospitalLibItemID);
                ArrayValues.Add(entity.StartDate);
                ArrayValues.Add(entity.EndDate);
                ArrayValues.Add(entity.Duration);
                ArrayValues.Add(entity.DuartionByHour);
                ArrayValues.Add(entity.PurposeLibItemID);
                ArrayValues.Add(entity.ApproverEmployeeID);
                ArrayValues.Add(entity.Status);
                ArrayValues.Add(entity.State);


                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_Update_Departure", ArrayValues, ArrayParams);


                if (dt != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public List<DepartureDashboardEntity> SP_GetList_DeparturesForDashboard(FilterDepartureEntity DepartureData)
        {
            // UNDONE: to this first and move it to EncouragementsCommunicatin
            ArrayParams.Clear();
            ArrayValues.Clear();

            int TotalCount = 0;

            ArrayParams.Add("FirstName");
            ArrayParams.Add("MiddleName");
            ArrayParams.Add("LastName");
            ArrayParams.Add("TypeLibItemID");
            ArrayParams.Add("PurposeLibItemID");
            ArrayParams.Add("StartDate");
            ArrayParams.Add("EndDate");
            ArrayParams.Add("Duration");
            ArrayParams.Add("DuartionByHour");
            ArrayParams.Add("State");
            ArrayParams.Add("PrisonerID");
            ArrayParams.Add("PrisonerType");
            ArrayParams.Add("OrgUnitIDList");
            ArrayParams.Add("CurrentPage");
            ArrayParams.Add("ViewCount");

            ArrayValues.Add(DepartureData.FirstName);
            ArrayValues.Add(DepartureData.MiddleName);
            ArrayValues.Add(DepartureData.LastName);
            ArrayValues.Add(DepartureData.TypeLibItemID);
            ArrayValues.Add(DepartureData.PurposeLibItemID);
            ArrayValues.Add(DepartureData.StartDate);
            ArrayValues.Add(DepartureData.EndDate);
            ArrayValues.Add(DepartureData.Duration);
            ArrayValues.Add(DepartureData.DuartionByHour);
            ArrayValues.Add(DepartureData.State);
            ArrayValues.Add(DepartureData.PrisonerID);
            ArrayValues.Add(DepartureData.PrisonerType);
            ArrayValues.Add((DepartureData.OrgUnitIDList != null && DepartureData.OrgUnitIDList.Any()) ? string.Join(",", DepartureData.OrgUnitIDList) : DepartureData.OrgUnitID.ToString());
            ArrayValues.Add(DepartureData.paging.CurrentPage);
            ArrayValues.Add(DepartureData.paging.ViewCount);


            ArrayList OutParams = new ArrayList();
            Dictionary<string, object> OutValues = new Dictionary<string, object>();

            OutParams.Add("TotalCount");
            OutValues.Add("TotalCount", TotalCount);

            try
            {
                DataTable dt = dtDataUtility.Exec_SP_Into_DataTable("SP_GetList_DeparturesForDashboard", ArrayValues, ArrayParams, ref OutValues, OutParams);

                List<DepartureDashboardEntity> list = new List<DepartureDashboardEntity>();
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        DepartureDashboardEntity item = new DepartureDashboardEntity(
                                ID: (int)row["ID"],
                                PrisonerID: (int)row["PrisonerID"],
                                TypeLibItemID: (int)row["TypeLibItemID"],
                                TypeLibItemLabel: (string)row["TypeLibItemLabel"],
                                Duration:(int)row["Duration"],
                                DuartionByHour: (int)row["DuartionByHour"], 
                                Place: (row["Place"] == DBNull.Value) ? null : (string)row["Place"],
                                State: (row["State"] == DBNull.Value) ? null : (bool?)row["State"],
                                StartDate: (row["StartDate"] == DBNull.Value) ? null : (DateTime?)row["StartDate"],
                                ApproverEmployeeName: (row["ApproverEmployeeName"] == DBNull.Value) ? null : (string)row["ApproverEmployeeName"],
                                ApproverEmployeeID: (row["ApproverEmployeeID"] == DBNull.Value) ? null : (int?)row["ApproverEmployeeID"],
                                EndDate: (row["EndDate"] == DBNull.Value) ? null : (DateTime?)row["EndDate"],
                                Status: (bool)row["Status"],
                                ArchiveStatus: (bool)row["ArchiveStatus"],
                                PurposeLibItemID: (int)row["PurposeLibItemID"],
                                PurposeLibItemLabel: (string)row["PurposeLibItemLabel"],
                                Personal_ID: (string)row["Personal_ID"],
                                PrisonerName: (string)row["PrisonerName"],
                                PrisonerType: (int)row["PrisonerType"]
                            );

                        list.Add(item);
                    }
                }

                DepartureData.paging.TotalCount = (int)OutValues["TotalCount"];
                DepartureData.paging.TotalPage = (int)Math.Ceiling(Convert.ToDouble(OutValues["TotalCount"]) / DepartureData.paging.ViewCount);
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                DepartureData.paging.TotalPage = 0;
                return new List<DepartureDashboardEntity>();
            }
        }
    }
}