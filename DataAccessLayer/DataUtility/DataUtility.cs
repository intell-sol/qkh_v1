﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Configuration;
using System.Transactions;
//using log4net;
using System.Reflection;
using System.Collections.Generic;

public class DataUtility
{
    //private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

    protected string m_ConnectionString;
    protected Dictionary<Type, DbType> typeMap;
    //public static string CnnString;
    //////////////////////////////////////////////////////////////////
    public DataUtility(string ConnectionString)
    {
        // m_ConnectionString = @"Data Source=SETASERVER\SQLEXPRESS;Initial Catalog=Inventory;User ID=sa;Password=123";
        m_ConnectionString = ConnectionString;
        typeMap = new Dictionary<Type, DbType>();
        typeMap[typeof(byte)] = DbType.Byte;
        typeMap[typeof(sbyte)] = DbType.SByte;
        typeMap[typeof(short)] = DbType.Int16;
        typeMap[typeof(ushort)] = DbType.UInt16;
        typeMap[typeof(int)] = DbType.Int32;
        typeMap[typeof(uint)] = DbType.UInt32;
        typeMap[typeof(long)] = DbType.Int64;
        typeMap[typeof(ulong)] = DbType.UInt64;
        typeMap[typeof(float)] = DbType.Single;
        typeMap[typeof(double)] = DbType.Double;
        typeMap[typeof(decimal)] = DbType.Decimal;
        typeMap[typeof(bool)] = DbType.Boolean;
        typeMap[typeof(string)] = DbType.String;
        typeMap[typeof(char)] = DbType.StringFixedLength;
        typeMap[typeof(Guid)] = DbType.Guid;
        typeMap[typeof(DateTime)] = DbType.DateTime;
        typeMap[typeof(DateTimeOffset)] = DbType.DateTimeOffset;
        typeMap[typeof(byte[])] = DbType.Binary;
        typeMap[typeof(byte?)] = DbType.Byte;
        typeMap[typeof(sbyte?)] = DbType.SByte;
        typeMap[typeof(short?)] = DbType.Int16;
        typeMap[typeof(ushort?)] = DbType.UInt16;
        typeMap[typeof(int?)] = DbType.Int32;
        typeMap[typeof(uint?)] = DbType.UInt32;
        typeMap[typeof(long?)] = DbType.Int64;
        typeMap[typeof(ulong?)] = DbType.UInt64;
        typeMap[typeof(float?)] = DbType.Single;
        typeMap[typeof(double?)] = DbType.Double;
        typeMap[typeof(decimal?)] = DbType.Decimal;
        typeMap[typeof(bool?)] = DbType.Boolean;
        typeMap[typeof(char?)] = DbType.StringFixedLength;
        typeMap[typeof(Guid?)] = DbType.Guid;
        typeMap[typeof(DateTime?)] = DbType.DateTime;
        typeMap[typeof(DateTimeOffset?)] = DbType.DateTimeOffset;
    }

    /////////////////////////////////////////////////////////////////
    #region Query Executers
    public DataTable RunQuery(string sQuery)
    {
        try
        {
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sQuery, m_ConnectionString);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            if (dataTable == null || dataTable.Rows.Count == 0)
                return null;
            return dataTable;
        }
        catch (Exception e)
        {
            System.Diagnostics.Debug.WriteLine(e.Message);
            return null;
        }

    }

    public DataTable Exec_SP_Into_DataTable(string sSP_Name, ArrayList Values, ArrayList Parameters)
    {
        try
        {
            SqlConnection conn = new SqlConnection(m_ConnectionString);
            conn.Open();
            // Create a data adapter to fill the DataSet
            SqlCommand cmd = new SqlCommand(sSP_Name, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            for (int i = 0; i < Values.Count; i++)
            {
                SqlParameter param = null;
                if (Values[i] != null && Values[i].GetType() == typeof(DateTime))
                {
                    param = new SqlParameter(Parameters[i].ToString(), SqlDbType.DateTime);
                    param.Direction = ParameterDirection.Input;
                    param.Value = Values[i];
                }
                else
                {
                    param = new SqlParameter(Parameters[i].ToString(), Values[i] == null ? (object)DBNull.Value : Values[i].ToString());
                    //param.Direction = ParameterDirection.InputOutput;
                }
                cmd.Parameters.Add(param);
            }
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter();
            // Set the data adapter’s select command
            da.SelectCommand = cmd;
            da.Fill(dt);
            String command = da.SelectCommand.ToString();
            if (dt == null)
            {
                conn.Close();
                conn = null;
                return null;
            }
            else if (dt.Rows.Count > 0)
            {
                conn.Close();
                conn = null;
                return dt;
            }
            else
            {
                conn.Close();
                conn = null;
                return dt;
            }
        }
        catch (Exception e)
        {


            Console.WriteLine(e);
            Console.WriteLine(e.Message);
            //log.Error("Exec_SP_Into_DataTable", e);
            return null;
        }

    }
    public DataTable Exec_SP_Into_DataTable(string sSP_Name, ArrayList Values, ArrayList Parameters, ref Dictionary<string, object> OutValues, ArrayList OutParameters)
    {
        try
        {
            SqlConnection conn = new SqlConnection(m_ConnectionString);
            conn.Open();
            // Create a data adapter to fill the DataSet
            SqlCommand cmd = new SqlCommand(sSP_Name, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            for (int i = 0; i < Values.Count; i++)
            {
                SqlParameter param = null;
                if (Values[i] != null && Values[i].GetType() == typeof(DateTime))
                {
                    param = new SqlParameter(Parameters[i].ToString(), SqlDbType.DateTime);
                    param.Direction = ParameterDirection.Input;
                    param.Value = Values[i];
                }
                else
                    param = new SqlParameter(Parameters[i].ToString(), Values[i] == null ? (object)DBNull.Value : Values[i].ToString());
                cmd.Parameters.Add(param);
            }
            //Output 
            for (int i = 0; i < OutParameters.Count; i++)
            {
                SqlParameter param = null;

                param = new SqlParameter(OutParameters[i].ToString(), typeMap[OutValues[OutParameters[i].ToString()].GetType()]);
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);
            }
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter();
            // Set the data adapter’s select command
            da.SelectCommand = cmd;
            da.Fill(dt);
            String command = da.SelectCommand.ToString();
            try
            {
                //OutValues.Clear();
                for (int i = 0; i < OutParameters.Count; i++)
                    OutValues[OutParameters[i].ToString()] = cmd.Parameters[OutParameters[i].ToString()].Value;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            if (dt == null)
            {
                conn.Close();
                conn = null;
                return null;
            }
            else if (dt.Rows.Count > 0)
            {
                conn.Close();
                conn = null;
                return dt;
            }
            else
            {
                conn.Close();
                conn = null;
                return dt;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            Console.WriteLine(e.Message);
            //log.Error("Exec_SP_Into_DataTable", e);
            return null;
        }

    }
    public DataSet Exec_SP_Into_DataSet(string sSP_Name, ArrayList Values, ArrayList Parameters) //TODO cr by And
    {
        try
        {
            SqlConnection conn = new SqlConnection(m_ConnectionString);
            conn.Open();
            // Create a data adapter to fill the DataSet
            SqlCommand cmd = new SqlCommand(sSP_Name, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            for (int i = 0; i < Values.Count; i++)
            {
                SqlParameter param = null;
                if (Values[i] != null && Values[i].GetType() == typeof(DateTime))
                {
                    param = new SqlParameter(Parameters[i].ToString(), SqlDbType.DateTime);
                    param.Direction = ParameterDirection.Input;
                    param.Value = Values[i];
                }
                else
                {
                    param = new SqlParameter(Parameters[i].ToString(), Values[i] == null ? (object)DBNull.Value : Values[i].ToString());
                    //param.Direction = ParameterDirection.InputOutput;
                }
                cmd.Parameters.Add(param);
            }
            DataSet dt = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            // Set the data adapter’s select command
            da.SelectCommand = cmd;
            da.Fill(dt);
            String command = da.SelectCommand.ToString();
            if (dt == null)
            {
                conn.Close();
                conn = null;
                return null;
            }
            else if (dt.Tables[0].Rows.Count > 0)
            {
                conn.Close();
                conn = null;
                return dt;
            }
            else
            {
                conn.Close();
                conn = null;
                return dt;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            Console.WriteLine(e.Message);
            //log.Error("Exec_SP_Into_DataTable", e);
            return null;
        }

    }
    public SqlDataReader Exec_SP_Into_Reader(string sSP_Name, ArrayList Values, ArrayList Parameters)
    {
        try
        {
            SqlConnection conn = new SqlConnection(m_ConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand(sSP_Name, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            for (int i = 0; i < Values.Count; i++)
            {
                cmd.Parameters.Add(new SqlParameter(Parameters[i].ToString(), Values[i].ToString()));
            }
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader == null || (!reader.HasRows))
            {
                conn.Close();
                return null;
            }
            else
            {
                return reader;
            }
        }
        catch (Exception e)
        {
            System.Diagnostics.Debug.WriteLine(e.Message);
            return null;
        }

    }

    public Object Exec_SP_Into_Scalar(string sSP_Name, ArrayList Values, ArrayList Parameters)
    {
        try
        {
            object obj = null;
            SqlConnection conn = new SqlConnection(m_ConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand(sSP_Name, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            for (int i = 0; i < Values.Count; i++)
            {
                cmd.Parameters.Add(new SqlParameter(Parameters[i].ToString(), Values[i].ToString()));
            }
            obj = cmd.ExecuteScalar();
            if (obj == DBNull.Value)
            {
                conn.Close();
                conn.Close();
                return null;
            }
            else
            {
                conn.Close();
                conn.Close();
                return obj;
            }
        }
        catch (Exception e)
        {
            System.Diagnostics.Debug.WriteLine(e.Message);
            return null;
        }

    }

    public void Exec_SP_NonQuery(string sSP_Name, ArrayList Values, ArrayList Parameters)
    {
        try
        {
            SqlConnection conn = new SqlConnection(m_ConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand(sSP_Name, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            for (int i = 0; i < Values.Count; i++)
            {
                if (Parameters[i].ToString() != "@LMD")
                {
                    cmd.Parameters.Add(new SqlParameter(Parameters[i].ToString(), Values[i].ToString()));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter(Parameters[i].ToString(), Convert.ToDateTime(Values[i].ToString())));
                }
            }
            cmd.ExecuteNonQuery();
            conn.Close();
            conn = null;
        }
        catch (Exception e)
        {
            System.Diagnostics.Debug.WriteLine(e.Message);

        }

    }

    /*
            public SqlDataReader ExecReader(string sQuery)
            {
                try
                {
                    SqlConnection conn = new SqlConnection(m_ConnectionString);
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(sQuery, conn);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader == null || (!reader.HasRows))
                    {
                        conn.Close();
                        return null;
                    }
                    else
                    {
                        conn.Close();
                        return reader;
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    return null;
                }

            }
    */
    public object ExecuteScalar(string sQuery)
    {
        SqlConnection connection = null;
        object obj = null;
        try
        {
            connection = new SqlConnection(m_ConnectionString);
            connection.Open();
            //////////////////////////////////////////////////////////////////////////			
            SqlCommand command = new SqlCommand(sQuery, connection);
            obj = command.ExecuteScalar();
            if (obj == DBNull.Value)
            {
                connection.Close();
                return null;
            }
            connection.Close();
            return obj;
        }
        catch (Exception e)
        {
            connection.Close();
            System.Diagnostics.Debug.WriteLine(e.Message);
            return null;
        }
        finally
        {
            if (connection != null)
                connection.Close();
        }
    }
    #endregion
    //////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////
    #region Table Work Flow
    public string GetMinValue(DataTable dT, string column)
    {
        DataRow[] dR = dT.Select(column + "=MIN(" + column + ")");
        return dR[0][column].ToString();

    }

    public string GetMaxValue(DataTable dT, string column)
    {
        DataRow[] dR = dT.Select(column + "=MAX(" + column + ")");
        return dR[0][column].ToString();

    }

    public string GetSumValue(DataTable dT, string column)
    {
        DataRow[] dR = dT.Select(column + "=SUM(" + column + ")");
        if (dR != null)
            return dR[0][column].ToString();
        else
            return "";

    }

    public int GetRowsCount(DataTable dT, string filter)
    {
        DataRow[] dR = dT.Select(filter);
        return dR.Length;

    }

    public DataTable AddRowInTable(DataTable dTSours, DataRow[] dRows)
    {
        DataTable dT = new DataTable();
        for (int i = 0; i < dTSours.Columns.Count; i++)
        {
            dT.Columns.Add(dTSours.Columns[i].Caption);
        }
        foreach (DataRow dR in dRows)
        {
            dT.Rows.Add(dR.ItemArray);
        }
        return dT;
    }

    public DataTable SelectDistinct(DataTable SourceTable, params string[] FieldNames)
    {
        object[] lastValues;
        DataTable newTable;
        DataRow[] orderedRows;

        if (FieldNames == null || FieldNames.Length == 0)
            throw new ArgumentNullException("FieldNames");

        lastValues = new object[FieldNames.Length];
        newTable = new DataTable();

        foreach (string fieldName in FieldNames)
            newTable.Columns.Add(fieldName, SourceTable.Columns[fieldName].DataType);

        orderedRows = SourceTable.Select("", string.Join(", ", FieldNames));

        foreach (DataRow row in orderedRows)
        {
            if (!fieldValuesAreEqual(lastValues, row, FieldNames))
            {
                newTable.Rows.Add(createRowClone(row, newTable.NewRow(), FieldNames));

                setLastValues(lastValues, row, FieldNames);
            }
        }

        return newTable;
    }

    private bool fieldValuesAreEqual(object[] lastValues, DataRow currentRow, string[] fieldNames)
    {
        bool areEqual = true;

        for (int i = 0; i < fieldNames.Length; i++)
        {
            if (lastValues[i] == null || !lastValues[i].Equals(currentRow[fieldNames[i]]))
            {
                areEqual = false;
                break;
            }
        }

        return areEqual;
    }

    private DataRow createRowClone(DataRow sourceRow, DataRow newRow, string[] fieldNames)
    {
        foreach (string field in fieldNames)
            newRow[field] = sourceRow[field];

        return newRow;
    }

    private void setLastValues(object[] lastValues, DataRow sourceRow, string[] fieldNames)
    {
        for (int i = 0; i < fieldNames.Length; i++)
            lastValues[i] = sourceRow[fieldNames[i]];
    }
    #endregion //Table Distinct
    //////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////
    #region Work With String
    public string dictionary_KeyWithLngToText(string key, string lng)
    {
        ArrayList val = new ArrayList();
        ArrayList pName = new ArrayList();
        pName.Add("@key");
        val.Add(key.ToString());
        DataTable dt = Exec_SP_Into_DataTable("Dictionary_GetLabelTextByKeyandlng", val, pName);
        //Label lbl=Page.FindControl(LabelID);
        string txt = "";
        if (dt != null)
        {
            DataView dw = new DataView();
            dw = dt.DefaultView;
            txt = dw[0]["caption_" + lng.ToString()].ToString();
        }
        else
        {
            txt = "";
        }
        return (string)txt;
    }
    public int[] intsStrToArray(string src)
    {
        //string src=strSource.Text;
        int pos;
        int length = src.Length;
        int i = 0;
        int[] array_of_elements = new int[100];
        while (src.IndexOf(",", 0) != 0)
        {
            pos = src.IndexOf(",", 0);
            if (pos == -1) break;
            //questionID_array.Text=questionID_array.Text+src.Substring(0,pos);
            array_of_elements[i] = Convert.ToInt32(src.Substring(0, pos));
            i++;
            src = src.Substring(pos + 1, length - pos - 1);
            length = src.Length;
        }
        return (int[])array_of_elements;
    }
    #endregion
    /////////////////////////////////////////////////////////////

}

public sealed class TransactionContext : IDisposable
{
    private bool disposed;
    private TransactionScope scope;

    public void Begin()
    {
        if (this.disposed) throw new ObjectDisposedException("TransactionContext");
        this.scope = new TransactionScope();
    }

    public void Submit()
    {
        if (this.disposed) throw new ObjectDisposedException("TransactionContext");
        if (this.scope == null) throw new InvalidOperationException();
        this.scope.Complete();
    }

    public void Dispose()
    {
        if (this.scope != null)
        {
            this.scope.Dispose();
            this.scope = null;
        }
        this.disposed = true;
    }
}