﻿using CommonLayer.BusinessEntities;
using DataAccessLayer.DataCommunication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BusinessLayer.BusinessServices
{
    public class BL_SystemUsersAuthentication : BusinessServicesCommunication
    {
        public BESystemUser SystemUserLogIn(string UserName, string Password)
        {
            BESystemUser SysUser = DAL_SystemUsersAuthentification.SP_Login(UserName, Password);
            if (SysUser != null)
            {
                return SysUser;
            }
            return null;
        }
    }
}
