﻿using DataAccessLayer.DataCommunication;

namespace BusinessLayer.BusinessServices
{
    public class BusinessServicesCommunication
    {
        // back
        protected DAL_SystemUsersAuthentification DAL_SystemUsersAuthentification = new DAL_SystemUsersAuthentification();
        protected DAL_LibsServices DAL_LibsServices = new DAL_LibsServices();
        protected DAL_OrganizationServices DAL_OrgsServices = new DAL_OrganizationServices();
        protected DAL_PermissionServices DAL_PermServices = new DAL_PermissionServices();
        protected DAL_PropsServices DAL_PropsServices = new DAL_PropsServices();
        protected DAL_OrderServices DAL_OrderServices = new DAL_OrderServices();
        protected DAL_NationalityService DAL_NationalityService = new DAL_NationalityService();

        // front Data Access Layer
        protected DAL_LogActionService DAL_LogActionService = new DAL_LogActionService();
        protected DAL_UsersAuthentification DAL_UsersAuthentification = new DAL_UsersAuthentification();
        protected DAL_FileService DAL_FileService = new DAL_FileService();
        protected DAL_PrisonersService DAL_PrisionersService = new DAL_PrisonersService();
        protected DAL_PersonsService DAL_PersonsService = new DAL_PersonsService();
        protected DAL_PhysicalDataService DAL_PhysicalDataService = new DAL_PhysicalDataService(); 
        protected DAL_HeightWeightService DAL_HeightWeightService = new DAL_HeightWeightService(); 
        protected DAL_LeaningService DAL_LeaningService = new DAL_LeaningService();
        protected DAL_InvalidsService DAL_InvalidsService = new DAL_InvalidsService();
        protected DAL_IdentificationDocumentsService DAL_IdentificationDocumentsService = new DAL_IdentificationDocumentsService();
        protected DAL_PreviousConvictionsService DAL_PreviousConvictionsService = new DAL_PreviousConvictionsService();
        protected DAL_FingerprintsTattoosScarsService DAL_FingerprintsTattoosScarsService = new DAL_FingerprintsTattoosScarsService();
        protected DAL_ArmyService DAL_ArmyService = new DAL_ArmyService();
        protected DAL_ArmyAwardsService DAL_ArmyAwardsService = new DAL_ArmyAwardsService();
        protected DAL_EducationsProfessionsService DAL_EducationsProfessionsService = new DAL_EducationsProfessionsService();
        protected DAL_ProfessionsService DAL_ProfessionsService = new DAL_ProfessionsService();
        protected DAL_InterestsService DAL_InterestsService = new DAL_InterestsService();
        protected DAL_AbilitiesSkillsService DAL_AbilitiesSkillsService = new DAL_AbilitiesSkillsService();
        protected DAL_LanguagesService DAL_LanguagesService = new DAL_LanguagesService();
        protected DAL_UniversityDegreeService DAL_UniversityDegreeService = new DAL_UniversityDegreeService();
        protected DAL_CameraCardService DAL_CameraCardService = new DAL_CameraCardService();
        protected DAL_ItemsService DAL_ItemsService = new DAL_ItemsService();
        protected DAL_InjunctionsService DAL_InjunctionsService = new DAL_InjunctionsService();
        protected DAL_NotServedService DAL_NotServedService = new DAL_NotServedService();
        protected DAL_ArrestDataService DAL_ArrestDataService = new DAL_ArrestDataService();
        protected DAL_PrisonAccessProtocolService DAL_PrisonAccessProtocolService = new DAL_PrisonAccessProtocolService();
        protected DAL_SentencingDataService DAL_SentencingDataService = new DAL_SentencingDataService();
        protected DAL_WarInvolvedService DAL_WarInvolvedService = new DAL_WarInvolvedService();
        protected DAL_SpousesService DAL_SpousesService = new DAL_SpousesService();
        protected DAL_ChildrenService DAL_ChildrenService = new DAL_ChildrenService();
        protected DAL_ParentsService DAL_ParentsService = new DAL_ParentsService();
        protected DAL_SisterBrotherService DAL_SisterBrotherService = new DAL_SisterBrotherService();
        protected DAL_OtherRelativesService DAL_OtherRelativesService = new DAL_OtherRelativesService();
        protected DAL_FamilyRelativesService DAL_FamilyRelativesService = new DAL_FamilyRelativesService();
        protected DAL_CloseCaseService DAL_CloseCaseService = new DAL_CloseCaseService();
        protected DAL_CaseOpenService DAL_CaseOpenService = new DAL_CaseOpenService();
        protected DAL_EncouragementService DAL_EncouragementService = new DAL_EncouragementService();
        protected DAL_ArchiveService DAL_ArchiveService = new DAL_ArchiveService();
        protected DAL_PenaltiesService DAL_PenaltiesService = new DAL_PenaltiesService();
        protected DAL_OfficialVisitsService DAL_OfficialVisitsService = new DAL_OfficialVisitsService();
        protected DAL_PersonalVisitsCommunication DAL_PersonalVisitsService = new DAL_PersonalVisitsCommunication();
        protected DAL_EducationalCoursesService DAL_EducationalCoursesService = new DAL_EducationalCoursesService();
        protected DAL_WorkLoadsService DAL_WorkLoadsService = new DAL_WorkLoadsService(); 
        protected DAL_PackageService DAL_PackageService = new DAL_PackageService();
        protected DAL_DepartureService DAL_DepartureService = new DAL_DepartureService();
        protected DAL_TransferService DAL_TransferService = new DAL_TransferService();
        protected DAL_AddressService DAL_AddressService = new DAL_AddressService();
        protected DAL_MedicalService DAL_MedicalService = new DAL_MedicalService();
        protected DAL_AmnestyService DAL_AmnestyService = new DAL_AmnestyService();
        protected DAL_ConflictsService DAL_ConflictsService = new DAL_ConflictsService();
        protected DAL_ReportService DAL_ReportService = new DAL_ReportService();
        protected DAL_MergeService DAL_MergeService = new DAL_MergeService();
        protected DAL_PrisonerTypeStatusService DAL_PrisonerTypeStatusService = new DAL_PrisonerTypeStatusService();
        protected DAL_PrisonerPunishmentTypeService DAL_PrisonerPunishmentTypeService = new DAL_PrisonerPunishmentTypeService();
    }
}