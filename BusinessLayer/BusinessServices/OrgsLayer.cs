﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLayer.BusinessEntities;

namespace BusinessLayer.BusinessServices
{
    public class BL_OrganizatonLayer : BusinessServicesCommunication
    {
        public OrgUnitEntity GetOrgUnit(int? ID = null, int? ParentID = null)
        {
            List<OrgUnitEntity> orgUnitList = DAL_OrgsServices.SP_GetList_OrganizationUnit(ID, ParentID);
            if (orgUnitList != null)
            {
                OrgUnitEntity entity = null;
                if (ParentID != null)
                {
                    entity = orgUnitList.Find(item => item.ParentID == ParentID);
                    entity.subItems = this.ClassifyOrgUnits(orgUnitList, entity.ID);
                }
                else
                    entity = orgUnitList.Find(item => item.ID == ID);

                
                return entity;
            }
            return null;
        }


        public List<OrgUnitEntity> GetOrgUnitTree(int? ID = null, int? ParentID = 0)
        {
            List<OrgUnitEntity> units = DAL_OrgsServices.SP_GetList_OrganizationUnit(ID, ParentID);

            List<OrgUnitEntity> positions = new List<OrgUnitEntity>();

            foreach (OrgUnitEntity item in units)
            {
                if (item.TypeID == 4) positions.Add(item);
            }

            positions = ClassifyOrgUnit(positions);

            List<Tuple<int, OrgUnitEntity>> tupleList = new List<Tuple<int, OrgUnitEntity>>();

            List<OrgUnitEntity> changeList = new List<OrgUnitEntity>();

            foreach (OrgUnitEntity item in positions)
            {
                OrgUnitEntity currentParent = units.FindAll(r => r.ID == item.ParentID).First();

                if (currentParent.BindPositionID != null) tupleList.Add(new Tuple<int, OrgUnitEntity>(currentParent.BindPositionID.Value, item.ShallowCopy()));

                if (units.FindAll(r => r.ParentID == currentParent.ParentID && r.TypeID == 4).Any())
                {
                    changeList.Add(item.ShallowCopy());
                    item.ParentID = units.FindAll(r => r.ParentID == currentParent.ParentID && r.TypeID == 4).First().ID;
                    item.Label = currentParent.Label + " - " + item.Label;
                }
                else if (changeList.FindAll(r => r.ParentID == currentParent.ParentID && r.TypeID == 4).Any())
                {
                    changeList.Add(item.ShallowCopy());
                    item.ParentID = changeList.FindAll(r => r.ParentID == currentParent.ParentID && r.TypeID == 4).First().ID;
                    item.Label = currentParent.Label + " - " + item.Label;
                }

            }

            positions = UnClassifyOrgUnit(positions);

            foreach (Tuple<int, OrgUnitEntity> tuple in tupleList)
            {
                if (positions.FindAll(r=> r.ID == tuple.Item2.ID).Any())
                {
                    positions.FindAll(r => r.ID == tuple.Item2.ID).First().ParentID = tuple.Item1;
                }
            }

            positions = ClassifyOrgUnit(positions);

            return positions;
        }


        private List<OrgUnitEntity> UnClassifyOrgUnit(List<OrgUnitEntity> List)
        {
            List<OrgUnitEntity> finalList = new List<OrgUnitEntity>();

            foreach (OrgUnitEntity item in List)
            {
                if (item.subItems.Any())
                {
                    finalList = finalList.Concat(UnClassifyOrgUnit(item.subItems)).ToList();
                }
                item.subItems = new List<OrgUnitEntity>();
                finalList.Add(item);
            }

            return finalList;

        }

        private List<OrgUnitEntity> ClassifyOrgUnit(List<OrgUnitEntity> List)
        {
            List<OrgUnitEntity> removeList = new List<OrgUnitEntity>();

            foreach (OrgUnitEntity item in List)
            {
                Console.WriteLine(item.Label);
                if (List.FindAll(r=>r.ID == item.ParentID).Any())
                {
                    List.FindAll(r => r.ID == item.ParentID).First().subItems.Add(item);
                    removeList.Add(item);
                }
            }

            foreach (OrgUnitEntity item in removeList)
            {
                List.RemoveAll(r => r.ID == item.ID);
            }

            return List;

        }

        public OrgUnitEntity GetAvailableOrgUnits()
        {
            List<OrgUnitEntity> orgUnitList = DAL_OrgsServices.SP_GetList_AvailableOrganizationUnits();
            if (orgUnitList != null)
            {
                OrgUnitEntity entity = null;
                
                entity = orgUnitList.Find(item => item.ParentID == 0);
                entity.subItems = this.ClassifyOrgUnits(orgUnitList, entity.ID);
            
                return entity;
            }
            return null;
        }

        public List<Tuple<int, String>> GetStates()
        {
            List<Tuple<int, String>> States = DAL_OrgsServices.SP_GetList_OrganizationStates();

            return States;
        }

        public List<OrgUnitEntity> GetOrgUnitByTypeID(int TypeID = 3)
        {
            List<OrgUnitEntity> orgUnitList = DAL_OrgsServices.SP_GetList_OrganizationUnitByTypeID(TypeID);
            
            return orgUnitList;
        }
        public List<OrgUnitEntity> GetBindPositions(int OrgUnitID)
        {
            List<OrgUnitEntity> orgUnitList = DAL_OrgsServices.SP_GetList_OrganizationBindPositions(OrgUnitID);

            return orgUnitList;
        }

        public List<OrgUnitEntity> GetAvailablePositions(int OrgUnitID)
        {
            return DAL_OrgsServices.SP_GetList_AvailablePositions(OrgUnitID);
        }

        public List<OrgUnitEntity> GetPositionsByOrgUnitID(int OrgUnitID)
        {
            return DAL_OrgsServices.SP_GetList_PositionsByOrgUnitID(OrgUnitID);
        }

        public OrgUnitEntity GetOrgUnit(int ID)
        {
            List<OrgUnitEntity> orgUnitList = DAL_OrgsServices.SP_GetList_OrganizationUnitByID(ID);
            if (orgUnitList != null)
            {
                OrgUnitEntity entity = orgUnitList.First();
                entity.subItems = this.ClassifyOrgUnits(orgUnitList, entity.ID);
                return entity;
            }
            return null;
        }

       
        public OrgUnitEntity GetApproveOrgUnit(int OrgUnitID)
        {
            List<OrgUnitEntity> orgUnitList = DAL_OrgsServices.SP_GetList_ApproveOrganizationUnitByID(OrgUnitID);
            if (orgUnitList != null && orgUnitList.Any())
            {
                OrgUnitEntity entity = orgUnitList.First();
                entity.subItems = this.ClassifyOrgUnits(orgUnitList, entity.ID);
                return entity;
            }

            return new OrgUnitEntity();
        }

        public int AddOrgUnit(int ParentID, int TypeID, bool Single, string Label = null, string Address = null, string Phone = null, string Fax = null, string Email = null, int? StateID = null, int? BindPositionID = null)
        {
            int? result = DAL_OrgsServices.SP_Add_OrganizationUnit(ParentID, TypeID,  Single, Label, Address, Phone, Fax, Email, StateID, BindPositionID);
            return result != null ? Convert.ToInt32(result) : 0;
        }

        public bool UpdateOrgUnit(int ID, int ParentID, int TypeID, bool Single, string Label = null, string Address = null, string Phone = null, string Fax = null, string Email = null, bool? Status = null, int? StateID = null, int? BindPositionID = null)
        {
            int? result = DAL_OrgsServices.SP_Update_OrganizationUnit(ID, Label, Address, Phone, Fax, Email, Status, StateID, ParentID, TypeID, Single, BindPositionID);
            return result != null;
        }

        public bool UpdateOrgUnitLevel(OrgUnitEntity item)
        {
            return DAL_OrgsServices.SP_Update_OrganizationUnitLevel(item);
        }

        public bool DeleteOrgUnit(int OrgUnitID)
        {
            int? result = DAL_OrgsServices.SP_Update_OrganizationUnit(OrgUnitID, null, null, null, null, null, false, null, null, null, null);
            return result != null && result == 0;
        }

        public int AddPositionToOrg(int PositionID, int OrgUnitID)
        {
            int? result = DAL_OrgsServices.SP_Add_PositionToOrganisation(PositionID, OrgUnitID);
            return result != null ? Convert.ToInt32(result) : 0;
        }

        public void UpdatePositionToOrg(int? ID = null, int? PositionID = null, bool Status = true)
        {
            DAL_OrgsServices.SP_Update_PositionToOrganization(ID, PositionID, Status);
        }

        public EmployeeEntity GetEmployee(int EmployeeID)
        {
            List<EmployeeEntity> employees = DAL_OrgsServices.SP_GetList_Employees(EmployeeID, null, null, null, null, null);
            if(employees.Any())
            {
                return employees.First();
            }

            return null;
        }

        public List<EmployeeEntity> GetEmployees(int OrgUnitID)
        {
            return DAL_OrgsServices.SP_GetList_Employees(null, null, null, null, OrgUnitID, true);
        }

        public List<EmployeeEntity> GetEmployeeByPSN(string PSN, bool status)
        {
            return DAL_OrgsServices.SP_GetList_Employees(null, PSN, null, null, null, status);
        }

        public List<EmployeeEntity> GetOrganizationEmployees(int OrgUnitID)
        {
            return DAL_OrgsServices.SP_GetList_OrganizationEmployees(OrgUnitID);
        }

        public int AddEmployee(int OrgUnitID, string PSN, string FirstName, string MiddleName, string LastName, DateTime BirthDay, string Passport, string Login, string Password, String Phone, String Fax, String Email, String RegistrationAddress, String LivingAddress, String Nationality, DateTime PassportDate, String PassportBy, int? fileID)
        {
            List<EmployeeEntity> matchedEmployees = DAL_OrgsServices.SP_GetList_Employees(null, PSN, null, null, null, false);
            if(matchedEmployees.Count > 0)
            {
                int? result = DAL_OrgsServices.SP_Update_Employees(null, matchedEmployees[0].PSN, OrgUnitID, FirstName, MiddleName, LastName,
                                                                        BirthDay, Passport, Login, Password, Phone, Fax, Email, null, true, RegistrationAddress, LivingAddress, Nationality, PassportDate, PassportBy, fileID);

                return matchedEmployees[0].ID;
            }
            else
            {
                int? result = DAL_OrgsServices.SP_Add_Employees(OrgUnitID, PSN, FirstName, MiddleName, LastName, 
                                                                    BirthDay, Passport, Login, Password, Phone, Fax, Email, RegistrationAddress, LivingAddress, Nationality, PassportDate, PassportBy, fileID);
                return result == null ? 0 : result.Value;
            }
        }

        public bool UpdateEmployee(int EmployeeID, string FirstName, string MiddleName, string LastName, DateTime? BirthDay, string Passport, string Login, string Password, String Phone, String Fax, String Email, String RegistrationAddress, String LivingAddress, String Nationality, DateTime? PassportDate, String PassportBy, int? fileID)
        {
            if (Password != null)
                if (Password == "" || Password.Length == 0) Password = null;
            int? result = DAL_OrgsServices.SP_Update_Employees(EmployeeID, null, null, FirstName, MiddleName, LastName, BirthDay, Passport, Login, Password, Phone, Fax, Email, null, null, RegistrationAddress, LivingAddress, Nationality, PassportDate, PassportBy, fileID);
            return result != null && result == 0;
        }

        public bool DeleteEmployee(int EmployeeID)
        {
            int? result = DAL_OrgsServices.SP_Update_Employees(EmployeeID, null, null, null, null, null, null, null, null, null, null, null, null, null, false, null, null, null, null, null, null);
            return result != null && result == 0;
        }

        public Boolean CheckEmployeeUserName(string Login, int? UserID)
        {
            return DAL_OrgsServices.SP_Check_UserName(Login, UserID);
        }

        public List<OrgTypeEntity> GetOrgTypes(int? TypeID = null)
        {
            string Types = null;
                List<int> TypeList = new List<int>();
                switch (TypeID)
                {
                    case 0:
                        TypeList.Add(1);
                        break;
                    case 1:
                        TypeList.Add(2);
                        TypeList.Add(4);
                        TypeList.Add(5);
                        break;
                    case 2:
                        TypeList.Add(3);
                        TypeList.Add(4);
                        TypeList.Add(5);
                        break;
                    case 3:
                        TypeList.Add(4);
                        TypeList.Add(5);
                        break;
                    case 4:
                        TypeList.Add(4);
                        break;
                    case 5:
                        TypeList.Add(4);
                        break;
                default:
                    TypeList.Add(1);
                    TypeList.Add(2);
                    TypeList.Add(3);
                    TypeList.Add(4);
                    TypeList.Add(5);
                    break;
                }

                Types = string.Join(",", TypeList);

            return DAL_OrgsServices.SP_GetList_OrganizationTypes(Types);
        }

        private List<OrgUnitEntity> ClassifyOrgUnits(List<OrgUnitEntity> OrgUnitList, int parentID)
        {
            List<OrgUnitEntity> list = new List<OrgUnitEntity>();
            //List<OrgUnitEntity> currentLibsList = OrgUnitList.FindAll(r => (r.ParentID == parentID) && (r.TypeID == 4));
            List<OrgUnitEntity> currentLibsList = OrgUnitList.FindAll(r => (r.ParentID == parentID));
            currentLibsList = currentLibsList.OrderBy(x => x.SortLevel).ToList();

            if (currentLibsList.Any())
            {
                foreach (OrgUnitEntity currentLib in currentLibsList)
                {
                    List<OrgUnitEntity> tempLibsList = OrgUnitList.FindAll(r => r.ParentID == currentLib.ID);
                    if (tempLibsList.Any())
                    {
                        currentLib.subItems = this.ClassifyOrgUnits(OrgUnitList, currentLib.ID);
                    }
                    list.Add(currentLib);
                }
            }

            //currentLibsList = OrgUnitList.FindAll(r => (r.ParentID == parentID) && (r.TypeID != 4));
            //currentLibsList.OrderBy(x => x.SortLevel);
            //if (currentLibsList.Any())
            //{
            //    foreach (OrgUnitEntity currentLib in currentLibsList)
            //    {
            //        List<OrgUnitEntity> tempLibsList = OrgUnitList.FindAll(r => r.ParentID == currentLib.ID);
            //        if (tempLibsList.Any())
            //        {
            //            currentLib.subItems = this.ClassifyOrgUnits(OrgUnitList, currentLib.ID);
            //        }
            //        list.Add(currentLib);
            //    }
            //}
            return list;
        }

    }
}
