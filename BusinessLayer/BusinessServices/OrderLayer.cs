﻿using CommonLayer.BusinessEntities;
//using DataAccessLayer.DataCommunication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessServices
{
    public class BL_OrderLayer : BusinessServicesCommunication
    {
        /// <summary>
        /// Get Property List
        /// </summary>
        public Boolean AddOrder(int EmployeeID, int OrderTypeID, string OrderNumber, string OrderBy, DateTime OrderDate, int FileID)
        {
            OrderEntity curEntity = new OrderEntity(EmployeeID, OrderTypeID, OrderNumber, OrderBy, OrderDate, FileID);

            return DAL_OrderServices.SP_Add_EmployeeOrder(curEntity);
        }

    }
}
