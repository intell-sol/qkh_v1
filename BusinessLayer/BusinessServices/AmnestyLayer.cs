﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessServices
{
    public class BL_AmnestyLayer : BusinessServicesCommunication
    {
        #region Add
        public int? AddAmnesty(AmnestyEntity entity)
        {
            int? id = DAL_AmnestyService.SP_Add_Amnesty(entity);

            List<AmnestyPointsEntity> newList = new List<AmnestyPointsEntity>();
            if (entity.AmnestyPoints != null && entity.AmnestyPoints.Any())
            {
                newList = ClassifyPoint(entity.AmnestyPoints);
            }
            if (id != null)
            {
                foreach (AmnestyPointsEntity item in newList)
                {
                    item.AmnestyID = id;
                    bool status = AmnestyPointsAddWorker(item);
                }
            }

            return id;
        }

        public int? AddAmnestyPoints(AmnestyPointsEntity entity)
        {
            return DAL_AmnestyService.SP_Add_AmnestyPoints(entity);
        }
        #endregion

        #region GetList
        public List<AmnestyDashboardEntity> GetList_AmenstyForDashboard(FilterAmnestyEntity entity)
        {
            return DAL_AmnestyService.SP_GetList_AmnestyForDashboard(entity);
        }

        public List<AmnestyEntity> GetList_AmenstyByNumber(AmnestyEntity entity)
        {
            return DAL_AmnestyService.SP_GetList_AmnestyByNumber(entity);
        }
        public List<AmnestyEntity> GetList_Amensty(AmnestyEntity entity)
        {
            return DAL_AmnestyService.SP_GetList_Amnesty(entity);
        }

        public List<AmnestyPointsEntity> GetList_AmenstyPoints(AmnestyPointsEntity entity)
        {
            return DAL_AmnestyService.SP_GetList_AmnestyPoints(entity);
        }

        public List<AmnestyPointsEntity> GetList_AmenstyPointsClassified(AmnestyPointsEntity entity)
        {
            List<AmnestyPointsEntity> list = DAL_AmnestyService.SP_GetList_AmnestyPoints(entity);

            List<AmnestyPointsEntity> removeList = new List<AmnestyPointsEntity>();

            foreach (AmnestyPointsEntity item in list)
            {
                if (list.FindAll(r => r.ID == item.RealParentID).Any())
                {
                    list.FindAll(r => r.ID == item.RealParentID).First().subItems.Add(item);
                    removeList.Add(item);
                }
            }

            foreach (AmnestyPointsEntity item in removeList)
            {
                list.RemoveAll(r => r.ID == item.ID);
            }

            return list;
        }

        #endregion

        #region Update
        public bool? UpdateAmnesty(AmnestyEntity entity)
        {
            bool status = DAL_AmnestyService.Sp_Update_Amnesty(entity);

            List<AmnestyPointsEntity> oldList = DAL_AmnestyService.SP_GetList_AmnestyPoints(new AmnestyPointsEntity(AmnestyID: entity.ID));
            List<AmnestyPointsEntity> newList = entity.AmnestyPoints;


            foreach (AmnestyPointsEntity item in oldList)
            {
                if (newList.FindAll(r => r.ID == item.ID).Any())
                {
                    AmnestyPointsEntity point = newList.FindAll(r => r.ID == item.ID).First();
                    if (point.Name != item.Name || point.Order != point.Order)
                    {
                        bool? tempStatus = DAL_AmnestyService.Sp_Update_AmnestyPoints(point);
                    }
                }
                else
                {
                    item.Status = false;
                    bool? tempStatus = DAL_AmnestyService.Sp_Update_AmnestyPoints(item);
                }
            }

            //newList = Clas

            //foreach (AmnestyPointsEntity item in newList)
            //{
            //    if (!oldList.FindAll(r => r.ID == item.ID).Any())
            //    {
            //        AmnestyPointsEntity point = newList.FindAll(r => r.ID == item.ID).First();
            //        if (point.Name != item.Name || point.Order != point.Order)
            //        {
            //            bool? tempStatus = DAL_AmnestyService.Sp_Update_AmnestyPoints(point);
            //        }
            //    }
            //}


            return status;
        }
        public bool? UpdateAmnestyPoints(AmnestyPointsEntity entity)
        {
            bool? status = false;
            List<AmnestyPointsEntity> entityList = DAL_AmnestyService.SP_GetList_AmnestyPoints(entity);
            if (entityList.Any())
            {
                AmnestyPointsEntity prevEntity = entityList.First();

                prevEntity.Status = entity.Status;
                prevEntity.AmnestyID = entity.AmnestyID;

                status = DAL_AmnestyService.Sp_Update_AmnestyPoints(prevEntity);

                if (status == null) status = false;
            }
            else
            {
                int? id = DAL_AmnestyService.SP_Add_AmnestyPoints(entity);
                if (id != null) status = true;
            }
            return status;
        }
        #endregion

        private bool AmnestyPointsAddWorker(AmnestyPointsEntity item)
        {
            bool status = true;
            if (item.RealParentID == null) item.RealParentID = 0;
            item.Order = item.TempID;
            int? tempID = DAL_AmnestyService.SP_Add_AmnestyPoints(item);
            if (tempID != null)
            {
                foreach (AmnestyPointsEntity Items in item.subItems)
                {
                    Items.RealParentID = tempID;
                    Items.AmnestyID = item.AmnestyID;
                    bool tempStatus = this.AmnestyPointsAddWorker(Items);
                    if (!tempStatus) status = false;
                }
            }
            else status = false;
            return status;
        }

        private List<AmnestyPointsEntity> ClassifyPoint(List<AmnestyPointsEntity> List)
        {
            List<AmnestyPointsEntity> removeList = new List<AmnestyPointsEntity>();

            foreach (var item in List)
            {
                if (item.New != null && item.New.Value)
                {
                    AmnestyPointsEntity curParent = new AmnestyPointsEntity();
                    if ((item.RealParentID != null && item.RealParentID != 0) || item.ParentID != "0")
                    {
                        if (!List.FindAll(r => r.ID.ToString() == item.ParentID).Any())
                        {
                            List.FindAll(r => r.TempID == item.ParentID).First().subItems.Add(item);
                            removeList.Add(item);
                        }
                        else
                        {
                            List.FindAll(r => r.ID.ToString() == item.ParentID).First().subItems.Add(item);
                            removeList.Add(item);
                        }
                    }
                }
            }

            for (int i = 0; i < removeList.Count; i++)
            {
                List.RemoveAll(r => r.ParentID == removeList[i].ParentID);
                if (removeList[i].RealParentID != null) List.RemoveAll(r => r.RealParentID == removeList[i].RealParentID);
            }
            return List;
        }
    }
}
