﻿using CommonLayer.BusinessEntities;
using DataAccessLayer.DataCommunication;

namespace BusinessLayer.BusinessServices
{
    public class Spouses : SpousesEntity 
    {
        public int Save()
        {
            int? insertID = DAL_SpousesService.getInstance().SP_Add_Spouses(this);

            return insertID.Value;
        }
    }
}
