﻿using System.Collections.Generic;
using System.Linq;
using CommonLayer.BusinessEntities;
using System.Threading.Tasks;
using System;

namespace BusinessLayer.BusinessServices
{
    public class BL_PrisonersLayer : BusinessServicesCommunication
    {
        public List<PrisonerEntity> GetPrisonersForDashboard(FilterPrisonerEntity FilterPrisoner) {
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            FilterPrisoner.OrgUnitID = User.SelectedOrgUnitID;
            return DAL_PrisionersService.SP_GetList_PrisonersForDashboard(FilterPrisoner);
        }
        public List<EncouragementsDashboardEntity> GetEncouragementsForDashboard(FilterEncouragementsEntity FilterEncouragements)
        {
            // UNDONE: this sectin is undone maybe :D
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            FilterEncouragements.OrgUnitID = User.SelectedOrgUnitID;

            return DAL_EncouragementService.SP_GetList_EncouragementsForDashboard(FilterEncouragements);
        }
        public List<EducationalCoursesDashboardEntity> GetEducationalCoursesForDashboard(FilterEducationalCoursesEntity FilterData)
        {
            // UNDONE: this sectin is undone maybe :D
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            FilterData.OrgUnitID = User.SelectedOrgUnitID;

            return DAL_EducationalCoursesService.SP_GetList_EducationalCoursesForDashboard(FilterData);
        }
        public List<WorkLoadsDashboardEntity> GetWorkLoadsForDashboard(FilterWorkLoadsEntity FilterData)
        {
            // UNDONE: this sectin is undone maybe :D
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            FilterData.OrgUnitID = User.SelectedOrgUnitID;

            return DAL_WorkLoadsService.SP_GetList_WorkLoadsForDashboard(FilterData);
        }
        public List<OfficialVisitsDashboardEntity> GetOfficialVisitsForDashboard(FilterOfficialVisitsEntity FilterData)
        {
            // UNDONE: this sectin is undone maybe :D
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            FilterData.OrgUnitID = User.SelectedOrgUnitID;

            return DAL_OfficialVisitsService.SP_GetList_OfficialVisitsForDashboard(FilterData);
        }
        public List<PersonalVisitsDashboardEntity> GetPersonalVisitsForDashboard(FilterPersonalVisitsEntity FilterData)
        {
            // UNDONE: this sectin is undone maybe :D
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            FilterData.OrgUnitID = User.SelectedOrgUnitID;
            return DAL_PersonalVisitsService.SP_GetList_PersonalVisitsForDashboard(FilterData);
        }
        public List<DepartureDashboardEntity> GetDeparturesForDashboard(FilterDepartureEntity FilterData)
        {
            // UNDONE: this sectin is undone maybe :D
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            FilterData.OrgUnitID = User.SelectedOrgUnitID;

            return DAL_DepartureService.SP_GetList_DeparturesForDashboard(FilterData);
        }
       
        public List<TransferDashboardEntity> GetTransfersForDashboard(FilterTransferEntity FilterData)
        {
            // UNDONE: this sectin is undone maybe :D
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            FilterData.OrgUnitID = User.SelectedOrgUnitID;

            return DAL_TransferService.SP_GetList_TransfersForDashboard(FilterData);
        }
        public List<PenaltiesDashboardEntity> GetPenaltiesForDashboard(FilterPenaltiesEntity FilterPenalties)
        {
            // UNDONE: this sectin is undone maybe :D
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            FilterPenalties.OrgUnitID = User.SelectedOrgUnitID;

            return DAL_PenaltiesService.SP_GetList_PenaltiesForDashboard(FilterPenalties);
        }
        public List<PackagesDashboardEntity> GetPackagesForDashboard(FilterPackagesEntity FilterEntity)
        {
            // UNDONE: this sectin is undone maybe :D
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            FilterEntity.OrgUnitID = User.SelectedOrgUnitID;

            return DAL_PackageService.SP_GetList_PackagesForDashboard(FilterEntity);
        }

        public PrisonerEntity AddPrisoner(PrisonerEntity prisoner)
        {
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            
            prisoner.OrgUnitID = User.SelectedOrgUnitID;
            prisoner.EmployeeID = User.ID;


            int? PrisonerID = DAL_PrisionersService.SP_Add_Prisoners(prisoner);

            if (PrisonerID != null) prisoner.ID = PrisonerID;

            //if (PrisonerID.HasValue && prisoner.IdentificationDocuments != null)
            //{
            //    foreach (IdentificationDocument doc in prisoner.IdentificationDocuments)
            //    {
            //        doc.PrisonerID = PrisonerID.Value;
            //        AddIdentificationDocuments(doc);
            //    }
            //}

            return prisoner;
        }
        public PrisonerEntity GetPrisoner(int PrisonerID, bool person = false)
        {
            List<PrisonerEntity> list = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID);
            if (list.Any()) {
                PrisonerEntity entity = list.First();
                if(person)
                    entity.Person = DAL_PersonsService.SP_GetList_PersonsByPrisonerID((int)entity.ID).First();
               
                return entity;
            }

            return null;
        }
        public bool UpdatePrisoner(PrisonerEntity entity)
        {
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            entity.OrgUnitID = User.SelectedOrgUnitID;

            return DAL_PrisionersService.SP_Update_Prisoners(entity);
        }
        public List<PrisonerEntity> GetPrisonersBySearch(string query)
        {
            return DAL_PrisionersService.SP_GetList_PrisonersBySearch(query);
        }
        public bool? CheckConflictPersons(int PrisonerID, int? Number = null)
        {
            int? res = DAL_PrisionersService.SP_Check_ConflictPersonsTogether(PrisonerID, Number);
            if (res == 0)
            {
                return false;
            }
            
            return true;
        }
        
        #region Person
        public int AddPerson(PersonEntity person)
        {
            person.Registration_ID = SetAddressID(person.Registration_Entity);
            person.Living_ID = SetAddressID(person.Living_Entity);
            List<PersonEntity> curList = new List<PersonEntity>();
            int? PersonID = null;
            if (person.Registration_ID != null && person.Living_ID != null)
            {
                //curList = DAL_PersonsService.SP_GetList_Persons(new PersonEntity(Personal_ID: person.Personal_ID));
                //if (curList.Any())
                //{
                //    PersonEntity curPerson = curList.Last();
                //    person.ID = curPerson.ID;
                //    bool status = DAL_PersonsService.SP_Update_Persons(person);
                //    PersonID = person.ID;
                //}
                //else
                //{
                    PersonID = DAL_PersonsService.SP_Add_Persons(person);
                //}
            }

            if (PersonID != null && person.IdentificationDocuments != null)
            {
                List<IdentificationDocument> List = DAL_IdentificationDocumentsService.SP_GetList_IdentificationDocuments(new IdentificationDocument(PersonID: PersonID));

                if (List.Any())
                {
                    foreach (IdentificationDocument item in List)
                    {
                        item.Status = false;
                        bool? status = DAL_IdentificationDocumentsService.SP_Update_IdentificationDocuments(item);
                    }
                }

                foreach (IdentificationDocument doc in person.IdentificationDocuments)
                {
                    doc.PersonID = PersonID.Value;
                    int? id = AddIdentificationDocuments(doc);
                }
            }

            return PersonID.Value;
        }
        public int? SetAddressID(RegistrationAddress item)
        {
            int? id = null;
            int? tempID = null;
            PersonToAddressEntity PersoonAddress = new PersonToAddressEntity();           
            //List<AddressItemEntity> AppartmentAddress = DAL_AddressService.SP_GetList_AddressApartment(new AddressItemEntity(Name: item.Registration_Apartment));
            //if (AppartmentAddress.Any()) PersoonAddress.ApartmentID = AppartmentAddress.First().ID;
            //else
            //{
            tempID = DAL_AddressService.SP_Add_AddressApartment(new AddressItemEntity(Name: item.Registration_Apartment));
            if (tempID != null) PersoonAddress.ApartmentID = tempID;
                //else return null;
            //}
            //List<AddressItemEntity> BuildingAddress = DAL_AddressService.SP_GetList_AddressBuilding(new AddressItemEntity(Name: item.Registration_Building));
            //if (BuildingAddress.Any()) PersoonAddress.BuildingID = BuildingAddress.First().ID;
            //else
            //{
            tempID = DAL_AddressService.SP_Add_AddressBuilding(new AddressItemEntity(Name: item.Registration_Building));
            if (tempID != null) PersoonAddress.BuildingID = tempID;
            
            //}
            //List<AddressItemEntity> BuildingTypeAddress = DAL_AddressService.SP_GetList_AddressBuildingType(new AddressItemEntity(Name: item.Registration_Building_Type));
            //if (BuildingTypeAddress.Any()) PersoonAddress.BuildingTypeID = BuildingTypeAddress.First().ID;
            //else
            //{
            tempID = DAL_AddressService.SP_Add_AddressBuildingType(new AddressItemEntity(Name: item.Registration_Building_Type));
            if (tempID != null) PersoonAddress.BuildingTypeID = tempID;
            //}
            //List<AddressItemEntity> CommunityAddress = DAL_AddressService.SP_GetList_AddressCommunity(new AddressItemEntity(Name: item.Registration_Community));
            //if (CommunityAddress.Any()) PersoonAddress.ComunityID = CommunityAddress.First().ID;
            //else
            //{
            tempID = DAL_AddressService.SP_Add_AddressCommunity(new AddressItemEntity(Name: item.Registration_Community));
            if (tempID != null) PersoonAddress.ComunityID = tempID;
            //}
            //List<AddressItemEntity> RegionAddress = DAL_AddressService.SP_GetList_AddressRegion(new AddressItemEntity(Name: item.Registration_Region));
            //if (RegionAddress.Any()) PersoonAddress.RegionID = RegionAddress.First().ID;
            //else
            //{
            tempID = DAL_AddressService.SP_Add_AddressRegion(new AddressItemEntity(Name: item.Registration_Region));
            if (tempID != null) PersoonAddress.RegionID = tempID;
            //}
            //List<AddressItemEntity> StreetAddress = DAL_AddressService.SP_GetList_AddressStreet(new AddressItemEntity(Name: item.Registration_Street));
            //if (StreetAddress.Any()) PersoonAddress.StreetID = StreetAddress.First().ID;
            //else
            //{
            tempID = DAL_AddressService.SP_Add_AddressStreet(new AddressItemEntity(Name: item.Registration_Street));
            if (tempID != null) PersoonAddress.StreetID = tempID;

            id = DAL_AddressService.SP_Add_PersonToAddress(PersoonAddress);

            return id;
        }

        public PersonEntity GetPersonByPrisonerID(int PrisonerID)
        {
            PersonEntity tempPerson = new PersonEntity();
            
            List<PersonEntity> list = DAL_PersonsService.SP_GetList_PersonsByPrisonerID(PrisonerID);
            if (list != null && list.Any())
            {
                PersonEntity entity = list.First();
                entity.IdentificationDocuments = DAL_IdentificationDocumentsService.SP_GetList_IdentificationDocuments(new IdentificationDocument(PersonID: entity.ID));

                return entity;
            }

            return null;
        }
        public PersonEntity GetPerson(int? PersonID = null, string PersonalID = null, bool Full = false)
        {
            PersonEntity tempPerson = new PersonEntity();
            if (PersonID != null) tempPerson.ID = PersonID.Value;
            tempPerson.Personal_ID = PersonalID;


            List<PersonEntity> list = DAL_PersonsService.SP_GetList_Persons(tempPerson);
            if (list != null && list.Any())
            {
                PersonEntity entity = list.First();
                entity.IdentificationDocuments = DAL_IdentificationDocumentsService.SP_GetList_IdentificationDocuments(new IdentificationDocument(PersonID: entity.ID));

                if (Full)
                {
                    if (entity.Living_ID != null)
                    {
                        List<PersonToAddressEntity> tempList = DAL_AddressService.SP_GetList_PersonToAddress(new PersonToAddressEntity(ID: entity.Living_ID));
                        if (tempList != null && tempList.Any())
                        {
                            entity.Living_Entity = new RegistrationAddress(tempList.Last());
                        }
                    }
                    if (entity.Registration_ID != null)
                    {
                        List<PersonToAddressEntity> tempList = DAL_AddressService.SP_GetList_PersonToAddress(new PersonToAddressEntity(ID: entity.Registration_ID));
                        if (tempList != null && tempList.Any())
                        {
                            entity.Registration_Entity = new RegistrationAddress(tempList.Last());
                        }
                    }

                    List<AdditionalFileEntity> files = GetFiles(PersonID: entity.ID.Value, TypeID: FileType.MAIN_DATA_AVATAR_IMAGES);

                    if (files != null && files.Any())
                    {
                        entity.PhotoLink = BL_Files.getInstance().GetLink(files.First().ID, files.First().FileName);
                    }

                }
                    return entity;
            }


            return null;
        }
        public PersonEntity AddPersonAndGet(int? PersonID = null, string PersonalID = null, PersonEntity Person = null)
        {
            PersonEntity tempPerson = new PersonEntity();

            if (PersonID != null) tempPerson.ID = PersonID.Value;
            if (Person != null && Person.ID != null) tempPerson.ID = Person.ID;

            tempPerson.Personal_ID = PersonalID ?? Person.Personal_ID;

            List<PersonEntity> list = null;
            if (tempPerson.Personal_ID != null || tempPerson.ID != null)
            {
                list = DAL_PersonsService.SP_GetList_Persons(tempPerson);
            }
            if (list != null && list.Any() && (tempPerson.Personal_ID != null || tempPerson.ID != null))
            {
                PersonEntity entity = list.First();
                entity.IdentificationDocuments = DAL_IdentificationDocumentsService.SP_GetList_IdentificationDocuments(new IdentificationDocument(PersonID: entity.ID));

                Person.ID = entity.ID;
                //bool tempStatus = UpdatePerson(Person);

                if (Person != null && (Person.Photo_ID != null || Person.PhotoLink != null))
                {
                    string PhotoLink = Person.Photo_ID ?? Person.PhotoLink;

                    Byte[] bytes = Convert.FromBase64String(PhotoLink);

                    AdditionalFileEntity fileEntry = new AdditionalFileEntity(bytes.Length);

                    fileEntry.FileName = Person.Personal_ID.ToString() + ".jpg";
                    fileEntry.FileExtension = ".jpg";
                    fileEntry.ContentType = "image/jpeg";
                    fileEntry.isPrisoner = false;
                    fileEntry.PersonID = entity.ID;
                    fileEntry.PNum = Person.Personal_ID;
                    fileEntry.TypeID = FileType.PERSON_ADD_PHOTO;
                    fileEntry.FileContent = bytes;

                    int? fileID = BL_Files.getInstance().AddFile(fileEntry);
                }

                return entity;
            }
            else if (Person != null)
            {
                if (Person.Personal_ID == null)
                    Person.Personal_ID = "8" + Person.FirstName.GetHashCode().ToString();
                PersonID = AddPerson(Person);

                if (PersonID != null)
                {
                    if (Person != null && (Person.Photo_ID != null || Person.PhotoLink != null))
                    {
                        string PhotoLink = Person.Photo_ID ?? Person.PhotoLink;

                        Byte[] bytes = Convert.FromBase64String(PhotoLink);

                        AdditionalFileEntity fileEntry = new AdditionalFileEntity(bytes.Length);

                        fileEntry.FileName = Person.Personal_ID.ToString() + ".jpg";
                        fileEntry.FileExtension = ".jpg";
                        fileEntry.ContentType = "image/jpeg";
                        fileEntry.isPrisoner = false;
                        fileEntry.PersonID = PersonID;
                        fileEntry.PNum = Person.Personal_ID;
                        fileEntry.TypeID = FileType.PERSON_ADD_PHOTO;
                        fileEntry.FileContent = bytes;

                        int? fileID = BL_Files.getInstance().AddFile(fileEntry);
                    }
                    tempPerson = GetPerson(PersonID: PersonID);
                }
            }

            return tempPerson;
        }
        public bool UpdatePerson(PersonEntity Person)
        {
            bool status = true;
            if (Person != null && Person.ID != null)
            {
                PersonEntity OldEntity = GetPerson(PersonID: Person.ID, Full: true);

                bool? tempStatus = DAL_PersonsService.SP_Update_Persons(Person);
                if (tempStatus == null || !tempStatus.Value) status = false;

                if (Person.isCustom != null && Person.isCustom.Value)
                {
                    if (OldEntity != null && OldEntity.IdentificationDocuments != null)
                    {
                        // update identification document
                        if (Person.IdentificationDocuments != null)
                        {
                            foreach (var curEntity in Person.IdentificationDocuments)
                            {
                                if (!OldEntity.IdentificationDocuments.FindAll(r => r.ID == curEntity.ID).Any())
                                {
                                    curEntity.PersonID = Person.ID;
                                    int? curID = DAL_IdentificationDocumentsService.SP_Add_IdentificationDocuments(curEntity);
                                    if (curID == null) status = false;
                                }
                            }
                            foreach (var curEntity in OldEntity.IdentificationDocuments)
                            {
                                if (!Person.IdentificationDocuments.FindAll(r => r.ID == curEntity.ID).Any())
                                {
                                    curEntity.Status = false;
                                    bool tempIdStatus = DAL_IdentificationDocumentsService.SP_Update_IdentificationDocuments(curEntity);
                                    if (!tempIdStatus) status = false;
                                }
                            }
                        }
                    }
                }


                // update addresses
                if (Person.Living_Entity != null)
                {
                    if (OldEntity.Living_ID != null)
                    {
                        List<PersonToAddressEntity> tempList = DAL_AddressService.SP_GetList_PersonToAddress(new PersonToAddressEntity(ID: OldEntity.Living_ID));
                        if (tempList != null && tempList.Any())
                        {
                            PersonToAddressEntity PersoonAddress = tempList.Last();
                            if (PersoonAddress.ApartmentID != null)
                            {
                                bool? addressStatus = DAL_AddressService.SP_Update_AddressApartment(new AddressItemEntity(PersoonAddress.ApartmentID, Name: Person.Living_Entity.Registration_Apartment));
                                if (addressStatus == null && !addressStatus.Value) status = false;
                            }
                            if (PersoonAddress.BuildingID != null)
                            {
                                bool? addressStatus = DAL_AddressService.SP_Update_AddressBuilding(new AddressItemEntity(PersoonAddress.BuildingID, Name: Person.Living_Entity.Registration_Building));
                                if (addressStatus == null && !addressStatus.Value) status = false;
                            }
                            if (PersoonAddress.BuildingTypeID != null)
                            {
                                bool? addressStatus = DAL_AddressService.SP_Update_AddressBuildingType(new AddressItemEntity(PersoonAddress.BuildingTypeID, Name: Person.Living_Entity.Registration_Building_Type));
                                if (addressStatus == null && !addressStatus.Value) status = false;
                            }
                            if (PersoonAddress.ComunityID != null)
                            {
                                bool? addressStatus = DAL_AddressService.SP_Update_AddressCommunity(new AddressItemEntity(PersoonAddress.ComunityID, Name: Person.Living_Entity.Registration_Community));
                                if (addressStatus == null && !addressStatus.Value) status = false;
                            }
                            if (PersoonAddress.RegionID != null)
                            {
                                bool? addressStatus = DAL_AddressService.SP_Update_AddressRegion(new AddressItemEntity(PersoonAddress.RegionID, Name: Person.Living_Entity.Registration_Region));
                                if (addressStatus == null && !addressStatus.Value) status = false;
                            }
                            if (PersoonAddress.StreetID != null)
                            {
                                bool? addressStatus = DAL_AddressService.SP_Update_AddressStreet(new AddressItemEntity(PersoonAddress.StreetID, Name: Person.Living_Entity.Registration_Street));
                                if (addressStatus == null && !addressStatus.Value) status = false;
                            }
                        }
                    }
                }
                if (Person.Registration_Entity != null)
                {
                    if (OldEntity.Registration_ID != null)
                    {
                        List<PersonToAddressEntity> tempList = DAL_AddressService.SP_GetList_PersonToAddress(new PersonToAddressEntity(ID: OldEntity.Registration_ID));
                        if (tempList != null && tempList.Any())
                        {
                            PersonToAddressEntity PersoonAddress = tempList.Last();
                            if (PersoonAddress.ApartmentID != null)
                            {
                                bool? addressStatus = DAL_AddressService.SP_Update_AddressApartment(new AddressItemEntity(PersoonAddress.ApartmentID, Name: Person.Registration_Entity.Registration_Apartment));
                                if (addressStatus == null && !addressStatus.Value) status = false;
                            }
                            if (PersoonAddress.BuildingID != null)
                            {
                                bool? addressStatus = DAL_AddressService.SP_Update_AddressBuilding(new AddressItemEntity(PersoonAddress.BuildingID, Name: Person.Registration_Entity.Registration_Building));
                                if (addressStatus == null && !addressStatus.Value) status = false;
                            }
                            if (PersoonAddress.BuildingTypeID != null)
                            {
                                bool? addressStatus = DAL_AddressService.SP_Update_AddressBuildingType(new AddressItemEntity(PersoonAddress.BuildingTypeID, Name: Person.Registration_Entity.Registration_Building_Type));
                                if (addressStatus == null && !addressStatus.Value) status = false;
                            }
                            if (PersoonAddress.ComunityID != null)
                            {
                                bool? addressStatus = DAL_AddressService.SP_Update_AddressCommunity(new AddressItemEntity(PersoonAddress.ComunityID, Name: Person.Registration_Entity.Registration_Community));
                                if (addressStatus == null && !addressStatus.Value) status = false;
                            }
                            if (PersoonAddress.RegionID != null)
                            {
                                bool? addressStatus = DAL_AddressService.SP_Update_AddressRegion(new AddressItemEntity(PersoonAddress.RegionID, Name: Person.Registration_Entity.Registration_Region));
                                if (addressStatus == null && !addressStatus.Value) status = false;
                            }
                            if (PersoonAddress.StreetID != null)
                            {
                                bool? addressStatus = DAL_AddressService.SP_Update_AddressStreet(new AddressItemEntity(PersoonAddress.StreetID, Name: Person.Registration_Entity.Registration_Street));
                                if (addressStatus == null && !addressStatus.Value) status = false;
                            }
                        }
                    }
                }
            }
            return status;
        }
        #endregion

        #region Main Data
        public int AddMainData(MainDataEntity mainData)
        {
            if (mainData.PrisonerID.Value > 0)
            {
                if (mainData.PreviousConvictions != null)
                {
                    foreach (PreviousConvictionsEntity prevConvictions in mainData.PreviousConvictions)
                    {
                        prevConvictions.PrisonerID = mainData.PrisonerID.Value;
                        DAL_PreviousConvictionsService.SP_Add_PreviousConvictions(prevConvictions);
                    }
                }
            }


            return 0;
        }
        public MainDataEntity GetMainData(int PrisonerID)
        {
            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();
            MainDataEntity MainData = new MainDataEntity();

            AdditionalFileEntity curEntityFirst = new AdditionalFileEntity();
            curEntityFirst.PrisonerID = PrisonerID;
            curEntityFirst.TypeID = FileType.MAIN_DATA_ATTACHED_IMAGES;

            AdditionalFileEntity curEntitySecond = new AdditionalFileEntity();
            curEntitySecond.PrisonerID = PrisonerID;
            curEntitySecond.TypeID = FileType.MAIN_DATA_ATTACHED_FILES;

            // current prev conviction list
            MainData.PreviousConvictions = DAL_PreviousConvictionsService.SP_GetList_PreviousConvictions(new PreviousConvictionsEntity(PrisonerID: PrisonerID));
            foreach (var item in MainData.PreviousConvictions)
            {
                item.SentencingDataArticles = this.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(PrevConvictionDataID: item.ID));
            }
            List<PrisonerEntity> prisonerList = DAL_PrisionersService.SP_GetList_Prisoners(Personal_ID: currentPrisoner.PersonID);
            if (prisonerList != null && prisonerList.Any())
            {
                foreach (PrisonerEntity item in prisonerList)
                {
                    if (item.ID != PrisonerID)
                    {
                        // prisoner's old sentences
                        List<SentencingDataEntity> sentenceList = DAL_SentencingDataService.SP_GetList_SentencingData(new SentencingDataEntity(PrisonerID: item.ID));
                        if (sentenceList != null && sentenceList.Any())
                        {
                            foreach (SentencingDataEntity subItem in sentenceList)
                            {
                                PreviousConvictionsEntity tempPrevConvictionEntity = new PreviousConvictionsEntity(PrisonerID: PrisonerID);
                                tempPrevConvictionEntity.CodeLibItemID = subItem.CodeLibItem_ID;
                                tempPrevConvictionEntity.CodeLibItemName = subItem.CodeLibItem_Label;
                                tempPrevConvictionEntity.EndDate = subItem.SentencingEndDate;
                                tempPrevConvictionEntity.StartDate = subItem.SentencingStartDate;
                                tempPrevConvictionEntity.PenaltyDay = subItem.PenaltyDay;
                                tempPrevConvictionEntity.PenaltyMonth = subItem.PenaltyMonth;
                                tempPrevConvictionEntity.PenaltyYear = subItem.PenaltyYear;
                                tempPrevConvictionEntity.Archive = true;
                                tempPrevConvictionEntity.SentencingDataArticles = this.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(SentencingDataID: subItem.ID));
                                MainData.PreviousConvictions.Add(tempPrevConvictionEntity);
                            }
                        }

                        // prisoner's old prev convictions
                        List<PreviousConvictionsEntity> prevConvList = DAL_PreviousConvictionsService.SP_GetList_PreviousConvictions(new PreviousConvictionsEntity(PrisonerID: item.ID));
                        if (prevConvList != null && prevConvList.Any())
                        {
                            foreach (PreviousConvictionsEntity subItem in prevConvList)
                            {
                                subItem.Archive = true;
                                MainData.PreviousConvictions.Add(subItem);
                            }
                        }
                    }
                }
            }

            // prisoner current sentence
            List<SentencingDataEntity> curSentenceList = DAL_SentencingDataService.SP_GetList_SentencingData(new SentencingDataEntity(PrisonerID: PrisonerID));
            foreach (SentencingDataEntity subItem in curSentenceList)
            {
                if (subItem.State != null && !subItem.State.Value)
                {
                    PreviousConvictionsEntity tempPrevConvictionEntity = new PreviousConvictionsEntity(PrisonerID: PrisonerID);
                    tempPrevConvictionEntity.CodeLibItemID = subItem.CodeLibItem_ID;
                    tempPrevConvictionEntity.CodeLibItemName = subItem.CodeLibItem_Label;
                    tempPrevConvictionEntity.EndDate = subItem.SentencingEndDate;
                    tempPrevConvictionEntity.StartDate = subItem.SentencingStartDate;
                    tempPrevConvictionEntity.PenaltyDay = subItem.PenaltyDay;
                    tempPrevConvictionEntity.PenaltyMonth = subItem.PenaltyMonth;
                    tempPrevConvictionEntity.PenaltyYear = subItem.PenaltyYear;
                    tempPrevConvictionEntity.SentencingDataArticles = this.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(SentencingDataID: subItem.ID));
                    tempPrevConvictionEntity.Archive = true;
                    MainData.PreviousConvictions.Add(tempPrevConvictionEntity);
                }
            }

            MainData.Images = DAL_FileService.SP_GetList_Files(curEntityFirst);
            MainData.Files = DAL_FileService.SP_GetList_Files(curEntitySecond);

            return MainData;
        }
        #region Identification Documents
        public int AddIdentificationDocuments(IdentificationDocument entity)
        {
            int? newItemID = null;

            if (entity.PersonID == null && entity.PrisonerID != null)
            {
                entity.PersonID = GetPrisoner(entity.PrisonerID.Value).PersonID;
            }

            newItemID = DAL_IdentificationDocumentsService.SP_Add_IdentificationDocuments(entity);

            return newItemID.Value;
        }
        public List<IdentificationDocument> GetIdentificationDocuments(IdentificationDocument entity)
        {
            List<IdentificationDocument> identificationDocument = DAL_IdentificationDocumentsService.SP_GetList_IdentificationDocuments(entity);
            
            foreach (IdentificationDocument curDoc in identificationDocument)
            {
                curDoc.CitizenshipLibItem_Name = DAL_LibsServices.SP_Get_LibByID((int)curDoc.CitizenshipLibItem_ID).Name;
                curDoc.TypeLibItem_Name = DAL_LibsServices.SP_Get_LibByID((int)curDoc.TypeLibItem_ID).Name;
            }

            return identificationDocument;
        }
        public bool UpdateIdentificationDocuments(IdentificationDocument entity)
        {
            return DAL_IdentificationDocumentsService.SP_Update_IdentificationDocuments(entity);
        }
        #endregion
        #endregion

        #region Physical Data

        public int AddPhysicalData(PhysicalDataEntity physicalData)
        {
            int? newItemID = DAL_PhysicalDataService.SP_Add_PhysicalData(physicalData);
            return newItemID.Value;
        }
        public bool UpdatePhysicalData(PhysicalDataEntity physicalData)
        {
            return DAL_PhysicalDataService.SP_Update_PhysicalData(physicalData);
        }

        public PhysicalDataEntity GetPhysicalData(int PrisonerID)
        {
            PhysicalDataEntity physicalData = new PhysicalDataEntity();
            List<PhysicalDataEntity> list = DAL_PhysicalDataService.SP_GetList_PhysicalData(PrisonerID: PrisonerID);

            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();

            if (list.Any())
            {
                physicalData = list.First();

                physicalData.Invalides = GetInvalides(new InvalidEntity(PrisonerID:PrisonerID));
                physicalData.Leanings = GetLeaning(new LeaningEntity( PrisonerID: PrisonerID));
                physicalData.HeightWeight = GetHeightWeight(new HeightWeightEntity(PrisonerID: PrisonerID, PhysicalDataID: physicalData.ID));
                physicalData.Files = GetFiles(PrisonerID: PrisonerID, TypeID: FileType.PHYSICAL_DATA_ATTACHED_FILES);
            }
            return physicalData;
        }

        public bool CheckPhysicalData(int PrisonerID)
        {
            bool status = false;
            PhysicalDataEntity physicalData = new PhysicalDataEntity();
            List<PhysicalDataEntity> list = DAL_PhysicalDataService.SP_GetList_PhysicalData(PrisonerID: PrisonerID);

            if (list.Any())
            {
                status = true;
            }

            return status;

        }

        #region Invalides
        public int AddInvalides(InvalidEntity entity)
        {
            int? newItemID = DAL_InvalidsService.SP_Add_Invalids(entity);

            return newItemID.Value;
        }
        public List<InvalidEntity> GetInvalides(InvalidEntity entity)
        {
            List<InvalidEntity> items = DAL_InvalidsService.SP_GetList_Invalids(entity);

            return items;
        }
        public bool UpdateInvalides(InvalidEntity entity)
        {
            return DAL_InvalidsService.SP_Update_Invalids(entity);
        }
        #endregion
        #region Leaning
        public int AddLeaning(LeaningEntity entity)
        {
            int? newItemID = DAL_LeaningService.SP_Add_Leaning(entity);

            return newItemID.Value;
        }
        public List<LeaningEntity> GetLeaning(LeaningEntity entity)
        {
            List<LeaningEntity> items = DAL_LeaningService.SP_GetList_Leaning(entity);

            return items;
        }
        public bool UpdateLeaning(LeaningEntity entity)
        {
            return DAL_LeaningService.SP_Update_Leaning(entity);
        }
        #endregion
        #region HeightWeight
        public int AddHeightWeight(HeightWeightEntity entity)
        {
            int? newItemID = DAL_HeightWeightService.SP_Add_HeightWeight(entity);

            return newItemID.Value;
        }
        public List<HeightWeightEntity> GetHeightWeight(HeightWeightEntity entity)
        {
            List<HeightWeightEntity> items = DAL_HeightWeightService.SP_GetList_HeightWeight(entity);

            return items;
        }
        public bool UpdateHeightWeight(List<HeightWeightEntity> newList, int PrisonerID, int PPType)
        {
            bool status = true;

            List<HeightWeightEntity> oldList = DAL_HeightWeightService.SP_GetList_HeightWeight(new HeightWeightEntity(PrisonerID: PrisonerID));

            // update Height Weight
            foreach (var curEntity in newList)
            {
                if (!oldList.FindAll(r => r.ID == curEntity.ID).Any())
                {
                    int? curID = DAL_HeightWeightService.SP_Add_HeightWeight(curEntity);
                    if (curID == null) status = false;
                }
            }
            foreach (var curEntity in oldList)
            {
                if (!newList.FindAll(r => r.ID == curEntity.ID).Any())
                {
                    curEntity.Status = false;
                    bool tempStatus = DAL_HeightWeightService.SP_Update_HeightWeight(curEntity);
                    if (!tempStatus) status = false;
                }
            }

            return status;
        }
        public bool RemoveHeightWeight(HeightWeightEntity curEntity)
        {
            bool status = true;

            curEntity.Status = false;
            bool tempStatus = DAL_HeightWeightService.SP_Update_HeightWeight(curEntity);
            if (!tempStatus) status = false;

            return status;
        }

        #endregion
        #endregion

        #region Fingerprints Tattoos Scars
        public int AddFingerprintsTattoosScars(FingerprintsTattoosScarsItemEntity entity)
        {
            int? insertID = DAL_FingerprintsTattoosScarsService.SP_Add_FingerprintsTattoosScars(entity);

            return insertID.Value;
        }
        public List<FingerprintsTattoosScarsItemEntity> GetFingerprintsTattoosScarsItem(int? ID = null, int? PrisonerID = null, int? typeID = null)
        {
            //FingerprintsTattoosScarsItemEntity fingerprintsTattoosScarsItem = new FingerprintsTattoosScarsItemEntity();
            List<FingerprintsTattoosScarsItemEntity> fingerprintsTattoosScarsItems = DAL_FingerprintsTattoosScarsService.SP_GetList_FingerprintsTattoosScars(ID: ID, TypeID: typeID, PrisonerID: PrisonerID);

            return fingerprintsTattoosScarsItems;
        }
        public FingerprintsTattoosScarsEntity GetFingerprintsTattoosScars(int PrisonerID)
        {
            FingerprintsTattoosScarsEntity fingerprintsTattoosScarsItem = new FingerprintsTattoosScarsEntity();
            fingerprintsTattoosScarsItem.Fingerprints = GetFingerprintsTattoosScarsItem(PrisonerID: PrisonerID, typeID: (int)FileType.FINGERTATOOSCARS_FINGER);
            fingerprintsTattoosScarsItem.Tattoos = GetFingerprintsTattoosScarsItem(PrisonerID: PrisonerID, typeID: (int)FileType.FINGERTATOOSCARS_TATOO);
            fingerprintsTattoosScarsItem.Scars = GetFingerprintsTattoosScarsItem(PrisonerID: PrisonerID, typeID: (int)FileType.FINGERTATOOSCARS_SCAR);
            
            return fingerprintsTattoosScarsItem;
        }
        public bool UpdateFingerprintsTattoosScarsItem(FingerprintsTattoosScarsItemEntity entity)
        {
            return DAL_FingerprintsTattoosScarsService.SP_Update_FingerprintsTattoosScars(entity);
        }
        #endregion

        #region Educations Professions Interests AbilitiesSkills Language UniversityDegree
        #region Educations Professions
        public int AddEducationsProfessions(EducationsProfessionsEntity entity)
        {
            int? insertID = DAL_EducationsProfessionsService.SP_Add_EducationsProfessions(entity);

            return insertID.Value;
        }

        public EducationsProfessionsEntity GetEducationsProfessions(EducationsProfessionsEntity entity, bool grouped =false)
        {
            List<EducationsProfessionsEntity> educationsProfessions = new List<EducationsProfessionsEntity>();
            EducationsProfessionsEntity educationsProfession = new EducationsProfessionsEntity();
            
            educationsProfessions = DAL_EducationsProfessionsService.SP_GetList_EducationsProfessions(entity);
            if (educationsProfessions.Any())
            {
                educationsProfession = educationsProfessions.Last();
                if (grouped )
                {
                    educationsProfession.Professions = GetProfessions(new ProfessionEntity(PrisonerID: entity.PrisonerID, EducationProfessionsID: educationsProfession.ID));
                    educationsProfession.Interests = GetInterest(new InterestEntity(PrisonerID: entity.PrisonerID, EducationProfessionsID: educationsProfession.ID));
                    educationsProfession.AbilitiesSkills = GetAbilitiesSkills(new AbilitiesSkillsEntity(PrisonerID: entity.PrisonerID, EducationProfessionsID: educationsProfession.ID));
                    educationsProfession.Languages = GetLanguages(new LanguageEntity(PrisonerID: entity.PrisonerID, EducationProfessionsID: educationsProfession.ID));
                    educationsProfession.UniversityDegree = GetUniversityDegree(new UniversityDegreeEntity(PrisonerID: entity.PrisonerID, EducationProfessionsID: educationsProfession.ID));
                    //educationsProfession.Files = GetFiles(PrisonerID: entity.PrisonerID.Value, TypeID: FileType.EDUCATION_PROFESSIONS_FILES);
                }
            }
                

            return educationsProfession;
        }

        public bool UpdateEducationProfessions(EducationsProfessionsEntity newEntity)
        {
            bool status = false;

            EducationsProfessionsEntity oldEntity = GetEducationsProfessions(newEntity, true);

            // update Education
            newEntity.ID = oldEntity.ID;
            bool curStatus = DAL_EducationsProfessionsService.SP_Update_EducationsProfessions(newEntity);

            if (curStatus)
            {
                status = true;

                // update Professions
                foreach (ProfessionEntity curEntity in newEntity.Professions)
                {
                    if (!oldEntity.Professions.FindAll(r => r.LibItemID == curEntity.LibItemID).Any())
                    {
                        curEntity.EducationProfessionsID = oldEntity.ID;
                        int? curID = DAL_ProfessionsService.SP_Add_Professions(curEntity);
                        if (curID == null) status = false;
                    }
                }
                foreach (ProfessionEntity curEntity in oldEntity.Professions)
                {
                    if (!newEntity.Professions.FindAll(r => r.LibItemID == curEntity.LibItemID).Any())
                    {
                        curEntity.Status = false;
                        bool tempStatus = DAL_ProfessionsService.SP_Update_Professions(curEntity);
                        if (!tempStatus) status = false;
                    }
                }

                // update Interests
                foreach (InterestEntity curEntity in newEntity.Interests)
                {
                    if (!oldEntity.Interests.FindAll(r => r.LibItemID == curEntity.LibItemID).Any())
                    {
                        curEntity.EducationProfessionsID = oldEntity.ID;
                        int? curID = DAL_InterestsService.SP_Add_Interests(curEntity);
                        if (curID == null) status = false;
                    }
                }
                foreach (InterestEntity curEntity in oldEntity.Interests)
                {
                    if (!newEntity.Interests.FindAll(r => r.LibItemID == curEntity.LibItemID).Any())
                    {
                        curEntity.Status = false;
                        bool tempStatus = DAL_InterestsService.SP_Update_Interests(curEntity);
                        if (!tempStatus) status = false;
                    }
                }

                // update Languages
                foreach (LanguageEntity curEntity in newEntity.Languages)
                {
                    if (!oldEntity.Languages.FindAll(r => r.LibItemID == curEntity.LibItemID).Any())
                    {
                        curEntity.EducationProfessionsID = oldEntity.ID;
                        int? curID = DAL_LanguagesService.SP_Add_Languages(curEntity);
                        if (curID == null) status = false;
                    }
                }
                foreach (LanguageEntity curEntity in oldEntity.Languages)
                {
                    if (!newEntity.Languages.FindAll(r => r.LibItemID == curEntity.LibItemID).Any())
                    {
                        curEntity.Status = false;
                        bool tempStatus = DAL_LanguagesService.SP_Update_Languages(curEntity);
                        if (!tempStatus) status = false;
                    }
                }

                // update AbilitiesSkills
                foreach (AbilitiesSkillsEntity curEntity in newEntity.AbilitiesSkills)
                {
                    if (!oldEntity.AbilitiesSkills.FindAll(r => r.LibItemID == curEntity.LibItemID).Any())
                    {
                        curEntity.EducationProfessionsID = oldEntity.ID;
                        int? curID = DAL_AbilitiesSkillsService.SP_Add_AbilitiesSkills(curEntity);
                        if (curID == null) status = false;
                    }
                }
                foreach (AbilitiesSkillsEntity curEntity in oldEntity.AbilitiesSkills)
                {
                    if (!newEntity.AbilitiesSkills.FindAll(r => r.LibItemID == curEntity.LibItemID).Any())
                    {
                        curEntity.Status = false;
                        bool tempStatus = DAL_AbilitiesSkillsService.SP_Update_AbilitiesSkills(curEntity);
                        if (!tempStatus) status = false;
                    }
                }


                if (newEntity.LibItemID == 202)
                {
                    // update UnivercityDegreenewEntity.UniversityDegree
                    if (oldEntity.UniversityDegree.ID == null)
                    {
                        //if (newEntity.UniversityDegree.AcademicDegreeLibItemID != null && newEntity.UniversityDegree.DegreeLibItemID != null)
                        //{
                            newEntity.UniversityDegree.EducationProfessionsID = oldEntity.ID;
                            int? currentID = DAL_UniversityDegreeService.SP_Add_UniversityDegree(newEntity.UniversityDegree);
                            if (currentID == null) status = false;
                        //}
                    }
                    else
                    {
                        newEntity.UniversityDegree.ID = oldEntity.UniversityDegree.ID;
                        if (newEntity.UniversityDegree.AcademicDegreeLibItemID == null || newEntity.UniversityDegree.DegreeLibItemID == null) newEntity.UniversityDegree.Status = false;
                        bool updateStatus = DAL_UniversityDegreeService.SP_Update_UniversityDegree(newEntity.UniversityDegree);
                        if (!updateStatus) status = false;
                    }
                }
                else
                {
                    if (oldEntity.UniversityDegree.ID != null)
                    {
                        bool updateStatus = DAL_UniversityDegreeService.SP_Update_UniversityDegree(new UniversityDegreeEntity(ID: oldEntity.UniversityDegree.ID, Status: false));
                        if (!updateStatus) status = false;
                    }
                }

                if (newEntity.UniversityDegree != null && newEntity.UniversityDegree.Awardes != null)
                {
                    foreach (EducationAwardsEntity curEntity in newEntity.UniversityDegree.Awardes)
                    {
                        if (!oldEntity.UniversityDegree.Awardes.FindAll(r => r.LibItemID == curEntity.LibItemID).Any())
                        {
                            int? curID = DAL_UniversityDegreeService.SP_Add_EducationAwards(curEntity);
                            if (curID == null) status = false;
                        }
                    }
                    foreach (EducationAwardsEntity curEntity in oldEntity.UniversityDegree.Awardes)
                    {
                        if (!newEntity.UniversityDegree.Awardes.FindAll(r => r.LibItemID == curEntity.LibItemID).Any())
                        {
                            curEntity.Status = false;
                            bool tempStatus = DAL_UniversityDegreeService.SP_Update_EducationAwards(curEntity);
                            if (!tempStatus) status = false;
                        }
                    }
                }

            }

            return status;
        }
        #endregion
        #region Professions
        public int AddProfession(ProfessionEntity entity)
        {
            int? insertID = DAL_ProfessionsService.SP_Add_Professions(entity);
            return insertID.Value;
        }
        public List<ProfessionEntity> GetProfessions(ProfessionEntity entity)
        {
            List<ProfessionEntity> professions =  DAL_ProfessionsService.SP_GetList_Professions(entity);

            return professions;
        }
        public bool UpdateProfessions(ProfessionEntity entity)
        {
            return DAL_ProfessionsService.SP_Update_Professions(entity);
        }
        #endregion
        #region Interest
        public int AddInterest(InterestEntity entity)
        {
            int? insertID = DAL_InterestsService.SP_Add_Interests(entity);
            return insertID.Value;
        }
        public List<InterestEntity> GetInterest(InterestEntity entity)
        {
            List<InterestEntity> professions = DAL_InterestsService.SP_GetList_Interests(entity);

            return professions;
        }
        public bool UpdateInterest(InterestEntity entity)
        {
            return DAL_InterestsService.SP_Update_Interests(entity);
        }
        #endregion
        #region AbilitiesSkills
        public int AddAbilitiesSkills(AbilitiesSkillsEntity entity)
        {
            int? insertID = DAL_AbilitiesSkillsService.SP_Add_AbilitiesSkills(entity);
            return insertID.Value;
        }
        public List<AbilitiesSkillsEntity> GetAbilitiesSkills(AbilitiesSkillsEntity entity)
        {
            List<AbilitiesSkillsEntity> abilitiesSkillsEntity = new List<AbilitiesSkillsEntity>();

            abilitiesSkillsEntity = DAL_AbilitiesSkillsService.SP_GetList_AbilitiesSkills(entity);

            return abilitiesSkillsEntity;
        }
        public bool UpdateAbilitiesSkills(AbilitiesSkillsEntity entity)
        {
            return DAL_AbilitiesSkillsService.SP_Update_AbilitiesSkills(entity);
        }
        #endregion
        #region Language
        public int AddLanguage(LanguageEntity entity)
        {
            int? insertID = DAL_LanguagesService.SP_Add_Languages(entity);
            return insertID.Value;
        }
        public List<LanguageEntity> GetLanguages(LanguageEntity entity)
        {
            List<LanguageEntity> languages = new List<LanguageEntity>();

            languages = DAL_LanguagesService.SP_GetList_Languages(entity);

            return languages;
        }
        public bool UpdateLanguages(LanguageEntity entity)
        {
            return DAL_LanguagesService.SP_Update_Languages(entity);
        }
        #endregion
        #region UniversityDegree
        public int AddUniversityDegree(UniversityDegreeEntity entity)
        {
            int? insertID = DAL_UniversityDegreeService.SP_Add_UniversityDegree(entity);

            // add 
            foreach (EducationAwardsEntity award in entity.Awardes)
            {
                int? tempid = DAL_UniversityDegreeService.SP_Add_EducationAwards(award);
                if (tempid == null) insertID = null;
            }

            return insertID.Value;
        }
        public bool AddEducationAwards(List<EducationAwardsEntity> list)
        {
            bool status = true;
            // add 
            foreach (EducationAwardsEntity award in list)
            {
                int? tempid = DAL_UniversityDegreeService.SP_Add_EducationAwards(award);
                if (tempid == null) status = false;
            }
            return status;
        }
        public UniversityDegreeEntity GetUniversityDegree(UniversityDegreeEntity entity)
        {
            UniversityDegreeEntity universityDegree = new UniversityDegreeEntity();

            universityDegree = DAL_UniversityDegreeService.SP_GetList_UniversityDegree(entity);

            universityDegree.Awardes = DAL_UniversityDegreeService.SP_GetList_EducationAwards(PrisonerID: entity.PrisonerID);

            return universityDegree;
        }

        public bool UpdateUniversityDegree(UniversityDegreeEntity entity)
        {
            return DAL_UniversityDegreeService.SP_Update_UniversityDegree(entity);
        }
        #endregion
        #endregion

        #region Files
        public List<AdditionalFileEntity> GetFiles(int? PersonID = null, FileType? TypeID = null, int? PrisonerID = null)
        {
            AdditionalFileEntity curEntity = new AdditionalFileEntity();
            curEntity.PersonID = PersonID;
            curEntity.PrisonerID = PrisonerID;
            curEntity.TypeID = TypeID;
            return DAL_FileService.SP_GetList_Files(curEntity);
        }
        public bool UpdateFiles(AdditionalFileEntity entity)
        {
            return DAL_FileService.SP_Update_Files(entity);
        }
        #endregion

        #region Camera Card
        public int AddCameraCardItem(CameraCardItemEntity entity)
        {
            int? insertID = DAL_CameraCardService.SP_Add_CameraCard(entity);
            return insertID.Value;
        }
        public List<CameraCardItemEntity> GetCameraCardItems(CameraCardItemEntity entity)
        {
            List<CameraCardItemEntity> cameraCardItems = new List<CameraCardItemEntity>();

            cameraCardItems = DAL_CameraCardService.SP_GetList_CameraCard(entity);

            return cameraCardItems;
        }
        public CameraCardEntity GetCamerCards(CameraCardEntity entity)
        {
            CameraCardEntity cameraCards = new CameraCardEntity(entity.PrisonerID);

            cameraCards.CameraCardItem = GetCameraCardItems(new CameraCardItemEntity(PrisonerID: entity.PrisonerID));
            cameraCards.Files = GetFiles(PrisonerID: entity.PrisonerID.Value, TypeID: FileType.CAMERA_CARD_ATTACHED_FILES);

            return cameraCards;
        }
        public bool UpdateCameraCard(CameraCardItemEntity entity)
        {
            return DAL_CameraCardService.SP_Update_CameraCard(entity);
        }
        #endregion

        #region Items and Objects
        public int AddItemObject(ItemEntity entity)
        {
            int? insertID = DAL_ItemsService.SP_Add_Items(entity);
            return insertID.Value;
        }
        public List<ItemEntity> GetItems(ItemEntity entity)
        {
            List<ItemEntity> items = new List<ItemEntity>();

            items = DAL_ItemsService.SP_GetList_Items(entity);

            return items;
        }
        public ItemObjectEntity GetItemObject(int PrisonerID)
        {
            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();
         
            ItemObjectEntity itemObject = new ItemObjectEntity();
            itemObject.Items = GetItems(new ItemEntity(PrisonerID: PrisonerID));
            itemObject.Files = GetFiles(PrisonerID: PrisonerID, TypeID: FileType.ITEMS_ATTACHED_FILES);

            return itemObject;
        }
        public bool UpdateItemObject(ItemEntity entity)
        {
            return DAL_ItemsService.SP_Update_Items(entity);
        }
        #endregion

        #region Injunctions
        public int AddInjunctionItem(InjunctionItemEntity entity)
        {
            int? insertID = DAL_InjunctionsService.SP_Add_Injunctions(entity);
            if (entity.Objects != null && entity.Objects.Any() && insertID != null)
            {
                foreach (var item in entity.Objects)
                {
                    item.InjunctionID = insertID;
                    int? tempID = DAL_InjunctionsService.SP_Add_InjunctionObject(item);
                }
            }
            return insertID.Value;
        }
        public List<InjunctionItemEntity> GetInjunctionItem(InjunctionItemEntity entity)
        {
            List<InjunctionItemEntity> items = new List<InjunctionItemEntity>();

            items = DAL_InjunctionsService.SP_GetList_Injunctions(entity);

            foreach (InjunctionItemEntity item in items)
            {
                item.Objects = DAL_InjunctionsService.SP_GetList_InjunctionObjects(new InjunctionItemObjectEntity(InjunctionID: item.ID));
            }

            return items;
        }
        public InjunctionsEntity GetInjunctions(int PrisonerID)
        {
            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();
          
            InjunctionsEntity injunctions = new InjunctionsEntity();
            injunctions.Items = GetInjunctionItem(new InjunctionItemEntity(PrisonerID: PrisonerID));
            injunctions.Files = GetFiles(PrisonerID: PrisonerID, TypeID: FileType.INJUNCTIONS_ATTACHED_FILES);

            return injunctions;
        }
        public bool UpdateInjunctions(InjunctionItemEntity entity)
        {
            bool status = false;

            status = DAL_InjunctionsService.SP_Update_Injunctions(entity);

            if (entity.Objects != null)
            {
                List<InjunctionItemObjectEntity> newList = entity.Objects;
                List<InjunctionItemObjectEntity> oldList = DAL_InjunctionsService.SP_GetList_InjunctionObjects(new InjunctionItemObjectEntity(InjunctionID: entity.ID));

                foreach (InjunctionItemObjectEntity item in newList)
                {
                    if (!oldList.FindAll(r=>r.LibItem_ID == item.LibItem_ID).Any())
                    {
                        item.InjunctionID = entity.ID;
                        int? tempID = DAL_InjunctionsService.SP_Add_InjunctionObject(item);
                        if (tempID == null) status = false;
                    }
                }

                foreach (InjunctionItemObjectEntity item in oldList)
                {
                    if (!newList.FindAll(r => r.LibItem_ID == item.LibItem_ID).Any())
                    {
                        item.Status = false;
                        bool? tempStatus = DAL_InjunctionsService.SP_Update_InjunctionObject(item);
                        if (tempStatus == null || tempStatus == false) status = false;
                    }
                }
            }

            return status;
        }
        #endregion

        #region Military Service
        public void AddMilitaryServiceData(MilitaryServiceEntity entity)
        {
            if(entity.ArmyAwards != null)
            {
                foreach(ArmyEntity ae in entity.Army)
                {
                    AddArmy(ae);
                }
            }
            if(entity.ArmyAwards != null)
            {
                foreach (ArmyAwardEntity aae in entity.ArmyAwards)
                {
                    AddArmyAwards(aae);
                }
            }
            if (entity.NotServed != null)
            {
                AddNotServed(entity.NotServed);
            }
            if(entity.WarInvolved != null)
            {
                foreach (WarInvolvedEntity wie in entity.WarInvolved)
                {
                    AddWarInvolved(wie);
                }                
            }
            if(entity.Files != null)
            {
                foreach (AdditionalFileEntity afe in entity.Files)
                {

                }
            }

        }

        public MilitaryServiceEntity GetMilitaryServiceData(int PrisonerID)
        {
            MilitaryServiceEntity militaryServiceData = new MilitaryServiceEntity();

            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();
            
            militaryServiceData.Army = GetArmy(new ArmyEntity(PrisonerID: PrisonerID));
            militaryServiceData.ArmyAwards = GetArmyAwards(PrisonerID);
            militaryServiceData.NotServed = GetNotServed(new NotServedEntity(PrisonerID: PrisonerID));
            militaryServiceData.WarInvolved = GetWarInvolved(new WarInvolvedEntity(PrisonerID: PrisonerID));
            militaryServiceData.Files = GetFiles(PersonID: currentPrisoner.PersonID.Value, TypeID: FileType.ARMY_ATTACHED_FILES);

            return militaryServiceData;
        }

        #region Army
        public int AddArmy(ArmyEntity armyEntity)
        {
            int? newItemID = DAL_ArmyService.SP_Add_Army(armyEntity);
            return newItemID.Value;
        }
        
        public List<ArmyEntity> GetArmy(ArmyEntity entity)
        {
            return DAL_ArmyService.SP_GetList_Army(entity);
        }
        public bool UpdateArmy(ArmyEntity Army)
        {
            return DAL_ArmyService.SP_Update_Army(Army);
        }
        public bool UpdateInjunctions(ArmyEntity entity)
        {
            return DAL_ArmyService.SP_Update_Army(entity);
        }
        #endregion
        #region Army Awards
        public int AddArmyAwards(ArmyAwardEntity armyAwardsEntity)
        {
            int? newItemID = DAL_ArmyAwardsService.SP_Add_ArmyAwards(armyAwardsEntity);
            return newItemID.Value;
        }

        public List<ArmyAwardEntity> GetArmyAwards(int PrisonerID)
        {
            return DAL_ArmyAwardsService.SP_GetList_ArmyAwards(new ArmyAwardEntity( PrisonerID: PrisonerID));
        }
        public bool UpdateArmyAwards(ArmyAwardEntity entity)
        {
            return DAL_ArmyAwardsService.SP_Update_ArmyAwards(entity);
        }
        #endregion
        #region Not Served
        public int AddNotServed(NotServedEntity entity)
        {
            int? newItemID = DAL_NotServedService.SP_Add_NotServed(entity);

            return newItemID.Value;
        }     
        public NotServedEntity GetNotServed(NotServedEntity entity)
        {
            List<NotServedEntity> items = DAL_NotServedService.SP_GetList_NotServed(entity);

            if (items.Any())
                return items.First();

            return new NotServedEntity();
        }
        public bool UpdateNotServed(NotServedEntity entity)
        {
            return DAL_NotServedService.SP_Update_NotServed(entity);
        }
        #endregion
        #region War Involved
        public int AddWarInvolved(WarInvolvedEntity entity)
        {
            int? newItemID = DAL_WarInvolvedService.SP_Add_WarInvolved(entity);

            return newItemID.Value;
        }
        public List<WarInvolvedEntity> GetWarInvolved(WarInvolvedEntity entity)
        {
            List<WarInvolvedEntity> items = DAL_WarInvolvedService.SP_GetList_WarInvolved(entity);
            
            return items;
        }
        public bool UpdateWarInvolved(WarInvolvedEntity entity)
        {
            return DAL_WarInvolvedService.SP_Update_WarInvolved(entity);
        }
        #endregion
        #endregion

        #region Adoption Information
        #region ArrestData
        public int AddArrestData(ArrestDataEntity entity)
        {
            int? insertID = DAL_ArrestDataService.SP_Add_ArrestData(entity);

            if (insertID != null && entity.ArrestDataProceedings != null && entity.ArrestDataProceedings.Any())
            {
                foreach (ArrestDataProceedingEntity item in entity.ArrestDataProceedings)
                {
                    if (item.ProceedLibItemID != null && item.Date != null)
                    {
                        item.ArrestDataID = insertID;
                        int? tempid = DAL_ArrestDataService.SP_Add_ArrestDataProceeding(item);
                    }
                }
            }
            return insertID.Value;
        }
        public  List<ArrestDataEntity> GetArrestData(ArrestDataEntity entity)
        {
            List<ArrestDataEntity> list = DAL_ArrestDataService.SP_GetList_ArrestData(entity);
            foreach (ArrestDataEntity item in list)
            {
                item.ArrestDataProceedings = DAL_ArrestDataService.SP_GetList_ArrestDataProceedings(new ArrestDataProceedingEntity(ArrestDataID: item.ID));
            }

            return list;
        }
        public bool UpdateArrestData(ArrestDataEntity entity)
        {
            bool status = true;
            List<ArrestDataProceedingEntity> newList = entity.ArrestDataProceedings ?? new List<ArrestDataProceedingEntity>();
            if (entity.ID != null)
            {
                List<ArrestDataProceedingEntity> oldList = GetArrestDataProceedings(new ArrestDataProceedingEntity(ArrestDataID: entity.ID)) ?? new List<ArrestDataProceedingEntity>();
                foreach (ArrestDataProceedingEntity item in oldList)
                {
                    if (!newList.FindAll(r => r.ProceedLibItemID == item.ProceedLibItemID && r.Date == item.Date).Any())
                    {
                        status = UpdateArrestDataProceeding(new ArrestDataProceedingEntity(ID: item.ID, Status: false));
                    }
                }

                foreach (ArrestDataProceedingEntity item in newList)
                {
                    if (!oldList.FindAll(r => r.ProceedLibItemID == item.ProceedLibItemID && r.Date == item.Date).Any())
                    {
                        item.ArrestDataID = entity.ID;
                        int? tempid = AddArrestDataProceeding(item);
                        if (tempid == null) status = false;
                    }
                }
            }

            if (entity.SentencingDataArticles != null)
            {
                SentencingDataArticleLibsEntity tempEntity = new SentencingDataArticleLibsEntity();

                tempEntity.ArrestDataID = entity.ID;

                List<SentencingDataArticleLibsEntity> oldList = DAL_SentencingDataService.SP_GetList_SentencingDataArticleLibs(tempEntity);

                foreach (SentencingDataArticleLibsEntity curEntity in oldList)
                {
                    if (!entity.SentencingDataArticles.FindAll(r => r.LibItem_ID == curEntity.LibItem_ID).Any())
                    {
                        curEntity.Status = false;
                        bool tempstatus = DAL_SentencingDataService.SP_Update_SentencingDataArticleLibs(curEntity);
                        if (!tempstatus) status = false;
                    }
                }

                foreach (SentencingDataArticleLibsEntity curEntity in entity.SentencingDataArticles)
                {
                    if (!oldList.FindAll(r => r.LibItem_ID == curEntity.LibItem_ID).Any())
                    {
                        curEntity.ArrestDataID = entity.ID;
                        int? id = DAL_SentencingDataService.SP_Add_SentencingDataArticleLibs(curEntity);
                        if (id == null) status = false;
                    }
                }
            }

            status = DAL_ArrestDataService.SP_Update_ArrestData(entity);

            return status;
        }
        #endregion

        #region PrisonerTypeStatus
        public int? AddPrisonerTypeStatus(PrisonerTypeStatusEntity entity)
        {
            return DAL_PrisonerTypeStatusService.SP_Add_PrisonerTypeStatus(entity);
        }
        public List<PrisonerTypeStatusEntity> GetPrisonerTypeStatus(PrisonerTypeStatusEntity entity)
        {
            return DAL_PrisonerTypeStatusService.SP_GetList_PrisonerTypeStatus(entity);
        }
        public bool? UpdatePrisonerTypeStatus(PrisonerTypeStatusEntity entity)
        {
            return DAL_PrisonerTypeStatusService.SP_Update_PrisonerTypeStatus(entity);
        }
        #endregion
        #region ArrestDataProceedings
        public int AddArrestDataProceeding(ArrestDataProceedingEntity entity)
        {
            int? insertID = DAL_ArrestDataService.SP_Add_ArrestDataProceeding(entity);
            return insertID.Value;
        }
        public List<ArrestDataProceedingEntity> GetArrestDataProceedings(ArrestDataProceedingEntity entity)
        {
            return DAL_ArrestDataService.SP_GetList_ArrestDataProceedings(entity);

        }
        public bool UpdateArrestDataProceeding(ArrestDataProceedingEntity entity)
        {
            return DAL_ArrestDataService.SP_Update_ArrestDataProceedings(entity);
        }
        #endregion
        #region PrisonAccessProtocol
        public int AddPrisonAccessProtocol(PrisonAccessProtocolEntity entity)
        {
            int? insertID = DAL_PrisonAccessProtocolService.SP_Add_PrisonAccessProtocol(entity);
            return insertID.Value;
        }
        public List<PrisonAccessProtocolEntity> GetPrisonAccessProtocol(PrisonAccessProtocolEntity entity)
        {
            return DAL_PrisonAccessProtocolService.SP_GetList_PrisonAccessProtocol(entity);

        }
        public bool UpdatePrisonAccessProtocol(PrisonAccessProtocolEntity entity)
        {
            return DAL_PrisonAccessProtocolService.SP_Update_PrisonAccessProtocol(entity);
        }
        #endregion
        #region SentencingData
        public int AddSentencingData(SentencingDataEntity entity)
        {
            int? insertID = DAL_SentencingDataService.SP_Add_SentencingData(entity);
            return insertID.Value;
        }
        public List<SentencingDataEntity> GetSentencingData(SentencingDataEntity entity)
        {
            return DAL_SentencingDataService.SP_GetList_SentencingData(entity);

        }
        public bool UpdateSentencingData(SentencingDataEntity entity)
        {
            bool status = true;

            if (entity.SentencingDataArticles != null)
            {
                SentencingDataArticleLibsEntity tempEntity = new SentencingDataArticleLibsEntity();

                tempEntity.SentencingDataID = entity.ID;

                List<SentencingDataArticleLibsEntity> oldList = DAL_SentencingDataService.SP_GetList_SentencingDataArticleLibs(tempEntity);

                foreach (SentencingDataArticleLibsEntity curEntity in oldList)
                {
                    if (!entity.SentencingDataArticles.FindAll(r => r.LibItem_ID == curEntity.LibItem_ID).Any())
                    {
                        curEntity.Status = false;
                        bool tempstatus = DAL_SentencingDataService.SP_Update_SentencingDataArticleLibs(curEntity);
                        if (!tempstatus) status = false;
                    }
                }

                foreach (SentencingDataArticleLibsEntity curEntity in entity.SentencingDataArticles)
                {
                    if (!oldList.FindAll(r => r.LibItem_ID == curEntity.LibItem_ID).Any())
                    {
                        curEntity.SentencingDataID = entity.ID;
                        int? id = DAL_SentencingDataService.SP_Add_SentencingDataArticleLibs(curEntity);
                        if (id == null) status = false;
                    }
                }
            }

            status = DAL_SentencingDataService.SP_Update_SentencingData(entity);

            return status;
        }

        #endregion
        #region SentencingDataArticles
        public int AddSentencingDataArticle(SentencingDataArticleLibsEntity entity)
        {
            int? insertID = DAL_SentencingDataService.SP_Add_SentencingDataArticleLibs(entity);
            return insertID.Value;
        }
        public List<SentencingDataArticleLibsEntity> GetSentencingDataArticles(SentencingDataArticleLibsEntity entity)
        {
            return DAL_SentencingDataService.SP_GetList_SentencingDataArticleLibs(entity);

        }
        public bool UpdateSentencingDataArticle(SentencingDataArticleLibsEntity entity)
        {
            return DAL_SentencingDataService.SP_Update_SentencingDataArticleLibs(entity);
        }
        public  void ChangeSentencingDataArticles(SentencingDataEntity entity)
        {
            List<SentencingDataArticleLibsEntity> fromDB =  GetSentencingDataArticles(new SentencingDataArticleLibsEntity(SentencingDataID: entity.ID));

            IEnumerable<SentencingDataArticleLibsEntity> forAdd = fromDB.Except(entity.SentencingDataArticles);
            IEnumerable<SentencingDataArticleLibsEntity> forRemove = entity.SentencingDataArticles.Except(fromDB);

            foreach (SentencingDataArticleLibsEntity ent in forAdd)
                AddSentencingDataArticle(ent);

            foreach (SentencingDataArticleLibsEntity ent in forRemove)
            {
                ent.Status = false;
                UpdateSentencingDataArticle(ent);
            }

        }

        #endregion
        #endregion

        #region Family Relations
        public int AddFamilyRelations(FamilyRelativeEntity entity)
        {
            int? insertID = DAL_FamilyRelativesService.SP_Add_FamilyRelatives(entity);
            
            return insertID.Value;
        }
        public FamilyRelativeEntity GetFamilyRelations(FamilyRelativeEntity entity)
        {
            List<FamilyRelativeEntity> list = DAL_FamilyRelativesService.SP_GetList_FamilyRelatives(entity);
            if (list.Any())
            {
                return list.Last();
            }
            return new FamilyRelativeEntity();
        }
        public bool UpdateFamilyRelations(FamilyRelativeEntity entity)
        {
            return DAL_FamilyRelativesService.SP_Update_FamilyRelatives(entity);
        }
        #region Spouses
        public int AddSpouses(SpousesEntity entity)
        {
            int? insertID = null;
            if (entity.Person.ID != null)
            {
                entity.PersonID = entity.Person.ID;
                insertID = DAL_SpousesService.SP_Add_Spouses(entity);
            }
            return insertID.Value;
        }
        public List<SpousesEntity> GetSpouses(SpousesEntity entity)
        {
            List<SpousesEntity> list = DAL_SpousesService.SP_GetList_Spouses(entity);
            if(list.Any())
            {
                for (int i = 0; i < list.Count; i++)
                    list[i].Person = GetPerson(PersonID: list[i].PersonID.Value, Full: true);
            }
            return list;
        }
        public bool UpdateSpouses(SpousesEntity entity)
        {
            if (entity.Person != null)
            {
                UpdatePerson(entity.Person);
            }
            return DAL_SpousesService.SP_Update_Spouses(entity);
        }
        #endregion
        #region Children
        public int AddChildren(ChildrenEntity entity)
        {
            int? insertID = null;
            if (entity.Person.ID != null)
            {
                entity.PersonID = entity.Person.ID;
                insertID = DAL_ChildrenService.SP_Add_Children(entity);

            }
            return insertID.Value;
        }
        public List<ChildrenEntity> GetChildren(ChildrenEntity entity)
        {
            List<ChildrenEntity> list = DAL_ChildrenService.SP_GetList_Children(entity);
            if (list.Any())
            {
                for (int i = 0; i < list.Count; i++)
                    list[i].Person = GetPerson(PersonID: list[i].PersonID.Value, Full: true);
            }
            return list;

        }
        public bool UpdateChildren(ChildrenEntity entity)
        {
            if (entity.Person != null)
            {
                UpdatePerson(entity.Person);
            }
            return DAL_ChildrenService.SP_Update_Children(entity);
        }
        #endregion
        #region Parents
        public int AddParents(ParentsEntity entity)
        {

            int? insertID = null;
            if (entity.Person.ID != null)
            {
                entity.PersonID = entity.Person.ID;
                insertID = DAL_ParentsService.SP_Add_Parents(entity);

            }
            return insertID.Value;
        }
        public List<ParentsEntity> GetParents(ParentsEntity entity)
        {
            List<ParentsEntity> list = DAL_ParentsService.SP_GetList_Parents(entity);
            if (list.Any())
            {
                for (int i = 0; i < list.Count; i++)
                {
                    list[i].Person = GetPerson(PersonID: list[i].PersonID.Value, Full: true);
                    //list[i].RelatedPerson = GetPerson(list[i].RelatedPersonID.Value);
                }
            }
            return list;

        }

        public List<PersonEntity> GetParentRelatedPersons(int PrisonerID)
        {
            List<PersonEntity> persons = DAL_ParentsService.SP_GetList_ParentRelatedPersons(PrisonerID);
            
            return persons;
        }

        public bool UpdateParents(ParentsEntity entity)
        {
            if (entity.Person != null)
            {
                UpdatePerson(entity.Person);
            }
            return DAL_ParentsService.SP_Update_Parents(entity);
        }

        #endregion
        #region Sister Brothers
        public int AddSisterBrother(SisterBrotherEntity entity)
        {
            int? insertID = null;
            if (entity.Person.ID != null)
            {
                entity.PersonID = entity.Person.ID;
                insertID = DAL_SisterBrotherService.SP_Add_SisterBrother(entity);

            }
            return insertID.Value;
        }
        public List<SisterBrotherEntity> GetSisterBrother(SisterBrotherEntity entity)
        {
            List<SisterBrotherEntity> list = DAL_SisterBrotherService.SP_GetList_SisterBrother(entity);
            if (list.Any())
            {
                for (int i = 0; i < list.Count; i++)
                    list[i].Person = GetPerson(PersonID: list[i].PersonID.Value, Full: true);
            }
            return list;
        }
        public bool UpdateSisterBrother(SisterBrotherEntity entity)
        {
            if (entity.Person != null)
            {
                UpdatePerson(entity.Person);
            }
            return DAL_SisterBrotherService.SP_Update_SisterBrother(entity);
        }
        #endregion
        #region Other Relatives
        public int AddOtherRelatives(OtherRelativesEntity entity)
        {
            int? insertID = null;
            if (entity.Person.ID != null)
            {
                entity.PersonID = entity.Person.ID;
                insertID = DAL_OtherRelativesService.SP_Add_OtherRelatives(entity);

            }
            return insertID.Value;
        }
        public List<OtherRelativesEntity> GetOtherRelatives(OtherRelativesEntity entity)
        {
            List<OtherRelativesEntity> list = DAL_OtherRelativesService.SP_GetList_OtherRelatives(entity);
            if (list.Any())
            {
                for (int i = 0; i < list.Count; i++)
                    list[i].Person = GetPerson(PersonID: list[i].PersonID.Value, Full: true);
            }
            return list;
        }
        public bool UpdateOtherRelatives(OtherRelativesEntity entity)
        {
            if (entity.Person != null)
            {
                UpdatePerson(entity.Person);
            }
            return DAL_OtherRelativesService.SP_Update_OtherRelatives(entity);
        }
        #endregion

        #endregion

        #region CaseClose
        public int AddCaseClose(CaseCloseEntity entity)
        {
            int? insertID = DAL_CloseCaseService.SP_Add_CaseClose(entity);
            return insertID.Value;
        }
        public List<CaseCloseEntity> GetCaseClose(CaseCloseEntity entity)
        {
            return DAL_CloseCaseService.SP_GetList_CaseClose(entity);
        }
        public List<CaseCloseEntity> GetActiveCase(CaseCloseEntity entity)
        {
            return DAL_CloseCaseService.SP_GetList_ActiveCase(entity);
        }
        #endregion

        #region CaseOpen
        public int? AddCaseOpen(CaseOpenEntity entity)
        {
            int? insertID = DAL_CaseOpenService.SP_Add_CaseOpen(entity);
            return insertID;
        }
        public List<CaseOpenEntity> GetCaseOpen(CaseOpenEntity entity)
        {
            return DAL_CaseOpenService.SP_GetList_CaseOpen(entity);
        }
        #endregion

        #region Encouragements
        public int? AddEncouragement(EncouragementsEntity entity)
        {
            int? insertID = DAL_EncouragementService.SP_Add_Encouragement(entity);
            return insertID;
        }
        public List<EncouragementsEntity> GetEncouragements(EncouragementsEntity entity)
        {
            return DAL_EncouragementService.SP_GetList_Encouragements(entity);
        }
        public bool UpdateEncouragements(EncouragementsEntity entity)
        {
            return DAL_EncouragementService.SP_Update_Encouragement(entity);
        }
        public EncouragementsObjectEntity GetEncouragement(int PrisonerID)
        {
            EncouragementsObjectEntity returnEntity = new EncouragementsObjectEntity();
            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();
            returnEntity.PrisonerID = PrisonerID;
            returnEntity.Encouragements = DAL_EncouragementService.SP_GetList_Encouragements(new EncouragementsEntity(PrisonerID: PrisonerID));
            AdditionalFileEntity tempFile = new AdditionalFileEntity();
            tempFile.PrisonerID = PrisonerID;
            tempFile.TypeID = FileType.ENCOURAGEMENTS;
            returnEntity.Files = DAL_FileService.SP_GetList_Files(tempFile);
            return returnEntity;
        }
        #endregion

        #region Penalties
        public int? AddPenalty(PenaltiesEntity entity)
        {
            int? insertID = DAL_PenaltiesService.SP_Add_Penalty(entity);
            return insertID;
        }
        public List<PenaltiesEntity> GetPenalties(PenaltiesEntity entity)
        {
            List<PenaltiesEntity> PenaltyList = DAL_PenaltiesService.SP_GetList_Penalties(entity);

            for (int i = 0; i < PenaltyList.Count; i++)
            {
                PenaltyList[i].ViolationList = DAL_PenaltiesService.SP_GetList_ViolationLibItemToPenalty(new PenaltiesViolationEntity(PenaltyID: PenaltyList[i].ID));
                PenaltyList[i].ProtestList = DAL_PenaltiesService.SP_GetList_PenaltyProtest(new PenaltiesProtestEntity(PenaltyID: PenaltyList[i].ID));
            }

            return PenaltyList;
        }
        public bool UpdatePenalties(PenaltiesEntity entity)
        {
            return DAL_PenaltiesService.SP_Update_Penalty(entity);
        }
        public PenaltiesObjectEntity GetPenalty(int PrisonerID)
        {
            PenaltiesObjectEntity returnEntity = new PenaltiesObjectEntity();
            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();
            returnEntity.PrisonerID = PrisonerID;
            returnEntity.Penalties = DAL_PenaltiesService.SP_GetList_Penalties(new PenaltiesEntity(PrisonerID: PrisonerID));
            for (int i = 0; i < returnEntity.Penalties.Count; i++)
            {
                returnEntity.Penalties[i].ViolationList = DAL_PenaltiesService.SP_GetList_ViolationLibItemToPenalty(new PenaltiesViolationEntity(PenaltyID: returnEntity.Penalties[i].ID));
                returnEntity.Penalties[i].ProtestList = DAL_PenaltiesService.SP_GetList_PenaltyProtest(new PenaltiesProtestEntity(PenaltyID: returnEntity.Penalties[i].ID));
            }
            AdditionalFileEntity tempFile = new AdditionalFileEntity();
            tempFile.PrisonerID = PrisonerID;
            tempFile.TypeID = FileType.PENALTIES;
            returnEntity.Files = DAL_FileService.SP_GetList_Files(tempFile);
            return returnEntity;
        }
        public int? AddViolationLibItemToPenalty(PenaltiesViolationEntity entity)
        {
            int? insertID = DAL_PenaltiesService.SP_Add_ViolationLibItemToPenalty(entity);
            return insertID;
        }
        public List<PenaltiesViolationEntity> GetViolationLibItemToPenalty(PenaltiesViolationEntity entity)
        {
            return DAL_PenaltiesService.SP_GetList_ViolationLibItemToPenalty(entity);
        }
        public bool UpdateViolationLibItemToPenalty(PenaltiesViolationEntity entity)
        {
            return DAL_PenaltiesService.SP_Update_ViolationLibItemToPenalty(entity);
        }
        public int? AddPenaltyProtest(PenaltiesProtestEntity entity)
        {
            int? insertID = DAL_PenaltiesService.SP_Add_PenaltyProtest(entity);
            return insertID;
        }
        public List<PenaltiesProtestEntity> GetPenaltyProtest(PenaltiesProtestEntity entity)
        {
            return DAL_PenaltiesService.SP_GetList_PenaltyProtest(entity);
        }
        public bool UpdatePenaltyProtest(PenaltiesProtestEntity entity)
        {
            return DAL_PenaltiesService.SP_Update_PenaltyProtest(entity);
        }

        #endregion

        #region OfficialVisits
        public int? AddOfficialVisit(OfficialVisitsEntity entity)
        {
            int? insertID = DAL_OfficialVisitsService.SP_Add_OfficialVisit(entity);
            return insertID;
        }
        public List<OfficialVisitsEntity> GetOfficialVisits(OfficialVisitsEntity entity)
        {
            return DAL_OfficialVisitsService.SP_GetList_OfficialVisits(entity);
        }
        public bool UpdateOfficialVisit(OfficialVisitsEntity entity)
        {
            if (entity.Person != null)
            {
                UpdatePerson(entity.Person);
            }
            return DAL_OfficialVisitsService.SP_Update_OfficialVisit(entity);
        }
        public OfficialVisitsObjectEntity GetOfficialVisit(int PrisonerID)
        {
            OfficialVisitsObjectEntity returnEntity = new OfficialVisitsObjectEntity();
            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();
            returnEntity.PrisonerID = PrisonerID;
            returnEntity.Visits = DAL_OfficialVisitsService.SP_GetList_OfficialVisits(new OfficialVisitsEntity(PrisonerID: PrisonerID));
            AdditionalFileEntity tempFile = new AdditionalFileEntity();
            tempFile.PrisonerID = PrisonerID;
            tempFile.TypeID = FileType.OFFICIAL_VISITS;
            returnEntity.Files = DAL_FileService.SP_GetList_Files(tempFile);
            return returnEntity;
        }
        #endregion

        #region Departure
        public int? AddDeparture(DepartureEntity entity)
        {
            int? insertID = DAL_DepartureService.SP_Add_Departure(entity);
            return insertID;
        }
        public List<DepartureEntity> GetDeparture(DepartureEntity entity)
        {
            return DAL_DepartureService.SP_GetList_Departure(entity);
        }
        public bool UpdateDeparture(DepartureEntity entity)
        {
            return DAL_DepartureService.SP_Update_Departure(entity);
        }
        public DepartureObjectEntity GetDeparture(int PrisonerID)
        {
            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();
            DepartureObjectEntity returnEntity = new DepartureObjectEntity();
            returnEntity.PrisonerID = PrisonerID;
            returnEntity.Departures = DAL_DepartureService.SP_GetList_Departure(new DepartureEntity(PrisonerID: PrisonerID));
            AdditionalFileEntity tempFile = new AdditionalFileEntity();
            tempFile.PersonID = currentPrisoner.PersonID;
            tempFile.TypeID = FileType.DEPARTURES;
            returnEntity.Files = DAL_FileService.SP_GetList_Files(tempFile);
            return returnEntity;
        }
        #endregion

        #region Transfer
        public int? AddTransfer(TransferEntity entity)
        {
            int? insertID = DAL_TransferService.SP_Add_Transfer(entity);
            return insertID;
        }
        public List<TransferEntity> GetTransfer(TransferEntity entity)
        {
            return DAL_TransferService.SP_GetList_Trasnfer(entity);
        }
        public bool UpdateTransfer(TransferEntity entity)
        {
            if (entity.Person != null)
            {
                UpdatePerson(entity.Person);
            }
            return DAL_TransferService.SP_Update_Transfer(entity);
        }
        public TransferObjectEntity GetTransfer(int PrisonerID)
        {
            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();
            TransferObjectEntity returnEntity = new TransferObjectEntity();
            returnEntity.PrisonerID = PrisonerID;
            returnEntity.Transfers = DAL_TransferService.SP_GetList_Trasnfer(new TransferEntity(PrisonerID: PrisonerID));
            AdditionalFileEntity tempFile = new AdditionalFileEntity();
            tempFile.PrisonerID = PrisonerID;
            tempFile.TypeID = FileType.TRANSFERS;
            returnEntity.Files = DAL_FileService.SP_GetList_Files(tempFile);
            return returnEntity;
        }
        #endregion

        #region PersonalVisits
        public int? AddPersonalVisit(PersonalVisitsEntity entity)
        {
            int? insertID = DAL_PersonalVisitsService.SP_Add_PersonalVisits(entity);
            return insertID;
        }
        public List<PersonalVisitsEntity> GetPersonalVisits(PersonalVisitsEntity entity)
        {
            return DAL_PersonalVisitsService.SP_GetList_PersonalVisits(entity);
        }
        public bool UpdatePersonalVisit(PersonalVisitsEntity entity)
        {
            if (entity.Person != null)
            {
                UpdatePerson(entity.Person);
            }
            return DAL_PersonalVisitsService.SP_Update_PersonalVisits(entity);
        }
        public PersonalVisitsObjectEntity GetPersonalVisit(int PrisonerID)
        {
            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();
            PersonalVisitsObjectEntity returnEntity = new PersonalVisitsObjectEntity();
            returnEntity.PrisonerID = PrisonerID;
            returnEntity.Visits = DAL_PersonalVisitsService.SP_GetList_PersonalVisits(new PersonalVisitsEntity(PrisonerID: PrisonerID));
            AdditionalFileEntity tempFile = new AdditionalFileEntity();
            tempFile.PrisonerID = PrisonerID;
            tempFile.TypeID = FileType.PERSONAL_VISITS;
            returnEntity.Files = DAL_FileService.SP_GetList_Files(tempFile);
            return returnEntity;
        }
        #endregion

        #region EducationalCourses
        public int? AddEducationalCourse(EducationalCoursesEntity entity)
        {
            int? insertID = DAL_EducationalCoursesService.SP_Add_EducationalCourse(entity);
            return insertID;
        }
        public List<EducationalCoursesEntity> GetEducationalCourses(EducationalCoursesEntity entity)
        {
            return DAL_EducationalCoursesService.SP_GetList_EducationalCourses(entity);
        }
        public bool UpdateEducationalCourse(EducationalCoursesEntity entity)
        {
            return DAL_EducationalCoursesService.SP_Update_EducationalCourse(entity);
        }
        public EducationalCoursesObjectEntity GetEducationalCourse(int PrisonerID)
        {
            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();
            EducationalCoursesObjectEntity returnEntity = new EducationalCoursesObjectEntity();
            returnEntity.PrisonerID = PrisonerID;
            returnEntity.EducationCourses = DAL_EducationalCoursesService.SP_GetList_EducationalCourses(new EducationalCoursesEntity(PrisonerID: PrisonerID));
            AdditionalFileEntity tempFile = new AdditionalFileEntity();
            tempFile.PersonID = currentPrisoner.PersonID;
            tempFile.TypeID = FileType.EDUCATIONAL_COURSES;
            returnEntity.Files = DAL_FileService.SP_GetList_Files(tempFile);
            return returnEntity;
        }
        #endregion

        #region WorkLoads
        public int? AddWorkLoad(WorkLoadsEntity entity)
        {
            int? insertID = DAL_WorkLoadsService.SP_Add_WorkLoad(entity);
            return insertID;
        }
        public List<WorkLoadsEntity> GetWorkLoads(WorkLoadsEntity entity)
        {
            return DAL_WorkLoadsService.SP_GetList_WorkLoads(entity);
        }
        public bool UpdateWorkLoad(WorkLoadsEntity entity)
        {
            return DAL_WorkLoadsService.SP_Update_WorkLoad(entity);
        }
        public WorkLoadsObjectEntity GetWorkLoad(int PrisonerID)
        {
            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();
            WorkLoadsObjectEntity returnEntity = new WorkLoadsObjectEntity();
            returnEntity.PrisonerID = PrisonerID;
            returnEntity.WorkLoads = DAL_WorkLoadsService.SP_GetList_WorkLoads(new WorkLoadsEntity(PrisonerID: PrisonerID));
            AdditionalFileEntity tempFile = new AdditionalFileEntity();
            tempFile.PrisonerID = PrisonerID;
            tempFile.TypeID = FileType.WORKLOADS;
            returnEntity.Files = DAL_FileService.SP_GetList_Files(tempFile);
            return returnEntity;
        }
        #endregion

        #region Packages
        public int? AddPackage(PackagesEntity entity)
        {
            int? insertID = DAL_PackageService.SP_Add_Package(entity);
            return insertID;
        }
        public List<PackagesEntity> GetPackages(PackagesEntity entity)
        {
            List<PackagesEntity> PackageList = DAL_PackageService.SP_GetList_Packages(entity);

            for (int i = 0; i < PackageList.Count; i++)
            {
                PackageList[i].ContentList = DAL_PackageService.SP_GetList_PackageContent(new PackageContentEntity(PackageID: PackageList[i].ID));
            }

            return PackageList;
        }
        public bool UpdatePackage(PackagesEntity entity)
        {
            if (entity.ContentList != null)
            {
                List<PackageContentEntity> oldList = DAL_PackageService.SP_GetList_PackageContent(new PackageContentEntity(PackageID: entity.ID));

                if (oldList != null)
                {

                    foreach (PackageContentEntity curEntity in oldList)
                    {
                        if (!entity.ContentList.FindAll(r => r.ID == curEntity.ID).Any())
                        {
                            curEntity.Status = false;
                            bool tempstatus = DAL_PackageService.SP_Update_PackageContent(curEntity);
                        }
                    }

                    foreach (PackageContentEntity curEntity in entity.ContentList)
                    {
                        if (!oldList.FindAll(r => r.ID == curEntity.ID).Any())
                        {
                            curEntity.PackageID = entity.ID;
                            int? id = DAL_PackageService.SP_Add_PackageContent(curEntity);
                        }
                    }
                }
            }
            if (entity.Person != null)
            {
                UpdatePerson(entity.Person);
            }
            return DAL_PackageService.SP_Update_Package(entity);
        }
        public PackagesObjectEntity GetPackage(int PrisonerID)
        {
            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();
            PackagesObjectEntity returnEntity = new PackagesObjectEntity();
            returnEntity.PrisonerID = PrisonerID;
            returnEntity.Packages = DAL_PackageService.SP_GetList_Packages(new PackagesEntity(PrisonerID: PrisonerID));
            for (int i = 0; i < returnEntity.Packages.Count; i++)
            {
                returnEntity.Packages[i].ContentList = DAL_PackageService.SP_GetList_PackageContent(new PackageContentEntity(PackageID: returnEntity.Packages[i].ID));
            }
            AdditionalFileEntity tempFile = new AdditionalFileEntity();
            tempFile.PrisonerID = PrisonerID;
            tempFile.TypeID = FileType.PACKAGES;
            returnEntity.Files = DAL_FileService.SP_GetList_Files(tempFile);
            return returnEntity;
        }
        public int? AddPackageContent(PackageContentEntity entity)
        {
            int? insertID = DAL_PackageService.SP_Add_PackageContent(entity);
            return insertID;
        }
        public List<PackageContentEntity> GetPackageContent(PackageContentEntity entity)
        {
            return DAL_PackageService.SP_GetList_PackageContent(entity);
        }
        public bool UpdatePackageContent(PackageContentEntity entity)
        {
            return DAL_PackageService.SP_Update_PackageContent(entity);
        }
        #endregion

        #region Prisoner Archive
        public List<ArchiveEntity> GetPrisonerArchive(int PrisonerID)
        {
            return DAL_ArchiveService.SP_GetList_Archive(PrisonerID);
        }
        #endregion

        #region PreviousConviction
        public List<PreviousConvictionsEntity> GetPreviousConviction(PreviousConvictionsEntity entity)
        {
            int? PrisonerID = entity.PrisonerID;
            List<PreviousConvictionsEntity> list = new List<PreviousConvictionsEntity>();

            // current prev conviction list
            list = DAL_PreviousConvictionsService.SP_GetList_PreviousConvictions(new PreviousConvictionsEntity(ID:entity.ID, PrisonerID: PrisonerID));
            foreach (var item in list)
            {
                item.SentencingDataArticles = this.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(PrevConvictionDataID: item.ID));
            }
            PrisonerEntity currentPrisoner = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID).First();
            List<PrisonerEntity> prisonerList = DAL_PrisionersService.SP_GetList_Prisoners(Personal_ID: currentPrisoner.PersonID);
            if (prisonerList != null && prisonerList.Any() && entity.ID ==null)
            {
                foreach (PrisonerEntity item in prisonerList)
                {
                    if (item.ID != PrisonerID)
                    {
                        // prisoner's old sentences
                        List<SentencingDataEntity> sentenceList = DAL_SentencingDataService.SP_GetList_SentencingData(new SentencingDataEntity(PrisonerID: item.ID));
                        if (sentenceList != null && sentenceList.Any())
                        {
                            foreach (SentencingDataEntity subItem in sentenceList)
                            {
                                PreviousConvictionsEntity tempPrevConvictionEntity = new PreviousConvictionsEntity(PrisonerID: PrisonerID);
                                tempPrevConvictionEntity.CodeLibItemID = subItem.CodeLibItem_ID;
                                tempPrevConvictionEntity.CodeLibItemName = subItem.CodeLibItem_Label;
                                tempPrevConvictionEntity.EndDate = subItem.SentencingEndDate;
                                tempPrevConvictionEntity.StartDate = subItem.SentencingStartDate;
                                tempPrevConvictionEntity.PenaltyDay = subItem.PenaltyDay;
                                tempPrevConvictionEntity.PenaltyMonth = subItem.PenaltyMonth;
                                tempPrevConvictionEntity.PenaltyYear = subItem.PenaltyYear;
                                tempPrevConvictionEntity.SentencingDataArticles = this.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(SentencingDataID: subItem.ID));
                                tempPrevConvictionEntity.Archive = true;
                                list.Add(tempPrevConvictionEntity);
                            }
                        }

                        // prisoner's old prev convictions
                        List<PreviousConvictionsEntity> prevConvList = DAL_PreviousConvictionsService.SP_GetList_PreviousConvictions(new PreviousConvictionsEntity(PrisonerID: item.ID));
                        if (prevConvList != null && prevConvList.Any())
                        {
                            foreach (PreviousConvictionsEntity subItem in prevConvList)
                            {
                                subItem.Archive = true;
                                list.Add(subItem);
                            }
                        }
                    }
                }
            }
            if (entity.ID == null)
            {
                // prisoner current sentence
                List<SentencingDataEntity> curSentenceList = DAL_SentencingDataService.SP_GetList_SentencingData(new SentencingDataEntity(PrisonerID: PrisonerID));
                foreach (SentencingDataEntity subItem in curSentenceList)
                {
                    if (subItem.State != null && !subItem.State.Value)
                    {
                        PreviousConvictionsEntity tempPrevConvictionEntity = new PreviousConvictionsEntity(PrisonerID: PrisonerID);
                        tempPrevConvictionEntity.CodeLibItemID = subItem.CodeLibItem_ID;
                        tempPrevConvictionEntity.CodeLibItemName = subItem.CodeLibItem_Label;
                        tempPrevConvictionEntity.EndDate = subItem.SentencingEndDate;
                        tempPrevConvictionEntity.StartDate = subItem.SentencingStartDate;
                        tempPrevConvictionEntity.PenaltyDay = subItem.PenaltyDay;
                        tempPrevConvictionEntity.PenaltyMonth = subItem.PenaltyMonth;
                        tempPrevConvictionEntity.PenaltyYear = subItem.PenaltyYear;
                        tempPrevConvictionEntity.SentencingDataArticles = this.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(SentencingDataID: subItem.ID));
                        tempPrevConvictionEntity.Archive = true;
                        list.Add(tempPrevConvictionEntity);
                    }
                }
            }

            return list;
        }
        public int? AddPreviousConviction(PreviousConvictionsEntity prevConvEntity)
        {
            int? ID = DAL_PreviousConvictionsService.SP_Add_PreviousConvictions(prevConvEntity);
            
            return ID;
        }
        public bool? UpdatePreviousConviction(PreviousConvictionsEntity entity)
        {
            bool? status = true;

            if (entity.SentencingDataArticles != null)
            {
                SentencingDataArticleLibsEntity tempEntity = new SentencingDataArticleLibsEntity();

                tempEntity.PrevConvictionDataID = entity.ID;

                List<SentencingDataArticleLibsEntity> oldList = DAL_SentencingDataService.SP_GetList_SentencingDataArticleLibs(tempEntity);

                foreach (SentencingDataArticleLibsEntity curEntity in oldList)
                {
                    if (!entity.SentencingDataArticles.FindAll(r => r.LibItem_ID == curEntity.LibItem_ID).Any())
                    {
                        curEntity.Status = false;
                        bool tempstatus = DAL_SentencingDataService.SP_Update_SentencingDataArticleLibs(curEntity);
                        if (!tempstatus) status = false;
                    }
                }

                foreach (SentencingDataArticleLibsEntity curEntity in entity.SentencingDataArticles)
                {
                    if (!oldList.FindAll(r => r.LibItem_ID == curEntity.LibItem_ID).Any())
                    {
                        curEntity.PrevConvictionDataID = entity.ID;
                        int? id = DAL_SentencingDataService.SP_Add_SentencingDataArticleLibs(curEntity);
                        if (id == null) status = false;
                    }
                }
            }

            status = DAL_PreviousConvictionsService.SP_Update_PreviousConvictions(entity);

            return status;
        }
        #endregion

        #region Conflicts
        public int? AddConflict(ConflictsEntity entity)
        {
            int? insertID = null;

            insertID = DAL_ConflictsService.SP_Add_Conflict(entity);

            return insertID.Value;
        }
        public List<ConflictsEntity> GetConflicts(ConflictsEntity entity)
        {
            List<ConflictsEntity> curList = DAL_ConflictsService.SP_GetList_Conflicts(entity);
            foreach (var item in curList)
            {
                if (item.PersonID != null)
                {
                    item.Person = GetPerson(PersonID: item.PersonID, Full: true);
                }
            }
            return curList;
        }
        public List<ConflictsEntity> GetConflictsMain(ConflictsEntity entity)
        {
            return DAL_ConflictsService.SP_GetList_ConflictsMain(entity);
        }
        public bool? UpdateConflict(ConflictsEntity entity)
        {
            if (entity.Person != null)
            {
                UpdatePerson(entity.Person);
            }

            return DAL_ConflictsService.SP_Update_Conflict(entity);
        }
        
        public bool UpdateWorkLoad(ConflictsEntity entity)
        {
            return DAL_ConflictsService.SP_Update_Conflict(entity);
        }
        public ConflictsObjectEntity GetConflict(int PrisonerID)
        {
            ConflictsObjectEntity returnEntity = new ConflictsObjectEntity();
            returnEntity.PrisonerID = PrisonerID;
            int? PersonID = null;
            List<PrisonerEntity> curPrisoners = DAL_PrisionersService.SP_GetList_Prisoners(ID: PrisonerID);
            if (curPrisoners != null && curPrisoners.Any()) PersonID = curPrisoners.First().PersonID;
            returnEntity.Conflicts = DAL_ConflictsService.SP_GetList_Conflicts(new ConflictsEntity(PrisonerID: PrisonerID));
            returnEntity.ConflictsMain = DAL_ConflictsService.SP_GetList_ConflictsMain(new ConflictsEntity(PrisonerID: PrisonerID));
            foreach (var item in returnEntity.ConflictsMain)
            {
                if (item.PersonID != null)
                {
                    item.Person = GetPerson(PersonID: item.PersonID);
                }
            }
            //if (PersonID != null)
            //{
                AdditionalFileEntity tempFile = new AdditionalFileEntity();
                tempFile.PrisonerID = PrisonerID;
                tempFile.TypeID = FileType.CONFLICTS;
                returnEntity.Files = DAL_FileService.SP_GetList_Files(tempFile);
            //}
            return returnEntity;
        }
        #endregion

        #region PrisonerPunishmentType
        public int? AddPrisonerPunishmentType(PrisonerPunishmentTypeEntity entity)
        {
            return DAL_PrisonerPunishmentTypeService.SP_Add_PrisonerPunishmentType(entity);
        }
        public List<PrisonerPunishmentTypeEntity> GetPrisonerPunishmentType(PrisonerPunishmentTypeEntity entity)
        {
            return DAL_PrisonerPunishmentTypeService.SP_GetList_PrisonerPunishmentType(entity);
        }
        public bool? UpdatePrisonerPunishmentType(PrisonerPunishmentTypeEntity entity)
        {
            return DAL_PrisonerPunishmentTypeService.SP_Update_PrisonerPunishmentType(entity);
        }
        #endregion

        #region CaseCloseAmnesies
        public int AddCaseCloseAmnesties(CaseCloseAmnestiesEntity entity)
        {
            int? insertID = DAL_CloseCaseService.SP_Add_CaseCloseAmnesties(entity);
            return insertID.Value;
        }
        public List<CaseCloseAmnestiesEntity> GetCaseCloseAmnesties(CaseCloseAmnestiesEntity entity)
        {
            return DAL_CloseCaseService.SP_GetList_CaseCloseAmnesties(entity);
        }
        public bool UpdateCaseCloseAmnesties(List<CaseCloseAmnestiesEntity> newList, int CaseCloseID, int PPType)
        {
            bool status = true;

            List<CaseCloseAmnestiesEntity> oldList = DAL_CloseCaseService.SP_GetList_CaseCloseAmnesties (new CaseCloseAmnestiesEntity(CaseCloseID: CaseCloseID));

            // update Height Weight
            foreach (var curEntity in newList)
            {
                if (!oldList.FindAll(r => r.ID == curEntity.ID).Any())
                {
                    int? curID = DAL_CloseCaseService.SP_Add_CaseCloseAmnesties(curEntity);
                    if (curID == null) status = false;
                }
            }
            foreach (var curEntity in oldList)
            {
                if (!newList.FindAll(r => r.ID == curEntity.ID).Any())
                {
                    curEntity.Status = false;
                    bool tempStatus = DAL_CloseCaseService.Sp_Update_CaseCloseAmnesties(curEntity);
                    if (!tempStatus) status = false;
                }
            }

            return status;
        }
        #endregion

        #region PermissionsGuard
        public int AddPermissionGuard(int PositionID, int? PermissionID = null, int? OrgUnitID = null)
        {
            int? result = DAL_PermServices.SP_Add_PermissionsGuard(new PermissionsGurdEntity {PositionID = PositionID, PermissionIDs = PermissionID, OrgUnitID = OrgUnitID });
            return result != null ? Convert.ToInt32(result) : 0;
        }
        public List<PermissionsGurdEntity> GetPermissionGuard(int? OrgUnitID = null, int? PermissionID = null, int? PositionID = null, bool Full = true)
        {
            List<PermissionsGurdEntity> list = DAL_PermServices.SP_GetList_PermissionsGuard(new PermissionsGurdEntity(PermissionIDs: PermissionID, OrgUnitID:  OrgUnitID, PositionID: PositionID ));
            if (Full)
            {
                foreach (PermissionsGurdEntity item in list)
                {
                    item.Employees = DAL_OrgsServices.SP_GetList_Employees(OrgUnitID: item.PositionID, Status: true);
                }
            }
            return list;
        }
        public void UpdatePermissionGuard(int? ID, int? PermissionID, int? PositionID, bool? Status = null)
        {
            DAL_PermServices.SP_Update_PermissionsGuard(new PermissionsGurdEntity {ID = ID, PermissionIDs = PermissionID, PositionID = PositionID, Status = Status });
        }
        #endregion
    }
}
