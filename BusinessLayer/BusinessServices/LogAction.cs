﻿using CommonLayer.BusinessEntities;
using System.Collections.Generic;
using System.Web;

namespace BusinessLayer.BusinessServices
{

    public class BL_Log : BusinessServicesCommunication
    {
        private static BL_Log instance = null;
        
        public static BL_Log getInstance()
        {
            if (instance == null)
                instance = new BL_Log();

            return instance;
        }

        public void LogAction(LogActionEntity entity)
        {
            entity.EmployeeID = ((BEUser)HttpContext.Current.Session["User"]).ID;
            entity.PositionID = ((BEUser)HttpContext.Current.Session["User"]).PositionOrgUnitID;
            DAL_LogActionService.SP_Add_LogAction(entity);
        }

        public List<LogActionEntity> GetLogActions(FilterLogAcionEntity filterEntity)
        {
            return DAL_LogActionService.SP_GetList_LogAction(filterEntity);
        }

        public List<LogActionEntity> GetLogActionsForDashboard(FilterLogAcionEntity filterEntity)
        {
            return DAL_LogActionService.SP_GetList_LogAction_ForDashboard(filterEntity);
        }
    }
}
