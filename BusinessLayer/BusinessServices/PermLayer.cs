﻿
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessServices
{
    public class BL_PermLayer : BusinessServicesCommunication
    {
        public void UpdatePermission(OrgUnitEntity OrgUnit)
        {
            List<PermEntity> NewPermissions = OrgUnit.Permissions;
            List<PermEntity> OldPermissions = GetPermissionList(OrgUnit.ID);

            PermEntityComparer peComparer = new PermEntityComparer();
            IEnumerable<PermEntity> Intersection = OldPermissions.Intersect(NewPermissions, peComparer);
            //IEnumerable<PermEntity> Union = OldPermissions.Union(NewPermissions, peComparer);

            IEnumerable<PermEntity> Removing = OldPermissions.Except(Intersection, peComparer);
            foreach (PermEntity e in Removing)
            {
                DeletePermissionToPosition(PermissionID: e.ID, OrgUnitID: OrgUnit.ID);
            }
            IEnumerable<PermEntity> Adding = NewPermissions.Except(Intersection, peComparer);
            JoinPermissionsToPosition(list: Adding.ToList(), OrgUnitID: OrgUnit.ID, TypeID: OrgUnit.TypeID);

            IEnumerable<PermEntity> Updating = NewPermissions.Except(Intersection, new PermEntityStrictComparer());
            foreach (PermEntity e in Updating)
            {
                if(e.PPType == 0)
                {
                    UpdatePermissionToPosition(ID: null, PermissionID: e.ID, Type: e.PPType, OrgUnitID: OrgUnit.ID, Status: null, Recursive: true);
                }
                else
                {
                    UpdatePermissionToPosition(ID: null, PermissionID: e.ID, Type: e.PPType, OrgUnitID: OrgUnit.ID, Status: null, Recursive: false);
                    if (e.PPType == 2)
                    {
                        DeletePermissionToPosition(PermissionID: e.ID, OrgUnitID: OrgUnit.ID);
                        JoinPermissionToPosition(Entity: e, OrgUnitID: OrgUnit.ID, TypeID: e.PPType);
                    }
                }
            }

        }

        public int JoinPermissionToPosition(int PermissionID, int OrgUnitID, int Type)
        {
            int? result = DAL_PermServices.SP_Add_PermissionsToPosition(OrgUnitID, PermissionID, Type);
            return result != null ? Convert.ToInt32(result) : 0;
        }

        public void JoinPermissionToPosition(PermEntity Entity, int OrgUnitID, int TypeID)
        {
            int PermToPosID = JoinPermissionToPosition(Entity.ID.Value, OrgUnitID, Entity.PPType);
            if (Entity.PPType == 2)
            {
                foreach (int ApprovingOrgUnitID in Entity.Approvers)
                {
                    DAL_PermServices.SP_Add_PermissionsApprove(ApprovingOrgUnitID, PermToPosID);
                }
            }
        }

        public void JoinPermissionsToPosition(List<PermEntity> list, int OrgUnitID, int TypeID)
        {
            // TODO: Change cyclic database request to single request
            foreach(PermEntity entity in list)
            {
                int PermToPosID = JoinPermissionToPosition(entity.ID.Value, OrgUnitID, entity.PPType);
                if(entity.PPType == 2)
                {
                    foreach(int ApprovingOrgUnitID in entity.Approvers)
                    {
                        DAL_PermServices.SP_Add_PermissionsApprove(ApprovingOrgUnitID, PermToPosID);
                    }
                }
            }
        }

        public void JoinPositionToPermitionApprove(int PositionID, int PermToPosID)
        {
            DAL_PermServices.SP_Add_PermissionsApprove(PositionID, PermToPosID, false);
        }
        
        public void RemovePositionToPermissionApprov(int ID, int PermToPosID)
        {
            DAL_PermServices.SP_Update_PermissionsApprove(ID: ID, PermToPosID: PermToPosID, Status: false);
        }

        public List<PermissionsDashboardEntity> GetPermissionedPositionsForDashboard(int PermissionID, int QKHOrgUnitID)
        {
            return DAL_PermServices.SP_GetList_PermissionedPositionsForDashboard(PermissionID, QKHOrgUnitID);
        }

        public List<PermToPosEntity> GetPermissionToPosition(int? OrgUnitID, int? PermissionID = null)
        {
            return DAL_PermServices.SP_GetList_PermissionsToPosition(null, PermissionID, OrgUnitID);
        }

        public void UpdatePermissionToPosition(int? ID, int? OrgUnitID, int? PermissionID = null, int? Type = null, bool? Status = null, bool Recursive = false)
        {
            DAL_PermServices.SP_Update_PermissionsToPosition(ID: ID, OrgUnitID: OrgUnitID, PermissionID: PermissionID, Type: Type, Status: Status, Recursive: Recursive);
        }

        public void DeletePermissionToPosition(int? PermissionID, int? OrgUnitID)
        {
            DAL_PermServices.SP_Delete_PermissionsToPosition(PermissionID, OrgUnitID);
        }

        public List<PermEntity> GetPermissionByParentID(int ParentID)
        {
            List<PermEntity> permList = DAL_PermServices.SP_GetList_Permissions(null, ParentID);
            if (permList != null)
            {
                List<PermEntity> list = permList.FindAll(item => item.ParentID == ParentID);
                foreach(PermEntity entity in list)
                {
                    entity.subItems = this.ClassifyPermissions(permList, entity.ID);
                }
                return list;
            }
            return null;
        }

        public List<PermEntity> GetPermissionList(int OrgUnitID)
        {
           return DAL_PermServices.SP_GetList_PermissionsByOrgUnitID(OrgUnitID);
        }

        public List<int>  GetPermissionApproveIDs(int OrgUnitID)
        {
            return DAL_PermServices.SP_GetList_PermissionApproveIDs(OrgUnitID);
        }

        public List<PermissionsDashboardEntity> GetPermissionedPositions(int PermissionID, int QKHOrgUnitID, bool Permanent = false)
        {
            return DAL_PermServices.SP_GetList_PermissionedPositions(PermissionID, QKHOrgUnitID, Permanent);
        }

        private List<PermEntity> ClassifyPermissions(List<PermEntity> PermList, int? parentID)
        {
            List<PermEntity> finalLibEntityList = new List<PermEntity>();
            List<PermEntity> currentLibsList = PermList.FindAll(r => r.ParentID == parentID);

            if (currentLibsList.Any())
            {
                foreach (PermEntity currentLib in currentLibsList)
                {
                    List<PermEntity> tempLibsList = PermList.FindAll(r => r.ParentID == currentLib.ID);
                    if (tempLibsList.Any())
                    {
                        currentLib.subItems = this.ClassifyPermissions(PermList, currentLib.ID);
                    }
                    finalLibEntityList.Add(currentLib);
                }
            }
            return finalLibEntityList;
        }
    }
}
