﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusinessLayer.BusinessServices
{

    public class BL_Permissions : BusinessServicesCommunication
    {
        private static BL_Permissions instance = null;
        private List<PermEntity> Permissions;
        public static BL_Permissions getInstance()
        {
            if (instance == null)
            {
                instance = new BL_Permissions();
            }

            return instance;
        }

        public string name;

        private BL_Permissions()
        {
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            this.Permissions = User.Permissions;
            PermissionToApproveTableMap = new Dictionary<PermissionsHardCodedIds, IList<MergeEntity.TableIDS>>();
            PermissionToApproveTableMap.Add(PermissionsHardCodedIds.WorkLoads, new List<MergeEntity.TableIDS>() { MergeEntity.TableIDS.WORKLOADS_MAIN, MergeEntity.TableIDS.WORKLOADS_FILES });
            PermissionToApproveTableMap.Add(PermissionsHardCodedIds.CameraCard, new List<MergeEntity.TableIDS>() { MergeEntity.TableIDS.CELL_MAIN, MergeEntity.TableIDS.CELL_FILES });
        }

        public int getPermission(int permissionID)
        {
            PermEntity permissions = this.Permissions.Find(permission => permission.ID == permissionID);
            if (permissions == null && permissionID != 0)
            {
                return 0;
            }
            else if (permissions != null)
            {
                return permissions.PPType;
            }

            return 0;
        }

        public Dictionary<PermissionsHardCodedIds, IList<MergeEntity.TableIDS>> PermissionToApproveTableMap { get; set; }
    }
}