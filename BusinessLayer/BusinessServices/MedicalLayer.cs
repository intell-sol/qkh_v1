﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessServices
{
    public class BL_MedicalLayer : BusinessServicesCommunication
    {
        #region Add
        public int? AddComplaints(MedicalComplaintsEntity entity)
        {
            return DAL_MedicalService.SP_Add_MedicalComplaints(entity);
        }
        public int? AddDiagnosis(MedicalDiagnosisEntity entity)
        {
            return DAL_MedicalService.SP_Add_MedicalDiagnosis(entity);
        }
        public int? AddExternalExamination(MedicalExternalExaminationEntity entity)
        {
            return DAL_MedicalService.SP_Add_MedicalExternalExamination(entity);
        }
        public int? AddHealing(MedicalHealingEntity entity)
        {
            return DAL_MedicalService.SP_Add_MedicalHealing(entity);
        }
        public int? AddHistory(MedicalHistoryEntity entity)
        {
            return DAL_MedicalService.SP_Add_MedicalHistory(entity);
        }
        public int? AddResearch(MedicalResearchEntity entity)
        {
            return DAL_MedicalService.SP_Add_MedicalResearch(entity);
        }
        #endregion

        #region GetList
        public List<MedicalComplaintsEntity> GetList_MedicalComplaints(MedicalComplaintsEntity entity)
        {
            return DAL_MedicalService.SP_GetList_MedicalComplaints(entity);
        }

        public List<MedicalDiagnosisEntity> GetList_MedicalDiagnosis(MedicalDiagnosisEntity entity)
        {
            return DAL_MedicalService.SP_GetList_MedicalDiagnosis(entity);
        }

        public List<MedicalExternalExaminationEntity> GetList_MedicalExternalExamination(MedicalExternalExaminationEntity entity)
        {
            return DAL_MedicalService.SP_GetList_MedicalExternalExamination(entity);
        }

        public List<MedicalHealingEntity> GetList_MedicalMedicalHealing(MedicalHealingEntity entity)
        {
            return DAL_MedicalService.SP_GetList_MedicalHealing(entity);
        }

        public List<MedicalHistoryEntity> GetList_MedicalHistory(MedicalHistoryEntity entity)
        {
            return DAL_MedicalService.SP_GetList_MedicalHistory(entity);
        }

        public List<MedicalPrimaryEntity> GetList_MedicalPrimary(MedicalPrimaryEntity entity)
        {
            return DAL_MedicalService.SP_GetList_MedicalPrimary(entity);
        }

        public List<MedicalResearchEntity> GetList_MedicalResearch(MedicalResearchEntity entity)
        {
            return DAL_MedicalService.SP_GetList_MedicalResearch(entity);
        }
        #endregion

        #region Update
        public bool? UpdatePrimary(MedicalPrimaryEntity entity)
        {
            bool? status = false;
            List<MedicalPrimaryEntity> entityList = DAL_MedicalService.SP_GetList_MedicalPrimary(entity);
            if (entityList.Any())
            {
                MedicalPrimaryEntity prevEntity = entityList.First();

                prevEntity.Status = entity.Status;
                prevEntity.HealthLibItemID = entity.HealthLibItemID;

                status = DAL_MedicalService.SP_Update_MedicalPrimary(prevEntity);

                if (status == null) status = false;
            }
            else
            {
                int? id = DAL_MedicalService.SP_Add_MedicalPrimary(entity);
                if (id != null) status = true;
            }
            return status;
        }

        public bool? UpdateMedicalComplaints(MedicalComplaintsEntity entity)
        {
            bool? status = true;

            status = DAL_MedicalService.SP_Update_MedicalComplaints(entity);

            if (status == null) status = false;

            return status;
        }

        public bool? UpdateMedicalDiagnosis(MedicalDiagnosisEntity entity)
        {
            bool? status = true;

            status = DAL_MedicalService.SP_Update_MedicalDiagnosis(entity);

            if (status == null) status = false;

            return status;
        }

        public bool? UpdateMedicalExternalExamination(MedicalExternalExaminationEntity entity)
        {

            bool? status = true;

            status = DAL_MedicalService.SP_Update_MedicalExternalExamination(entity);

            if (status == null) status = false;

            return status;
        }

        public bool? UpdateMedicalHealing(MedicalHealingEntity entity)
        {
            bool? status = true;

            status = DAL_MedicalService.SP_Update_MedicalHealing(entity);

            if (status == null) status = false;

            return status;
        }

        public bool? UpdateMedicalHistory(MedicalHistoryEntity entity)
        {
            bool? status = true;

            status = DAL_MedicalService.SP_Update_MedicalHistory(entity);

            if (status == null) status = false;

            return status;
        }
        public bool? UpdateMedicalResearch(MedicalResearchEntity entity)
        {
            bool? status = true;
           
            status = DAL_MedicalService.SP_Update_MedicalResearch(entity);

            if (status == null) status = false;

            return status;
        }
        #endregion
    }
}
