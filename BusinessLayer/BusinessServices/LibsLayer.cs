﻿using CommonLayer.BusinessEntities;
//using DataAccessLayer.DataCommunication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace BusinessLayer.BusinessServices
{
    public class BL_LibsLayer : BusinessServicesCommunication
    {
        private static BL_LibsLayer instance { set; get; }
        private static IDictionary<int, List<LibsEntity>> LibCache = new Dictionary<int, List<LibsEntity>>();

        public static  BL_LibsLayer getInstance()
        {
            if (instance == null)
                instance = new BL_LibsLayer();
            return instance;
        }

        public LibPathEntity GetLibPathByID(int ID) {
            return DAL_LibsServices.SP_GetList_LibPathByID(ID);
        }

        /// <summary>
        /// Getting Lib Paths from Database
        /// Sum optional parameters added for filling main parent fileds as procedure selects only childs
        /// Here ParentID is Id for main parent Entity
        /// </summary>
        public LibPathEntity GetLibPath(int ParentID = 0, string Name = "", int ThisParentId = 0)
        {
            List<LibPathEntity> LibList = DAL_LibsServices.SP_Get_LibPath(ParentID);
            if (LibList != null)
            {
                LibPathEntity finalLibEntity = new LibPathEntity();

                // if selected all elements then parent is with parent id 0
                if (ParentID == 0)
                {
                    finalLibEntity = LibList.Find(r => r.ParentID == ParentID);
                    finalLibEntity.subitems = this.ClassifyLibPath(LibList, finalLibEntity.ID);
                }
                else
                {
                    List<LibPathEntity> finalLibEntityList = LibList.FindAll(r => r.ParentID == ParentID);

                    // if selected from middle then recursivle add parents then make null main parent
                    foreach (LibPathEntity curLibEntity in finalLibEntityList)
                    {
                        curLibEntity.subitems = this.ClassifyLibPath(LibList, curLibEntity.ID);
                        finalLibEntity.subitems.Add(curLibEntity);
                    }

                    // adding main parent parameters
                    finalLibEntity.ID = ParentID;
                    finalLibEntity.Name = Name;
                    finalLibEntity.ParentID = ThisParentId;
                }

                return finalLibEntity;
            }
            return null;
        }

        public List<LibPathEntity> GetLibTreePathByParentID(int ParentID)
        {
            List<LibPathEntity> LibList = DAL_LibsServices.SP_Get_LibPathsWithCountByParentID(ParentID: ParentID);

            return LibList;
        }

        /// <summary>
        /// Get List of Libs by given libpath  id
        /// </summary>
        /// <param name="LibPathID">LibPath ID</param>
        /// <returns></returns>
        public List<LibsEntity> GetLibsByLibPathID(int LibPathID)
        {
            List<LibsEntity> libs = DAL_LibsServices.SP_GetList_LibsByLibPathID(LibPathID);
            return libs;
        }

        public List<LibsEntity> GetLibsLocalTreeByLibPathID(int LibPathID)
        {
            List<LibsEntity> libs = DAL_LibsServices.SP_GetList_LibsLocalTreeByLibPathID(LibPathID);
            libs = ClassifyLibs(libs);
            return libs;
        }
        public List<LibsEntity> GetLibsLocalTreeWithPropertiesByLibParentID(int ParentID)
        {
            
            if (!LibCache.ContainsKey(ParentID))
            {
                List<LibsEntity> libs = DAL_LibsServices.SP_GetList_LibsLocalTreeWithPropertiesByLibParentID(ParentID);
                libs.RemoveAll(x => x.Name.IndexOf("ԲԱԺԻՆ", StringComparison.OrdinalIgnoreCase) >= 0);
                libs.RemoveAll(x => x.Name.IndexOf("Գլուխ", StringComparison.OrdinalIgnoreCase) >= 0);
                libs = ClassifyLibs(libs);
                LibCache.Add(ParentID, libs);
            }
            
            return LibCache[ParentID];
        }
        public List<LibsEntity> GetLibsLocalTreeWithPropertiesByLibPathID(int LibPathID)
        {
            List<LibsEntity> libs = DAL_LibsServices.SP_GetList_LibsLocalTreeWithPropertiesByLibPathID(LibPathID);
            libs = ClassifyLibs(libs);
            return libs;
        }
        /// <summary>
        /// Get List of Libs by given libpath  id
        /// </summary>
        /// <param name="LibPathID">LibPath ID</param>
        /// <returns></returns>
        public List<LibsEntity> GetCitizenshipLibsByCode(string Code)
        {
            List<LibsEntity> libs = DAL_LibsServices.SP_GetList_CitizenshipLibsByCode(Code: Code, CitizenshipLibPathID: (int)LibsEntity.LIBS.CITIZENSHIP, IdentCodePropID: (int)PropsEntity.PropToLibID.IDENTIFICATION_CODE);
            return libs;
        }

        /// <summary>
        /// Get List of Libs by given libpath  id
        /// </summary>
        /// <param name="LibPathIDList">LibPath ID</param>
        /// <returns></returns>
        public Dictionary<int, List<LibsEntity>> GetLibsByLibPathID(List<int> LibPathIDList)
        {
            return DAL_LibsServices.SP_GetList_LibsByLibPathIDList(String.Join(",", LibPathIDList));
        }

        /// <summary>
        /// Get list libs by parent id
        /// </summary>
        /// <param name="ParentID">Parent ID</param>
        /// <returns></returns>
        public List<LibsEntity> GetLibsByLibParentID(int? LibPathID = null)
        {
            return DAL_LibsServices.SP_GetList_LibsByLibParentID(LibPathID);
        }

        /// <summary>
        /// Get Lib by id
        /// </summary>
        /// <param name="LibID">LibPath ID</param>
        /// <returns></returns>
        public LibsEntity GetLibByID(int LibID)
        {
            return DAL_LibsServices.SP_Get_LibByID(LibID);
        }

        /// <summary>
        /// Get Dictionary where key is a LibPathID and Value is dictionary of  (key => LibID, value => Libs Count depended with LibPathID) 
        /// </summary>
        /// <param name="LibPathID"></param>
        /// <returns></returns>
        public int GetLibCountByLibPathID(int LibPathID)
        {
            return DAL_LibsServices.SP_Get_LibCountByLibPathID(LibPathID);
        }

        /// <summary>
        /// Add Lib by Lib path id
        /// </summary>
        /// <param name="LibPathID">LibPath ID</param>
        /// <returns></returns>
        public int? AddLibsByLibPathID(string Name = null, int? LibPathID = null, int LibParentID = 0)
        {
            //Boolean status = false;

            int? newId = null;
            int? LibPathLibID = DAL_LibsServices.SP_Get_LibPathLibID(LibPathID);

            if (LibPathLibID != null)
            {
                // old code for old construction
                //nextId = DAL_LibsServices.SP_Add_LibPathToLibs(LibPathID, newId);

                newId = DAL_LibsServices.SP_Add_Libs(Name, LibPathLibID);

                //if (nextId != null)
                //{
                //    status = true;
                //}
            }

            return newId;
        }

        /// <summary>
        /// Add Lib by Lib parent id
        /// </summary>
        /// <param name="LibParentID">LibParent ID</param>
        /// <returns></returns>
        public int? AddLibsByLibParentID(int? LibParentID = null, string Name = null)
        {
            //Boolean status = false;

            int? newId = null;
            //LibsEntity curLib = DAL_LibsServices.SP_Get_Lib(LibParentID);

            if (LibParentID != null)
            {
                newId = DAL_LibsServices.SP_Add_Libs(Name, LibParentID.Value);
            }

            return newId;
        }

        /// <summary>
        /// Add Lib by Lib path id
        /// </summary>
        /// <param name="LibID">Lib ID</param>
        /// <param name="Name">Name</param>
        /// <returns></returns>
        public Boolean EditLibByID(int LibID, string Name = null)
        {
            //Boolean status = false;

            Boolean status = DAL_LibsServices.SP_Update_LibByID(LibID, Name);
            
            return status;
        }

        /// <summary>
        /// Del Lib by Lib path id
        /// </summary>
        /// <param name="LibID">Lib ID</param>
        /// <returns></returns>
        public Boolean DelLibByID(int LibID)
        {
            //Boolean status = false;

            Boolean status = DAL_LibsServices.SP_Delete_LibByID(LibID);

            return status;
        }

        /// <summary>
        /// get number of libs for lib path last childs which have libs
        /// </summary>
        //public List<LibPathEntity> GetLibCountByLibPathID(string idList)
        //{
        //    if (idList == null) return null
        //    return DAL_LibsServices.SP_GetList_LibPathLibsCount(idList);
        //}

        /// <summary>
        /// Making lib path tree
        /// </summary>
        private List<LibPathEntity> ClassifyLibPath(List<LibPathEntity> LibsList, int finalID)
        {
            List<LibPathEntity> finalLibEntityList = new List<LibPathEntity>();
            List<LibPathEntity> currentLibsList = LibsList.FindAll(r => r.ParentID == finalID);

            if (currentLibsList.Any())
            {
                foreach (LibPathEntity currentLib in currentLibsList)
                {
                    List<LibPathEntity> tempLibsList = LibsList.FindAll(r => r.ParentID == currentLib.ID);
                    if (tempLibsList.Any())
                    {
                        currentLib.subitems = this.ClassifyLibPath(LibsList, currentLib.ID);
                    }
                    finalLibEntityList.Add(currentLib);
                }
            }
            return finalLibEntityList;
        }
        private List<LibsEntity> ClassifyLibs(List<LibsEntity> LibsList)
        {
            List<LibsEntity> removeList = new List<LibsEntity>();
            for (int i=0 ;i < LibsList.Count; i++)
            {
                LibsEntity curItem = LibsList[i];
                List<LibsEntity> tempList = LibsList.FindAll(r => r.ID == curItem.ParentID);
                if (tempList.Any())
                {
                    tempList.First().subitems.Add(curItem);
                    removeList.Add(curItem);
                }
            }

            foreach (var item in removeList)
            {
                LibsList.Remove(item);
            }

            return LibsList;
        }

        public bool CheckLibBind(int LibPathID)
        {
            bool exist = false;

            int? LibID = DAL_LibsServices.SP_Get_LibPathLibID(LibPathID);

            if (LibID != null) exist = true;

            return exist;
        }

        /// <summary>
        /// Registering Lib path in database
        /// </summary>
        public Boolean AddLibPath(string Name, int ParentID)
        {
            DAL_LibsServices.SP_Add_LibPath(Name, ParentID);
            return true;
        }

        public List<LibPathEntity> GetLibPathWay(int ParentID)
        {
            return DAL_LibsServices.SP_GetList_LibPathWay(ParentID:ParentID);
        }

        public List<NationalityEntity> GetNationality(int? ID = null, int? Code = null)
        {
            return DAL_NationalityService.SP_GetList_Nationality(ID, Code);
        }

        public LibsEntity GetCitizenshipLibByCode(string Code)
        {
            LibsEntity finalLib = BL_PropsLayer.getInstance().GetCitizenshipLibIDByCode((int)LibsEntity.LIBS.CITIZENSHIP, Code);

            return finalLib;
        }

        public string drawCustomSelectRecursionSelected(List<LibsEntity> LibList, List<string> checkedTextList, int? checkedID = null, List<int> checkedIDList = null, string newName = "")
        {
            foreach (var curLib in LibList)
            {
                string matchName = "";
                Regex reg = new Regex(@"^((Հոդված\s{1}[\d]+\.)|([\d]+[\.|\)]))", RegexOptions.IgnoreCase);
                Match m = reg.Match(curLib.Name.TrimStart());
                while (m.Success)
                {
                    matchName = m.Value;
                    break;
                }
                if (curLib.subitems.Any())
                {

                    drawCustomSelectRecursionSelected(curLib.subitems, checkedTextList, checkedID, checkedIDList, newName + " " + matchName);

                }
                else
                {
                    if ((checkedIDList != null && checkedIDList.FindAll(r => r == curLib.ID).Any()) || (checkedID != null && checkedID == curLib.ID))
                    {
                        checkedTextList.Add(newName + " " + matchName);
                    }
                }
            }
            return String.Join(",", checkedTextList);
        }
    }
}
