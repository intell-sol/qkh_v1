﻿using CommonLayer.BusinessEntities;
using System.Collections.Generic;

namespace BusinessLayer.BusinessServices
{
    public class BL_ReportLayer : BusinessServicesCommunication
    {
        public List<EarlyReportEntity> EarlyReport_List(EarlyReportEntity entity)
        {
            List<EarlyReportEntity> model = DAL_ReportService.SP_GetReportList_Early(entity);
            return model;
        }

        public List<OpenTypeReportEntity> FillOpenTypeReport_List(OpenTypeReportEntity entity)
        {
            List<OpenTypeReportEntity> model = DAL_ReportService.SP_GetReportList_OpenType(entity);

            return model;
        }

        public List<PersonalDescriptionReportEntity> FillPersonalDescriptionReport_List(PersonalDescriptionReportEntity entity)
        {
            List<PersonalDescriptionReportEntity> model = DAL_ReportService.SP_GetReportList_PersonalDescription(entity);

            return model;
        }

        public List<CaseClosedReportEntity> FillCaseClosedReport_List(CaseClosedReportEntity entity)
        {
            List<CaseClosedReportEntity> model = DAL_ReportService.SP_GetReportList_CaseClosed(entity);

            return model;
        }

        public List<PrisonerReferenceReportEntity> FillPrisonerReferenceReport_List(PrisonerReferenceReportEntity entity)
        {
            List<PrisonerReferenceReportEntity> model = DAL_ReportService.SP_GetReportList_Reference(entity);

            //MemoryStream mem = new MemoryStream(BL_Files.getInstance().GetPrisonerFile(model[0].FileID).Item1);
            model[0].FilePath = BL_Files.getInstance().GetPrisonerFile(model[0].FileID).Item1; //Image.FromStream(mem);//BL_Files.getInstance().GetPrisonerFile(model[0].FileID).Item1;//BL_Files.getInstance().GetLinkImage(model[0].FileID, 200, null);
            return model;
        }

        public List<MedicalCountReportEntity> FillMedicalCountReport_List(MedicalCountReportEntity entity)
        {
            List<MedicalCountReportEntity> model = DAL_ReportService.SP_GetReportList_MedicalCount(entity);

            return model;
        }

        public List<CountPrisonersByOrgReportEntity> FillPrisonersCountByOrgUnitReport_List(CountPrisonersByOrgReportEntity entity)
        {
            List<CountPrisonersByOrgReportEntity> model = DAL_ReportService.SP_GetReportList_PrisonersCountByOrgUnit(entity);

            return model;
        }

        public List<CountPrisonersByOrgTypeEntity> FillPrisonersCountByOrgType_List(CountPrisonersByOrgTypeEntity entity)
        {
            List<CountPrisonersByOrgTypeEntity> model = DAL_ReportService.SP_GetReportList_PrisonersCountByType(entity);

            return model;
        }

        public List<CountPrisonersByCaseCloseEntity> FillPrisonersCountClosedReport_List(CountPrisonersByCaseCloseEntity entity)
        {
            List<CountPrisonersByCaseCloseEntity> model = DAL_ReportService.SP_GetReportList_CloseCountPrisoners(entity);

            return model;
        }

        public List<PrisonerConvictSemesterReportEntity> FillPrisonersConvictCountSemesterReport_List(PrisonerConvictSemesterReportEntity entity)
        {
            List<PrisonerConvictSemesterReportEntity> model = DAL_ReportService.SP_GetReportList_CountList(entity);

            return model;
        }

        public List<ArticlePercentReportEntity> FillArticlePercentReport_List(ArticlePercentReportEntity entity)
        {
            List<ArticlePercentReportEntity> model = DAL_ReportService.SP_GetReportList_ArticlePercent(entity);
            return model;
        }
        public List<PrisonersReferendumReportEntity> FillPrisonersReferendumReport_List(PrisonersReferendumReportEntity entity)
        {
            List<PrisonersReferendumReportEntity> model = DAL_ReportService.SP_GetReportList_PrisonersReferendum(entity);

            return model;
        }

        public List<OrgTypeChangedReportEntity> FillOrgTypeChangedReport_List(OrgTypeChangedReportEntity entity)
        {
            List<OrgTypeChangedReportEntity> model = DAL_ReportService.SP_GetReportList_OrgTypeChanged(entity);

            return model;
        }
        public List<CustomReportEntity> FillCustomReport_List(CustomReportEntity entity)
        {
            List<CustomReportEntity> model = DAL_ReportService.SP_GetReportList_Custom(entity);

            return model;
        }
        public List<PackageReportEntity> FillPackageReport_List(PackageReportEntity entity)
        {
            List<PackageReportEntity> model = DAL_ReportService.SP_GetReportList_Package(entity);

            BL_LibsLayer libService = new BL_LibsLayer();
            BL_PrisonersLayer prisonerService = new BL_PrisonersLayer();
            foreach (PackageReportEntity item in model)
            {
                List<LibsEntity> ArrestlibList = libService.GetLibsLocalTreeWithPropertiesByLibParentID(item.ArrestCodeLibItemID.Value);
                List<int> ArrestcheckList = new List<int>();
                List<SentencingDataArticleLibsEntity> ArrestList = prisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(ArrestDataID: item.ArrestDataID));
                foreach (var lib in ArrestList)
                {
                    ArrestcheckList.Add(lib.LibItem_ID.Value);
                }
                item.ArrestArticleName = libService.drawCustomSelectRecursionSelected(ArrestlibList, new List<string>(), null, ArrestcheckList, "");



                List<LibsEntity> SentlibList = libService.GetLibsLocalTreeWithPropertiesByLibParentID(item.SentencCodeLibItemID.Value);
                List<int> SentencecheckList = new List<int>();
                List<SentencingDataArticleLibsEntity> SentenceList = prisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(SentencingDataID: item.SentencingDataID));
                foreach (var lib in SentenceList)
                {
                    SentencecheckList.Add(lib.LibItem_ID.Value);
                }
                item.SentArticleName = libService.drawCustomSelectRecursionSelected(SentlibList, new List<string>(), null, SentencecheckList, "");
            }


            return model;
        }
        public List<PackagesContentReportEntity> FillPackagesContenReport_List(PackagesContentReportEntity entity)
        {
            List<PackagesContentReportEntity> model = DAL_ReportService.SP_GetReportList_PackageContent(entity);

            return model;
        }

        public List<PersonalVisitsReportEntity> FillPersonalVisitReport_List(PersonalVisitsReportEntity entity)
        {
            List<PersonalVisitsReportEntity> model = DAL_ReportService.SP_GetReportList_PersonalVisits(entity);

            BL_LibsLayer libService = new BL_LibsLayer();
            BL_PrisonersLayer prisonerService = new BL_PrisonersLayer();
            foreach (PersonalVisitsReportEntity item in model)
            {
                List<LibsEntity> ArrestlibList = libService.GetLibsLocalTreeWithPropertiesByLibParentID(item.ArrestCodeLibItemID.Value);
                List<int> ArrestcheckList = new List<int>();
                List<SentencingDataArticleLibsEntity> ArrestList = prisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(ArrestDataID: item.ArrestDataID));
                foreach (var lib in ArrestList)
                {
                    ArrestcheckList.Add(lib.LibItem_ID.Value);
                }
                item.ArrestArticleName = libService.drawCustomSelectRecursionSelected(ArrestlibList, new List<string>(), null, ArrestcheckList, "");



                List<LibsEntity> SentlibList = libService.GetLibsLocalTreeWithPropertiesByLibParentID(item.SentencCodeLibItemID.Value);
                List<int> SentencecheckList = new List<int>();
                List<SentencingDataArticleLibsEntity> SentenceList = prisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(SentencingDataID: item.SentencingDataID));
                foreach (var lib in SentenceList)
                {
                    SentencecheckList.Add(lib.LibItem_ID.Value);
                }
                item.SentArticleName = libService.drawCustomSelectRecursionSelected(SentlibList, new List<string>(), null, SentencecheckList, "");
            }


            return model;
        }
        public List<OfficialVisitReportEntity> FillOfficialVisitReport_List(OfficialVisitReportEntity entity)
        {
            List<OfficialVisitReportEntity> model = DAL_ReportService.SP_GetReportList_OfficialVisit(entity);

            BL_LibsLayer libService = new BL_LibsLayer();
            BL_PrisonersLayer prisonerService = new BL_PrisonersLayer();
            foreach (OfficialVisitReportEntity item in model)
            {
                if (item.ArrestCodeLibItemID != null)
                {
                    List<LibsEntity> ArrestlibList = libService.GetLibsLocalTreeWithPropertiesByLibParentID(item.ArrestCodeLibItemID.Value);
                    List<int> ArrestcheckList = new List<int>();
                    List<SentencingDataArticleLibsEntity> ArrestList = prisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(ArrestDataID: item.ArrestDataID));
                    foreach (var lib in ArrestList)
                    {
                        ArrestcheckList.Add(lib.LibItem_ID.Value);
                    }
                    item.ArrestArticleName = libService.drawCustomSelectRecursionSelected(ArrestlibList, new List<string>(), null, ArrestcheckList, "");
                }

                if (item.SentencCodeLibItemID != null)
                {
                    List<LibsEntity> SentlibList = libService.GetLibsLocalTreeWithPropertiesByLibParentID(item.SentencCodeLibItemID.Value);
                    List<int> SentencecheckList = new List<int>();
                    List<SentencingDataArticleLibsEntity> SentenceList = prisonerService.GetSentencingDataArticles(new SentencingDataArticleLibsEntity(SentencingDataID: item.SentencingDataID));
                    foreach (var lib in SentenceList)
                    {
                        SentencecheckList.Add(lib.LibItem_ID.Value);
                    }
                    item.SentArticleName = libService.drawCustomSelectRecursionSelected(SentlibList, new List<string>(), null, SentencecheckList, "");
                }
            }


            return model;
        }
    }
}
