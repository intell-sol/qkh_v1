﻿using CommonLayer.BusinessEntities;
//using DataAccessLayer.DataCommunication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessServices
{
    public class BL_PropsLayer : BusinessServicesCommunication
    {
        private static BL_PropsLayer instance { set; get; }

        public static BL_PropsLayer getInstance()
        {
            if (instance == null)
                instance = new BL_PropsLayer();
            return instance;
        }
        /// <summary>
        /// Get Property List
        /// </summary>
        public List<PropsEntity> GetPropsList(int? LibID = null)
        {
            List<PropsEntity> finalList = new List<PropsEntity>();

            finalList = DAL_PropsServices.SP_GetList_Properties(LibID);


            for (int i = 0; i < finalList.Count; i++)
            {
                if (finalList[i].Fixed.Value || LibID != null)
                {
                    finalList[i].Values = DAL_PropsServices.SP_GetList_PropertiesValue(PropID: finalList[i].ID.Value, LibID: LibID);
                }
            }
            
            return finalList;
        }

        public List<PropValuesEntity> GetProperty(int PropID)
        {
            PropsEntity finalEntity = new PropsEntity();

            finalEntity = DAL_PropsServices.SP_GetList_Property(PropID);

            if (finalEntity != null)
            {
                finalEntity.Values = DAL_PropsServices.SP_GetList_PropertiesValue(PropID: PropID);
            }

            return finalEntity.Values;
        }
        /// <summary>
        /// Get Property List
        /// </summary>
        public LibsEntity GetCitizenshipLibIDByCode(int PathID, string Value)
        {
            return DAL_PropsServices.SP_Get_CitizenshipLibIDByCode(PathID, Value);
        }

        /// <summary>
        /// Bind Proprty to Lib
        /// </summary>
        public int? AddPropsToList(int? ListID = null, int? PropID = null, int? CustomID = null)
        {
            int? NewID = DAL_PropsServices.SP_Add_PropertyToList(ListID, PropID, CustomID);

            return NewID;
        }

        /// <summary>
        /// Update Property to List and ValueTable table
        /// </summary>
        public Boolean UpdatePropsStatus(List<PropsEntity> checkEntityList, int LibID)
        {
            List<int> listValueIds = new List<int>();
            foreach (PropsEntity tempEntity in checkEntityList)
            {
                if (tempEntity.Values.Any())
                {
                    listValueIds.Add(tempEntity.Values[0].ID.Value);
                }
            }

            string listValues = string.Join(",", listValueIds);

            Boolean statusListUpdate = DAL_PropsServices.SP_Update_PropertyToList(LibID);

            Boolean statusListValueUpdate = DAL_PropsServices.SP_Update_PropertyValue(listValues);

            if (statusListUpdate && statusListValueUpdate)
            {
                return true;
            }

            return false;
        }

        public bool UpdateProopertyStatus(int LibID, int PropID, bool Status) {
            return DAL_PropsServices.SP_Update_PropertiesToPropertyValue(LibID: LibID, PropID: PropID, Status: Status);
        }

        /// <summary>
        /// Bind Proprty to Lib
        /// </summary>
        public int? AddPropValue(String Value, Boolean Fixed, int? PropID = null)
        {
            int? NewID = DAL_PropsServices.SP_Add_PropertyValue(PropID, Value, Fixed);

            return NewID;
        }
    }
}
