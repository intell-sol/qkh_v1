﻿using CommonLayer.BusinessEntities;
using DataAccessLayer.DataCommunication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BusinessLayer.BusinessServices
{
    public class BL_UsersAuthentication : BusinessServicesCommunication
    {
        public BEUser UserLogIn(string UserName, string Password)
        {
            BEUser User = DAL_UsersAuthentification.SP_Login(UserName, Password);
            if (User != null)
            {
                return User;
            }
            return null;
        }
    }
}
