﻿using CommonLayer.BusinessEntities;
using DataAccessLayer.DataCommunication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BusinessLayer.BusinessServices
{
    public class BL_MergeLayer : BusinessServicesCommunication
    {
        /// <summary>
        /// Add Modification
        /// </summary>
        public int? AddModification(MergeEntity entity)
        {
            int? NewID = DAL_MergeService.SP_Add_Modification(entity);
            return NewID.HasValue && NewID.Value > 0 ? NewID : null;
        }

        public bool UpdateModification(MergeEntity entity)
        {
            int? result = DAL_MergeService.SP_Update_Modification(entity);
            return result != null;
        }

        /// <summary>
        /// Get Modification List
        /// </summary>
        public List<MergeObjectEntity> GetModificationList()
        {
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];

            int positionID = User.PositionOrgUnitID;

            List<int> PermIDList = DAL_MergeService.SP_GetList_PermissionApproveIDs(new MergeEntity(OrgUnitID: positionID));

            List<int> IDList = new List<int>();

            foreach (var item in BL_Permissions.getInstance().PermissionToApproveTableMap.Where(Item => PermIDList.Contains((int)Item.Key)))
            {
                foreach (var values in item.Value)
                {
                    int curValue = (int)values;
                    IDList.Add(curValue);
                }
            }

            string IdStrings = string.Join(",", IDList);

            int currentOrgUnit = User.SelectedOrgUnitID;

            return DAL_MergeService.SP_GetList_ModificationInfoByEIDList(EIDList: IdStrings, QKHOrgUnitID: currentOrgUnit);
        }

        public MergeObjectEntity GetPrisonerActiveModifications(int PrisonerID)
        {
            return DAL_MergeService.SP_GetList_PrisonersActiveModifications(PrisonerID: PrisonerID);
        }
        public List<string> GetMergEIDs(int PrisonerID)
        {
            return DAL_MergeService.SP_GetList_EID_WaitAppove(PrisonerID);
        }
        public List<MergeEntity> GetModificationInfo(MergeEntity entity)
        {
            return DAL_MergeService.SP_GetList_ModificationInfo(entity);
        }

        /// <summary>
        /// Get Modification for dashboard
        /// </summary>
        public List<MergeDashboardEntity> GetMergeForDashboard(FilterMergeEntity entity)
        {
            // UNDONE: this sectin is undone maybe :D
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            entity.OrgUnitID = User.SelectedOrgUnitID;

            List<MergeDashboardEntity> list = DAL_MergeService.SP_GetList_ModificationsForDashboard(entity);
            return list;
        }
        
        public List<MergeDashboardEntity> GetGroupedMergeForDashboard(FilterMergeEntity entity)
        {
            // UNDONE: this sectin is undone maybe :D
            BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
            entity.OrgUnitID = User.SelectedOrgUnitID;

            List<MergeDashboardEntity> list = DAL_MergeService.SP_GetList_GroupedModificationsForDashboard(entity);
            return list;
        }

        public bool ApproveModification(MergeEntity entity)
        {
            bool result = false;
            switch (entity.Type)
            {
                case 1:// Add
                    result = AddData(entity);
                    break;
                case 2:// Remove
                case 3:// Update
                    result = UpdateData(entity);
                    break;
            }
            return result;
        }

        public bool CancelModification(MergeEntity entity)
        {
            return UpdateModification(new MergeEntity(ID: entity.ID, ApproverID: entity.ApproverID, Approved: false, Date: DateTime.Now, Status: false));
        }

        public bool AddData(MergeEntity entity)
        {
            IsoDateTimeConverter DateTimeSetting = new IsoDateTimeConverter();
            BL_PrisonersLayer PrisonerService = new BL_PrisonersLayer();

            using (TransactionContext context = new TransactionContext())
            {
                context.Begin();

                switch ((MergeEntity.TableIDS)entity.EID)
                {
                    case MergeEntity.TableIDS.PRIMARY_PREVENTION:

                        PreviousConvictionsEntity ppEntity = JsonConvert.DeserializeObject<PreviousConvictionsEntity>(entity.Json, DateTimeSetting);

                        BEUser User = (BEUser)System.Web.HttpContext.Current.Session["User"];
                        int? prevConvId = PrisonerService.AddPreviousConviction(ppEntity);

                        if (prevConvId != null && ppEntity.SentencingDataArticles != null)
                        {
                            foreach (SentencingDataArticleLibsEntity curData in ppEntity.SentencingDataArticles)
                            {
                                curData.PrevConvictionDataID = prevConvId;
                                int? curID = PrisonerService.AddSentencingDataArticle(curData);
                            }
                        }
                        break;
                    case MergeEntity.TableIDS.CELL_MAIN:

                        CameraCardItemEntity cciEntity = JsonConvert.DeserializeObject<CameraCardItemEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.AddCameraCardItem(cciEntity);
                        break;
                    case MergeEntity.TableIDS.INITIAL_DOC_IDENTITY:

                        IdentificationDocument idEntity = JsonConvert.DeserializeObject<IdentificationDocument>(entity.Json, DateTimeSetting);
                        PrisonerService.AddIdentificationDocuments(idEntity);
                        break;
                    case MergeEntity.TableIDS.PHYSICAL_HEIGHT_WEIGHT:

                        HeightWeightEntity hwEntity = JsonConvert.DeserializeObject<HeightWeightEntity>(entity.Json, DateTimeSetting);
                        //int? id = PrisonerService.AddHeightWeight(hwEntity);
                        break;
                    case MergeEntity.TableIDS.PHYSICAL_CONTENT:

                        PhysicalDataEntity pdEntity = JsonConvert.DeserializeObject<PhysicalDataEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.AddPhysicalData(pdEntity);
                        break;
                    case MergeEntity.TableIDS.PHYSICAL_LEANINGS:

                        LeaningEntity plEntity = JsonConvert.DeserializeObject<LeaningEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.AddLeaning(plEntity);
                        break;
                    case MergeEntity.TableIDS.WORKLOADS_MAIN:
                        WorkLoadsEntity wlEntity = JsonConvert.DeserializeObject<WorkLoadsEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.AddWorkLoad(wlEntity);
                        break;
                    case MergeEntity.TableIDS.DEPARTURES_MAIN:
                        DepartureEntity pdepEntity = JsonConvert.DeserializeObject<DepartureEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.AddDeparture(pdepEntity);
                        break;
                    case MergeEntity.TableIDS.EDUCATION:
                        EducationalCoursesEntity ecEntity = JsonConvert.DeserializeObject<EducationalCoursesEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.AddEducationalCourse(ecEntity);
                        break;
                    case MergeEntity.TableIDS.ENCOURAGEMENTS_MAIN:
                        EncouragementsEntity encEntity = JsonConvert.DeserializeObject<EncouragementsEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.AddEncouragement(encEntity);
                        break;
                    case MergeEntity.TableIDS.ITEMS_MAIN:
                        ItemEntity itEntity = JsonConvert.DeserializeObject<ItemEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.AddItemObject(itEntity);
                        break;
                    case MergeEntity.TableIDS.OFFICIAL_VISITS_MAIN:
                        OfficialVisitsEntity ovEntity = JsonConvert.DeserializeObject<OfficialVisitsEntity>(entity.Json, DateTimeSetting);
                        foreach (VisitorEntity curvEntitiy in ovEntity.VisitorList)
                        {
                            OfficialVisitsEntity tempEnt = ovEntity;
                            tempEnt.PositionLibItemID = curvEntitiy.PositionLibItemID;
                            tempEnt.PersonID = curvEntitiy.Person.ID;
                            PrisonerService.AddOfficialVisit(tempEnt);
                        }
                        break;
                    case MergeEntity.TableIDS.CONFLICTS_MAIN:
                        ConflictsEntity confEntity = JsonConvert.DeserializeObject<ConflictsEntity>(entity.Json, DateTimeSetting);

                        if (confEntity.PrisonerID != null)
                        {
                            if (confEntity.Person != null)
                            {

                                confEntity.Person = PrisonerService.AddPersonAndGet(Person: confEntity.Person);

                                confEntity.PersonID = confEntity.Person.ID;

                                if (confEntity.PersonID != null)
                                {
                                    int? confID = PrisonerService.AddConflict(confEntity);
                                }
                            }
                        }
                        break;
                    case MergeEntity.TableIDS.PHYSICAL_FILES:
                    case MergeEntity.TableIDS.PRIMARY_IMAGES:
                    case MergeEntity.TableIDS.PRIMARY_FIELS:
                    case MergeEntity.TableIDS.MEDICAL_FILES:
                    case MergeEntity.TableIDS.FAMILY_FILES:
                    case MergeEntity.TableIDS.ARMY_FILES:
                    case MergeEntity.TableIDS.ENTERANCE_FILES_ARREST:
                    case MergeEntity.TableIDS.ENTERANCE_FILES:
                    case MergeEntity.TableIDS.CELL_FILES:
                    case MergeEntity.TableIDS.ITEMS_FILES:
                    case MergeEntity.TableIDS.BAN_FILES:
                    case MergeEntity.TableIDS.ENCOURAGEMENTS_FILES:
                    case MergeEntity.TableIDS.PENALTY_FILES:
                    case MergeEntity.TableIDS.EDUCATIONAL_COURSES_FILES:
                    case MergeEntity.TableIDS.WORKLOADS_FILES:
                    case MergeEntity.TableIDS.OFFICIAL_VISITS_FILES:
                    case MergeEntity.TableIDS.PERSONAL_VISITS_FILES:
                    case MergeEntity.TableIDS.PACKAGES_FILES:
                    case MergeEntity.TableIDS.DEPARTURES_FILES:
                    case MergeEntity.TableIDS.TRANSFERS_FILES:
                    case MergeEntity.TableIDS.CONFLICTS_FILES:

                        AdditionalFileEntity afEntity = JsonConvert.DeserializeObject<AdditionalFileEntity>(entity.Json, DateTimeSetting);
                        //PrisonerService.AddFiles(afEntity);
                        break;
                    case MergeEntity.TableIDS.BAN_MAIN:

                        InjunctionItemEntity iiEntity = JsonConvert.DeserializeObject<InjunctionItemEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.AddInjunctionItem(iiEntity);
                        break;
                }

                UpdateModification(new MergeEntity(ID: entity.ID, ApproverID: entity.ApproverID, Approved: true, Date: DateTime.Now, Status: false));

                context.Submit();
            }

            return false;
        }

        public bool UpdateData(MergeEntity entity)
        {
            IsoDateTimeConverter DateTimeSetting = new IsoDateTimeConverter();
            BL_PrisonersLayer PrisonerService = new BL_PrisonersLayer();
            BL_MedicalLayer MedicalService = new BL_MedicalLayer();
            using (TransactionContext context = new TransactionContext())
            {
                context.Begin();

                switch ((MergeEntity.TableIDS)entity.EID)
                {
                    case MergeEntity.TableIDS.CELL_MAIN:

                        CameraCardItemEntity cciEntity = JsonConvert.DeserializeObject<CameraCardItemEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateCameraCard(cciEntity);
                        break;
                    case MergeEntity.TableIDS.INITIAL_DOC_IDENTITY:

                        IdentificationDocument idEntity = JsonConvert.DeserializeObject<IdentificationDocument>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateIdentificationDocuments(idEntity);
                        break;
                    case MergeEntity.TableIDS.PRIMARY_PREVENTION:

                        PreviousConvictionsEntity pcEntity = JsonConvert.DeserializeObject<PreviousConvictionsEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdatePreviousConviction(pcEntity);
                        break;
                    case MergeEntity.TableIDS.PHYSICAL_CONTENT:

                        PhysicalDataEntity pdEntity = JsonConvert.DeserializeObject<PhysicalDataEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdatePhysicalData(pdEntity);
                        break;
                    case MergeEntity.TableIDS.PHYSICAL_HEIGHT_WEIGHT:

                        HeightWeightEntity hwEntity = JsonConvert.DeserializeObject<HeightWeightEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.RemoveHeightWeight(hwEntity);
                        break;
                    case MergeEntity.TableIDS.PHYSICAL_LEANINGS:

                        LeaningEntity plEntity = JsonConvert.DeserializeObject<LeaningEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateLeaning(plEntity);
                        break;
                    case MergeEntity.TableIDS.PHYSICAL_INVALIDS:
    
                        InvalidEntity piEntity = JsonConvert.DeserializeObject<InvalidEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateInvalides(piEntity);
                        break;
                    case MergeEntity.TableIDS.MEDICAL_PRELIMINARY_CONTENT:

                        MedicalPrimaryEntity medContent = JsonConvert.DeserializeObject<MedicalPrimaryEntity>(entity.Json, DateTimeSetting);
                        MedicalService.UpdatePrimary(medContent);
                        break;
                    case MergeEntity.TableIDS.MEDICAL_PRELIMINARY_COMPLATIN:
                    case MergeEntity.TableIDS.MEDICAL_AMBULATOR_HISTORY_COMPLATIN:
                    case MergeEntity.TableIDS.MEDICAL_STATIONARY_HISTORY_COMPLATIN:

                        MedicalComplaintsEntity medComplaint = JsonConvert.DeserializeObject<MedicalComplaintsEntity>(entity.Json, DateTimeSetting);
                        MedicalService.UpdateMedicalComplaints(medComplaint);
                        break;
                    case MergeEntity.TableIDS.MEDICAL_PRELIMINARY_DIAGNOISIS:
                    case MergeEntity.TableIDS.MEDICAL_STATIONARY_HISTORY_DIAGNOSIS:
                    case MergeEntity.TableIDS.MEDICAL_AMBULATOR_HISTORY_DIAGNOSIS:

                        MedicalDiagnosisEntity medDiagnosis = JsonConvert.DeserializeObject<MedicalDiagnosisEntity>(entity.Json, DateTimeSetting);
                        MedicalService.UpdateMedicalDiagnosis(medDiagnosis);
                        break;
                    case MergeEntity.TableIDS.MEDICAL_PRELIMINARY_EXTERNAL_EXAMINATION:

                        MedicalExternalExaminationEntity medExternalExamination = JsonConvert.DeserializeObject<MedicalExternalExaminationEntity>(entity.Json, DateTimeSetting);
                        MedicalService.UpdateMedicalExternalExamination(medExternalExamination);
                        break;
                    case MergeEntity.TableIDS.MEDICAL_AMBULATOR_HISTORY:
                    case MergeEntity.TableIDS.MEDICAL_STATIONARY_HISTORY:

                        MedicalHistoryEntity medHistory = JsonConvert.DeserializeObject<MedicalHistoryEntity>(entity.Json, DateTimeSetting);
                        MedicalService.UpdateMedicalHistory(medHistory);
                        break;
                    case MergeEntity.TableIDS.MEDICAL_AMBULATOR_HISTORY_RESEARCH:
                    case MergeEntity.TableIDS.MEDICAL_STATIONARY_HISTORY_RESEARCH:

                        MedicalResearchEntity medResearch = JsonConvert.DeserializeObject<MedicalResearchEntity>(entity.Json, DateTimeSetting);
                        MedicalService.UpdateMedicalResearch(medResearch);
                        break;
                    case MergeEntity.TableIDS.FINGERTATOOSCARS_FINGERS:
                    case MergeEntity.TableIDS.FINGERTATOOSCARS_TATOOS:
                    case MergeEntity.TableIDS.FINGERTATOOSCARS_SCARS:

                        FingerprintsTattoosScarsItemEntity medFingerTatooScars = JsonConvert.DeserializeObject<FingerprintsTattoosScarsItemEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateFingerprintsTattoosScarsItem(medFingerTatooScars);
                        break;
                    case MergeEntity.TableIDS.FAMILY_CONTENT:

                        FamilyRelativeEntity FamiltyContent = JsonConvert.DeserializeObject<FamilyRelativeEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateFamilyRelations(FamiltyContent);
                        break;
                    case MergeEntity.TableIDS.FAMILY_SPOUSES:

                        SpousesEntity FamiltySpouses = JsonConvert.DeserializeObject<SpousesEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateSpouses(FamiltySpouses);
                        break;
                    case MergeEntity.TableIDS.FAMILY_PARENTS:

                        ParentsEntity FamiltyParents = JsonConvert.DeserializeObject<ParentsEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateParents(FamiltyParents);
                        break;
                    case MergeEntity.TableIDS.FAMILY_OTHERS:

                        OtherRelativesEntity FamiltyOthers = JsonConvert.DeserializeObject<OtherRelativesEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateOtherRelatives(FamiltyOthers);
                        break;
                    case MergeEntity.TableIDS.FAMILY_CHILDRENS:

                        ChildrenEntity FamiltyChildren = JsonConvert.DeserializeObject<ChildrenEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateChildren(FamiltyChildren);
                        break;
                    case MergeEntity.TableIDS.FAMILY_SYSTER_BROTHER:

                        SisterBrotherEntity FamiltySysterBrother = JsonConvert.DeserializeObject<SisterBrotherEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateSisterBrother(FamiltySysterBrother);
                        break;
                    case MergeEntity.TableIDS.PHYSICAL_FILES:
                    case MergeEntity.TableIDS.PRIMARY_IMAGES:
                    case MergeEntity.TableIDS.PRIMARY_FIELS:
                    case MergeEntity.TableIDS.MEDICAL_FILES:
                    case MergeEntity.TableIDS.FAMILY_FILES:
                    case MergeEntity.TableIDS.ARMY_FILES:
                    case MergeEntity.TableIDS.ENTERANCE_FILES_ARREST:
                    case MergeEntity.TableIDS.ENTERANCE_FILES:
                    case MergeEntity.TableIDS.CELL_FILES:
                    case MergeEntity.TableIDS.ITEMS_FILES:
                    case MergeEntity.TableIDS.BAN_FILES:
                    case MergeEntity.TableIDS.ENCOURAGEMENTS_FILES:
                    case MergeEntity.TableIDS.PENALTY_FILES:
                    case MergeEntity.TableIDS.EDUCATIONAL_COURSES_FILES:
                    case MergeEntity.TableIDS.WORKLOADS_FILES:
                    case MergeEntity.TableIDS.OFFICIAL_VISITS_FILES:
                    case MergeEntity.TableIDS.PERSONAL_VISITS_FILES:
                    case MergeEntity.TableIDS.PACKAGES_FILES:
                    case MergeEntity.TableIDS.DEPARTURES_FILES:
                    case MergeEntity.TableIDS.TRANSFERS_FILES:
                    case MergeEntity.TableIDS.CONFLICTS_FILES:

                        AdditionalFileEntity afEntity = JsonConvert.DeserializeObject<AdditionalFileEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateFiles(afEntity);
                        break;
                    case MergeEntity.TableIDS.BAN_MAIN:

                        InjunctionItemEntity iiEntity = JsonConvert.DeserializeObject<InjunctionItemEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateInjunctions(iiEntity);
                        break;
                    case MergeEntity.TableIDS.CONFLICTS_MAIN:

                        ConflictsEntity confEntity = JsonConvert.DeserializeObject<ConflictsEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateConflict(confEntity);
                        break;
                    case MergeEntity.TableIDS.DEPARTURES_MAIN:

                        DepartureEntity depEntity = JsonConvert.DeserializeObject<DepartureEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateDeparture(depEntity);
                        break;
                    case MergeEntity.TableIDS.EDUCATIONAL_COURSES_MAIN:
                        EducationalCoursesEntity ecEntity = JsonConvert.DeserializeObject<EducationalCoursesEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateEducationalCourse(ecEntity);
                        break;
                    case MergeEntity.TableIDS.EDUCATION:
                        EducationsProfessionsEntity EducationProfesion = JsonConvert.DeserializeObject<EducationsProfessionsEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateEducationProfessions(EducationProfesion);
                        break;
                    case MergeEntity.TableIDS.ENCOURAGEMENTS_MAIN:
                        EncouragementsEntity encEntity = JsonConvert.DeserializeObject<EncouragementsEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateEncouragements(encEntity);
                        break;
                    case MergeEntity.TableIDS.ITEMS_MAIN:
                        ItemEntity itEntity = JsonConvert.DeserializeObject<ItemEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateItemObject(itEntity);
                        break;
                    case MergeEntity.TableIDS.OFFICIAL_VISITS_MAIN:
                        OfficialVisitsEntity ofvEntity = JsonConvert.DeserializeObject<OfficialVisitsEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateOfficialVisit(ofvEntity);
                        break;
                    case MergeEntity.TableIDS.PACKAGES_MAIN:
                        PackagesEntity packEntity = JsonConvert.DeserializeObject<PackagesEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdatePackage(packEntity);
                        break;
                    case MergeEntity.TableIDS.ARMY_CONTENT:
                        MilitaryServiceEntity armyContentEntity = JsonConvert.DeserializeObject<MilitaryServiceEntity>(entity.Json, DateTimeSetting);
                        this.UpdateMilitaryService(armyContentEntity);
                        break;
                    case MergeEntity.TableIDS.PENALTY_MAIN:
                        PenaltiesEntity penEntity = JsonConvert.DeserializeObject<PenaltiesEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdatePenalties(penEntity);
                        if (penEntity.ID != null && penEntity.PrisonerID != null)
                        {
                            PrisonerService.UpdatePenalties(penEntity);

                            List<PenaltiesViolationEntity> oldList = PrisonerService.GetViolationLibItemToPenalty(new PenaltiesViolationEntity(PenaltyID: penEntity.ID));

                            List<PenaltiesViolationEntity> newList = penEntity.ViolationList;

                            foreach (PenaltiesViolationEntity item in oldList)
                            {
                                if (!newList.FindAll(r => r.LibItemID == item.LibItemID).Any())
                                {
                                    bool tempStatus = PrisonerService.UpdateViolationLibItemToPenalty(new PenaltiesViolationEntity(ID: item.ID, Status: false));
                                }
                            }

                            foreach (PenaltiesViolationEntity item in newList)
                            {
                                if (!oldList.FindAll(r => r.LibItemID == item.LibItemID).Any())
                                {
                                    item.PenaltyID = penEntity.ID;
                                    item.PrisonerID = penEntity.PrisonerID;
                                    int? tempID = PrisonerService.AddViolationLibItemToPenalty(item);
                                }
                            }
                        }
                        break;
                    case MergeEntity.TableIDS.TRANSFERS_MAIN:
                        TransferEntity transEntity = JsonConvert.DeserializeObject<TransferEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateTransfer(transEntity);
                        break;
                    case MergeEntity.TableIDS.WORKLOADS_MAIN:
                        WorkLoadsEntity worklEntity = JsonConvert.DeserializeObject<WorkLoadsEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateWorkLoad(worklEntity);
                        break;
                    case MergeEntity.TableIDS.PERSONAL_VISITS_MAIN:
                        PersonalVisitsEntity pvisEntity = JsonConvert.DeserializeObject<PersonalVisitsEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdatePersonalVisit(pvisEntity);
                        break;
                    case MergeEntity.TableIDS.ENTERANCE_SENTENCE:
                        SentencingDataEntity sentdEntity = JsonConvert.DeserializeObject<SentencingDataEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateSentencingData(sentdEntity);
                        break;
                    case MergeEntity.TableIDS.ENTERANCE_PROTOCOL:
                        PrisonAccessProtocolEntity pprotEntity = JsonConvert.DeserializeObject<PrisonAccessProtocolEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdatePrisonAccessProtocol(pprotEntity);
                        break;
                    case MergeEntity.TableIDS.ENTERANCE_PrisonerTypeStatus:
                        PrisonerTypeStatusEntity pristypeEntity = JsonConvert.DeserializeObject<PrisonerTypeStatusEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdatePrisonerTypeStatus(pristypeEntity);
                        break;
                    case MergeEntity.TableIDS.ENTERANCE_PRISONERPUNISHMENTTYPE:
                        PrisonerPunishmentTypeEntity prispunishtypeEntity = JsonConvert.DeserializeObject<PrisonerPunishmentTypeEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdatePrisonerPunishmentType(prispunishtypeEntity);
                        break;
                    case MergeEntity.TableIDS.ARMY_MAIN:
                        ArmyEntity armyEntity = JsonConvert.DeserializeObject<ArmyEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateArmy(armyEntity);
                        break;
                    case MergeEntity.TableIDS.ENTERANCE_ARREST_DATA:
                        ArrestDataEntity arestEntity = JsonConvert.DeserializeObject<ArrestDataEntity>(entity.Json, DateTimeSetting);
                        PrisonerService.UpdateArrestData(arestEntity);
                        break;
                }

                UpdateModification(new MergeEntity(ID: entity.ID, ApproverID: entity.ApproverID, Approved: true, Date: DateTime.Now, Status: false));

                context.Submit();
            }
            return false;
        }

        private bool UpdateMilitaryService(MilitaryServiceEntity Entity)
        {
            bool status = true;

            BL_PrisonersLayer PrisonerService = new BL_PrisonersLayer();

            if (Entity != null && Entity.PrisonerID != null)
            {
                MilitaryServiceEntity curPrisonerMilitary = new MilitaryServiceEntity();

                curPrisonerMilitary.Army = PrisonerService.GetArmy(new ArmyEntity(PrisonerID: Entity.PrisonerID.Value));

                status = true; ;


                // check awards
                curPrisonerMilitary.ArmyAwards = PrisonerService.GetArmyAwards(Entity.PrisonerID.Value);
                curPrisonerMilitary.WarInvolved = PrisonerService.GetWarInvolved(new WarInvolvedEntity(PrisonerID: Entity.PrisonerID.Value));
                NotServedEntity curEntity = PrisonerService.GetNotServed(new NotServedEntity(PrisonerID: Entity.PrisonerID));

              
                if (curPrisonerMilitary.ArmyAwards != null && Entity.ArmyAwards != null)
                {
                    // update ArmyAwards
                    foreach (ArmyAwardEntity currentEntity in Entity.ArmyAwards)
                    {
                        if (currentEntity.LibItemID != null && currentEntity.PrisonerID != null && !curPrisonerMilitary.ArmyAwards.FindAll(r => r.LibItemID == currentEntity.LibItemID).Any())
                        {
                            int? curID = PrisonerService.AddArmyAwards(currentEntity);
                            if (curID == null) status = false;
                        }
                    }
                    foreach (ArmyAwardEntity currentEntity in curPrisonerMilitary.ArmyAwards)
                    {
                        if (!Entity.ArmyAwards.FindAll(r => (r.LibItemID != null && r.PrisonerID != null && r.LibItemID == currentEntity.LibItemID)).Any())
                        {
                            currentEntity.Status = false;
                            bool tempStatus = PrisonerService.UpdateArmyAwards(currentEntity);
                            if (!tempStatus) status = false;
                        }
                    }
                }

                // check war involved

                if (curPrisonerMilitary.WarInvolved != null && Entity.WarInvolved != null)
                {
                    // update War involved
                    foreach (WarInvolvedEntity currentEntity in Entity.WarInvolved)
                    {
                        if (currentEntity.LibItemID != null && currentEntity.PrisonerID != null && !curPrisonerMilitary.WarInvolved.FindAll(r => r.LibItemID == currentEntity.LibItemID).Any())
                        {
                            int? curID = PrisonerService.AddWarInvolved(currentEntity);
                            if (curID == null) status = false;
                        }
                    }
                    foreach (WarInvolvedEntity currentEntity in curPrisonerMilitary.WarInvolved)
                    {
                        if (!Entity.WarInvolved.FindAll(r => (r.LibItemID != null && r.PrisonerID != null && r.LibItemID == currentEntity.LibItemID)).Any())
                        {
                            currentEntity.Status = false;
                            bool tempStatus = PrisonerService.UpdateWarInvolved(currentEntity);
                            if (!tempStatus) status = false;
                        }
                    }
                }

                if (Entity.NotServed != null && Entity.NotServed.ReasonLibItemID != null && Entity.NotServed.PrisonerID != null)
                {
                    status = true;

                    if (curPrisonerMilitary.Army != null && curPrisonerMilitary.Army.Any() && curEntity != null && curEntity.ID != null)
                    {
                        status = PrisonerService.UpdateNotServed(new NotServedEntity(ID: curEntity.ID, Status: false));
                    }
                    else if (curPrisonerMilitary.Army == null || !curPrisonerMilitary.Army.Any())
                    {
                        if (curEntity != null && curEntity.PrisonerID != null && curEntity.ReasonLibItemID != null)
                        {
                            if ((curEntity.ReasonLibItemID != Entity.NotServed.ReasonLibItemID || curEntity.Description != Entity.NotServed.Description) && curEntity.PrisonerID == Entity.NotServed.PrisonerID)
                            {
                                curEntity.ReasonLibItemID = Entity.NotServed.ReasonLibItemID;
                                curEntity.Description = Entity.NotServed.Description;
                                status = PrisonerService.UpdateNotServed(curEntity);
                            }
                        }
                        else
                        {
                            int? id = PrisonerService.AddNotServed(Entity.NotServed);
                            if (id == null) status = false;
                        }
                    }
                }

            }

            return status;
        }
    }
}
