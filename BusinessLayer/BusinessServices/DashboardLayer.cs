﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessServices
{
    public class BL_DashboardLayer : BusinessServicesCommunication
    {
        #region Sentence
        public List<SentencingOverdueDashboardDataEntity> GetListOverdueForDashboard(int OrgUnitID)
        {
            return DAL_SentencingDataService.SP_GetList_OverdueForDashboard(OrgUnitID);
        }

        public List<SentencingOverdueDashboardDataEntity> GetListSevenDayForDashboard(int OrgUnitID)
        {
            return DAL_SentencingDataService.SP_GetList_SevenDayForDashboard(OrgUnitID);
        }

        public List<SentencingOverdueDashboardDataEntity> GetListEarlyDateForDashboard(int OrgUnitID)
        {
            return DAL_SentencingDataService.SP_GetList_EarlyDate(OrgUnitID);
        }
        #endregion
    }
}
