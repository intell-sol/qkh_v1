﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using CommonLayer.BusinessEntities;
using System.Linq;

namespace BusinessLayer.BusinessServices
{
    
    public class BL_PoliceService: BusinessServicesCommunication
    {
        private enum RequestType {
            PersonalNumber = 1,
            IdentityDocument = 2,
            PersonalData = 3
        }
        private static string EndPoint = CommonLayer.HelperClasses.ConfigurationHelper.GetServiceAVVURL();

//        private static string testBody = @"<?xml version=""1.0"" encoding=""utf-8""?>
//<Envelope xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns=""http://ErMMGI.org/ag/AG_Objects.xsd"">
//    <Header>Գործող  անձնագրերի  որոնում. գործող  անձնագրային  տվյալներով:</Header>
//    <Body>
//        <AVVResponse xsi:type=""AVVResponse60"">
//            <Response>
//                <Exit_Code Code=""-1"" Message=""Հաջող ավարտ"" />
//                <Exit_Date>01/12/2016 5:45:28 PM</Exit_Date>
//                <Request_Code>60</Request_Code>
//                <Request_Number>7777777</Request_Number>
//                <Request_Department>001</Request_Department>
//            </Response>
//            <AVVPersons>
//                <AVVPerson>
//                    <PNum>3108830961</PNum>
//                    <IsDead>false</IsDead>
//                    <AVVDocuments>
//                        <Document>
//                            <Document_Status>ACTIVE</Document_Status>
//                            <Document_Type>NON_BIOMETRIC_PASSPORT</Document_Type>
//                            <Document_Number>AF0362794</Document_Number>
//                            <Document_Department>057</Document_Department>
//                            <Person>
//                                <Nationality>40</Nationality>
//                                <Citizenship>ARM</Citizenship>
//                                <Last_Name>ՄԱՐԳԱՐՅԱՆ</Last_Name>
//                                <First_Name>ԳԵՎՈՐԳ</First_Name>
//                                <Patronymic_Name>ՎԼԱԴԻՄԻՐԻ</Patronymic_Name>
//                                <Birth_Date>21/08/1983</Birth_Date>
//                                <Genus>M</Genus>
//                                <English_Last_Name>MARGARYAN</English_Last_Name>
//                                <English_First_Name>GEVORG</English_First_Name>
//                                <English_Patronymic_Name>VLADIMIRI</English_Patronymic_Name>
//                                <Birth_Country>ARM</Birth_Country>
//                                <Birth_Region>ԵՐԵՎԱՆ</Birth_Region>
//                                <Birth_Address>Ք. ԵՐԵՎԱՆ</Birth_Address>
//                            </Person>
//                            <PassportData>
//                                <Passport_Type>C</Passport_Type>
//                                <Passport_Issuance_Date>14/07/2001</Passport_Issuance_Date>
//                                <Passport_Validity_Date>14/07/2011</Passport_Validity_Date>
//                            </PassportData>
//                        </Document>
//                    </AVVDocuments>
//                    <AVVAddresses>
//                        <AVVAddress>
//                            <RegistrationAddress>
//                                <Registration_Region>ԱՐՄԱՎԻՐ</Registration_Region>
//                                <Registration_Community>ԱՅԳԵՇԱՏ(ՎԱՂԱՐՇԱՊԱՏ)</Registration_Community>
//                                <Registration_Street>ԳՐԱԴԱՐԱՆԱՅԻՆ Փ.</Registration_Street>
//                                <Registration_Building>12Բ</Registration_Building>
//                                <Registration_Building_Type>Տ</Registration_Building_Type>
//                            </RegistrationAddress>
//                            <RegistrationData>
//                                <Registration_Department>057</Registration_Department>
//                                <Registration_Date>14/07/2001</Registration_Date>
//                                <Registration_Code>Դ</Registration_Code>
//                                <Registration_Status>P</Registration_Status>
//                                <Registration_Aim>Հ</Registration_Aim>
//                                <Registered_Date>01/02/2008</Registered_Date>
//                                <Registered_Department>112</Registered_Department>
//                            </RegistrationData>
//                        </AVVAddress>
//                    </AVVAddresses>
//                </AVVPerson>
//            </AVVPersons>
//        </AVVResponse>
//    </Body>
//</Envelope>";


        private static AVVPerson getDataFromServer(String body)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(EndPoint);
                var data = Encoding.UTF8.GetBytes(body);

                request.Method = "POST";
                request.ContentType = "text/xml";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var responce = (HttpWebResponse)request.GetResponse();


                if (responce.StatusCode == HttpStatusCode.OK)
                {
                    

                    var serializer = new XmlSerializer(typeof(Envelope));
                    Envelope envelop;
                    string xml = new StreamReader(responce.GetResponseStream()).ReadToEnd();
                    //xml = testBody;
                    envelop = (Envelope)serializer.Deserialize(new XmlTextReader(new StringReader(xml)));

                    if (envelop.Body.AVVResponse.AVVPersons.AVVPerson.Count != 1)
                        return null;



                    return envelop.Body.AVVResponse.AVVPersons.AVVPerson[0];
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
            return null;
        }

        private static string getRequsetBodyPSN(string psn,int code=60)
        {
            psn = psn.TrimEnd();
            psn = psn.TrimStart();
            return @"<?xml version=""1.0"" encoding=""utf-8"" ?>
                        <Envelope xmlns="""+ CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace + @""" >
	                        <Header>Որոնում</Header>
	                        <Body>
		                        <AVVRequest>
			                        <SignedData>
				                        <Data>
					                        <SignableData xsi:type=""AVVRequest"+ code.ToString()+ @""" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
						                        <Request>
							                        <Request_Code>" + code.ToString() + @"</Request_Code>
							                        <Request_Number>7777777</Request_Number>
							                        <Request_Department>001</Request_Department>
							                        <Request_Date>" + DateTime.Now.ToString("dd/MM/yyyy") + @"</Request_Date>
						                        </Request>
						                        <PersonalData>
							                        <PNum>" + psn + @"</PNum>
						                        </PersonalData>
					                        </SignableData>
				                        </Data>
				                        <Signature>
					                        <SignatureType>RSA</SignatureType>
					                        <SignatureValue>SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS</SignatureValue>
				                        </Signature>
			                        </SignedData>
		                        </AVVRequest>
	                        </Body>
                        </Envelope>";
         }

        private static string getRequsetBodyDocumentNumber(string DocumentNumber, int code=60)
        {
            DocumentNumber = DocumentNumber.TrimEnd();
            DocumentNumber = DocumentNumber.TrimStart();
            return @"<?xml version=""1.0"" encoding=""utf-8"" ?>
                        <Envelope xmlns=""" + CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace + @""">
	                        <Header>Որոնում</Header>
	                        <Body>
		                        <AVVRequest>
			                        <SignedData>
				                        <Data>
					                        <SignableData xsi:type=""AVVRequest" + code.ToString() + @""" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
						                        <Request>
							                        <Request_Code>" + code.ToString() + @"</Request_Code>
							                        <Request_Number>7777777</Request_Number>
							                        <Request_Department>001</Request_Department>
							                        <Request_Date>" + DateTime.Now.ToString("dd/MM/yyyy") + @"</Request_Date>
						                        </Request>
						                        <PersonalData>
							                        <Document_Number>" + DocumentNumber + @"</Document_Number>
						                        </PersonalData>
					                        </SignableData>
				                        </Data>
				                        <Signature>
					                        <SignatureType>RSA</SignatureType>
					                        <SignatureValue>SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS</SignatureValue>
				                        </Signature>
			                        </SignedData>
		                        </AVVRequest>
	                        </Body>
                        </Envelope>";
        }
        private static string getRequsetBodyPersonalData(string lastName = "", string firstName = "", string birthday = "", int code = 60)
        {
            lastName = lastName.TrimEnd();
            lastName = lastName.TrimStart();

            firstName = firstName.TrimEnd();
            firstName = firstName.TrimStart();
            
            birthday = birthday.TrimEnd();
            birthday = birthday.TrimStart();

            return @"<?xml version=""1.0"" encoding=""utf-8"" ?>
                        <Envelope xmlns=""" + CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace + @""">
	                        <Header>Որոնում</Header>
	                        <Body>
		                        <AVVRequest>
			                        <SignedData>
				                        <Data>
					                        <SignableData xsi:type=""AVVRequest" + code.ToString() + @""" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
						                        <Request>
							                        <Request_Code>" + code.ToString() + @"</Request_Code>
							                        <Request_Number>7777777</Request_Number>
							                        <Request_Department>001</Request_Department>
							                        <Request_Date>" + DateTime.Now.ToString("dd/MM/yyyy") + @"</Request_Date>
						                        </Request>
						                        <PersonalData>
							                        <Person>
				                                          <Last_Name>" + lastName + @"</Last_Name>
				                                          <First_Name>" + firstName + @"</First_Name>
				                                          <Birth_Date>" + birthday + @"</Birth_Date>
			                                        </Person>
						                        </PersonalData>
					                        </SignableData>
				                        </Data>
				                        <Signature>
					                        <SignatureType>RSA</SignatureType>
					                        <SignatureValue>SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS</SignatureValue>
				                        </Signature>
			                        </SignedData>
		                        </AVVRequest>
	                        </Body>
                        </Envelope>";
        }
        


        // TODO: Add Logging get functions from in police service 
        public AVVPerson getBYPersonalNumber(string psn)
        {
            AVVPerson person = getDataFromServer(getRequsetBodyPSN(psn));
            if(person == null)
                person = getDataFromServer(getRequsetBodyPSN(psn,61));

            if (person == null)
                person = getDataFromServer(getRequsetBodyPSN(psn, 53));
            if (person == null)
                person = getDataFromServer(getRequsetBodyPSN(psn, 55));
            return person;
        }

        public AVVPerson getBYDocumentNumber(string DocumentNumber)
        {
            AVVPerson person = getDataFromServer(getRequsetBodyDocumentNumber(DocumentNumber));
            if (person == null)
                person = getDataFromServer(getRequsetBodyDocumentNumber(DocumentNumber, 61));
            if (person == null)
                person = getDataFromServer(getRequsetBodyDocumentNumber(DocumentNumber, 53));
            if (person == null)
                person = getDataFromServer(getRequsetBodyDocumentNumber(DocumentNumber, 55));
            return person;
        }

        public AVVPerson getBYDocumentPersonalData(string lastName = "", string firstName = "", string birthday = "")
        {
            AVVPerson person = getDataFromServer(getRequsetBodyPersonalData(lastName, firstName, birthday));
            if (person == null)
                person = getDataFromServer(getRequsetBodyPersonalData(lastName, firstName, birthday, 61));
            if (person == null)
                person = getDataFromServer(getRequsetBodyPersonalData(lastName, firstName, birthday, 53));
            if (person == null)
                person = getDataFromServer(getRequsetBodyPersonalData(lastName, firstName, birthday, 55));
            return person;
        }

    }

    [XmlRoot(ElementName = "Exit_Code", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class Exit_Code
    {
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
        [XmlAttribute(AttributeName = "Message")]
        public string Message { get; set; }
    }

    [XmlRoot(ElementName = "Response", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class Response
    {
        [XmlElement(ElementName = "Exit_Code", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public Exit_Code Exit_Code { get; set; }
        [XmlElement(ElementName = "Exit_Date", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Exit_Date { get; set; }
        [XmlElement(ElementName = "Request_Code", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Request_Code { get; set; }
        [XmlElement(ElementName = "Request_Number", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Request_Number { get; set; }
        [XmlElement(ElementName = "Request_Department", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Request_Department { get; set; }
    }

    [XmlRoot(ElementName = "Person", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class Person: BusinessServicesCommunication
    {
        [XmlElement(ElementName = "Nationality", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Nationality { get; set; }
        public string NationalityLabel { get {
                return base.DAL_NationalityService.SP_GetList_Nationality(null, int.Parse(Nationality))[0].Label;
        } } 
        [XmlElement(ElementName = "Citizenship", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Citizenship { get; set; }
        [XmlElement(ElementName = "Last_Name", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Last_Name { get; set; }
        [XmlElement(ElementName = "First_Name", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string First_Name { get; set; }
        [XmlElement(ElementName = "Patronymic_Name", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Patronymic_Name { get; set; }
        [XmlElement(ElementName = "Birth_Date", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Birth_Date { get; set; }
        //public DateTime Birth_Date_
        //{
        //    get
        //    {
        //        return Convert.ToDateTime(Birth_Date);
        //    }
        //    set
        //    {
        //        Birth_Date = Convert.ToString(value);
        //    }
        //}
        [XmlElement(ElementName = "Genus", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Genus { get; set; }
        [XmlElement(ElementName = "English_Last_Name", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string English_Last_Name { get; set; }
        [XmlElement(ElementName = "English_First_Name", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string English_First_Name { get; set; }
        [XmlElement(ElementName = "English_Patronymic_Name", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string English_Patronymic_Name { get; set; }
        [XmlElement(ElementName = "Birth_Country", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Birth_Country { get; set; }
        [XmlElement(ElementName = "Birth_Region", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Birth_Region { get; set; }
        [XmlElement(ElementName = "Birth_Community", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Birth_Community { get; set; }
        [XmlElement(ElementName = "Birth_Address", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Birth_Address { get; set; }
    }

    [XmlRoot(ElementName = "PassportData", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class PassportData
    {
        [XmlElement(ElementName = "Passport_Type", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Passport_Type { get; set; }
        [XmlElement(ElementName = "Passport_Issuance_Date", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Passport_Issuance_Date { get; set; }
        [XmlElement(ElementName = "Passport_Validity_Date", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Passport_Validity_Date { get; set; }
        [XmlElement(ElementName = "Passport_Extension_Date", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Passport_Extension_Date { get; set; }
        [XmlElement(ElementName = "Passport_Extension_Department", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Passport_Extension_Department { get; set; }
    }

    [XmlRoot(ElementName = "Document", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class Document
    {
        [XmlElement(ElementName = "Document_Status", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public Document_Status Document_Status { get; set; }
        [XmlElement(ElementName = "Document_Type", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public Document_Type Document_Type { get; set; }
        [XmlElement(ElementName = "Document_Number", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Document_Number { get; set; }
        [XmlElement(ElementName = "Document_Department", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Document_Department { get; set; }
        [XmlElement(ElementName = "Person", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public Person Person { get; set; }
        [XmlElement(ElementName = "PassportData", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public PassportData PassportData { get; set; }
    }

    [XmlRoot(ElementName = "AVVDocuments", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class AVVDocuments
    {
        [XmlElement(ElementName = "Document", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public List<Document> Document { get; set; }
    }

    [XmlRoot(ElementName = "RegistrationData", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class RegistrationData
    {
        [XmlElement(ElementName = "Registration_Date", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Registration_Date { get; set; }
        [XmlElement(ElementName = "Registration_Code", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Registration_Code { get; set; }
        [XmlElement(ElementName = "Registration_Status", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public Registration_Status Registration_Status { get; set; }
        [XmlElement(ElementName = "Registered_Date", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Registered_Date { get; set; }
        [XmlElement(ElementName = "Registered_Department", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Registered_Department { get; set; }
        [XmlElement(ElementName = "Registration_Department", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Registration_Department { get; set; }
    }
    
    [XmlRoot(ElementName = "AVVAddress", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class AVVAddress
    {
        [XmlElement(ElementName = "RegistrationAddress", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public RegistrationAddress RegistrationAddress { get; set; }
        [XmlElement(ElementName = "RegistrationData", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public RegistrationData RegistrationData { get; set; }
    }

    [XmlRoot(ElementName = "AVVAddresses", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class AVVAddresses
    {
        [XmlElement(ElementName = "AVVAddress", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public List<AVVAddress> AVVAddress { get; set; }
    }

    [XmlRoot(ElementName = "AVVPerson", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class AVVPerson
    {
        [XmlElement(ElementName = "PNum", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string PNum { get; set; }
        [XmlElement(ElementName = "IsDead", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string IsDead { get; set; }
        [XmlElement(ElementName = "Photo_ID", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Photo_ID { get; set; }
        [XmlElement(ElementName = "AVVDocuments", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public AVVDocuments AVVDocuments { get; set; }
        [XmlElement(ElementName = "AVVAddresses", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public AVVAddresses AVVAddresses { get; set; }
        public PersonEntity ToPerson()
        {
            Document idCard = AVVDocuments.Document.Find(d => d.Document_Type == Document_Type.ID_CARD);
            Document biometricPassport = AVVDocuments.Document.Find(d => d.Document_Type == Document_Type.BIOMETRIC_PASSPORT && d.Document_Status == Document_Status.ACTIVE);
            Document nonBiometricPassport = AVVDocuments.Document.Find(d => d.Document_Type == Document_Type.NON_BIOMETRIC_PASSPORT && d.Document_Status == Document_Status.ACTIVE);

            Document document = idCard == null ? nonBiometricPassport : idCard;
            document = document ?? AVVDocuments.Document.First();

            string firstName = document.Person.First_Name;
            string middleName = document.Person.Patronymic_Name;
            string lastName = document.Person.Last_Name;

            AVVAddress PermanentAddress = AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Code == "Ն");
            PermanentAddress = PermanentAddress ?? AVVAddresses.AVVAddress.First();

            AVVAddress ActiveAddress = AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.A);
            AVVAddress TemporaryAddress = AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.T);

            string Nationality = document.Person.NationalityLabel;
            string PassportDepartment = document.Document_Department;
            string PassportIssuanceDate = document.PassportData.Passport_Issuance_Date;

            RegistrationAddress liv = ActiveAddress != null ? ActiveAddress.RegistrationAddress : PermanentAddress.RegistrationAddress;
            RegistrationAddress reg = PermanentAddress.RegistrationAddress;

            //string LivingAddress = liv.Registration_Region + ", " + liv.Registration_Community + " " + (liv.Registration_Apartment != null ? liv.Registration_Apartment : "") + ", " + liv.Registration_Street + " " + liv.Registration_Building_Type + " " + liv.Registration_Building;

            //string RegistrationAddress = reg.Registration_Region + ", " + reg.Registration_Community + " " + (reg.Registration_Apartment != null ? reg.Registration_Apartment : "") + ", " + reg.Registration_Street + " " + reg.Registration_Building_Type + " " + reg.Registration_Building;

            // gender part
            // getting gender libs

            List<LibsEntity> genderLibs = BL_LibsLayer.getInstance().GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);
            foreach (LibsEntity lib in genderLibs)
            {
                // calling getting lib path method from business layer
                lib.propList = BL_PropsLayer.getInstance().GetPropsList(lib.ID);
            }

            int? GenderLibID = null;

            foreach (LibsEntity curGender in genderLibs)
            {
                PropsEntity idCodeProp = curGender.propList.Find(w => w.ID == (int)PropsEntity.PropToLibID.IDENTIFICATION_CODE);
                string value = idCodeProp.Values[0].Value;
                if (value == document.Person.Genus)
                {
                    GenderLibID = curGender.ID;
                    break;
                }
            }

            // nationality part
            List<NationalityEntity> nationality = BL_LibsLayer.getInstance().GetNationality(Code: int.Parse(document.Person.Nationality));

            PersonEntity newPerson = new PersonEntity(Personal_ID: PNum, FirstName: firstName, MiddleName: middleName, LastName: lastName, Birthday: DateTime.ParseExact(document.Person.Birth_Date, "dd/MM/yyyy", null),
                SexLibItem_ID: Convert.ToInt32(GenderLibID.Value), NationalityID: Convert.ToInt32(nationality[0].ID.Value));

            newPerson.Registration_Entity = reg;
            newPerson.Living_Entity = liv;
     
            List<IdentificationDocument> docIdentities = new List<IdentificationDocument>();
            foreach (Document tempDoc in AVVDocuments.Document)
            {
                IdentificationDocument currentDocumentIdentity = new IdentificationDocument();

                Document_Type docType = tempDoc.Document_Type;
                string docTypeName = BL_LibsLayer.getInstance().GetLibByID((int)docType).Name;

                LibsEntity citizenshipLib = BL_LibsLayer.getInstance().GetCitizenshipLibByCode(tempDoc.Person.Citizenship);

                currentDocumentIdentity.TypeLibItem_ID = (int)docType;
                currentDocumentIdentity.TypeLibItem_Name = docTypeName;
                currentDocumentIdentity.CitizenshipLibItem_ID = citizenshipLib.ID;
                currentDocumentIdentity.CitizenshipLibItem_Name = citizenshipLib.Name;
                currentDocumentIdentity.Number = tempDoc.Document_Number;
                currentDocumentIdentity.FromWhom = tempDoc.Document_Department;
                currentDocumentIdentity.Type = true;
                currentDocumentIdentity.Date = DateTime.ParseExact(tempDoc.PassportData.Passport_Issuance_Date, "dd/MM/yyyy", null);
                currentDocumentIdentity.ID = int.Parse(currentDocumentIdentity.TypeLibItem_ID.ToString() + currentDocumentIdentity.CitizenshipLibItem_ID.ToString());

                docIdentities.Add(currentDocumentIdentity);
            }

            newPerson.Photo_ID = this.Photo_ID;

            newPerson.IdentificationDocuments = docIdentities;

            return newPerson;
        }
    }

    [XmlRoot(ElementName = "AVVPersons", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class AVVPersons
    {
        [XmlElement(ElementName = "AVVPerson", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public List<AVVPerson> AVVPerson { get; set; }
    }


    [System.Xml.Serialization.XmlIncludeAttribute(typeof(AVVResponse53))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(AVVResponse55))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(AVVResponse60))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(AVVResponse61))]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class AVVResponse
    {
        [XmlElement(ElementName = "Response", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public Response Response { get; set; }
        [XmlElement(ElementName = "AVVPersons", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public AVVPersons AVVPersons { get; set; }
        [XmlAttribute(AttributeName = "type", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "AVVResponse", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class AVVResponse60: AVVResponse
    {
  
    }
    [XmlRoot(ElementName = "AVVResponse", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class AVVResponse61: AVVResponse
    {

    }

    [XmlRoot(ElementName = "AVVResponse", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class AVVResponse53 : AVVResponse
    {

    }

    [XmlRoot(ElementName = "AVVResponse", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class AVVResponse55 : AVVResponse
    {

    }

    [XmlRoot(ElementName = "Body", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class Body
    {
        [XmlElement(ElementName = "AVVResponse", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public AVVResponse AVVResponse { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
    public class Envelope
    {
        [XmlElement(ElementName = "Header", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Header { get; set; }
        [XmlElement(ElementName = "Body", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public Body Body { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "xsd", Namespace = CommonLayer.HelperClasses.ConfigurationHelper.XMLNamespace)]
        public string Xsd { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    public enum Document_Status
    {
        ACTIVE = 1,
        NO_ACTIVE = 0
    }

    public enum Document_Type
    {
        NONE = 0,
        NON_BIOMETRIC_PASSPORT = 161,
        DRIVER_LICENSE = 162,
        ID_CARD = 163,
        BIOMETRIC_PASSPORT = 164,
        TEXEKANQ = 3522,
        ZIN_GRQUYK = 3523
    }

    public enum Registration_Status
    {
        P = 0,
        A = 1,
        T = 2
    }
}
