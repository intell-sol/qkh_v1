﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC___Internal_System_Administration.Models
{
    public class YesOrNoViewModel
    {
        private int type { get; set; }
        public YesOrNoViewModel(int type)
        {
            this.type = type;
        }
        public string GetName() {
            string text = "";
            switch (this.type)
            {
                case 0:
                    text = "Դուք համոզվա՞ծ եք, որ ուզում եք հեռացնել այս բաժինը";
                    break;
                case 1:
                    text = "Դուք համոզվա՞ծ եք, որ ուզում եք հեռացնել այս տողը";
                    break;
                case 2:
                    text = "Դուք համոզվա՞ծ եք, որ ցանկանում եք հաստատել";
                    break;
                case 3:
                    text = "Դուք համոզվա՞ծ եք, որ ցանկանում եք մերժել";
                    break;
                case 4:
                    text = "Դուք համոզվա՞ծ եք, որ ցանկանում եք ավարտել";
                    break;
                default:
                    text = "Դուք համոզվա՞ծ եք, որ ուզում եք հեռացնել այս բաժինը";
                    break;
            }

            return text;
        }
    }
}