﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC___Internal_System_Administration.Models
{
    public class ListsViewModel
    {
            public LibPathEntity LibsList { get; set; }
            public List<LibPathEntity> LibPathList { get; set; }
            public List<LibsEntity> LibList { get; set; }
            public List<PropsEntity> PropertyList { get; set; }
            public LibsEntity CurrentLibItem { get; set; }
            public List<string> PathWay { get; set; }
            public string CurrentLibName { get; set; }
            public string SystemUserFullName { get; set; }
            public int ParentID { get; set; }
            public bool IsPath { get; set; }
            public int? LibID { get; set; }
            public int? PathID { get; set; }
    }
}