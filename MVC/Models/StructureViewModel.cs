﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC___Internal_System_Administration.Models
{
    public class StructureViewModel
    {
        public OrgUnitEntity RootOrganizationUnit { get; set; }
        public List<OrgUnitEntity> OrganizationUnits { get; set; }
        public List<OrgTypeEntity> OrganizationTypes { get; set; }

        public OrgUnitEntity CurrentOrgUnit { get; set; }
        public int? SelectedTypeID { get; set; }

        public List<OrgUnitEntity> BindPositionList { get; set; }
        public List<PermEntity> AllPermisionList { get; set; }
        public List<OrgUnitEntity> AllApproveList { get; set; }
        public List<OrgUnitEntity> VisibleOrgUnitList { get; set; }
        public EmployeeEntity CurrentEmployee { get; set; }
        public string PhotoLinkDefault { get; set; }

        public List<LibsEntity> Gender { get; set; }
        public List<NationalityEntity> Nationality { get; set; }
        public List<LibsEntity> DocIdentity { get; set; }
        public List<LibsEntity> Citizenships { get; set; }
        public PersonEntity Person { get; set; }
        public List<EmployeeEntity> Employees { get; set; }
    }
}