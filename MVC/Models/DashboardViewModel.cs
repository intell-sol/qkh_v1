﻿using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC___Internal_System_Administration.Models
{
    public class DashboardViewModel
    {
            public string SystemUserFullName { get; set; }

            public LibPathEntity LibsList { get; set; }

            public string Name { get; set; }

            public int ParentID { get; set; }
    }
}