﻿using System.Web;
using System.Web.Mvc;

namespace MVC___Internal_System_Administration
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
