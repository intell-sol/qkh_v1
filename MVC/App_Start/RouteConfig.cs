﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVC___Internal_System_Administration
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Structure",
                url: "Structure/{action}/{id}",
                defaults: new { controller = "Structure", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Lists",
                url: "Lists/{action}",
                defaults: new { controller = "Lists", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "Login", id = UrlParameter.Optional }
            );
        }
    }
}
