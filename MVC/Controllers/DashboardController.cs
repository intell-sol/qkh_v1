﻿using MVC___Internal_System_Administration.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC___Internal_System_Administration.Controllers
{
    public class DashboardController : BaseController
    {
        //
        // GET: /Dashboard/
        public ActionResult Dashboard()
        {
            try
            {
                //testing sasd
                ViewBag.Title = "Dashboard";

                DashboardViewModel model = new DashboardViewModel();
                model.LibsList = BusinessLayer_LibsLayer.GetLibPath();

                 return View(model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //log error
                return View("Error");
            }
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(DashboardViewModel model)
        {
            try
            {
                string Name = model.Name;
                int ParentID = model.ParentID;

                BusinessLayer_LibsLayer.AddLibPath(Name, ParentID);
                return RedirectToAction("Dashboard", "Dashboard");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return View("Error");
            }
        }
    }
}