﻿using MVC___Internal_System_Administration.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using CommonLayer.BusinessEntities;
using BusinessLayer.BusinessServices;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Converters;
using System.IO;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters.Binary;


namespace MVC___Internal_System_Administration.Controllers
{
    public class _Popup_Controller : BaseController
    {
        /// <summary>
        /// Get lib default view
        /// </summary>
        public ActionResult Get_LibDefaultView() {

            return PartialView("LibDefaultView");
        }

        /// <summary>
        /// Get lib content
        /// </summary>
        public ActionResult Get_LibContentView(int ParentID)
        {
            ListsViewModel Model = new ListsViewModel();

            List<LibPathEntity> LibPathList = BusinessLayer_LibsLayer.GetLibTreePathByParentID(ParentID: ParentID);

            foreach (var item in LibPathList)
            {
                if (item.LibsCount == 0)
                {
                    item.LibsCount = BusinessLayer_LibsLayer.GetLibCountByLibPathID(LibPathID: item.ID);
                }
            }

            Model.PathWay = this.GetPathWay(ParentID);

            Model.LibPathList = LibPathList;

            return PartialView("LibContentView", Model);
        }

        /// <summary>
        /// Get selectd lib content
        /// </summary>
        public ActionResult Get_LibSelectedView(int? PathID = null, int? LibID = null)
        {
            ListsViewModel Model = new ListsViewModel();

            Model.PathWay = this.GetPathWay(PathID.Value);

            Model.LibList = new List<LibsEntity>();

            if (LibID == null && PathID != null)
            {
                LibPathEntity CurrentLibPath = BusinessLayer_LibsLayer.GetLibPathByID(PathID.Value);
                Model.CurrentLibName = (CurrentLibPath != null) ? CurrentLibPath.Name : "";
                Model.LibList = BusinessLayer_LibsLayer.GetLibsByLibPathID(LibPathID: PathID.Value);
            }
            else
            {
                LibsEntity CurrentLib = BusinessLayer_LibsLayer.GetLibByID(LibID: LibID.Value);
                Model.CurrentLibName = (CurrentLib != null) ? CurrentLib.Name : "";
                Model.LibList = BusinessLayer_LibsLayer.GetLibsByLibParentID(LibPathID: LibID);
            }

            Model.IsPath = LibID == null;

            Model.PathID = PathID;
            Model.LibID = LibID;

            return PartialView("LibSelectedView", Model);
        }

        /// <summary>
        /// Get selectd lib item
        /// </summary>
        public ActionResult Get_LibItem(int? ID = null)
        {
            ListsViewModel Model = new ListsViewModel();

            if (ID != null)
            {
                Model.CurrentLibItem = BusinessLayer_LibsLayer.GetLibByID(LibID: ID.Value);

                Model.CurrentLibItem.propList = BusinessLayer_PropsLayer.GetPropsList(Model.CurrentLibItem.ID);
            }

            Model.PropertyList = BusinessLayer_PropsLayer.GetPropsList();

            return PartialView("LibItem", Model);
        }



        /// <summary>
        /// Get selectd lib item
        /// </summary>
        public ActionResult Get_Order(int? ID = null)
        {
            StructureViewModel Model = new StructureViewModel();

            if (ID != null)
            {
            }

            return PartialView("Order", Model);
        }

        /// <summary>
        /// Get org unit view item
        /// </summary>
        public ActionResult Get_OrgUnitView(int TypeID, int? ID = null, int? ParentID = null, int? SelectedTypeID = null)
        {
            StructureViewModel Model = new StructureViewModel();

            bool isSingleAndHasEmployee = false;

            if (ParentID != null)
            {
                Model.BindPositionList = BusinessLayer_OrgLayer.GetBindPositions(OrgUnitID: ParentID.Value);

                Model.AllPermisionList = BusinessLayer_PermLayer.GetPermissionList(OrgUnitID: ParentID.Value);
            }

            if (ID != null)
            {
                Model.CurrentOrgUnit = BusinessLayer_OrgLayer.GetOrgUnit(ID: ID.Value);

                Model.CurrentOrgUnit.Permissions = BusinessLayer_PermLayer.GetPermissionList(OrgUnitID: ID.Value);

                Model.OrganizationTypes = BusinessLayer_OrgLayer.GetOrgTypes();

                OrgUnitEntity orgUnits = BusinessLayer_OrgLayer.GetApproveOrgUnit(OrgUnitID: ID.Value);

                Model.AllApproveList = new List<OrgUnitEntity>();

                Model.AllApproveList.Add(orgUnits);
            }
            else
            {
                
                // check if is type is position, it is single and has employee
                if (TypeID == 4 && ParentID != null)
                {
                    OrgUnitEntity current_parent = BusinessLayer_OrgLayer.GetOrgUnit(ID: ParentID);
                    if (current_parent != null)
                    {
                        if (current_parent.Single)
                        {
                            if (BusinessLayer_OrgLayer.GetEmployees(OrgUnitID: ParentID.Value).Count != 0)
                            {
                                isSingleAndHasEmployee = true;
                            }
                        }
                    }
                }

                // TODO add check for miandznya
                Model.OrganizationTypes = BusinessLayer_OrgLayer.GetOrgTypes(TypeID:TypeID);

                if (ParentID != null)
                {
                    OrgUnitEntity orgUnits = BusinessLayer_OrgLayer.GetApproveOrgUnit(OrgUnitID: ParentID.Value);

                    Model.AllApproveList = new List<OrgUnitEntity>();

                    Model.AllApproveList.Add(orgUnits);
                }
            }

            if (TypeID == 4 && !isSingleAndHasEmployee)
            {
                Model.OrganizationTypes.Add(new OrgTypeEntity(ID: 1001, Name: "Աշխատակից"));
            }

            Model.SelectedTypeID = SelectedTypeID;

            return PartialView("OrgUnitView", Model);
        }


        /// <summary>
        /// draw org unit
        /// </summary>
        public ActionResult Draw_OrgUnit(OrgUnitEntity Data)
        {
            StructureViewModel Model = new StructureViewModel();

            Model.CurrentOrgUnit = Data;

            return PartialView("OrgUnit", Model);
        }

        /// <summary>
        /// draw position
        /// </summary>
        public ActionResult Draw_OrgUnitPosition(OrgUnitEntity Data)
        {
            StructureViewModel Model = new StructureViewModel();

            Model.VisibleOrgUnitList = BusinessLayer_OrgLayer.GetOrgUnitByTypeID(3);

            Model.CurrentOrgUnit = Data;

            return PartialView("OrgUnitPosition", Model);
        }


        /// <summary>
        /// Get employee view
        /// </summary>
        public ActionResult Get_EmployeeView(int? ID = null, int? ParentID = null)
        {
            StructureViewModel Model = new StructureViewModel();

            if (ID != null)
            {
                Model.CurrentEmployee = BusinessLayer_OrgLayer.GetEmployee(EmployeeID: ID.Value);

                if (Model.CurrentEmployee != null) Model.CurrentEmployee.PhotoLink = BL_Files.getInstance().GetLink(Model.CurrentEmployee.Photo);
            }

            Model.PhotoLinkDefault = BL_Files.getInstance().getDefaultPhotoLink();

            Model.OrganizationTypes = BusinessLayer_OrgLayer.GetOrgTypes(4);
            Model.OrganizationTypes.Add(new OrgTypeEntity(ID: 1001, Name: "Աշխատակից"));

            return PartialView("EmployeeView", Model);
        }


        /// <summary>
        /// Get Structure Tree
        /// </summary>
        public ActionResult Get_StructureTree()
        {
            StructureViewModel Model = new StructureViewModel();

            OrgUnitEntity unit = BusinessLayer_OrgLayer.GetOrgUnit(null, 0);

            Model.RootOrganizationUnit = unit;

            return PartialView("StructureTree", Model);
        }


        /// <summary>
        /// Get Employee List
        /// </summary>
        public ActionResult GetEmployees(int OrgUnitID)
        {
            StructureViewModel Model = new StructureViewModel();

            List<EmployeeEntity> employees = BusinessLayer_OrgLayer.GetEmployees(OrgUnitID);

            Model.Employees = employees;

            return PartialView("Employees", Model);
        }


        /// <summary>
        /// Get structure position tree
        /// </summary>
        public ActionResult GetStructurePositionTree()
        {
            StructureViewModel Model = new StructureViewModel();

            List<OrgUnitEntity> unit = BusinessLayer_OrgLayer.GetOrgUnitTree(null, 0);

            Model.OrganizationUnits = unit;

            return PartialView("StructurePositionTree", Model);
        }

        /// <summary>
        /// Get Path Way
        /// </summary>
        private List<string> GetPathWay(int ID)
        {
            List<string> PathWay = new List<string>();
            List<LibPathEntity> list = BusinessLayer_LibsLayer.GetLibPathWay(ParentID: ID);

            foreach (var item in list)
            {
                PathWay.Add(item.Name);
            }

            return PathWay;
        }

        /// <summary>
        /// Get selectd lib item
        /// </summary>
        public ActionResult Get_YesOrNO(int type)
        {
            YesOrNoViewModel Model = new YesOrNoViewModel(type);

            return PartialView("YesOrNo", Model);
        }

        public ActionResult Get_FindPerson()
        {
            StructureViewModel Model = new StructureViewModel();

            Model.Gender = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);
            for (int i = 0; i < Model.Gender.Count; i++)
            {
                // calling getting lib path method from business layer
                Model.Gender[i].propList = BusinessLayer_PropsLayer.GetPropsList(Model.Gender[i].ID);
            }
            Model.Nationality = BusinessLayer_LibsLayer.GetNationality();
            Model.Citizenships = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);
            Model.DocIdentity = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DOCIDENTITY);

            return PartialView("FindPerson", Model);
        }

        public ActionResult Draw_PersonFullData(PersonEntity person)
        {
            StructureViewModel Model = new StructureViewModel();

            Model.Person = person;

            if (Model.Person.PhotoLink == null)
            {
                Model.Person.PhotoLink = BL_Files.getInstance().getDefaultPhotoLink();
            }

            Model.Gender = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.GERNER);
            for (int i = 0; i < Model.Gender.Count; i++)
            {
                // calling getting lib path method from business layer
                Model.Gender[i].propList = BusinessLayer_PropsLayer.GetPropsList(Model.Gender[i].ID);
            }

            Model.Nationality = BusinessLayer_LibsLayer.GetNationality();

            Model.Citizenships = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.CITIZENSHIP);

            Model.DocIdentity = BusinessLayer_LibsLayer.GetLibsByLibPathID((int)LibsEntity.LIBS.DOCIDENTITY);

            return PartialView("PersonFullData", Model);
        }
    }
}
