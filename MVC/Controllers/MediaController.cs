﻿using System.Web.Mvc;
using CommonLayer.BusinessEntities;
using BusinessLayer.BusinessServices;
using System;

namespace MVC___Internal_System_Administration.Controllers
{ 
    public class MediaController : BaseController
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if ((Session["SystemUser"] as BESystemUser) == null)
            {
                if (Request.IsAjaxRequest())
                    filterContext.Result = new HttpUnauthorizedResult();
                else
                    filterContext.Result = RedirectToAction("Login", "Login");
                return;
            }
        }

        public ActionResult GetImage(int? id)
        {
            Tuple<Byte[], string> imageData = BL_Files.getInstance().GetEmployeeFile(id);
            if (imageData.Item2 == null || imageData.Item1 == null) return null;
            return File(imageData.Item1, imageData.Item2);
        }
        
    }
}