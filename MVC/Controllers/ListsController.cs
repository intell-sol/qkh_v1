﻿using BusinessLayer.BusinessServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC___Internal_System_Administration.Models;
using CommonLayer.BusinessEntities;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace MVC___Internal_System_Administration.Controllers
{
    public class ListsController : BaseController
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if ((Session["SystemUser"] as BESystemUser) == null)
            {
                if (Request.IsAjaxRequest())
                    filterContext.Result = new HttpUnauthorizedResult();
                else
                    filterContext.Result = RedirectToAction("Login", "Login");
                return;
            }
        }

        /// <summary>
        /// Main function for working with classifiers.
        /// </summary>
        public ActionResult Index()
        {
            try
            {
                // Page title
                ViewBag.Title = "Ցանկեր";

                // for appending js files in layout
                ViewBag.FileNames.Add("initLists.js");

                // getting libpaths
                ListsViewModel model = new ListsViewModel();
                model.LibsList = BusinessLayer_LibsLayer.GetLibPath();

                return View(model);
            }
            catch (Exception e)
            {
                //log error
                Console.WriteLine(e);

                return View("Error");
            }
        }

        /// <summary>
        /// Getting classifier list as json
        /// </summary>
        [HttpPost]
        public ActionResult GetList(int ParentID = 0, string Name = null, int ThisParentId = 0)
        {
            try
            {
                // calling getting lib path method from business layer
                LibPathEntity currentLib = BusinessLayer_LibsLayer.GetLibPath(ParentID, Name, ThisParentId);

                return Json(currentLib);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return null;
            }
        }

        /// <summary>
        /// Adding classifiers method
        /// </summary>
        [HttpPost]
        public ActionResult Register(DashboardViewModel model)
        {
            try
            {
                string Name = model.Name;
                int ParentID = model.ParentID;

                BusinessLayer_LibsLayer.AddLibPath(Name, ParentID);

                return RedirectToAction("Dashboard", "Dashboard");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return View("Error");
            }
        }

        /// <summary>
        /// Getting lib list by path id
        /// </summary>
        [HttpPost]
        public ActionResult GetLibList(int LibPathID, int? ParentID = null)
        {
            try
            {
                List<LibsEntity> list = new List<LibsEntity>();
                if (ParentID == null)
                {
                    // calling getting lib path method from business layer
                    list = BusinessLayer_LibsLayer.GetLibsByLibPathID(LibPathID);
                }
                else
                {
                    list = BusinessLayer_LibsLayer.GetLibsByLibParentID(ParentID);
                }

                return Json(list);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return null;
            }
        }

        /// <summary>
        /// Getting lib list by path id
        /// </summary>
        [HttpPost]
        public ActionResult CheckLibBind(int LibPathID)
        {
            try
            {
                    // calling getting lib path method from business layer
                bool exist = BusinessLayer_LibsLayer.CheckLibBind(LibPathID);

                return Json(exist);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return null;
            }
        }

        /// <summary>
        /// get lib count by libpathid
        /// </summary>
        //public ActionResult GetLibCountByPathID (string idList = null)
        //{
        //    try
        //    {
        //        // calling getting lib path method from business layer
        //        Dictionary<int, Dictionary<int, int>> libPaths = BusinessLayer_LibsLayer.GetLibCountByLibPathID(idList);

        //        string json = JsonConvert.SerializeObject(libPaths, Formatting.None);

        //        return Json(json);
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);

        //        return null;
        //    }
        //}

        /// <summary>
        /// add Libs by Lib Path id
        /// </summary>
        [HttpPost]
        public ActionResult AddLib(List<PropLinkEntity> propList, string Name = null, int? LibPathID = null, int ParentID = 0, int? LibToLibID = null)
        {
            bool status = true;
            try
            {
                int? LibID = null;
                // calling getting lib path method from business layer
                if (LibPathID != null && ParentID == 0)
                {
                    LibID = BusinessLayer_LibsLayer.AddLibsByLibPathID(Name, LibPathID, ParentID);
                }
                else
                {
                    LibID = BusinessLayer_LibsLayer.AddLibsByLibParentID(LibToLibID, Name);
                }

                if (LibID == null) status = false;

                if (propList != null && propList.Any())
                {
                    foreach (PropLinkEntity curProp in propList)
                    {
                        PropsEntity curPropsEntity = curProp.property;
                        PropValuesEntity curPropValueEntity = curProp.value;

                        if (curPropsEntity.Fixed != null && curPropsEntity.Fixed.Value)
                        {
                            int? NewID = BusinessLayer_PropsLayer.AddPropsToList(LibID, curPropsEntity.ID, curPropValueEntity.ID);
                            if (NewID == null) status = false;
                        }
                        else
                        {
                            int? CustomID = BusinessLayer_PropsLayer.AddPropValue(curPropValueEntity.Value, false, curPropsEntity.ID);
                            if (CustomID != null)
                            {
                                int? NewID = BusinessLayer_PropsLayer.AddPropsToList(LibID, curPropsEntity.ID, CustomID);
                                if (NewID == null) status = false;
                            }
                        }
                    }
                }

                return Json(new { status = status});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return Json(new { status = status });
            }
        }

        /// <summary>
        /// Edit Lib by Lib id
        /// </summary>
        [HttpPost]
        public ActionResult EditLib(List<PropLinkEntity> propList, int LibID, string Name = null)
        {
            bool status = true;
            try
            {
                // calling getting lib path method from business layer
                status = BusinessLayer_LibsLayer.EditLibByID(LibID, Name);

                if (propList == null)
                {
                    propList = new List<PropLinkEntity>();
                }
                List<PropsEntity> oldList = BusinessLayer_PropsLayer.GetPropsList(LibID);

                // update Height Weight
                foreach (var curEntity in oldList)
                {
                    if (!propList.FindAll(r => r.property.ID == curEntity.ID && r.property.NewItem == curEntity.NewItem).Any())
                    {
                        bool tempStatus = BusinessLayer_PropsLayer.UpdateProopertyStatus(LibID: LibID, PropID: curEntity.ID.Value, Status: false);
                        if (!tempStatus) status = false;
                    }
                }
                foreach (var curEntity in propList)
                {
                    List<PropsEntity> foundList = oldList.FindAll(r => r.ID == curEntity.property.ID);
                    bool found = foundList.Any();
                    
                    if (!oldList.FindAll(r => r.ID == curEntity.property.ID && r.NewItem == curEntity.property.NewItem).Any())
                    {
                        PropsEntity curPropsEntity = curEntity.property;
                        PropValuesEntity curPropValueEntity = curEntity.value;

                        if (curPropsEntity.Fixed != null && curPropsEntity.Fixed.Value)
                        {
                            int? NewID = BusinessLayer_PropsLayer.AddPropsToList(LibID, curPropsEntity.ID, curPropValueEntity.ID);
                            if (NewID == null) status = false;
                        }
                        else
                        {
                            int? CustomID = BusinessLayer_PropsLayer.AddPropValue(curPropValueEntity.Value, false, curPropsEntity.ID);
                            if (CustomID != null)
                            {
                                int? NewID = BusinessLayer_PropsLayer.AddPropsToList(LibID, curPropsEntity.ID, CustomID);
                                if (NewID == null) status = false;
                            }
                        }
                    }
                }
                return Json(new { status = status});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return Json(new { status = status});
            }
        }

        /// <summary>
        /// Delete Lib by ID
        /// </summary>
        [HttpPost]
        public ActionResult DelLib(int LibID)
        {
            bool status = true;
            try
            {
                // delete lib by id
                status = BusinessLayer_LibsLayer.DelLibByID(LibID);

                return Json(new { status = status});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return Json(new { status = status});
            }
        }

        /// <summary>
        /// get Property List
        /// </summary>
        [HttpPost]
        public ActionResult GetPropertyList(int? ListID = null)
        {
            try
            {
                // calling getting lib path method from business layer
                List<PropsEntity> PropsEntity = BusinessLayer_PropsLayer.GetPropsList(ListID);

                string json = JsonConvert.SerializeObject(PropsEntity, Formatting.None);

                return Json(json);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return null;
            }
        }

        /// <summary>
        /// get Property List
        /// </summary>
        [HttpPost]
        public ActionResult GetCurrentLib(int LibID)
        {
            try
            {
                // calling getting lib path method from business layer
                LibsEntity curLibEntity = BusinessLayer_LibsLayer.GetLibByID(LibID);

                curLibEntity.propList = BusinessLayer_PropsLayer.GetPropsList(curLibEntity.ID);

                string json = JsonConvert.SerializeObject(curLibEntity, Formatting.None);

                return Json(json);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return null;
            }
        }
    }
}
