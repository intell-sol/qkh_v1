﻿using BusinessLayer.BusinessServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC___Internal_System_Administration.Models;
using CommonLayer.BusinessEntities;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MVC___Internal_System_Administration.Controllers
{
    /// <summary>
    /// Base controller for another controllers
    /// </summary>
    public class BaseController : Controller
    {
        public BL_SystemUsersAuthentication BusinesLayer_SystemUsersAuthentication = new BL_SystemUsersAuthentication();
        public BL_LibsLayer BusinessLayer_LibsLayer = new BL_LibsLayer();
        public BL_OrganizatonLayer BusinessLayer_OrgLayer = new BL_OrganizatonLayer();
        public BL_PermLayer BusinessLayer_PermLayer = new BL_PermLayer();
        public BL_PoliceService BusinessLayer_PoliceService = new BL_PoliceService();
        public BL_PropsLayer BusinessLayer_PropsLayer = new BL_PropsLayer();
        public BL_OrderLayer BusinessLayer_Order = new BL_OrderLayer();
        //public static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public BaseController()
        {
            ViewBag.LoggedIn = false;
            ViewBag.FirstName = "Unauthorized User";
            ViewBag.FileNames = new List<string>();

            // setting system user parameters
            if ((System.Web.HttpContext.Current.Session["SystemUser"] as BESystemUser) != null)
            {
                BESystemUser SystemUser = (BESystemUser) System.Web.HttpContext.Current.Session["SystemUser"];
                ViewBag.LoggedIn = true;
                ViewBag.FirstName = SystemUser.FirstName;

                // for appending js files in layout
                ViewBag.FileNames.Add("initLayout.js");
            }
        }
        public ActionResult Error()
        {
            return View();
        }

        new public ActionResult Json(object Object)
        {
            string json = "";

            json = JsonConvert.SerializeObject(Object, Formatting.None,new IsoDateTimeConverter() { DateTimeFormat = System.Globalization.CultureInfo.DefaultThreadCurrentCulture.DateTimeFormat.ShortDatePattern });

            return Content(json, "application/json");
        }
    }
}