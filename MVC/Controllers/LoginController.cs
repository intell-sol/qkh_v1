﻿using MVC___Internal_System_Administration.Models;
using CommonLayer.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MVC___Internal_System_Administration.Controllers
{
    public class LoginController : BaseController
    {

        /// <summary>
        /// Main function for working with login page.
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            try
            {
                // check if user is logged in or not
                if ((Session["SystemUser"] as BESystemUser) != null)
                {
                    // dont go to login page, redirect to lists
                    return RedirectToAction("Index", "Lists");
                }
                else
                {
                    // page title
                    ViewBag.Title = "Login";

                    return View();
                }
            }
            catch (Exception e)
            {
                //ErrorLog.ErrorLog(e);

                return View("Error", e);
            }
        }

        /// <summary>
        /// Login action method
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogIn(LoginViewModel model)
        {
            try
            {
                string strUserName = model.UserName;
                string strPassword = model.Password;

                // calling user login method from business layer
                BESystemUser SysUser = BusinesLayer_SystemUsersAuthentication.SystemUserLogIn(strUserName, strPassword);
                if (SysUser != null)
                {
                    // store user in session
                    Session["SystemUser"] = SysUser;

                    // redirect to lists
                    return RedirectToAction("Index", "Lists");
                }
                else
                {
                    // wrong login
                    ViewBag.ErrorMessage = "Մուտքագրեք ճիշտ տվյալներ";

                    return View();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return View("Error");
            }
        }

        /// <summary>
        /// LogOut action method
        /// </summary>
        public ActionResult Logout()
        {
            try
            {
                // making login session null
                Session["SystemUser"] = null;

                return RedirectToAction("Login", "Login");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                // if Session key changed
                System.Web.HttpContext.Current.Session["SystemUser"] = null;

                return RedirectToAction("Login", "Login");
            }
        }
    }
}