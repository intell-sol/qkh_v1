﻿using MVC___Internal_System_Administration.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using CommonLayer.BusinessEntities;
using BusinessLayer.BusinessServices;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Converters;
using System.IO;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters.Binary;


namespace MVC___Internal_System_Administration.Controllers
{ 
    public class StructureController : BaseController
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if ((Session["SystemUser"] as BESystemUser) == null)
            {
                if (Request.IsAjaxRequest())
                    filterContext.Result = new HttpUnauthorizedResult();
                else
                    filterContext.Result = RedirectToAction("Login", "Login");
                return;
            }
        }

        [HttpGet]
        public ActionResult Index()
        {
            StructureViewModel model = new StructureViewModel();
            
            OrgUnitEntity unit = BusinessLayer_OrgLayer.GetOrgUnit(null, 0);
            List<OrgTypeEntity> orgTypes = BusinessLayer_OrgLayer.GetOrgTypes();

            model.RootOrganizationUnit = unit;
            model.OrganizationTypes = orgTypes;

            ViewBag.Title = "Կառուցվածք";

            // binding init js file
            ViewBag.FileNames.Add("initStructure.js");

            return View(model);
        }
        
        [HttpPost]
        public ActionResult GetOrgUnits()
        {
            OrgUnitEntity unit = BusinessLayer_OrgLayer.GetOrgUnit(null, 0);

            return Json(unit);
        }

        //[HttpPost]
        //public ActionResult AddOrgUnit(int ParentID = 0, int TypeID = 1, bool Single = true, string Label = null, string Address = null)
        //{
        //    bool result = BusinessLayer_OrgLayer.AddOrgUnit(ParentID, TypeID, Single, Label, Address);
        //    return Json(new { status = result? "success": "error" });
        //}

        [HttpPost]
        public ActionResult AddOrgUnit(OrgUnitEntity entity)
        {
            ////List<PermToPosEntity> Permissions = JsonConvert.DeserializeObject<List<PermToPosEntity>>(entity.Permissions);
            //List<PermToPosEntity> Permissions = new List<PermToPosEntity>();
            int OrgUnitID = BusinessLayer_OrgLayer.AddOrgUnit(entity.ParentID, entity.TypeID, entity.Single, entity.Label, entity.Address, entity.Phone, entity.Fax, entity.Email, entity.StateID, entity.BindPositionID);

            if (OrgUnitID > 0)
            {
                List<int> VisibleOrgUnits = entity.VisibleOrgUnits; // Բոլոր տեսանելի ՔԿՀ-ները
                foreach(int id in VisibleOrgUnits)
                {
                    BusinessLayer_OrgLayer.AddPositionToOrg(OrgUnitID, id);
                }

                BusinessLayer_PermLayer.JoinPermissionsToPosition(entity.Permissions ?? new List<PermEntity>(), OrgUnitID, entity.TypeID);
            }   

            return Json(new { status = OrgUnitID > 0, id = OrgUnitID });
        }

        [HttpPost]
        public ActionResult CheckUserName(string Login, int? UserID = null)
        {
            Boolean exist = BusinessLayer_OrgLayer.CheckEmployeeUserName(Login, UserID);

            return Json(new { status = !exist  });
        }
        
        [HttpPost]
        public ActionResult UpdateOrgUnit(OrgUnitEntity entity)
        {
            //List<PermToPosEntity> Permissions = new List<PermToPosEntity>();
            //List<PermToPosEntity> Permissions = JsonConvert.DeserializeObject<List<PermToPosEntity>>(entity.Permissions);
            bool result = BusinessLayer_OrgLayer.UpdateOrgUnit(ID: entity.ID, ParentID: entity.ParentID, TypeID: entity.TypeID, Single: entity.Single, Label: entity.Label, Address: entity.Address, Phone: entity.Phone, Fax: entity.Fax, Email: entity.Email, Status: true, StateID: entity.StateID, BindPositionID: entity.BindPositionID);

            if (result)
            {
                BusinessLayer_OrgLayer.UpdatePositionToOrg(null, entity.ID, false);
                List<int> VisibleOrgUnits = entity.VisibleOrgUnits; // Բոլոր տեսանելի ՔԿՀ-ները
                foreach (int id in VisibleOrgUnits)
                {
                    BusinessLayer_OrgLayer.AddPositionToOrg(entity.ID, id);
                }

                BusinessLayer_PermLayer.UpdatePermission(entity);
                //BusinessLayer_PermLayer.UpdatePermissionToPosition(ID: null, OrgUnitID: entity.ID, Status: false);
                //BusinessLayer_PermLayer.JoinPermissionsToPosition(entity.Permissions ?? new List<PermEntity>(), entity.ID, entity.TypeID);
            }
            return Json(new { status = entity.ID > 0, id = entity.ID });
        }

        [HttpPost]
        public ActionResult DeleteOrgUnit(int ID)
        {
            bool result = BusinessLayer_OrgLayer.DeleteOrgUnit(ID);

            return Json(new { status = result });
        }

        [HttpPost]
        public ActionResult AddEmployeeByDocNumber(int OrgUnitID, String DocNumber, bool Type, String OrderNumber, String OrderBy, String OrderDate, String Phone, String Login, String Password, String Fax, String Email)
        {
            AVVPerson person = null;
            int? EmployeeID = null;
            int? fileID = null;
            int result = 0;

            if (Type)
                person = BusinessLayer_PoliceService.getBYDocumentNumber(DocNumber);
            else
            {
                person = BusinessLayer_PoliceService.getBYPersonalNumber(DocNumber);
                if (person.PNum == null) person.PNum = DocNumber;
            }
            

            // convert orderDate to datetime type
            //DateTime orderDt = DateTime.Now;
            //if(orderDate != null)
            //    orderDt = Convert.ToDateTime(orderDate);
            DateTime orderDt = DateTime.Now;
            try
            {
                orderDt = DateTime.ParseExact(OrderDate, "dd/MM/yyyy", null);
            }
            catch
            {
                orderDt = DateTime.Now;
            }
            

            if (person != null)
            {
                // adding employee
                EmployeeID = AddEmployeePrivate(person, OrgUnitID, Phone, Login, Password, Fax, Email);
                if (EmployeeID > 0) result = 1;

                // adding file
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    AdditionalFileEntity fileEntry = new AdditionalFileEntity(file.ContentLength);

                    fileEntry.FileName = Path.GetFileName(file.FileName);

                    fileEntry.PrisonerID = EmployeeID;
                    fileEntry.FileExtension = Path.GetExtension(file.FileName);
                    fileEntry.ContentType = file.ContentType;
                    fileEntry.isPrisoner = false;
                    fileEntry.PNum = person.PNum;
                    file.InputStream.Read(fileEntry.FileContent, 0, fileEntry.FileContent.Length);

                    fileID = BL_Files.getInstance().AddEmployeeFile(fileEntry);
                }

                if (fileID != null) BusinessLayer_Order.AddOrder(EmployeeID.Value, 2, OrderNumber, OrderBy, orderDt, fileID.Value);
            }

            return Json(new { status = result == 1, id = EmployeeID });
        }

        [HttpPost]
        public ActionResult AddEmployee(int OrgUnitID, String FirstName, String LastName, String BirthDate, String orderNum, String orderBy, String orderDate, String Phone, String Login, String Password, String Fax, String Email)
        {
            AVVPerson person = BusinessLayer_PoliceService.getBYDocumentPersonalData(LastName.ToUpper(), FirstName.ToUpper(), BirthDate);
            int? fileID = null;


            // convert orderDate to datetime type
            DateTime orderDt = DateTime.Now;
            if (orderDate != null)
                orderDt = Convert.ToDateTime(orderDate);

            int result = 0;
            if(person.PNum != null)
            {
                int EmployeeID = AddEmployeePrivate(person, OrgUnitID, Phone, Login, Password, Fax, Email);
                if (EmployeeID > 0) result = 1;
                // adding file
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    AdditionalFileEntity fileEntry = new AdditionalFileEntity(file.ContentLength);

                    fileEntry.PrisonerID = EmployeeID;
                    fileEntry.FileName = Path.GetFileName(file.FileName);
                    fileEntry.FileExtension = Path.GetExtension(file.FileName);
                    fileEntry.ContentType = file.ContentType;
                    fileEntry.isPrisoner = false;
                    fileEntry.PNum = person.PNum;
                    file.InputStream.Read(fileEntry.FileContent, 0, fileEntry.FileContent.Length);

                    fileID = BL_Files.getInstance().AddEmployeeFile(fileEntry);
                }
                // add order
                if (fileID != null) BusinessLayer_Order.AddOrder(EmployeeID, 2, orderNum, orderBy, orderDt, fileID.Value);
            }

            return Json(new { status = result > 0 ? "success" : "error" });
        }

        [HttpPost]
        public ActionResult UpdateEmployee(int ID, String Phone, String Login, String Password, String Fax, String Email, bool UpdatePolice = false) 
        {
            bool result = false;
            if (UpdatePolice)
            {
                //AVVPerson person = null;

                //if (Type)
                //    person = BusinessLayer_PoliceService.getBYDocumentNumber(DocNumber);
                //else
                //    person = BusinessLayer_PoliceService.getBYPersonalNumber(DocNumber);

                //if (person.PNum != null)
                //{
                //    Document idCard = person.AVVDocuments.Document.Find(d => d.Document_Type == Document_Type.ID_CARD);
                //    Document biometricPassport = person.AVVDocuments.Document.Find(d => d.Document_Type == Document_Type.BIOMETRIC_PASSPORT && d.Document_Status == Document_Status.ACTIVE);
                //    Document nonBiometricPassport = person.AVVDocuments.Document.Find(d => d.Document_Type == Document_Type.NON_BIOMETRIC_PASSPORT && d.Document_Status == Document_Status.ACTIVE);

                //    Document document = idCard == null ? nonBiometricPassport : idCard;

                //    String firstName = document.Person.First_Name;
                //    String middleName = document.Person.Patronymic_Name;
                //    String lastName = document.Person.Last_Name;


                //    AVVAddress PermanentAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.P);
                //    AVVAddress ActiveAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.A);
                //    AVVAddress TemporaryAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.T);

                //    String Nationality = document.Person.NationalityLabel;
                //    String PassportDepartment = document.Document_Department;
                //    String PassportIssuanceDate = document.PassportData.Passport_Issuance_Date;

                //    RegistrationAddress liv = ActiveAddress != null ? ActiveAddress.RegistrationAddress : PermanentAddress.RegistrationAddress;
                //    RegistrationAddress reg = PermanentAddress.RegistrationAddress;


                //    String LivingAddress = liv.Registration_Region + ", " + liv.Registration_Community + " " + (liv.Registration_Apartment != null ? liv.Registration_Apartment : "") + ", " + liv.Registration_Street + " " + liv.Registration_Building_Type + " " + liv.Registration_Building;

                //    String RegistrationAddress = reg.Registration_Region + ", " + reg.Registration_Community + " " + (reg.Registration_Apartment != null ? reg.Registration_Apartment : "") + ", " + reg.Registration_Street + " " + reg.Registration_Building_Type + " " + reg.Registration_Building;

                //    DateTime bDay = DateTime.ParseExact(document.Person.Birth_Date, "dd/MM/yyyy", null);
                //    DateTime PassportDate = DateTime.ParseExact(PassportIssuanceDate, "dd/MM/yyyy", null);

                //    int? fileID = null;

                //    if (person.Photo_ID != null)
                //    {

                //        String PhotoLink = person.Photo_ID;

                //        Byte[] bytes = Convert.FromBase64String(PhotoLink);

                //        AdditionalFileEntity fileEntry = new AdditionalFileEntity(bytes.Length);

                //        fileEntry.FileName = person.PNum.ToString() + ".jpg";
                //        fileEntry.FileExtension = ".jpg";
                //        fileEntry.ContentType = "image/jpeg";
                //        fileEntry.isPrisoner = false;
                //        fileEntry.PrisonerID = EmployeeID;
                //        fileEntry.PNum = person.PNum;
                //        fileEntry.FileContent = bytes;

                //        fileID = BL_Files.getInstance().AddEmployeeFile(fileEntry);
                //    }

                //    result = BusinessLayer_OrgLayer.UpdateEmployee(EmployeeID, firstName, middleName, lastName, bDay, document.Document_Number, Login, Password, Phone, Fax, Email, RegistrationAddress, LivingAddress, Nationality, PassportDate, PassportDepartment, fileID);
                //}
            }
            else
            {
                result = BusinessLayer_OrgLayer.UpdateEmployee(ID, null, null, null, null, null, Login, Password, Phone, Fax, Email, null, null, null, null, null, null);
            }

            //bool result = BusinessLayer_OrgLayer.UpdateEmployee(EmployeeID, OrgUnitID);
            result = true;
            return Json(new { status = result, id = ID });
        }

        [HttpPost]
        public ActionResult DeleteEmployee(int EmployeeID, String OrderNumber, String OrderBy, String OrderDate)
        {
            int? fileID = null;

            // adding file
            if (Request.Files.Count > 0)
            {
                EmployeeEntity entity = BusinessLayer_OrgLayer.GetEmployee(EmployeeID);

                HttpPostedFileBase file = Request.Files[0];
                AdditionalFileEntity fileEntry = new AdditionalFileEntity(file.ContentLength);

                fileEntry.FileName = Path.GetFileName(file.FileName);
                fileEntry.FileExtension = Path.GetExtension(file.FileName);
                fileEntry.ContentType = file.ContentType;
                fileEntry.isPrisoner = false;
                fileEntry.PNum = entity.PSN;
                fileEntry.PrisonerID = EmployeeID;
                file.InputStream.Read(fileEntry.FileContent, 0, fileEntry.FileContent.Length);

                fileID = BL_Files.getInstance().AddEmployeeFile(fileEntry);
            }

            // convert orderDate to datetime type
            DateTime orderDt = DateTime.Now;
            try
            {
                if (OrderDate != null)
                    orderDt = Convert.ToDateTime(OrderDate);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                orderDt = DateTime.Now;
            }

            bool result = false;
            if (fileID != null) {
                BusinessLayer_Order.AddOrder(EmployeeID, 1, OrderNumber, OrderBy, orderDt, fileID.Value);
                result = BusinessLayer_OrgLayer.DeleteEmployee(EmployeeID);
            }

            return Json(new { status = result });
        }

        [HttpPost]
        public ContentResult GetEmployee(int EmployeeID)
        {
            EmployeeEntity entity = BusinessLayer_OrgLayer.GetEmployee(EmployeeID);

            entity.PhotoLink = BL_Files.getInstance().GetLink(entity.Photo);

            if (entity == null)
                return Content(JsonConvert.SerializeObject(new { status = "error" }, Formatting.None), "application/json");
            else
                return Content(JsonConvert.SerializeObject(entity, Formatting.None), "application/json");
        }

        [HttpPost]
        public ContentResult GetEmployees(int OrgUnitID)
        {
            List<EmployeeEntity> employees = BusinessLayer_OrgLayer.GetEmployees(OrgUnitID);
            string json = JsonConvert.SerializeObject(employees, Formatting.None);
            return Content(json, "application/json");
        }

        [HttpPost]
        public ContentResult GetBindPositions(int OrgUnitID)
        {
            List<OrgUnitEntity> employees = BusinessLayer_OrgLayer.GetBindPositions(OrgUnitID);
            //List<EmployeeEntity> employees = BusinessLayer_OrgLayer.GetEmployeesForOrgUnit(OrgUnitID);
            string json = JsonConvert.SerializeObject(employees, Formatting.None);
            return Content(json, "application/json");
        }

        [HttpPost]
        public ActionResult GetStates()
        {
            List<Tuple<int, String>> states = BusinessLayer_OrgLayer.GetStates();

            return Json(states);
        }

        [HttpPost]
        public ActionResult GetOrgUnit(int ID)
        {
            OrgUnitEntity unit = BusinessLayer_OrgLayer.GetOrgUnit(ID, null);

            return Json(unit);
        }

        [HttpPost]
        public ActionResult GetAvailableOrgUnits()
        {
            OrgUnitEntity unit = BusinessLayer_OrgLayer.GetAvailableOrgUnits();

            return Json(unit);
        }

        [HttpPost]
        public ActionResult GetAvailablePositions(int OrgUnitID)
        {
            return Json(BusinessLayer_OrgLayer.GetAvailablePositions(OrgUnitID));
        }

        [HttpPost]
        public ActionResult GetOrgUnitByTypeID(int TypeID)
        {
            List<OrgUnitEntity> orgUnitList = BusinessLayer_OrgLayer.GetOrgUnitByTypeID(TypeID);

            return Json(orgUnitList);
        }

        [HttpPost]
        public ActionResult GetParentOrgUnits(int ID)
        {
            OrgUnitEntity unit = BusinessLayer_OrgLayer.GetOrgUnit(ID);

            return Json(unit);
        }

        [HttpPost]
        public ActionResult GetOrgTypes(int? TypeID = null)
        {
            List<OrgTypeEntity> orgTypes = BusinessLayer_OrgLayer.GetOrgTypes(TypeID);

            string json = JsonConvert.SerializeObject(orgTypes, Formatting.None);

            return Json(json);
        }

        [HttpPost]
        public ActionResult GetPermissionsByParentID(int ParentID)
        {
            List<PermEntity> EntityList = BusinessLayer_PermLayer.GetPermissionByParentID(ParentID);

            return Json(EntityList);
        }

        [HttpPost]
        public ActionResult GetPermissionList(int OrgUnitID)
        {
            List<PermEntity> permissions = BusinessLayer_PermLayer.GetPermissionList(OrgUnitID);

            string json = JsonConvert.SerializeObject(permissions, Formatting.None);
             
            return Json(permissions);
        }

        [HttpPost]
        public ActionResult GetApproveOrgUnits(int OrgUnitID)
        {
            OrgUnitEntity orgUnits = BusinessLayer_OrgLayer.GetApproveOrgUnit(OrgUnitID);

            string json = JsonConvert.SerializeObject(orgUnits, Formatting.None);

            return Json(orgUnits);
        }

        public ActionResult Get_StructureTree()
        {
            StructureViewModel Model = new StructureViewModel();

            List<OrgUnitEntity> unit = BusinessLayer_OrgLayer.GetOrgUnitTree(null, 0);

            Model.OrganizationUnits = unit;

            return PartialView("StructureTree", Model);
        }

        public ActionResult UpdateStructureTreeLevel(List<OrgUnitEntity> list)
        {
            if (list != null && list.Any())
            {
                foreach (var item in list)
                {
                    bool status = BusinessLayer_OrgLayer.UpdateOrgUnitLevel(item);
                }
            }

            return Json(new { status = true });
        }

        //public ActionResult RemoveEmployeeOrder()
        //{

        //}
        /// <summary>
        /// Add Person to Employee's with given Organization Unit ID
        /// </summary>
        /// <param name="person"></param>
        /// <returns> 
        /// Return 1 if employee finded and successfully added to database, 
        /// Return 0 if employee not finded, and -1 if error occured
        /// </returns>
        private int AddEmployeePrivate(AVVPerson person, int OrgUnitID, String Phone, String Login, String Password, String Fax, String Email)
        {
            try
            {
                if (person.PNum != null)
                {
                    Document idCard = person.AVVDocuments.Document.Find(d => d.Document_Type == Document_Type.ID_CARD);
                    Document biometricPassport = person.AVVDocuments.Document.Find(d => d.Document_Type == Document_Type.BIOMETRIC_PASSPORT && d.Document_Status == Document_Status.ACTIVE);
                    Document nonBiometricPassport = person.AVVDocuments.Document.Find(d => d.Document_Type == Document_Type.NON_BIOMETRIC_PASSPORT && d.Document_Status == Document_Status.ACTIVE);
                    
                    Document document = idCard == null ? nonBiometricPassport : idCard;
                    
                    String firstName = document.Person.First_Name;
                    String middleName = document.Person.Patronymic_Name;
                    String lastName = document.Person.Last_Name;


                    AVVAddress PermanentAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.P);
                    AVVAddress ActiveAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.A);
                    AVVAddress TemporaryAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.T);

                    String Nationality = document.Person.NationalityLabel;
                    String PassportDepartment = document.Document_Department;
                    String PassportIssuanceDate = document.PassportData.Passport_Issuance_Date;

                    RegistrationAddress liv = ActiveAddress != null ? ActiveAddress.RegistrationAddress : PermanentAddress.RegistrationAddress;
                    RegistrationAddress reg = PermanentAddress.RegistrationAddress;


                    String LivingAddress = liv.Registration_Region + ", " + liv.Registration_Community + " " + (liv.Registration_Apartment != null ? liv.Registration_Apartment : "") + ", " + liv.Registration_Street + " " + liv.Registration_Building_Type + " " + liv.Registration_Building;

                    String RegistrationAddress = reg.Registration_Region + ", " + reg.Registration_Community + " " + (reg.Registration_Apartment != null ? reg.Registration_Apartment : "") + ", " + reg.Registration_Street + " " + reg.Registration_Building_Type + " " + reg.Registration_Building;

                    DateTime bDay = DateTime.ParseExact(document.Person.Birth_Date, "dd/MM/yyyy", null);
                    DateTime passportDate = DateTime.ParseExact(PassportIssuanceDate, "dd/MM/yyyy", null);

                    // Check if Emplloyee is in use;
                    List<EmployeeEntity> employeeList = BusinessLayer_OrgLayer.GetEmployeeByPSN(person.PNum, true);
                    if (employeeList.Count > 0)
                        return 0;

                    int EmployeeID = BusinessLayer_OrgLayer.AddEmployee(OrgUnitID, person.PNum, firstName, middleName, lastName, bDay, document.Document_Number, Login, Password, Phone, Fax, Email, RegistrationAddress, LivingAddress, Nationality, passportDate, PassportDepartment, null);

                    int? fileID = null;

                    if (person.Photo_ID != null)
                    {
                        Byte[] bytes = Convert.FromBase64String(person.Photo_ID);

                        AdditionalFileEntity fileEntry = new AdditionalFileEntity(bytes.Length);

                        fileEntry.FileName = person.PNum.ToString() + ".jpg";
                        fileEntry.FileExtension = ".jpg";
                        fileEntry.ContentType = "image/jpeg";
                        fileEntry.isPrisoner = false;
                        fileEntry.PrisonerID = EmployeeID;
                        fileEntry.PNum = person.PNum;
                        fileEntry.FileContent = bytes;

                        fileID = BL_Files.getInstance().AddEmployeeFile(fileEntry);
                    }

                    BusinessLayer_OrgLayer.UpdateEmployee(EmployeeID, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, fileID);

                    return EmployeeID;
                }
                return 0;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return -1;
            }
        }


        [HttpPost]
        public ActionResult CheckEmployee(String FirstName, String LastName, String BirthDate)
        {
            AVVPerson person = null;
            person = BusinessLayer_PoliceService.getBYDocumentPersonalData(LastName.ToUpper(), FirstName.ToUpper(), BirthDate);

            //return transferPerson(person);
            return transferPerson(person);
        }


        private ActionResult transferPerson(AVVPerson person)
        {

            //if (person != null && person.PNum != null && person.AVVAddresses != null)
            //{
            //    Document NonBioPassport = person.AVVDocuments.Document.Find(d => d.Document_Status == Document_Status.ACTIVE && d.Document_Type == Document_Type.NON_BIOMETRIC_PASSPORT);
            //    Document BioPassport = person.AVVDocuments.Document.Find(d => d.Document_Status == Document_Status.ACTIVE && d.Document_Type == Document_Type.BIOMETRIC_PASSPORT);
            //    Document IDCard = person.AVVDocuments.Document.Find(d => d.Document_Status == Document_Status.ACTIVE && d.Document_Type == Document_Type.ID_CARD);
            //    Document document = BioPassport ?? NonBioPassport ?? IDCard;

            //    document = document ?? person.AVVDocuments.Document.First();

            //    AVVAddress PermanentAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Code == "Ն");
            //    PermanentAddress = PermanentAddress ?? person.AVVAddresses.AVVAddress.First();
            //    AVVAddress ActiveAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.A);
            //    AVVAddress TemporaryAddress = person.AVVAddresses.AVVAddress.Find(a => a.RegistrationData.Registration_Status == Registration_Status.T);

            //    //=======================
            //    // make document list for _Data_Initial_ subcontroller
            //    List<IdentificationDocument> docIdentities = new List<IdentificationDocument>();
            //    foreach (Document tempDoc in person.AVVDocuments.Document)
            //    {
            //        IdentificationDocument currentDocumentIdentity = new IdentificationDocument();

            //        Document_Type docType = tempDoc.Document_Type;
            //        string docTypeName = BusinessLayer_LibsLayer.GetLibByID((int)docType).Name;

            //        LibsEntity citizenshipLib = this.GetCitizenshipLibByCode((string)tempDoc.Person.Citizenship);

            //        currentDocumentIdentity.TypeLibItem_ID = (int)docType;
            //        currentDocumentIdentity.TypeLibItem_Name = docTypeName;
            //        currentDocumentIdentity.CitizenshipLibItem_ID = citizenshipLib.ID;
            //        currentDocumentIdentity.CitizenshipLibItem_Name = citizenshipLib.Name;
            //        currentDocumentIdentity.Number = tempDoc.Document_Number;
            //        currentDocumentIdentity.FromWhom = tempDoc.Document_Department;
            //        currentDocumentIdentity.Type = true;
            //        currentDocumentIdentity.Date = DateTime.ParseExact(tempDoc.PassportData.Passport_Issuance_Date, "dd/MM/yyyy", null);
            //        currentDocumentIdentity.ID = int.Parse(currentDocumentIdentity.TypeLibItem_ID.ToString() + currentDocumentIdentity.CitizenshipLibItem_ID.ToString());

            //        docIdentities.Add(currentDocumentIdentity);
            //    }
            //    //===========================           

            //    // default image
            //    bool photoExistThere = true;
            //    if (person.Photo_ID == null)
            //    {
            //        photoExistThere = false;
            //        person.Photo_ID = "/9j/4AAQSkZJRgABAQAAAQABAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2NjIpLCBxdWFsaXR5ID0gOTAK/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgBLAEsAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A9Dop2B6UYHpQA2inYHpRgelADaKdgelGB6UANop2B6UYHpQA2inYHpRgelADaKdgelGB6UANop2B6UYHpQA2inYHpRgelADaK0dP8PanquPsenXNyp/ijiYr+eMVvW3wp8S3Iz/Z3lL6yTIP0zmgDkKK7tfg14hYcpbL9Zv8BTH+DviRAdtvBJ/uzgfzxQBw9FdNd/DjxJZKTJpE7Y/55ESf+gk1gXNnNZyGO4gkgkH8EilT+RoAgop2B6UYHpQA2inYHpRgelADaKdgelGB6UANop2B6UYHpQA2inYHpRgelADaKdgelGB6UANop2B6UYHpQA2inYHpRgelABz6Uc+lPooAZz6Uc+lPooAZz6Uc+lPooAZz6Uc+lPooAZz6Uc+lPooAZz6Uc+lPooAZz6U6ON5ZFREZ3Y4CqMkn2rS0Hw/e+JNQSzsYjJIeWY8Ki+rHsK928HfD7T/CUSyKBdX5HzXLryPUKP4R+tAHnPhn4NahqqpPqkn9nW55EQGZWH06L+PPtXpmifD3QdBVTBYJLMP+W1wPMfPrzwPwAro6KAEVQowKWiigAooooAKgvLG21CExXVvFcxHqkyBl/I1PRQBwPiD4OaNqis9iW0y4PI2fNGT7qen4EV5X4n8C6t4UYtdwb7bOFuYvmQ/XuD9a+kqZNDHcRtHKiyRsNrK4BBHoQaAPk/n0o59K9Y8d/CXylkv9CQkctJZDnHun/wAT+XpXlRGDg8EdQe1ADOfSjn0p9FADOfSjn0p9FADOfSjn0p9FADOfSjn0p9FADOfSjn0p9FADOfSjn0p9FAC7T6UbT6U6igBu0+lG0+lOooAbtPpRtPpTqKAG7T6UbT6U6igBu0+lG0+lOooAbtPpWhoOg3fiLVIbG0TMkh5Y/dRe7H2FUa99+GvhAeGdEEsyAahdAPKSOUHZPw7+/wBBQBseFvC1n4T01bS0XLHBlmYfNI3qf6DtWxRRQAUUUUAFFFFABRRRQAUUUUAFFFFABXmXxP8Ahwt9HLq+lwgXSgtcQIP9YO7Af3v5/Xr6bRQB8obT6UbT6V3fxV8IDQNWW9tU22N4Sdq9I5OpHsD1H4+lcNQA3afSjafSnUUAN2n0o2n0p1FADdp9KNp9KdRQA3afSjafSnUUAN2n0o2n0p1FADqKdto20ANop22jbQA2inbaNtADaKdto20ANop22jbQB1Hw20Aa94ptxIu63th58nHBx90ficfhmvoGvO/gtpQt9Du79lxJcy7AcfwKP8S35V6JQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAYvjDQl8R+HruywDKyb4j6OOR/h+NfN5BUkMMMDgg9q+qq+d/iBpQ0nxdqMSjEcj+cn0b5v5kj8KAOcop22jbQA2inbaNtADaKdto20ANop22jbQA2inbaNtADttG2l59KOfSgBNtG2l59KOfSgBNtG2l59KOfSgBNtG2l59KOfSgBNtG2l59KOfSgD6H8BWYsfB+lRgYzCJD/AMC+b+tb9VNIi8jSrOLGBHCi/koq3QAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFePfGqzCazYXIGPNgKE+u1s/+zV7DXmfxuh3WOlS45WSRfzA/wAKAPJNtG2l59KOfSgBNtG2l59KOfSgBNtG2l59KOfSgBNtG2l59KOfSgBNtG2l59KOfSgCTafSjafSnUUAN2n0o2n0p1FADdp9KNp9KdRQA3afSjafSnUUAN2n0o2n0p1FAH05bgCCMDkBRg/hUlV9OkEun2zj+KNT+gqxQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFed/Gn/kDaf8A9fB/9BNeiV5x8apQum6Yn96Zm/Jf/r0AeSbT6UbT6U6igBu0+lG0+lOooAbtPpRtPpTqKAG7T6UbT6U6igBu0+lG0+lOooAk2n0o2n0paKAE2n0o2n0paKAE2n0o2n0paKAE2n0o2n0paKAE2n0o2n0paKAPovw62/QNMb+9axH/AMcFaFYfgi4+0+EtKfOcQKn/AHz8v9K3KACiiigAooooAKKKKACiiigAooooAKKKKACiiigArzD41HcNHQdcyk/+Of8A169Pryb4yXAbVdPg/uQF/wDvpsf+y0AeebT6UbT6UtFACbT6UbT6UtFACbT6UbT6UtFACbT6UbT6UtFACbT6UbT6UtFAD6KfRQAyin0UAMop9FADKKfRQAyin0UAex/Ci9+0+FhCT81vM6Y9j83/ALMa7KvKvg/qPk6je2LH/XRiRR7qcH9G/SvVaACiiigAooooAKKKKACiiigAooooAKKKKACiiigArxD4mXn2vxdcqDlYUSIfgMn9Sa9smlWCJ5HO1EBZiewHU187alenUdRurpvvTSM5B7ZOaAKdFPooAZRT6KAGUU+igBlFPooAZRT6KAH7T6UbT6U6igBu0+lG0+lOooAbtPpRtPpTqKAG7T6UbT6U6igBu0+lG0+lOooA0PDmptouuWd5yEjkAfH908N+hNe/qwdQwIIPIIPUV84V7L8ONd/tfQEhds3FpiJvUr/Cfy4/CgDq6KKKACiiigAooooAKKKKACiiigAooooAKKKKAOX+I+r/ANl+GZ41OJro+So9j979Mj8a8V2n0rrviXrY1bXjbxtmC0HljHQt/Ef6fhXJ0AN2n0o2n0p1FADdp9KNp9KdRQA3afSjafSnUUAN2n0o2n0p1FADdp9KNp9KdRQBJtPpRtPpS0UAJtPpRtPpS0UAJtPpRtPpS0UAJtPpRtPpS0UAJtPpRtPpS0UAJtPpW54N19vDutRTOSLaT93MP9k9/wAOtYlFAH0SjiRQykMpAIIOQRTq4P4aeKPtdsNKuW/fQj9wx/iT0+o/l9K7ygAooooAKKKKACiiigAooooAKKKKACsLxn4hHh3RZJkI+0yZjhH+0e/4dfyrammS3ieWRgkaAszE4AA6mvEvFviJ/EmqvMMi2j+SFD2X1+p/w9KAMMhmJJySTkk96Np9KWigBNp9KNp9KWigBNp9KNp9KWigBNp9KNp9KWigBNp9KNp9KWigBNp9KNp9KWigB/PpRz6U+igBnPpRz6U+igBnPpRz6U+igBnPpRz6U+igBnPpRz6U+igBnPpRz6U+igBba4ms7iOeFjHLGwZWHUGvaPCfieLxJp4k+WO5jGJoweh9R7GvFqs6bqdzpF4lzaymKVO46EehHcUAe+UVneHtTfWdHtb10EbyrkqpyAQSP6Vo0AFFFFABRRRQAUUUUAFFFcL8SPEl3pxisLZvKE0ZZ5VPzYyRgenSgDL+Ini77a7aXZvmBD++kU8Of7o9h/OuF59KfRQAzn0o59KfRQAzn0o59KfRQAzn0o59KfRQAzn0o59KfRQAzn0o59KfRQAzn0o59KfRQA/afSjafSnUUAN2n0o2n0p1FADdp9KNp9KdRQA3afSjafSnUUAN2n0o2n0p1FADdp9KNp9KcASQACSewrZ07wdq+pgGOzeND/HN8g/XmgDIgtZrqQRwxPLIeiopYn8BXS6f8ONXvYi8oitOOFlbk/gM4r0Pwxoo0PSYbdliM4B8x4x945PfgmtagDJ8K2UunaDa2067Jotysv8AwI1rUUUAFFFFABRRRQAUUUUAFcZ4w8JXniXVongZIoooMF5M4J3HgYrs6KAPFdV8IarpG5prVniH/LWL5l/+t+NY+0+lfQVcN408FXWqXy3WnxQf6sB0BCMzZPPp0I70AebbT6UbT6Vd1DSL7S2xd2ksH+0ynH59KqUAN2n0o2n0p1FADdp9KNp9KdRQA3afSjafSnUUAN2n0o2n0p1FADdp9KNp9KdRQA7n0o59KfRQAzn0o59KfRQAzn0o59KfRQAzn0o59K6LQvBeoa0FkKfZbY/8tZR1HsOp/lXeaP4I0zSdrmP7VOP+Wk3P5L0FAHm+leF9T1jabe2YRn/lrJ8qfn3/AArr9M+GMKYa/uWlbvHD8q/meT+ldz0ooAoadoVhpQxa2scR/vgZb8zzV+iigAooooAKKKKACiiigAooooAKKKKACiiigAooooARlDqQwDA9QehrC1LwRpGp5Y2wt5D/AB252fp0/St6igDzTU/hleWwZrKdbpeojf5X/wAD+lcpeWFzp8vl3NvJA/o4xn6ete7VDd2UF9EYriJJoz/C6gigDwfn0o59K9I1n4a28+6TTpTA5/5ZSElD9D1H61wup6Vd6PP5V3A0Tdifut9D0NAFHn0o59KfRQAzn0o59KfRQAzn0o59KfRQA/n0o59KfRQAzn0o59KfVnTtPm1W9itbdd0jn8AO5PtQBHp+n3Gp3SW9tEZJW7DsPUnsK9H8O+BbXSws10Fu7vr8w+RD7Dv9TWtoOg2+g2ghhG6Q/wCslI5c/wCHtWnQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVDeWUF/A0NxEs0TdVcZqaigDzfxL4ClsQ9zp+6eActCeXQe3qP1rj+fSveK4jxt4QWSOTUbJAsi5aaJRww7sB6+tAHn3PpRz6U+igBnPpRz6U+igB2B6UYHpT8D0owPSgBmB6V6L8O9IW105r5lxLOcKSOiD/E/yFefxQmaVI0GWdgoHua9osrVbG0ht0+5EgQfgKAJqKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA8l8W6Quka1KiLiGUeZGB0APb8DmsbA9K9E+I1gJtOt7oDLQvsP8Aun/64H5159gelADMD0owPSn4HpRgelAC7T6UbT6U/bRtoA1vB9p9q8RWgI+VCZD+AyP1xXq1ef8Aw7tw2pXMxGdkQUH6kf4V6BQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQBm+I7T7dod7FjJMRYfUcj9RXkW0+le3MoYEEZBGDXjFzB9nuJYieUYqfwNAEG0+lG0+lP20baAFop22jbQB3Hw4jAtr2T+86r+QP+NdjXL/D1NujTH+9Of/QVrqKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACvJfEUQh12+X/pqx/M5r1qvL/GKbfEd52yVP5qKAMSinbaNtADtp9KNp9KdRQB6H4DAGhf8AbVv6V0Vc74E/5AX/AG1b+ldFQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAV5r42U/wDCQznHVV/lXpVeb+NjnxBN/ur/ACoAwNp9KNp9KdRQAUU6igDuvh9OH0ueLukufwIH+Brqa4f4fyML26QH5WjBI9wf/rmu4oAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK8w8Vzi48QXjDorBPyAB/UV6fXkNy5lupnY5ZnLE+5NAEFFOooA//2Q==";
            //    }
            if (person != null)
            {

                PersonEntity Person = person.ToPerson();
                NationalityEntity Nationality = BusinessLayer_LibsLayer.GetNationality(ID: Person.NationalityID.Value).First();
                Person.NationalityLabel = (Nationality.Label != null) ? Nationality.Label : "";
                string Citizenship = "";
                if (Person.IdentificationDocuments.Any() && Person.IdentificationDocuments.First().CitizenshipLibItem_ID != null)
                {

                    LibsEntity CitizenshipLib = BusinessLayer_LibsLayer.GetLibByID(Person.IdentificationDocuments.First().CitizenshipLibItem_ID.Value);
                    Citizenship = (CitizenshipLib.Name != null) ? CitizenshipLib.Name : "";
                }
                return Json(new
                {
                    Birthday = Person.Birthday.Value.ToString("dd/MM/yyyy"),
                    FirstName = Person.FirstName,
                    IdentificationDocuments = Person.IdentificationDocuments,
                    isCustom = Person.isCustom,
                    isEditable = Person.isEditable,
                    LastName = Person.LastName,
                    Living_Entity = Person.Living_Entity,
                    Living_ID = Person.Living_ID,
                    Living_place = Person.Living_place,
                    MiddleName = Person.MiddleName,
                    NationalityID = Person.NationalityID,
                    NationalityLabel = Person.NationalityLabel,
                    Personal_ID = Person.Personal_ID,
                    PhotoLink = (Person.Photo_ID == null || Person.Photo_ID == "") ? "" : "data:image/jpeg;base64," + Person.Photo_ID,
                    Photo_ID = Person.Photo_ID,
                    Registration_address = Person.Registration_address,
                    Registration_Entity = Person.Registration_Entity,
                    Registration_ID = Person.Registration_ID,
                    RelativeLabel = Person.RelativeLabel,
                    SexLibItem_ID = Person.SexLibItem_ID,
                    Status = Person.Status,
                    Citizenship = Citizenship,
                });
            }
            else
            {
                return Json(new { Status = false });
            }
        }

        [HttpPost]
        public ActionResult CheckEmployeeByDocNumber(String DocNumber, bool Type)
        {
            //bool result = BusinessLayer_OrgLayer.AddEmployee(OrgUnitID, FirstName, LastName);

            AVVPerson person = null;
            if (Type)
                person = BusinessLayer_PoliceService.getBYDocumentNumber(DocNumber);
            else
                person = BusinessLayer_PoliceService.getBYPersonalNumber(DocNumber);

            return transferPerson(person);


        }


    }
}