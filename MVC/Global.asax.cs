﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MVC___Internal_System_Administration
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            System.Globalization.CultureInfo culture = (System.Globalization.CultureInfo)System.Globalization.CultureInfo.InvariantCulture.Clone();
            culture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            culture.DateTimeFormat.ShortTimePattern = "hh:mm";
            culture.DateTimeFormat.LongTimePattern = "";// "hh:mm:ss";
            culture.DateTimeFormat.DateSeparator = "/";
            culture.DateTimeFormat.FullDateTimePattern = "dd/MM/yyyy hh:mm:ss";
            System.Globalization.CultureInfo.DefaultThreadCurrentCulture = culture;
        }
    }
}
