﻿// Lists controller

/*****Main Function******/
var ListsControllerClass = function () {
    var _self = this;

    _self.getLibPathViewURL = "/_Popup_/Get_LibContentView";
    _self.getLibViewURL = "/_Popup_/Get_LibSelectedView";
    _self.ViewHolder = null;
    _self.WidgetHolder = null;
    _self.LibItemURL = "/_Popup_/Get_AddLibItem"

    _self.mode = "Add"; // initial mode

    _self.init = function () {
        console.log('List controller inited with action');

        _self.ViewHolder = $('#viewholder');
        _self.WidgetHolder = $('#widgetHolder');
        _self.PathID = null;
        _self.isPathElem = null;
        _self.LibID = null;

        onpopstate();
    }

    // index action
    _self.Index = function () {
        console.log('index called');

        _self.init();

        // bind lists events
        bindListElementEvents();

    }

    // bind List Events
    function bindListElementEvents() {
        var listFolderBtn = $('.listbuttons'); //> button:not([data-target="#lists"])  
        //var listSelectBtn = $(".listTreeFolderIn > a:not(.roleFolderAdd)");

        // bind list main elemtns click and double events
        listFolderBtn.on('click', function (target) {
            target.preventDefault();
            target.stopPropagation();
        });

        listFolderBtn.single_double_click(function (target) {
            // main working function

            var id = parseInt($(this).data('id'));
            var parentid = $(this).data('parentid');
            var isPathElem = $(this).hasClass("libpathelem");

            _self.isPathElem = isPathElem;
            _self.PathID = id;
            _self.LibID = parentid;

            // get current view
            getMainView();

            // push the state in router
            pushRoutingState();

        }, function () {
            var dataTarget = $(this).data('target');
            $(dataTarget).collapse('toggle');
        });

        //// edit role (on click draw employees and edit root)
        //listSelectBtn.on('click', function (target) {
        //    target.preventDefault();
        //    target.stopPropagation();
        //});
        //listSelectBtn.single_double_click(function (target) {
        //    var id = parseInt($(this).data('id'));
        //}, function () {
        //    var id = parseInt($(this).data('id'));
        //    var dataTarget = $(this).data('target');
        //    $(dataTarget).collapse('toggle');
        //});
    }

    // js bindings
    function bindButtons() {

    }

    // main View Router
    function getMainView() {

        if (_self.isPathElem) {

            // get path subitem list
            getLibPathList();
        }
        else {
            if (_self.PathID != null || _self.LibID != null) {

                // get current lib view
                getLibList();
            }
        }

        changeSelectedItem();
    }

    function changeSelectedItem() {

        $(".allSelectedElem").removeClass("selected");
        $(".selectedItem"+_self.PathID).addClass("selected");
    }

    // draw Lib path list
    function getLibPathList() {
        var ID = _self.PathID;
        var ParentID = _self.LibID;
        if (ID !== "" && ParentID !== "") {
            var data = {};
            data.ParentID = ID;
            var url = _self.getLibPathViewURL;
            loader(true);
            _core.getService("Post").Service.postPartial(data, url, function (res) {
                if (res !== null) {
                    _self.ViewHolder.empty().append(res);

                    bindLibListViewEvents();
                    loader(false);
                }
            })
        }
    }

    // lib list events
    function bindLibListViewEvents() {
        var gobackbutton = $("#gobackbutton");

        gobackbutton.unbind().click(function (e) {
            e.preventDefault();

            history.back();
        });
    }

    // lib events
    function bindLibEvents() {
        var addlibitem = $('#addlibitem');
        var editLibItem = $('.editLibItem');
        var removeLibItem = $('.removeLibItem');
        var LibItemIn = $('.LibItemIn');
        var gobackbutton = $("#gobackbutton");

        removeLibItem.unbind().click(function (e) {
            e.preventDefault();

            var ID = $(this).data("id");
            var data = {};
            data.ID = ID;
            var url = _self.LibItemURL;
            loader(true);

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    _core.getWidget("LibItem").Widget.Remove(data, function (res) {
                        if (res != null && res.status) {
                            getLibList();
                        }
                    });
                }
            })
        });

        editLibItem.unbind().click(function (e) {
            e.preventDefault();

            var ID = $(this).data("id");
            var data = {};
            data.ID = ID;
            var url = _self.LibItemURL;
            loader(true);
            _core.getWidget("LibItem").Widget.Edit(data, function (res) {
                if (res != null && res.status) {
                    getLibList();
                }
            });
        });

        addlibitem.unbind().click(function (e) {
            e.preventDefault();

            var PathID = $(this).data("pathid");
            var LibID = $(this).data("libid");
            var IsPath = $(this).data("ispath");
            var ID = $(this).data("id");
            var data = {};
            data.PathID = ((IsPath) ? PathID : null);
            data.ID = ID;
            data.LibID = LibID;
            var url = _self.LibItemURL;
            loader(true);
            _core.getWidget("LibItem").Widget.Add(data, function (res) {
                if (res != null && res.status) {
                    getLibList();
                }
            });
        });

        LibItemIn.unbind().click(function (e) {
            e.preventDefault();

            var data = {};

            //_self.PathID = $(this).data("pathid");
            _self.LibID = $(this).data("libid");
            _self.isPathElem = false;
            
            // get sub list
            getMainView();

            // push to history
            pushRoutingState();
        });

        gobackbutton.unbind().click(function (e) {
            e.preventDefault();

            history.back();
        });
    }

    // get lib item list
    function getLibList() {
        var data = {};
        data.LibID = _self.LibID;
        data.PathID = _self.PathID;
        var url = _self.getLibViewURL;
        loader(true);
        _core.getService("Post").Service.postPartial(data, url, function (res) {
            if (res !== null) {
                _self.ViewHolder.empty().append(res);

                bindLibEvents();
                loader(false);
            }
        })
    }

    // enable disable loader
    function loader(status) {
        if (status)
            _core.getService("Loading").Service.enableBlur("loaderDiv");
        else
            _core.getService("Loading").Service.disableBlur("loaderDiv");
    }

    // push state
    function pushRoutingState() {

        var stateData = {};
        stateData.isPathElem = _self.isPathElem;
        stateData.PathID = _self.PathID;
        stateData.LibID = _self.LibID;
        var page = "?page=" + ((typeof _self.LibID == "undefined") ? _self.PathID : _self.LibID);
        var title = "Դասակարգիչ" + _self.LibID;

        if (stateData.LibID != null || stateData.PathID != null)
            history.pushState(stateData, title, page);
    }

    // pop state part
    function onpopstate() {
        window.onpopstate = function (event) {

            _self.isPathElem = ((event.state == null) ? null : event.state.isPathElem);
            _self.PathID = ((event.state == null) ? null : event.state.PathID);
            _self.LibID = ((event.state == null) ? null : event.state.LibID);
            getMainView();
            //alert("location: " + document.location + ", state: " + JSON.stringify(event.state));
        };
    }
}
/************************/


// creating class instance
var ListsController = new ListsControllerClass();

// creating object
var ListsControllerObject = {
    // important
    Name: "Lists",

    Controller: ListsController
}

// registering controller object
_core.addController(ListsControllerObject);