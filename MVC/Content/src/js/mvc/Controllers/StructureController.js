﻿// Structure controller

/*****Main Function******/
var StructureControllerClass = function () {
    var _self = this;

    _self.Person = null;
    _self.passMatch = false;
    _self.ViewHolder = null;
    _self.WidgetHolder = null;
    _self.isOrgUnit = null;
    _self.SelectedTypeID = null;
    _self.OrgUnitID = null;
    _self.canUpdateTree = true;
    _self.StructureTreeCollapseList = [];
    _self.getOrgUnitViewURL = "/_Popup_/Get_OrgUnitView";
    _self.getEmployeeViewURL = "/_Popup_/Get_EmployeeView";
    _self.AddOrgUnitURL = "/Structure/AddOrgUnit";
    _self.EditOrgUnitURL = "/Structure/UpdateOrgUnit";
    _self.RemOrgUnitURL = "/Structure/DeleteOrgUnit";
    _self.UpdateStructureTreeLevelURL = "/Structure/UpdateStructureTreeLevel";
    _self.OrgUnitURL = "/_Popup_/Draw_OrgUnit";
    _self.OrgUnitPositionURL = "/_Popup_/Draw_OrgUnitPosition";
    _self.GetStructureTreeURL = "/_Popup_/Get_StructureTree";
    _self.GetEmployeeListURL = "/_Popup_/GetEmployees";
    _self.AddEmployeeURL = "/Structure/AddEmployeeByDocNumber";
    _self.EditEmployeeURL = "/Structure/UpdateEmployee";
    _self.RemEmployeeURL = "/Structure/DeleteEmployee";
    _self.getStructurePositionTreeURL = "/_Popup_/GetStructurePositionTree";

    _self.mode = "Add"; // initial mode

    _self.init = function () {
        console.log('List controller inited with action');

        _self.ViewHolder = $('#viewholder');
        _self.WidgetHolder = $('#widgetHolder');
        _self.PathID = null;
        _self.isOrgUnit = null;
        _self.SelectedTypeID = null;
        _self.OrgUnitID = null;
        _self.isPathElem = null;
        _self.LibID = null;
        _self.Person = null;
        _self.StructureTreeCollapseList = (typeof $.cookie("structure-collapse-state") != "undefined") ? JSON.parse($.cookie("structure-collapse-state")) : [];

        //onpopstate();
    }

    // index action
    _self.Index = function () {
        console.log('index called');

        _self.init();

        // bind Events()
        bindEvents();

        // bind lists events
        bindSctructureElementEvents();

        // apply collapse state
        ApplyCollapseState();

    }

    // bind Structure Events
    function bindSctructureElementEvents() {
        var listbutton = $('.listbutton '); //> button:not([data-target="#lists"])  
        //var listSelectBtn = $(".listTreeFolderIn > a:not(.roleFolderAdd)");
        listbutton.unbind();
        // bind list main elemtns click and double events
        listbutton.on('click', function (target) {
            target.preventDefault();
            target.stopPropagation();
        });
        listbutton.single_double_click(function (target) {
            // main working function

            var id = $(this).data('id');
            var editable = $(this).data('editable') == "True";
            var typeid = $(this).data('typeid');
            var hassubitems = $(this).data('hassubitems');
            var parentid = $(this).data('parentid');

            _self.ID = id;
            _self.Editable = editable;
            _self.TypeID = typeid;
            _self.hasSubItems = hassubitems;
            _self.ParentID = parentid;

            // get current view
            getMainView();

            // push the state in router
            pushRoutingState();

        }, function () {
            var dataTarget = $(this).data('target');
            var id = $(this).data("id");
            var typeid = $(this).data("typeid");

            // save coolapse state of current orgunit structure
            var data = {};
            data.id = id;
            data.action = (!$('#' + id).hasClass('in')) ? "show" : "hide";

            var added = false;
            for (var i in _self.StructureTreeCollapseList) {
                if (_self.StructureTreeCollapseList[i].id == id) {
                    _self.StructureTreeCollapseList[i].action = data.action;
                    added = true;
                }
            }
            if (!added) {
                _self.StructureTreeCollapseList.push(data);
            }

            // hold in cookies
            $.cookie("structure-collapse-state", JSON.stringify(_self.StructureTreeCollapseList));

            // get employees if typeid =  4 (position org unit)
            if (typeid == "4" && data.action == "show") {
                getEmployeeList(id);
            }

            $(dataTarget).collapse('toggle');
        });

        //// edit role (on click draw employees and edit root)
        //listSelectBtn.on('click', function (target) {
        //    target.preventDefault();
        //    target.stopPropagation();
        //});
        //listSelectBtn.single_double_click(function (target) {
        //    var id = parseInt($(this).data('id'));
        //}, function () {
        //    var id = parseInt($(this).data('id'));
        //    var dataTarget = $(this).data('target');
        //    $(dataTarget).collapse('toggle');
        //});

        $(".managerTree > .managerTreeContent > .managerTreeFolder").sortable({
            handle: ".handle",
            scroll: false
        });
        $(".managerTree > .managerTreeContent > .managerTreeFolder .managerTreeFolderIn").each(function () {
            $(this).sortable({
                handle: ".handle",
                scroll: false
            });
            $(this).on('sortupdate', function () {
                
                if (_self.canUpdateTree) {

                    _core.getService("Loading").Service.enableBlur("structureTreeLoader"); // enable loader

                    _self.canUpdateTree = false;
                    var parentid = $(this).data('id');
                    if (typeof parentid != "undefined" && parentid != "" && parentid != null) {

                        var list = [];
                        var iterator = 0;
                        $(".dragElem" + parentid).each(function () {
                            var data = {};
                            data.ID = $(this).data('id');
                            data.SortLevel = iterator;
                            list.push(data);
                            iterator++;
                        })

                        var url = _self.UpdateStructureTreeLevelURL;

                        _core.getService("Post").Service.postJson(list, url, function (res) {

                            _core.getService("Loading").Service.disableBlur("structureTreeLoader"); // enable loader

                            getStructureTree(function (res) {
                                _self.canUpdateTree = true;
                            });
                        })

                    }
                };
            });
        })
    }

    // main View Router
    function getMainView() {

        getOrgUnitView(null, true, function () { });

        changeSelectedItem();
    }

    function changeSelectedItem() {

        $(".allSelectedElem").removeClass("selected");
        $(".selectedItem"+_self.PathID).addClass("selected");
    }

    // bind main events
    function bindEvents() {
        var buildTreeBtn = $("#buildTreeBtn");

        buildTreeBtn.unbind().click(function () {

            getStructurePositionTree();
        });
    }

    // draw orgunit view
    function getOrgUnitView(SelectedTypeID, isOrgUnit, callback) {

        var ID = _self.ID;
        var TypeID = _self.TypeID;
        var ParentID = _self.ParentID;
        _self.isOrgUnit = isOrgUnit;
        _self.SelectedTypeID = SelectedTypeID;

        var data = {};
        data.ID = ID;
        data.ParentID = ParentID;
        if (isOrgUnit) {
            data.TypeID = TypeID;
            data.SelectedTypeID = SelectedTypeID;
        }
        var url = _self.getOrgUnitViewURL;
        if (!isOrgUnit) {
            url = _self.getEmployeeViewURL;
        }

        loader(true);
        _core.getService("Post").Service.postPartial(data, url, function (res) {
            loader(false);

            if (res !== null) {
                _self.ViewHolder.empty().append(res);

                if (isOrgUnit) {
                    bindOrgUnitViewEvents();
                }
                else {
                    bindEmployeeViewEvents();
                }
            }

            callback();
        })
    }

    // get employee list for position org unit
    function getEmployeeList(id){
        if (typeof id != "undefined" && id != "" && id != null) {
            var data = {};
            data.OrgUnitID = id;
            var url = _self.GetEmployeeListURL;

            // get async
            _core.getService("Post").Service.postPartialAsync(data, url, function (res) {

                $(".employee" + id).remove();
                $("#" + id).prepend(res);

                // bind click events
                bindEmployeeEvents();
            })
        }
    }

    // OrgUnit events
    function bindOrgUnitViewEvents() {

        var editRootBtn = $("#editRootBtn");
        var declineEditRootBtn = $("#declineEditRootBtn");
        var removeRootBtn = $("#removeRootBtn");
        var saveRootBtn = $("#saveRootBtn");
        var orgUnitType = $("#add-coretype");
        var checkItems = $(".checkItems");

        if (_self.ID == null) {
            editRootBtn.addClass("hidden");
            declineEditRootBtn.addClass("hidden");
            removeRootBtn.addClass("hidden");
            saveRootBtn.removeClass("hidden");
        }
        else {
            editRootBtn.removeClass("hidden");
            declineEditRootBtn.addClass("hidden");
            removeRootBtn.addClass("hidden");
            saveRootBtn.addClass("hidden");
        }

        // bind button Events
        editRootBtn.unbind().bind("click", function (e) {
            e.preventDefault();

            // show other buttons
            editRootBtn.addClass("hidden");
            declineEditRootBtn.removeClass("hidden");
            removeRootBtn.removeClass("hidden");
            saveRootBtn.removeClass("hidden");

            // enable fields
            toggleFieldsDisable(false);
        })

        declineEditRootBtn.unbind().bind("click", function (e) {
            e.preventDefault();

            // show other buttons
            editRootBtn.removeClass("hidden");
            declineEditRootBtn.addClass("hidden");
            removeRootBtn.addClass("hidden");
            saveRootBtn.addClass("hidden");

            // load again
            getOrgUnitView(null, true, function () { });

            // enable fields
            //toggleFieldsDisable(true);
        });

        saveRootBtn.unbind().click(function (e) {
            e.preventDefault();

            var data = collectData();

            if (typeof data == "object") {
                saveRoot(data);
            }
        });

        removeRootBtn.unbind().click(function (e) {
            e.preventDefault();

            var id = _self.ID;
            if (typeof id != "undefined" && id != null && id != "") {
                removeRoot(id);
            }
        });

        // bind Validation
        bindValidation("checkItems", "#saveRootBtn"); 

        orgUnitType.bind('change', function () {

            var TypeID = orgUnitType.val();

            if (TypeID != "1001")
                getOrgUnitView(TypeID, true, function () { });
            else
                getOrgUnitView(TypeID, false, function () { })
        });

        // select read if write is selected
        $('.writeCheckBox').unbind().bind('change', function () {
            var id = $(this).data('id');

            if (typeof id != "undefined" && id != null && id != "") {
                if ($(this).is(":checked")) {
                    $('.permissionRead' + id).prop("checked", true);
                }
                else {
                    removePositionApprover(id);
                }
            }
        });

        // remove write if read is unselected
        $('.readCheckBox').unbind().bind('change', function () {
            var id = $(this).data('id');
            if (typeof id != "undefined" && id != null && id != "") {
                if (!$(this).is(":checked")) {
                    $('.permissionWrite' + id).prop("checked", false);
                    removePositionApprover(id);
                }
            }
        });

        // selectize
        $(".selectize").each(function () {
            if ($(this).is("select") && typeof $(this)[0].selectize == "undefined") {
                $(this).selectize({
                    create: false
                });
            }
        });

        // bind approve select
        bindApproveSelect();

        if (_self.ID != null) {
            // generate selected approvers names
            $(".getPermission").each(function () {
                var PositionID = $(this).data("id");

                if (typeof PositionID != "undefined" && PositionID != null && PositionID != "") {
                    GenerateApproversName(PositionID);
                }
            });

            // disable fields
            toggleFieldsDisable(true);
        }
    }

    // EmployeeView Events
    function bindEmployeeViewEvents() {

        var editEmployeeBtn = $("#editEmployeeBtn");
        var declineEditEmployeeBtn = $("#declineEditEmployeeBtn");
        var removeEmployeeBtn = $("#removeEmployeeBtn");
        var saveEmployeeBtn = $("#saveEmployeeBtn");
        var userPoliceDataSearch = $("#userPoliceDataSearch");
        var userPoliceDataUpdate = $("#userPoliceDataUpdate");
        var dates = $(".Dates");
        var orgUnitType = $("#add-coretype");
        var checkItems = $(".checkItems");
        var pass1 = $("#pass1");
        var pass2 = $("#pass2");

        if (_self.ID == null) {
            editEmployeeBtn.addClass("hidden");
            declineEditEmployeeBtn.addClass("hidden");
            removeEmployeeBtn.addClass("hidden");
            saveEmployeeBtn.removeClass("hidden");
        }
        else {
            editEmployeeBtn.removeClass("hidden");
            declineEditEmployeeBtn.addClass("hidden");
            removeEmployeeBtn.addClass("hidden");
            userPoliceDataUpdate.addClass("hidden");
            saveEmployeeBtn.addClass("hidden");
        }

        // bind button Events
        editEmployeeBtn.unbind().bind("click", function (e) {
            e.preventDefault();

            // show other buttons
            editEmployeeBtn.addClass("hidden");
            declineEditEmployeeBtn.removeClass("hidden");
            removeEmployeeBtn.removeClass("hidden");
            saveEmployeeBtn.removeClass("hidden");
            userPoliceDataUpdate.addClass("hidden");

            // enable fields
            toggleFieldsDisable(false);
        });

        declineEditEmployeeBtn.unbind().bind("click", function (e) {
            e.preventDefault();

            // show other buttons
            editEmployeeBtn.removeClass("hidden");
            declineEditEmployeeBtn.addClass("hidden");
            removeEmployeeBtn.addClass("hidden");
            saveEmployeeBtn.addClass("hidden");
            userPoliceDataUpdate.addClass("hidden");

            // load again
            getOrgUnitView(null, false, function () { });

            // enable fields
            //toggleFieldsDisable(true);
        });

        saveEmployeeBtn.unbind().click(function (e) {
            e.preventDefault();
            var data = collectEmployeeData();

            if (typeof data == "object") {
                saveEmployee(data);
            }
        });

        removeEmployeeBtn.unbind().click(function (e) {
            e.preventDefault();

            var id = _self.ID;
            if (typeof id != "undefined" && id != null && id != "") {
                removeEmployee(id);
            }
        });

        // police search bind
        userPoliceDataSearch.unbind().click(function () {

            loader(true);

            // call find persion widget
            var findPersionWidget = _core.getWidget('FindPerson').Widget;

            //run
            findPersionWidget.Show(function (res) {
                _self.Person = res.Person;
                matchPersonData(res.Person, res.menualSearch);
                _self.menualSearch = res.menualSearch;
                
            });
        });

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // tooltip
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'manual'
        });

        checkItems.unbind().bind("change keyup", function () {
            var block = false;

            checkItems.each(function () {
                var name = $(this).attr("name");

                if (typeof name != "undefined" && name != "" && $(this).val() == "" || $(this).val() == null) {
                    if (name == "File" || name == "Password") {
                        if (_self.ID == null) block = true;
                    }
                    else
                        block = true;
                }
            });

            if ((pass1.val() != "" || pass2.val() != "") && _self.ID == null) {
                if (pass1.val() != pass2.val()) {
                    block = true;
                    pass2.parent().tooltip('show');
                }
                else {
                    pass2.parent().tooltip('hide');
                }
            }

            saveEmployeeBtn.prop("disabled", block);
        });

        orgUnitType.bind('change', function () {

            var TypeID = orgUnitType.val();

            if (TypeID != "1001")
                getOrgUnitView(TypeID, true, function () { });
            else
                getOrgUnitView(TypeID, false, function () { })
        });

        // selectize
        $(".selectize").each(function () {
            if ($(this).is("select") && typeof $(this)[0].selectize == "undefined") {
                $(this).selectize({
                    create: false
                });
            }
        });

        // bind approve select
        bindApproveSelect();

        if (_self.ID != null) {
            // disable fields
            toggleFieldsDisable(true);
        }
    }

    // employe events
    function bindEmployeeEvents() {

        $(".employeeItem").unbind().click(function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            _self.ID = id;
            _self.OrgUnitID = $(this).data("orgunitid");

            getOrgUnitView(null, false, function () {

            })
        });
    }

    // validation of form
    function bindValidation(bindClass, toggleBtn) {
        var checkItems = $("." + bindClass);

        checkItems.unbind().bind("change keyup", function () {
            var block = false;

            checkItems.each(function () {
                var name = $(this).attr("name");

                if (typeof name != "undefined" && name != "" && $(this).val() == "" || $(this).val() == null) {
                    block = true;
                }
            });

            $(toggleBtn).prop("disabled", block);
        });
    };

    // enable disable loader
    function loader(status) {
        if (status)
            _core.getService("Loading").Service.enableBlur("loaderDiv");
        else
            _core.getService("Loading").Service.disableBlur("loaderDiv");
    }

    // push state
    function pushRoutingState() {

        var stateData = {};
        stateData.isPathElem = _self.isPathElem;
        stateData.PathID = _self.PathID;
        stateData.LibID = _self.LibID;
        var page = "?page=" + ((typeof _self.LibID == "undefined") ? _self.PathID : _self.LibID);
        var title = "Դասակարգիչ" + _self.LibID;

        if (stateData.LibID != null || stateData.PathID != null)
            history.pushState(stateData, title, page);
    }

    // pop state part
    function onpopstate() {
        window.onpopstate = function (event) {

            _self.isPathElem = ((event.state == null) ? null : event.state.isPathElem);
            _self.PathID = ((event.state == null) ? null : event.state.PathID);
            _self.LibID = ((event.state == null) ? null : event.state.LibID);
            getMainView();
            //alert("location: " + document.location + ", state: " + JSON.stringify(event.state));
        };
    }

    // match person data
    function matchPersonData(Person) {
        try {
            $("#user-person-name-1").val(Person.FirstName);
            $("#user-person-name-2").val(Person.LastName);
            $("#user-person-name-3").val(Person.MiddleName);
            $("#user-person-address-reg").val(Person.Registration_address);
            $("#user-person-bd").val(Person.Birthday);
            $("#user-person-social-id").val(Person.Personal_ID);
            $("#user-person-nationality").val(Person.NationalityLabel);
            $("#user-person-address-liv").val(Person.Living_place);
            $("#user-passport-number").val((typeof Person.IdentificationDocuments != "undefined" && Person.IdentificationDocuments != null && Person.IdentificationDocuments.length != 0) ? Person.IdentificationDocuments[0].Number : "");
            $("#user-passport-date").val((typeof Person.IdentificationDocuments != "undefined" && Person.IdentificationDocuments != null && Person.IdentificationDocuments.length != 0) ? Person.IdentificationDocuments[0].Date : "");
            $("#user-passport-issuer").val((typeof Person.IdentificationDocuments != "undefined" && Person.IdentificationDocuments != null && Person.IdentificationDocuments.length != 0) ? Person.IdentificationDocuments[0].FromWhom : "");

            if (Person.PhotoLink != "") $("#user-person-photolink").attr("src", Person.PhotoLink);
        }
        catch (e) {
            console.error(e)
        }
    }

    // bind dropdown watch
    function bindApproveSelect() {
        var approveSelect = $(".approveSelect");
        var body = $("body");
        var approveSelectDrop = $(".approveSelectDropInside");
        var openClass = $(".open");
        var approverCheckBox = $(".approverCheckBox");

        // bind <ul> <li> dropdown select
        approveSelectDrop.unbind().bind('click', function () {
            $(this).parent().toggleClass("open");
        });

        // close if out of div is clicked
        body.unbind();
        body.on("click", function (e) {
            if (!approveSelect.parents(".approveSelectWrap").is(e.target) && $(".approveSelectWrap").has(e.target).length === 0 && openClass.has(e.target).length === 0) {
                approveSelectDrop.parents(".approveSelectWrap").removeClass("open");
            }
        });

        // bind list change event
        approverCheckBox.unbind().change(function () {

            // generate name of select
            var PositionID = $(this).data('positionid');

            // generate approver's names
            GenerateApproversName(PositionID);
        })
    }

    // get structure tree
    function getStructurePositionTree() {

        var data = {};
        var url = _self.getStructurePositionTreeURL;
        loader(true);
        _core.getService("Post").Service.postPartial(data, url, function (res) {
            loader(false);
            _self.WidgetHolder.empty().append(res);

            bindStructurePositionTreeEvenets();

            $(".treeViewModal").modal("show");
        })
    }
    
    // bind structure position tree Events
    function bindStructurePositionTreeEvenets() {

        var treeWrap = $(".treeWrap");
        var treeInside = $(".treeInside");
        var tree = $(".tree");

        treeWrap.addClass("treeWrapHidden");

        $(".modal").on("show.bs.modal", function (e) {
            $("html").addClass("noScroll");
        });

        $(".modal").on("hide.bs.modal", function (e) {
            $("html").removeClass("noScroll");
        });

        $(".modal").on("shown.bs.modal", function (e) {
            treeInside.css("width", tree.width());
            treeWrap.scrollLeft((treeInside.width() - treeWrap.width()) / 2);
            treeWrap.removeClass("treeWrapHidden");
        });
    }

    // generate approver's names
    function GenerateApproversName(PositionID) {
        if (typeof PositionID != "undefined" && PositionID != "" && PositionID != null) {
            var approverNameItem = $(".approverNameItem" + PositionID);
            var defaultName = approverNameItem.data("defaultname");
            var ApproversName = "";

            $(".getApprover" + PositionID).each(function () {
                var currentName = $(this).data("name");
                if ($(this).is(":checked")) {
                    if (typeof currentName != "undefined" && currentName != null && currentName != "") {
                        if (ApproversName == "") ApproversName = currentName;
                        else ApproversName += ", " + currentName
                    }
                }
            });

            if (ApproversName == "") ApproversName = defaultName;
            approverNameItem.html(ApproversName);
        }
    }

    // remove position approver
    function removePositionApprover(PositionID) {
        if (typeof PositionID != "undefined" && PositionID != "" && PositionID != null) {
            $(".getApprover" + PositionID).each(function () {
                $(this).prop("checked", false);
            });

            // generate name again
            GenerateApproversName(PositionID);
        }
    }

    // toggle org unit fields
    function toggleFieldsDisable(disable){
        
        // field elems
        $('.disableFieldElem').each(function () {

            $(this).attr("disabled", disable);
        });

        // field elems
        $('.disableFieldSelect').each(function () {
            if (typeof $(this)[0].selectize != "undefined") {
                if (disable)
                    $(this)[0].selectize.disable();
                else
                    $(this)[0].selectize.enable();
            }
        });
    }

    // collect employee data
    function collectEmployeeData() {
        var data = new FormData();

        $(".getItems").each(function () { // get input's data
            var name = $(this).attr("name");
            if (name != "undefined" && name != null && name != "") {
                if (name == "File") {
                    data.append(name, $(this)[0].files[0], $(this).val());
                }
                else {
                    data.append(name, $(this).val());
                }
            }
        });

        // append person data
        if (_self.Person != null && typeof _self.Person == "object") {
            data.append("DocNumber", _self.Person.Personal_ID);
            data.append("Type", false);
        }

        // append OrgUnitID and ID
        //data.append("OrgUnitID", (_self.OrgUnitID == null) ? _self.ParentID : _self.OrgUnitID);
        data.append("OrgUnitID", _self.ParentID);
        data.append("ID", _self.ID);

        return data;
    }

    // collect Org Unit Data
    function collectData() {
        var data = {};
        $(".getItems").each(function () { // get input's data
            var name = $(this).attr("name");
            if (name != "undefined" && name != null && name != "") {
                data[name] = $(this).val();
            }
        });

        $(".getItemsSelect").each(function () { // get selectize data
            var name = $(this).attr("name");
            if (name != "undefined" && name != null && name != "") {
                var currentElem = $(this)[0].selectize;
                if (typeof currentElem != "undefined") {
                    if (name == "Single")
                        data[name] = currentElem.getValue() == "True";
                    else
                        data[name] = currentElem.getValue();
                }
            }
        });

        $(".getItemsSelectMulti").each(function () { // get multi selectize data
            var name = $(this).attr("name");
            if (name != "undefined" && name != null && name != "") {
                var currentElem = $(this)[0].selectize;
                if (typeof currentElem != "undefined") {
                    var values = currentElem.getValue();
                    if (values != null) {
                        var list = [];
                        for (var i in values) {
                            list.push(values[i]);
                        }
                        data[name] = list;
                    }
                }
            }
        });
        
        var Permissions = [];
        $(".getPermission").each(function () { // get permissions data
            var id = $(this).data("id");
            if (typeof id != "undefined" && id != null && id != "") {
                var isRead = $(".permissionRead" + id).is(":checked");
                var isWrite = $(".permissionWrite" + id).is(":checked");

                if (isRead) {
                    var PPType = 0;
                    var ID = id;
                    var Approvers = [];

                    if (isWrite) {
                        PPType = 1;

                        $(".getApprover" + id).each(function () {
                            if ($(this).is(":checked")) {
                                var ApproverID = $(this).data("id");
                                if (typeof ApproverID != "undefined" && ApproverID != null && ApproverID != "") {
                                    Approvers.push(ApproverID);
                                }
                            }
                        });

                        if (Approvers.length != 0)
                            PPType = 2;
                    }

                    var tempData = {};
                    tempData["ID"] = ID;
                    tempData["Approvers"] = Approvers;
                    tempData["PPType"] = PPType;

                    Permissions.push(tempData);
                }
            }
        });

        data["Permissions"] = Permissions;
        data["ParentID"] = _self.ParentID;
        data["ID"] = _self.ID;

        return data;
    }

    // save root
    function saveRoot(data) {
        
        var url = _self.AddOrgUnitURL;
        if (_self.ID != null)
            url = _self.EditOrgUnitURL;

        loader(true); // enable loader

        _core.getService("Post").Service.postJson(data, url, function (res) { // save on server
            loader(false); // disable loader
            if (typeof res != "undefined" && res.status) {

                _self.ID = res.id;
                getOrgUnitView(null, true, function () {

                    getStructureTree(function () { });
                });
            }
        });
    }

    // remove root
    function removeRoot(id) {
        var data = {};
        data.ID = id;

        var url = _self.RemOrgUnitURL;

        loader(true); // enable loader

        _core.getWidget("YesOrNo").Widget.Ask(1, function (res) {
            if (res) {

                loader(true); // enable loader
                
                _core.getService("Post").Service.postJson(data, url, function (res) { // save on server
                    loader(false); // disable loader
                    if (res != null) {
                        _self.ID = null;
                        
                        getOrgUnitView(null, true, function () {
                            getStructureTree(function () { });
                        });
                    }
                });
            }
        });
    }

    // save employee
    function saveEmployee(data) {

        var url = _self.AddEmployeeURL;
        if (_self.ID != null)
            url = _self.EditEmployeeURL;

        loader(true); // enable loader

        _core.getService("Post").Service.postFormData(data, url, function (res) { // save on server
            loader(false); // disable loader
            if (typeof res != "undefined" && res.status) {

                _self.ID = res.id;
                getOrgUnitView(null, false, function () {

                    getEmployeeList(_self.ParentID);
                    //getEmployeeList((_self.OrgUnitID == null) ? _self.ParentID : _self.OrgUnitID);
                });
            }
        });
    }

    function removeEmployee(id) {
        var data = {};
        data.EmployeeID = id;
        data.URL = _self.RemEmployeeURL;

        loader(true); // enable loader

        _core.getWidget("Order").Widget.Add(data, function (res) {
            if (res) {

                _self.ID = null;

                getOrgUnitView(null, false, function () {

                    getEmployeeList(_self.ParentID);
                    //getEmployeeList((_self.OrgUnitID == null) ? _self.ParentID : _self.OrgUnitID)
                });
            }
        });
    }

    // get Organization tree
    function getStructureTree(callback) {
        var data = {};
        var url = _self.GetStructureTreeURL;
        _core.getService("Loading").Service.enableBlur("structureTreeLoader"); // enable loader

        _core.getService("Post").Service.postPartial(data, url, function (res) { // save on server

            callback();

            if (res != null) {
                $("#structureTreeHolder").empty().append(res);

                // bind structure tree events
                bindSctructureElementEvents();

                ApplyCollapseState();

            }

            _core.getService("Loading").Service.disableBlur("structureTreeLoader"); // disable loader
        });
    }

    // apply collapse states which were before new load
    function ApplyCollapseState() {
        for (var i in _self.StructureTreeCollapseList) {
            var item = _self.StructureTreeCollapseList[i];

            if (typeof item.id != "undefined" && item.id != null && item.id != "") {
                if ($("#" + item.id).hasClass("in") && item.action == "hide")
                    $("#" + item.id).collapse(item.action);
                if (!$("#" + item.id).hasClass("in") && item.action == "show")
                    $("#" + item.id).collapse(item.action);
            }
        }
    }
}
/************************/


// creating class instance
var StructureController = new StructureControllerClass();

// creating object
var StructureControllerObject = {
    // important
    Name: "Structure",

    Controller: StructureController
}

// registering controller object
_core.addController(StructureControllerObject);