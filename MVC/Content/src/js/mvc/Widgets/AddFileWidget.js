﻿// AddFile widget

/*****Main Function******/
var AddFileWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.PrisonerID = null;
    _self.type = null;
    _self.id = null;
    _self.mode = null;
    _self.modalUrl = "/_Popup_/Get_AddFile";
    _self.viewUrl = "/_Popup_Views_/Get_AddFile";
    _self.finalFormData = new FormData();
    _self.removeFileURL = '/_Data_Main_/RemoveFile';
    _self.addFileURL = '/_Data_Main_/AddFile';
    _self.editFileURL = '/_Data_Main_/EditFile';
    
    // action list
    _self.actionList = {
    };

    _self.init = function () {
        console.log('File Widget Inited');

        _self.finalFormData = new FormData();
        _self.callback = null;
        _self.content = null;
        _self.mode = null;
        _self.id = null;
        _self.PrisonerID = null;
        _self.type = null;
    };

    // ask action
    _self.Add = function (data, callback) {

        // initilize
        _self.init();

        console.log('File Add called');

        // save callback
        _self.callback = callback;

        _self.mode = 'Add';

        _self.type = data.TypeID;

        _self.PrisonerID = data.PrisonerID;

        // loading view
        loadView(_self.modalUrl);
    };

    // ask action
    _self.Edit = function (data, callback) {

        // initilize
        _self.init();

        console.log('File Edit called');

        // save callback
        _self.callback = callback;

        _self.mode = 'Edit';

        _self.type = data.TypeID;

        _self.id = data.ID;

        // loading view
        loadView(_self.modalUrl);
    };

    // ask action
    _self.View = function (data, callback)
    {

        // initilize
        _self.init();

        console.log('File Edit called');

        // save callback
        _self.callback = callback;

        _self.mode = 'View';

        _self.type = data.TypeID;

        _self.id = data.ID;

        // loading view
        loadView(_self.viewUrl);
    };

    // remove file Action
    _self.Remove = function (data, callback) {

        // initilize
        _self.init();

        _self.type = data.TypeID;


        _core.getService('Loading').Service.enableBlur('loaderClass');

        // yes or no widget
        _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

            _core.getService('Loading').Service.disableBlur('loaderClass');

            if (res) {

                var url = _self.removeFileURL;
                
                _core.getService('Post').Service.postJson(data, url, function (res) {
                    if (res != null) {
                        callback(res);
                    }
                });
            }
        });
    }

    // loading modal from Views
    function loadView(url) {
            if (_self.id != null) url = url + '/' + _self.id;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            $.get(url, function (res) {

                _core.getService('Loading').Service.disableBlur('loaderClass');

                // save content for later usage
                _self.content = res;

                // content
                $('#widgetHolder').html(_self.content);

                // bind events for yes and no
                bindEvents();

                // showing modal
                $('.add-file-modal').modal('show');
            }, 'html');
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var addFileToggle = $('#add-file-toggle');
        var acceptBtn = $('#acceptBtn');
        var checkItems = $('.checkValidateAddFile');
        var fileName = $('#filename');
        _self.getItems = $('.getItemsFile');

        // unbind
        addFileToggle.unbind();
        acceptBtn.unbind();

        // bind file change
        addFileToggle.bind('change', function () {
            _self.finalFormData.delete('fileImage');
            _self.finalFormData.append('fileImage', $(this)[0].files[0], $(this).val());

            // change filename fields and trigger change
            fileName.val($(this).val());
            fileName.html($(this).val());
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            // adding file
            if (_self.id == null) {

                // default add file
                var url = _self.addFileURL;

                // append prisonerid and type
                data.append('PrisonerID', _self.PrisonerID);
                data.append('TypeID', _self.type);
            }
            else {

                // default edit file
                url = _self.editFileURL;

                // append id
                data.append('ID', _self.id);
            }

            //// log it
            //for (var pair of data.entries()) {
            //    console.log(pair[0] + ', ' + pair[1]);
            //}

            var curClass = '';
            if (typeof _self.type == 'undefined') curClass = 'fileLoader';
            else {
                if (parseInt(_self.type) == 1) curClass = 'imageLoader';
                else if (parseInt(_self.type) == 8) curClass = 'fileSentenceLoader';
                else curClass = 'fileLoader';
            }

            _core.getService('Loading').Service.enableBlur(curClass);

            // post service
            var postService = _core.getService('Post').Service;

            postService.postFormData(data, url, function (res) {

                _core.getService('Loading').Service.disableBlur(curClass);

                if (res != null) {
                    _self.callback(res);
                }
                else alert('serious eror on adding file');

            })

            //hiding modal
            $('.add-file-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // selectize
        $(".selectizeAddFile").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // collect data
    function collectData() {

        // collect data
        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            _self.finalFormData.delete(name);
            _self.finalFormData.append(name, $(this).val());

        });

        return _self.finalFormData;
    }

}
/************************/


// creating class instance
var AddFileWidget = new AddFileWidgetClass();

// creating object
var AddFileWidgetObject = {
    // important
    Name: 'AddFile',

    Widget: AddFileWidget
}

// registering widget object
_core.addWidget(AddFileWidgetObject);