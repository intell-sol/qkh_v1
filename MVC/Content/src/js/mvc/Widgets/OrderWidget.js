﻿// Order widget

/*****Main Function******/
var OrderWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.PrisonerID = null;
    _self.type = null;
    _self.id = null;
    _self.URL = null;
    _self.EmployeeID = null;
    _self.mode = null;
    _self.modalUrl = "/_Popup_/Get_Order";
    _self.ModalName = "add-order-modal";
    _self.viewUrl = "/_Popup_Views_/Get_Order";
    _self.finalFormData = new FormData();
    _self.removeFileURL = '/Structure/RemoveFile';
    _self.OrderURL = '/_Data_Main_/Order';
    _self.editFileURL = '/_Data_Main_/EditFile';
    
    // action list
    _self.actionList = {
    };

    _self.init = function () {
        console.log('File Widget Inited');

        _self.finalFormData = new FormData();
        _self.callback = null;
        _self.URL = null;
        _self.EmployeeID = null;
        _self.content = null;
        _self.mode = null;
        _self.id = null;
        _self.PrisonerID = null;
        _self.type = null;
    };

    // ask action
    _self.Add = function (data, callback) {

        // initilize
        _self.init();

        console.log('File Add called');

        // save callback
        _self.callback = callback;

        _self.mode = 'Add';

        _self.URL = data.URL;

        _self.EmployeeID = data.EmployeeID;

        // loading view
        loadView(_self.modalUrl);
    };

    // ask action
    _self.Edit = function (data, callback) {

        // initilize
        _self.init();

        console.log('File Edit called');

        // save callback
        _self.callback = callback;

        _self.mode = 'Edit';

        _self.type = data.TypeID;

        _self.id = data.ID;

        // loading view
        loadView(_self.modalUrl);
    };

    // ask action
    _self.View = function (data, callback)
    {

        // initilize
        _self.init();

        console.log('File Edit called');

        // save callback
        _self.callback = callback;

        _self.mode = 'View';

        _self.type = data.TypeID;

        _self.id = data.ID;

        // loading view
        loadView(_self.viewUrl);
    };

    // remove file Action
    _self.Remove = function (data, callback) {

        // initilize
        _self.init();

        _self.type = data.TypeID;


        _core.getService('Loading').Service.enableBlur('loaderClass');

        // yes or no widget
        _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

            _core.getService('Loading').Service.disableBlur('loaderClass');

            if (res) {

                var url = _self.removeFileURL;
                
                _core.getService('Post').Service.postJson(data, url, function (res) {
                    if (res != null) {
                        callback(res);
                    }
                });
            }
        });
    }

    // loading modal from Views
    function loadView(url) {
            var data = {};

            if (_self.id != null && _self.mode == "Edit") {
                data.ID = _self.id;
            }

            _core.getService("Post").Service.postPartial(data, url, function (res) {

                _self.content = res;

                _core.getService('Loading').Service.disableBlur('loaderDiv');

                // content
                $('#widgetHolder').html(_self.content);

                // bind events for yes and no
                bindEvents();

                // showing modal
                $('.' + _self.ModalName).modal('show');
            });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnModal');
        var checkItems = $('.checkValidateModal');
        var dates = $('.DatesModal');
        _self.getItems = $('.getItemsModal');
        var propertyselect = $('#propertyselect');
        var addPropertyButton = $("#addPropertyButton");

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind hide show release data
        propertyselect.unbind().bind('change', function () {

            var value = propertyselect[0].selectize.getValue();

            if (value != "" && typeof value != "undefined" && value != null) {
                $(".propertyselectvalue").addClass("hidden");
                $("#propertyselectvaluediv" + value).removeClass("hidden");
            }
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == 'Add') {

                data.append("EmployeeID", _self.EmployeeID);

                // add
                addData(data);
            }
            else if (_self.mode == 'Edit') {

                data.LibID = _self.id;

                // edit
                editData(data);
            };

            //hiding modal
            $('.' + _self.ModalName).modal('hide');
        });

        // bind add property
        addPropertyButton.unbind().bind("click", function (e) {
            e.preventDefault();
            var data = {};
            var propertyselectSelectize = $("#propertyselect")[0].selectize;
            data.PropertyID = propertyselectSelectize.getValue();
            data.PropertyName = propertyselectSelectize.getItem(propertyselectSelectize.getValue())[0].innerHTML;

            var dataCustom = $('#propertyselectvalue' + data.PropertyID).data("custom");

            if (dataCustom != null && typeof dataCustom != "undefined" && dataCustom != "") {
                data.ValueName = $('#propertyselectvalue' + data.PropertyID).val();
                data.ValueID = $('#propertyselectvalue' + data.PropertyID).data("id");
                data.Custom = true;
            }
            else {
                var tempselect = $('#propertyselectvalue' + data.PropertyID)[0].selectize;
                data.ValueID = tempselect.getValue();
                data.ValueName = tempselect.getItem(tempselect.getValue())[0].innerHTML;
                data.Custom = false;
            }

            data.ValueID = (typeof data.ValueID == 'undefined' ? '' : data.ValueID);

            drawPropertyList(data);
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'ReleaseBasisLibItemID' || curName == 'ReleaseDate' || curName == 'ReleaseNote' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // selectize
        $(".selectizeModal").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // collect data
    function collectData() {

        // collect data
        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');
            if (name == "File") {
                _self.finalFormData.append(name, $(this)[0].files[0], $(this).val());
            }
            else {
                _self.finalFormData.append(name, $(this).val());
            }
        });

        return _self.finalFormData;
    }

    // add Data
    function addData(data) {

        var postService = _core.getService('Post').Service;

        postService.postFormData(data, _self.URL, function (res) {

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding Order');
        })
    }

}
/************************/


// creating class instance
var OrderWidget = new OrderWidgetClass();

// creating object
var OrderWidgetObject = {
    // important
    Name: 'Order',

    Widget: OrderWidget
}

// registering widget object
_core.addWidget(OrderWidgetObject);