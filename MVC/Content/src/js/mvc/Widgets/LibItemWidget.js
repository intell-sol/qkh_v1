﻿// Add LibItem widget

/*****Main Function******/
var LibItemWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.id = null;
    _self.mode = null;

    _self.modalUrl = "/_Popup_/Get_LibItem";
    _self.addUrl = '/Lists/AddLib';
    _self.editUrl = '/Lists/EditLib';
    _self.removeUrl = '/Lists/DelLib';
    _self.ModalName = 'libitem-modal';
    _self.finalData = {};
    
    _self.init = function () {
        console.log('Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.id = null;
        _self.mode = null;
    };

    // add action
    _self.Add = function (data, callback) {

        // initilize
        _self.init();

        console.log('Add called');

        _self.pathid = data.PathID;
        _self.libid = data.LibID;

        _self.mode = 'Add';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    }

    // edit action
    _self.Edit = function (data, callback) {
        // initilize
        _self.init();

        console.log('Edit called');

        _self.id = data.ID;
        _self.pathid = data.PathID;
        _self.libid = data.LibID;

        _self.mode = 'Edit';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // edit action
    _self.Remove = function (data, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = data.ID;

        // save callback
        _self.callback = callback;

        // remove
        removeData();
    };

    // loading modal from Views
    function loadView(url) {

        var data = {};

        if (_self.id != null && _self.mode == "Edit") {
            data.ID = _self.id;
        }

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _self.content = res;

            _core.getService('Loading').Service.disableBlur('loaderDiv');

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.'+_self.ModalName).modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnModal');
        var checkItems = $('.checkValidateModal');
        var dates = $('.DatesModal');
        _self.getItems = $('.getItemsModal');
        var propertyselect = $('#propertyselect');
        var addPropertyButton = $("#addPropertyButton");

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind hide show release data
        propertyselect.unbind().bind('change', function () {

            var value = propertyselect[0].selectize.getValue();
            
            if (value != "" && typeof value != "undefined" && value != null) {
                $(".propertyselectvalue").addClass("hidden");
                $("#propertyselectvaluediv" + value).removeClass("hidden");
            }
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == 'Add') {

                data.LibPathID = _self.pathid;
                data.LibToLibID = _self.libid;

                // add
                addData(data);
            }
            else if (_self.mode == 'Edit') {

                data.LibID = _self.id;

                // edit
                editData(data);
            };

            //hiding modal
            $('.'+_self.ModalName).modal('hide');
        });

        // bind add property
        addPropertyButton.unbind().bind("click", function (e) {
            e.preventDefault();
            var data = {};
            var propertyselectSelectize = $("#propertyselect")[0].selectize;
            data.PropertyID = propertyselectSelectize.getValue();
            data.PropertyName = propertyselectSelectize.getItem(propertyselectSelectize.getValue())[0].innerHTML;

            var dataCustom = $('#propertyselectvalue' + data.PropertyID).data("custom");

            if (dataCustom != null && typeof dataCustom != "undefined" && dataCustom != "") {
                data.ValueName = $('#propertyselectvalue' + data.PropertyID).val();
                data.ValueID = $('#propertyselectvalue' + data.PropertyID).data("id");
                data.Custom = true;
            }
            else {
                var tempselect = $('#propertyselectvalue' + data.PropertyID)[0].selectize;
                data.ValueID = tempselect.getValue();
                data.ValueName = tempselect.getItem(tempselect.getValue())[0].innerHTML;
                data.Custom = false;
            }

            data.ValueID = (typeof data.ValueID == 'undefined' ? '' : data.ValueID);

            drawPropertyList(data);
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'ReleaseBasisLibItemID' || curName == 'ReleaseDate' || curName == 'ReleaseNote' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // selectize
        $(".selectizeModal").each(function () {
            $(this).selectize({
                create: false
            });
        });

        // bind property list events
        bindPropertyListEvents();
    }

    // draw property list
    function drawPropertyList(data) {

        var curHtml = "<tr data-newitem='true' class='getPropertyListRow' data-custom='" + data.Custom + "' data-propertyid='" + data.PropertyID + "' data-propertyname='" + data.PropertyName + "' data-valueid='" + data.ValueID + "' data-valuename='" + data.ValueName + "' data-id=''>";
        curHtml += "<td>" + data.PropertyName + "</td>";
        curHtml += "<td>" + data.ValueName + "</td>";
        curHtml += '<td><button data-id="" class="btn btn-sm btn-default btn-flat removePropertyListRow" title="Հեռացնել"><i class="fa fa-trash-o" ></i></button></td>';
        curHtml += "</tr>";

        $('#propertyselectdraw').append(curHtml);

        // bind draw ContentList events
        bindPropertyListEvents();
    }
    
    // remove property from selected list
    function bindPropertyListEvents() {
        $('.removePropertyListRow').unbind().click(function (e) {
            e.preventDefault();

            $(this).parent().parent().remove();
        })
    }

    // add
    function addData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService("Loading").Service.enableBlur("loaderDiv");

        postService.postJson(data, _self.addUrl, function (res) {

            _core.getService("Loading").Service.disableBlur("loaderDiv");

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding');
        })
    }

    // edit
    function editData(data) {
        // post service
        var postService = _core.getService('Post').Service;

        _core.getService("Loading").Service.enableBlur("loaderDiv");

        postService.postJson(data, _self.editUrl, function (res) {

            _core.getService("Loading").Service.disableBlur("loaderDiv");

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing');
        })
    }

    // remove
    function removeData() {

        var data = {};
        data.LibID = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService("Loading").Service.enableBlur("loaderDiv");

        postService.postJson(data, _self.removeUrl, function (res) {

            _core.getService("Loading").Service.enableBlur("loaderDiv");

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on removing');
        })
    }

    // collect data
    function collectData() {

        // array of violation list
        //_self.finalData["ViolationList"] = [];

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            if (name == "ViolationList") {
                //var tempValue = $(this).val();
                //for (var i in tempValue) {
                //    var temp = {};
                //    temp["LibItemID"] = parseInt(tempValue[i]);
                //    _self.finalData[name].push(temp);
                //}
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }

            _self.finalData["propList"] = collectPropertyList();

        });

        return _self.finalData;
    }

    // collect property list
    function collectPropertyList() {
        var list = [];
        $(".getPropertyListRow").each(function () {

            var data = {};
            var PropsEntity = {};
            var PropValuesEntity = {};

            PropsEntity.ID = $(this).data("propertyid");
            PropsEntity.Fixed = !$(this).data("custom");
            PropsEntity.NewItem = $(this).data("newitem");
            PropValuesEntity.ID = $(this).data("valueid");
            PropValuesEntity.Value = $(this).data("valuename");

            data.property = PropsEntity;
            data.value = PropValuesEntity;

            list.push(data);
        });
        return list;
    }
}
/************************/


// creating class instance
var LibItemWidget = new LibItemWidgetClass();

// creating object
var LibItemWidgetObject = {
    // important
    Name: 'LibItem',

    Widget: LibItemWidget
}

// registering widget object
_core.addWidget(LibItemWidgetObject);