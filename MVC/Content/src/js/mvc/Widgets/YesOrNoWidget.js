﻿// yes or no widget

/*****Main Function******/
var YesOrNoWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.modal = $('.confirmDeleteModal');
    _self.modalUrl = "/_Popup_/Get_YesOrNO";
    _self.type = null;
    _self.ModalName = 'confirmDeleteModal';

    // action list
    _self.actionList = {
    };

    _self.init = function () {
        console.log('YesOrNo Widget Inited');
        _self.type = null;
    };

    // ask action
    _self.Ask = function (type, callback) {
        // initilize
        _self.init();
        _self.type = type;

        console.log('YesOrNo Ask called');

        // save callback
        _self.callback = callback;

        // loading view
        loadView();
    };

    // loading modal from Views
    function loadView() {

        var data = {};
        var url = _self.modalUrl;
        
        data.type = _self.type;

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _core.getService("Loading").Service.disableBlur("loaderDiv");
            _core.getService('Loading').Service.disableBlur('loaderClass');

            _self.content = res;

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.' + _self.ModalName).modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {
        var yesBtn = $('#acceptremove');
        var noBtn = $('#declineremove');

        // unbind
        yesBtn.unbind();
        noBtn.unbind();

        // bind
        yesBtn.bind('click', function () {
            // calling callback
            if (_self.callback != null)
                _self.callback(true);

            // remove callback
            _self.callback = null;

            //hiding modal
            $('.confirmDeleteModal').modal('hide');
        });

        noBtn.bind('click', function () {
            //hiding modal
            $('.confirmDeleteModal').modal('hide');

            // call callback
            _self.callback(false);

            // remove callback
            _self.callback = null;
        });
    }
}
/************************/


// creating class instance
var YesOrNoWidget = new YesOrNoWidgetClass();

// creating object
var YesOrNoWidgetObject = {
    // important
    Name: 'YesOrNo',

    Widget: YesOrNoWidget
}

// registering widget object
_core.addWidget(YesOrNoWidgetObject);