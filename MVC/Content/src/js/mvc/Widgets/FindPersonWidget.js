﻿// find person widget

/*****Main Function******/
var FindPersonWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.modal = $('.searchUserModal');
    _self.modalUrl = "/_Popup_/Get_FindPerson";
    _self.searchByDocUrl = "/Structure/CheckEmployeeByDocNumber";
    _self.searchByNameUrl = "/Structure/CheckEmployee";
    _self.idDocTableRowDrawURL = "/Structure/Draw_DocIdentity_Table_Row";
    _self.currentPerson = null;
    _self.searchCallback = null;
    _self.response = {};
    _self.response.status = false;
    _self.response.data = null;
    _self.DocIdentities = [];
    _self.menualSearch = false;
    _self.isBind = false;


    _self.init = function () {
        console.log('FindPerson Widget Inited');

        _self.menualSearch = $("#persondata").data("iscustom") == "True";;
        _self.DocIdentities = [];
        _self.response = {};
        _self.response.status = false;
        _self.response.data = null;
        _self.searchCallback = null;
        _self.isBind = false;

        bindIdDocTableRows();
    };

    // ask action
    _self.Show = function (callback) {

        // initilize
        _self.init();

        console.log('FindPerson Ask called');

        // save callback
        _self.callback = callback;

        // loading view
        loadView();
    };

    // bind person search
    _self.BindPersonSearch = function (callback) {

        // call init
        _self.init();

        _self.isBind = true;

        _self.searchCallback = callback;

        var findPersonBrn = $('#findperson');

        bindEvents();

        bindCollapseTable();
    }

    // loading modal from Views
    function loadView() {
        var data = {};

        if (_self.id != null && (_self.mode == "Edit" || _self.mode == "View")) {
            data.ID = _self.id;
        }

        var url = _self.modalUrl;

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _core.getService("Loading").Service.disableBlur("loaderDiv");
            _core.getService('Loading').Service.disableBlur('loaderClass');

            _self.content = res;

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.searchUserModal').modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons
        _self.getItemsPerson = $('.getItemsPerson');
        _self.getItemsDocs = $('.getItemsDocuments');
        _self.checkItemsPerson = $('.checkValidatePerson');
        var checkItemsDocs = $('.checkValidateDocuments');
        var docDate = $('.Dates');

        var findPersonBrn = $('#findperson');
        var findPersonAgainBtn = $('#findpersonAgain');
        var addDocBtn = $('#addDocidentityButton');
        var nextStepBtn = $('#nextStepButton');
        var prevStepBtn = $('#prevStepButton');
        var saveBtn = $('#userformsave');
        var menualBtn = $('#menualAddBtn');
        
        // searching for person
        findPersonBrn.unbind();
        findPersonBrn.bind('click', function () {
            
            _self.menualSearch = false;
            $('.partdetails').removeClass('hidden');
            $('.partcustom').addClass('hidden');
            $('.partcustomdetails').addClass('hidden');
            $('.partcustom').addClass('hidden');
            $('.partnationality').addClass('hidden');
            $('.partsex').addClass('hidden');
            saveBtn.attr('disabled', true);

            // find person function
            getPerson();
        });

        // save button
        saveBtn.unbind();
        saveBtn.bind('click', function () {
            if (_self.callback != null) {

                // call callback
                //_self.response.data = 
                //_self.response.menual = _self.menualSearch;
                //_self.response.status = true;

                _self.callback(_self.getPersonData());

                // making null
                _self.callback = null;
                _self.currentPerson = null;

                // hide modal
                $('.searchUserModal').modal('hide');
            }
            else {
                console.warn('currentPerson: ', _self.currentPerson);
                console.warn('callback: ', _self.callback);
            }
        });

        // validation of form
        _self.checkItemsPerson.unbind().bind('change keyup', function () {

            var action = !_self.isValidData();

            if (!action && _self.isBind) {
                _self.searchCallback(true);
            }
            else {

                saveBtn.attr("disabled", action);
            }
        });

        // validation of doc identity add
        checkItemsDocs.unbind().bind('change keyup', function () {

            var action = false;

            checkItemsDocs.each(function () {
                if (($(this).val() == '' || $(this).val() == null) && _self.menualSearch) {
                    action = true;
                }
            });

            if (_self.menualSearch) {
                addDocBtn.attr('disabled', action);
            }
        });

        // add doc identity
        addDocBtn.unbind().click(function () {

            // add docidentity
            addDocIdentity();

            // trigger change
            _self.checkItemsPerson.trigger('change');
        });

        // menual add person
        menualBtn.unbind().click(function () {

            _self.menualSearch = true;

            $('.partnationality').removeClass('hidden');
            $('.partsex').removeClass('hidden');
            $('.partcustom').removeClass('hidden');
            $('.partdetails').addClass('hidden');
            $('.partcustomdetails').removeClass('hidden');

            var status = false;

            $('#modal-person-name-1').attr('disabled', status);
            $('#modal-person-name-2').attr('disabled', status);
            $('#modal-person-name-3').attr('disabled', status);
            $('#modal-person-bd').attr('disabled', status);
            $('#modal-person-social-id').attr('disabled', status);
            $('#modal-person-passport').attr('disabled', status);

            $('#modal-person-name-1').trigger('change');
        })

        // find again person
        findPersonAgainBtn.unbind().click(function () {

            // clean search
            cleanSearch();

            if (_self.isBind)
            _self.searchCallback(false);

            // disable button
            saveBtn.attr('disabled', true);
        });

        // daterangepicker
        docDate.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        docDate.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
        });
        docDate.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
        });

        // selectize
        $(".selectizePerson").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // clean search
    function cleanSearch() {

        // hide main row for loader show (it is new user search)
        $('#foundusertable').addClass('hidden');
        $('#foundusererrorp').addClass('hidden');

        // disableing or enabling fields
        $('#modal-person-name-1').val('');
        $('#modal-person-name-2').val('');
        $('#modal-person-name-3').val('');
        $('#modal-person-bd').val('');
        $('#modal-person-social-id').val('');
        $('#modal-person-passport').val('');

        $('#modal-person-name-1').attr('disabled', false);
        $('#modal-person-name-2').attr('disabled', false);
        $('#modal-person-name-3').attr('disabled', false);
        $('#modal-person-bd').attr('disabled', false);
        $('#modal-person-social-id').attr('disabled', false);
        $('#modal-person-passport').attr('disabled', false);

        _self.currentPerson = null;

        _self.menualSearch = false;

        $('.partcustom').addClass('hidden');
        $('.partdetails').addClass('hidden');
        $('.partcustomdetails').addClass('hidden');
        $('.partnationality').addClass('hidden');
        $('.partsex').addClass('hidden');

        //_self.currentPersonalID = null;
    }

    // get person from police database
    function getPerson() {

        // data to send to server
        var data = {};

        // in search progress part
        searchProcessEvents();

        // loader
        var loadingService = _core.getService('Loading').Service;

        loadingService.enable('searchuserloader', 'foundusertable');

        // getting fields
        var firstname = $('#modal-person-name-1').val();
        var lastname = $('#modal-person-name-2').val();
        var bday = $('#modal-person-bd').val();
        var passport = $('#modal-person-passport').val();
        var socId = $('#modal-person-social-id').val();

        // first search by socid
        if (socId != '' && socId != null) {
            data['DocNumber'] = socId;
            data['Type'] = false;

            // search by doc
            searchPerson('doc', data);
        }
            // else search by pasport
        else if (passport != '' && passport != null) {
            data['DocNumber'] = passport;
            data['Type'] = true;

            // search by doc
            searchPerson('doc', data);
        }
            // then search by firstname lastname bday
        else if (firstname != '' && lastname != '' && bday != '') {
            data['FirstName'] = firstname;
            data['LastName'] = lastname;
            data['BirthDate'] = bday;

            // search by doc
            searchPerson('name', data);
        }
        else {
            // disable loader
            var loadingService = _core.getService('Loading').Service;
            loadingService.disable('searchuserloader', 'foundusertable', false);
        }
    }

    // check validate for binds from other modal
    _self.isValidData = function () {
        var action = true;

        _self.checkItemsPerson.each(function () {
            if (_self.menualSearch) {
                if (($(this).val() == '' || $(this).val() == null)) {
                    action = false;
                }
            }
            else {
                if (_self.currentPerson == null) action = false;
            }
        });

        return action;

    }

    // get person data for bindings from other modal
    _self.getPersonData = function () {
        var data = {};
        data['Person'] = collectData();
        data['menualSearch'] = data['Person']['isCustom'];
        //data['PhotoLink'] = data['Person']['PhotoLink'];
        return data;
    }

    // collect data
    function collectData() {

        var data = {};
        var hasID = $("#persondata").data("id") != "";

        // if data is from police
        if (!_self.menualSearch && !hasID) {
            data = _self.currentPerson;
        }
        // menul data
        else {
            _self.getItemsPerson.each(function () {

                // name of Field (same as in Entity)
                var name = $(this).attr('name');

                // appending to Data
                //if (name == "SexLibItem_ID") {
                //    debugger
                //    data["SexLibItem_ID"] = $(this).data('id');
                //    data["SexLibItem_ID"] = $(this).val();
                //}
                //else

                data[name] = $(this).val();
            });

            data.IdentificationDocuments = CollectDocIdentityData();
        }

        data['isCustom'] = _self.menualSearch;
        
        data.ID = $("#persondata").data("id");

        //if (_self.menualSearch) {
            var livingData = {};
            var livingStr = '';
            $('.getItemsLivAddress').each(function () {
                livingData[$(this).attr('name')] = $(this).val();
                livingStr = livingStr + $(this).val() + ' ';
            });

            var regData = {};
            var regStr = '';
            $('.getItemsRegAddress').each(function () {
                regData[$(this).attr('name')] = $(this).val();
                regStr = regStr + $(this).val() + ' ';
            });

            data['Registration_address'] = regStr;
            data['Living_place'] = livingStr;
            data['Registration_Entity'] = regData;
            data['Living_Entity'] = livingData;
        //}
        delete data['status'];
        data['Status'] = null;

        console.log(data);

        return data;
    }

    // add menual doc identity
    function addDocIdentity() {

        var citizenship = $('#family-partner-doc-citizenship')[0].selectize;
        var docidentityselect = $('#family-partner-doc-type')[0].selectize;

        var data = {};
        var url = _self.idDocTableRowDrawURL;

        _self.getItemsDocs.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            // appending to Data
            data[name] = $(this).val();
        });

        data.CitizenshipLibItem_Name = citizenship.getItem(citizenship.getValue())[0].innerHTML;
        data.TypeLibItem_Name = docidentityselect.getItem(docidentityselect.getValue())[0].innerHTML;

        data.ID = Math.floor((Math.random() * 100) + 1);
        data.Type = false;

        drawDocIdntityTableRow(data);
    }

    // draw docidentity table row
    function drawDocIdntityTableRow(data) {
        var curHtml = '<tr class="getManualDocIdentity" data-TypeLibItem_Name="' + data.TypeLibItem_Name + '" data-CitizenshipLibItem_Name="' + data.CitizenshipLibItem_Name + '" data-ID="" data-FromWhom="' + data.FromWhom + '" data-Date="' + data.Date + '" data-Number="' + data.Number + '" data-CitizenshipLibItem_ID="' + data.CitizenshipLibItem_ID + '" data-TypeLibItem_ID="' + data.TypeLibItem_ID + '">';
        curHtml += '<td>'+data.CitizenshipLibItem_Name+'</td>';
        curHtml += '<td>'+data.TypeLibItem_Name+'</td>';
            curHtml += '<td>'+data.Number+'</td>';
            curHtml += '<td>'+data.Date+'</td>';
            curHtml += '<td>'+data.FromWhom+'</td>';
            curHtml += '<td>';
            curHtml += '<button class="btn btn-sm btn-default btn-flat removeManualDocIdentity"><i class="fa fa-trash-o" title="Հեռացնել"></i></button>';
            curHtml += '</td>';
            curHtml += '</tr>';

            $('#menualDocIdentityTableBody').append(curHtml);

        bindIdDocTableRows();
    }

    // collect doc identity data
    function CollectDocIdentityData() {
        var list = [];

        $('.getManualDocIdentity').each(function () {
            var data = {};
            
            data.ID = $(this).data('id');
            data.FromWhom = $(this).data('fromwhom');
            data.Date = $(this).data('date');
            data.Number = $(this).data('number');
            data.CitizenshipLibItem_ID = $(this).data('citizenshiplibitem_id');
            data.CitizenshipLibItem_Name = $(this).data('citizenshiplibitem_name');
            data.TypeLibItem_ID = $(this).data('typelibitem_id');
            data.TypeLibItem_Name = $(this).data('typelibitem_name');

            list.push(data);
        });

        return list;
    }

    // events while searching for person
    function searchProcessEvents() {

        // hide main row for loader show (it is new user search)
        $('#foundusertable').addClass('hidden');
        $('#foundusererrorp').addClass('hidden');
    }

    // event after search
    function searchProcessAfterEvents(status) {
        if (status) {

            // toggle message table and <p>
            $('#foundusertable').removeClass('hidden');
            $('#foundusererrorp').addClass('hidden');

            //$('#menualAddBtn').attr('disabled', true);

        } 
        else {

            // current person is null
            _self.currentPerson = null;

            // hide table and show error <p>
            $('#foundusertable').addClass('hidden');
            $('#foundusererrorp').removeClass('hidden');
            $('#foundusererrorp').html('Որոնման արդյունքում ոչ մի անձ չի գտնվել');

            //$('#menualAddBtn').attr('disabled', false);

        }

        // disableing or enabling fields
        $('#modal-person-name-1').attr('disabled', status);
        $('#modal-person-name-2').attr('disabled', status);
        $('#modal-person-name-3').attr('disabled', status);
        $('#modal-person-bd').attr('disabled', status);
        $('#modal-person-social-id').attr('disabled', status);
        $('#modal-person-passport').attr('disabled', status);
    }

    // match data to table
    function matchPersonDataToTable(data) {

        // if user found
        if (typeof data != 'undefined' && data != null) {

            if (!data.Status) {

                // find again button toggle
                //$('#findpersonAgain').attr('disabled', false);

                // aftet search events
                searchProcessAfterEvents(false);
                if (_self.isBind) _self.searchCallback(false);
            }
            else if (data.Status) {

                // find again button toggle
                //$('#findpersonAgain').attr('disabled', true);

                // after search events
                searchProcessAfterEvents(true);

                // current person
                _self.currentPerson = data;

                // bind values to table
                drawUserList(data);

                $('.partcustom').removeClass("hidden");

                // draw fields
                drawUserFields(data, false);

                // bind collapse table info
                bindCollapseTable();
                if (_self.isBind) _self.searchCallback(true);
            }

            // disable loader
            var loadingService = _core.getService('Loading').Service;
            loadingService.disable('searchuserloader', 'foundusertable', false);
        }
        else {
            console.error('server returned null from server while searching for user')
        }
    }

    // draw user table row
    function drawUserList(user) {
        //console.log(user);
        var tbody = $('#searchuserinfoshort');

        // empty
        tbody.empty();

        curHtml = '';
        curHtml += '<tr><td data-title="#">';
        curHtml += '0</td>';
        curHtml += '<td data-title="Անուն Ազգանուն Հայրանուն">' + user.FirstName + ' ' + user.MiddleName + ' ' + user.LastName + '</td>';
        curHtml += '<td data-title="Ծննդ. ամսաթիվ">' + user.Birthday + '</td>';
        curHtml += '<td data-title="Սոց. քարտ">' + user.Personal_ID + '</td>';
        curHtml += '<td data-title="Անձնագիր">' + ((user.IdentificationDocuments.length != 0 ) ? user.IdentificationDocuments[0].Number : "") + '</td></tr>';

        var reg = user.Registration_Entity;
        var registerationData = reg.Registration_Region + ', ' + reg.Registration_Community + ' ' + (reg.Registration_Apartment != null ? reg.Registration_Apartment : "") + ', ' + reg.Registration_Street + ' ' + reg.Registration_Building_Type + ' ' + reg.Registration_Building;
        reg = user.Living_Entity;
        var livingData = reg.Registration_Region + ', ' + reg.Registration_Community + ' ' + (reg.Registration_Apartment != null ? reg.Registration_Apartment : "") + ', ' + reg.Registration_Street + ' ' + reg.Registration_Building_Type + ' ' + reg.Registration_Building;
        
        if (user.SexLibItem_ID == 153) user.GenderName = 'Իգական';
        else if (user.SexLibItem_ID == 154) user.GenderName = 'Արական';
        else user.GenderName = '';

        curHtml += ' <tr class="collapse"><td colspan="5"> <div class="row"> <div class="col-xs-12 col-md-4"><img class="img-responsive" src="' + user.PhotoLink + '" alt="" /></div> <div class="col-xs-12 col-md-8">';
        curHtml += ' <div class="form-group"> <label>Անձնագիր՝</label> <p>' + ((user.IdentificationDocuments.length != 0) ? user.IdentificationDocuments[0].Number : "") + ', ' + ((user.IdentificationDocuments.length != 0) ? user.IdentificationDocuments[0].Date : "") + ', ' + ((user.IdentificationDocuments.length != 0) ? user.IdentificationDocuments[0].FromWhom : "") + '</p> </div> <div class="form-group"> <label>Ազգություն՝</label> <p>' + user.NationalityLabel + '</p> </div> <div class="form-group"> <label>Քաղաքացիությւն՝</label> <p>' + user.Citizenship + '</p> </div> <div class="form-group"> <label>Սեռ՝</label> <p>' + user.GenderName + '</p> ';
        curHtml += '</div> <div class="form-group"> <label>Գրանցման հասցե՝</label> <p>' + registerationData + '</p> </div> <div class="form-group"> <label>Բնակության հասցե՝</label> <p>' + livingData + '</p> </div> </div> </div> </td> </tr>';

        tbody.append(curHtml);
    }


    // search person
    function searchPerson(type, data) {

        var url = null;
        if (type == 'doc') url = _self.searchByDocUrl;
        else if (type == 'name') url = _self.searchByNameUrl;

        // post service
        var postService = _core.getService('Post').Service;

        // sending to server and reciving answer
        postService.postJson(data, url, function (res) {
            matchPersonDataToTable(res);
        })
    }
    // draw user fields
    function drawUserFields(user, disabled) {

        // fiving values
        $('#modal-person-name-1').val(user.FirstName);
        $('#modal-person-name-2').val(user.LastName);
        $('#modal-person-name-3').val(user.MiddleName);
        $('#modal-person-bd').val(typeof user.Birthday != 'undefined' ? user.Birthday : user.BirthDay);
        $('#modal-person-social-id').val(typeof user.Personal_ID != 'undefined' ? user.Personal_ID : user.PSN);
        $('#modal-person-passport').val((user.IdentificationDocuments.length != 0) ? user.IdentificationDocuments[0].Number : "");

        $('#family-partner-sex')[0].selectize.setValue(user.SexLibItem_ID);
        $('#family-partner-nationality')[0].selectize.setValue(user.NationalityID);
        if (user.IdentificationDocuments.length != 0) {
            drawDocIdntityTableRow(user.IdentificationDocuments[0]);
        }

        $('#registrationRegionLiv').val((typeof user.Living_Entity != "undefined" && typeof user.Living_Entity.Registration_Region != " undefined") ? user.Living_Entity.Registration_Region : "");
        $('#registrationComLiv').val((typeof user.Living_Entity != "undefined" && typeof user.Living_Entity.Registration_Community != " undefined") ? user.Living_Entity.Registration_Community : "");
        $('#registrationStreetLiv').val((typeof user.Living_Entity != "undefined" && typeof user.Living_Entity.Registration_Street != " undefined") ? user.Living_Entity.Registration_Street : "");
        $('#registrationBuildingTypeLiv').val((typeof user.Living_Entity != "undefined" && typeof user.Living_Entity.Registration_Building != " undefined") ? user.Living_Entity.Registration_Building : "");
        $('#registrationBuildingLiv').val((typeof user.Living_Entity != "undefined" && typeof user.Living_Entity.Registration_Building_Type != " undefined") ? user.Living_Entity.Registration_Building_Type : "");
        $('#registrationAptLiv').val((typeof user.Living_Entity != "undefined" && typeof user.Living_Entity.Registration_Apartment != " undefined") ? user.Living_Entity.Registration_Apartment : "");

        $('#registrationRegionReg').val((typeof user.Registration_Entity != "undefined" && typeof user.Registration_Entity.Registration_Region != " undefined") ? user.Registration_Entity.Registration_Region : "");
        $('#registrationComReg').val((typeof user.Registration_Entity != "undefined" && typeof user.Registration_Entity.Registration_Community != " undefined") ? user.Registration_Entity.Registration_Community : "");
        $('#registrationStreetReg').val((typeof user.Registration_Entity != "undefined" && typeof user.Registration_Entity.Registration_Street != " undefined") ? user.Registration_Entity.Registration_Street : "");
        $('#registrationBuildingTypeReg').val((typeof user.Registration_Entity != "undefined" && typeof user.Registration_Entity.Registration_Building != " undefined") ? user.Registration_Entity.Registration_Building : "");
        $('#registrationBuildingReg').val((typeof user.Registration_Entity != "undefined" && typeof user.Registration_Entity.Registration_Building_Type != " undefined") ? user.Registration_Entity.Registration_Building_Type : "");
        $('#registrationAptReg').val((typeof user.Registration_Entity != "undefined" && typeof user.Registration_Entity.Registration_Apartment != " undefined") ? user.Registration_Entity.Registration_Apartment : "");

        // trigger change
        $('.checkValidatePerson').trigger('change');
    }

    // bind table collapse
    function bindCollapseTable() {

        // collapse table
        var collapseTable = $(".collapseTable");
        var collapseTableTr = collapseTable.find("tbody tr:not(.collapse)");

        collapseTableTr.click(function () {
            if ($(this).next("tr.collapse").hasClass("in")) {
                $(this).parents(".collapseTable").find("tr.collapse.in").removeClass("in");
            }
            else {
                $(this).parents(".collapseTable").find("tr.collapse.in").removeClass("in");
                $(this).parents(".collapseTable").find("input[name='modal-person-radio']").prop("checked", false);
                $(this).next("tr.collapse").addClass("in");
                $(this).find("input[name='modal-person-radio']").prop("checked", true);
            }
        });

        if (_self.searchCallback == null) {
            collapseTableTr.trigger('click');
        }
    }

    function bindIdDocTableRows() {

        var tableRow = $('.removeManualDocIdentity');

        tableRow.unbind().click(function () {

            $(this).parent().parent().remove();
        })
    }
}
/************************/


// creating class instance
var FindPersonWidget = new FindPersonWidgetClass();

// creating object
var FindPersonWidgetObject = {
    // important
    Name: 'FindPerson',

    Widget: FindPersonWidget
}

// registering widget object
_core.addWidget(FindPersonWidgetObject);