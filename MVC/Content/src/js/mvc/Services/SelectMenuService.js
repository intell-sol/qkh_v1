﻿// loading service

/*****Main Function******/
var SelectMenuServiceClass = function () {
    var _self = this;

    _self.valueList = {};
    _self.nameList = {};
    _self.LoadContentURL = "/_Popup_/Get_SentencingDataArticles";

    _self.Init = function () {
        _self.valueList = {};
        _self.nameList = {};
    }

    function bindEvents(name) {

        // call init
        //_self.Init();

        // bind change
        $('.customSelectInput'+name).unbind().bind('change', function () {

            //_self.valueList = {};
            //_self.nameList = {};

            var curName = $(this).attr("name");
            _self.valueList[curName] = [];
            _self.nameList[curName] = [];

            $(".customSelectInput" + name).each(function () {
                if ($(this).is(":checked")) {
                    var name = $(this).attr("name");
                    var newName = $(this).data("newname");
                    var value = $(this).val();
                    var dataName = $(this).data('name');

                    if (name == "SentencingDataArticles") {
                        dataName = '';
                        var arr = newName.split(';');
                        arr.splice(0, 1);
                        for (var i in arr) {
                            var newarr = arr[i];
                            var newarr = newarr.split('.');
                            var curValue = newarr[0];
                            dataName = dataName + curValue + ' ';
                        }
                    }

                    if (name != '' && name != null) {
                        if (typeof _self.valueList[name] == 'undefined') {
                            _self.valueList[name] = [];
                        }
                        if (typeof _self.nameList[name] == 'undefined') {
                            _self.nameList[name] = [];
                        }
                        _self.valueList[name].push(value);
                        _self.nameList[name].push(dataName);
                    }
                    else {
                        alert('serious error on custom select');
                    }
                }
            });

            // after generate name to its span
            var free = true;
            var currentName = '';
            if (typeof _self.nameList[curName] != 'undefined') {
                if (_self.nameList[curName].length == 0) {
                    var free = true
                    currentName = '<a class="mainNode selectItems" href="javascript:;" >Ընտրել</a>';
                }
                else {
                    for (var i in _self.nameList[curName]) {
                        free = false;
                        //if (i > 0 && _self.nameList[curName].length != i) {
                        //    currentName += ', ';
                        //}
                        currentName += '<a data-name="' + curName + '" data-value="' + _self.valueList[curName][i] + '" class="item selectItem" href="javascript:;" >' + _self.nameList[curName][i] + '</a>';
                    }
                }
            }
            if (free) currentName = '<a class="mainNode selectItems" href="javascript:;" >Ընտրել</a>';
            $('.customSelectNames' + curName).html(currentName);

            // bind delete event()
            bindDeleteEvent()
        });

        var openClass = $(".open");

        var customSelect1 = $("#customSelect" + name);
        var customSelect1Drop = $("#customSelect" + name + "Drop");

        customSelect1Drop.unbind();

        customSelect1Drop.on('click', function (e) {
            if (!$(this).hasClass('item'))
                customSelect1.parents(".customSelectWrap").toggleClass("open");
        });

        $(".custom-select-dropdown-in-body").find("button[data-toggle='collapse']").click(function () {
            $(this).parents(".custom-select-dropdown-in-body").animate({
                scrollTop: $(this).offset().bottom
            }, 300);
        });

        $("body").on("click", function (e) {
            if (!customSelect1.parents(".customSelectWrap").is(e.target) && customSelect1.parents(".customSelectWrap").has(e.target).length === 0 && openClass.has(e.target).length === 0) {
                customSelect1.parents(".customSelectWrap").removeClass("open");
            }
        });
    }

    function bindDeleteEvent() {
        $('.selectItem').unbind().bind('keyup', function (e) {
            if (e.keyCode == 8 || e.keyCode == 46) {
                var id = $(this).data('value');
                var curName = $(this).data('name');
                console.log($('#customSelectInput' + curName + id).val());
                $('#customSelectInput' + curName + id).prop('checked', false);
                $('#customSelectInput' + curName + id).trigger('change');
            }
        })
    }

    // bind events and draws selected names at first run
    _self.bindSelectEvents = function (name) {
        bindEvents(name);
        $('.customSelectInput'+name).trigger('change');
    }

    // load content
    _self.loadContent = function (ParentID, type, curData, callback) {

        var data = {};
        if (type == "prevData") {
            data["prevData"] = curData;
        }
        else if (type == "sentenceData") {
            data["sentenceData"] = curData;
        }
        else if (type == "arrestData") {
            data["arrestData"] = curData;
        }
        data["ParentID"] = ParentID;

        _core.getService('Post').Service.postPartial(data, _self.LoadContentURL, function (res) {
            var response = {};
            if (res != null) {
                $('.articlesHolder').empty();
                //var change = $('.articlesHolder').append(res);


                var change = $(res).appendTo(".articlesHolder");
                response.status = true;
            }
            else {
                response.status = false;
            }

            callback(response);
        })
    }

    _self.collectData = function (name) {
        if (name != null)
            collectDataWorker(name);
        if (typeof _self.valueList[name] != 'undefined') {
            return filterValues(_self.valueList[name]);
        }
    }

    _self.clearData = function (name) {
        if (name != null) {
            if (typeof _self.valueList[name] != 'undefined' && typeof _self.nameList[name] != 'undefined') {
                _self.valueList = [];
                _self.nameList = [];
                $('.customSelectNames' + name).html('');
            }
        }
        return true;
    }

    _self.collectFullData = function (name) {
        var list = [];
        if (name != null)
            collectDataWorker(name);
        if (typeof _self.valueList[name] != 'undefined' && typeof _self.nameList[name] != 'undefined') {
            var valueList = filterValues(_self.valueList[name]);
            var nameList = filterValues(_self.nameList[name]);
            
            if (valueList.length == nameList.length) {
                for (var i in valueList) {
                    var data = {};
                    data['ID'] = valueList[i];
                    data['Name'] = nameList[i];
                    list.push(data);
                }
            }
        }
        return list;
    }

    function filterValues(list) {
        var newList = [];
        for (var i in list) {
            var found = false;
            var curValue = list[i];

            for (var j in newList) {
                if (newList[j] == curValue) found = true;
            }
            if (!found) newList.push(curValue);
        }

        return newList;
    }

    function collectDataWorker(name) {
        $(".customSelectInput" + name).each(function () {
            if ($(this).is(":checked")) {
                var name = $(this).attr("name");
                var value = $(this).val();
                var dataName = $(this).data('name');

                if (name != '' && name != null) {
                    if (typeof _self.valueList[name] == 'undefined') {
                        _self.valueList[name] = [];
                    }
                    if (typeof _self.nameList[name] == 'undefined') {
                        _self.nameList[name] = [];
                    }
                    _self.valueList[name].push(value);
                    _self.nameList[name].push(dataName);
                }
                else {
                    alert('serious error on custom select');
                }
            }
        });
    }
}
/************************/


// creating class instance
var SelectMenuService = new SelectMenuServiceClass();

// creating object
var SelectMenuServiceObject = {
    // important
    Name: 'SelectMenu',

    Service: SelectMenuService
}

// registering controller object
_core.addService(SelectMenuServiceObject);