﻿// Date service

/*****Main Function******/
var DateServiceClass = function () {
    var _self = this;

    function init() {
        _self.today = new Date();
        _self.dd = _self.today.getDate();
        _self.mm = _self.today.getMonth() + 1;
        _self.yyyy = _self.today.getFullYear();
    }

    _self.CurrentDate = function () {
        init();
        return _self.dd + '/' + _self.mm + '/' + _self.yyyy;
    }

}
/************************/


// creating class instance
var DateService = new DateServiceClass();

// creating object
var DateServiceObject = {
    // important
    Name: 'Date',

    Service: DateService
}

// registering controller object
_core.addService(DateServiceObject);