﻿// Merge service

/*****Main Function******/
var MergeServiceClass = function () {
    var _self = this;
    _self.modalUrl = "/_Popup_/Get_MergeApprove";
    _self.approveUrl = '/Merge/ApproveData'
    _self.ModalName = "merge-approve-modal";

    function init() {

    }

    _self.bindPrisonerMerges = function (callback) {
        _self.callback = callback;
        bindEvents();
    }

    function bindEvents() {
        var mergeApproveRow = $('.mergeApproveRow');

        mergeApproveRow.unbind().bind('click', function () {
            var currentID = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            loadView(currentID);
        })
    }

    function loadView(currentID) {

        var data = {};

        data.ID = currentID;

        _core.getService("Post").Service.postPartial(data, _self.modalUrl, function (res) {

            _self.content = res;

            // content
            $('#widgetHolder').html(_self.content);

            // bind merge approve events
            bindMergeApproveEvents();

            _core.getService('Loading').Service.disableBlur('loaderClass');

            // showing modal
            $('.' + _self.ModalName).modal('show');
        });
    }

    function bindMergeApproveEvents() {
        var approveModalBtn = $('#approveModalBtn');
        var declineModalBtn = $('#declineModalBtn');

        approveModalBtn.unbind().bind('click', function () {
            var id = $(this).data('id');
            approveAnswer(id, true);
        });

        declineModalBtn.unbind().bind('click', function () {
            var id = $(this).data('id');
            approveAnswer(id, false);
        })
    }

    // approve or decline
    function approveAnswer(id, status) {
        var url = _self.approveUrl;
        var data = {};
        data['ID'] = id;
        data['State'] = status;
        _core.getService('Post').Service.postJson(data, url, function (res) {

            //hiding modal
            $('.' + _self.ModalName).modal('hide');

            _self.callback({});
        });
    }

}
/************************/


// creating class instance
var MergeService = new MergeServiceClass();

// creating object
var MergeServiceObject = {
    // important
    Name: 'Merge',

    Service: MergeService
}

// registering controller object
_core.addService(MergeServiceObject);