/**** BOOT PART ****/

window.onload = function () {
    // getting controller and action
    
    var currentControllerName = getControllerNameFromUrl();
    var currentActionName = getActionNameFromUrl();
    var currentId = getIdFromUrl();

    // load selectize no delete
    loadSelectizePlugins();

    // create jquery double click and single click fucntion
    jqueryDoubleClick();

    // methods
    function getControllerNameFromUrl() {
        var manJSCmon = location.pathname.split("/");
        //var local = location.protocol + '//' + location.host + "/" + manJSCmon[1] + "/" + manJSCmon[2];
        return manJSCmon[1] || null;
    }
    function getActionNameFromUrl() {
        var manJSCmon = location.pathname.split("/");
        //var local = location.protocol + '//' + location.host + "/" + manJSCmon[1] + "/" + manJSCmon[2];
        return manJSCmon[2] || null;
    }
    function getIdFromUrl() {
        var manJSCmon = location.pathname.split("/");
        //var local = location.protocol + '//' + location.host + "/" + manJSCmon[1] + "/" + manJSCmon[2];
        return manJSCmon[3] || null;
    }

    if (currentControllerName != null) {
        // default action name
        if (currentActionName == null) currentActionName = 'Index';

        //getting controller
        var currentControllerObject = _core.getController(currentControllerName);
        if (currentControllerObject != null) {
            var currentController = currentControllerObject.Controller;
            if (currentActionName == 'Index' || currentActionName == 'index') currentController.Index(); // calling Index
            else if (currentActionName == 'Add' || currentActionName == 'add') currentController.Add(PrisonerType[currentControllerName]); // calling add
            else if (currentActionName == 'Edit' || currentActionName == 'edit') currentController.Edit(PrisonerType[currentControllerName], currentId); // calling edit
            else if (currentActionName == 'Preview' || currentActionName == 'preview') currentController.View(PrisonerType[currentControllerName], currentId); // calling view
            else if (currentActionName == "Prisoner") currentController.Prisoner(currentId);
                
            // change tab
            changeTab(currentControllerName);
        }
        else {
            console.warn('no controller registered by name ', currentControllerName);
        }
    }
    else {
        console.warn('could not get controller or action name from pathname');
        $(document).keypress(function (e) {
            if (e.which == 13) {
                $('#loginBtn').click();
            }
        });
    }

    function changeTab(controllerName) {

        var menuTab = $('.menutab');

        menuTab.removeClass('selected');

        menuTab.each(function () {
            if ($(this).attr('name') == controllerName) {
                $(this).addClass('selected');
            }
        });

    }

    function loadSelectizePlugins() {

        Selectize.define('no-delete', function (options) {
            this.deleteSelection = function () { };
        });
    }

    function jqueryDoubleClick() {
    
        jQuery.fn.single_double_click = function (single_click_callback, double_click_callback, timeout) {
            return this.each(function () {
                var clicks = 0, self = this;
                jQuery(this).click(function (event) {
                    clicks++;
                    if (clicks == 1) {
                        setTimeout(function () {
                            if (clicks == 1) {
                                single_click_callback.call(self, event);
                            } else {
                                double_click_callback.call(self, event);
                            }
                            clicks = 0;
                        }, timeout || 300);
                    }
                });
            });
        }
    }

}