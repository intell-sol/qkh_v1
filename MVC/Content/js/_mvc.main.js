// Lists controller

/*****Main Function******/
var ListsControllerClass = function () {
    var _self = this;

    _self.getLibPathViewURL = "/_Popup_/Get_LibContentView";
    _self.getLibViewURL = "/_Popup_/Get_LibSelectedView";
    _self.ViewHolder = null;
    _self.WidgetHolder = null;
    _self.LibItemURL = "/_Popup_/Get_AddLibItem"

    _self.mode = "Add"; // initial mode

    _self.init = function () {
        console.log('List controller inited with action');

        _self.ViewHolder = $('#viewholder');
        _self.WidgetHolder = $('#widgetHolder');
        _self.PathID = null;
        _self.isPathElem = null;
        _self.LibID = null;

        onpopstate();
    }

    // index action
    _self.Index = function () {
        console.log('index called');

        _self.init();

        // bind lists events
        bindListElementEvents();

    }

    // bind List Events
    function bindListElementEvents() {
        var listFolderBtn = $('.listbuttons'); //> button:not([data-target="#lists"])  
        //var listSelectBtn = $(".listTreeFolderIn > a:not(.roleFolderAdd)");

        // bind list main elemtns click and double events
        listFolderBtn.on('click', function (target) {
            target.preventDefault();
            target.stopPropagation();
        });

        listFolderBtn.single_double_click(function (target) {
            // main working function

            var id = parseInt($(this).data('id'));
            var parentid = $(this).data('parentid');
            var isPathElem = $(this).hasClass("libpathelem");

            _self.isPathElem = isPathElem;
            _self.PathID = id;
            _self.LibID = parentid;

            // get current view
            getMainView();

            // push the state in router
            pushRoutingState();

        }, function () {
            var dataTarget = $(this).data('target');
            $(dataTarget).collapse('toggle');
        });

        //// edit role (on click draw employees and edit root)
        //listSelectBtn.on('click', function (target) {
        //    target.preventDefault();
        //    target.stopPropagation();
        //});
        //listSelectBtn.single_double_click(function (target) {
        //    var id = parseInt($(this).data('id'));
        //}, function () {
        //    var id = parseInt($(this).data('id'));
        //    var dataTarget = $(this).data('target');
        //    $(dataTarget).collapse('toggle');
        //});
    }

    // js bindings
    function bindButtons() {

    }

    // main View Router
    function getMainView() {

        if (_self.isPathElem) {

            // get path subitem list
            getLibPathList();
        }
        else {
            if (_self.PathID != null || _self.LibID != null) {

                // get current lib view
                getLibList();
            }
        }

        changeSelectedItem();
    }

    function changeSelectedItem() {

        $(".allSelectedElem").removeClass("selected");
        $(".selectedItem"+_self.PathID).addClass("selected");
    }

    // draw Lib path list
    function getLibPathList() {
        var ID = _self.PathID;
        var ParentID = _self.LibID;
        if (ID !== "" && ParentID !== "") {
            var data = {};
            data.ParentID = ID;
            var url = _self.getLibPathViewURL;
            loader(true);
            _core.getService("Post").Service.postPartial(data, url, function (res) {
                if (res !== null) {
                    _self.ViewHolder.empty().append(res);

                    bindLibListViewEvents();
                    loader(false);
                }
            })
        }
    }

    // lib list events
    function bindLibListViewEvents() {
        var gobackbutton = $("#gobackbutton");

        gobackbutton.unbind().click(function (e) {
            e.preventDefault();

            history.back();
        });
    }

    // lib events
    function bindLibEvents() {
        var addlibitem = $('#addlibitem');
        var editLibItem = $('.editLibItem');
        var removeLibItem = $('.removeLibItem');
        var LibItemIn = $('.LibItemIn');
        var gobackbutton = $("#gobackbutton");

        removeLibItem.unbind().click(function (e) {
            e.preventDefault();

            var ID = $(this).data("id");
            var data = {};
            data.ID = ID;
            var url = _self.LibItemURL;
            loader(true);

            // yes or no widget
            _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

                if (res) {

                    _core.getWidget("LibItem").Widget.Remove(data, function (res) {
                        if (res != null && res.status) {
                            getLibList();
                        }
                    });
                }
            })
        });

        editLibItem.unbind().click(function (e) {
            e.preventDefault();

            var ID = $(this).data("id");
            var data = {};
            data.ID = ID;
            var url = _self.LibItemURL;
            loader(true);
            _core.getWidget("LibItem").Widget.Edit(data, function (res) {
                if (res != null && res.status) {
                    getLibList();
                }
            });
        });

        addlibitem.unbind().click(function (e) {
            e.preventDefault();

            var PathID = $(this).data("pathid");
            var LibID = $(this).data("libid");
            var IsPath = $(this).data("ispath");
            var ID = $(this).data("id");
            var data = {};
            data.PathID = ((IsPath) ? PathID : null);
            data.ID = ID;
            data.LibID = LibID;
            var url = _self.LibItemURL;
            loader(true);
            _core.getWidget("LibItem").Widget.Add(data, function (res) {
                if (res != null && res.status) {
                    getLibList();
                }
            });
        });

        LibItemIn.unbind().click(function (e) {
            e.preventDefault();

            var data = {};

            //_self.PathID = $(this).data("pathid");
            _self.LibID = $(this).data("libid");
            _self.isPathElem = false;
            
            // get sub list
            getMainView();

            // push to history
            pushRoutingState();
        });

        gobackbutton.unbind().click(function (e) {
            e.preventDefault();

            history.back();
        });
    }

    // get lib item list
    function getLibList() {
        var data = {};
        data.LibID = _self.LibID;
        data.PathID = _self.PathID;
        var url = _self.getLibViewURL;
        loader(true);
        _core.getService("Post").Service.postPartial(data, url, function (res) {
            if (res !== null) {
                _self.ViewHolder.empty().append(res);

                bindLibEvents();
                loader(false);
            }
        })
    }

    // enable disable loader
    function loader(status) {
        if (status)
            _core.getService("Loading").Service.enableBlur("loaderDiv");
        else
            _core.getService("Loading").Service.disableBlur("loaderDiv");
    }

    // push state
    function pushRoutingState() {

        var stateData = {};
        stateData.isPathElem = _self.isPathElem;
        stateData.PathID = _self.PathID;
        stateData.LibID = _self.LibID;
        var page = "?page=" + ((typeof _self.LibID == "undefined") ? _self.PathID : _self.LibID);
        var title = "Դասակարգիչ" + _self.LibID;

        if (stateData.LibID != null || stateData.PathID != null)
            history.pushState(stateData, title, page);
    }

    // pop state part
    function onpopstate() {
        window.onpopstate = function (event) {

            _self.isPathElem = ((event.state == null) ? null : event.state.isPathElem);
            _self.PathID = ((event.state == null) ? null : event.state.PathID);
            _self.LibID = ((event.state == null) ? null : event.state.LibID);
            getMainView();
            //alert("location: " + document.location + ", state: " + JSON.stringify(event.state));
        };
    }
}
/************************/


// creating class instance
var ListsController = new ListsControllerClass();

// creating object
var ListsControllerObject = {
    // important
    Name: "Lists",

    Controller: ListsController
}

// registering controller object
_core.addController(ListsControllerObject);

// Structure controller

/*****Main Function******/
var StructureControllerClass = function () {
    var _self = this;

    _self.Person = null;
    _self.passMatch = false;
    _self.ViewHolder = null;
    _self.WidgetHolder = null;
    _self.isOrgUnit = null;
    _self.SelectedTypeID = null;
    _self.OrgUnitID = null;
    _self.canUpdateTree = true;
    _self.StructureTreeCollapseList = [];
    _self.getOrgUnitViewURL = "/_Popup_/Get_OrgUnitView";
    _self.getEmployeeViewURL = "/_Popup_/Get_EmployeeView";
    _self.AddOrgUnitURL = "/Structure/AddOrgUnit";
    _self.EditOrgUnitURL = "/Structure/UpdateOrgUnit";
    _self.RemOrgUnitURL = "/Structure/DeleteOrgUnit";
    _self.UpdateStructureTreeLevelURL = "/Structure/UpdateStructureTreeLevel";
    _self.OrgUnitURL = "/_Popup_/Draw_OrgUnit";
    _self.OrgUnitPositionURL = "/_Popup_/Draw_OrgUnitPosition";
    _self.GetStructureTreeURL = "/_Popup_/Get_StructureTree";
    _self.GetEmployeeListURL = "/_Popup_/GetEmployees";
    _self.AddEmployeeURL = "/Structure/AddEmployeeByDocNumber";
    _self.EditEmployeeURL = "/Structure/UpdateEmployee";
    _self.RemEmployeeURL = "/Structure/DeleteEmployee";
    _self.getStructurePositionTreeURL = "/_Popup_/GetStructurePositionTree";

    _self.mode = "Add"; // initial mode

    _self.init = function () {
        console.log('List controller inited with action');

        _self.ViewHolder = $('#viewholder');
        _self.WidgetHolder = $('#widgetHolder');
        _self.PathID = null;
        _self.isOrgUnit = null;
        _self.SelectedTypeID = null;
        _self.OrgUnitID = null;
        _self.isPathElem = null;
        _self.LibID = null;
        _self.Person = null;
        _self.StructureTreeCollapseList = (typeof $.cookie("structure-collapse-state") != "undefined") ? JSON.parse($.cookie("structure-collapse-state")) : [];

        //onpopstate();
    }

    // index action
    _self.Index = function () {
        console.log('index called');

        _self.init();

        // bind Events()
        bindEvents();

        // bind lists events
        bindSctructureElementEvents();

        // apply collapse state
        ApplyCollapseState();

    }

    // bind Structure Events
    function bindSctructureElementEvents() {
        var listbutton = $('.listbutton '); //> button:not([data-target="#lists"])  
        //var listSelectBtn = $(".listTreeFolderIn > a:not(.roleFolderAdd)");
        listbutton.unbind();
        // bind list main elemtns click and double events
        listbutton.on('click', function (target) {
            target.preventDefault();
            target.stopPropagation();
        });
        listbutton.single_double_click(function (target) {
            // main working function

            var id = $(this).data('id');
            var editable = $(this).data('editable') == "True";
            var typeid = $(this).data('typeid');
            var hassubitems = $(this).data('hassubitems');
            var parentid = $(this).data('parentid');

            _self.ID = id;
            _self.Editable = editable;
            _self.TypeID = typeid;
            _self.hasSubItems = hassubitems;
            _self.ParentID = parentid;

            // get current view
            getMainView();

            // push the state in router
            pushRoutingState();

        }, function () {
            var dataTarget = $(this).data('target');
            var id = $(this).data("id");
            var typeid = $(this).data("typeid");

            // save coolapse state of current orgunit structure
            var data = {};
            data.id = id;
            data.action = (!$('#' + id).hasClass('in')) ? "show" : "hide";

            var added = false;
            for (var i in _self.StructureTreeCollapseList) {
                if (_self.StructureTreeCollapseList[i].id == id) {
                    _self.StructureTreeCollapseList[i].action = data.action;
                    added = true;
                }
            }
            if (!added) {
                _self.StructureTreeCollapseList.push(data);
            }

            // hold in cookies
            $.cookie("structure-collapse-state", JSON.stringify(_self.StructureTreeCollapseList));

            // get employees if typeid =  4 (position org unit)
            if (typeid == "4" && data.action == "show") {
                getEmployeeList(id);
            }

            $(dataTarget).collapse('toggle');
        });

        //// edit role (on click draw employees and edit root)
        //listSelectBtn.on('click', function (target) {
        //    target.preventDefault();
        //    target.stopPropagation();
        //});
        //listSelectBtn.single_double_click(function (target) {
        //    var id = parseInt($(this).data('id'));
        //}, function () {
        //    var id = parseInt($(this).data('id'));
        //    var dataTarget = $(this).data('target');
        //    $(dataTarget).collapse('toggle');
        //});

        $(".managerTree > .managerTreeContent > .managerTreeFolder").sortable({
            handle: ".handle",
            scroll: false
        });
        $(".managerTree > .managerTreeContent > .managerTreeFolder .managerTreeFolderIn").each(function () {
            $(this).sortable({
                handle: ".handle",
                scroll: false
            });
            $(this).on('sortupdate', function () {
                
                if (_self.canUpdateTree) {

                    _core.getService("Loading").Service.enableBlur("structureTreeLoader"); // enable loader

                    _self.canUpdateTree = false;
                    var parentid = $(this).data('id');
                    if (typeof parentid != "undefined" && parentid != "" && parentid != null) {

                        var list = [];
                        var iterator = 0;
                        $(".dragElem" + parentid).each(function () {
                            var data = {};
                            data.ID = $(this).data('id');
                            data.SortLevel = iterator;
                            list.push(data);
                            iterator++;
                        })

                        var url = _self.UpdateStructureTreeLevelURL;

                        _core.getService("Post").Service.postJson(list, url, function (res) {

                            _core.getService("Loading").Service.disableBlur("structureTreeLoader"); // enable loader

                            getStructureTree(function (res) {
                                _self.canUpdateTree = true;
                            });
                        })

                    }
                };
            });
        })
    }

    // main View Router
    function getMainView() {

        getOrgUnitView(null, true, function () { });

        changeSelectedItem();
    }

    function changeSelectedItem() {

        $(".allSelectedElem").removeClass("selected");
        $(".selectedItem"+_self.PathID).addClass("selected");
    }

    // bind main events
    function bindEvents() {
        var buildTreeBtn = $("#buildTreeBtn");

        buildTreeBtn.unbind().click(function () {

            getStructurePositionTree();
        });
    }

    // draw orgunit view
    function getOrgUnitView(SelectedTypeID, isOrgUnit, callback) {

        var ID = _self.ID;
        var TypeID = _self.TypeID;
        var ParentID = _self.ParentID;
        _self.isOrgUnit = isOrgUnit;
        _self.SelectedTypeID = SelectedTypeID;

        var data = {};
        data.ID = ID;
        data.ParentID = ParentID;
        if (isOrgUnit) {
            data.TypeID = TypeID;
            data.SelectedTypeID = SelectedTypeID;
        }
        var url = _self.getOrgUnitViewURL;
        if (!isOrgUnit) {
            url = _self.getEmployeeViewURL;
        }

        loader(true);
        _core.getService("Post").Service.postPartial(data, url, function (res) {
            loader(false);

            if (res !== null) {
                _self.ViewHolder.empty().append(res);

                if (isOrgUnit) {
                    bindOrgUnitViewEvents();
                }
                else {
                    bindEmployeeViewEvents();
                }
            }

            callback();
        })
    }

    // get employee list for position org unit
    function getEmployeeList(id){
        if (typeof id != "undefined" && id != "" && id != null) {
            var data = {};
            data.OrgUnitID = id;
            var url = _self.GetEmployeeListURL;

            // get async
            _core.getService("Post").Service.postPartialAsync(data, url, function (res) {

                $(".employee" + id).remove();
                $("#" + id).prepend(res);

                // bind click events
                bindEmployeeEvents();
            })
        }
    }

    // OrgUnit events
    function bindOrgUnitViewEvents() {

        var editRootBtn = $("#editRootBtn");
        var declineEditRootBtn = $("#declineEditRootBtn");
        var removeRootBtn = $("#removeRootBtn");
        var saveRootBtn = $("#saveRootBtn");
        var orgUnitType = $("#add-coretype");
        var checkItems = $(".checkItems");

        if (_self.ID == null) {
            editRootBtn.addClass("hidden");
            declineEditRootBtn.addClass("hidden");
            removeRootBtn.addClass("hidden");
            saveRootBtn.removeClass("hidden");
        }
        else {
            editRootBtn.removeClass("hidden");
            declineEditRootBtn.addClass("hidden");
            removeRootBtn.addClass("hidden");
            saveRootBtn.addClass("hidden");
        }

        // bind button Events
        editRootBtn.unbind().bind("click", function (e) {
            e.preventDefault();

            // show other buttons
            editRootBtn.addClass("hidden");
            declineEditRootBtn.removeClass("hidden");
            removeRootBtn.removeClass("hidden");
            saveRootBtn.removeClass("hidden");

            // enable fields
            toggleFieldsDisable(false);
        })

        declineEditRootBtn.unbind().bind("click", function (e) {
            e.preventDefault();

            // show other buttons
            editRootBtn.removeClass("hidden");
            declineEditRootBtn.addClass("hidden");
            removeRootBtn.addClass("hidden");
            saveRootBtn.addClass("hidden");

            // load again
            getOrgUnitView(null, true, function () { });

            // enable fields
            //toggleFieldsDisable(true);
        });

        saveRootBtn.unbind().click(function (e) {
            e.preventDefault();

            var data = collectData();

            if (typeof data == "object") {
                saveRoot(data);
            }
        });

        removeRootBtn.unbind().click(function (e) {
            e.preventDefault();

            var id = _self.ID;
            if (typeof id != "undefined" && id != null && id != "") {
                removeRoot(id);
            }
        });

        // bind Validation
        bindValidation("checkItems", "#saveRootBtn"); 

        orgUnitType.bind('change', function () {

            var TypeID = orgUnitType.val();

            if (TypeID != "1001")
                getOrgUnitView(TypeID, true, function () { });
            else
                getOrgUnitView(TypeID, false, function () { })
        });

        // select read if write is selected
        $('.writeCheckBox').unbind().bind('change', function () {
            var id = $(this).data('id');

            if (typeof id != "undefined" && id != null && id != "") {
                if ($(this).is(":checked")) {
                    $('.permissionRead' + id).prop("checked", true);
                }
                else {
                    removePositionApprover(id);
                }
            }
        });

        // remove write if read is unselected
        $('.readCheckBox').unbind().bind('change', function () {
            var id = $(this).data('id');
            if (typeof id != "undefined" && id != null && id != "") {
                if (!$(this).is(":checked")) {
                    $('.permissionWrite' + id).prop("checked", false);
                    removePositionApprover(id);
                }
            }
        });

        // selectize
        $(".selectize").each(function () {
            if ($(this).is("select") && typeof $(this)[0].selectize == "undefined") {
                $(this).selectize({
                    create: false
                });
            }
        });

        // bind approve select
        bindApproveSelect();

        if (_self.ID != null) {
            // generate selected approvers names
            $(".getPermission").each(function () {
                var PositionID = $(this).data("id");

                if (typeof PositionID != "undefined" && PositionID != null && PositionID != "") {
                    GenerateApproversName(PositionID);
                }
            });

            // disable fields
            toggleFieldsDisable(true);
        }
    }

    // EmployeeView Events
    function bindEmployeeViewEvents() {

        var editEmployeeBtn = $("#editEmployeeBtn");
        var declineEditEmployeeBtn = $("#declineEditEmployeeBtn");
        var removeEmployeeBtn = $("#removeEmployeeBtn");
        var saveEmployeeBtn = $("#saveEmployeeBtn");
        var userPoliceDataSearch = $("#userPoliceDataSearch");
        var userPoliceDataUpdate = $("#userPoliceDataUpdate");
        var dates = $(".Dates");
        var orgUnitType = $("#add-coretype");
        var checkItems = $(".checkItems");
        var pass1 = $("#pass1");
        var pass2 = $("#pass2");

        if (_self.ID == null) {
            editEmployeeBtn.addClass("hidden");
            declineEditEmployeeBtn.addClass("hidden");
            removeEmployeeBtn.addClass("hidden");
            saveEmployeeBtn.removeClass("hidden");
        }
        else {
            editEmployeeBtn.removeClass("hidden");
            declineEditEmployeeBtn.addClass("hidden");
            removeEmployeeBtn.addClass("hidden");
            userPoliceDataUpdate.addClass("hidden");
            saveEmployeeBtn.addClass("hidden");
        }

        // bind button Events
        editEmployeeBtn.unbind().bind("click", function (e) {
            e.preventDefault();

            // show other buttons
            editEmployeeBtn.addClass("hidden");
            declineEditEmployeeBtn.removeClass("hidden");
            removeEmployeeBtn.removeClass("hidden");
            saveEmployeeBtn.removeClass("hidden");
            userPoliceDataUpdate.addClass("hidden");

            // enable fields
            toggleFieldsDisable(false);
        });

        declineEditEmployeeBtn.unbind().bind("click", function (e) {
            e.preventDefault();

            // show other buttons
            editEmployeeBtn.removeClass("hidden");
            declineEditEmployeeBtn.addClass("hidden");
            removeEmployeeBtn.addClass("hidden");
            saveEmployeeBtn.addClass("hidden");
            userPoliceDataUpdate.addClass("hidden");

            // load again
            getOrgUnitView(null, false, function () { });

            // enable fields
            //toggleFieldsDisable(true);
        });

        saveEmployeeBtn.unbind().click(function (e) {
            e.preventDefault();
            var data = collectEmployeeData();

            if (typeof data == "object") {
                saveEmployee(data);
            }
        });

        removeEmployeeBtn.unbind().click(function (e) {
            e.preventDefault();

            var id = _self.ID;
            if (typeof id != "undefined" && id != null && id != "") {
                removeEmployee(id);
            }
        });

        // police search bind
        userPoliceDataSearch.unbind().click(function () {

            loader(true);

            // call find persion widget
            var findPersionWidget = _core.getWidget('FindPerson').Widget;

            //run
            findPersionWidget.Show(function (res) {
                _self.Person = res.Person;
                matchPersonData(res.Person, res.menualSearch);
                _self.menualSearch = res.menualSearch;
                
            });
        });

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // tooltip
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'manual'
        });

        checkItems.unbind().bind("change keyup", function () {
            var block = false;

            checkItems.each(function () {
                var name = $(this).attr("name");

                if (typeof name != "undefined" && name != "" && $(this).val() == "" || $(this).val() == null) {
                    if (name == "File" || name == "Password") {
                        if (_self.ID == null) block = true;
                    }
                    else
                        block = true;
                }
            });

            if ((pass1.val() != "" || pass2.val() != "") && _self.ID == null) {
                if (pass1.val() != pass2.val()) {
                    block = true;
                    pass2.parent().tooltip('show');
                }
                else {
                    pass2.parent().tooltip('hide');
                }
            }

            saveEmployeeBtn.prop("disabled", block);
        });

        orgUnitType.bind('change', function () {

            var TypeID = orgUnitType.val();

            if (TypeID != "1001")
                getOrgUnitView(TypeID, true, function () { });
            else
                getOrgUnitView(TypeID, false, function () { })
        });

        // selectize
        $(".selectize").each(function () {
            if ($(this).is("select") && typeof $(this)[0].selectize == "undefined") {
                $(this).selectize({
                    create: false
                });
            }
        });

        // bind approve select
        bindApproveSelect();

        if (_self.ID != null) {
            // disable fields
            toggleFieldsDisable(true);
        }
    }

    // employe events
    function bindEmployeeEvents() {

        $(".employeeItem").unbind().click(function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            _self.ID = id;
            _self.OrgUnitID = $(this).data("orgunitid");

            getOrgUnitView(null, false, function () {

            })
        });
    }

    // validation of form
    function bindValidation(bindClass, toggleBtn) {
        var checkItems = $("." + bindClass);

        checkItems.unbind().bind("change keyup", function () {
            var block = false;

            checkItems.each(function () {
                var name = $(this).attr("name");

                if (typeof name != "undefined" && name != "" && $(this).val() == "" || $(this).val() == null) {
                    block = true;
                }
            });

            $(toggleBtn).prop("disabled", block);
        });
    };

    // enable disable loader
    function loader(status) {
        if (status)
            _core.getService("Loading").Service.enableBlur("loaderDiv");
        else
            _core.getService("Loading").Service.disableBlur("loaderDiv");
    }

    // push state
    function pushRoutingState() {

        var stateData = {};
        stateData.isPathElem = _self.isPathElem;
        stateData.PathID = _self.PathID;
        stateData.LibID = _self.LibID;
        var page = "?page=" + ((typeof _self.LibID == "undefined") ? _self.PathID : _self.LibID);
        var title = "Դասակարգիչ" + _self.LibID;

        if (stateData.LibID != null || stateData.PathID != null)
            history.pushState(stateData, title, page);
    }

    // pop state part
    function onpopstate() {
        window.onpopstate = function (event) {

            _self.isPathElem = ((event.state == null) ? null : event.state.isPathElem);
            _self.PathID = ((event.state == null) ? null : event.state.PathID);
            _self.LibID = ((event.state == null) ? null : event.state.LibID);
            getMainView();
            //alert("location: " + document.location + ", state: " + JSON.stringify(event.state));
        };
    }

    // match person data
    function matchPersonData(Person) {
        try {
            $("#user-person-name-1").val(Person.FirstName);
            $("#user-person-name-2").val(Person.LastName);
            $("#user-person-name-3").val(Person.MiddleName);
            $("#user-person-address-reg").val(Person.Registration_address);
            $("#user-person-bd").val(Person.Birthday);
            $("#user-person-social-id").val(Person.Personal_ID);
            $("#user-person-nationality").val(Person.NationalityLabel);
            $("#user-person-address-liv").val(Person.Living_place);
            $("#user-passport-number").val((typeof Person.IdentificationDocuments != "undefined" && Person.IdentificationDocuments != null && Person.IdentificationDocuments.length != 0) ? Person.IdentificationDocuments[0].Number : "");
            $("#user-passport-date").val((typeof Person.IdentificationDocuments != "undefined" && Person.IdentificationDocuments != null && Person.IdentificationDocuments.length != 0) ? Person.IdentificationDocuments[0].Date : "");
            $("#user-passport-issuer").val((typeof Person.IdentificationDocuments != "undefined" && Person.IdentificationDocuments != null && Person.IdentificationDocuments.length != 0) ? Person.IdentificationDocuments[0].FromWhom : "");

            if (Person.PhotoLink != "") $("#user-person-photolink").attr("src", Person.PhotoLink);
        }
        catch (e) {
            console.error(e)
        }
    }

    // bind dropdown watch
    function bindApproveSelect() {
        var approveSelect = $(".approveSelect");
        var body = $("body");
        var approveSelectDrop = $(".approveSelectDropInside");
        var openClass = $(".open");
        var approverCheckBox = $(".approverCheckBox");

        // bind <ul> <li> dropdown select
        approveSelectDrop.unbind().bind('click', function () {
            $(this).parent().toggleClass("open");
        });

        // close if out of div is clicked
        body.unbind();
        body.on("click", function (e) {
            if (!approveSelect.parents(".approveSelectWrap").is(e.target) && $(".approveSelectWrap").has(e.target).length === 0 && openClass.has(e.target).length === 0) {
                approveSelectDrop.parents(".approveSelectWrap").removeClass("open");
            }
        });

        // bind list change event
        approverCheckBox.unbind().change(function () {

            // generate name of select
            var PositionID = $(this).data('positionid');

            // generate approver's names
            GenerateApproversName(PositionID);
        })
    }

    // get structure tree
    function getStructurePositionTree() {

        var data = {};
        var url = _self.getStructurePositionTreeURL;
        loader(true);
        _core.getService("Post").Service.postPartial(data, url, function (res) {
            loader(false);
            _self.WidgetHolder.empty().append(res);

            bindStructurePositionTreeEvenets();

            $(".treeViewModal").modal("show");
        })
    }
    
    // bind structure position tree Events
    function bindStructurePositionTreeEvenets() {

        var treeWrap = $(".treeWrap");
        var treeInside = $(".treeInside");
        var tree = $(".tree");

        treeWrap.addClass("treeWrapHidden");

        $(".modal").on("show.bs.modal", function (e) {
            $("html").addClass("noScroll");
        });

        $(".modal").on("hide.bs.modal", function (e) {
            $("html").removeClass("noScroll");
        });

        $(".modal").on("shown.bs.modal", function (e) {
            treeInside.css("width", tree.width());
            treeWrap.scrollLeft((treeInside.width() - treeWrap.width()) / 2);
            treeWrap.removeClass("treeWrapHidden");
        });
    }

    // generate approver's names
    function GenerateApproversName(PositionID) {
        if (typeof PositionID != "undefined" && PositionID != "" && PositionID != null) {
            var approverNameItem = $(".approverNameItem" + PositionID);
            var defaultName = approverNameItem.data("defaultname");
            var ApproversName = "";

            $(".getApprover" + PositionID).each(function () {
                var currentName = $(this).data("name");
                if ($(this).is(":checked")) {
                    if (typeof currentName != "undefined" && currentName != null && currentName != "") {
                        if (ApproversName == "") ApproversName = currentName;
                        else ApproversName += ", " + currentName
                    }
                }
            });

            if (ApproversName == "") ApproversName = defaultName;
            approverNameItem.html(ApproversName);
        }
    }

    // remove position approver
    function removePositionApprover(PositionID) {
        if (typeof PositionID != "undefined" && PositionID != "" && PositionID != null) {
            $(".getApprover" + PositionID).each(function () {
                $(this).prop("checked", false);
            });

            // generate name again
            GenerateApproversName(PositionID);
        }
    }

    // toggle org unit fields
    function toggleFieldsDisable(disable){
        
        // field elems
        $('.disableFieldElem').each(function () {

            $(this).attr("disabled", disable);
        });

        // field elems
        $('.disableFieldSelect').each(function () {
            if (typeof $(this)[0].selectize != "undefined") {
                if (disable)
                    $(this)[0].selectize.disable();
                else
                    $(this)[0].selectize.enable();
            }
        });
    }

    // collect employee data
    function collectEmployeeData() {
        var data = new FormData();

        $(".getItems").each(function () { // get input's data
            var name = $(this).attr("name");
            if (name != "undefined" && name != null && name != "") {
                if (name == "File") {
                    data.append(name, $(this)[0].files[0], $(this).val());
                }
                else {
                    data.append(name, $(this).val());
                }
            }
        });

        // append person data
        if (_self.Person != null && typeof _self.Person == "object") {
            data.append("DocNumber", _self.Person.Personal_ID);
            data.append("Type", false);
        }

        // append OrgUnitID and ID
        //data.append("OrgUnitID", (_self.OrgUnitID == null) ? _self.ParentID : _self.OrgUnitID);
        data.append("OrgUnitID", _self.ParentID);
        data.append("ID", _self.ID);

        return data;
    }

    // collect Org Unit Data
    function collectData() {
        var data = {};
        $(".getItems").each(function () { // get input's data
            var name = $(this).attr("name");
            if (name != "undefined" && name != null && name != "") {
                data[name] = $(this).val();
            }
        });

        $(".getItemsSelect").each(function () { // get selectize data
            var name = $(this).attr("name");
            if (name != "undefined" && name != null && name != "") {
                var currentElem = $(this)[0].selectize;
                if (typeof currentElem != "undefined") {
                    if (name == "Single")
                        data[name] = currentElem.getValue() == "True";
                    else
                        data[name] = currentElem.getValue();
                }
            }
        });

        $(".getItemsSelectMulti").each(function () { // get multi selectize data
            var name = $(this).attr("name");
            if (name != "undefined" && name != null && name != "") {
                var currentElem = $(this)[0].selectize;
                if (typeof currentElem != "undefined") {
                    var values = currentElem.getValue();
                    if (values != null) {
                        var list = [];
                        for (var i in values) {
                            list.push(values[i]);
                        }
                        data[name] = list;
                    }
                }
            }
        });
        
        var Permissions = [];
        $(".getPermission").each(function () { // get permissions data
            var id = $(this).data("id");
            if (typeof id != "undefined" && id != null && id != "") {
                var isRead = $(".permissionRead" + id).is(":checked");
                var isWrite = $(".permissionWrite" + id).is(":checked");

                if (isRead) {
                    var PPType = 0;
                    var ID = id;
                    var Approvers = [];

                    if (isWrite) {
                        PPType = 1;

                        $(".getApprover" + id).each(function () {
                            if ($(this).is(":checked")) {
                                var ApproverID = $(this).data("id");
                                if (typeof ApproverID != "undefined" && ApproverID != null && ApproverID != "") {
                                    Approvers.push(ApproverID);
                                }
                            }
                        });

                        if (Approvers.length != 0)
                            PPType = 2;
                    }

                    var tempData = {};
                    tempData["ID"] = ID;
                    tempData["Approvers"] = Approvers;
                    tempData["PPType"] = PPType;

                    Permissions.push(tempData);
                }
            }
        });

        data["Permissions"] = Permissions;
        data["ParentID"] = _self.ParentID;
        data["ID"] = _self.ID;

        return data;
    }

    // save root
    function saveRoot(data) {
        
        var url = _self.AddOrgUnitURL;
        if (_self.ID != null)
            url = _self.EditOrgUnitURL;

        loader(true); // enable loader

        _core.getService("Post").Service.postJson(data, url, function (res) { // save on server
            loader(false); // disable loader
            if (typeof res != "undefined" && res.status) {

                _self.ID = res.id;
                getOrgUnitView(null, true, function () {

                    getStructureTree(function () { });
                });
            }
        });
    }

    // remove root
    function removeRoot(id) {
        var data = {};
        data.ID = id;

        var url = _self.RemOrgUnitURL;

        loader(true); // enable loader

        _core.getWidget("YesOrNo").Widget.Ask(1, function (res) {
            if (res) {

                loader(true); // enable loader
                
                _core.getService("Post").Service.postJson(data, url, function (res) { // save on server
                    loader(false); // disable loader
                    if (res != null) {
                        _self.ID = null;
                        
                        getOrgUnitView(null, true, function () {
                            getStructureTree(function () { });
                        });
                    }
                });
            }
        });
    }

    // save employee
    function saveEmployee(data) {

        var url = _self.AddEmployeeURL;
        if (_self.ID != null)
            url = _self.EditEmployeeURL;

        loader(true); // enable loader

        _core.getService("Post").Service.postFormData(data, url, function (res) { // save on server
            loader(false); // disable loader
            if (typeof res != "undefined" && res.status) {

                _self.ID = res.id;
                getOrgUnitView(null, false, function () {

                    getEmployeeList(_self.ParentID);
                    //getEmployeeList((_self.OrgUnitID == null) ? _self.ParentID : _self.OrgUnitID);
                });
            }
        });
    }

    function removeEmployee(id) {
        var data = {};
        data.EmployeeID = id;
        data.URL = _self.RemEmployeeURL;

        loader(true); // enable loader

        _core.getWidget("Order").Widget.Add(data, function (res) {
            if (res) {

                _self.ID = null;

                getOrgUnitView(null, false, function () {

                    getEmployeeList(_self.ParentID);
                    //getEmployeeList((_self.OrgUnitID == null) ? _self.ParentID : _self.OrgUnitID)
                });
            }
        });
    }

    // get Organization tree
    function getStructureTree(callback) {
        var data = {};
        var url = _self.GetStructureTreeURL;
        _core.getService("Loading").Service.enableBlur("structureTreeLoader"); // enable loader

        _core.getService("Post").Service.postPartial(data, url, function (res) { // save on server

            callback();

            if (res != null) {
                $("#structureTreeHolder").empty().append(res);

                // bind structure tree events
                bindSctructureElementEvents();

                ApplyCollapseState();

            }

            _core.getService("Loading").Service.disableBlur("structureTreeLoader"); // disable loader
        });
    }

    // apply collapse states which were before new load
    function ApplyCollapseState() {
        for (var i in _self.StructureTreeCollapseList) {
            var item = _self.StructureTreeCollapseList[i];

            if (typeof item.id != "undefined" && item.id != null && item.id != "") {
                if ($("#" + item.id).hasClass("in") && item.action == "hide")
                    $("#" + item.id).collapse(item.action);
                if (!$("#" + item.id).hasClass("in") && item.action == "show")
                    $("#" + item.id).collapse(item.action);
            }
        }
    }
}
/************************/


// creating class instance
var StructureController = new StructureControllerClass();

// creating object
var StructureControllerObject = {
    // important
    Name: "Structure",

    Controller: StructureController
}

// registering controller object
_core.addController(StructureControllerObject);

// Date service

/*****Main Function******/
var CalendarServiceClass = function () {
    var _self = this;

    _self.getDateTimeLocal = function(time) {
        return {
            "format": (time ? "DD/MM/YYYY HH:mm:ss" : "DD/MM/YYYY"),
            "separator": " - ",
            "applyLabel": "Հաստատել",
            "cancelLabel": "Մաքրել",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "daysOfWeek": [
                "Կրկ",
                "Երկ",
                "Երք",
                "Չրք",
                "Հնգ",
                "Ուր",
                "Շբթ"
            ],
            "monthNames": [
                "Հունվար",
                "Փետրվար",
                "Մարտ",
                "Ապրիլ",
                "Մայիս",
                "Հունիս",
                "Հուլիս",
                "Օգոստոս",
                "Սեպտեմբեր",
                "Հոկտեմբեր",
                "Նոյեմբեր",
                "Դեկտեմբեր"
            ],
            "firstDay": 1
        };
    }
}
/************************/


// creating class instance
var CalendarService = new CalendarServiceClass();

// creating object
var CalendarServiceObject = {
    // important
    Name: 'Calendar',

    Service: CalendarService
}

// registering controller object
_core.addService(CalendarServiceObject);

// Date service

/*****Main Function******/
var DateServiceClass = function () {
    var _self = this;

    function init() {
        _self.today = new Date();
        _self.dd = _self.today.getDate();
        _self.mm = _self.today.getMonth() + 1;
        _self.yyyy = _self.today.getFullYear();
    }

    _self.CurrentDate = function () {
        init();
        return _self.dd + '/' + _self.mm + '/' + _self.yyyy;
    }

}
/************************/


// creating class instance
var DateService = new DateServiceClass();

// creating object
var DateServiceObject = {
    // important
    Name: 'Date',

    Service: DateService
}

// registering controller object
_core.addService(DateServiceObject);

// loading service

/*****Main Function******/
var LoadingServiceClass = function () {
    var self = this;

    // enable loader
    self.enable = function (loaderDivClass, hideAndShowDivClass) {
        var elem = $('.' + loaderDivClass);
        var elemNew = $('.' + hideAndShowDivClass).addClass('hidden');
        elem.addClass('loading');
    }
    
    // disable loader
    self.disable = function (loaderDivClass, hideAndShowDivClass, hideAll) {
        var elem = $('.' + loaderDivClass);
        if (hideAll) $('.workingforms').addClass('hidden');
        var elemNew = $('.' + hideAndShowDivClass).removeClass('hidden');
        elem.removeClass('loading');
    }

    // disable all loader
    self.disableAll = function (loaderDivClass) {
        var elem = $('.' + loaderDivClass);
        elem.removeClass('loading');
    }

    // enable blur loader
    self.enableBlur = function (loaderDivClass) {
        var elemNew = $('.' + loaderDivClass);
        elemNew.addClass('blur');
        elemNew.addClass('loading');
    }

    // disable blur loader
    self.disableBlur = function (loaderDivClass) {
        var elemNew = $('.' + loaderDivClass);
        elemNew.removeClass('blur');
        elemNew.removeClass('loading');
    }

    // enable clear loader
    self.enableClear = function (loaderDivClass) {
        var elemNew = $('.' + loaderDivClass);
        elemNew.addClass('loading');
    }

    // disable clear loader
    self.disableClear = function (loaderDivClass) {
        var elemNew = $('.' + loaderDivClass);
        elemNew.removeClass('loading');
    }

    // enable clear loader
    self.enableClearByID = function (loaderDivID) {
        var elemNew = $('#' + loaderDivID);
        elemNew.addClass('loading');
    }

    // disable clear loader
    self.disableClearByID = function (loaderDivID) {
        var elemNew = $('#' + loaderDivID);
        elemNew.removeClass('loading');
    }
}
/************************/


// creating class instance
var LoadingService = new LoadingServiceClass();

// creating object
var LoadingServiceObject = {
    // important
    Name: 'Loading',

    Service: LoadingService
}

// registering controller object
_core.addService(LoadingServiceObject);

// Merge service

/*****Main Function******/
var MergeServiceClass = function () {
    var _self = this;
    _self.modalUrl = "/_Popup_/Get_MergeApprove";
    _self.approveUrl = '/Merge/ApproveData'
    _self.ModalName = "merge-approve-modal";

    function init() {

    }

    _self.bindPrisonerMerges = function (callback) {
        _self.callback = callback;
        bindEvents();
    }

    function bindEvents() {
        var mergeApproveRow = $('.mergeApproveRow');

        mergeApproveRow.unbind().bind('click', function () {
            var currentID = $(this).data('id');

            _core.getService('Loading').Service.enableBlur('loaderClass');

            loadView(currentID);
        })
    }

    function loadView(currentID) {

        var data = {};

        data.ID = currentID;

        _core.getService("Post").Service.postPartial(data, _self.modalUrl, function (res) {

            _self.content = res;

            // content
            $('#widgetHolder').html(_self.content);

            // bind merge approve events
            bindMergeApproveEvents();

            _core.getService('Loading').Service.disableBlur('loaderClass');

            // showing modal
            $('.' + _self.ModalName).modal('show');
        });
    }

    function bindMergeApproveEvents() {
        var approveModalBtn = $('#approveModalBtn');
        var declineModalBtn = $('#declineModalBtn');

        approveModalBtn.unbind().bind('click', function () {
            var id = $(this).data('id');
            approveAnswer(id, true);
        });

        declineModalBtn.unbind().bind('click', function () {
            var id = $(this).data('id');
            approveAnswer(id, false);
        })
    }

    // approve or decline
    function approveAnswer(id, status) {
        var url = _self.approveUrl;
        var data = {};
        data['ID'] = id;
        data['State'] = status;
        _core.getService('Post').Service.postJson(data, url, function (res) {

            //hiding modal
            $('.' + _self.ModalName).modal('hide');

            _self.callback({});
        });
    }

}
/************************/


// creating class instance
var MergeService = new MergeServiceClass();

// creating object
var MergeServiceObject = {
    // important
    Name: 'Merge',

    Service: MergeService
}

// registering controller object
_core.addService(MergeServiceObject);

// post service

/*****Main Function******/
var PostServiceClass = function () {
    var self = this;
    self.loaderWidget = null;
    self.divClass = 'loaderClass';
    self.partialXHRList = [];
    
    // Json Post With return Datatype json
    self.postJsonReturn = function (data, url, callback) {
        if (typeof url != 'undefined') {
            try {
                // enable loader
                if (data != null && typeof data['Description'] != 'undefined' && data['Description'] == '') data['Description'] = ' ';
                if (data != null && typeof data['Description'] != 'undefined' && data['Note'] == '') data['Note'] = ' ';
                self.loaderWidget = _core.getService('Loading').Service;
                self.loaderWidget.enableBlur(self.divClass);

                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'text',
                    beforeSend: function (xhr) { // for ASP.NET auto deserialization
                        xhr.setRequestHeader("Content-type", "application/json");
                    },
                    data: data,
                    statusCode: {
                        401: function () {
                            location.reload();
                        }
                    },
                    success: function (res, textStatus, xhr) {
                        // disable loader
                        self.loaderWidget = _core.getService('Loading').Service;
                        self.loaderWidget.disableBlur(self.divClass);

                        callback(JSON.parse(res));
                    },
                    error: function (xhr, textStatus, err) {
                        // disable loader
                        self.loaderWidget = _core.getService('Loading').Service;
                        self.loaderWidget.disableBlur(self.divClass);

                        callback(null);
                    }
                });
            } catch (err) {
                //console.log(err);
                callback(null);
            }
        }
        else {
            console.log('error', 'undefined parameter (plugins.post)');
            callback(null);
        }
    }

    // Json Post
    self.post = function (data, url, callback) {
        if (typeof url != 'undefined') {
            try {
                if (data != null && typeof data['Description'] != 'undefined' && data['Description'] == '') data['Description'] = ' ';
                if (data != null && typeof data['Description'] != 'undefined' && data['Note'] == '') data['Note'] = ' ';
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'text',
                    data: data,
                    statusCode: {
                        401: function () {
                            location.reload();
                        }
                    },
                    success: function (res, textStatus, xhr) {
                        callback(res);
                    },
                    error: function (xhr, textStatus, err) {
                        callback(null);
                    }
                });
            } catch (err) {
                //console.log(err);
                callback(null);
            }
        }
        else {
            console.log('error', 'undefined parameter (plugins.post)');
            callback(null);
        }
    }

    // Json Post
    self.postJson = function (data, url, callback) {
        if (typeof data != 'undefined' && typeof url != 'undefined') {
            try {
                if (data != null && typeof data['Description'] != 'undefined' && data['Description'] == '') data['Description'] = ' ';
                if (data != null && typeof data['Description'] != 'undefined' && data['Note'] == '') data['Note'] = ' ';
                // enable loader
                //self.loaderWidget = _core.getService('Loading').Service;
                //self.loaderWidget.enableBlur(self.divClass);

                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'text',
                    beforeSend: function (xhr) { // for ASP.NET auto deserialization
                        xhr.setRequestHeader("Content-type", "application/json");
                    },
                    data: JSON.stringify(data),
                    statusCode: {
                        401: function () {
                            location.reload();
                        }
                    },
                    success: function (res, textStatus, xhr) {
                        // disable loader
                        //self.loaderWidget = _core.getService('Loading').Service;
                        //self.loaderWidget.disableBlur(self.divClass);

                        callback(JSON.parse(res));
                    },
                    error: function (xhr, textStatus, err) {
                        // disable loader
                        self.loaderWidget = _core.getService('Loading').Service;
                        self.loaderWidget.disableBlur(self.divClass);

                        callback(null);
                    }
                });
            } catch (err) {
                //console.log(err);
                callback(null);
            }
        }
        else {
            console.log('error', 'undefined parameter (plugins.post)');
            callback(null);
        }
    }

    // post Partial
    self.postPartial = function (data, url, callback) {
        if (typeof data != 'undefined' && typeof url != 'undefined') {
            try {
                var xhr = $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'text',
                    beforeSend: function (xhr) { // for ASP.NET auto deserialization
                        xhr.setRequestHeader("Content-type", "application/json");
                    },
                    data: JSON.stringify(data),
                    statusCode: {
                        401: function () {
                            location.reload();
                        }
                    },
                    success: function (res, textStatus, xhr) {
                        // clean list
                        cleanPartialList();

                        // callback
                        callback(res);
                    },
                    error: function (xhr, textStatus, err) {
                        // clean list
                        cleanPartialList();

                        // callback
                        callback(null);
                    }
                });
                
                // clean list and push in new xhr
                cleanPartialList();
                self.partialXHRList.push(xhr);
            } catch (err) {
                //console.log(err);
                callback(null);
            }
        }
        else {
            console.log('error', 'undefined parameter (plugins.post)');
            callback(null);
        }
    }

    // post Partial async
    self.postPartialAsync = function (data, url, callback) {
        if (typeof data != 'undefined' && typeof url != 'undefined') {
            try {
                var xhr = $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'text',
                    beforeSend: function (xhr) { // for ASP.NET auto deserialization
                        xhr.setRequestHeader("Content-type", "application/json");
                    },
                    data: JSON.stringify(data),
                    statusCode: {
                        401: function () {
                            location.reload();
                        }
                    },
                    success: function (res, textStatus, xhr) {
                        // clean list
                        //cleanPartialList();

                        // callback
                        callback(res);
                    },
                    error: function (xhr, textStatus, err) {
                        // clean list
                        //cleanPartialList();

                        // callback
                        callback(null);
                    }
                });

                // clean list and push in new xhr
                //cleanPartialList();
                //self.partialXHRList.push(xhr);
            } catch (err) {
                //console.log(err);
                callback(null);
            }
        }
        else {
            console.log('error', 'undefined parameter (plugins.post)');
            callback(null);
        }
    }

    // FormData Post
    self.postFormData = function (data, url, callback) {
        if (typeof data != 'undefined' && typeof url != 'undefined') {
            try {
                if (data != null && typeof data['Description'] != 'undefined' && data['Description'] == '') data['Description'] = ' ';
                if (data != null && typeof data['Description'] != 'undefined' && data['Note'] == '') data['Note'] = ' ';
                // enable loader
                //self.loaderWidget = _core.getService('Loading').Service;
                //self.loaderWidget.enableBlur(self.divClass);

                $.ajax({
                    type: "POST",
                    url: url,
                    //processData: true,
                    processData: false,
                    contentType: false,
                    data: data,
                    //beforeSend: function (xhr) { // for ASP.NET auto deserialization
                    //    xhr.setRequestHeader("Content-type", "multipart/form-data");
                    //},
                    statusCode: {
                        401: function () {
                            location.reload();
                        }
                    },
                    success: function (res, textStatus, xhr) {
                        // disable loader
                        //self.loaderWidget = _core.getService('Loading').Service;
                        //self.loaderWidget.disableBlur(self.divClass);

                        callback(res);
                    },
                    error: function (xhr, textStatus, err) {
                        // disable loader
                        //self.loaderWidget = _core.getService('Loading').Service;
                        //self.loaderWidget.disableBlur(self.divClass);

                        console.log('error', err.responseText);
                        callback(null);
                    }
                });
            } catch (err) {
                //console.log(err);
                callback(null);
            }
        }
        else {
            console.log('error', 'undefined parameter (plugins.post)');
            callback(null);
        }
    }
    

    // clean partial list
    function cleanPartialList() {
        for (var i in self.partialXHRList) {
            self.partialXHRList[i].abort();
        }

        partialXHRList = [];
    }
}
/************************/


// creating class instance
var PostService = new PostServiceClass();

// creating object
var PostServiceObject = {
    // important
    Name: 'Post',

    Service: PostService
}

// registering controller object
_core.addService(PostServiceObject);

// loading service

/*****Main Function******/
var SelectMenuServiceClass = function () {
    var _self = this;

    _self.valueList = {};
    _self.nameList = {};
    _self.LoadContentURL = "/_Popup_/Get_SentencingDataArticles";

    _self.Init = function () {
        _self.valueList = {};
        _self.nameList = {};
    }

    function bindEvents(name) {

        // call init
        //_self.Init();

        // bind change
        $('.customSelectInput'+name).unbind().bind('change', function () {

            //_self.valueList = {};
            //_self.nameList = {};

            var curName = $(this).attr("name");
            _self.valueList[curName] = [];
            _self.nameList[curName] = [];

            $(".customSelectInput" + name).each(function () {
                if ($(this).is(":checked")) {
                    var name = $(this).attr("name");
                    var newName = $(this).data("newname");
                    var value = $(this).val();
                    var dataName = $(this).data('name');

                    if (name == "SentencingDataArticles") {
                        dataName = '';
                        var arr = newName.split(';');
                        arr.splice(0, 1);
                        for (var i in arr) {
                            var newarr = arr[i];
                            var newarr = newarr.split('.');
                            var curValue = newarr[0];
                            dataName = dataName + curValue + ' ';
                        }
                    }

                    if (name != '' && name != null) {
                        if (typeof _self.valueList[name] == 'undefined') {
                            _self.valueList[name] = [];
                        }
                        if (typeof _self.nameList[name] == 'undefined') {
                            _self.nameList[name] = [];
                        }
                        _self.valueList[name].push(value);
                        _self.nameList[name].push(dataName);
                    }
                    else {
                        alert('serious error on custom select');
                    }
                }
            });

            // after generate name to its span
            var free = true;
            var currentName = '';
            if (typeof _self.nameList[curName] != 'undefined') {
                if (_self.nameList[curName].length == 0) {
                    var free = true
                    currentName = '<a class="mainNode selectItems" href="javascript:;" >Ընտրել</a>';
                }
                else {
                    for (var i in _self.nameList[curName]) {
                        free = false;
                        //if (i > 0 && _self.nameList[curName].length != i) {
                        //    currentName += ', ';
                        //}
                        currentName += '<a data-name="' + curName + '" data-value="' + _self.valueList[curName][i] + '" class="item selectItem" href="javascript:;" >' + _self.nameList[curName][i] + '</a>';
                    }
                }
            }
            if (free) currentName = '<a class="mainNode selectItems" href="javascript:;" >Ընտրել</a>';
            $('.customSelectNames' + curName).html(currentName);

            // bind delete event()
            bindDeleteEvent()
        });

        var openClass = $(".open");

        var customSelect1 = $("#customSelect" + name);
        var customSelect1Drop = $("#customSelect" + name + "Drop");

        customSelect1Drop.unbind();

        customSelect1Drop.on('click', function (e) {
            if (!$(this).hasClass('item'))
                customSelect1.parents(".customSelectWrap").toggleClass("open");
        });

        $(".custom-select-dropdown-in-body").find("button[data-toggle='collapse']").click(function () {
            $(this).parents(".custom-select-dropdown-in-body").animate({
                scrollTop: $(this).offset().bottom
            }, 300);
        });

        $("body").on("click", function (e) {
            if (!customSelect1.parents(".customSelectWrap").is(e.target) && customSelect1.parents(".customSelectWrap").has(e.target).length === 0 && openClass.has(e.target).length === 0) {
                customSelect1.parents(".customSelectWrap").removeClass("open");
            }
        });
    }

    function bindDeleteEvent() {
        $('.selectItem').unbind().bind('keyup', function (e) {
            if (e.keyCode == 8 || e.keyCode == 46) {
                var id = $(this).data('value');
                var curName = $(this).data('name');
                console.log($('#customSelectInput' + curName + id).val());
                $('#customSelectInput' + curName + id).prop('checked', false);
                $('#customSelectInput' + curName + id).trigger('change');
            }
        })
    }

    // bind events and draws selected names at first run
    _self.bindSelectEvents = function (name) {
        bindEvents(name);
        $('.customSelectInput'+name).trigger('change');
    }

    // load content
    _self.loadContent = function (ParentID, type, curData, callback) {

        var data = {};
        if (type == "prevData") {
            data["prevData"] = curData;
        }
        else if (type == "sentenceData") {
            data["sentenceData"] = curData;
        }
        else if (type == "arrestData") {
            data["arrestData"] = curData;
        }
        data["ParentID"] = ParentID;

        _core.getService('Post').Service.postPartial(data, _self.LoadContentURL, function (res) {
            var response = {};
            if (res != null) {
                $('.articlesHolder').empty();
                //var change = $('.articlesHolder').append(res);


                var change = $(res).appendTo(".articlesHolder");
                response.status = true;
            }
            else {
                response.status = false;
            }

            callback(response);
        })
    }

    _self.collectData = function (name) {
        if (name != null)
            collectDataWorker(name);
        if (typeof _self.valueList[name] != 'undefined') {
            return filterValues(_self.valueList[name]);
        }
    }

    _self.clearData = function (name) {
        if (name != null) {
            if (typeof _self.valueList[name] != 'undefined' && typeof _self.nameList[name] != 'undefined') {
                _self.valueList = [];
                _self.nameList = [];
                $('.customSelectNames' + name).html('');
            }
        }
        return true;
    }

    _self.collectFullData = function (name) {
        var list = [];
        if (name != null)
            collectDataWorker(name);
        if (typeof _self.valueList[name] != 'undefined' && typeof _self.nameList[name] != 'undefined') {
            var valueList = filterValues(_self.valueList[name]);
            var nameList = filterValues(_self.nameList[name]);
            
            if (valueList.length == nameList.length) {
                for (var i in valueList) {
                    var data = {};
                    data['ID'] = valueList[i];
                    data['Name'] = nameList[i];
                    list.push(data);
                }
            }
        }
        return list;
    }

    function filterValues(list) {
        var newList = [];
        for (var i in list) {
            var found = false;
            var curValue = list[i];

            for (var j in newList) {
                if (newList[j] == curValue) found = true;
            }
            if (!found) newList.push(curValue);
        }

        return newList;
    }

    function collectDataWorker(name) {
        $(".customSelectInput" + name).each(function () {
            if ($(this).is(":checked")) {
                var name = $(this).attr("name");
                var value = $(this).val();
                var dataName = $(this).data('name');

                if (name != '' && name != null) {
                    if (typeof _self.valueList[name] == 'undefined') {
                        _self.valueList[name] = [];
                    }
                    if (typeof _self.nameList[name] == 'undefined') {
                        _self.nameList[name] = [];
                    }
                    _self.valueList[name].push(value);
                    _self.nameList[name].push(dataName);
                }
                else {
                    alert('serious error on custom select');
                }
            }
        });
    }
}
/************************/


// creating class instance
var SelectMenuService = new SelectMenuServiceClass();

// creating object
var SelectMenuServiceObject = {
    // important
    Name: 'SelectMenu',

    Service: SelectMenuService
}

// registering controller object
_core.addService(SelectMenuServiceObject);

// Validation service

/*****Main Function******/
var ValidationServiceClass = function () {
    var self = this;

    self.validate = function(action) {
        if (action == null || action) {
            alert('Խնդրում ենք լրացնել անհրաժաշտ բոլոր դաշտերը');
        }
    }
}
/************************/


// creating class instance
var ValidationService = new ValidationServiceClass();

// creating object
var ValidationServiceObject = {
    // important
    Name: 'Validation',

    Service: ValidationService
}

// registering controller object
_core.addService(ValidationServiceObject);

// AddFile widget

/*****Main Function******/
var AddFileWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.PrisonerID = null;
    _self.type = null;
    _self.id = null;
    _self.mode = null;
    _self.modalUrl = "/_Popup_/Get_AddFile";
    _self.viewUrl = "/_Popup_Views_/Get_AddFile";
    _self.finalFormData = new FormData();
    _self.removeFileURL = '/_Data_Main_/RemoveFile';
    _self.addFileURL = '/_Data_Main_/AddFile';
    _self.editFileURL = '/_Data_Main_/EditFile';
    
    // action list
    _self.actionList = {
    };

    _self.init = function () {
        console.log('File Widget Inited');

        _self.finalFormData = new FormData();
        _self.callback = null;
        _self.content = null;
        _self.mode = null;
        _self.id = null;
        _self.PrisonerID = null;
        _self.type = null;
    };

    // ask action
    _self.Add = function (data, callback) {

        // initilize
        _self.init();

        console.log('File Add called');

        // save callback
        _self.callback = callback;

        _self.mode = 'Add';

        _self.type = data.TypeID;

        _self.PrisonerID = data.PrisonerID;

        // loading view
        loadView(_self.modalUrl);
    };

    // ask action
    _self.Edit = function (data, callback) {

        // initilize
        _self.init();

        console.log('File Edit called');

        // save callback
        _self.callback = callback;

        _self.mode = 'Edit';

        _self.type = data.TypeID;

        _self.id = data.ID;

        // loading view
        loadView(_self.modalUrl);
    };

    // ask action
    _self.View = function (data, callback)
    {

        // initilize
        _self.init();

        console.log('File Edit called');

        // save callback
        _self.callback = callback;

        _self.mode = 'View';

        _self.type = data.TypeID;

        _self.id = data.ID;

        // loading view
        loadView(_self.viewUrl);
    };

    // remove file Action
    _self.Remove = function (data, callback) {

        // initilize
        _self.init();

        _self.type = data.TypeID;


        _core.getService('Loading').Service.enableBlur('loaderClass');

        // yes or no widget
        _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

            _core.getService('Loading').Service.disableBlur('loaderClass');

            if (res) {

                var url = _self.removeFileURL;
                
                _core.getService('Post').Service.postJson(data, url, function (res) {
                    if (res != null) {
                        callback(res);
                    }
                });
            }
        });
    }

    // loading modal from Views
    function loadView(url) {
            if (_self.id != null) url = url + '/' + _self.id;

            _core.getService('Loading').Service.enableBlur('loaderClass');

            $.get(url, function (res) {

                _core.getService('Loading').Service.disableBlur('loaderClass');

                // save content for later usage
                _self.content = res;

                // content
                $('#widgetHolder').html(_self.content);

                // bind events for yes and no
                bindEvents();

                // showing modal
                $('.add-file-modal').modal('show');
            }, 'html');
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var addFileToggle = $('#add-file-toggle');
        var acceptBtn = $('#acceptBtn');
        var checkItems = $('.checkValidateAddFile');
        var fileName = $('#filename');
        _self.getItems = $('.getItemsFile');

        // unbind
        addFileToggle.unbind();
        acceptBtn.unbind();

        // bind file change
        addFileToggle.bind('change', function () {
            _self.finalFormData.delete('fileImage');
            _self.finalFormData.append('fileImage', $(this)[0].files[0], $(this).val());

            // change filename fields and trigger change
            fileName.val($(this).val());
            fileName.html($(this).val());
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            // adding file
            if (_self.id == null) {

                // default add file
                var url = _self.addFileURL;

                // append prisonerid and type
                data.append('PrisonerID', _self.PrisonerID);
                data.append('TypeID', _self.type);
            }
            else {

                // default edit file
                url = _self.editFileURL;

                // append id
                data.append('ID', _self.id);
            }

            //// log it
            //for (var pair of data.entries()) {
            //    console.log(pair[0] + ', ' + pair[1]);
            //}

            var curClass = '';
            if (typeof _self.type == 'undefined') curClass = 'fileLoader';
            else {
                if (parseInt(_self.type) == 1) curClass = 'imageLoader';
                else if (parseInt(_self.type) == 8) curClass = 'fileSentenceLoader';
                else curClass = 'fileLoader';
            }

            _core.getService('Loading').Service.enableBlur(curClass);

            // post service
            var postService = _core.getService('Post').Service;

            postService.postFormData(data, url, function (res) {

                _core.getService('Loading').Service.disableBlur(curClass);

                if (res != null) {
                    _self.callback(res);
                }
                else alert('serious eror on adding file');

            })

            //hiding modal
            $('.add-file-modal').modal('hide');
        });

        // check validate and toggle accept button
        checkItems.bind('change', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // selectize
        $(".selectizeAddFile").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // collect data
    function collectData() {

        // collect data
        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            _self.finalFormData.delete(name);
            _self.finalFormData.append(name, $(this).val());

        });

        return _self.finalFormData;
    }

}
/************************/


// creating class instance
var AddFileWidget = new AddFileWidgetClass();

// creating object
var AddFileWidgetObject = {
    // important
    Name: 'AddFile',

    Widget: AddFileWidget
}

// registering widget object
_core.addWidget(AddFileWidgetObject);

// find person widget

/*****Main Function******/
var FindPersonWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.modal = $('.searchUserModal');
    _self.modalUrl = "/_Popup_/Get_FindPerson";
    _self.searchByDocUrl = "/Structure/CheckEmployeeByDocNumber";
    _self.searchByNameUrl = "/Structure/CheckEmployee";
    _self.idDocTableRowDrawURL = "/Structure/Draw_DocIdentity_Table_Row";
    _self.currentPerson = null;
    _self.searchCallback = null;
    _self.response = {};
    _self.response.status = false;
    _self.response.data = null;
    _self.DocIdentities = [];
    _self.menualSearch = false;
    _self.isBind = false;


    _self.init = function () {
        console.log('FindPerson Widget Inited');

        _self.menualSearch = $("#persondata").data("iscustom") == "True";;
        _self.DocIdentities = [];
        _self.response = {};
        _self.response.status = false;
        _self.response.data = null;
        _self.searchCallback = null;
        _self.isBind = false;

        bindIdDocTableRows();
    };

    // ask action
    _self.Show = function (callback) {

        // initilize
        _self.init();

        console.log('FindPerson Ask called');

        // save callback
        _self.callback = callback;

        // loading view
        loadView();
    };

    // bind person search
    _self.BindPersonSearch = function (callback) {

        // call init
        _self.init();

        _self.isBind = true;

        _self.searchCallback = callback;

        var findPersonBrn = $('#findperson');

        bindEvents();

        bindCollapseTable();
    }

    // loading modal from Views
    function loadView() {
        var data = {};

        if (_self.id != null && (_self.mode == "Edit" || _self.mode == "View")) {
            data.ID = _self.id;
        }

        var url = _self.modalUrl;

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _core.getService("Loading").Service.disableBlur("loaderDiv");
            _core.getService('Loading').Service.disableBlur('loaderClass');

            _self.content = res;

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.searchUserModal').modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons
        _self.getItemsPerson = $('.getItemsPerson');
        _self.getItemsDocs = $('.getItemsDocuments');
        _self.checkItemsPerson = $('.checkValidatePerson');
        var checkItemsDocs = $('.checkValidateDocuments');
        var docDate = $('.Dates');

        var findPersonBrn = $('#findperson');
        var findPersonAgainBtn = $('#findpersonAgain');
        var addDocBtn = $('#addDocidentityButton');
        var nextStepBtn = $('#nextStepButton');
        var prevStepBtn = $('#prevStepButton');
        var saveBtn = $('#userformsave');
        var menualBtn = $('#menualAddBtn');
        
        // searching for person
        findPersonBrn.unbind();
        findPersonBrn.bind('click', function () {
            
            _self.menualSearch = false;
            $('.partdetails').removeClass('hidden');
            $('.partcustom').addClass('hidden');
            $('.partcustomdetails').addClass('hidden');
            $('.partcustom').addClass('hidden');
            $('.partnationality').addClass('hidden');
            $('.partsex').addClass('hidden');
            saveBtn.attr('disabled', true);

            // find person function
            getPerson();
        });

        // save button
        saveBtn.unbind();
        saveBtn.bind('click', function () {
            if (_self.callback != null) {

                // call callback
                //_self.response.data = 
                //_self.response.menual = _self.menualSearch;
                //_self.response.status = true;

                _self.callback(_self.getPersonData());

                // making null
                _self.callback = null;
                _self.currentPerson = null;

                // hide modal
                $('.searchUserModal').modal('hide');
            }
            else {
                console.warn('currentPerson: ', _self.currentPerson);
                console.warn('callback: ', _self.callback);
            }
        });

        // validation of form
        _self.checkItemsPerson.unbind().bind('change keyup', function () {

            var action = !_self.isValidData();

            if (!action && _self.isBind) {
                _self.searchCallback(true);
            }
            else {

                saveBtn.attr("disabled", action);
            }
        });

        // validation of doc identity add
        checkItemsDocs.unbind().bind('change keyup', function () {

            var action = false;

            checkItemsDocs.each(function () {
                if (($(this).val() == '' || $(this).val() == null) && _self.menualSearch) {
                    action = true;
                }
            });

            if (_self.menualSearch) {
                addDocBtn.attr('disabled', action);
            }
        });

        // add doc identity
        addDocBtn.unbind().click(function () {

            // add docidentity
            addDocIdentity();

            // trigger change
            _self.checkItemsPerson.trigger('change');
        });

        // menual add person
        menualBtn.unbind().click(function () {

            _self.menualSearch = true;

            $('.partnationality').removeClass('hidden');
            $('.partsex').removeClass('hidden');
            $('.partcustom').removeClass('hidden');
            $('.partdetails').addClass('hidden');
            $('.partcustomdetails').removeClass('hidden');

            var status = false;

            $('#modal-person-name-1').attr('disabled', status);
            $('#modal-person-name-2').attr('disabled', status);
            $('#modal-person-name-3').attr('disabled', status);
            $('#modal-person-bd').attr('disabled', status);
            $('#modal-person-social-id').attr('disabled', status);
            $('#modal-person-passport').attr('disabled', status);

            $('#modal-person-name-1').trigger('change');
        })

        // find again person
        findPersonAgainBtn.unbind().click(function () {

            // clean search
            cleanSearch();

            if (_self.isBind)
            _self.searchCallback(false);

            // disable button
            saveBtn.attr('disabled', true);
        });

        // daterangepicker
        docDate.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        docDate.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
        });
        docDate.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
        });

        // selectize
        $(".selectizePerson").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // clean search
    function cleanSearch() {

        // hide main row for loader show (it is new user search)
        $('#foundusertable').addClass('hidden');
        $('#foundusererrorp').addClass('hidden');

        // disableing or enabling fields
        $('#modal-person-name-1').val('');
        $('#modal-person-name-2').val('');
        $('#modal-person-name-3').val('');
        $('#modal-person-bd').val('');
        $('#modal-person-social-id').val('');
        $('#modal-person-passport').val('');

        $('#modal-person-name-1').attr('disabled', false);
        $('#modal-person-name-2').attr('disabled', false);
        $('#modal-person-name-3').attr('disabled', false);
        $('#modal-person-bd').attr('disabled', false);
        $('#modal-person-social-id').attr('disabled', false);
        $('#modal-person-passport').attr('disabled', false);

        _self.currentPerson = null;

        _self.menualSearch = false;

        $('.partcustom').addClass('hidden');
        $('.partdetails').addClass('hidden');
        $('.partcustomdetails').addClass('hidden');
        $('.partnationality').addClass('hidden');
        $('.partsex').addClass('hidden');

        //_self.currentPersonalID = null;
    }

    // get person from police database
    function getPerson() {

        // data to send to server
        var data = {};

        // in search progress part
        searchProcessEvents();

        // loader
        var loadingService = _core.getService('Loading').Service;

        loadingService.enable('searchuserloader', 'foundusertable');

        // getting fields
        var firstname = $('#modal-person-name-1').val();
        var lastname = $('#modal-person-name-2').val();
        var bday = $('#modal-person-bd').val();
        var passport = $('#modal-person-passport').val();
        var socId = $('#modal-person-social-id').val();

        // first search by socid
        if (socId != '' && socId != null) {
            data['DocNumber'] = socId;
            data['Type'] = false;

            // search by doc
            searchPerson('doc', data);
        }
            // else search by pasport
        else if (passport != '' && passport != null) {
            data['DocNumber'] = passport;
            data['Type'] = true;

            // search by doc
            searchPerson('doc', data);
        }
            // then search by firstname lastname bday
        else if (firstname != '' && lastname != '' && bday != '') {
            data['FirstName'] = firstname;
            data['LastName'] = lastname;
            data['BirthDate'] = bday;

            // search by doc
            searchPerson('name', data);
        }
        else {
            // disable loader
            var loadingService = _core.getService('Loading').Service;
            loadingService.disable('searchuserloader', 'foundusertable', false);
        }
    }

    // check validate for binds from other modal
    _self.isValidData = function () {
        var action = true;

        _self.checkItemsPerson.each(function () {
            if (_self.menualSearch) {
                if (($(this).val() == '' || $(this).val() == null)) {
                    action = false;
                }
            }
            else {
                if (_self.currentPerson == null) action = false;
            }
        });

        return action;

    }

    // get person data for bindings from other modal
    _self.getPersonData = function () {
        var data = {};
        data['Person'] = collectData();
        data['menualSearch'] = data['Person']['isCustom'];
        //data['PhotoLink'] = data['Person']['PhotoLink'];
        return data;
    }

    // collect data
    function collectData() {

        var data = {};
        var hasID = $("#persondata").data("id") != "";

        // if data is from police
        if (!_self.menualSearch && !hasID) {
            data = _self.currentPerson;
        }
        // menul data
        else {
            _self.getItemsPerson.each(function () {

                // name of Field (same as in Entity)
                var name = $(this).attr('name');

                // appending to Data
                //if (name == "SexLibItem_ID") {
                //    debugger
                //    data["SexLibItem_ID"] = $(this).data('id');
                //    data["SexLibItem_ID"] = $(this).val();
                //}
                //else

                data[name] = $(this).val();
            });

            data.IdentificationDocuments = CollectDocIdentityData();
        }

        data['isCustom'] = _self.menualSearch;
        
        data.ID = $("#persondata").data("id");

        //if (_self.menualSearch) {
            var livingData = {};
            var livingStr = '';
            $('.getItemsLivAddress').each(function () {
                livingData[$(this).attr('name')] = $(this).val();
                livingStr = livingStr + $(this).val() + ' ';
            });

            var regData = {};
            var regStr = '';
            $('.getItemsRegAddress').each(function () {
                regData[$(this).attr('name')] = $(this).val();
                regStr = regStr + $(this).val() + ' ';
            });

            data['Registration_address'] = regStr;
            data['Living_place'] = livingStr;
            data['Registration_Entity'] = regData;
            data['Living_Entity'] = livingData;
        //}
        delete data['status'];
        data['Status'] = null;

        console.log(data);

        return data;
    }

    // add menual doc identity
    function addDocIdentity() {

        var citizenship = $('#family-partner-doc-citizenship')[0].selectize;
        var docidentityselect = $('#family-partner-doc-type')[0].selectize;

        var data = {};
        var url = _self.idDocTableRowDrawURL;

        _self.getItemsDocs.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            // appending to Data
            data[name] = $(this).val();
        });

        data.CitizenshipLibItem_Name = citizenship.getItem(citizenship.getValue())[0].innerHTML;
        data.TypeLibItem_Name = docidentityselect.getItem(docidentityselect.getValue())[0].innerHTML;

        data.ID = Math.floor((Math.random() * 100) + 1);
        data.Type = false;

        drawDocIdntityTableRow(data);
    }

    // draw docidentity table row
    function drawDocIdntityTableRow(data) {
        var curHtml = '<tr class="getManualDocIdentity" data-TypeLibItem_Name="' + data.TypeLibItem_Name + '" data-CitizenshipLibItem_Name="' + data.CitizenshipLibItem_Name + '" data-ID="" data-FromWhom="' + data.FromWhom + '" data-Date="' + data.Date + '" data-Number="' + data.Number + '" data-CitizenshipLibItem_ID="' + data.CitizenshipLibItem_ID + '" data-TypeLibItem_ID="' + data.TypeLibItem_ID + '">';
        curHtml += '<td>'+data.CitizenshipLibItem_Name+'</td>';
        curHtml += '<td>'+data.TypeLibItem_Name+'</td>';
            curHtml += '<td>'+data.Number+'</td>';
            curHtml += '<td>'+data.Date+'</td>';
            curHtml += '<td>'+data.FromWhom+'</td>';
            curHtml += '<td>';
            curHtml += '<button class="btn btn-sm btn-default btn-flat removeManualDocIdentity"><i class="fa fa-trash-o" title="Հեռացնել"></i></button>';
            curHtml += '</td>';
            curHtml += '</tr>';

            $('#menualDocIdentityTableBody').append(curHtml);

        bindIdDocTableRows();
    }

    // collect doc identity data
    function CollectDocIdentityData() {
        var list = [];

        $('.getManualDocIdentity').each(function () {
            var data = {};
            
            data.ID = $(this).data('id');
            data.FromWhom = $(this).data('fromwhom');
            data.Date = $(this).data('date');
            data.Number = $(this).data('number');
            data.CitizenshipLibItem_ID = $(this).data('citizenshiplibitem_id');
            data.CitizenshipLibItem_Name = $(this).data('citizenshiplibitem_name');
            data.TypeLibItem_ID = $(this).data('typelibitem_id');
            data.TypeLibItem_Name = $(this).data('typelibitem_name');

            list.push(data);
        });

        return list;
    }

    // events while searching for person
    function searchProcessEvents() {

        // hide main row for loader show (it is new user search)
        $('#foundusertable').addClass('hidden');
        $('#foundusererrorp').addClass('hidden');
    }

    // event after search
    function searchProcessAfterEvents(status) {
        if (status) {

            // toggle message table and <p>
            $('#foundusertable').removeClass('hidden');
            $('#foundusererrorp').addClass('hidden');

            //$('#menualAddBtn').attr('disabled', true);

        } 
        else {

            // current person is null
            _self.currentPerson = null;

            // hide table and show error <p>
            $('#foundusertable').addClass('hidden');
            $('#foundusererrorp').removeClass('hidden');
            $('#foundusererrorp').html('Որոնման արդյունքում ոչ մի անձ չի գտնվել');

            //$('#menualAddBtn').attr('disabled', false);

        }

        // disableing or enabling fields
        $('#modal-person-name-1').attr('disabled', status);
        $('#modal-person-name-2').attr('disabled', status);
        $('#modal-person-name-3').attr('disabled', status);
        $('#modal-person-bd').attr('disabled', status);
        $('#modal-person-social-id').attr('disabled', status);
        $('#modal-person-passport').attr('disabled', status);
    }

    // match data to table
    function matchPersonDataToTable(data) {

        // if user found
        if (typeof data != 'undefined' && data != null) {

            if (!data.Status) {

                // find again button toggle
                //$('#findpersonAgain').attr('disabled', false);

                // aftet search events
                searchProcessAfterEvents(false);
                if (_self.isBind) _self.searchCallback(false);
            }
            else if (data.Status) {

                // find again button toggle
                //$('#findpersonAgain').attr('disabled', true);

                // after search events
                searchProcessAfterEvents(true);

                // current person
                _self.currentPerson = data;

                // bind values to table
                drawUserList(data);

                $('.partcustom').removeClass("hidden");

                // draw fields
                drawUserFields(data, false);

                // bind collapse table info
                bindCollapseTable();
                if (_self.isBind) _self.searchCallback(true);
            }

            // disable loader
            var loadingService = _core.getService('Loading').Service;
            loadingService.disable('searchuserloader', 'foundusertable', false);
        }
        else {
            console.error('server returned null from server while searching for user')
        }
    }

    // draw user table row
    function drawUserList(user) {
        //console.log(user);
        var tbody = $('#searchuserinfoshort');

        // empty
        tbody.empty();

        curHtml = '';
        curHtml += '<tr><td data-title="#">';
        curHtml += '0</td>';
        curHtml += '<td data-title="Անուն Ազգանուն Հայրանուն">' + user.FirstName + ' ' + user.MiddleName + ' ' + user.LastName + '</td>';
        curHtml += '<td data-title="Ծննդ. ամսաթիվ">' + user.Birthday + '</td>';
        curHtml += '<td data-title="Սոց. քարտ">' + user.Personal_ID + '</td>';
        curHtml += '<td data-title="Անձնագիր">' + ((user.IdentificationDocuments.length != 0 ) ? user.IdentificationDocuments[0].Number : "") + '</td></tr>';

        var reg = user.Registration_Entity;
        var registerationData = reg.Registration_Region + ', ' + reg.Registration_Community + ' ' + (reg.Registration_Apartment != null ? reg.Registration_Apartment : "") + ', ' + reg.Registration_Street + ' ' + reg.Registration_Building_Type + ' ' + reg.Registration_Building;
        reg = user.Living_Entity;
        var livingData = reg.Registration_Region + ', ' + reg.Registration_Community + ' ' + (reg.Registration_Apartment != null ? reg.Registration_Apartment : "") + ', ' + reg.Registration_Street + ' ' + reg.Registration_Building_Type + ' ' + reg.Registration_Building;
        
        if (user.SexLibItem_ID == 153) user.GenderName = 'Իգական';
        else if (user.SexLibItem_ID == 154) user.GenderName = 'Արական';
        else user.GenderName = '';

        curHtml += ' <tr class="collapse"><td colspan="5"> <div class="row"> <div class="col-xs-12 col-md-4"><img class="img-responsive" src="' + user.PhotoLink + '" alt="" /></div> <div class="col-xs-12 col-md-8">';
        curHtml += ' <div class="form-group"> <label>Անձնագիր՝</label> <p>' + ((user.IdentificationDocuments.length != 0) ? user.IdentificationDocuments[0].Number : "") + ', ' + ((user.IdentificationDocuments.length != 0) ? user.IdentificationDocuments[0].Date : "") + ', ' + ((user.IdentificationDocuments.length != 0) ? user.IdentificationDocuments[0].FromWhom : "") + '</p> </div> <div class="form-group"> <label>Ազգություն՝</label> <p>' + user.NationalityLabel + '</p> </div> <div class="form-group"> <label>Քաղաքացիությւն՝</label> <p>' + user.Citizenship + '</p> </div> <div class="form-group"> <label>Սեռ՝</label> <p>' + user.GenderName + '</p> ';
        curHtml += '</div> <div class="form-group"> <label>Գրանցման հասցե՝</label> <p>' + registerationData + '</p> </div> <div class="form-group"> <label>Բնակության հասցե՝</label> <p>' + livingData + '</p> </div> </div> </div> </td> </tr>';

        tbody.append(curHtml);
    }


    // search person
    function searchPerson(type, data) {

        var url = null;
        if (type == 'doc') url = _self.searchByDocUrl;
        else if (type == 'name') url = _self.searchByNameUrl;

        // post service
        var postService = _core.getService('Post').Service;

        // sending to server and reciving answer
        postService.postJson(data, url, function (res) {
            matchPersonDataToTable(res);
        })
    }
    // draw user fields
    function drawUserFields(user, disabled) {

        // fiving values
        $('#modal-person-name-1').val(user.FirstName);
        $('#modal-person-name-2').val(user.LastName);
        $('#modal-person-name-3').val(user.MiddleName);
        $('#modal-person-bd').val(typeof user.Birthday != 'undefined' ? user.Birthday : user.BirthDay);
        $('#modal-person-social-id').val(typeof user.Personal_ID != 'undefined' ? user.Personal_ID : user.PSN);
        $('#modal-person-passport').val((user.IdentificationDocuments.length != 0) ? user.IdentificationDocuments[0].Number : "");

        $('#family-partner-sex')[0].selectize.setValue(user.SexLibItem_ID);
        $('#family-partner-nationality')[0].selectize.setValue(user.NationalityID);
        if (user.IdentificationDocuments.length != 0) {
            drawDocIdntityTableRow(user.IdentificationDocuments[0]);
        }

        $('#registrationRegionLiv').val((typeof user.Living_Entity != "undefined" && typeof user.Living_Entity.Registration_Region != " undefined") ? user.Living_Entity.Registration_Region : "");
        $('#registrationComLiv').val((typeof user.Living_Entity != "undefined" && typeof user.Living_Entity.Registration_Community != " undefined") ? user.Living_Entity.Registration_Community : "");
        $('#registrationStreetLiv').val((typeof user.Living_Entity != "undefined" && typeof user.Living_Entity.Registration_Street != " undefined") ? user.Living_Entity.Registration_Street : "");
        $('#registrationBuildingTypeLiv').val((typeof user.Living_Entity != "undefined" && typeof user.Living_Entity.Registration_Building != " undefined") ? user.Living_Entity.Registration_Building : "");
        $('#registrationBuildingLiv').val((typeof user.Living_Entity != "undefined" && typeof user.Living_Entity.Registration_Building_Type != " undefined") ? user.Living_Entity.Registration_Building_Type : "");
        $('#registrationAptLiv').val((typeof user.Living_Entity != "undefined" && typeof user.Living_Entity.Registration_Apartment != " undefined") ? user.Living_Entity.Registration_Apartment : "");

        $('#registrationRegionReg').val((typeof user.Registration_Entity != "undefined" && typeof user.Registration_Entity.Registration_Region != " undefined") ? user.Registration_Entity.Registration_Region : "");
        $('#registrationComReg').val((typeof user.Registration_Entity != "undefined" && typeof user.Registration_Entity.Registration_Community != " undefined") ? user.Registration_Entity.Registration_Community : "");
        $('#registrationStreetReg').val((typeof user.Registration_Entity != "undefined" && typeof user.Registration_Entity.Registration_Street != " undefined") ? user.Registration_Entity.Registration_Street : "");
        $('#registrationBuildingTypeReg').val((typeof user.Registration_Entity != "undefined" && typeof user.Registration_Entity.Registration_Building != " undefined") ? user.Registration_Entity.Registration_Building : "");
        $('#registrationBuildingReg').val((typeof user.Registration_Entity != "undefined" && typeof user.Registration_Entity.Registration_Building_Type != " undefined") ? user.Registration_Entity.Registration_Building_Type : "");
        $('#registrationAptReg').val((typeof user.Registration_Entity != "undefined" && typeof user.Registration_Entity.Registration_Apartment != " undefined") ? user.Registration_Entity.Registration_Apartment : "");

        // trigger change
        $('.checkValidatePerson').trigger('change');
    }

    // bind table collapse
    function bindCollapseTable() {

        // collapse table
        var collapseTable = $(".collapseTable");
        var collapseTableTr = collapseTable.find("tbody tr:not(.collapse)");

        collapseTableTr.click(function () {
            if ($(this).next("tr.collapse").hasClass("in")) {
                $(this).parents(".collapseTable").find("tr.collapse.in").removeClass("in");
            }
            else {
                $(this).parents(".collapseTable").find("tr.collapse.in").removeClass("in");
                $(this).parents(".collapseTable").find("input[name='modal-person-radio']").prop("checked", false);
                $(this).next("tr.collapse").addClass("in");
                $(this).find("input[name='modal-person-radio']").prop("checked", true);
            }
        });

        if (_self.searchCallback == null) {
            collapseTableTr.trigger('click');
        }
    }

    function bindIdDocTableRows() {

        var tableRow = $('.removeManualDocIdentity');

        tableRow.unbind().click(function () {

            $(this).parent().parent().remove();
        })
    }
}
/************************/


// creating class instance
var FindPersonWidget = new FindPersonWidgetClass();

// creating object
var FindPersonWidgetObject = {
    // important
    Name: 'FindPerson',

    Widget: FindPersonWidget
}

// registering widget object
_core.addWidget(FindPersonWidgetObject);

// Add LibItem widget

/*****Main Function******/
var LibItemWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.getItems = null
    _self.id = null;
    _self.mode = null;

    _self.modalUrl = "/_Popup_/Get_LibItem";
    _self.addUrl = '/Lists/AddLib';
    _self.editUrl = '/Lists/EditLib';
    _self.removeUrl = '/Lists/DelLib';
    _self.ModalName = 'libitem-modal';
    _self.finalData = {};
    
    _self.init = function () {
        console.log('Widget Inited');

        _self.finalData = {};
        _self.callback = null;
        _self.content = null;
        _self.getItems = null;
        _self.id = null;
        _self.mode = null;
    };

    // add action
    _self.Add = function (data, callback) {

        // initilize
        _self.init();

        console.log('Add called');

        _self.pathid = data.PathID;
        _self.libid = data.LibID;

        _self.mode = 'Add';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    }

    // edit action
    _self.Edit = function (data, callback) {
        // initilize
        _self.init();

        console.log('Edit called');

        _self.id = data.ID;
        _self.pathid = data.PathID;
        _self.libid = data.LibID;

        _self.mode = 'Edit';

        // save callback
        _self.callback = callback;

        // loading view
        loadView(_self.modalUrl);
    };

    // edit action
    _self.Remove = function (data, callback) {
        // initilize
        _self.init();

        console.log('Remove called');

        _self.id = data.ID;

        // save callback
        _self.callback = callback;

        // remove
        removeData();
    };

    // loading modal from Views
    function loadView(url) {

        var data = {};

        if (_self.id != null && _self.mode == "Edit") {
            data.ID = _self.id;
        }

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _self.content = res;

            _core.getService('Loading').Service.disableBlur('loaderDiv');

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.'+_self.ModalName).modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnModal');
        var checkItems = $('.checkValidateModal');
        var dates = $('.DatesModal');
        _self.getItems = $('.getItemsModal');
        var propertyselect = $('#propertyselect');
        var addPropertyButton = $("#addPropertyButton");

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind hide show release data
        propertyselect.unbind().bind('change', function () {

            var value = propertyselect[0].selectize.getValue();
            
            if (value != "" && typeof value != "undefined" && value != null) {
                $(".propertyselectvalue").addClass("hidden");
                $("#propertyselectvaluediv" + value).removeClass("hidden");
            }
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == 'Add') {

                data.LibPathID = _self.pathid;
                data.LibToLibID = _self.libid;

                // add
                addData(data);
            }
            else if (_self.mode == 'Edit') {

                data.LibID = _self.id;

                // edit
                editData(data);
            };

            //hiding modal
            $('.'+_self.ModalName).modal('hide');
        });

        // bind add property
        addPropertyButton.unbind().bind("click", function (e) {
            e.preventDefault();
            var data = {};
            var propertyselectSelectize = $("#propertyselect")[0].selectize;
            data.PropertyID = propertyselectSelectize.getValue();
            data.PropertyName = propertyselectSelectize.getItem(propertyselectSelectize.getValue())[0].innerHTML;

            var dataCustom = $('#propertyselectvalue' + data.PropertyID).data("custom");

            if (dataCustom != null && typeof dataCustom != "undefined" && dataCustom != "") {
                data.ValueName = $('#propertyselectvalue' + data.PropertyID).val();
                data.ValueID = $('#propertyselectvalue' + data.PropertyID).data("id");
                data.Custom = true;
            }
            else {
                var tempselect = $('#propertyselectvalue' + data.PropertyID)[0].selectize;
                data.ValueID = tempselect.getValue();
                data.ValueName = tempselect.getItem(tempselect.getValue())[0].innerHTML;
                data.Custom = false;
            }

            data.ValueID = (typeof data.ValueID == 'undefined' ? '' : data.ValueID);

            drawPropertyList(data);
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'ReleaseBasisLibItemID' || curName == 'ReleaseDate' || curName == 'ReleaseNote' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // selectize
        $(".selectizeModal").each(function () {
            $(this).selectize({
                create: false
            });
        });

        // bind property list events
        bindPropertyListEvents();
    }

    // draw property list
    function drawPropertyList(data) {

        var curHtml = "<tr data-newitem='true' class='getPropertyListRow' data-custom='" + data.Custom + "' data-propertyid='" + data.PropertyID + "' data-propertyname='" + data.PropertyName + "' data-valueid='" + data.ValueID + "' data-valuename='" + data.ValueName + "' data-id=''>";
        curHtml += "<td>" + data.PropertyName + "</td>";
        curHtml += "<td>" + data.ValueName + "</td>";
        curHtml += '<td><button data-id="" class="btn btn-sm btn-default btn-flat removePropertyListRow" title="Հեռացնել"><i class="fa fa-trash-o" ></i></button></td>';
        curHtml += "</tr>";

        $('#propertyselectdraw').append(curHtml);

        // bind draw ContentList events
        bindPropertyListEvents();
    }
    
    // remove property from selected list
    function bindPropertyListEvents() {
        $('.removePropertyListRow').unbind().click(function (e) {
            e.preventDefault();

            $(this).parent().parent().remove();
        })
    }

    // add
    function addData(data) {

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService("Loading").Service.enableBlur("loaderDiv");

        postService.postJson(data, _self.addUrl, function (res) {

            _core.getService("Loading").Service.disableBlur("loaderDiv");

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding');
        })
    }

    // edit
    function editData(data) {
        // post service
        var postService = _core.getService('Post').Service;

        _core.getService("Loading").Service.enableBlur("loaderDiv");

        postService.postJson(data, _self.editUrl, function (res) {

            _core.getService("Loading").Service.disableBlur("loaderDiv");

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on editing');
        })
    }

    // remove
    function removeData() {

        var data = {};
        data.LibID = _self.id;

        // post service
        var postService = _core.getService('Post').Service;

        _core.getService("Loading").Service.enableBlur("loaderDiv");

        postService.postJson(data, _self.removeUrl, function (res) {

            _core.getService("Loading").Service.enableBlur("loaderDiv");

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on removing');
        })
    }

    // collect data
    function collectData() {

        // array of violation list
        //_self.finalData["ViolationList"] = [];

        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');

            if (name == "ViolationList") {
                //var tempValue = $(this).val();
                //for (var i in tempValue) {
                //    var temp = {};
                //    temp["LibItemID"] = parseInt(tempValue[i]);
                //    _self.finalData[name].push(temp);
                //}
            }
            else {
                // appending to Data
                _self.finalData[name] = $(this).val();
            }

            _self.finalData["propList"] = collectPropertyList();

        });

        return _self.finalData;
    }

    // collect property list
    function collectPropertyList() {
        var list = [];
        $(".getPropertyListRow").each(function () {

            var data = {};
            var PropsEntity = {};
            var PropValuesEntity = {};

            PropsEntity.ID = $(this).data("propertyid");
            PropsEntity.Fixed = !$(this).data("custom");
            PropsEntity.NewItem = $(this).data("newitem");
            PropValuesEntity.ID = $(this).data("valueid");
            PropValuesEntity.Value = $(this).data("valuename");

            data.property = PropsEntity;
            data.value = PropValuesEntity;

            list.push(data);
        });
        return list;
    }
}
/************************/


// creating class instance
var LibItemWidget = new LibItemWidgetClass();

// creating object
var LibItemWidgetObject = {
    // important
    Name: 'LibItem',

    Widget: LibItemWidget
}

// registering widget object
_core.addWidget(LibItemWidgetObject);

// Order widget

/*****Main Function******/
var OrderWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.PrisonerID = null;
    _self.type = null;
    _self.id = null;
    _self.URL = null;
    _self.EmployeeID = null;
    _self.mode = null;
    _self.modalUrl = "/_Popup_/Get_Order";
    _self.ModalName = "add-order-modal";
    _self.viewUrl = "/_Popup_Views_/Get_Order";
    _self.finalFormData = new FormData();
    _self.removeFileURL = '/Structure/RemoveFile';
    _self.OrderURL = '/_Data_Main_/Order';
    _self.editFileURL = '/_Data_Main_/EditFile';
    
    // action list
    _self.actionList = {
    };

    _self.init = function () {
        console.log('File Widget Inited');

        _self.finalFormData = new FormData();
        _self.callback = null;
        _self.URL = null;
        _self.EmployeeID = null;
        _self.content = null;
        _self.mode = null;
        _self.id = null;
        _self.PrisonerID = null;
        _self.type = null;
    };

    // ask action
    _self.Add = function (data, callback) {

        // initilize
        _self.init();

        console.log('File Add called');

        // save callback
        _self.callback = callback;

        _self.mode = 'Add';

        _self.URL = data.URL;

        _self.EmployeeID = data.EmployeeID;

        // loading view
        loadView(_self.modalUrl);
    };

    // ask action
    _self.Edit = function (data, callback) {

        // initilize
        _self.init();

        console.log('File Edit called');

        // save callback
        _self.callback = callback;

        _self.mode = 'Edit';

        _self.type = data.TypeID;

        _self.id = data.ID;

        // loading view
        loadView(_self.modalUrl);
    };

    // ask action
    _self.View = function (data, callback)
    {

        // initilize
        _self.init();

        console.log('File Edit called');

        // save callback
        _self.callback = callback;

        _self.mode = 'View';

        _self.type = data.TypeID;

        _self.id = data.ID;

        // loading view
        loadView(_self.viewUrl);
    };

    // remove file Action
    _self.Remove = function (data, callback) {

        // initilize
        _self.init();

        _self.type = data.TypeID;


        _core.getService('Loading').Service.enableBlur('loaderClass');

        // yes or no widget
        _core.getWidget('YesOrNo').Widget.Ask(1, function (res) {

            _core.getService('Loading').Service.disableBlur('loaderClass');

            if (res) {

                var url = _self.removeFileURL;
                
                _core.getService('Post').Service.postJson(data, url, function (res) {
                    if (res != null) {
                        callback(res);
                    }
                });
            }
        });
    }

    // loading modal from Views
    function loadView(url) {
            var data = {};

            if (_self.id != null && _self.mode == "Edit") {
                data.ID = _self.id;
            }

            _core.getService("Post").Service.postPartial(data, url, function (res) {

                _self.content = res;

                _core.getService('Loading').Service.disableBlur('loaderDiv');

                // content
                $('#widgetHolder').html(_self.content);

                // bind events for yes and no
                bindEvents();

                // showing modal
                $('.' + _self.ModalName).modal('show');
            });
    }

    // binding buttons for Modal
    function bindEvents() {

        // buttons and fields
        var acceptBtn = $('#acceptBtnModal');
        var checkItems = $('.checkValidateModal');
        var dates = $('.DatesModal');
        _self.getItems = $('.getItemsModal');
        var propertyselect = $('#propertyselect');
        var addPropertyButton = $("#addPropertyButton");

        // unbind
        acceptBtn.unbind();
        checkItems.unbind();

        // date range picker
        dates.daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            "minDate": "01/01/1900",
            "locale": _core.getService('Calendar').Service.getDateTimeLocal(false),
            "buttonClasses": "btn btn-sm btn-flat",
            "applyClass": "color-success-bg color-success-hover-bg color-text-white",
            "cancelClass": "btn-default"
        });
        dates.on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
            dates.trigger('change');
        });
        dates.on("cancel.daterangepicker", function (ev, picker) {
            $(this).val("");
            dates.trigger('change');
        });

        // bind hide show release data
        propertyselect.unbind().bind('change', function () {

            var value = propertyselect[0].selectize.getValue();

            if (value != "" && typeof value != "undefined" && value != null) {
                $(".propertyselectvalue").addClass("hidden");
                $("#propertyselectvaluediv" + value).removeClass("hidden");
            }
        });

        // bind accept
        acceptBtn.bind('click', function () {

            var data = collectData();

            if (_self.mode == 'Add') {

                data.append("EmployeeID", _self.EmployeeID);

                // add
                addData(data);
            }
            else if (_self.mode == 'Edit') {

                data.LibID = _self.id;

                // edit
                editData(data);
            };

            //hiding modal
            $('.' + _self.ModalName).modal('hide');
        });

        // bind add property
        addPropertyButton.unbind().bind("click", function (e) {
            e.preventDefault();
            var data = {};
            var propertyselectSelectize = $("#propertyselect")[0].selectize;
            data.PropertyID = propertyselectSelectize.getValue();
            data.PropertyName = propertyselectSelectize.getItem(propertyselectSelectize.getValue())[0].innerHTML;

            var dataCustom = $('#propertyselectvalue' + data.PropertyID).data("custom");

            if (dataCustom != null && typeof dataCustom != "undefined" && dataCustom != "") {
                data.ValueName = $('#propertyselectvalue' + data.PropertyID).val();
                data.ValueID = $('#propertyselectvalue' + data.PropertyID).data("id");
                data.Custom = true;
            }
            else {
                var tempselect = $('#propertyselectvalue' + data.PropertyID)[0].selectize;
                data.ValueID = tempselect.getValue();
                data.ValueName = tempselect.getItem(tempselect.getValue())[0].innerHTML;
                data.Custom = false;
            }

            data.ValueID = (typeof data.ValueID == 'undefined' ? '' : data.ValueID);

            drawPropertyList(data);
        });

        // check validate and toggle accept button
        checkItems.bind('change keyup', function () {
            var action = false;

            checkItems.each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    var curName = $(this).attr('name');
                    if (curName == 'Description' || curName == 'ReleaseBasisLibItemID' || curName == 'ReleaseDate' || curName == 'ReleaseNote' || curName == 'Note' || curName == 'Notes') $(this).val(' ');
                    else action = true;
                }
            });

            acceptBtn.attr("disabled", action);
        });

        // selectize
        $(".selectizeModal").each(function () {
            $(this).selectize({
                create: false
            });
        });
    }

    // collect data
    function collectData() {

        // collect data
        _self.getItems.each(function () {

            // name of Field (same as in Entity)
            var name = $(this).attr('name');
            if (name == "File") {
                _self.finalFormData.append(name, $(this)[0].files[0], $(this).val());
            }
            else {
                _self.finalFormData.append(name, $(this).val());
            }
        });

        return _self.finalFormData;
    }

    // add Data
    function addData(data) {

        var postService = _core.getService('Post').Service;

        postService.postFormData(data, _self.URL, function (res) {

            if (res != null) {
                _self.callback(res);
            }
            else alert('serious eror on adding Order');
        })
    }

}
/************************/


// creating class instance
var OrderWidget = new OrderWidgetClass();

// creating object
var OrderWidgetObject = {
    // important
    Name: 'Order',

    Widget: OrderWidget
}

// registering widget object
_core.addWidget(OrderWidgetObject);

// yes or no widget

/*****Main Function******/
var YesOrNoWidgetClass = function () {
    var _self = this;
    _self.callback = null;
    _self.content = null;
    _self.modal = $('.confirmDeleteModal');
    _self.modalUrl = "/_Popup_/Get_YesOrNO";
    _self.type = null;
    _self.ModalName = 'confirmDeleteModal';

    // action list
    _self.actionList = {
    };

    _self.init = function () {
        console.log('YesOrNo Widget Inited');
        _self.type = null;
    };

    // ask action
    _self.Ask = function (type, callback) {
        // initilize
        _self.init();
        _self.type = type;

        console.log('YesOrNo Ask called');

        // save callback
        _self.callback = callback;

        // loading view
        loadView();
    };

    // loading modal from Views
    function loadView() {

        var data = {};
        var url = _self.modalUrl;
        
        data.type = _self.type;

        _core.getService("Post").Service.postPartial(data, url, function (res) {

            _core.getService("Loading").Service.disableBlur("loaderDiv");
            _core.getService('Loading').Service.disableBlur('loaderClass');

            _self.content = res;

            // content
            $('#widgetHolder').html(_self.content);

            // bind events for yes and no
            bindEvents();

            // showing modal
            $('.' + _self.ModalName).modal('show');
        });
    }

    // binding buttons for Modal
    function bindEvents() {
        var yesBtn = $('#acceptremove');
        var noBtn = $('#declineremove');

        // unbind
        yesBtn.unbind();
        noBtn.unbind();

        // bind
        yesBtn.bind('click', function () {
            // calling callback
            if (_self.callback != null)
                _self.callback(true);

            // remove callback
            _self.callback = null;

            //hiding modal
            $('.confirmDeleteModal').modal('hide');
        });

        noBtn.bind('click', function () {
            //hiding modal
            $('.confirmDeleteModal').modal('hide');

            // call callback
            _self.callback(false);

            // remove callback
            _self.callback = null;
        });
    }
}
/************************/


// creating class instance
var YesOrNoWidget = new YesOrNoWidgetClass();

// creating object
var YesOrNoWidgetObject = {
    // important
    Name: 'YesOrNo',

    Widget: YesOrNoWidget
}

// registering widget object
_core.addWidget(YesOrNoWidgetObject);