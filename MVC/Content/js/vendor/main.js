$(function () {

	$(".selectize").each(function () {
		$(this).selectize({
			create: false
		});
	});

	var approveSelect1 = $("#approveSelect1");
	var approveSelect1Drop = $("#approveSelect1Drop");

	approveSelect1Drop.click(function (event) {
		approveSelect1.parents(".approveSelectWrap").toggleClass("open");
	});

	var openClass = $(".open");

	$("body").on("click", function (e) {
		if (!approveSelect1.parents(".approveSelectWrap").is(e.target) && approveSelect1.parents(".approveSelectWrap").has(e.target).length === 0 && openClass.has(e.target).length === 0) {
			approveSelect1.parents(".approveSelectWrap").removeClass("open");
		}
	});

	var formsWrapTabsLeftHeight = $(".formsWrapTabsLeft").outerHeight();
	if ($(".formsWrapTabsRight").length) {
		$(".formsWrapTabsRight").css("min-height", formsWrapTabsLeftHeight);
	}
});